% Everfree Outpost Changelog

## Release 2018-09-17

First release of Everfree Outpost version 2!

Almost everything has changed:

 * Movement: Use WASD to move.  There is now only one movement speed, which is
   roughly as fast as the old gallop speed.
 * Structures: Left-click on a structure, such as a workbench or door, to
   interact with it.  Right-click on a structure to destroy it.  Right-clicking
   will automatically use the appropriate tool, if you have one equipped.
 * Hotbar: Open the inventory dialog (C), then hover over an item and press
   1-9 to assign it to the hotbar.  Same goes for abilities, using the ability
   dialog (Z).
 * Using items: Press 1-9 to select an item from the hotbar.  The item will
   appear on your mouse cursor.  Left-click to use the item.  Deselect the item
   by right-clicking, or by pressing 1-9 again.
 * Crafting: Each crafting station now has an internal inventory for materials
   and crafted results.  To craft something, select a recipe, move the required
   materials into the crafting station's inventory, and press the "Craft"
   button.  Crafting is no longer instantaneous, and the crafting process will
   pause automatically if you close the crafting dialog while it's running.
 * Characters: Character creation now supports a much wider range of coat and
   mane/tail colors.


## Next Release

(Last modified: 2018-10-08)

 * Fixed: Server no longer locks up when double-clicking a container or
   crafting station
 * Fixed: Server no longer crashes when closing inventory dialog while crafting
   is paused waiting for inputs
 * Fixed: Pony no longer teleports to spawn half the time when moving
 * Fixed: Pony no longer "floats away" through all trees and rocks into a black
   void
 * Fixed: Trees and rocks no longer vanish when moving too far from spawn
 * Fixed: Server no longer crashes when a player connects from a new
   browser/tab while already connected
