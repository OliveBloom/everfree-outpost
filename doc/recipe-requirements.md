# Recipe Requirements

There are two requirements that must be satisfied for a player to craft a
recipe: the player must have the recipe's required ability, and the player must
be using a crafting station that supports the recipe's crafting class.


## Ability

A recipe may have a *required ability*, which is an item (ability) reference.
The recipe then appears in the crafting dialog recipe list only if the required
ability is present in the player's ability inventory.  This is useful for
limiting recipes to players that have found a particular blueprint: the
blueprint item grants an ability when consumed, and that ability is required
for the recipe.

Recipes with no required ability are always available.


## Crafting Class

Each recipe has a *crafting class*, which is a string identifier.  The recipe
appears in the crafting dialog only when the player is using a crafting station
that supports the recipe's crafting class.  This is useful for gating recipes
to specific tiered crafting stations, while also allowing higher-tier stations
to craft lower-tier recipes.

There are currently three categories of crafting classes in use:

 * `basic`: Craftable at a workbench.  Currently there is only one tier of
   workbench.
 * `smelt-T`, where `T` is a metal tier: Craftable at a furnace that supports
   the given metal tier or higher.
 * `metalwork-T`, where `T` is a metal tier: Craftable at an anvil that
   supports the given metal tier or higher.

Metal tiers are:

 * `1`: Copper
 * `2`: Iron
 * `3e`, `3p`, `3u`: T1 tribal metals (titanium, iridium, palladium)
 * `4e`, `4p`, `4u`: T2 tribal metals (adamantium, aetherium, scintillium)

Higher-tier crafting stations support all lower tiers as well, except tribal
crafting stations are still restricted to their own tribe.  For example, a
furnace that supports `smelt-4p` will also support `smelt-3p`, `smelt-2`, and
`smelt-1`, but not `smelt-3u`.

There is currently a limit of 32 crafting classes, since the supported classes
for a given station get packed into a 32-bit field.

