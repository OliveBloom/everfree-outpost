# Data definition format

The `data2` game data compiler is based on a process of repeated template
expansion.  A game object definition looks like this:

    item torch
        key1 = value1
        key2 = value2
        key3:
            long value
            with multiple lines

Upon encountering such a definition, the compiler will load the file
`item.tmpl`, which contains lower-level definitions in the same format.  The
template will be expanded using the arguments (`key1`, `key2`, and `key3`)
provided by the caller.  For example:

    raw_item item_%{name}
        code1:
          %if key1
            a + b
          %else
            %block key3

        code2 = %key1 + %key2

The process bottoms out with the builtin `raw` template, whose arguments are
used directly to constructing the data definitions and server-side scripts.

Note that top-level `.des` files are expanded using the same method as
`.tmpl`s, but with no arguments.  The main use for this is to generate multiple
similar definitions using the `%for` directive.


## Detailed compilation model

This section covers the operation of the data compiler in more detail.

The data compiler begins by loading all `.des` files in the data directory.
For each one, it runs template expansion with an empty scope (no arguments),
then parses the file to get a list of blocks.  Each block has a kind, a name,
and a dictionary that maps string argument names to string values.

For each block whose kind is not `raw`, the compiler looks for a template file,
named the same as the kind with a `.tmpl` extension.  It runs template
expansion on the file contents using the original block's dictionary (plus an
entry mapping `'name'` to the name of the block) as the variable scope.  Then
it parses the resulting text for new blocks.  On each new block, the compiler
processes any inheritance or override definitions, then recursively processes
each block.

For each `raw` block, the compiler records the name and the values of the
`gen_images_code`, `gen_data_code`, and `script_code` fields, for use in code
generation once all blocks have been parsed.


## Inheritance and overrides

Inheritance and overrides are handled after parsing a block in an expanded
template file.  Both mechanisms provide ways of copying values from the current
template scope (or the arguments of the originating block) into the newly
parsed block.

Inheritance is indicated by providing an argument named `__inherit__` whose
value is a list of comma-separated argument names.  For each named argument,
the value of that argument in the originating block (if it was present) will be
copied into the argument dict of the current block.  This is similar to writing
`foo = %foo` for each inherited argument, but it correctly passes through
multiline strings without error.

Overrides are indicated by providing an argument `__override__` with a list of
comma-separated argument names.  For each named argument `foo`, if the
originating block contains an argument `override_foo`, then the value of `foo`
in the new block will be replaced with the value of the originating block's
`override_foo`.  The behavior can be replicated by the following code:

    foo:
      %if override_foo
        %block override_foo
      %else
        # Normal value for `foo` goes here...
      %end


## Templating language

The template expansion process uses a simple language based on Python.  Most
text is emitted verbatim, with `%`-directives used to invoke special
substitution logic.

Template expansion runs within a local scope that maps variable names to Python
values.  Various `%`-directives can include Python expressions that refer to
variables in that scope.

### Placeholders

The form `%{expr}` expands to the string that results from evaluating the
Python expression `expr` in the current scope.  The result of `expr` should
always be a string - the template expander does not invoke `str()`
automatically.

The form `%name` is short for `%{name}`, with the limitation that `name` must
contain only alphanumeric characters and underscores.  Also, this form always
takes the longest valid name following the `%`, so `%foo_bar.baz` expands to
`%{foo_bar}.baz` - if you want `%{foo}_bar.baz`, you must write that
explicitly.

### Line joining

If a line ends with a trailing `%`, the trailing end-of-line character will be
omitted from the output.  So this input:

    foo %
    bar

Produces the output `foo bar`.

### Conditionals

Conditional statements are written as follows:

    %if cond
        Text 1
    %elif foo == bar
        Text 2
    %else
        Text 3
    %end

This works like a Python conditional, emitting "Text 1" if `cond` is true,
"Text 2" if `cond` is false and `foo == bar`, and "Text 3" if `cond` is false
and `foo != bar`.  The body of each block is processed as normal, so it may
contain further `%`-directives.

### Loops

For loops are written as follows:

    %for x, y in collection
        Text with %x and %y
    %end

Each element in `collection` is destructured using the pattern `x, y`, and the
text in the body is expanded and emitted once for each element.  Note that
nested patterns (such as `x, (y, z)`) are not supported.

### Inline conditionals and loops

Conditionals and loops can be written inline using a short form:

    %if cond% text 1 %end%, %for x in items% text 2 with %{x} %end%

### Imports

Variables from Python modules can be imported into the current scope by writing
the appropriate Python import statement prefixed with `%`:

    %import foo
    %from bar import baz

### Blocks

The `%block expr` directive must appear on a line by itself, but it may be
preceded by any amount of whitespace.  The expander will evaluate `expr` to
obtain a string, indent each line of the string to the same level as the
original `%block` directive, and emit the result.  This is useful for emitting
a block of code contained in a variable with appropriate indentation.

