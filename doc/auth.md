# Authentication

Everfree Outpost supports Minecraft-style single sign-on, where players can log
in to any game server using a single Everfree Outpost account.

Here is the process for authenticating your client to a game server.

 1. Log in to the auth server, `auth.everfree-outpost.com`, by POSTing your
    `name` and `password` to `/login`.  This will give you a session cookie for
    the auth server, which can be reused to connect to any number of game
    servers.

 2. Connect to the game server, and complete the handshake.  Once the encrypted
    session is set up, get its channel binding token `cb_token`.  Leave the
    connection open while you perform the next step.

 3. POST to `https://auth.everfree-outpost.com/api/sign_challenge` the JSON
    object `{ 'challenge': base64(cb_token) }`.  (Note that this protocol uses
    URL-safe base64, where `+/` are replaced by `-_`.)  Don't forget to include
    the auth server session cookie with the request.  You should receive a JSON
    object `{ 'status': 'ok', 'response': <some base64'd string> }`.

 4. Send the game server an `AUTH_RESPONSE` message with `unbase64(response)`
    as the body.  You should receive an `AUTH_RESULT` with 2 bytes of flags
    followed by a string.  If flag `1 << 0` is set, then authentication was
    successful, and the string is your login name.  Otherwise, the string is an
    error message.

 5. At this point, authentication is finished.  Send `READY` to begin receiving
    game data from the server.


## Behind the scenes

At startup, the game server queries the auth server to obtain its public key
(`/api/get_verify_key`).

The `AUTH_RESPONSE` message contains the user's account name and ID, the channel
binding token, and a valid signature from the auth server.  The game server
checks the signature using the public key it downloaded previously, and also
checks that the channel binding token is the same as the one it sees on its own
side of the encrypted session.
