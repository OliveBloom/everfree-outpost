Everfree Outpost Frequently Asked Questions
===========================================

You expect me to read all this text?
------------------------------------

What a lazy pony. Here's the important stuff.

  - Read the instructions (F2 -> Instructions)
  - Type `/help` in chat


How do I make things?
---------------------

Use anvils to craft items. You should always carry at least one.
There is always an anvil at at spawn - Type `/spawn` to get there.

To place items in the world:

  - Send them to your hotbar by highlighting them in your inventory and pressing
    a number 1-9
  - Press "D"


How do I crops?
---------------

Every pony can take from the cornucopia at spawn once to receive five of a
random type of crop. To get all of them, you'll have to trade, so make friends!

Crops can be planted in dirt tilled with a shovel. They will slowly grow and can
be harvested for a return of 1-3 crops.

The crops are:

  - Tomatoes
  - Corn
  - Peppers
  - Cucumbers
  - Potatoes
  - Carrots
  - Artichokes


What's with the tools?
----------------------

Shovels
:   for tilling the soil into dirt to plant crops.

Axes
:   for picking up items that you would otherwise interact with when
    pressing "A" like chests or barrels, and for removing trees.

Pickaxes
:   for picking up anvils, digging through cave walls, and
    destroying stones.

Mallets
:   for rotating walls and floors.


What do wards do?
-----------------

Wards stop ponies from modifying a 32x32 area other than the ward's creator.

A pony can only have one ward at a time.

You can grant your friends permission to build with commands.
    
/permit [name]
:   Give [name] permission to bypass your ward

/revoke [name]
:   Revoke [name]'s permission to bypass your ward


How do I explore caves and dungeons?
------------------------------------

There are two interesting places to loot in Everfree Outpost; caves and
dungeons.

Caves are found on the overworld down holes or through doors in the side of
cliffs. You can break the walls of caves with a pickaxe. Inside caves there are
chests which can contain crystals, wood, stone, books, and sometimes hats and
blueprints. There are also glowing red crystals which allow you to access
dungeons.

Dungeons are accessed from the glowing red crystals inside caves. They are very
dark, so bring lots of torches, and consider using your unicorn "candle" ability
to help light the way. Inside dungeons there are chests which spawn colored
gems, books, keys, and sometimes hats. Fountains and trophies naturally spawn.
Some areas of dungeons are hidden behind locked doors which require keys to
access, or gem locks which require you to solve a simple color mixing puzzle.


How do I hats?
--------------

Hats are found in chests in caves and dungeons.

  - To put them on, send them to your hotbar and press "D" like any other item.
  - To take them off, select the "Remove Hat" ability with "W", and activate it
    with "S". Make sure you have inventory space before removing your hat!


How do I colored floors and torches?
------------------------------------

Blueprints can be found in cave chests which can add colored torches and floors
to the crafting list. The added recipes are as follows:

| Recipe            | Ingredients            |
|-------------------+------------------------|
| Colored torch x10 | Colored gem, torch x10 |
| Colored floor x20 | Colored gem, floor x20 |


How can I make a teleporter subnetwork?
---------------------------------------

Teleporters will only connect to other teleporters which share the same network
name. Remember what you type in the network name field when you make a
teleporter, because you will not be able to view the network name once it is
made!


Is this game open source?
-------------------------

Yes. The source code is on GitHub.

<https://github.com/everfree-outpost/everfree-outpost/>
