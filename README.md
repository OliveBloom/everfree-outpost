# Everfree Outpost


## Building

* Install dependencies.

  The easy way: install the [Nix package manager](https://nixos.org/nix/), then
  run `nix-shell util/nix`.  Nix will download and build all the dependencies.

  The hard way: read the comments in `util/nix/default.nix`, and manually
  install the listed packages.

* Run `./configure`.

  If you aren't using `nix-shell`, you'll likely need to manually specify
  important paths, such as the path to the Rust source code.  See `./configure
  --help` for a list of possible flags.

* Run `ninja`.


## Running

* Run `util/run_web_server.sh`, `util/run_auth_server.sh`, and
  `dist/server/bin/run_server.sh`.

* Open a browser to `http://localhost:8889/`.


### Server Administration

To make yourself a superuser on your server, first join the game, then run:

    echo 'eng.client_by_name("Your Name").extra()["superuser"] = True' | socat - unix:dist/server/repl

Afterward, you can use `/help` in-game to see the new superuser commands.

To shut down or restart the server cleanly, run:

    echo restart_server | socat - unix:dist/server/repl     # restart
    echo shutdown | socat - unix:dist/server/repl           # shutdown

In particular, use the restart command after compiling new changes in order to
see the effect in-game.
