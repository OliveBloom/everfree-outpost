import base64
import builtins
import json
import pickle
import socket



class RemoteObject:
    def __init__(self, session, pin):
        self._session = session
        self._pin = pin

    def __dir__(self):
        return self._session.obj_dir(self)

    def __getattr__(self, k):
        return self._session.obj_getattr(self, k)

    def keys(self):
        return self._session.obj_keys(self)

    def __getitem__(self, k):
        return self._session.obj_getitem(self, k)

    def __str__(self):
        return self._session.obj_str(self)

    def __repr__(self):
        return self._session.obj_repr(self)

    def __del__(self):
        self._session.unpin(self)

NOT_PRESENT = object()

class RemoteNamespace(dict):
    def __init__(self, remote):
        self._remote = remote

    def __getitem__(self, k):
        try:
            return super(RemoteNamespace, self).__getitem__(k)
        except KeyError:
            try:
                return self._remote[k]
            except KeyError:
                raise

    def __setitem__(self, k, v):
        super(RemoteNamespace, self).__setitem__(k, v)

    def __delitem__(self, k):
        super(RemoteNamespace, self).__delitem__(k)

    def setdefault(self, k, default):
        v = super(RemoteNamespace, self).get(k, NOT_PRESENT)

        if v is not NOT_PRESENT:
            return v

        try:
            return self._remote[k]
        except KeyError as e:
            return super(RemoteNamespace, self).setdefault(k, default)

    def get(self, k, default=None):
        v = super(RemoteNamespace, self).get(k, NOT_PRESENT)

        if v is not NOT_PRESENT:
            return v

        try:
            return self._remote[k]
        except KeyError as e:
            return default

    def update(self, *args, **kwargs):
        super(RemoteNamespace, self).update(*args, **kwargs)

    def keys(self):
        return set(super(RemoteNamespace, self).keys()) | set(self._remote.keys())


class BaseSession:
    def query(self, j):
        raise NotImplementedError()

    def _call_value(self, cmd, **kwargs):
        dct = kwargs.copy()
        dct['cmd'] = cmd
        result = self.query(dct)

        for l in result['print']:
            print(l)

        if result['ok']:
            return result['value']
        else:
            #print(result['backtrace'])

            exc_type = result['exc_type']

            exc_class = getattr(builtins, exc_type, None)
            if exc_class is not None:
                raise exc_class(result['exc_msg'])
            else:
                raise RuntimeError('%s: %s' % (exc_type, result['exc_msg']))

    def _call_pin(self, cmd, **kwargs):
        v = self._call_value(cmd, **kwargs)
        return RemoteObject(self, v)

    def get_globals(self):
        return self._call_pin('get_globals')

    def get_locals(self):
        return self._call_pin('get_locals')

    def unpin(self, obj):
        return self._call_value('unpin', pin=obj._pin)

    def compile(self, source, filename, mode,
            flags=0, dont_inherit=False, optimize=-1):
        source = base64.b64encode(pickle.dumps(source)).decode('ascii')
        return self._call_pin('compile',
                source=source, filename=filename, mode=mode,
                flags=flags, dont_inherit=dont_inherit, optimize=optimize)

    def run_code(self, code):
        return self._call_value('run_code', code=code._pin)

    def obj_dir(self, obj):
        return self._call_value('obj_dir', obj=obj._pin)

    def obj_getattr(self, obj, k):
        return self._call_pin('obj_getattr', obj=obj._pin, k=k)

    def obj_keys(self, obj):
        return self._call_value('obj_keys', obj=obj._pin)

    def obj_getitem(self, obj, k):
        return self._call_pin('obj_getitem', obj=obj._pin, k=k)

    def obj_str(self, obj):
        return self._call_value('obj_str', obj=obj._pin)

    def obj_repr(self, obj):
        return self._call_value('obj_repr', obj=obj._pin)


class LocalSession(BaseSession):
    def __init__(self, s):
        self.s = s

    def query(self, j):
        return self.s.handle(j)


log = open('py.log', 'w')

class UnixRemoteSession(BaseSession):
    def __init__(self, path):
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.sock.connect(path)
        self.sockfile = self.sock.makefile('rw')

    def query(self, j):
        log.write(json.dumps(j) + '\n')
        log.flush()
        self.sockfile.write(json.dumps(j) + '\n')
        self.sockfile.flush()
        return json.loads(self.sockfile.readline())
