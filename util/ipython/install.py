import json
import os
import sys
import tempfile

from jupyter_client.kernelspec import KernelSpecManager

def install():
    with tempfile.TemporaryDirectory() as d:
        j = {
            'argv': [
                sys.executable,
                os.path.join(os.path.abspath(os.path.dirname(__file__)), 'run_kernel.py'),
                '-f', '{connection_file}',
            ],
            'display_name': 'Everfree Outpost',
            'language': 'python3',
            'codemirror_mode': 'python3',
        }

        with open(os.path.join(d, 'kernel.json'), 'w') as f:
            json.dump(j, f, indent='  ')

        KernelSpecManager().install_kernel_spec(d, 'outpost', user=True, replace=True)

if __name__ == '__main__':
    install()


