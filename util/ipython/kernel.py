from collections import namedtuple
import os
import sys
from ipykernel.ipkernel import IPythonKernel
from ipykernel.zmqshell import ZMQInteractiveShell
import traitlets


#from server import Session
from client import RemoteNamespace, UnixRemoteSession


class RemoteModule:
    def __init__(self, dct):
        object.__setattr__(self, '_dct', dct)
        self.__name__ = '__main__'

    def __getattr__(self, k):
        return self._dct[k]

    def __setattr__(self, k, v):
        self._dct[k] = v

    def __delattr__(self, k):
        del self._dct[k]


class OutpostShell(ZMQInteractiveShell):
    def __init__(self, *args, **kwargs):
        #self.__session = Session()
        #self.__client = LocalSession(self.__session)
        self.__client = UnixRemoteSession(os.environ['OUTPOST_IPYTHON_SOCKET'])

        super(OutpostShell, self).__init__(*args, **kwargs)

    def init_create_namespaces(self, user_module, user_ns):
        g = RemoteNamespace(self.__client.get_globals())
        m = RemoteModule(g)
        super(OutpostShell, self).init_create_namespaces(m, g)

    def compile_remote(self, source, filename, mode, flags=0,
            dont_inherit=False, optimize=-1):
        rcode = self.__client.compile(source, filename, mode,
                flags=flags, dont_inherit=dont_inherit, optimize=optimize)
        return rcode

    def run_remote(self, rcode):
        self.__client.run_code(rcode)

    def run_ast_nodes(self, nodelist, cell_name, interactivity='last_expr',
                        compiler=compile, result=None):
        # Override compiler
        compiler = self.compile_remote

        return super(OutpostShell, self).run_ast_nodes(
                nodelist, cell_name, interactivity=interactivity,
                compiler=compiler, result=result)

    def run_code(self, code_obj, result=None):
        rcode = code_obj

        self.user_global_ns['_outpost_run_rcode'] = lambda: self.run_remote(rcode)
        def run():
            global _outpost_run_rcode
            return _outpost_run_rcode()

        try:
            return super(OutpostShell, self).run_code(run.__code__, result=result)
        finally:
            del self.user_global_ns['_outpost_run_rcode']

class OutpostKernel(IPythonKernel):
    implementation = 'outpost'
    implementation_version = '0.1.0'
    language_info = {
        'name': 'python',
        'version': sys.version.split()[0],
        'mimetype': 'text/x-python',
        'codemirror_mode': {
            'name': 'ipython',
            'version': sys.version_info[0]
        },
        'pygments_lexer': 'ipython3',
        'nbconvert_exporter': 'python',
        'file_extension': '.py'
    }

    banner = 'test banner'

    shell_class = traitlets.Type(OutpostShell)
