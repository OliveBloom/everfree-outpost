from collections import defaultdict
import sys

import numpy
import scipy.optimize
from PIL import Image


def extract_alpha1(c1, c2, cr):
    if c1 == c2:
        return None
    return (cr - c2) / (c1 - c2)

def extract_alpha(c1, c2, cr):
    '''"Inverse alpha blending": find alpha such that blending `c1` over `c2`
    with opacity `alpha` produces `cr`.'''
    a = tuple(extract_alpha1(c1[i], c2[i], cr[i]) for i in range(3))
    af = tuple(x for x in a if x is not None)
    if len(af) == 0:
        return 0
    return sum(af) / len(af)


N_COEFFS = 4

def alpha_func(coeffs, color):
    '''Compute the alpha value for a given color.  We optimize `coeffs` to make
    `alpha_func(coeffs, base_color)` accurately reflect the alpha needed to
    produce the shaded version of `base_color` present in the example cube.'''
    r, g, b = color
    r /= 255
    g /= 255
    b /= 255
    xs = numpy.array([r, g, b, 1], numpy.float32)
    return numpy.dot(coeffs, xs)

def error_func(coeffs, color, target):
    alpha = alpha_func(coeffs, color)
    return alpha - target

def error_func_vector(coeffs, examples):
    return numpy.array(
            [error_func(coeffs, color, target) * 1000 for color, target in examples],
            numpy.float32)

def optimize(examples):
    result = scipy.optimize.least_squares(
            lambda coeffs: error_func_vector(coeffs, examples),
            numpy.array([0] * N_COEFFS, numpy.float32),
            bounds=(-3, 3),
            )
    return result


# Load shaded examples from an image.

cube_img_path, = sys.argv[1:]
cube_img = Image.open(cube_img_path)

sample_positions = []

for i in range(27):
    r = i // 9
    g = i // 3 % 3
    b = i % 3

    x = 5 + 7 * r + 28 * b
    y = 17 + 9 * g

    sample_positions.append((x, y))

for i in range(23):
    row = i // 8
    col = i % 8

    x = 5 + 7 * col
    y = 58 + 9 * row

    sample_positions.append((x, y))


examples = defaultdict(list)
for (x, y) in sample_positions:
    base_color = cube_img.getpixel((x + 2, y + 2))[:3]

    def store_example(name, ox, oy, c2):
        shaded = cube_img.getpixel((x + ox, y + oy))[:3]
        alpha = extract_alpha(base_color, c2, shaded)
        examples[name].append((base_color, alpha))

    store_example('highlight', 1, 1, (255, 255, 255))
    store_example('shadow', 1, 5, (0, 0, 0))
    store_example('border', 1, 0, (0, 0, 0))
    store_example('border_light', 0, 1, (0, 0, 0))


for name, examples in examples.items():
    result = optimize(examples)
    coeffs = result.x

    if False:    # debug
        for (r, g, b), target in examples:
            alpha = alpha_func(coeffs, (r, g, b))
            sys.stderr.write('%d %d %d: alpha = %.3f, target = %.3f (error: %.3f = %d)\n' %
                    (r, g, b, alpha, target,
                        abs(alpha - target), round(abs(alpha - target) * 255)))
    sys.stderr.write('%s: total error: %g\n' % (name, result.cost))
    print('const vec4 SHADING_COEFFS_%s = vec4(%g, %g, %g, %g);' %
            ((name.upper(),) + tuple(coeffs)))
