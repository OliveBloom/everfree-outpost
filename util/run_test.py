'''Run a test, copying its stdout to both the terminal and a log file, and
touching a stamp file if the test passes.'''
import sys
import subprocess
import time

log_path = sys.argv[1]
stamp_path = sys.argv[2]
cmd = sys.argv[3:]

p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
output = p.stdout.read().decode('utf-8')
p.wait()

with open(log_path, 'w') as f:
    f.write(output)
sys.stderr.write(output)

if p.returncode == 0:
    with open(stamp_path, 'w') as f:
        f.write('test %r passed at %s\n' % (' '.join(cmd), time.asctime()))
