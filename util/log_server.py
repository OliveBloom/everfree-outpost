import os
import time

import tornado.ioloop
import tornado.web
import tornado.websocket


LOG_CONFIG = os.environ.get('RUST_LOG')
os.makedirs('logs', exist_ok=True)


CONNS = []

class WSHandler(tornado.websocket.WebSocketHandler):
    def __init__(self, *args, **kwargs):
        super(WSHandler, self).__init__(*args, **kwargs)

        idx = None
        for i in range(len(CONNS)):
            if CONNS[i] is None:
                idx = i
                break
        else:
            idx = len(CONNS)
            CONNS.append(None)

        CONNS[idx] = self
        self.conn_id = idx

        self.file = None

        timestamp = time.strftime('%Y%m%d-%H%M%S')
        self.file = open('logs/client-%s.log' % timestamp, 'w')

    def check_origin(self, origin):
        return True

    def open(self):
        self._output(' == connected ==')
        if LOG_CONFIG is not None:
            self.write_message(LOG_CONFIG)

    def on_message(self, message):
        self._output(message)

    def on_close(self):
        self._output(' == disconnected ==')
        CONNS[self.conn_id] = None

    def _output(self, msg):
        line = '%s%d:%s' % ('\t' * self.conn_id, self.conn_id, msg)
        print(line)
        self.file.write(line + '\n')

application = tornado.web.Application([
    (r'/log', WSHandler),
], debug=True)

if __name__ == "__main__":
    PORT = int(os.environ.get('OUTPOST_LOG_SERVER_PORT', 8892))
    application.listen(PORT)
    tornado.ioloop.IOLoop.instance().start()
