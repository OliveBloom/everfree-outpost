with builtins;
let
    branch = "release-19.03";
    date = "2019-03-18";
    rev = "0dd99c0fa0cf8416dec9c10b7abb9b1c943a6a63";
    sha256 = "1a6wd3cfv46c8dhyayppwaich7yfvvxcg9ncm6d1sbv6kmfp5av4";

    src = fetchTarball {
        name = "nixpkgs-${branch}-${date}-${rev}";
        url = "https://github.com/nixos/nixpkgs/archive/${rev}.tar.gz";
        inherit sha256;
    };

in import src
