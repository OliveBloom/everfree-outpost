from outpost.core.data import DATA
from outpost.lib import permission
from outpost.lib.util import handle

def make_on_create(inv_size):
    def on_create(s, e):
        i = s.create_child_inventory(inv_size)
        i.set_public(True)
        s.set_contents(i)

    return on_create

def register(name):
    struct = DATA.template(name)

    @handle(struct.handlers, 'can_interact')
    def container_can_interact(s, e):
        bounds = (s.pos(), s.pos() + struct.size)
        if not permission.check_region(e, bounds, 'container_open'):
            return False

        return True

    @handle(struct.handlers, 'on_interact')
    def container_on_interact(s, e):
        e.controller().open_container_dialog(e.main_inv(), s.contents(), s)


