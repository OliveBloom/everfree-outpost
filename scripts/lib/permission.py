from _outpost_types import *
from outpost.core import logic


def check_region(e, region, action):
    blocker = get_blocker(e, region, action)
    if blocker is not None:
        e.log_msg('This area belongs to %s.' % blocker)
        return False
    else:
        return True

def get_blocker(e, region, action):
    c = e.controller()
    region = region.reduce()
    blocker = logic.permission.get_blocker(
            c.stable_id(), c.name(), e.plane_id(), region)
    if blocker is not None and c.is_superuser():
        e.log_msg('Bypassing ward owned by %s.' % blocker)
        return None
    return blocker
