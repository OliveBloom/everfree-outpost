import _outpost_data as _DATA

from outpost.core import data_handlers


def _thing(proxy, by_name):
    def f(self, x):
        if isinstance(x, proxy):
            return x
        elif isinstance(x, str):
            id = by_name(x)
            return proxy.by_id(id)
        elif isinstance(x, int):
            return proxy.by_id(x)
        else:
            raise TypeError('expected %s, int, or str' % proxy.__name__)
    return f

def _get_thing(proxy, get_by_name):
    def f(self, x):
        if isinstance(x, proxy):
            return x
        elif isinstance(x, str):
            id = get_by_name(x)
            return proxy.by_id(id) if id is not None else None
        elif isinstance(x, int):
            if 0 <= x < len(proxy.INSTANCES):
                return proxy.by_id(x)
            else:
                return None
        elif x is None:
            return None
        else:
            raise TypeError('expected %s, int, or str' % proxy.__name__)
    return f

def _thing_id(proxy, by_name):
    def f(self, x):
        if isinstance(x, proxy):
            return x.id
        elif isinstance(x, str):
            return by_name(x)
        elif isinstance(x, int):
            if x < 0 or x >= len(proxy.INSTANCES):
                raise IndexError(x)
            return x
        else:
            raise TypeError('expected %s, int, or str' % proxy.__name__)
    return f

def _num_things(proxy):
    def f(self):
        return len(proxy.INSTANCES)
    return f

def _define_methods(cls, proxy, thing, things=None):
    things = things or thing + 's'
    by_name = getattr(_DATA, '%s_by_name' % thing)
    get_by_name = getattr(_DATA, 'get_%s_by_name' % thing)

    setattr(cls, '%s' % thing, _thing(proxy, by_name))
    setattr(cls, 'get_%s' % thing, _get_thing(proxy, get_by_name))
    setattr(cls, 'num_%s' % things, _num_things(proxy))
    setattr(cls, '%s_id' % thing, _thing_id(proxy, by_name))

class DataProxy:
    @classmethod
    def _init(cls):
        _define_methods(cls, BlockProxy, 'block')
        _define_methods(cls, ItemProxy, 'item')
        _define_methods(cls, RecipeProxy, 'recipe')
        _define_methods(cls, TemplateProxy, 'template')
        _define_methods(cls, CraftingClassProxy, 'crafting_class')

DATA = DataProxy()


class DefProxy:
    def __init__(self, id):
        self.id = id
        if hasattr(type(self), 'HANDLERS_CLASS'):
            self.handlers = type(self).HANDLERS_CLASS()

    INSTANCES = []

    @classmethod
    def by_id(cls, id):
        if cls.INSTANCES[id] is None:
            cls.INSTANCES[id] = cls(id)
        return cls.INSTANCES[id]

    def __hash__(self):
        return hash(self.id)

    def __repr__(self):
        try:
            # `self.name` isn't guaranteed to be present, but we'd like to use
            # it if it is.
            return '<%s #%d %r>' % (type(self).__name__, self.id, self.name)
        except Exception:
            return '<%s #%d>' % (type(self).__name__, self.id)

class BlockProxy(DefProxy):
    @property
    def name(self):
        return _DATA.block_name(self.id)

    @property
    def shape(self):
        return _DATA.block_shape(self.id)

class ItemProxy(DefProxy):
    HANDLERS_CLASS = data_handlers.ItemHandlers

    @property
    def name(self):
        return _DATA.item_name(self.id)

class RecipeProxy(DefProxy):
    @property
    def name(self):
        return _DATA.recipe_name(self.id)

    @property
    def station(self):
        id = _DATA.recipe_station(self.id)
        return TemplateProxy.by_id(id)

    @property
    def inputs(self):
        lst = _DATA.recipe_inputs(self.id)
        return {ItemProxy.by_id(k): v for k, v in lst}

    @property
    def outputs(self):
        lst = _DATA.recipe_outputs(self.id)
        return {ItemProxy.by_id(k): v for k, v in lst}

class TemplateProxy(DefProxy):
    HANDLERS_CLASS = data_handlers.TemplateHandlers

    @property
    def name(self):
        return _DATA.template_name(self.id)

    @property
    def layer(self):
        return _DATA.template_layer(self.id)

    @property
    def size(self):
        return _DATA.template_size(self.id)

class CraftingClassProxy(DefProxy):
    @property
    def name(self):
        return _DATA.crafting_class_name(self.id)


def _init():
    BlockProxy.INSTANCES = [None] * _DATA.block_count()
    ItemProxy.INSTANCES = [None] * _DATA.item_count()
    RecipeProxy.INSTANCES = [None] * _DATA.recipe_count()
    TemplateProxy.INSTANCES = [None] * _DATA.template_count()
    CraftingClassProxy.INSTANCES = [None] * _DATA.crafting_class_count()

    DataProxy._init()

_init()
