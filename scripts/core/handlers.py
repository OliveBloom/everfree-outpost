import _outpost_engine
from outpost.core.data import DATA
from outpost.core import chat, engine

import json
import os


def on_engine_init():
    print('engine init at time %d '% engine.now())
    return ()

def on_client_join(cid):
    client = engine.Client(cid)
    if os.path.isfile('motd.txt'):
        with open('motd.txt') as f:
            motd = f.read()
        for line in chat.reflow(motd):
            client.log_msg(line)
            print(line)
    return ()

def on_chat_command(cid, cmd, args):
    client = engine.Client(cid)
    chat.handle_command(client, cmd, args)
    return ()

def on_action_interact(cid, sid):
    c = engine.Client(cid)
    e = c.pawn()
    if e is None:
        return

    s = engine.Structure(sid)
    template = s.template()
    if template.handlers.can_interact(s, e):
        print('%s: interact %s %s' % (cid, s.template(), s))
        template.handlers.on_interact(s, e)
    else:
        print('%s: attempted interact of %s %s denied' % (cid, s.template(), s))
    return ()

def on_action_destroy(cid, sid):
    c = engine.Client(cid)
    e = c.pawn()
    if e is None:
        return

    s = engine.Structure(sid)
    template = s.template()
    if template.handlers.can_destroy(s, e):
        print('%s: destroy %s %s' % (cid, s.template(), s))
        template.handlers.on_destroy(s, e)
    else:
        print('%s: attempted destroy of %s %s denied' % (cid, s.template(), s))
    return ()

def on_action_use_item(cid, pos, item_id):
    c = engine.Client(cid)
    e = c.pawn()
    if e is None:
        return

    item = DATA.item(item_id)
    if item.handlers.can_use(e, pos):
        print('%s: use %s at %s' % (cid, item, pos))
        item.handlers.on_use(e, pos)
    else:
        print('%s: attempted use of %s at %s denied' % (cid, item, pos))
    return ()

def on_inventory_change(iid):
    return ()


IPYTHON_SESSION = None

def on_ipython_request(req):
    global IPYTHON_SESSION
    if IPYTHON_SESSION is None:
        from outpost.core.ipython import Session
        g = {}
        def imp_from(src, what):
            exec('from %s import %s' % (src, what), None, g)
        imp_from('_outpost_types', '*')
        imp_from('outpost.core', 'engine')
        imp_from('outpost.core.engine', 'Client, Entity, Inventory, Plane, Structure')
        imp_from('outpost.core.data', 'DATA')
        print(g)
        IPYTHON_SESSION = Session(g)

    req_j = json.loads(req)
    resp_j = IPYTHON_SESSION.handle(req_j)
    return json.dumps(resp_j) + '\n'
