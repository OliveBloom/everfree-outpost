import base64
import pickle
import sys
import traceback


class Session:
    def __init__(self, g=None):
        self.pins = {}
        self.pin_keys = {}
        # `refcounts` is indexed by the `pin` key, not `id(obj)`
        self.refcounts = {}
        self.pin_counter = 0

        self.globals = {
                '__builtins__': __builtins__,
                'print': self.print,
                '__name__': '__main__',
                }
        if g is not None:
            self.globals.update(g)

        self.locals = self.globals

        self.print_buf = []


    # Entry points

    def handle(self, j):
        try:
            old_displayhook = sys.displayhook
            sys.displayhook = self.print

            x = self._handle(j)
            return {
                    'ok': True,
                    'value': x,
                    'print': self.take_print_buf(),
                    }

        except Exception as exc:
            return {
                    'ok': False,
                    'exc_type': type(exc).__name__,
                    'exc_msg': str(exc),
                    'backtrace': traceback.format_exc(),
                    'print': self.take_print_buf(),
                    }

        finally:
            sys.displayhook = old_displayhook

    def _handle(self, j):
        cmd = j['cmd']
        if cmd == 'get_globals':
            return self.get_globals()
        elif cmd == 'get_locals':
            return self.get_locals()
        elif cmd == 'unpin':
            return self.unpin(j['pin'])
        elif cmd == 'compile':
            source = pickle.loads(base64.b64decode(j['source'].encode('ascii')))
            return self.compile(
                    source,
                    j['filename'],
                    j['mode'],
                    j.get('flags', 0),
                    j.get('dont_inherit', False),
                    j.get('optimize', -1),
                    )
        elif cmd == 'run_code':
            return self.run_code(j['code'])
        elif cmd == 'obj_dir':
            return self.obj_dir(j['obj'])
        elif cmd == 'obj_getattr':
            return self.obj_getattr(j['obj'], j['k'])
        elif cmd == 'obj_keys':
            return self.obj_keys(j['obj'])
        elif cmd == 'obj_getitem':
            return self.obj_getitem(j['obj'], j['k'])
        elif cmd == 'obj_str':
            return self.obj_str(j['obj'])
        elif cmd == 'obj_repr':
            return self.obj_repr(j['obj'])
        else:
            raise ValueError('unsupported command: %r' % cmd)

    def get_globals(self):
        return self._pin(self.globals)

    def get_locals(self):
        return self._pin(self.locals)

    def unpin(self, pin):
        self._unpin(pin)

    def compile(self, source, filename, mode,
            flags=0, dont_inherit=False, optimize=-1):
        code = compile(source, filename, mode,
                flags=flags, dont_inherit=dont_inherit, optimize=optimize)
        return self._pin(code)

    def run_code(self, code_pin):
        code = self.pins[code_pin]
        exec(code, self.globals, self.locals)

    def obj_dir(self, obj_pin):
        obj = self.pins[obj_pin]
        return list(dir(obj))

    def obj_getattr(self, obj_pin, k):
        obj = self.pins[obj_pin]
        return self._pin(getattr(obj, k))

    def obj_keys(self, obj_pin):
        obj = self.pins[obj_pin]
        return list(obj.keys())

    def obj_getitem(self, obj_pin, k):
        obj = self.pins[obj_pin]
        return self._pin(obj[k])

    def obj_str(self, obj_pin):
        obj = self.pins[obj_pin]
        return str(obj)

    def obj_repr(self, obj_pin):
        obj = self.pins[obj_pin]
        return repr(obj)


    # Helper functions

    def take_print_buf(self):
        pb = self.print_buf
        self.print_buf = []
        return pb

    def _pin(self, obj):
        if id(obj) in self.pin_keys:
            pin = self.pin_keys[id(obj)]
            self.refcounts[pin] += 1
            return pin

        key = self.pin_counter
        self.pin_counter += 1
        self.pins[key] = obj
        self.refcounts[key] = 1
        self.pin_keys[id(obj)] = key
        return key

    def _unpin(self, pin):
        self.refcounts[pin] -= 1
        if self.refcounts[pin] == 0:
            obj = self.pins.pop(pin)
            del self.pin_keys[id(obj)]
            del self.refcounts[pin]

    def print(self, *objs, sep=' ', end='\n', file=None, flush=False):
        if file is not None:
            print(*objs, sep=sep, end=end, file=file, flush=flush)

        if sep is None:
            sep = ' '
        if end is None:
            end = '\n'
        self.print_buf.append(sep.join(str(o) for o in objs) + end)
