import _outpost_engine as ENG
from _outpost_types import *
from outpost.core.data import DATA, ItemProxy

def can_place(template, p, pos):
    return ENG.logic_structure_can_place(p.id, pos, DATA.template(template).id)

def can_replace(template, s):
    return ENG.logic_structure_can_replace(s.id, DATA.template(template).id)
