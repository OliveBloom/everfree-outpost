import _outpost_engine as ENG
from _outpost_types import *


def get_blocker(actor, actor_name, plane, region):
    return ENG.logic_permission_get_blocker(actor, actor_name, plane, region)
