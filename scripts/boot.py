"""Outpost server initialization script.

On server startup, this script runs (as `__main__`) to set up search paths,
custom importers, etc., so that the server binary will be able find the
`outpost.core.handlers` module.
"""

import sys

# Set up stderr `print` immediately so we can use it for debugging
old_print = __builtins__.print
def err_print(*args, file=None, flush=True, **kwargs):
    old_print(*args, file=file or sys.stderr, flush=flush, **kwargs)
__builtins__.print = err_print


import os

class FakePackage(object):
    def __init__(self, name, path):
        self.__name__ = name
        self.__package__ = name
        self.__path__ = [path]

if __name__ == '__main__':
    script_dir = os.path.dirname(__file__)
    sys.modules['outpost'] = FakePackage('outpost', script_dir)

    # Import game.py for its side effects
    try:
        # In test cases, `boot.py` is invoked by `preboot.py`, which passes in
        # a custom `game.py` path in a global variable.
        path = GAME_PY_PATH
    except NameError:
        path = os.path.join(script_dir, 'game.py')

    with open(path, 'r') as f:
        code = compile(f.read(), path, 'exec')
    dct = {'__builtins__': __builtins__}
    exec(code, dct, dct)
