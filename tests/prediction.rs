#![feature(box_syntax, iterator_step_by)]
extern crate env_logger;
#[macro_use] extern crate log;

extern crate client;
extern crate client_data;
extern crate common;
extern crate server_config;
extern crate test_paths;

use common::types::*;
use std::cell::Cell;
use std::cmp;
use std::fs::File;
use std::io::Read;
use std::iter;
use std::sync::Once;

use client::client::rle16_decode;
use client::predictor::Predictor;
use client::state::{State, Delta};
use client::test::TerrainShape;
use client_data::Data as ClientData;
use common::Activity;
use common::types::*;
use common::proto::game::Request;
use server_config::Data as ServerData;

fn client_data() -> ClientData {
    let mut buf = Vec::new();
    File::open(test_paths::FULL_CLIENT_DATA).unwrap()
        .read_to_end(&mut buf).unwrap();
    ClientData::new(buf.into_boxed_slice())
}

fn server_data() -> ServerData {
    let mut buf = Vec::new();
    File::open(test_paths::FULL_SERVER_DATA).unwrap()
        .read_to_end(&mut buf).unwrap();
    ServerData::new(buf.into_boxed_slice())
}

fn init_cdata_atomic() {
    static INIT: Once = Once::new();
    INIT.call_once(|| {
        let cdata = client_data();
        client::data::set_data(Box::new(cdata));
    });
}

const E_PAWN: EntityId = EntityId(0);

fn init_state() -> (State, Box<TerrainShape>, ServerData) {
    let sdata = server_data();
    init_cdata_atomic();
    let mut s = State::new();
    let mut ts = box TerrainShape::new();

    let b_empty = sdata.block_id("empty");
    let b_grass = sdata.block_id("grass");

    s.apply_immediate(Delta::EntityAppear(E_PAWN,
                                          Default::default(),
                                          Box::new("Pony".to_owned())));
    s.apply_immediate(Delta::SetPawn(Some(E_PAWN)));
    let act = Box::new(Activity::Stand {
        pos: scalar(0),
        dir: 0,
    });
    s.apply_immediate(Delta::EntityActivitySet(E_PAWN, act, 0));

    let layer_size = CHUNK_SIZE * CHUNK_SIZE;
    let layer_count = CHUNK_SIZE;
    let blocks = Box::new([
        0xf000 | layer_size as u16, b_grass,
        0xf000 | ((layer_count - 1) * layer_size) as u16, b_empty,
    ]);
    for cpos in Region::<V2>::sized(LOCAL_SIZE).points() {
        s.apply_immediate(Delta::TerrainChunkBlocks(
                (cpos.x as u8, cpos.y as u8), blocks.clone()));

        ts.set_terrain(cpos, |pos| if pos.z == 0 { b_grass } else { b_empty });
    }

    s.apply_immediate(Delta::SetNow(0));

    (s, ts, sdata)
}

fn advance(s: &mut State, p: &mut Predictor, terrain: &TerrainShape,
           now: Time, platform_now: Time) {
    s.advance(now);
    p.advance(now);
    p.predict(platform_now, s, terrain);
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
enum TimeMode {
    State,
    Platform,
}

fn check_skew(s: &mut State, p: &mut Predictor, terrain: &TerrainShape,
              end_time: Time, time_base: Time, time_mode: TimeMode,
              x_breaks: &[(Time, i32)], ok: &Cell<bool>) {
    fn get_value(time: Time, breaks: &[(Time, i32)]) -> i32 {
        if breaks.len() == 0 {
            return 0;
        }

        if time <= breaks[0].0 {
            return breaks[0].1;
        }
        if time >= breaks.last().unwrap().0 {
            return breaks.last().unwrap().1;
        }

        for (prev, next) in breaks.iter().zip(breaks.iter().skip(1)) {
            if time >= prev.0 && time < next.0 {
                let delta = time - prev.0;
                let offset = (next.1 - prev.1) as i64 * delta / (next.0 - prev.0);
                return prev.1 + offset as i32;
            }
        }

        unreachable!()
    }

    s.unpredict();
    let start_time =
        if time_mode == TimeMode::State { s.now() }
        else { s.now() + time_base };
    for t in (start_time + 50 .. end_time).step_by(50) {
        let (state_t, platform_t) =
            if time_mode == TimeMode::State { (t, t + time_base) }
            else { (t - time_base, t) };
        advance(s, p, terrain, state_t, platform_t);

        let expect_x = get_value(t, x_breaks);
        let actual_x = s.entities()[E_PAWN].pos(state_t).x;
        if (actual_x - expect_x).abs() <= 1 {
            println!("{} ({}): {}", state_t, platform_t, actual_x);
        } else {
            println!("{} ({}): {} != {}", state_t, platform_t, actual_x, expect_x);
            ok.set(false);
        }
    }
}

fn check(s: &mut State, p: &mut Predictor, terrain: &TerrainShape,
         end_time: Time, x_breaks: &[(Time, i32)], ok: &Cell<bool>) {
    check_skew(s, p, terrain, end_time, 0, TimeMode::State, x_breaks, ok);
}

fn check_2d_skew(s: &mut State, p: &mut Predictor, terrain: &TerrainShape,
                end_time: Time, time_base: Time, time_mode: TimeMode,
                breaks: &[(Time, i32, i32)], ok: &Cell<bool>) {
    fn get_value(time: Time, breaks: &[(Time, i32, i32)]) -> V2 {
        if breaks.len() == 0 {
            return scalar(0);
        }

        if time <= breaks[0].0 {
            let b = breaks[0];
            return V2::new(b.1, b.2);
        }
        if time >= breaks.last().unwrap().0 {
            let b = breaks.last().unwrap();
            return V2::new(b.1, b.2);
        }

        for (prev, next) in breaks.iter().zip(breaks.iter().skip(1)) {
            if time >= prev.0 && time < next.0 {
                let delta = time - prev.0;
                let next_pos = V2::new(next.1, next.2);
                let prev_pos = V2::new(prev.1, prev.2);
                let offset = (next_pos - prev_pos)
                    * delta as i32 / (next.0 - prev.0) as i32;
                return prev_pos + offset;
            }
        }

        unreachable!()
    }

    s.unpredict();
    let start_time =
        if time_mode == TimeMode::State { s.now() }
        else { s.now() + time_base };
    for t in (start_time + 50 .. end_time).step_by(50) {
        let (state_t, platform_t) =
            if time_mode == TimeMode::State { (t, t + time_base) }
            else { (t - time_base, t) };
        advance(s, p, terrain, state_t, platform_t);

        let expect = get_value(t, breaks);
        let actual = s.entities()[E_PAWN].pos(t).reduce();
        if (actual - expect).abs().max() <= 1 {
            println!("{} ({}): {:?}", state_t, platform_t, actual);
        } else {
            println!("{} ({}): {:?} != {:?}", state_t, platform_t, actual, expect);
            ok.set(false);
        }
    }
}

fn check_2d(s: &mut State, p: &mut Predictor, terrain: &TerrainShape,
            end_time: Time, breaks: &[(Time, i32, i32)], ok: &Cell<bool>) {
    check_2d_skew(s, p, terrain, end_time, 0, TimeMode::State, breaks, ok);
}

#[test]
fn basic_movement() {
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    s.entity_act_walk(E_PAWN, 1000, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1000);

    s.entity_act_stand(E_PAWN, 2000, V3::new(50, 0, 0), 0);
    s.tick_end(2000);

    let ok = Cell::new(true);
    check(&mut s, &mut p, &terrain, 3000, &[(1000, 0), (2000, 50)], &ok);
    assert!(ok.get());
}

#[test]
fn no_lag() {
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0), (2000, 50)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check(s, p, &terrain, end, breaks, &ok);
    };

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(0, 0, 1));
    p.ack_request(1000);
    s.entity_act_walk(E_PAWN, 1000, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1000);

    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 0, 0));
    p.ack_request(2000);
    s.entity_act_stand(E_PAWN, 2000, V3::new(50, 0, 0), 0);
    s.tick_end(2000);

    run(&mut s, &mut p, 3000);

    assert!(ok.get());
}

#[test]
fn fixed_lag_no_start_delay() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0), (2000, 50)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check(s, p, &terrain, end, breaks, &ok);
    };

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(0, 0, 1));

    run(&mut s, &mut p, 1100);
    p.ack_request(1100);
    s.entity_act_walk(E_PAWN, 1100, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1100);

    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 0, 0));

    run(&mut s, &mut p, 2100);
    p.ack_request(2100);
    s.entity_act_stand(E_PAWN, 2100, V3::new(50, 0, 0), 0);
    s.tick_end(2100);

    run(&mut s, &mut p, 3000);

    assert!(ok.get());
}

#[test]
fn fixed_lag_with_start_delay() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0), (2000, 50)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check(s, p, &terrain, end, breaks, &ok);
    };

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(200, 0, 1));

    run(&mut s, &mut p, 1300);
    p.ack_request(1500);

    run(&mut s, &mut p, 1500);
    s.entity_act_walk(E_PAWN, 1500, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1500);


    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 0, 0));

    run(&mut s, &mut p, 2300);
    p.ack_request(2500);

    run(&mut s, &mut p, 2500);
    s.entity_act_stand(E_PAWN, 2500, V3::new(50, 0, 0), 0);
    s.tick_end(2500);

    run(&mut s, &mut p, 3000);

    assert!(ok.get());
}

#[test]
fn lag_spike_start() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0), (2000, 50)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check(s, p, &terrain, end, breaks, &ok);
    };

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(200, 0, 1));

    run(&mut s, &mut p, 1400);
    p.ack_request(1600);

    run(&mut s, &mut p, 1600);
    s.entity_act_walk(E_PAWN, 1600, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1600);


    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 0, 0));

    run(&mut s, &mut p, 2300);
    p.ack_request(2600);

    run(&mut s, &mut p, 2600);
    s.entity_act_stand(E_PAWN, 2600, V3::new(50, 0, 0), 0);
    s.tick_end(2600);

    run(&mut s, &mut p, 3000);

    assert!(ok.get());
}

#[test]
fn lag_spike_end() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0), (2000, 50)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check(s, p, &terrain, end, breaks, &ok);
    };

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(200, 0, 1));

    run(&mut s, &mut p, 1300);
    p.ack_request(1500);

    run(&mut s, &mut p, 1500);
    s.entity_act_walk(E_PAWN, 1500, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1500);


    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 0, 0));

    run(&mut s, &mut p, 2400);
    p.ack_request(2500);

    run(&mut s, &mut p, 2500);
    s.entity_act_stand(E_PAWN, 2500, V3::new(50, 0, 0), 0);
    s.tick_end(2500);

    run(&mut s, &mut p, 3000);

    assert!(ok.get());
}

/// Check behavior when the lag spike is so long it delays application of the InputChange.
#[test]
fn big_lag_spike() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    // Simulation proceeds as normal.  Once the delay in handling InputChange becomes evident to
    // the client, the character starts sliding forward.
    let breaks = &[(1000, 0), (2000, 50), (2200, 50), (2700, 75)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check(s, p, &terrain, end, breaks, &ok);
    };

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(100, 0, 1));

    // InputStart is processed at 1100, and effects are scheduled for 1200 (1100 + 100 delay)
    run(&mut s, &mut p, 1100);
    p.ack_request(1200);

    run(&mut s, &mut p, 1200);
    s.entity_act_walk(E_PAWN, 1200, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1200);


    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 0, 0));

    // InputChange is processed at 2700.  Effects would be scheduled for 2200 (1200 + 1000
    // rel_time), but that's in the past.  Effects are scheduled for 2700 instead.
    run(&mut s, &mut p, 2700);
    p.ack_request(2700);
    s.entity_act_stand(E_PAWN, 2700, V3::new(75, 0, 0), 0);
    s.tick_end(2700);

    run(&mut s, &mut p, 3000);

    assert!(ok.get());
}

#[test]
fn fixed_lag_direction_change() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0, 0), (2000, 50, 0), (3000, 100, 50), (4000, 100, 100)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check_2d(s, p, &terrain, end, breaks, &ok);
    };


    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(0, 0, 1));

    run(&mut s, &mut p, 1100);
    p.ack_request(1100);
    s.entity_act_walk(E_PAWN, 1100, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1100);


    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 1, 1));

    run(&mut s, &mut p, 2100);
    p.ack_request(2100);
    s.entity_act_walk(E_PAWN, 2100, V3::new(50, 0, 0), V3::new(50, 50, 0), 1);
    s.tick_end(2100);


    run(&mut s, &mut p, 3000);
    p.request_sent(3000, &Request::InputChange(1000, 2, 1));

    run(&mut s, &mut p, 3100);
    p.ack_request(3100);
    s.entity_act_walk(E_PAWN, 3100, V3::new(100, 50, 0), V3::new(0, 50, 0), 2);
    s.tick_end(3100);


    run(&mut s, &mut p, 4000);
    p.request_sent(4000, &Request::InputChange(1000, 0, 0));

    run(&mut s, &mut p, 4100);
    p.ack_request(4100);
    s.entity_act_walk(E_PAWN, 4100, V3::new(100, 100, 0), V3::new(0, 0, 0), 2);
    s.tick_end(4100);

    assert!(ok.get());
}

#[test]
fn overlap_in_flight_stop() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0), (1100, 5)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check(s, p, &terrain, end, breaks, &ok);
    };

    // Fixed latency of 500 ticks

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(0, 0, 1));

    run(&mut s, &mut p, 1100);
    p.request_sent(1100, &Request::InputChange(100, 0, 0));

    run(&mut s, &mut p, 1500);
    p.ack_request(1500);
    s.entity_act_walk(E_PAWN, 1500, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1500);

    run(&mut s, &mut p, 1600);
    p.ack_request(1600);
    s.entity_act_walk(E_PAWN, 1600, V3::new(5, 0, 0), V3::new(0, 0, 0), 0);
    s.tick_end(1600);

    run(&mut s, &mut p, 2000);

    assert!(ok.get());
}

#[test]
fn overlap_in_flight_direction_change() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0, 0), (1400, 20, 0), (1800, 20, 20)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check_2d(s, p, &terrain, end, breaks, &ok);
    };

    // Fixed latency of 500 ticks

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(0, 0, 1));

    run(&mut s, &mut p, 1400);
    p.request_sent(1400, &Request::InputChange(400, 2, 1));

    run(&mut s, &mut p, 1500);
    p.ack_request(1500);
    s.entity_act_walk(E_PAWN, 1500, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1500);

    run(&mut s, &mut p, 1800);
    p.request_sent(1800, &Request::InputChange(400, 0, 0));

    run(&mut s, &mut p, 1900);
    p.ack_request(1900);
    s.entity_act_walk(E_PAWN, 1900, V3::new(20, 0, 0), V3::new(0, 50, 0), 2);
    s.tick_end(1900);

    run(&mut s, &mut p, 2300);
    p.ack_request(2300);
    s.entity_act_walk(E_PAWN, 2300, V3::new(20, 20, 0), V3::new(0, 0, 0), 0);
    s.tick_end(2300);

    run(&mut s, &mut p, 3000);

    assert!(ok.get());
}

#[test]
fn zig_zag_no_lag() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0, 0), (2000, 50, 0), (2100, 55, 5), (3000, 100, 5)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check_2d(s, p, &terrain, end, breaks, &ok);
    };

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(0, 0, 1));

    run(&mut s, &mut p, 1000);
    p.ack_request(1000);
    s.entity_act_walk(E_PAWN, 1000, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1000);

    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 1, 1));

    run(&mut s, &mut p, 2000);
    p.ack_request(2000);
    s.entity_act_walk(E_PAWN, 2000, V3::new(50, 0, 0), V3::new(50, 50, 0), 1);
    s.tick_end(2000);

    run(&mut s, &mut p, 2100);
    p.request_sent(2100, &Request::InputChange(100, 0, 1));

    run(&mut s, &mut p, 2100);
    p.ack_request(2100);
    s.entity_act_walk(E_PAWN, 2100, V3::new(55, 5, 0), V3::new(50, 0, 0), 0);
    s.tick_end(2100);

    run(&mut s, &mut p, 3000);
    p.request_sent(3000, &Request::InputChange(900, 0, 0));

    run(&mut s, &mut p, 3000);
    p.ack_request(3000);
    s.entity_act_walk(E_PAWN, 3000, V3::new(100, 5, 0), V3::new(0, 0, 0), 0);
    s.tick_end(3000);

    run(&mut s, &mut p, 4000);

    assert!(ok.get());
}

#[test]
fn zig_zag_no_overlap() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0, 0), (2000, 50, 0), (2100, 55, 5), (3000, 100, 5)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check_2d(s, p, &terrain, end, breaks, &ok);
    };

    // Fixed latency of 50 ticks

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(0, 0, 1));

    run(&mut s, &mut p, 1050);
    p.ack_request(1050);
    s.entity_act_walk(E_PAWN, 1050, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1050);

    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 1, 1));

    run(&mut s, &mut p, 2050);
    p.ack_request(2050);
    s.entity_act_walk(E_PAWN, 2050, V3::new(50, 0, 0), V3::new(50, 50, 0), 1);
    s.tick_end(2050);

    run(&mut s, &mut p, 2100);
    p.request_sent(2100, &Request::InputChange(100, 0, 1));

    run(&mut s, &mut p, 2150);
    p.ack_request(2150);
    s.entity_act_walk(E_PAWN, 2150, V3::new(55, 5, 0), V3::new(50, 0, 0), 0);
    s.tick_end(2150);

    run(&mut s, &mut p, 3000);
    p.request_sent(3000, &Request::InputChange(900, 0, 0));

    run(&mut s, &mut p, 3050);
    p.ack_request(3050);
    s.entity_act_walk(E_PAWN, 3050, V3::new(100, 5, 0), V3::new(0, 0, 0), 0);
    s.tick_end(3050);

    run(&mut s, &mut p, 4000);

    assert!(ok.get());
}

#[test]
fn zig_zag_with_overlap() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0, 0), (2000, 50, 0), (2100, 55, 5), (3000, 100, 5)];
    let ok = Cell::new(true);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check_2d(s, p, &terrain, end, breaks, &ok);
    };

    // Fixed latency of 250 ticks

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(0, 0, 1));

    run(&mut s, &mut p, 1250);
    p.ack_request(1250);
    s.entity_act_walk(E_PAWN, 1250, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1250);

    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 1, 1));

    run(&mut s, &mut p, 2100);
    p.request_sent(2100, &Request::InputChange(100, 0, 1));

    run(&mut s, &mut p, 2250);
    p.ack_request(2250);
    s.entity_act_walk(E_PAWN, 2250, V3::new(50, 0, 0), V3::new(50, 50, 0), 1);
    s.tick_end(2250);

    run(&mut s, &mut p, 2350);
    p.ack_request(2350);
    s.entity_act_walk(E_PAWN, 2350, V3::new(55, 5, 0), V3::new(50, 0, 0), 0);
    s.tick_end(2350);

    run(&mut s, &mut p, 3000);
    p.request_sent(3000, &Request::InputChange(900, 0, 0));

    run(&mut s, &mut p, 3250);
    p.ack_request(3250);
    s.entity_act_walk(E_PAWN, 3250, V3::new(100, 5, 0), V3::new(0, 0, 0), 0);
    s.tick_end(3250);

    run(&mut s, &mut p, 4000);

    assert!(ok.get());
}

#[test]
fn skew_test_basic() {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();

    let breaks = &[(1000, 0), (2000, 50)];
    let ok = Cell::new(true);

    let skew = Cell::new(0);

    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check_skew(s, p, &terrain, end, skew.get(), TimeMode::Platform, breaks, &ok);
    };

    // Fixed latency of 200 ticks
    
    skew.set(100);

    run(&mut s, &mut p, 1000);
    p.request_sent(1000, &Request::InputStart(100, 0, 1));

    run(&mut s, &mut p, 1200);
    p.ack_request(1300);

    run(&mut s, &mut p, 1300);
    s.entity_act_walk(E_PAWN, 1300, V3::new(0, 0, 0), V3::new(50, 0, 0), 0);
    s.tick_end(1300);


    run(&mut s, &mut p, 1500);
    skew.set(50);
    run(&mut s, &mut p, 1700);
    skew.set(0);


    run(&mut s, &mut p, 2000);
    p.request_sent(2000, &Request::InputChange(1000, 0, 0));

    run(&mut s, &mut p, 2200);
    p.ack_request(2300);

    run(&mut s, &mut p, 2300);
    s.entity_act_walk(E_PAWN, 2300, V3::new(50, 0, 0), V3::new(0, 0, 0), 0);
    s.tick_end(2300);

    run(&mut s, &mut p, 3000);

    assert!(ok.get());
}

#[derive(Clone, Debug)]
enum EventKind {
    SendReq(Request),
    AckReq(Time),
    EntityWalk(Time, V3, V3),
    TickEnd(Time),
    ShiftBase(Time),
}

impl EventKind {
    fn apply(&self, s: &mut State, p: &mut Predictor, tl: &Timeline, wall_time: Time) {
        match *self {
            EventKind::SendReq(ref req) => p.request_sent(wall_time, req),
            EventKind::AckReq(apply_time) => p.ack_request(apply_time),
            EventKind::EntityWalk(start_time, pos, vel) =>
                s.entity_act_walk(E_PAWN, start_time, pos, vel, 0),
            EventKind::TickEnd(sim_time) => s.tick_end(sim_time),
            EventKind::ShiftBase(delta) => tl.c_base.set(tl.c_base.get() + delta),
        }
    }
}

#[derive(Clone, Debug)]
struct Event {
    time: Time,
    kind: EventKind,
}

struct Timeline {
    /// Time taken to send a request to the server.  When a request is sent, its arrival time is
    /// send time + req_lag, and its processing time is arrival time - s_base.
    req_lag: Time,
    /// Time taken to receive a response from the server.  The arrival time of a response is the
    /// request's send time + req_lag + resp_lag.
    resp_lag: Time,
    /// Time base for the server.  The server's sim time is real time - s_base.
    s_base: Time,
    /// Time base for the client.  The client's sim time is real time - c_base.  Normally c_base is
    /// at least s_base + resp_lag.  In other words, the client's sim is delayed by the time it
    /// takes for updates to travel from server to client.
    c_base: Cell<Time>,

    events: Vec<Event>,

    movement_time_base: Time,
}

impl Timeline {
    fn push_input_start(&mut self,
                        time: Time,
                        delay: u16,
                        dir: u8,
                        speed: u8,
                        pos: V3,
                        vel: V3) {
        // Wall-clock timestamps of the various events.
        let send_ts = time;
        let process_ts = send_ts + self.req_lag;
        let apply_ts = process_ts + delay as Time;
        let ack_recv_ts = process_ts + self.resp_lag;
        let apply_recv_ts = apply_ts + self.resp_lag;

        self.events.push(Event {
            time: send_ts,
            kind: EventKind::SendReq(Request::InputStart(delay, dir, speed)),
        });

        self.events.push(Event {
            time: ack_recv_ts,
            kind: EventKind::AckReq(apply_ts - self.s_base),
        });

        self.events.push(Event {
            time: apply_recv_ts,
            kind: EventKind::EntityWalk(apply_ts - self.s_base, pos, vel),
        });

        self.events.push(Event {
            time: apply_recv_ts,
            kind: EventKind::TickEnd(apply_ts - self.s_base),
        });

        self.movement_time_base = apply_ts;
    }

    fn push_input_change(&mut self,
                         time: Time,
                         rel_time: u16,
                         dir: u8,
                         speed: u8,
                         pos: V3,
                         vel: V3) {
        // Wall-clock timestamps of the various events.
        let send_ts = time;
        let process_ts = send_ts + self.req_lag;
        let apply_ts = cmp::max(self.movement_time_base + rel_time as Time, process_ts);
        let ack_recv_ts = process_ts + self.resp_lag;
        let apply_recv_ts = apply_ts + self.resp_lag;

        self.events.push(Event {
            time: send_ts,
            kind: EventKind::SendReq(Request::InputChange(rel_time, dir, speed)),
        });

        self.events.push(Event {
            time: ack_recv_ts,
            kind: EventKind::AckReq(apply_ts - self.s_base),
        });

        self.events.push(Event {
            time: apply_recv_ts,
            kind: EventKind::EntityWalk(apply_ts - self.s_base, pos, vel),
        });

        self.events.push(Event {
            time: apply_recv_ts,
            kind: EventKind::TickEnd(apply_ts - self.s_base),
        });

        self.movement_time_base += rel_time as Time;
    }

    fn push_shift_base(&mut self, time: Time, delta: Time) {
        self.events.push(Event {
            time: time,
            kind: EventKind::ShiftBase(delta),
        });
    }
}


#[test]
fn timeline_basic_test() {
    timeline_base_skew_by(150, 0);
}

#[test]
fn timeline_base_skew_positive() {
    timeline_base_skew_by(150, 100);
}

#[test]
fn timeline_base_skew_negative() {
    timeline_base_skew_by(150, -100);
}

// This test fails, for good reason.  Negative skew means that the client will try to display game
// states that it hasn't actually received from the server yet.  This causes serious glitching, but
// should be prevented during real execution by the client's time_base adjustments.
#[ignore]
#[test]
fn timeline_negative_base() {
    timeline_base_skew_by(-150, 0);
}

fn timeline_base_skew_by(start_skew: Time, delta: Time) {
    let _ = env_logger::init();
    let (mut s, terrain, sdata) = init_state();
    let mut p = Predictor::new();


    let mut tl = Timeline {
        req_lag: 100,
        resp_lag: 100,
        s_base: 1000,
        c_base: Cell::new(0),
        events: Vec::new(),
        movement_time_base: 0,
    };
    tl.c_base.set(tl.s_base + tl.req_lag + start_skew);

    tl.push_input_start(2000, 100, 0, 1, V3::new(0, 0, 0), V3::new(50, 0, 0));
    tl.push_input_change(3000, 1000, 0, 0, V3::new(50, 0, 0), V3::new(0, 0, 0));
    tl.push_shift_base(2500, delta);

    tl.events.sort_by_key(|e| e.time);


    let breaks = &[(2000, 0), (3000, 50)];
    let ok = Cell::new(true);
    let run = |s: &mut State, p: &mut Predictor, end: Time| {
        check_skew(s, p, &terrain, end, tl.c_base.get(), TimeMode::Platform, breaks, &ok);
    };


    for e in &tl.events {
        run(&mut s, &mut p, e.time);
        eprintln!("{}: {:?}", e.time, e.kind);
        e.kind.apply(&mut s, &mut p, &tl, e.time);
    }

    run(&mut s, &mut p, 4000);

    assert!(ok.get());
}
