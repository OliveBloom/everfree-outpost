extern crate env_logger;
#[macro_use] extern crate log;

extern crate common;
extern crate engine;
extern crate server_bundle;
extern crate server_config;
extern crate server_types;
extern crate test_paths;
extern crate world;

use std::collections::VecDeque;
use std::fs::File;
use std::io::Read;
use std::mem;
use std::path::Path;

use server_types::*;
use engine::Engine;
use engine::chat;
use engine::component::crafting::StationId;
use engine::ext::{Ext, FullWorld};
use engine::lifecycle;
use engine::lifecycle::demand::{Source, Target};
use engine::update::{Delta, UpdateItem, flags};
use engine::update::delta::DeltaInternals;
use server_bundle::builder::Builder;
use server_config::{data, data_tls, Data};
use server_types::Stable;
use world::World;
use world::objects::*;


enum Event {
    Update(Delta),
    RequestClient(u32, String),
    RequestPlane(Stable<PlaneId>),
    RequestTerrainChunk(Stable<PlaneId>, V2),
}

struct EventTracker {
    events: Vec<Event>,
}

impl EventTracker {
    fn take_events(&mut self) -> Vec<Event> {
        mem::replace(&mut self.events, Vec::new())
    }
}

impl Ext for EventTracker {
    fn handle_update(&mut self, w: &FullWorld, u: Delta) {
        self.events.push(Event::Update(u));
    }

    fn request_client_bundle(&mut self, uid: u32, name: &str) {
        self.events.push(Event::RequestClient(uid, name.to_owned()))
    }
    fn request_plane_bundle(&mut self, stable_id: Stable<PlaneId>) {
        self.events.push(Event::RequestPlane(stable_id))
    }
    fn request_terrain_chunk_bundle(&mut self, plane: Stable<PlaneId>, cpos: V2) {
        self.events.push(Event::RequestTerrainChunk(plane, cpos))
    }

    fn client_world_info(&mut self,
                         id: ClientId,
                         now: Time,
                         day_night_cycle_ms: u32) {}
    fn client_readiness(&mut self, id: ClientId, readiness: bool) {}

    fn client_loaded(&mut self, uid: u32, cid: ClientId) {}
    fn client_unloaded(&mut self, uid: u32) {}

    fn kick_user(&mut self, uid: u32, reason: &str) {}

    fn chat_message(&mut self, channel: chat::Channel, name: &str, text: &str) {}

    fn ack_request(&mut self, cid: ClientId, apply_time: Time) {}
    fn nak_request(&mut self, cid: ClientId) {}
}

fn init_data() {
    let mut buf = Vec::new();
    File::open(test_paths::TEST_SERVER_DATA).unwrap()
        .read_to_end(&mut buf).unwrap();
    let d = Box::new(Data::new(buf.into_boxed_slice()));
    unsafe { data_tls::set_data(&d as &Data as *const Data) };
    mem::forget(d);
}

fn init_engine() -> Engine<EventTracker> {
    init_data();
    // TODO: turns out multithreaded scripting doesn't actually work yet
    //unsafe { engine::enable_multithreading() };
    let mut eng = Engine::new(Path::new(test_paths::TEST_BOOT_PY),
                              Delta::new(),
                              EventTracker { events: Vec::new() });

    eng
}


const CID: ClientId = ClientId(0);
const TCID: TerrainChunkId = TerrainChunkId(0);
const PID: PlaneId = PlaneId(0);
const P_SID: Stable<PlaneId> = STABLE_PLANE_FOREST;


#[test]
fn load_chunk() {
    let _ = env_logger::init();
    let mut eng = init_engine();


    let mut d = DeltaInternals::new();
    d.flags.terrain_chunk_mut(TCID).insert(flags::TC_CREATED);
    d.new.insert_terrain_chunk(TCID, TerrainChunk::new(PID, V2::new(0, 0)));
    d.new.insert_plane_ref(PID, P_SID);
    let chunk_delta = d.into();

    lifecycle::start_import(&mut eng, chunk_delta);
    engine::tick::on_tick(&mut eng);

    assert!(eng.ext.events.iter().any(|e| match *e {
        Event::RequestPlane(p) if p == P_SID => true,
        _ => false,
    }));

    eng.ext.events.clear();


    let mut d = DeltaInternals::new();
    d.flags.plane_mut(PID).insert(flags::P_CREATED);
    d.new.insert_plane(PID, Plane {
        stable_id: P_SID.unwrap(),
        .. Plane::new()
    });
    let plane_delta = d.into();

    lifecycle::start_import(&mut eng, plane_delta);
    engine::tick::on_tick(&mut eng);

    assert!(eng.ext.events.iter().any(|e| match *e {
        Event::Update(ref d) => d.items().any(|it| match it {
            UpdateItem::Plane(id, f) => f.contains(flags::P_CREATED),
            _ => false,
        }),
        _ => false,
    }));

    assert!(eng.ext.events.iter().any(|e| match *e {
        Event::Update(ref d) => d.items().any(|it| match it {
            UpdateItem::TerrainChunk(id, f) => f.contains(flags::TC_CREATED),
            _ => false,
        }),
        _ => false,
    }));

    eng.ext.events.clear();
}


#[test]
fn load_client() {
    let _ = env_logger::init();
    let mut eng = init_engine();


    eng.w.create_client_as(CID, Client::new(1, "Anon"));

    // Hack so that `demand` will request-load our plane properly.
    eng.demand.inc_ref(Source::ClientCamera(CID), Target::Plane(P_SID));

    // On the first tick, the attempted import of the client should trigger a plane request.
    engine::tick::on_tick(&mut eng);
    {
        let mut num_request_plane = 0;
        for evt in eng.ext.take_events() {
            match evt {
                Event::Update(_) => {},
                Event::RequestClient(_, _) => panic!("unexpected RequestClient"),
                Event::RequestPlane(psid) => {
                    assert!(psid == P_SID);
                    num_request_plane += 1;

                    let mut d = DeltaInternals::new();
                    d.flags.plane_mut(PID).insert(flags::P_CREATED);
                    d.new.insert_plane(PID, Plane {
                        stable_id: P_SID.unwrap(),
                        .. Plane::new()
                    });
                    let delta = d.into();
                    lifecycle::start_import(&mut eng, delta);
                },
                Event::RequestTerrainChunk(_, _) => panic!("unexpected RequestTerrainChunk"),
            }
        }
        assert_eq!(num_request_plane, 1);
    }

    info!(" ========== setting camera ==========");
    engine::component::camera::set(&mut eng, CID, PID, scalar(0));

    // Plane import should complete immediately, followed by client import.  The next tick should
    // request the terrain chunks surrounding the client camera.
    engine::tick::on_tick(&mut eng);
    {
        let mut num_request_chunk = 0;
        for evt in eng.ext.take_events() {
            match evt {
                Event::Update(_) => {},
                Event::RequestClient(_, _) => panic!("unexpected RequestClient"),
                Event::RequestPlane(_) => panic!("unexpected RequestPlane"),
                Event::RequestTerrainChunk(psid, cpos) => {
                    assert!(psid == P_SID);
                    assert!((Region::sized(V2::new(5, 6)) - 2).contains(cpos));
                    num_request_chunk += 1;

                    let mut d = DeltaInternals::new();
                    d.flags.terrain_chunk_mut(TCID).insert(flags::TC_CREATED);
                    d.new.insert_terrain_chunk(TCID, TerrainChunk::new(PID, cpos));
                    d.new.insert_plane_ref(PID, P_SID);
                    let delta = d.into();
                    lifecycle::start_import(&mut eng, delta);
                },
            }
        }
        assert_eq!(num_request_chunk, 5 * 6);
    }

    // Now move the camera, and check that (1) some chunks are unloaded, and (2) other chunks are
    // requested.

    info!(" ========== moving camera ==========");
    engine::component::camera::set(&mut eng, CID, PID, V3::new(0, CHUNK_SIZE * TILE_SIZE, 0));

    engine::tick::on_tick(&mut eng);
    {
        let mut num_request_chunk = 0;
        let mut num_unload_chunk = 0;
        for evt in eng.ext.take_events() {
            match evt {
                Event::Update(d) => {
                    for it in d.items() {
                        match it {
                            UpdateItem::TerrainChunk(_, f) if f.contains(flags::TC_UNLOADED) => {
                                num_unload_chunk += 1;
                            },
                            _ => {},
                        }
                    }
                },
                Event::RequestClient(_, _) => panic!("unexpected RequestClient"),
                Event::RequestPlane(_) => panic!("unexpected RequestPlane"),
                Event::RequestTerrainChunk(psid, cpos) => {
                    assert!(psid == P_SID);
                    num_request_chunk += 1;

                    let mut d = DeltaInternals::new();
                    d.flags.terrain_chunk_mut(TCID).insert(flags::TC_CREATED);
                    d.new.insert_terrain_chunk(TCID, TerrainChunk::new(PID, cpos));
                    d.new.insert_plane_ref(PID, P_SID);
                    let delta = d.into();
                    lifecycle::start_import(&mut eng, delta);
                },
            }
        }
        assert_eq!(num_request_chunk, 5);
        assert_eq!(num_unload_chunk, 5);
    }

    // Destroy the client and check that the chunks are unloaded.

    info!(" ========== destroying client ==========");
    eng.w.destroy_client(CID);
    engine::tick::on_tick(&mut eng);
    {
        let mut num_unload_chunk = 0;
        for evt in eng.ext.take_events() {
            match evt {
                Event::Update(d) => {
                    for it in d.items() {
                        match it {
                            UpdateItem::TerrainChunk(_, f) if f.contains(flags::TC_UNLOADED) => {
                                num_unload_chunk += 1;
                            },
                            _ => {},
                        }
                    }
                },
                Event::RequestClient(_, _) => panic!("unexpected RequestClient"),
                Event::RequestPlane(_) => panic!("unexpected RequestPlane"),
                Event::RequestTerrainChunk(_, _) => panic!("unexpected RequestTerrainChunk"),
            }
        }
        assert_eq!(num_unload_chunk, 5 * 6);
    }
}


/// Check that the recorded events contain one `RequestClient(1, "Anon")`.
fn expect_client_request(eng: &mut Engine<EventTracker>) {
    let mut num_request_client = 0;
    for evt in eng.ext.take_events() {
        match evt {
            Event::Update(_) => {},
            Event::RequestClient(uid, name) => {
                assert_eq!(uid, 1);
                assert_eq!(name, "Anon");
                num_request_client += 1;

                let mut d = DeltaInternals::new();
                d.flags.client_mut(CID).insert(flags::C_CREATED);
                d.new.insert_client(CID, Client::new(uid, &name));
                let delta = d.into();
                lifecycle::start_import(eng, delta);
            },
            Event::RequestPlane(_) => panic!("unexpected RequestPlane"),
            Event::RequestTerrainChunk(_, _) => panic!("unexpected RequestTerrainChunk"),
        }
    }
    assert_eq!(num_request_client, 1);
}

/// Check that the recorded events contain one `RequestPlane` with the indicated stable ID.
fn expect_plane_request(eng: &mut Engine<EventTracker>, expect_id: Stable<PlaneId>) {
    let mut num_request_plane = 0;
    for evt in eng.ext.take_events() {
        match evt {
            Event::Update(_) => {},
            Event::RequestClient(_, _) => panic!("unexpected RequestClient"),
            Event::RequestPlane(psid) => {
                assert!(psid == expect_id);
                num_request_plane += 1;

                let mut d = DeltaInternals::new();
                d.flags.plane_mut(PID).insert(flags::P_CREATED);
                d.new.insert_plane(PID, Plane {
                    stable_id: psid.unwrap(),
                    .. Plane::new()
                });
                let delta = d.into();
                lifecycle::start_import(eng, delta);
            },
            Event::RequestTerrainChunk(_, _) => panic!("unexpected RequestTerrainChunk"),
        }
    }
    assert_eq!(num_request_plane, 1);
}

/// Check that the recorded events contain `RequestTerrainChunk` events for the indicated area.
fn expect_chunk_requests(eng: &mut Engine<EventTracker>,
                         expect_plane: Stable<PlaneId>,
                         expect_region: Region<V2>,
                         expect_count: usize) {
    let mut num_request_chunk = 0;
    for evt in eng.ext.take_events() {
        match evt {
            Event::Update(_) => {},
            Event::RequestClient(_, _) => panic!("unexpected RequestClient"),
            Event::RequestPlane(_) => panic!("unexpected RequestPlane"),
            Event::RequestTerrainChunk(psid, cpos) => {
                assert!(psid == expect_plane);
                assert!(expect_region.contains(cpos));
                num_request_chunk += 1;

                let mut d = DeltaInternals::new();
                d.flags.terrain_chunk_mut(TCID).insert(flags::TC_CREATED);
                d.new.insert_terrain_chunk(TCID, TerrainChunk::new(PID, cpos));
                d.new.insert_plane_ref(PID, psid);
                let delta = d.into();
                lifecycle::start_import(eng, delta);
            },
        }
    }
    assert_eq!(num_request_chunk, expect_count);
}

fn expect_unloads(eng: &mut Engine<EventTracker>,
                  expect_clients: usize,
                  expect_planes: usize,
                  expect_chunks: usize) {
    let mut num_unload_client = 0;
    let mut num_unload_plane = 0;
    let mut num_unload_chunk = 0;
    for evt in eng.ext.take_events() {
        match evt {
            Event::Update(d) => {
                for it in d.items() {
                    match it {
                        UpdateItem::Client(_, f) if f.contains(flags::C_UNLOADED) => {
                            num_unload_client += 1;
                        },
                        UpdateItem::Plane(_, f) if f.contains(flags::P_UNLOADED) => {
                            num_unload_plane += 1;
                        },
                        UpdateItem::TerrainChunk(_, f) if f.contains(flags::TC_UNLOADED) => {
                            num_unload_chunk += 1;
                        },
                        _ => {},
                    }
                }
            },
            Event::RequestClient(_, _) => panic!("unexpected RequestClient"),
            Event::RequestPlane(_) => panic!("unexpected RequestPlane"),
            Event::RequestTerrainChunk(_, _) => panic!("unexpected RequestTerrainChunk"),
        }
    }
    assert_eq!(num_unload_client, expect_clients);
    assert_eq!(num_unload_plane, expect_planes);
    assert_eq!(num_unload_chunk, expect_chunks);
}

fn do_login(eng: &mut Engine<EventTracker>) {
    lifecycle::user_connect(eng, 1, "Anon".to_owned());

    // user_connect inc_ref's the client, so we get that request.

    info!(" ========== tick ==========");
    engine::tick::on_tick(eng);
    expect_client_request(eng);

    // Client import completes immediately.  login_fsm advances and requests STABLE_PLANE_FOREST.

    info!(" ========== tick ==========");
    engine::tick::on_tick(eng);
    expect_plane_request(eng, STABLE_PLANE_FOREST);

    // Plane import completes.  login_fsm sets the camera and finishes.
    assert!(!eng.login_fsm.contains(1));
    // The update after the camera change requests a bunch of terrain chunks.

    info!(" ========== tick ==========");
    engine::tick::on_tick(eng);
    expect_chunk_requests(eng,
                          STABLE_PLANE_FOREST,
                          Region::sized(V2::new(5, 6)) - 2,
                          30);
}

#[test]
fn test_login() {
    let _ = env_logger::init();
    let mut eng = init_engine();

    do_login(&mut eng);
}

fn test_logout_generic(i: usize) {
    let mut eng = init_engine();
    let eng = &mut eng;

    lifecycle::user_connect(eng, 1, "Anon".to_owned());

    if i == 0 {
        lifecycle::user_disconnect(eng, 1);
        engine::tick::on_tick(eng);
        expect_unloads(eng, 0, 0, 0);
        return;
    }

    engine::tick::on_tick(eng);
    expect_client_request(eng);

    if i == 1 {
        lifecycle::user_disconnect(eng, 1);
        engine::tick::on_tick(eng);
        expect_unloads(eng, 1, 0, 0);
        return;
    }

    engine::tick::on_tick(eng);
    expect_plane_request(eng, STABLE_PLANE_FOREST);

    if i == 2 {
        lifecycle::user_disconnect(eng, 1);
        engine::tick::on_tick(eng);
        expect_unloads(eng, 1, 1, 0);
        return;
    }

    engine::tick::on_tick(eng);
    expect_chunk_requests(eng,
                          STABLE_PLANE_FOREST,
                          Region::sized(V2::new(5, 6)) - 2,
                          30);

    if i == 3 {
        lifecycle::user_disconnect(eng, 1);
        engine::tick::on_tick(eng);
        expect_unloads(eng, 1, 1, 30);
        return;
    }
}

#[test]
fn test_logout_0() {
    let _ = env_logger::init();
    test_logout_generic(0);
}

#[test]
fn test_logout_1() {
    let _ = env_logger::init();
    test_logout_generic(1);
}

#[test]
fn test_logout_2() {
    let _ = env_logger::init();
    test_logout_generic(2);
}

#[test]
fn test_logout_3() {
    let _ = env_logger::init();
    test_logout_generic(3);
}
