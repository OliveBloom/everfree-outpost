extern crate common;

use common::util::SmallVec;

fn push_pop_n(n: u32) {
    let mut v: SmallVec<u32> = SmallVec::new();
    for i in 0 .. n {
        v.push(i);
    }
    for i in (0 .. n).rev() {
        assert_eq!(v.pop().unwrap(), i);
    }
    assert!(v.pop().is_none());
}

#[test]
fn insert_remove() {
    for n in 1 .. 11 {
        push_pop_n(n);
    }
}
