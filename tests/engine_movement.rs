extern crate env_logger;
#[macro_use] extern crate log;

extern crate common;
extern crate engine;
extern crate server_config;
extern crate server_types;
extern crate test_paths;
extern crate world;

use server_types::*;
use engine::Engine;
use engine::component::ward_map::WardMap;
use engine::input;
use engine::lifecycle;
use engine::logic;
use engine::update::{Delta, UpdateItem, flags};
use engine::update::delta::DeltaInternals;
use server_config::data;
use server_types::Stable;
use world::World;
use world::objects::*;


// TODO: make this into a proper library
#[path = "lib/engine.rs"]
mod test_engine;

use self::test_engine::*;


struct MapBundleProvider {
    tiles: &'static [&'static [u8]],
}

impl BundleProvider for MapBundleProvider {
    fn provide_client(&mut self, uid: u32, name: &str) -> DeltaInternals {
        DefaultBundleProvider.provide_client(uid, name)
    }

    fn provide_plane(&mut self, stable_id: Stable<PlaneId>) -> DeltaInternals {
        DefaultBundleProvider.provide_plane(stable_id)
    }

    fn provide_terrain_chunk(&mut self, stable_plane: Stable<PlaneId>, cpos: V2) -> DeltaInternals {
        let mut d = DefaultBundleProvider.provide_terrain_chunk(stable_plane, cpos);
        if cpos == 0 {
            let tc = d.new.get_terrain_chunk_mut(TCID0).unwrap();
            let bounds = Region::sized(TILE_SIZE);
            let blk = data().block_id("solid");
            for (y, row) in self.tiles.iter().enumerate() {
                for (x, &c) in row.iter().enumerate() {
                    if c == b'#' {
                        tc.blocks[bounds.index(V2::new(x as i32, y as i32))] = blk;
                    }
                }
            }
        }
        d
    }
}


fn test_corner_phase(eng: &mut Engine<EventTracker>,
                     provider: &mut MapBundleProvider,
                     start_x: i32,
                     start_y: i32,
                     delay: u16) -> bool {
    eprintln!("check {}, {}, {}", start_x, start_y, delay);
    let act = Activity::Stand {
        pos: V3::new(start_x, start_y, 0),
        dir: 0,
    };
    let now = eng.w.now;
    eng.w.client_mut(CID0).pawn_mut().unwrap().set_activity(act.clone(), now);

    tick_delta(eng, provider);
    tick_delta(eng, provider);
    tick_delta(eng, provider);

    let now = eng.w.now;
    engine::movement::on_queue_start(eng, CID0, delay, 5, 1);
    let mut ok = true;
    let mut stopped = false;
    while eng.w.now < now + 1200 {
        if !stopped && eng.w.now >= now + 500 {
            engine::movement::on_queue_change(eng, CID0, 1000, 5, 0);
            stopped = true;
        }
        let pos = eng.w.client(CID0).pawn().unwrap().pos(eng.w.now);
        eprintln!("  case {}, {}, {}: entity at {:?} as of {}",
                  start_x, start_y, delay, pos, eng.w.now);
        tick_delta(eng, provider);
        let pos = eng.w.client(CID0).pawn().unwrap().pos(eng.w.now);
        if pos.x < 16 || pos.y < 16 {
            eprintln!("error - in case {}, {}, {}, entity reached {:?} at {}",
                      start_x, start_y, delay, pos, eng.w.now);
            ok = false;
        }
    }

    let pos = eng.w.client(CID0).pawn().unwrap().pos(eng.w.now);
    if pos != V3::new(16, 16, 0) {
        eprintln!("error - in case {}, {}, {}, entity ended at {:?} at {}",
                  start_x, start_y, delay, pos, eng.w.now);
        ok = false;
    }

    ok
}

#[test]
fn corner_phase() {
    let _ = env_logger::init();
    let mut eng = init_engine(false);
    let mut provider = MapBundleProvider {
        tiles: &[
            b"##..",
            b"#...",
            b"....",
        ],
    };

    lifecycle::user_connect(&mut eng, 1, "Anon".to_owned());
    tick_delta(&mut eng, &mut provider);
    tick_delta(&mut eng, &mut provider);
    tick_delta(&mut eng, &mut provider);

    let mut ok = true;
    let mut test_idx = 0;
    for x in 32 .. 34 {
        for y in x - 2 .. x + 2 {
            for delay in 0 .. 5 {
                ok &= test_corner_phase(&mut eng, &mut provider, x, y, delay);
            }
        }
    }

    assert!(ok);
}


