extern crate common;
extern crate env_logger;

use common::types::*;
use common::activity::Activity;
use common::movement;
use common::physics::collide_2d::ShapeSource;


struct TileShapeSource {
    tiles: [u8; 16],
}

impl ShapeSource for TileShapeSource {
    fn can_pass(&mut self, start: V2, end: V2) -> bool {
        let bounds = Region2::sized(4);
        if !bounds.contains(end) {
            return false;
        }
        let tile = self.tiles[bounds.index(end)];
        tile == b'.'
    }
}


const SIZE: V2 = V2 { x: TILE_SIZE, y: TILE_SIZE };

#[test]
fn unobstructed() {
    // Entity moves around an unobstructed area.
    let mut tss = TileShapeSource {
        tiles: *b"....\
                  ....\
                  ....\
                  ....",
    };

    for offset in 0..10 {
        let r = movement::update_movement(
            &mut tss,
            Activity::Walk {
                pos: V3::new(0, 1, 0) * TILE_SIZE + V3::new(offset, 0, 0),
                velocity: V3::new(50, 0, 0),
                dir: 0,
            },
            0,
            0, 50,
            0, 500,
        );
        assert!(r.is_none());
    }
}

#[test]
fn obstructed() {
    // Entity moves around an unobstructed area.
    let mut tss = TileShapeSource {
        tiles: *b"....\
                  ....\
                  ....\
                  ####",
    };

    for offset in 0..10 {
        let r = movement::update_movement(
            &mut tss,
            Activity::Walk {
                pos: V3::new(0, 1, 0) * TILE_SIZE + V3::new(offset, 0, 0),
                velocity: V3::new(0, 50, 0),
                dir: 2,
            },
            0,
            2, 50,
            0, 1000,
        );
        assert!(r.is_some());
        let (new_act, new_start) = r.unwrap();
        assert_eq!(new_act.velocity(), 0);
        assert_eq!(new_act.start_pos().reduce(),
                   V2::new(0, 2) * TILE_SIZE + V2::new(offset, 0));
        assert_eq!(new_start, 320);
    }
}

#[test]
fn sliding_doorway() {
    // Sliding entity stops upon reaching a doorway.
    let mut tss = TileShapeSource {
        tiles: *b"....\
                  ....\
                  ##.#\
                  ....",
    };

    for offset in 0..10 {
        let r = movement::update_movement(
            &mut tss,
            Activity::Walk {
                pos: V3::new(0, 1, 0) * TILE_SIZE + V3::new(offset, 0, 0),
                velocity: V3::new(50, 0, 0),
                dir: 1,
            },
            0,
            1, 50,
            0, 1000,
        );
        assert!(r.is_some());
        let (new_act, new_start) = r.unwrap();
        assert_eq!(new_act.velocity(), 0);
        assert_eq!(new_act.start_pos().reduce(),
                   V2::new(2, 1) * TILE_SIZE);
    }
}

#[test]
fn corner_assist() {
    env_logger::init().unwrap();
    // Sliding entity uses corner assist to align with a doorway
    let mut tss = TileShapeSource {
        tiles: *b"....\
                  ....\
                  ##.#\
                  ....",
    };

    for offset in 1..10 {
        let r = movement::update_movement(
            &mut tss,
            Activity::Walk {
                pos: V3::new(2, 1, 0) * TILE_SIZE + V3::new(offset, 0, 0),
                velocity: V3::new(0, 0, 0),
                dir: 2,
            },
            0,
            2, 50,
            0, 1000,
        );

        if offset <= 4 {
            assert!(r.is_some());
            let (new_act, new_start) = r.unwrap();
            assert_eq!(new_act.velocity(), V3::new(-50, 0, 0));
        } else {
            assert!(r.is_none())
        }
    }
}
