const float TILE_SIZE = 16.0;
const float CHUNK_SIZE = 16.0;
const float LOCAL_SIZE = 8.0;
const float ATLAS_SIZE = 32.0;
const float ANIM_MODULUS_MS = 55440.0;

uniform vec2 camera_pos;
uniform vec2 camera_size;
uniform vec2 sliceCenter;
uniform float now;  // Seconds

attribute vec3 vert_offset;
attribute vec2 src_pos;
attribute vec3 block_pos;
attribute vec3 block_size;
attribute float anim_length;
attribute float anim_rate;
attribute float anim_oneshot_start;
attribute float anim_step;

varying vec2 tex_coord;
varying float baseZ;
varying float clipped;

vec2 uv(vec3 p) {
    return vec2(p.x, p.y - p.z);
}

bool should_clip() {
    vec3 center = vec3(sliceCenter, 0.0);
    vec3 c0 = center - vec3(1.0, 1.0, 0.0);
    vec3 c1 = center + vec3(2.0, 2.0, 0.0);
    vec3 pos = block_pos;
    vec3 size = block_size;

    // TODO: ugly hack
    // If it's too far left/up from the camera, wrap around.
    if (pos.x * TILE_SIZE < camera_pos.x - CHUNK_SIZE * TILE_SIZE) {
        // Remember, pos.x is measured in *blocks*.
        pos.x += LOCAL_SIZE * CHUNK_SIZE;
    }
    if (pos.y * TILE_SIZE < camera_pos.y - CHUNK_SIZE * TILE_SIZE) {
        pos.y += LOCAL_SIZE * CHUNK_SIZE;
    }

    return pos.z + size.z >= center.z + 2.0;
}

void main(void) {
    vec3 pos = block_pos * TILE_SIZE + vert_offset;

    // If it's too far left/up from the camera, wrap around.
    if (block_pos.x * TILE_SIZE < camera_pos.x - CHUNK_SIZE * TILE_SIZE) {
        // Remember, pos.x is measured in *blocks*.
        pos.x += LOCAL_SIZE * CHUNK_SIZE * TILE_SIZE;
    }
    if (block_pos.y * TILE_SIZE < camera_pos.y - CHUNK_SIZE * TILE_SIZE) {
        pos.y += LOCAL_SIZE * CHUNK_SIZE * TILE_SIZE;
    }

    vec2 pixel_pos = vec2(pos.x, pos.y - pos.z);

    float adj_z = block_pos.z / 16.0 + 1.0 / 32.0;
    float depth = pos.z + adj_z;

    vec2 norm_pos = (pixel_pos - camera_pos) / camera_size;
    float norm_depth = depth / (CHUNK_SIZE * TILE_SIZE);
    vec3 scaled_pos = vec3(norm_pos, norm_depth) * 2.0 - 1.0;
    gl_Position = vec4(scaled_pos, 1.0);

    vec2 tex_px = src_pos + vec2(vert_offset.x, vert_offset.y - vert_offset.z);

    if (anim_length != 0.0) {
        float frame;
        if (anim_length >= 0.0) {
            // This formula avoids losing precision when `now` is close to
            // `ANIM_MODULUS`.  (The previous one used `now * rate`.)
            frame = floor(anim_rate * mod(now, anim_length / anim_rate));
        } else {
            // Compute the delta in milliseconds between `now` and
            // `animOneshotStart`, in the range -MODULUS/2 .. MODULUS / 2.
            const float HALF_MOD = ANIM_MODULUS_MS / 2.0;
            float now_ms = mod(now * 1000.0, ANIM_MODULUS_MS);
            float delta = mod(now_ms - anim_oneshot_start + HALF_MOD, ANIM_MODULUS_MS) - HALF_MOD;
            frame = clamp(floor(delta / 1000.0 * anim_rate), 0.0, -anim_length - 1.0);
        }
        if (frame >= anim_length) {
            frame += 3.0;
        }
        tex_px.x += frame * anim_step;
    }

    tex_coord = tex_px / (ATLAS_SIZE * TILE_SIZE);
    baseZ = block_pos.z;


    clipped = should_clip() ? 1.0 : 0.0;
}
