const float TILE_SIZE = 16.0;
const float CHUNK_SIZE = 16.0;
const float LOCAL_SIZE = 8.0;

attribute vec2 corner;

uniform vec2 camera_size;
uniform vec2 tile_offset;

void main(void) {
    vec2 norm_pos = (tile_offset + corner * TILE_SIZE) / camera_size;
    vec2 adj_pos = norm_pos * 2.0 - 1.0;

    gl_Position = vec4(adj_pos, 0.5, 1.0);
}
