uniform vec4 output_color;

void main(void) {
    gl_FragColor = output_color;
}
