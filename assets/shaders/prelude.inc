#ifdef GL_ES
precision highp float;
#endif

#ifdef OUTPOST_FRAGMENT_SHADER

# ifdef OUTPOST_NO_DRAW_BUFFERS
#  define WRITE_COLOR(idx, val)      if (idx == OUTPOST_CUR_OUTPUT_IDX) gl_FragColor = (val)
# else
#  if GL_ES
#   extension GL_EXT_draw_buffers : enable
#  endif
#  define WRITE_COLOR(idx, val)      gl_FragData[(idx)] = (val)
# endif

# ifdef OUTPOST_NO_DEPTH_TEXTURE
vec4 READ_DEPTH(sampler2D tex, vec2 coord) {
    vec4 color = texture2D(tex, coord);
    return vec4(color.r + color.g / 256.0, 0.0, 0.0, 0.0);
}
#  ifdef OUTPOST_DEPTH_OUT_IDX
void WRITE_DEPTH() {
    float depth = gl_FragCoord.z;
    float big = depth * 256.0;
    vec4 color = vec4(floor(depth) / 256.0, mod(depth, 1.0), 0.0, 0.0);
    WRITE_COLOR(OUTPOST_DEPTH_OUT_IDX, color);
}
#  else
// This shader invocation has no depth output.
void WRITE_DEPTH() {}
#  endif
# else
#  define READ_DEPTH(tex, coord)     (texture2D((tex), (coord)))
#  define WRITE_DEPTH()
# endif

#endif
