uniform sampler2D sheet_tex;
uniform vec4 output_color;

varying vec2 tex_coord;

void main(void) {
    vec4 color = texture2D(sheet_tex, tex_coord);

    if (color.a < 1.0) {
        discard;
    } else {
        gl_FragColor = output_color;
    }
}
