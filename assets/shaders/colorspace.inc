#ifndef PI
# define PI 3.141592653589793
#endif


// Constants and helper functions

const float SRGB_A = 0.055;

float srgb_component_to_linear(float c) {
    if (c < 0.04045) {
        return c / 12.92;
    } else {
        return pow((c + SRGB_A) / (1. + SRGB_A), 2.4);
    }
}

float linear_rgb_component_to_srgb(float c) {
    if (c < 0.003130804) {
        return c * 12.92;
    } else {
        return (1. + SRGB_A) * pow(c, 1. / 2.4) - SRGB_A;
    }
}

const float LAB_DELTA = 6. / 29.;
const float LAB_DELTA2 = LAB_DELTA * LAB_DELTA;
const float LAB_DELTA3 = LAB_DELTA2 * LAB_DELTA;

float xyz_lab_f(float c) {
    if (c > LAB_DELTA3) {
        return pow(c, 1. / 3.);
    } else {
        return (c / (3. * LAB_DELTA2)) + (4. / 29.);
    }
}

float lab_xyz_f_inv(float c) {
    if (c > LAB_DELTA) {
        return pow(c, 3.);
    } else {
        return 3. * LAB_DELTA2 * (c - 4. / 29.);
    }
}

const float LAB_XN = 0.95047;
const float LAB_YN = 1.00000;
const float LAB_ZN = 1.08883;

// These are u'_n, v'_n for CIE Illuminant D65, which has (x,y) = (0.3128,
// 0.3290) (Note that these are lowercase x,y, not the uppercase X,Y used in
// XYZ.)
const float LUV_U_PRIME_N = 0.1979;
const float LUV_V_PRIME_N = 0.4683;

const mat3 LINEAR_RGB_XYZ_MATRIX = mat3(
    // NB: column-major order
    0.4124, 0.2126, 0.0193,
    0.3576, 0.7152, 0.1192,
    0.1805, 0.0722, 0.9505);

const mat3 LINEAR_RGB_XYZ_MATRIX_INV = mat3(
     3.24062548, -0.96893071,  0.05571012,
    -1.53720797,  1.87575606, -0.20402105,
    -0.49862860,  0.04151752,  1.05699594);


// sRGB <-> linear RGB

vec3 srgb_to_linear_rgb(vec3 c) {
    return vec3(
        srgb_component_to_linear(c.x),
        srgb_component_to_linear(c.y),
        srgb_component_to_linear(c.z));
}

vec3 linear_rgb_to_srgb(vec3 c) {
    return vec3(
        linear_rgb_component_to_srgb(c.x),
        linear_rgb_component_to_srgb(c.y),
        linear_rgb_component_to_srgb(c.z));
}


// linear RGB <-> XYZ

vec3 linear_rgb_to_xyz(vec3 c) {
    return LINEAR_RGB_XYZ_MATRIX * c;
}

vec3 xyz_to_linear_rgb(vec3 c) {
    return LINEAR_RGB_XYZ_MATRIX_INV * c;
}


// LUV <-> XYZ

vec3 xyz_to_luv(vec3 c) {
    float denom = c.x + 15. * c.y + 3. * c.z;
    float u_prime = (4. * c.x) / denom; 
    float v_prime = (9. * c.y) / denom; 

    float l_star;
    if (c.y <= pow(6. / 29., 3.)) {
        l_star = c.y * pow(29. / 3., 3.) / 100.;
    } else {
        l_star = 1.16 * pow(c.y, 1. / 3.) - 0.16;
    }

    // The formulas on wikipedia mention a Yn, but it's exactly 1, so it's omitted here.
    return vec3(
        l_star,
        13. * l_star * (u_prime - LUV_U_PRIME_N),
        13. * l_star * (v_prime - LUV_V_PRIME_N));
}

vec3 luv_to_xyz(vec3 c) {
    float u_prime = c.y / (13. * c.x) + LUV_U_PRIME_N;
    float v_prime = c.z / (13. * c.x) + LUV_V_PRIME_N;

    float y;
    if (c.x <= 0.08) {
        y = c.x * pow(3. / 29., 3.) * 100.;
    } else {
        y = pow((c.x + 0.16) / 1.16, 3.);
    }

    return vec3(
        y * (9. * u_prime) / (4. * v_prime),
        y,
        y * (12. - 3. * u_prime - 20. * v_prime) / (4. * v_prime));
}


// LUV <-> LCh(uv)

vec3 luv_to_lch_uv(vec3 c) {
    float theta = atan(c.z, c.y);
    if (theta < 0.) {
        theta += 2. * PI;
    }

    return vec3(
        c.x,
        sqrt(c.y * c.y + c.z * c.z),
        theta / 2. * PI);
}

vec3 lch_uv_to_luv(vec3 c) {
    float theta = c.z * 2. * PI;
    return vec3(
        c.x,
        cos(theta) * c.y,
        sin(theta) * c.y);
}


// Misc helper functions

bool srgb_in_gamut(vec3 c) {
    return
        0. <= c.x && c.x <= 1. &&
        0. <= c.y && c.y <= 1. &&
        0. <= c.z && c.z <= 1.;
}

