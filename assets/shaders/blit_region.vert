uniform vec2 region_pos;
uniform vec2 region_size;
uniform vec2 tex_size;

attribute vec2 corner;

varying vec2 tex_coord;

const mat4 transform = mat4(
        2.0,  0.0,  0.0,  0.0,
        0.0,  2.0,  0.0,  0.0,
        0.0,  0.0,  1.0,  0.0,
       -1.0, -1.0,  0.0,  1.0
       );

void main(void) {
    gl_Position = transform * vec4(corner, 0.0, 1.0);
    tex_coord = (region_pos + region_size * corner) / tex_size;
    //tex_coord = corner;
}
