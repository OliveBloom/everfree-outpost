uniform sampler2D color_tex;
uniform vec2 screen_size;
uniform vec4 output_color;

varying vec2 tex_coord;


vec2 off(float x_off, float y_off) {
    return tex_coord + vec2(x_off, y_off) / screen_size;
}

bool is_outline() {
    if (texture2D(color_tex, off(0.0, 0.0)).a != 0.0) {
        // This pixel is in the interior of the shape.
        return false;
    }

    vec4 n = texture2D(color_tex, off(0.0, -1.0));
    vec4 s = texture2D(color_tex, off(0.0, 1.0));
    vec4 w = texture2D(color_tex, off(-1.0, 0.0));
    vec4 e = texture2D(color_tex, off(1.0, 0.0));

    float max_a = max(max(n.a, s.a), max(w.a, e.a));
    if (max_a == 0.0) {
        // This pixel is not adjacent to any part of the shape.
        return false;
    } else {
        return true;
    }
}

void main(void) {
    if (is_outline()) {
        gl_FragColor = output_color;
    } else {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
    }
}
