%set STAND_BODY_LAYERS = ('body', 'wing', 'horn')
%set STAND_EYE_LAYERS = ('eye',)
%set STAND_HAIR_LAYERS = ('mane 1', 'mane 2', 'mane 3')
%set STAND_HAIR_LAYERS += ('tail 1', 'tail 2')



raw pony_stand_west
gen_images_code:
    from outpost_data.game.sprite import colorize_pony_layer_dict
    layers = load('sprites/pony_stand_west.aseprite', layers=True)
    layers = colorize_pony_layer_dict(layers)

    return dict((k, ATLAS.sprites.place(layers[k])) for k in
        ('body', 'wing', 'horn', 'eye',
            'mane 1', 'mane 2', 'mane 3',
            'tail 1', 'tail 2'))

gen_data_code:
    for k, v in IMG.items():
        DATA.sprite({
            # Remove spaces to convert "mane 1" to "mane1".
            'name': 'pony-{}-stand-4'.format(k.replace(' ', '')),
            'image': v,
            'ref': (12, 27),
            })

raw pony_trot_west
gen_images_code:
    from outpost_data.game.sprite import bounce, colorize_pony_layer, colorize_pony_layer_dict
    trot_layers = load('sprites/pony_trot_west.aseprite', layers=True, anim={'rate': 10})
    stand_layers = load('sprites/pony_stand_west.aseprite', layers=True)
    stand_layers = colorize_pony_layer_dict(stand_layers)

    trot_parts = [trot_layers[k] for k in ('right legs', 'body', 'left legs')]
    dct = {
        'body': colorize_pony_layer('body', trot_parts[0].stack(trot_parts[1:])),
        }

    bounce_keys = ('wing', 'horn', 'eye',
            'mane 1', 'mane 2', 'mane 3',
            'tail 1', 'tail 2')
    bounce_offsets = [-1, -2, -2, -1, -2, -2]
    dct.update((k, bounce(stand_layers[k], bounce_offsets, rate=10))
            for k in bounce_keys)

    return {k: ATLAS.sprites.place(v) for k,v in dct.items()}

gen_data_code:
    for k, v in IMG.items():
        DATA.sprite({
            # Remove spaces to convert "mane 1" to "mane1".
            'name': 'pony-{}-walk-4'.format(k.replace(' ', '')),
            'image': v,
            'ref': (12, 27),
            })


%set parts = ['body', 'wing', 'horn', 'eye']
%set parts += ['mane%d' % (i + 1) for i in range(3)]
%set parts += ['tail%d' % (i + 1) for i in range(2)]


%for part in parts

raw pony_layer-%part
gen_data_code:
    DATA.pony_layer({
        'name': 'pony-%part',
        'anims': {
            'stand-4': {
                'sprite': 'pony-%part-stand-4',
            },
            'walk-4': {
                'sprite': 'pony-%part-walk-4',
            },
            'stand-0': {
                'sprite': 'pony-%part-stand-4',
                'flip': True,
            },
            'walk-0': {
                'sprite': 'pony-%part-walk-4',
                'flip': True,
            },
        },
    })

%end



raw extra_pony
gen_data_code:
    DATA.extra({
        'name': 'pony_layer_table',
        'func': lambda id_maps: [
            id_maps.pony_layer.get('pony-%{}s' % p, 255)
            for p in ('body', 'horn', 'wing', None, 'eye',
                'mane1', 'mane2', 'mane3', 'tail1', 'tail2', 'tail3',
                'hat/party', 'hat/santa', 'hat/witch', 'hat/explorer',
                'socks/red', 'socks/orange', 'socks/yellow', 'socks/green',
                'socks/blue', 'socks/purple', 'socks/white', 'socks/black',)
            for s in ('f', 'm')
        ],
    })

    DATA.extra({
        'name': 'physics_anim_table',
        'func': lambda id_maps: [
          %for speed in range(4)
            [
              %for direction in range(8)
              %set display_dir = 4 if (direction + 1) % 8 >= 4 else 0
              %if speed == 0
                id_maps.pony_layer_anim['stand-%{str(display_dir)}'],
              %else
                id_maps.pony_layer_anim['walk-%{str(display_dir)}'],
              %end
              %end
            ],
          %end
        ],
    })

    DATA.extra({
        'name': 'anim_dir_table',
        'func': lambda id_maps: {
          %for direction in (0, 4)
          %for movement in ('stand', 'walk')
            id_maps.pony_layer_anim['%movement-%{str(direction)}']: %{str(direction)},
          %end
          %end
        },
    })


    DATA.extra({
        'name': 'default_anim',
        'func': lambda id_maps: id_maps.pony_layer_anim['stand-0'],
        })

    DATA.extra({
        'name': 'editor_anim',
        'func': lambda id_maps: id_maps.pony_layer_anim['stand-4'],
        })

    DATA.extra({
        'name': 'activity_none_anim',
        'func': lambda _: 0,
        })
    DATA.extra({
        'name': 'activity_layer',
        'func': lambda _: 0,
        })
    DATA.extra({
        'name': 'activity_bubble_graphics',
        'func': lambda _: 0,
        })

