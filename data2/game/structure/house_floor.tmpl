# "Furniture" object that can be placed and removed, and its accompanying item

# Arguments:

# image: path to the image file

# icon: path to the icon file (default: extract icon from structure image)
# icon_layer: layer to extract from the icon image
# ui_name: display name of the structure's item
# desc: description for the structure's item

# recipe_inputs: if set, defines a recipe that takes these inputs and produces
#                the furniture item.
# recipe_output_count: number of items produced per recipe (default: 1)
# recipe_time: time to craft in milliseconds (default: 1000)
# recipe_class: crafting class the station must have to craft this recipe
#               (default: workbench)
# recipe_ability: character ability required to use this recipe (default: none)

# behavior: script behavior to apply to this structure/item (default: basic)
# behavior_lib: script module where behavior function is defined (default:
#               struct_item)
# behavior_args: additional args to pass to behavior registration function

%set full_name = 'house_floor_' + name

raw structure_%full_name

gen_images_code:
    from outpost_data.game.images import load_image_layer, gen_item_image
    from outpost_data.game.house import load_floor_parts
    sheet = load(%{repr(image)})
    structure = load_floor_parts(sheet)
  %if icon
    item = load_image_layer(%{repr(icon)}, %{repr(icon_layer)})
  %else
    item = gen_item_image(structure[0])
  %end
    return {
        'structure': [ATLAS.structures.place(img) for img in structure],
        'item': ATLAS.items.place(item),
    }

gen_data_code:
    from outpost_data.game.structure import process_struct_size
    from outpost_data.game import parse_item_list
    size, shape, mesh, bounds = process_struct_size(1, 1, 0)

    parts = [{
        'image': img,
        'mesh': mesh,
        'mesh_bounds': bounds,
        'flags': 0,
    } for img in IMG['structure']]

    DATA.structure({
        'name': %{repr(full_name)},
        'layer': 0,
        'size': size,
        'shape': shape,
        'parts': parts,
        'draw_mode': DrawFence('house_floor', 1, True),
    })

    DATA.item({
        'name': %{repr(full_name)},
        'ui_name': %{repr(ui_name)},
        'desc': %{repr(desc)},
        'flags': I_USE_AT_POINT,
        'image': IMG['item'],
    })

  %if recipe_inputs
    DATA.recipe({
        'name': %{repr(full_name)},
        'ui_name': %{repr(ui_name)},
        'inputs': parse_item_list(%{repr(recipe_inputs)}),
        'outputs': [(%{repr(full_name)}, %{recipe_output_count or '1'})],
      %if recipe_ability
        'ability': %{repr(recipe_ability)},
      %end
        'crafting_class': %{repr(recipe_class or 'workbench')},
        'time': %{recipe_time or '1000'},
    })
  %end

script_code:
    from outpost.lib import %{behavior_lib or 'struct_item'}
    %{behavior_lib or 'struct_item'}.register_%{behavior or 'basic'}(
        %{repr(full_name)},
        item_name=%{repr(full_name)},
      %if behavior_args
        %{behavior_args},
      %end
        )

