%set FENCE_KEYS = ('base', 'n', 'e', 's', 'w')

raw structure_fence
gen_images_code:
    from outpost_data.game.images import gen_item_image
    layers = load('structures/fence.xcf', layers=True)
    keys = %{repr(FENCE_KEYS)}
    dct = dict((k, ATLAS.structures.place(layers[k])) for k in keys)
    dct['icon'] = ATLAS.items.place(gen_item_image(layers['base']))
    return dct

gen_data_code:
    from outpost_data.game.structure import process_struct_size
    size, shape, mesh, bounds = process_struct_size(1, 0, 1)

    keys = %{repr(FENCE_KEYS)}
    parts = [{
        'image': IMG[k],
        'mesh': mesh,
        'mesh_bounds': bounds,
    } for k in keys]

    DATA.structure({
        'name': 'fence',
        'layer': 1,
        'size': size,
        'shape': shape,
        'parts': parts,
        'draw_mode': DrawFence('fence', 1),
    })

    DATA.item({
        'name': 'fence',
        'ui_name': 'Fence',
        'desc': 'Makes for good neighbors.',
        'flags': I_USE_AT_POINT,
        'image': IMG['icon'],
    })

    DATA.recipe({
        'name': 'fence',
        'ui_name': 'Fence',
        'inputs': [('wood', 5)],
        'outputs': [('fence', 2)],
        'crafting_class': 'workbench',
        'time': 500,
    })

script_code:
    from outpost.lib import struct_item
    struct_item.register_basic('fence')
