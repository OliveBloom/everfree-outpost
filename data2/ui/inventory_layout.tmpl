# Inventory layout definition, for dialogs like Equipment that show an
# inventory with a special shape.

# *Image format*:  The input image must be an XCF.  It must have a layer group
# that contains one layer for each inventory slot.  Each slot layer must be
# 18x18 pixels and should depict a single item slot (with border but without a
# highlight) with an appropriate background icon in the slot.  The order of
# slot layers within the layer group determines the order of slots in the
# layout definition, with the bottom layer being first.

# Arguments:

# - image: The image containing the inventory slot icons and layout.  See above
#   for the expected format.
# - group_name: The name of the layer group containing the slot icons.


raw ui_inventory_layout_%{name}

gen_images_code:
    from outpost_data.lib.image import load_layer_info

    layers = load('ui2/%{image}', layers=True)
    info = load_layer_info('ui2/%{image}')

    target = %{repr(group_name)}

    slots = []
    for i in info:
        if i['name'].startswith(target + '/'):
            _, _, basename = i['name'].rpartition('/')
            img = layers[basename]
            # Extract only the icon.  The border will be provided by the normal
            # item slot widget.
            img = img.extract((2, 2), (14, 14), unit=1)

            x, y = i['offset']

            slots.append({
                'image': ATLAS.ui.place(img),
                'pos': (x, y),
                })

    # Find the bounding box of this layout, and convert absolute slot positions
    # (in image coordinates) to offsets.

    # The -1 and +19 (instead of +18) account for the fact that the actual slot
    # widget is 20x20, but the layers here are only 18x18.
    min_x = min(s['pos'][0] - 1 for s in slots)
    min_y = min(s['pos'][1] - 1 for s in slots)
    max_x = max(s['pos'][0] + 19 for s in slots)
    max_y = max(s['pos'][1] + 19 for s in slots)

    for s in slots:
        x, y = s['pos']
        s['pos'] = (x - min_x - 1, y - min_y - 1)

    return {
            'size': (max_x - min_x, max_y - min_y),
            'slots': slots,
            }

gen_data_code:
    DATA.inventory_layout({
        'name': %{repr(name)},
        'size': IMG['size'],
        'slots': IMG['slots'],
        })
