use server_types::*;
use std::borrow::Borrow;
use std::collections::{HashMap, HashSet};
use std::mem;
use std::ops::{Deref, DerefMut};
use common::util::StrResult;
use common::util::GridOrdered;
use server_config::data;
use server_extra::Extra;

use {World, Ext};
use ext::{Update, Components};
use maps::stable_id_map::{self, StableIdMap};
use object_ref::{ObjectRef, ObjectMut};
use objects::*;


impl<E: Ext> World<E> {
    pub fn new(components: E::Components,
               update: E::Update) -> World<E> {
        World {
            extra: Extra::new(),

            clients: StableIdMap::new(),
            entities: StableIdMap::new(),
            inventories: StableIdMap::new(),
            planes: StableIdMap::new(),
            terrain_chunks: StableIdMap::new(),
            structures: StableIdMap::new(),

            components: components,
            update: update,
        }
    }

    pub fn extra(&self) -> &Extra {
        &self.extra
    }
}

impl<E: Ext> Deref for World<E> {
    type Target = E::Components;

    fn deref(&self) -> &E::Components {
        &self.components
    }
}

impl<E: Ext> DerefMut for World<E> {
    fn deref_mut(&mut self) -> &mut E::Components {
        &mut self.components
    }
}


impl<E: Ext> World<E> {
    /// Create a new client.
    ///
    /// If `client.stable_id != NO_STABLE_ID`, the caller is responsible for avoiding collisions
    /// with existing stable_ids.
    pub fn try_create_client_as(&mut self,
                                id: ClientId,
                                client: Client) -> StrResult<ObjectMut<E, Client>> {
        self.clients.insert_as(id, Client::pre_insert(client))?;
        self.update.create_client(id);
        Ok(self.client_mut(id))
    }

    pub fn try_create_entity_as(&mut self,
                                id: EntityId,
                                entity: Entity) -> StrResult<ObjectMut<E, Entity>> {
        let Entity { attachment, .. } = entity;

        let attach_ok = match attachment {
            EntityAttachment::World => true,
            EntityAttachment::Chunk => {
                error!("EntityAttachment::Chunk is not supported");
                false
            },
            EntityAttachment::Client(cid) => self.clients.get(cid).is_some(),
        };
        if !attach_ok {
            fail!("parent object does not exist");
        }

        self.entities.insert_as(id, Entity::pre_insert(entity))?;

        match attachment {
            EntityAttachment::World => {},
            EntityAttachment::Chunk => unreachable!(),
            EntityAttachment::Client(cid) => {
                self.clients[cid].child_entities.insert(id);
            },
        }

        self.update.create_entity(id);
        Ok(self.entity_mut(id))
    }

    pub fn try_create_inventory_as(&mut self,
                                   id: InventoryId,
                                   inventory: Inventory) -> StrResult<ObjectMut<E, Inventory>> {
        let Inventory { attachment, .. } = inventory;

        let attach_ok = match attachment {
            InventoryAttachment::World => true,
            InventoryAttachment::Client(cid) => self.clients.get(cid).is_some(),
            InventoryAttachment::Entity(eid) => self.entities.get(eid).is_some(),
            InventoryAttachment::Structure(sid) => self.structures.get(sid).is_some(),
        };
        if !attach_ok {
            fail!("parent object does not exist");
        }

        self.inventories.insert_as(id, Inventory::pre_insert(inventory))?;

        match attachment {
            InventoryAttachment::World => {},
            InventoryAttachment::Client(cid) => {
                self.clients[cid].child_inventories.insert(id);
            },
            InventoryAttachment::Entity(eid) => {
                self.entities[eid].child_inventories.insert(id);
            },
            InventoryAttachment::Structure(sid) => {
                self.structures[sid].child_inventories.insert(id);
            },
        }

        self.update.create_inventory(id);
        Ok(self.inventory_mut(id))
    }

    pub fn try_create_plane_as(&mut self,
                               id: PlaneId,
                               plane: Plane) -> StrResult<ObjectMut<E, Plane>> {
        self.planes.insert_as(id, Plane::pre_insert(plane))?;
        // All planes should have a stable ID.  Entities and structures need to reference their
        // containing plane by stable ID on export.
        self.plane_mut(id).pin();
        self.update.create_plane(id);
        Ok(self.plane_mut(id))
    }

    pub fn try_create_terrain_chunk_as(&mut self,
                                       id: TerrainChunkId,
                                       terrain_chunk: TerrainChunk)
                                       -> StrResult<ObjectMut<E, TerrainChunk>> {
        let plane_id = terrain_chunk.plane;
        let cpos = terrain_chunk.cpos;

        self.terrain_chunks.insert_as(id, TerrainChunk::pre_insert(terrain_chunk))?;

        {
            let p = self.planes.get_mut(plane_id).ok_or("parent object does not exist")?;
            if let Some(tcid) = p.loaded_chunks.get(&cpos) {
                error!("overwriting existing chunk {:?} at {:?} {:?}", 
                       tcid, plane_id, cpos);
            }
            trace!("inserting: {:?} @ {:?} = {:?}", plane_id, cpos, id);
            p.loaded_chunks.insert(cpos, id);
        }

        self.update.create_terrain_chunk(id);
        Ok(self.terrain_chunk_mut(id))
    }

    pub fn try_create_structure_as(&mut self,
                                   id: StructureId,
                                   structure: Structure)
                                   -> StrResult<ObjectMut<E, Structure>> {
        let Structure { pos, plane, attachment, template, .. } = structure;
        let cpos = pos.reduce().div_floor(CHUNK_SIZE);

        let attach_ok = match attachment {
            StructureAttachment::Plane =>
                self.planes.get(plane).is_some(),
            StructureAttachment::Chunk =>
                self.planes.get(plane).map_or(false, |p| p.loaded_chunks.contains_key(&cpos)),
        };
        if !attach_ok {
            fail!("parent object does not exist");
        }

        self.structures.insert_as(id, Structure::pre_insert(structure))?;

        match attachment {
            StructureAttachment::Plane => {},
            StructureAttachment::Chunk => {
                let tcid = self.planes[plane].loaded_chunks[&cpos];
                self.terrain_chunks[tcid].child_structures.insert(id);
            },
        }

        {
            let data = data();
            let t = data.template(template);
            for p in (Region::sized(t.size) + pos).points() {
                self.planes[plane].structures_by_pos.insert(GridOrdered(p), id);
            }
        }

        self.update.create_structure(id);
        Ok(self.structure_mut(id))
    }

    for_each_obj! {
        pub fn $create_obj_as(&mut self, id: $ObjId, $obj: $Obj) -> ObjectMut<E, $Obj> {
            self.$try_create_obj_as(id, $obj).unwrap()
        }

        pub fn $try_create_obj(&mut self, $obj: $Obj) -> StrResult<ObjectMut<E, $Obj>> {
            let id = self.$objs.next_id();
            self.$try_create_obj_as(id, $obj)
        }

        pub fn $create_obj(&mut self, $obj: $Obj) -> ObjectMut<E, $Obj> {
            self.$try_create_obj($obj).unwrap()
        }
    }


    // When a parent object is destroyed, so are its children.  But the children should not try to
    // detach themselves from the parent, because the parent has already been removed from the map.
    // To implement this behavior, the private `destroy_obj_inner` functions take `detach`
    // arguments that indicate whether they should detach themselves.  This argument is generally
    // `true` for the parent object and `false` for all children.

    fn destroy_client_inner(&mut self, id: ClientId) {
        let mut c = unwrap_or_error!(self.clients.remove(id),
                                     "tried to destroy nonexistent {:?}", id);
        for eid in mem::replace(&mut c.child_entities, HashSet::new()) {
            self.destroy_entity_inner(eid, false);
        }
        for iid in mem::replace(&mut c.child_inventories, HashSet::new()) {
            self.destroy_inventory_inner(iid, false);
        }
        self.update.destroy_client(id, c);
        self.components.destroy_client(id, &self.update);
    }

    fn destroy_entity_inner(&mut self, id: EntityId, detach: bool) {
        let mut e = unwrap_or_error!(self.entities.remove(id),
                                     "tried to destroy nonexistent {:?}", id);
        for iid in mem::replace(&mut e.child_inventories, HashSet::new()) {
            self.destroy_inventory_inner(iid, false);
        }
        if detach {
            match e.attachment {
                EntityAttachment::World => {},
                EntityAttachment::Chunk => {},
                EntityAttachment::Client(cid) => {
                    self.client_mut(cid).obj_mut().child_entities.remove(&id);
                },
            }
        }
        self.update.destroy_entity(id, e);
        self.components.destroy_entity(id, &self.update);
    }

    fn destroy_inventory_inner(&mut self, id: InventoryId, detach: bool) {
        let i = unwrap_or_error!(self.inventories.remove(id),
                                 "tried to destroy nonexistent {:?}", id);
        if detach {
            match i.attachment {
                InventoryAttachment::World => {},
                InventoryAttachment::Client(cid) => {
                    self.client_mut(cid).obj_mut().child_inventories.remove(&id);
                },
                InventoryAttachment::Entity(eid) => {
                    self.entity_mut(eid).obj_mut().child_inventories.remove(&id);
                },
                InventoryAttachment::Structure(sid) => {
                    self.structure_mut(sid).obj_mut().child_inventories.remove(&id);
                },
            }
        }
        self.update.destroy_inventory(id, i);
        self.components.destroy_inventory(id, &self.update);
    }

    fn destroy_plane_inner(&mut self, id: PlaneId) {
        let mut p = unwrap_or_error!(self.planes.remove(id),
                                     "tried to destroy nonexistent {:?}", id);
        for (_, tcid) in mem::replace(&mut p.loaded_chunks, HashMap::new()) {
            self.destroy_terrain_chunk_inner(tcid, false);
        }
        self.update.destroy_plane(id, p);
        self.components.destroy_plane(id, &self.update);
    }

    fn destroy_terrain_chunk_inner(&mut self, id: TerrainChunkId, detach: bool) {
        let mut tc = unwrap_or_error!(self.terrain_chunks.remove(id),
                                      "tried to destroy nonexistent {:?}", id);
        for sid in mem::replace(&mut tc.child_structures, HashSet::new()) {
            self.destroy_structure_inner(sid, detach, false);
        }
        if detach {
            let old_id = self.plane_mut(tc.plane).obj_mut().loaded_chunks.remove(&tc.cpos);
            if old_id != Some(id) {
                error!("bad entry in loaded_chunks for {:?} at {:?}: {:?} (expected {:?})",
                       tc.plane, tc.cpos, old_id, Some(id));
            }
        }
        self.update.destroy_terrain_chunk(id, tc);
        self.components.destroy_terrain_chunk(id, &self.update);
    }

    fn destroy_structure_inner(&mut self,
                               id: StructureId,
                               detach_plane: bool,
                               detach_terrain_chunk: bool) {
        let mut s = unwrap_or_error!(self.structures.remove(id),
                                     "tried to destroy nonexistent {:?}", id);
        for iid in mem::replace(&mut s.child_inventories, HashSet::new()) {
            self.destroy_inventory_inner(iid, false);
        }
        let cpos = s.pos.tile_to_cpos();
        if detach_plane {
            let data = data();
            let t = data.template(s.template);
            let mut plane = self.plane_mut(s.plane);
            let cache = &mut plane.obj_mut().structures_by_pos;
            for p in (Region::sized(t.size) + s.pos).points() {
                cache.remove(GridOrdered(p), id);
            }
        }
        if detach_terrain_chunk {
            match s.attachment {
                StructureAttachment::Plane => {},
                StructureAttachment::Chunk => {
                    self.plane_mut(s.plane).chunk_mut(cpos).unwrap()
                        .obj_mut().child_structures.remove(&id);
                },
            }
        }
        self.update.destroy_structure(id, s);
        self.components.destroy_structure(id, &self.update);
    }


    pub fn destroy_client(&mut self, id: ClientId) {
        self.destroy_client_inner(id);
    }

    pub fn destroy_entity(&mut self, id: EntityId) {
        self.destroy_entity_inner(id, true);
    }

    pub fn destroy_inventory(&mut self, id: InventoryId) {
        self.destroy_inventory_inner(id, true);
    }

    pub fn destroy_plane(&mut self, id: PlaneId) {
        self.destroy_plane_inner(id);
    }

    pub fn destroy_terrain_chunk(&mut self, id: TerrainChunkId) {
        self.destroy_terrain_chunk_inner(id, true);
    }

    pub fn destroy_structure(&mut self, id: StructureId) {
        self.destroy_structure_inner(id, true, true);
    }


    // Stable ID access, for use in save/load code

    for_each_obj! {
        pub fn $next_obj_stable_id(&self) -> StableId {
            self.$objs.next_stable_id()
        }

        /// Set the next stable ID, without recording a change event.  This should be used only
        /// during import from a bundle.
        pub fn $import_next_obj_stable_id(&mut self, id: StableId) {
            // Don't permit the stable ID counter to run backwards.
            assert!(id >= self.$objs.next_stable_id());
            self.$objs.set_next_stable_id(id);
        }

        pub fn $set_next_obj_stable_id(&mut self, id: StableId) {
            self.update.$next_obj_stable_id(self.$next_obj_stable_id());
            self.$import_next_obj_stable_id(id);
        }
    }


    /// Free up IDs of recently destroyed objects so they can be reused.  Making this operation
    /// explicit helps avoid a sort of "ABA problem" with objects, which goes like this:
    ///
    ///  1. The object with ID `id` is scheduled for later processing.
    ///  2. Object `id` is destroyed.
    ///  3. A new object is created, and gets assigned ID `id`.
    ///  4. Deferred processing runs, and wrongly operates on the newly created object.
    ///
    /// The downside is that this method must be called regularly, otherwise the set of IDs to be
    /// recycled will grow without bound.
    pub fn recycle_ids(&mut self) {
        self.clients.recycle_ids();
        self.entities.recycle_ids();
        self.inventories.recycle_ids();
        self.planes.recycle_ids();
        self.terrain_chunks.recycle_ids();
        self.structures.recycle_ids();
    }
}


expand_objs! {
    impl<E: Ext> World<E> { $(

        pub fn $get_obj(&self, id: $ObjId) -> Option<ObjectRef<E, $Obj>> {
            ObjectRef::<E, $Obj>::new(self, id)
        }

        pub fn $obj(&self, id: $ObjId) -> ObjectRef<E, $Obj> {
            self.$get_obj(id)
                .unwrap_or_else(|| panic!("no such object: {:?}", id))
        }

        pub fn $objs(&self) -> $Objs<E> {
            $Objs {
                world: self,
                iter: self.$objs.iter(),
            }
        }

        pub fn $transient_obj_id(&self, stable_id: Stable<$ObjId>) -> Option<$ObjId> {
            self.$objs.get_id(stable_id)
        }

        pub fn $objs_len(&self) -> usize {
            self.$objs.len()
        }

    )* }

    impl<E: Ext> World<E> { $(

        pub fn $get_obj_mut(&mut self, id: $ObjId) -> Option<ObjectMut<E, $Obj>> {
            ObjectMut::<E, $Obj>::new(self, id)
        }

        pub fn $obj_mut(&mut self, id: $ObjId) -> ObjectMut<E, $Obj> {
            self.$get_obj_mut(id)
                .unwrap_or_else(|| panic!("no such object: {:?}", id))
        }

    )* }
}


for_each_obj! {
    pub struct $Objs<'a, E: Ext+'a> {
        world: &'a World<E>,
        iter: stable_id_map::Iter<'a, $ObjId, $Obj>,
    }

    impl<'a, E: Ext> Iterator for $Objs<'a, E> {
        type Item = ObjectRef<'a, E, $Obj>;

        fn next(&mut self) -> Option<ObjectRef<'a, E, $Obj>> {
            let world = self.world;
            self.iter.next().map(|(id, obj)| {
                ObjectRef::new_unchecked(world, id, obj)
            })
        }
    }
}


for_each_obj! {
    pub struct $ObjsById<'a, E: Ext+'a, I> {
        world: &'a World<E>,
        iter: I,
    }

    impl<'a, E: Ext, I> $ObjsById<'a, E, I>
            where I: Iterator, I::Item: Borrow<$ObjId> {
        pub fn new(world: &'a World<E>, iter: I) -> $ObjsById<'a, E, I> {
            $ObjsById {
                world: world,
                iter: iter,
            }
        }
    }

    impl<'a, E: Ext, I> Iterator for $ObjsById<'a, E, I>
            where I: Iterator, I::Item: Borrow<$ObjId> {
        type Item = ObjectRef<'a, E, $Obj>;
        fn next(&mut self) -> Option<ObjectRef<'a, E, $Obj>> {
            let world = self.world;
            self.iter.next().map(|id| {
                let id = *id.borrow();
                ObjectRef::new(world, id).unwrap()
            })
        }
    }
}
