
/// Generates functions that look like this:
///
///     fn get_field(&self) -> FieldType {
///         let obj = self.obj();
///         obj.field
///     }
///
/// The `$body` can be any expression over `$obj`.
macro_rules! simple_getters {
    (object_type = $Obj:ty;
     obj_ident = $obj:ident;
     $(fn $name:ident : $ret_ty:ty = $body:expr;)*) => {
        impl<'a, E: Ext> ObjectRef<'a, E, $Obj> {
            $(
                pub fn $name(&self) -> $ret_ty {
                    let $obj = self.obj();
                    $body
                }
            )*
        }

        impl<'a, E: Ext> ObjectMut<'a, E, $Obj> {
            $(
                pub fn $name(&self) -> $ret_ty {
                    let $obj = self.obj();
                    $body
                }
            )*
        }
    };
}
