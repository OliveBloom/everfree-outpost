use server_types::*;
use std::fmt::Debug;

use {World, Ext};
use maps::StableIdMap;
use maps::IntrusiveStableId;


#[macro_use] mod macros;

mod impls;

mod client;
mod entity;
mod inventory;
mod plane;
mod terrain_chunk;
mod structure;


pub trait Object<E: Ext>: 'static+Sized+IntrusiveStableId {
    type Id: ObjectId+Debug;
    type Flags: Copy;

    fn collection<'a>(world: &'a World<E>) -> &'a StableIdMap<Self::Id, Self>;
    fn collection_mut<'a>(world: &'a mut World<E>) -> &'a mut StableIdMap<Self::Id, Self>;

    fn get<'a>(world: &'a World<E>, id: Self::Id) -> Option<&'a Self> {
        Self::collection(world).get(id)
    }

    fn get_mut<'a>(world: &'a mut World<E>, id: Self::Id) -> Option<&'a mut Self> {
        Self::collection_mut(world).get_mut(id)
    }

    fn flags(&self) -> &Self::Flags;
    fn flags_mut(&mut self) -> &mut Self::Flags;

    fn notify_flag_change(world: &World<E>, id: Self::Id, old: Self::Flags);
    fn notify_stable_id_change(world: &World<E>, id: Self::Id, old: StableId);
    fn notify_next_stable_id_change(world: &World<E>, old: StableId);
}


pub struct ObjectRef<'a, E: Ext+'a, O: Object<E>> {
    world: &'a World<E>,
    id: O::Id,
    obj: &'a O,
}

impl<'a, E: Ext+'a, O: Object<E>> Clone for ObjectRef<'a, E, O> {
    fn clone(&self) -> ObjectRef<'a, E, O> {
        *self
    }
}

impl<'a, E: Ext+'a, O: Object<E>> Copy for ObjectRef<'a, E, O> {}


impl<'a, E: Ext, O: Object<E>> ObjectRef<'a, E, O> {
    pub fn new(world: &'a World<E>, id: O::Id) -> Option<ObjectRef<'a, E, O>> {
        O::get(world, id).map(|obj| {
            ObjectRef {
                world: world,
                id: id,
                obj: obj,
            }
        })
    }

    // Not memory-unsafe, but could produce weird results if `id` isn't actually the ID for `obj`.
    pub fn new_unchecked(world: &'a World<E>, id: O::Id, obj: &'a O) -> ObjectRef<'a, E, O> {
        ObjectRef {
            world: world,
            id: id,
            obj: obj,
        }
    }

    pub fn borrow<'b>(&'b self) -> ObjectRef<'b, E, O> {
        ObjectRef {
            world: self.world,
            id: self.id,
            obj: self.obj,
        }
    }

    pub fn world(&self) -> &'a World<E> {
        self.world
    }

    pub fn id(&self) -> O::Id {
        self.id
    }

    pub fn obj(&self) -> &'a O {
        self.obj
    }

    // Common object fields

    pub fn stable_id(&self) -> Option<Stable<O::Id>> {
        Stable::new_checked(self.obj().get_stable_id())
    }

    pub fn flags(&self) -> O::Flags {
        *self.obj().flags()
    }
}



pub struct ObjectMut<'a, E: Ext+'a, O: Object<E>> {
    world: &'a mut World<E>,
    id: O::Id,
}

impl<'a, E: Ext, O: Object<E>> ObjectMut<'a, E, O> {
    pub fn new(world: &'a mut World<E>, id: O::Id) -> Option<ObjectMut<'a, E, O>> {
        if let Some(_) = O::get_mut(world, id) {
            Some(ObjectMut {
                world: world,
                id: id,
            })
        } else {
            None
        }
    }

    // Not memory-unsafe, but could produce panics if `id` is not valid.
    pub fn new_unchecked(world: &'a mut World<E>, id: O::Id) -> ObjectMut<'a, E, O> {
        ObjectMut {
            world: world,
            id: id,
        }
    }

    pub fn borrow<'b>(&'b self) -> ObjectRef<'b, E, O> {
        ObjectRef::new(self.world, self.id)
            .unwrap_or_else(|| panic!("ObjectMut expired: {:?}", self.id))
    }

    pub fn borrow_mut<'b>(&'b mut self) -> ObjectMut<'b, E, O> {
        let id = self.id;
        ObjectMut::new(self.world, id)
            .unwrap_or_else(|| panic!("ObjectMut expired: {:?}", id))
    }

    pub fn world(&self) -> &World<E> {
        self.world
    }

    pub fn world_mut(&mut self) -> &mut World<E> {
        self.world
    }

    pub fn id(&self) -> O::Id {
        self.id
    }

    pub fn obj(&self) -> &O {
        O::get(self.world, self.id)
            .unwrap_or_else(|| panic!("ObjectMut expired: {:?}", self.id))
    }

    /// Get a mutable reference to the object data.
    ///
    /// **Warning**: This bypasses all sanity checks and allows violating `World` invariants!
    /// Don't use this unless you know what you're doing.
    pub fn obj_mut(&mut self) -> &mut O {
        let id = self.id;
        O::get_mut(self.world, id)
            .unwrap_or_else(|| panic!("ObjectMut expired: {:?}", id))
    }

    // Common object fields

    pub fn stable_id(&self) -> Option<Stable<O::Id>> {
        Stable::new_checked(self.obj().get_stable_id())
    }

    pub fn flags(&self) -> O::Flags {
        *self.obj().flags()
    }
}

impl<'a, E: Ext, O: Object<E>> ObjectMut<'a, E, O> {
    pub fn pin(&mut self) -> Stable<O::Id> {
        // `pin()` changes the stable_id and next_stable_id only when the stable_id is unset
        if self.obj().get_stable_id() == NO_STABLE_ID {
            O::notify_stable_id_change(self.world, self.id, self.obj().get_stable_id());
            O::notify_next_stable_id_change(self.world,
                                            O::collection(self.world).next_stable_id());
        }
        O::collection_mut(self.world).pin(self.id)
    }

    pub fn modify_flags<R, F: FnOnce(&mut O::Flags) -> R>(&mut self, f: F) -> R {
        O::notify_flag_change(self.world, self.id, *self.obj().flags());
        let result = f(self.obj_mut().flags_mut());
        result
    }

    pub fn set_flags(&mut self, flags: O::Flags) {
        self.modify_flags(|f| *f = flags);
    }
}
