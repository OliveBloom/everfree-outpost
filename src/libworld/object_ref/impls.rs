use server_types::*;

use World;
use ext::{Ext, Update};
use maps::StableIdMap;
use object_ref::Object;
use objects::*;

for_each_obj! {
    impl<E: Ext> Object<E> for $Obj {
        type Id = $ObjId;
        type Flags = $ObjFlags;

        fn collection(world: &World<E>) -> &StableIdMap<$ObjId, $Obj> {
            &world.$objs
        }

        fn collection_mut(world: &mut World<E>) -> &mut StableIdMap<$ObjId, $Obj> {
            &mut world.$objs
        }

        fn flags(&self) -> &$ObjFlags {
            &self.flags
        }

        fn flags_mut(&mut self) -> &mut $ObjFlags {
            &mut self.flags
        }

        fn notify_flag_change(world: &World<E>, id: Self::Id, old: Self::Flags) {
            world.update.$obj_flags(id, old);
        }

        fn notify_stable_id_change(world: &World<E>, id: Self::Id, old: StableId) {
            world.update.$obj_stable_id(id, old);
        }

        fn notify_next_stable_id_change(world: &World<E>, old: StableId) {
            world.update.$next_obj_stable_id(old);
        }
    }
}

