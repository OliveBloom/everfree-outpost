use server_types::*;
use std::collections::hash_set;

use Ext;
use ext::Update;
use objects::*;
use object_ref::{ObjectRef, ObjectMut};
use world::InventoriesById;


simple_getters! {
    object_type = Entity;
    obj_ident = obj;

    fn plane_id: PlaneId = obj.plane;
    fn activity: &Activity = &obj.activity;
    fn activity_start: Time = obj.activity_start;
    fn appearance: &Appearance = &obj.appearance;
    fn attachment: EntityAttachment = obj.attachment;
}

// Setters and modifiers

impl<'a, E: Ext> ObjectMut<'a, E, Entity> {
    /// Moves this entity to the indicated plane.  The plane must already exist.
    pub fn set_plane(&mut self, pid: PlaneId) {
        self.world.update.entity_plane(self.id, self.obj().plane);
        assert!(self.world().get_plane(pid).is_some(),
                "set_plane: plane does not exist");
        self.obj_mut().plane = pid;
    }

    pub fn set_activity(&mut self, act: Activity, time: Time) {
        {
            let obj = self.obj();
            self.world.update.entity_activity(self.id, &obj.activity, obj.activity_start);
        }
        {
            let obj = self.obj_mut();
            obj.activity = act;
            obj.activity_start = time;
        }
    }

    pub fn set_appearance(&mut self, val: Appearance) {
        self.world.update.entity_appearance(self.id, &self.obj().appearance);
        self.obj_mut().appearance = val;
    }

    pub fn modify_appearance<F: FnOnce(&mut Appearance) -> R, R>(&mut self, f: F) -> R {
        self.world.update.entity_appearance(self.id, &self.obj().appearance);
        f(&mut self.obj_mut().appearance)
    }
}

// Non-mut methods.

impl<'a, E: Ext> ObjectRef<'a, E, Entity> {
    pub fn plane(&self) -> ObjectRef<'a, E, Plane> {
        self.world().plane(self.plane_id())
    }

    pub fn pos(&self, now: Time) -> V3 {
        self.obj().activity.pos(self.activity_start(), now)
    }

    pub fn controller(&self) -> Option<ObjectRef<'a, E, Client>> {
        let cid = match self.obj().attachment {
            EntityAttachment::Client(cid) => cid,
            _ => return None,
        };

        let c = self.world().client(cid);
        if c.pawn_id() != Some(self.id) {
            return None;
        }
        Some(c)
    }

    pub fn child_inventories(&self) -> InventoriesById<'a, E, hash_set::Iter<'a, InventoryId>> {
        InventoriesById::new(self.world(), self.obj().child_inventories.iter())
    }
}

impl<'a, E: Ext> ObjectMut<'a, E, Entity> {
    pub fn plane(&self) -> ObjectRef<E, Plane> {
        self.world().plane(self.plane_id())
    }

    pub fn pos(&self, now: Time) -> V3 {
        self.obj().activity.pos(self.activity_start(), now)
    }

    pub fn controller(&self) -> Option<ObjectRef<E, Client>> {
        let cid = match self.obj().attachment {
            EntityAttachment::Client(cid) => cid,
            _ => return None,
        };

        let c = self.world().client(cid);
        if c.pawn_id() != Some(self.id) {
            return None;
        }
        Some(c)
    }

    pub fn child_inventories(&self) -> InventoriesById<E, hash_set::Iter<InventoryId>> {
        InventoriesById::new(self.world(), self.obj().child_inventories.iter())
    }
}

// Mut methods.

impl<'a, E: Ext> ObjectMut<'a, E, Entity> {
    pub fn plane_mut(&mut self) -> ObjectMut<E, Plane> {
        let pid = self.plane_id();
        self.world_mut().plane_mut(pid)
    }

    pub fn controller_mut(&mut self) -> Option<ObjectMut<E, Client>> {
        let eid = self.id;

        let cid = match self.obj().attachment {
            EntityAttachment::Client(cid) => cid,
            _ => return None,
        };

        let c = self.world_mut().client_mut(cid);
        if c.pawn_id() != Some(eid) {
            return None;
        }
        Some(c)
    }

    /// Changes the parent of this entity.  The new parent must refer to an existing object.
    ///
    /// If the entity's current parent is a client, and the entity is the pawn of that client, the
    /// client's pawn will be reset to `None`.  (Note: this happens even when the new attachment is
    /// the same as the old one.)
    pub fn set_attachment(&mut self, attach: EntityAttachment) {
        let id = self.id;
        let old_attach = self.attachment();

        self.world.update.entity_attachment(self.id, old_attach);

        match old_attach {
            EntityAttachment::World => {},
            EntityAttachment::Chunk => {},
            EntityAttachment::Client(cid) => {
                let mut c = self.world_mut().client_mut(cid);
                if c.pawn_id() == Some(id) {
                    c.clear_pawn();
                }
                c.obj_mut().child_entities.remove(&id);
            },
        }

        self.obj_mut().attachment = attach;

        match attach {
            EntityAttachment::World => {},
            EntityAttachment::Chunk => {},
            EntityAttachment::Client(cid) => {
                self.world_mut().client_mut(cid).obj_mut().child_entities.insert(id);
            },
        }
    }
}
