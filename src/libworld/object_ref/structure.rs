use server_types::*;
use std::collections::hash_set;

use Ext;
use ext::Update;
use objects::*;
use object_ref::{ObjectRef, ObjectMut};
use world::InventoriesById;


simple_getters! {
    object_type = Structure;
    obj_ident = obj;

    fn plane_id: PlaneId = obj.plane;
    fn pos: V3 = obj.pos;
    fn template: TemplateId = obj.template;
    fn attachment: StructureAttachment = obj.attachment;
}


// Setters and modifiers

impl<'a, E: Ext> ObjectMut<'a, E, Structure> {
    pub fn set_template(&mut self, template: TemplateId) {
        self.world.update.structure_template(self.id, self.obj().template);
        self.obj_mut().template = template;
    }
}

// Non-mut methods.

impl<'a, E: Ext> ObjectRef<'a, E, Structure> {
    pub fn plane(&self) -> ObjectRef<'a, E, Plane> {
        self.world().plane(self.plane_id())
    }

    pub fn child_inventories(&self) -> InventoriesById<'a, E, hash_set::Iter<'a, InventoryId>> {
        InventoriesById::new(self.world(), self.obj().child_inventories.iter())
    }
}

impl<'a, E: Ext> ObjectMut<'a, E, Structure> {
    pub fn plane(&self) -> ObjectRef<E, Plane> {
        self.world().plane(self.plane_id())
    }

    pub fn child_inventories(&self) -> InventoriesById<E, hash_set::Iter<InventoryId>> {
        InventoriesById::new(self.world(), self.obj().child_inventories.iter())
    }
}

// Mut methods.

impl<'a, E: Ext> ObjectMut<'a, E, Structure> {
    pub fn plane_mut(&mut self) -> ObjectMut<E, Plane> {
        let pid = self.plane_id();
        self.world_mut().plane_mut(pid)
    }

    // TODO: move_to(PlaneId, V3)

    /// Changes the parent of this structure.  The new parent must refer to an existing object.
    pub fn set_attachment(&mut self, attach: StructureAttachment) {
        let old_attach = self.attachment();
        let id = self.id;
        let plane = self.plane_id();
        let cpos = self.pos().reduce().div_floor(CHUNK_SIZE);

        self.world.update.structure_attachment(self.id, old_attach);

        match old_attach {
            StructureAttachment::Plane => {},
            StructureAttachment::Chunk => {
                let mut p = self.world_mut().plane_mut(plane);
                let mut chunk = p.chunk_mut(cpos).unwrap();
                chunk.obj_mut().child_structures.remove(&id);
            },
        }

        self.obj_mut().attachment = attach;

        match attach {
            StructureAttachment::Plane => {},
            StructureAttachment::Chunk => {
                let mut p = self.world_mut().plane_mut(plane);
                let mut chunk = p.chunk_mut(cpos).unwrap();
                chunk.obj_mut().child_structures.insert(id);
            },
        }
    }
}
