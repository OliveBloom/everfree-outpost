use server_types::*;
use common::util::GridOrdered;

use Ext;
use objects::*;
use object_ref::{ObjectRef, ObjectMut};


simple_getters! {
    object_type = Plane;
    obj_ident = obj;
}

// Non-mut methods.

impl<'a, E: Ext> ObjectRef<'a, E, Plane> {
    pub fn chunk_id(&self, cpos: V2) -> Option<TerrainChunkId> {
        self.obj().loaded_chunks.get(&cpos).map(|&x| x)
    }

    pub fn chunk(&self, cpos: V2) -> Option<ObjectRef<'a, E, TerrainChunk>> {
        self.chunk_id(cpos).and_then(|tcid| self.world().get_terrain_chunk(tcid))
    }

    pub fn structures_at(&self, pos: V3) -> impl Iterator<Item=StructureId>+'a {
        self.obj().structures_by_pos.get(GridOrdered(pos)).map(|&x| x)
    }
}

impl<'a, E: Ext> ObjectMut<'a, E, Plane> {
    pub fn chunk_id(&self, cpos: V2) -> Option<TerrainChunkId> {
        self.obj().loaded_chunks.get(&cpos).map(|&x| x)
    }

    pub fn chunk(&self, cpos: V2) -> Option<ObjectRef<E, TerrainChunk>> {
        self.chunk_id(cpos).and_then(|tcid| self.world().get_terrain_chunk(tcid))
    }

    pub fn structures_at<'b>(&'b self, pos: V3) -> impl Iterator<Item=StructureId>+'b {
        self.obj().structures_by_pos.get(GridOrdered(pos)).map(|&x| x)
    }
}

// Mut methods.

impl<'a, E: Ext> ObjectMut<'a, E, Plane> {
    pub fn chunk_mut(&mut self, cpos: V2) -> Option<ObjectMut<E, TerrainChunk>> {
        let &tcid = unwrap_or!(self.obj().loaded_chunks.get(&cpos), return None);
        self.world_mut().get_terrain_chunk_mut(tcid)
    }
}
