use server_types::*;
use std::collections::{HashMap, HashSet};
use common::movement::InputBits;
use common::util::{self, BTreeMultiMap1};
use common::util::GridOrdered;
use server_extra::Extra;

pub use common::activity::Activity;
pub use common::appearance::{Appearance, AppearanceFlags};
pub use common::appearance::{APP_WING, APP_HORN, APP_MALE, APP_LIGHT};
pub use server_world_types::*;
pub use server_world_types::flags::*;



#[derive(Clone, Debug)]
pub struct Client {
    pub name: String,
    /// *Invariant*: If `pawn` is `Some(eid)`, then entity `eid` exists and is a child of this
    /// client.
    pub pawn: Option<EntityId>,
    pub current_input: InputBits,

    pub extra: Extra,
    pub stable_id: StableId,
    pub flags: ClientFlags,
    pub child_entities: HashSet<EntityId>,
    pub child_inventories: HashSet<InventoryId>,
}
impl_IntrusiveStableId!(Client, stable_id);

impl Client {
    pub fn new(uid: u32, name: &str) -> Client {
        Client {
            name: name.to_owned(),
            pawn: None,
            current_input: InputBits::empty(),

            extra: Extra::new(),
            stable_id: uid as StableId,
            flags: ClientFlags::empty(),
            child_entities: HashSet::new(),
            child_inventories: HashSet::new(),
        }
    }

    pub fn pre_insert(mut c: Client) -> Client {
        c.pawn = None;

        if c.child_entities.len() > 0 {
            c.child_entities = HashSet::new();
        }
        if c.child_inventories.len() > 0 {
            c.child_inventories = HashSet::new();
        }

        c
    }

    pub fn stable_id(&self) -> Option<Stable<ClientId>> {
        Stable::new_checked(self.stable_id)
    }
}

impl Default for Client {
    fn default() -> Client {
        Client::new(0, "")
    }
}


#[derive(Clone, Debug)]
pub struct Entity {
    /// *Invariant*: `plane` refers to a loaded plane.
    pub plane: PlaneId,

    pub activity: Activity,
    pub activity_start: Time,
    pub appearance: Appearance,

    pub extra: Extra,
    pub stable_id: StableId,
    pub flags: EntityFlags,
    pub attachment: EntityAttachment,
    pub child_inventories: HashSet<InventoryId>,
}
impl_IntrusiveStableId!(Entity, stable_id);

impl Entity {
    pub fn new(plane: PlaneId, pos: V3, dir: u8, appearance: Appearance) -> Entity {
        Entity {
            plane: plane,

            activity: Activity::Stand { pos, dir },
            activity_start: 0,
            appearance: appearance,

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
            flags: EntityFlags::empty(),
            attachment: EntityAttachment::World,
            child_inventories: HashSet::new(),
        }
    }

    pub fn pre_insert(mut e: Entity) -> Entity {
        if e.child_inventories.len() > 0 {
            e.child_inventories = HashSet::new();
        }

        e
    }

    pub fn stable_id(&self) -> Option<Stable<EntityId>> {
        Stable::new_checked(self.stable_id)
    }
}

impl Default for Entity {
    fn default() -> Entity {
        Entity::new(PlaneId(0), scalar(0), 0, Appearance::default())
    }
}


#[derive(Clone, Debug)]
pub struct Inventory {
    // Inventory size (number of slots) is capped at 255
    pub contents: Box<[Item]>,

    pub extra: Extra,
    pub stable_id: StableId,
    pub flags: InventoryFlags,
    pub attachment: InventoryAttachment,
}
impl_IntrusiveStableId!(Inventory, stable_id);

impl Inventory {
    pub fn new(size: u8) -> Inventory {
        Inventory {
            contents: util::make_array(Item::none(), size as usize),

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
            flags: InventoryFlags::empty(),
            attachment: InventoryAttachment::World,
        }
    }

    pub fn pre_insert(i: Inventory) -> Inventory {
        i
    }

    pub fn stable_id(&self) -> Option<Stable<InventoryId>> {
        Stable::new_checked(self.stable_id)
    }
}

impl Default for Inventory {
    fn default() -> Inventory {
        Inventory::new(0)
    }
}


#[derive(Clone, Debug)]
pub struct Plane {
    pub loaded_chunks: HashMap<V2, TerrainChunkId>,
    pub structures_by_pos: BTreeMultiMap1<GridOrdered<V3>, StructureId>,

    pub extra: Extra,
    pub stable_id: StableId,
    pub flags: PlaneFlags,
}
impl_IntrusiveStableId!(Plane, stable_id);

impl Plane {
    pub fn new() -> Plane {
        Plane {
            loaded_chunks: HashMap::new(),
            structures_by_pos: BTreeMultiMap1::new(),

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
            flags: PlaneFlags::empty(),
        }
    }

    pub fn pre_insert(mut p: Plane) -> Plane {
        if p.loaded_chunks.len() > 0 {
            p.loaded_chunks = HashMap::new();
        }
        if p.structures_by_pos.len() > 0 {
            p.structures_by_pos.clear();
        }

        p
    }

    pub fn stable_id(&self) -> Option<Stable<PlaneId>> {
        Stable::new_checked(self.stable_id)
    }
}

impl Default for Plane {
    fn default() -> Plane {
        Plane::new()
    }
}


#[derive(Clone, Debug)]
pub struct TerrainChunk {
    /// *Invariant*: `plane` always refers to a loaded plane.
    pub plane: PlaneId,
    pub cpos: V2,
    pub blocks: Box<BlockChunk>,

    pub extra: Extra,
    pub stable_id: StableId,
    pub flags: TerrainChunkFlags,
    pub child_structures: HashSet<StructureId>,
}
impl_IntrusiveStableId!(TerrainChunk, stable_id);

impl TerrainChunk {
    pub fn new(plane: PlaneId, cpos: V2) -> TerrainChunk {
        TerrainChunk {
            plane: plane,
            cpos: cpos,
            blocks: Box::new(PLACEHOLDER_CHUNK),

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
            flags: TerrainChunkFlags::empty(),
            child_structures: HashSet::new(),
        }
    }

    pub fn pre_insert(mut tc: TerrainChunk) -> TerrainChunk {
        if tc.child_structures.len() > 0 {
            tc.child_structures = HashSet::new();
        }

        tc
    }

    pub fn stable_id(&self) -> Option<Stable<TerrainChunkId>> {
        Stable::new_checked(self.stable_id)
    }
}

impl Default for TerrainChunk {
    fn default() -> TerrainChunk {
        TerrainChunk::new(PlaneId(0), scalar(0))
    }
}


#[derive(Clone, Debug)]
pub struct Structure {
    /// *Invariant*: `plane` always refers to a loaded plane.
    pub plane: PlaneId,
    pub pos: V3,
    pub template: TemplateId,

    pub extra: Extra,
    pub stable_id: StableId,
    pub flags: StructureFlags,
    pub attachment: StructureAttachment,
    pub child_inventories: HashSet<InventoryId>,
}
impl_IntrusiveStableId!(Structure, stable_id);

impl Structure {
    pub fn new(plane: PlaneId, pos: V3, template: TemplateId) -> Structure {
        Structure {
            plane: plane,
            pos: pos,
            template: template,

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
            flags: StructureFlags::empty(),
            attachment: StructureAttachment::Chunk,
            child_inventories: HashSet::new(),
        }
    }

    pub fn pre_insert(mut s: Structure) -> Structure {
        if s.child_inventories.len() > 0 {
            s.child_inventories = HashSet::new();
        }

        s
    }

    pub fn stable_id(&self) -> Option<Stable<StructureId>> {
        Stable::new_checked(self.stable_id)
    }
}

impl Default for Structure {
    fn default() -> Structure {
        Structure::new(PlaneId(0), scalar(0), 0)
    }
}

