#[allow(unused)] use server_types::*;
use std::collections::BTreeSet;
use std::marker::PhantomData;
use std::mem;
use std::ops::{Index, IndexMut};
use std::slice;


#[derive(Clone)]
pub struct IdMap<K, V> {
    /// The values currently stored in this map.
    map: Vec<Option<V>>,

    /// Keys that have been removed, but not yet recycled.  These keys won't be reused for new
    /// items until `recycle_ids` is called.
    ///
    /// Invariant: each key in `to_recycle` corresponds to a `None` slot in `map`.  (And that slot
    /// must exist, so `map.len()` must be greater than the greatest key in `to_recycle`.)
    to_recycle: BTreeSet<K>,

    /// Keys for empty slots in `map` that are safe to reuse.
    ///
    /// Invariant: each key in `free` corresponds to a `None` slot in `map`.
    free: BTreeSet<K>,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub struct Reserved<K>(pub K);

impl<K: Copy+Ord+From<usize>+Into<usize>, V> IdMap<K, V> {
    pub fn new() -> IdMap<K, V> {
        IdMap {
            map: Vec::new(),
            to_recycle: BTreeSet::new(),
            free: BTreeSet::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.map.len() - self.free.len()
    }

    pub fn next_id(&self) -> K {
        if self.free.is_empty() {
            let rk = self.map.len();
            K::from(rk)
        } else {
            // nth(0) must be Some because the container is non-empty.
            *self.free.iter().nth(0).unwrap()
        }
    }

    pub fn next_n_ids(&mut self, n: usize) -> Vec<K> {
        if n == 0 {
            return Vec::new();
        }

        let mut v = Vec::with_capacity(n);

        for &k in &self.free {
            v.push(k);
            if v.len() == n {
                break;
            }
        }

        for raw in self.map.len() .. self.map.len() + (n - v.len()) {
            v.push(K::from(raw))
        }

        v
    }

    pub fn insert_as(&mut self, k: K, v: V) -> bool {
        let rk: usize = k.into();

        if rk >= self.map.len() {
            let extra = rk + 1 - self.map.len();
            self.map.reserve(extra);
            // Note the while loop uses `>`, so we stop before pushing the `rk`'th element.
            while rk > self.map.len() {
                self.map.push(None);
            }
            self.map.push(Some(v));
            true
        } else {
            if !self.map[rk].is_none() {
                return false;
            }
            self.map[rk] = Some(v);
            self.free.remove(&k);
            true
        }
    }

    pub fn insert(&mut self, v: V) -> K {
        let k = self.next_id();
        let ok = self.insert_as(k, v);
        assert!(ok);
        k
    }

    pub fn remove(&mut self, k: K) -> Option<V> {
        let rk: usize = k.into();
        if rk >= self.map.len() {
            return None;
        }
        let result = self.map[rk].take();
        if result.is_some() {
            self.to_recycle.insert(k);
        }
        result
    }

    pub fn contains(&self, k: K) -> bool {
        match self.map.get(k.into()) {
            None => false,
            Some(&None) => false,
            Some(&Some(_)) => true,
        }
    }

    pub fn get(&self, k: K) -> Option<&V> {
        // get(rk) returns Option<&Option<V>>.  The outer is None if `k` is out of bounds, and the
        // inner is None if `k` is in bounds but unoccupied.
        match self.map.get(k.into()) {
            None => None,
            Some(&None) => None,
            Some(&Some(ref v)) => Some(v),
        }
    }

    pub fn get_mut(&mut self, k: K) -> Option<&mut V> {
        match self.map.get_mut(k.into()) {
            None => None,
            Some(&mut None) => None,
            Some(&mut Some(ref mut v)) => Some(v),
        }
    }

    /// Obtain `n` reserved IDs.  These IDs can be passed to `insert_reserved` to insert a value
    /// into the map under a known key.
    ///
    /// This is useful for loading save files, where the eventual ID of each loaded object must be
    /// known before the objects themselves have been constructed.
    ///
    /// Note that this function simply produces a list of unused IDs.  It does not mark the
    /// returned IDs as "used" in any way.  Correct usage is to call `reserve_ids`, then call
    /// `insert_reserved` with each returned ID at most once.  Inserting elements any other way
    /// (with `insert`, or with `insert_reserved` using IDs returned by a different `reserve_ids`
    /// call) will likely invalidate the reserved IDs, causing `insert_reserved` to fail.
    pub fn reserve_ids(&mut self, n: usize) -> Vec<Reserved<K>> {
        self.next_n_ids(n).into_iter().map(|k| Reserved(k)).collect()
    }

    /// Insert a value into the map under a known, reserved ID.  See `reserve_ids` for details.
    pub fn insert_reserved(&mut self, k: Reserved<K>, v: V) -> K {
        let ok = self.insert_as(k.0, v);
        assert!(ok);
        k.0
    }

    /// Free up IDs of recently removed items so they can be reused.
    ///
    /// Note: this method has no relation to `reserve_ids`.
    pub fn recycle_ids(&mut self) {
        let ids = mem::replace(&mut self.to_recycle, BTreeSet::new());

        // If recycling will free up the last entry in `self.map`, then we can pop that last entry,
        // along with any consecutive unused entries that precede it.
        let should_compact = ids.iter().rev().nth(0)
            .map_or(false, |&k| <K as Into<usize>>::into(k) == self.map.len() - 1);

        for k in ids {
            self.free.insert(k);
        }

        if should_compact {
            while self.map.last().map_or(false, |opt_v| opt_v.is_none()) {
                self.map.pop();
            }
            // Remove all elements >= `map.len()` from `free`
            let k = K::from(self.map.len());
            drop(self.free.split_off(&k));
        }

        // After recycling and compaction, there's no reason to allow extra `None` entries at the
        // end of `map`.
        debug_assert!(self.map.last().map_or(true, |opt| opt.is_some()));

        // Sanity check: do `free` and `map` have the same idea about how many items are present?
        debug_assert!({
            let count: usize = self.map.iter().map(|opt| opt.is_some() as usize).sum();
            count == self.len()
        });
    }

    pub fn iter(&self) -> Iter<K, V> {
        Iter {
            idx: 0,
            iter: self.map.iter(),
            _marker: PhantomData,
        }
    }
}

impl<K: Copy+Ord+From<usize>+Into<usize>, V> Index<K> for IdMap<K, V> {
    type Output = V;

    fn index(&self, index: K) -> &V {
        self.get(index).expect("no entry found for key")
    }
}

impl<K: Copy+Ord+From<usize>+Into<usize>, V> IndexMut<K> for IdMap<K, V> {
    fn index_mut(&mut self, index: K) -> &mut V {
        self.get_mut(index).expect("no entry found for key")
    }
}

pub struct Iter<'a, K, V: 'a> {
    idx: usize,
    iter: slice::Iter<'a, Option<V>>,
    _marker: PhantomData<K>,
}

impl<'a, K: Copy+Ord+From<usize>+Into<usize>, V> Iterator for Iter<'a, K, V> {
    type Item = (K, &'a V);
    fn next(&mut self) -> Option<(K, &'a V)> {
        let mut result = None;
        for opt_ref in &mut self.iter {
            self.idx += 1;
            if let Some(ref x) = *opt_ref {
                let k = K::from(self.idx - 1);
                result = Some((k, x));
                break;
            }
        }
        result
    }
}
