use proc_macro::{TokenStream, TokenTree, Span, Literal, Ident};

pub fn ident_str(input: TokenStream) -> TokenStream {
    let mut ident = Ident::new("_", Span::call_site());

    let mut iter = input.into_iter();
    match iter.next() {
        Some(TokenTree::Ident(i)) => {
            ident = i;
        },
        Some(tt) => {
            tt.span().error("expected an identifier").emit();
        },
        None => {
            Span::call_site().error("expected an identifier argument").emit();
        },
    }
    if let Some(tt) = iter.next() {
        tt.span().error("unexpected tokens after identifier").emit();
    }

    let mut tt = TokenTree::Literal(Literal::string(&ident.to_string()));
    tt.set_span(ident.span());
    tt.into()
}
