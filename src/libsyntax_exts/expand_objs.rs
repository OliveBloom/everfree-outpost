use std::collections::HashMap;
use std::ptr;
use std::str::{self, FromStr};
use std::sync::{Mutex, MutexGuard};
use std::sync::atomic::{AtomicPtr, Ordering};
use std::usize;

use proc_macro::{TokenStream, Span};
use proc_macro::{TokenTree, Group, Ident, Delimiter};

use util::{TtBuf, TtCurs, TtResult};


#[derive(Clone, Debug)]
struct ObjName<'a> {
    camel: &'a str,
    snake: &'a str,
    caps: &'a str,
    camel_plur: &'a str,
    snake_plur: &'a str,
    caps_plur: &'a str,
    abbr: &'a str,
    args: &'a HashMap<String, String>,
}

#[derive(Debug)]
struct OwnedObjName {
    camel: String,
    snake: String,
    caps: String,
    camel_plur: String,
    snake_plur: String,
    caps_plur: String,
    abbr: String,
    args: HashMap<String, String>,
}

impl OwnedObjName {
    fn new() -> OwnedObjName {
        OwnedObjName {
            camel: String::new(),
            snake: String::new(),
            caps: String::new(),
            camel_plur: String::new(),
            snake_plur: String::new(),
            caps_plur: String::new(),
            abbr: String::new(),
            args: HashMap::new(),
        }
    }

    fn borrow(&self) -> ObjName {
        ObjName {
            camel: &self.camel,
            snake: &self.snake,
            caps: &self.caps,
            camel_plur: &self.camel_plur,
            snake_plur: &self.snake_plur,
            caps_plur: &self.caps_plur,
            abbr: &self.abbr,
            args: &self.args,
        }
    }
}


/// Persistent state across macro invocations.
pub struct ExpandObjsState {
    name_lists: HashMap<String, Vec<OwnedObjName>>,
}

impl ExpandObjsState {
    pub fn new() -> ExpandObjsState {
        ExpandObjsState {
            name_lists: HashMap::new(),
        }
    }
}


#[derive(Clone, Copy)]
struct NameLists<'a> {
    default: Option<&'a [OwnedObjName]>,
    other: &'a HashMap<String, Vec<OwnedObjName>>,
}


// Input format

#[derive(Clone, Debug)]
enum Chunk {
    Plain(TokenTree),
    Delim(Delimiter, Span, Vec<Chunk>),
    /// `$foo`
    Var(Ident),
    /// `$( ... )*`
    Rep(Option<Ident>, Span, Vec<Chunk>),
}

#[derive(Debug)]
enum NameSource {
    None,
    Ref(Ident),
    Inline(Vec<OwnedObjName>),
}

#[derive(Debug)]
struct ExpandArgs {
    names: NameSource,
    body: Vec<Chunk>,
}

#[derive(Debug)]
struct DefineArgs {
    key: String,
    names: Vec<OwnedObjName>,
}


// Input parsing

/// Parse a double-bracketed name-list reference: `[[foo]]`.
fn parse_name_list_ref(c: TtCurs) -> TtResult<(Ident, TtCurs)> {
    let (inner1, c) = c.delimited(Delimiter::Bracket)?;
    let (inner2, inner1) = inner1.curs().delimited(Delimiter::Bracket)?;
    let (id, inner2) = inner2.curs().ident()?;
    inner1.eof()?;
    inner2.eof()?;
    Ok((id.clone(), c))
}

fn parse_chunk(c: TtCurs, marker: char) -> TtResult<(Chunk, TtCurs)> {
    if let Ok((g, c)) = c.group() {
        let buf = TtBuf::new(g.stream(), g.span());
        let (chunks, _inner) = buf.curs().until_eof(move |c| parse_chunk(c, marker))?;
        return Ok((Chunk::Delim(g.delimiter(), g.span(), chunks), c));
    } else if let Ok((sp1, c)) = c.exact_punct_span(marker) {
        if let Ok((inner, c)) = c.delimited(Delimiter::Parenthesis) {
            if let Ok((sp2, c)) = c.exact_punct_span('*') {
                let (opt_ref, inner) = inner.curs().optional(parse_name_list_ref);
                let (chunks, _inner) = inner.until_eof(move |c| parse_chunk(c, marker))?;
                let sp = sp1.join(sp2).unwrap_or(sp1);
                return Ok((Chunk::Rep(opt_ref, sp, chunks), c));
            }
        } else if let Ok((id, c)) = c.ident() {
            return Ok((Chunk::Var(id.clone()), c));
        }
    }

    let (tt, c) = c.token_tree()?;
    Ok((Chunk::Plain(tt.clone()), c))
}

fn to_snake_plur(snake: &str) -> String {
    let mut snake_plur = String::with_capacity(1 + snake.len());
    snake_plur.push_str(snake);
    snake_plur.push('s');
    snake_plur
}

fn to_camel(snake: &str) -> String {
    let mut camel = String::with_capacity(snake.len());
    let mut start_word = true;
    for c in snake.chars() {
        if c == '_' {
            start_word = true;
        } else {
            if start_word {
                camel.extend(c.to_uppercase());
            } else {
                camel.push(c);
            }
            start_word = false;
        }
    }
    camel
}

fn to_abbr(snake: &str) -> String {
    let mut abbr = String::with_capacity(snake.len());
    let mut start_word = true;
    for c in snake.chars() {
        if c == '_' {
            start_word = true;
        } else {
            if start_word {
                abbr.extend(c.to_uppercase());
            }
            start_word = false;
        }
    }
    abbr
}

/// `{ name1; name2, foo = bar; ... }`
fn parse_name_list(c: TtCurs) -> TtResult<(Vec<OwnedObjName>, TtCurs)> {
    let (inner, c) = c.delimited(Delimiter::Brace)?;
    let (names, _inner) = inner.curs().until_eof(parse_name_list_entry)?;
    Ok((names, c))
}

/// `name, plur_name, key1 = val1, key2 = val2;`
fn parse_name_list_entry(c: TtCurs) -> TtResult<(OwnedObjName, TtCurs)> {
    let (base, base_val, c) = parse_ident_opt_val(c)?;
    if let Some(tt) = base_val {
        tt.span().error("initial name must not have a value").emit();
    }

    let mut plur: Option<&Ident> = None;
    let mut args: HashMap<String, String> = HashMap::new();

    let mut c = c;
    loop {
        if let Ok(new_c) = c.exact_punct(',') {
            c = new_c;
        } else if let Ok(new_c) = c.exact_punct(';') {
            c = new_c;
            break;
        } else {
            return Err(c.error("expected ',', ';', or '='"));
        }

        let (id, val, new_c) = parse_ident_opt_val(c)?;
        c = new_c;

        if let Some(val) = val {
            let key = id.to_string();
            if args.contains_key(&key) {
                id.span().error(format!("duplicate entry for argument {:?}", key)).emit();
            }
            args.insert(key, val.to_string());
        } else {
            if plur.is_some() {
                id.span().error("argument is missing a value").emit();
            } else if args.len() > 0 {
                id.span().error("plural name must come before all arguments").emit();
            } else {
                plur = Some(id);
            }
        }
    }

    let mut name = OwnedObjName::new();
    name.snake = base.to_string();
    if let Some(plur) = plur {
        name.snake_plur = plur.to_string();
    } else {
        name.snake_plur = to_snake_plur(&name.snake);
    }

    name.camel = to_camel(&name.snake);
    name.camel_plur = to_camel(&name.snake_plur);
    name.caps = name.snake.to_uppercase();
    name.caps_plur = name.snake_plur.to_uppercase();
    name.abbr = to_abbr(&name.snake);

    name.args = args;

    Ok((name, c))
}

/// `foo` or `foo = val`.
fn parse_ident_opt_val(c: TtCurs) -> TtResult<(&Ident, Option<&TokenTree>, TtCurs)> {
    let (id, c) = c.ident()?;
    if let Ok(c) = c.exact_punct('=') {
        let (tt, c) = c.token_tree()?;
        Ok((id, Some(tt), c))
    } else {
        Ok((id, None, c))
    }
}

fn parse_expand_args(c: TtCurs, marker: char) -> TtResult<ExpandArgs> {
    let (names, c) = if let Ok((ref_key, c)) = parse_name_list_ref(c) {
        (NameSource::Ref(ref_key), c)
    } else if let Ok((list, c)) = parse_name_list(c) {
        (NameSource::Inline(list), c)
    } else {
        (NameSource::None, c)
    };

    let (body, c) = c.until_eof(move |c| parse_chunk(c, marker))?;
    c.eof()?;

    Ok(ExpandArgs { names, body })
}

fn parse_define_args(c: TtCurs) -> TtResult<DefineArgs> {
    let (key, c) = c.ident()?;
    let c = c.exact_punct('=')?;
    let (names, c) = parse_name_list(c)?;
    c.eof()?;
    Ok(DefineArgs { key: key.to_string(), names })
}


// Rewriting and output generation

fn rewrite(cs: Vec<Chunk>,
           names: NameLists) -> Vec<TokenTree> {
    let mut tts = Vec::new();

    for c in cs {
        match c {
            Chunk::Plain(tt) => tts.push(tt),

            Chunk::Delim(d, sp, sub_cs) => {
                let mut sub_tts = rewrite(sub_cs, names);
                let mut g = Group::new(d, sub_tts.into_iter().collect());
                g.set_span(sp);
                tts.push(TokenTree::Group(g));
            },

            Chunk::Var(id) => {
                id.span().error("vars may appear only inside repetitions").emit();
                tts.push(TokenTree::Ident(id));
            },

            Chunk::Rep(opt_ref_key, sp, sub_cs) => {
                let names = if let Some(key) = opt_ref_key {
                    match names.other.get(&key.to_string()) {
                        Some(x) => x as &[_],
                        None => {
                            key.span().error(format!("unknown name list `{}`", key)).emit();
                            &[]
                        },
                    }
                } else if let Some(default) = names.default {
                    default
                } else {
                    sp.error("no name list provided for repetition and no default is avaliable")
                        .emit();
                    &[]
                };

                for name in names {
                    tts.extend(rewrite_rep(sub_cs.clone(), name.borrow()));
                }
            },
        }
    }

    tts
}

fn set_span_deep(ts: TokenStream, sp: Span) -> TokenStream {
    let mut out_ts = Vec::new();
    for mut t in ts {
        if let TokenTree::Group(ref mut g) = t {
            *g = Group::new(g.delimiter(), set_span_deep(g.stream(), sp));
        }
        t.set_span(sp);
        out_ts.push(t);
    }
    out_ts.into_iter().collect()
}

fn rewrite_rep(cs: Vec<Chunk>,
               name: ObjName) -> Vec<TokenTree> {
    let mut tts = Vec::new();

    for c in cs {
        match c {
            Chunk::Plain(tt) => tts.push(tt),

            Chunk::Delim(d, sp, sub_cs) => {
                let mut sub_tts = rewrite_rep(sub_cs, name.clone());
                let mut g = Group::new(d, sub_tts.into_iter().collect());
                g.set_span(sp);
                tts.push(TokenTree::Group(g));
            },

            Chunk::Var(id) => {
                let id_str = id.to_string();
                if let Some(tt_str) = name.args.get(&id_str) {
                    let ts = TokenStream::from_str(tt_str).unwrap();
                    tts.extend(set_span_deep(ts, id.span()));
                } else {
                    let new_id_str = rewrite_str(&id_str, &name);
                    tts.push(TokenTree::Ident(Ident::new(&new_id_str, id.span())));
                }
            },

            Chunk::Rep(_, sp, _) => {
                sp.error("repetitions must not be nested inside repetitions").emit();
            },
        }
    }

    tts
}

fn char_at(s: &str, off: usize) -> Option<char> {
    s.get(off..).and_then(|s| s.chars().nth(0))
}

fn not_present<F: FnOnce(char) -> bool>(s: &str, off: usize, f: F) -> bool {
    match char_at(s, off) {
        None => true,
        Some(c) => !f(c),
    }
}

/// Rewrite an identifier string, using `name` to replace for `obj` and its variants.
fn rewrite_str(s: &str, name: &ObjName) -> String {
    let mut new_s = String::with_capacity(s.len() + 10);

    let mut after_underscore = true;
    let mut skip = 0;
    for (i, c) in s.char_indices() {
        if skip > 0 {
            skip -= 1;
            continue;
        }

        // `obj`, `objs`
        if c == 'o' &&
           after_underscore &&
           s[i..].starts_with("obj") {
            if char_at(s, i + 3) == Some('s') && not_present(s, i + 4, |c| c.is_alphabetic()) {
                new_s.push_str(name.snake_plur);
                skip = 3;
                after_underscore = false;
                continue;
            } else if not_present(s, i + 3, |c| c.is_alphabetic()) {
                new_s.push_str(name.snake);
                skip = 2;
                after_underscore = false;
                continue;
            }
        }

        // `OBJ`, `OBJS`
        if c == 'O' &&
           after_underscore &&
           s[i..].starts_with("OBJ") {
            if char_at(s, i + 3) == Some('S') && not_present(s, i + 4, |c| c.is_alphabetic()) {
                new_s.push_str(name.caps_plur);
                skip = 3;
                after_underscore = false;
                continue;
            } else if not_present(s, i + 3, |c| c.is_alphabetic()) {
                new_s.push_str(name.caps);
                skip = 2;
                after_underscore = false;
                continue;
            }
        }

        // `Obj`, `Objs`
        if c == 'O' &&
           s[i..].starts_with("Obj") {
            if char_at(s, i + 3) == Some('s') && not_present(s, i + 4, |c| c.is_lowercase()) {
                new_s.push_str(name.camel_plur);
                skip = 3;
                after_underscore = false;
                continue;
            } else if not_present(s, i + 3, |c| c.is_lowercase()) {
                new_s.push_str(name.camel);
                skip = 2;
                after_underscore = false;
                continue;
            }
        }

        // `O_`
        if c == 'O' &&
           after_underscore &&
           s[i..].starts_with("O_") {
            new_s.push_str(name.abbr);
            skip = 0;
            after_underscore = false;
            continue;
        }

        if c == '_' {
            after_underscore = true;
        } else {
            after_underscore = false;
        }
        new_s.push(c)
    }

    new_s
}


// Persistent state

static STATE: AtomicPtr<Mutex<ExpandObjsState>> = AtomicPtr::new(ptr::null_mut());

fn get_state_ptr() -> *const Mutex<ExpandObjsState> {
    let ptr = STATE.load(Ordering::Acquire);
    if !ptr.is_null() {
        return ptr;
    }

    let b = Box::new(Mutex::new(ExpandObjsState::new()));
    let bp = Box::into_raw(b);
    let old = STATE.compare_and_swap(ptr::null_mut(), bp, Ordering::AcqRel);
    if old.is_null() {
        // CAS succeeded, so `bp` is the new STATE pointer.  We don't need to free it.
        bp
    } else {
        // CAS failed - some other thread set the new STATE pointer.  Free `bp`, then do another
        // read.
        unsafe { drop(Box::from_raw(bp)) };
        let ptr = STATE.load(Ordering::Acquire);
        assert!(!ptr.is_null());
        ptr
    }
}

fn lock_state() -> MutexGuard<'static, ExpandObjsState> {
    let ptr = get_state_ptr();
    unsafe { (*ptr).lock().unwrap() }
}


// Main entry points

pub fn expand_objs_named(ts: TokenStream, marker: char) -> TokenStream {
    let buf = TtBuf::new(ts, Span::call_site());
    let args = match parse_expand_args(buf.curs(), marker) {
        Ok(x) => x,
        Err(e) => {
            e.emit();
            return TokenStream::new();
        },
    };

    let state = lock_state();

    let mut names = NameLists {
        default: None,
        other: &state.name_lists,
    };
    match args.names {
        NameSource::None => {},
        NameSource::Ref(key) => {
            match names.other.get(&key.to_string()) {
                Some(x) => {
                    names.default = Some(x);
                },
                None => {
                    key.span().error(format!("unknown name list `{}`", key)).emit();
                    names.default = Some(&[]);
                },
            }
        },
        NameSource::Inline(ref ns) => {
            names.default = Some(ns);
        },
    }

    rewrite(args.body, names).into_iter().collect()
}

pub fn for_each_obj_named(ts: TokenStream, marker: char) -> TokenStream {
    let buf = TtBuf::new(ts, Span::call_site());
    let args = match parse_expand_args(buf.curs(), marker) {
        Ok(x) => x,
        Err(e) => {
            e.emit();
            return TokenStream::new();
        },
    };

    let state = lock_state();

    let name_list = match args.names {
        NameSource::None => {
            Span::call_site().error("must provide a name list to iterate over").emit();
            &[]
        },
        NameSource::Ref(key) => {
            match state.name_lists.get(&key.to_string()) {
                Some(x) => x as &[_],
                None => {
                    key.span().error(format!("unknown name list `{}`", key)).emit();
                    &[]
                },
            }
        },
        NameSource::Inline(ref ns) => ns,
    };

    let mut tts = Vec::new();
    for name in name_list {
        tts.extend(rewrite_rep(args.body.clone(), name.borrow()));
    }
    tts.into_iter().collect()
}

pub fn define_expand_objs_list(ts: TokenStream) -> TokenStream {
    let buf = TtBuf::new(ts, Span::call_site());
    let args = match parse_define_args(buf.curs()) {
        Ok(x) => x,
        Err(e) => {
            e.emit();
            return TokenStream::new();
        },
    };

    let mut state = lock_state();

    if state.name_lists.contains_key(&args.key) {
        Span::call_site().error(format!("duplicate name list `{}`", args.key)).emit();
    }
    state.name_lists.insert(args.key, args.names);

    TokenStream::new()
}
