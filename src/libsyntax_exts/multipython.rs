use std::collections::HashMap;

use proc_macro2::{TokenStream, TokenTree, Group, Span};
use syn::{self, Expr, Block, Type, TypeTuple, Path, Generics, FnArg, Ident, ReturnType, Pat};
use syn::fold::{self, Fold};
use syn::parse::{Parse, ParseStream};
use syn::punctuated::Punctuated;
use syn::token;


struct Module {
    name: Option<Ident>,
    builder_name: Option<Ident>,
    generics: Option<Generics>,

    data_type: Option<Type>,
    data_expr: Option<Expr>,
    self_type: Option<Type>,

    conv_module: Option<Path>,

    decl_order: Vec<String>,
    funcs: HashMap<String, Func>,
    getters: HashMap<String, Func>,
    setters: HashMap<String, Func>,
}

struct Func {
    name: String,
    name_ident: Ident,
    args: Vec<FnArg>,
    ret_type: Type,
    body: Block,
    is_static: bool,
}


mod kw {
    custom_keyword!(name);
    custom_keyword!(builder_name);
    custom_keyword!(generics);
    custom_keyword!(data);
    custom_keyword!(self_type);
    custom_keyword!(conv_module);
    custom_keyword!(get);
    custom_keyword!(set);
}

impl Parse for Module {
    fn parse(ps: ParseStream) -> syn::Result<Self> {
        let mut m = Module {
            name: None,
            builder_name: None,
            generics: None,
            data_type: None,
            data_expr: None,
            self_type: None,
            conv_module: None,
            decl_order: Vec::new(),
            funcs: HashMap::new(),
            getters: HashMap::new(),
            setters: HashMap::new(),
        };

        // Handle key/value arguments.
        while !ps.is_empty() {
            macro_rules! check_duplicate {
                ($f:ident, $t:expr) => {
                    if m.$f.is_some() {
                        $t.span.unwrap().error(
                            concat!("duplicate definition for `", stringify!($f), "`"))
                            .emit();
                    }
                };
            }

            if let Ok(t) = ps.parse::<kw::name>() {
                check_duplicate!(name, t);
                ps.parse::<Token![=]>()?;
                m.name = Some(ps.parse()?);

            } else if let Ok(t) = ps.parse::<kw::builder_name>() {
                check_duplicate!(builder_name, t);
                ps.parse::<Token![=]>()?;
                m.builder_name = Some(ps.parse()?);

            } else if let Ok(t) = ps.parse::<kw::generics>() {
                check_duplicate!(generics, t);
                ps.parse::<Token![=]>()?;
                m.generics = Some(ps.parse()?);

            } else if let Ok(t) = ps.parse::<kw::data>() {
                check_duplicate!(data_type, t);
                check_duplicate!(data_expr, t);
                if let Ok(_) = ps.parse::<Token![:]>() {
                    m.data_type = Some(ps.parse()?);
                }
                if let Ok(_) = ps.parse::<Token![=]>() {
                    m.data_expr = Some(ps.parse()?);
                }
                if m.data_type.is_none() && m.data_expr.is_none() {
                    t.span.unwrap().error("empty `data` declaration");
                }

            } else if let Ok(t) = ps.parse::<kw::self_type>() {
                check_duplicate!(self_type, t);
                ps.parse::<Token![=]>()?;
                m.self_type = Some(ps.parse()?);

            } else if let Ok(t) = ps.parse::<kw::conv_module>() {
                check_duplicate!(conv_module, t);
                ps.parse::<Token![=]>()?;
                m.conv_module = Some(ps.parse()?);

            } else {
                break;
            }

            // Each declaration ends with a semicolon.
            ps.parse::<Token![;]>()?;
        }

        // Handle getters, setters, and fns.
        while !ps.is_empty() {
            macro_rules! check_conflict {
                ($func:expr, $dest:ident, [$($conflict:ident),*]) => {
                    if m.$dest.contains_key(&$func.name) {
                        $func.name_ident.span().unwrap().error(format!(
                                "duplicate definition of `{}`", $func.name)).emit();
                    }
                    $(
                        if m.$conflict.contains_key(&$func.name) {
                            $func.name_ident.span().unwrap().error(format!(
                                    concat!(
                                        "conflicting definition of `{}`: ",
                                        "appears in both `", stringify!($dest),
                                        "` and `", stringify!($conflict), "`"),
                                    $func.name)).emit();
                        }
                    )*
                };
            }

            if let Ok(_) = ps.parse::<Token![fn]>() {
                let func = ps.call(parse_func_body)?;
                check_conflict!(func, funcs, [getters, setters]);
                m.decl_order.push(func.name.clone());
                m.funcs.insert(func.name.clone(), func);

            } else if let Ok(_) = ps.parse::<kw::get>() {
                let func = ps.call(parse_func_body)?;
                check_conflict!(func, getters, [funcs]);

                if func.is_static {
                    func.name_ident.span().unwrap()
                        .error("static getters are not supported").emit();
                }

                // When both a setter and getter are present, only add the first one to `decl_order`.
                if !m.setters.contains_key(&func.name) {
                    m.decl_order.push(func.name.clone());
                }
                m.getters.insert(func.name.clone(), func);

            } else if let Ok(_) = ps.parse::<kw::set>() {
                let func = ps.call(parse_func_body)?;
                check_conflict!(func, setters, [funcs]);

                if func.is_static {
                    func.name_ident.span().unwrap()
                        .error("static setters are not supported").emit();
                }

                if !m.getters.contains_key(&func.name) {
                    m.decl_order.push(func.name.clone());
                }
                m.setters.insert(func.name.clone(), func);

            } else {
                ps.error("expected `fn`, `get`, or `set`");
            }
        }

        Ok(m)
    }
}

fn parse_func_body(ps: ParseStream) -> syn::Result<Func> {
    let arg_ps;

    let name_ident = ps.parse::<Ident>()?;
    parenthesized!(arg_ps in ps);
    let args = arg_ps.parse_terminated::<_, Token![,]>(FnArg::parse)?;
    let ret = ps.parse::<ReturnType>()?;
    let body = ps.parse::<Block>()?;

    let is_static = args.first().map_or(true, |a| match *a.value() {
        FnArg::SelfRef(..) | FnArg::SelfValue(..) => false,
        _ => true,
    });

    Ok(Func {
        name: name_ident.to_string(),
        name_ident,
        args: args.into_iter().collect(),
        ret_type: match ret {
            ReturnType::Type(_, t) => *t,
            ReturnType::Default => Type::Tuple(TypeTuple {
                paren_token: token::Paren(Span::call_site()),
                elems: Punctuated::new(),
            }),
        },
        body,
        is_static,
    })
}


// `self_type` rewriting

struct RewriteSelf {
    new_ident: Ident,
    new_type: Type,
}

impl RewriteSelf {
    fn fold_module(&mut self, m: Module) -> Module {
        Module {
            funcs: m.funcs.into_iter()
                .map(|(k,f)| (k, self.fold_func(f)))
                .collect(),
            getters: m.getters.into_iter()
                .map(|(k,f)| (k, self.fold_func(f)))
                .collect(),
            setters: m.setters.into_iter()
                .map(|(k,f)| (k, self.fold_func(f)))
                .collect(),
            .. m
        }
    }

    fn fold_func(&mut self, f: Func) -> Func {
        Func {
            args: f.args.into_iter().map(|a| self.fold_fn_arg(a)).collect(),
            ret_type: self.fold_type(f.ret_type),
            body: self.fold_block(f.body),
            .. f
        }
    }

    fn fold_token_stream(&mut self, ts: TokenStream) -> TokenStream {
        ts.into_iter()
            .map(|tt| match tt {
                TokenTree::Ident(i) => TokenTree::Ident(self.fold_ident(i)),
                TokenTree::Group(g) => {
                    let mut new_g = Group::new(
                        g.delimiter(),
                        self.fold_token_stream(g.stream()));
                    new_g.set_span(g.span());
                    TokenTree::Group(new_g)
                },
                tt => tt,
            })
            .collect()
    }
}

impl Fold for RewriteSelf {
    fn fold_fn_arg(&mut self, a: FnArg) -> FnArg {
        match a {
            FnArg::SelfRef(a) => {
                FnArg::Captured(syn::ArgCaptured {
                    pat: Pat::Ident(syn::PatIdent {
                        by_ref: None,
                        mutability: None,
                        ident: self.new_ident.clone(),
                        subpat: None,
                    }),
                    colon_token: Token![:](Span::call_site()),
                    ty: Type::Reference(syn::TypeReference {
                        and_token: a.and_token,
                        lifetime: None,
                        mutability: a.mutability,
                        elem: Box::new(self.new_type.clone()),
                    }),
                })
            },

            FnArg::SelfValue(a) => {
                FnArg::Captured(syn::ArgCaptured {
                    pat: Pat::Ident(syn::PatIdent {
                        by_ref: None,
                        mutability: a.mutability,
                        ident: self.new_ident.clone(),
                        subpat: None,
                    }),
                    colon_token: Token![:](Span::call_site()),
                    ty: self.new_type.clone(),
                })
            },

            a => fold::fold_fn_arg(self, a),
        }
    }

    fn fold_type(&mut self, t: Type) -> Type {
        fn is_self_type(t: &Type) -> bool {
            let tp = match *t {
                Type::Path(ref tp) => tp,
                _ => return false,
            };
            if tp.qself.is_some() || tp.path.leading_colon.is_some() { return false; }
            let segs = &tp.path.segments;
            if segs.len() != 1 { return false; }
            let seg = segs.iter().nth(0).unwrap();
            match seg.arguments {
                syn::PathArguments::None => {},
                _ => return false,
            }
            seg.ident == "Self"
        }

        if is_self_type(&t) {
            return self.new_type.clone();
        }
        fold::fold_type(self, t)
    }

    fn fold_ident(&mut self, i: Ident) -> Ident {
        if i == "self" {
            return self.new_ident.clone();
        }
        fold::fold_ident(self, i)
    }

    fn fold_macro(&mut self, m: syn::Macro) -> syn::Macro {
        syn::Macro {
            tts: self.fold_token_stream(m.tts),
            .. m
        }
    }
}

fn do_rewrite_self(m: Module) -> Module {
    let new_type =
        if let Some(ty) = m.self_type.clone() { ty }
        else if let Some(ty) = m.data_type.clone() { ty }
        else {
            Span::call_site().unwrap()
                .error("must set either `data_type` or `self_type`").emit();
            return m;
        };
    let new_ident = Ident::new("_self", Span::call_site());

    RewriteSelf { new_type, new_ident }.fold_module(m)
}


// Slot definitions

struct SlotInfo {
    slot_id: &'static str,
    ctype: &'static str,
}

macro_rules! slot {
    ($name:ident, $slot_id:ident, $ctype:ident) => {
        (stringify!($name),
         SlotInfo {
             slot_id: stringify!($slot_id),
             ctype: stringify!($ctype),
         })
    };
}

static SLOTS: &'static [(&'static str, SlotInfo)] = &[
    slot!(__init__, Py_tp_init, initproc),
    slot!(__repr__, Py_tp_repr, reprfunc),
    slot!(__str__, Py_tp_str, reprfunc),
    slot!(__hash__, Py_tp_hash, hashfunc),
    slot!(__eq__, Py_tp_richcompare, richcmpfunc),
    slot!(__cmp__, Py_tp_richcompare, richcmpfunc),

    slot!(__add__, Py_nb_add, binaryfunc),
    slot!(__sub__, Py_nb_subtract, binaryfunc),
    slot!(__mul__, Py_nb_multiply, binaryfunc),
    slot!(__div__, Py_nb_true_divide, binaryfunc),
    slot!(__mod__, Py_nb_remainder, binaryfunc),
    slot!(__neg__, Py_nb_negative, unaryfunc),
];

fn get_slot_info(name: &str) -> Option<&'static SlotInfo> {
    for &(k, ref v) in SLOTS {
        if k == name {
            return Some(v);
        }
    }
    None
}


// Output generation

pub struct OutputRules {
    pub impl_mac: Ident,
    pub builder_name: bool,
    pub data_expr: bool,
    pub generics: bool,
}

impl OutputRules {
    /// An `OutputRules` instance with `impl_mac` named `name` and all optional output flags set to
    /// `false`.
    pub fn named(name: &str) -> OutputRules {
        OutputRules {
            impl_mac: Ident::new(name, Span::call_site()),
            builder_name: false,
            data_expr: false,
            generics: false,
        }
    }
}

macro_rules! unwrap_decl {
    ($e:expr, $f:ident, $sp:expr) => {
        match $e.$f {
            Some(ref x) => x,
            None => {
                $sp.unwrap().error(concat!(
                        "missing definition of `", stringify!($f), "`")).emit();
                return TokenStream::new();
            },
        }
    };
}

fn emit_module(m: &Module, rules: &OutputRules) -> TokenStream {
    let header = emit_header(m, rules);
    let impl_mac = &rules.impl_mac;
    let (funcs, slots, props) = emit_funcs(m);

    quote! {
        #impl_mac! { #header { #funcs } { #slots } { #props } }
    }
}

fn emit_header(m: &Module, rules: &OutputRules) -> TokenStream {
    let mut ts = TokenStream::new();

    let name = unwrap_decl!(m, name, Span::call_site());
    ts.extend(quote! { #name, });

    if rules.builder_name {
        let builder_name = m.builder_name.as_ref().unwrap_or(name);
        ts.extend(quote! { #builder_name, });
    }

    let data_type = unwrap_decl!(m, data_type, name.span());
    ts.extend(quote! { (#data_type), });

    if rules.data_expr {
        let data_expr = unwrap_decl!(m, data_expr, name.span());
        ts.extend(quote! { (#data_expr), });
    }

    let conv_traits = emit_conv_traits(m.conv_module.as_ref());
    ts.extend(quote! { #conv_traits, });

    if rules.generics {
        let (gdef, guse) = emit_generics(m.generics.as_ref());
        ts.extend(quote! { #gdef, #guse, });
    }

    ts
}

fn emit_conv_traits(path: Option<&Path>) -> TokenStream {
    if let Some(path) = path {
        quote! { (#path::Pack, #path::Unpack) }
    } else {
        quote! { () }
    }
}

fn emit_generics(g: Option<&Generics>) -> (TokenStream, TokenStream) {
    if let Some(g) = g {
        let mut ty_vars: Punctuated<_, Token![,]> = Punctuated::new();
        for gp in &g.params {
            match *gp {
                syn::GenericParam::Type(syn::TypeParam { ref ident, .. }) =>
                    ty_vars.push(ident),
                _ => {},
            }
        }

        (quote! { (#g) }, quote! { (#ty_vars) })
    } else {
        (quote! { () }, quote! { () })
    }
}

fn mk_ident(s: &str) -> Ident {
    Ident::new(s, Span::call_site())
}

fn emit_funcs(m: &Module) -> (TokenStream, TokenStream, TokenStream) {
    let mut funcs = TokenStream::new();
    let mut slots = TokenStream::new();
    let mut props = TokenStream::new();

    for name in &m.decl_order {
        if let Some(func) = m.funcs.get(name) {
            let name = &func.name_ident;
            let arg_ts = emit_func_args(&func.args);
            let ret_ty = &func.ret_type;
            let body = &func.body;

            if let Some(slot_info) = get_slot_info(&func.name) {
                let slot_id = mk_ident(slot_info.slot_id);
                let ctype = mk_ident(slot_info.ctype);

                slots.extend(quote! {
                    #name #slot_id #ctype
                    ( #ctype, #name, #slot_id, ( #arg_ts ), ( #ret_ty ), { #body }; )
                });

            } else {
                let kind = if func.is_static { mk_ident("static") } else { mk_ident("method") };

                funcs.extend(quote! {
                    #name ( #kind, #name, ( #arg_ts ), ( #ret_ty ), { #body }; )
                });
            }
        } else {
            let g = m.getters.get(name);
            let s = m.setters.get(name);
            assert!(g.is_some() || s.is_some(),
                    "{:?} is in decl_order but not in funcs", name);

            let kind = mk_ident(
                if g.is_some() && s.is_some() { "get_set" }
                else if g.is_some() { "get" }
                else { "set" }
            );

            let prop_name = &g.or(s).unwrap().name;
            let prop_name_ident = &g.or(s).unwrap().name_ident;
            let gname = Ident::new(&format!("get_{}", prop_name), prop_name_ident.span());
            let sname = Ident::new(&format!("set_{}", prop_name), prop_name_ident.span());

            let mut prop_info = quote! { #kind, #gname, #sname, };

            if let Some(g) = g {
                let arg_ts = emit_func_args(&g.args);
                let ret_ty = &g.ret_type;
                let body = &g.body;
                prop_info.extend(quote! { ( #arg_ts ), ( #ret_ty ), { #body }; });
            }

            if let Some(s) = s {
                let arg_ts = emit_func_args(&s.args);
                let ret_ty = &s.ret_type;
                let body = &s.body;
                prop_info.extend(quote! { ( #arg_ts ), ( #ret_ty ), { #body }; });
            }

            props.extend(quote! { #prop_name_ident ( #prop_info ) });
        }
    }

    (funcs, slots, props)
}

fn emit_func_args(args: &[FnArg]) -> TokenStream {
    let mut out_ts = TokenStream::new();
    for a in args {
        let arg_ts = match a {
            FnArg::SelfRef(a) => {
                let lt = &a.lifetime;
                let mutbl = &a.mutability;
                quote! { self, (& #lt #mutbl Self); }
            },
            FnArg::SelfValue(a) => {
                let mutbl = &a.mutability;
                quote! { (#mutbl self), Self; }
            },
            FnArg::Captured(a) => {
                let pat = &a.pat;
                let ty = &a.ty;
                quote! { (#pat), (#ty); }
            },
            FnArg::Inferred(pat) => {
                quote! { (#pat), _; }
            },
            FnArg::Ignored(ty) => {
                quote! { _, (#ty); }
            },
        };
        out_ts.extend(arg_ts);
    }
    out_ts
}


// Macro implementations

pub fn python_module(input: proc_macro::TokenStream,
                     rules: OutputRules) -> proc_macro::TokenStream {
    let m = parse_macro_input!(input as Module);
    let m = do_rewrite_self(m);
    let output = emit_module(&m, &rules).into();
    //eprintln!("/* output = */ {}", output);
    output
}
