import json
import re
import sys

COMMENT_RE = re.compile(r'//.*$')
START_RE = re.compile(r'pub enum ([A-Za-z0-9_]*) {$')
VARIANT_RE = re.compile(r'([A-Za-z0-9_]*),$')
END_RE = re.compile(r'}$')

def main(in_file, out_file):
    enums = {}
    cur_name = None
    cur_variants = None

    with open(in_file) as f:
        for line in f:
            line = COMMENT_RE.sub('', line).strip()

            if cur_name is None:
                m = START_RE.match(line)
                if m:
                    cur_name = m.group(1)
                    cur_variants = []
            else:
                m = VARIANT_RE.match(line)
                if m:
                    cur_variants.append(m.group(1))

                m = END_RE.match(line)
                if m:
                    assert cur_name not in enums
                    enums[cur_name] = cur_variants
                    cur_name = None
                    cur_variants = None

    with open(out_file, 'w') as f:
        json.dump(enums, f)

if __name__ == '__main__':
    in_file, out_file = sys.argv[1:]
    main(in_file, out_file)

