from outpost_data.lib.consts import *
from outpost_data.lib.mesh import Mesh

def rect(x1, y1, z1, x2, y2, z2):
    return (
        (x1, y1, z2), (x1, y2, z1), (x2, y1, z2),
        (x2, y1, z2), (x1, y2, z1), (x2, y2, z1),
        )

def solid(sx, sy, sz):
    return Mesh(
            # Top
            rect(0, 0, sz, sx, sy, sz) +
            # Front
            rect(0, sy, 0, sx, sy, sz)
            )

def front(sx, sy, sz):
    return Mesh(rect(0, sy, 0, sx, sy, sz))

def bottom(sx, sy):
    return Mesh(rect(0, 0, 0, sx, sy, 0))



