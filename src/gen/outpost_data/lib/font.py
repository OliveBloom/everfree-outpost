from outpost_data.lib.image import Image

import PIL.Image

def split_glyphs(img, palette, inset=(0, 0, 0, 0)):
    '''Split a font image into a list of individual glyphs.  `palette` should
    contain the background color, foreground color, and glyph-start marker
    color, each as an RGB triple.  If the image has an alpha channel, it will
    be pasted over the background color before processing (otherwise there
    would be no way to handle fonts with transparent backgrounds).

    Returns a list of images, each being an RGBA black-on-transparent glyph
    from the font, in the order they appeared in the input image.

    If `inset` is provided, glyph borders will be trimmed by the given amounts.
    This is uesful for removing extra padding that's baked into the font.  The
    order of inset values is `(left, right, top, bottom)`.
    '''

    bg, fg, marker = palette

    def maybe_paste_bg(raw):
        img = PIL.Image.new('RGBA', raw.size)
        img.paste(bg + (255,), (0, 0) + raw.size)
        raw = raw.convert('RGBA')
        img.paste(raw, (0, 0), raw)
        return img
    img = img.modify(maybe_paste_bg,
            desc=(__name__, 'maybe_paste_bg', bg))

    def quantize(raw):
        if raw.mode == 'P':
            return raw
        else:
            return raw.convert('RGB').quantize()
    img = img.modify(quantize)

    def tuple_pal(p):
        result = []
        for i in range(0, len(p), 3):
            result.append(tuple(p[i : i + 3]))
        return result

    def untuple_pal(p):
        return [x for c in p for x in c]

    def find_markers(raw):
        pal = tuple_pal(raw.getpalette())
        c = pal.index(marker)
        line = raw.crop((0, 0, raw.size[0], 1)).tobytes()
        xs = [x for x, b in enumerate(line) if b == c]
        xs.append(raw.size[0])
        return xs
    marker_xs = img.compute(find_markers)

    def map_colors(raw):
        pal = tuple_pal(raw.getpalette())

        # Build the alpha mask
        mask_pal = [(255, 255, 255) if c == fg else (0, 0, 0) for c in pal]
        mask = raw.copy()
        mask.putpalette(untuple_pal(mask_pal))
        mask = mask.convert('L')

        # Apply the mask to an `fg`-colored rectangle
        img = PIL.Image.new('RGB', raw.size)
        img.paste((0, 0, 0), (0, 0) + raw.size)
        img.putalpha(mask)

        return img
    img = img.modify(map_colors)

    glyphs = []
    ix0, ix1, iy0, iy1 = inset
    for x0, x1 in zip(marker_xs, marker_xs[1:]):
        x0 += 1 + ix0
        x1 -= ix1
        y0 = 1 + iy0
        y1 = img.px_size[1] - iy1

        g = img.extract((x0, y0), (x1 - x0, y1 - y0), unit=1)
        glyphs.append(g)

    return glyphs

def make_spans(ks, vs):
    '''Given a list of integer keys and a list of values, produce a sorted list
    of values and a list of `(first_key, first_value_idx, last_value_idx)`
    triples.
    '''

    if len(ks) == 0:
        return [], []
    assert len(ks) == len(vs)

    kvs = sorted(zip(ks, vs), key=lambda kv: kv[0])

    first_idx = 0
    expect_key = kvs[0][0]
    spans = []

    for i, (k, v) in enumerate(kvs):
        if k == expect_key:
            # Add to the current span
            expect_key += 1
        else:
            # Start a new span
            spans.append((kvs[first_idx][0], first_idx, i))
            first_idx = i
            expect_key = k + 1

    spans.append((kvs[first_idx][0], first_idx, len(kvs)))

    return ([v for k, v in kvs], spans)

def build_sheet(glyphs, margin=(0, 0, 0, 0)):
    '''Given a list of `glyphs`, build a sheet containing all the glyphs in
    order.  Returns the sheet image and a list of the x positions of all glyph
    start/end boundaries (including the end of the image).

    If `margin` is set, the glyphs will be padded with transparency before
    constructing the sheet.
    '''

    if len(glyphs) == 0:
        return Image.blank((0, 0))

    raw_h = glyphs[0].px_size[1]
    assert all(g.px_size[1] == raw_h for g in glyphs), \
            'all glyphs must be the same height'

    mx0, mx1, my0, my1 = margin

    x = 0
    y = my0
    img_off = []
    xs = []
    for g in glyphs:
        xs.append(x)
        x += mx0
        img_off.append((g, (x, y)))
        x += g.size[0] + mx1
    xs.append(x)

    w = x
    h = my0 + raw_h + my1

    return (Image.sheet(img_off, (w, h)), xs)

def split_span_by_length(span, xs, max_w):
    first_char, first_glyph, last_glyph = span

    start_idx = first_glyph
    start_x = xs[first_glyph]

    for i in range(first_glyph, last_glyph):
        # If the current glyph would put us over the width limit, break before
        # the current glyph.
        if xs[i + 1] - start_x > max_w:
            yield (first_char + start_idx - first_glyph, start_idx, i)
            start_idx = i
            start_x = xs[i]

    yield (first_char + start_idx - first_glyph, start_idx, last_glyph)


def place_glyphs(atlas, sheet, xs, spans):
    '''Place glyph images into an atlas, returning a list of font spans ready
    for output.
    '''

    max_w = atlas.size()[0]
    h = sheet.px_size[1]
    results = []

    for span in spans:
        for first_char, first_glyph, last_glyph in split_span_by_length(span, xs, max_w):
            x0 = xs[first_glyph]
            x1 = xs[last_glyph]
            span_img = sheet.extract((x0, 0), (x1 - x0, h), unit=1)

            # Be sure to include the one-past-end `x` position, so client code
            # can compute the width of the last glyph.
            span_xs = [x - x0 for x in xs[first_glyph : last_glyph + 1]]

            results.append({
                'first_char': first_char,
                'last_char': first_char + last_glyph - first_glyph,
                'xs': span_xs,
                'image': atlas.place(span_img),
                })

    return results
