from collections import namedtuple

AnyType = namedtuple('AnyType', ())
Any = AnyType()

Optional = namedtuple('Optional', ('ty',))

def check_type(x, ty):
    if ty in (int, float, str, bool):
        return isinstance(x, ty)
    elif isinstance(ty, list):
        elem_ty, = ty
        return isinstance(x, (list, tuple)) and \
                all(check_type(y, elem_ty) for y in x)
    elif isinstance(ty, Optional):
        return x is None or check_type(x, ty.ty)
    elif isinstance(ty, dict):
        return isinstance(x, dict) and \
                all(k in ty for k in x) and \
                all(check_type(x.get(k), field_ty) for k, field_ty in ty.items())
    elif ty is Any:
        return True
    # Tuple case goes near the end so it doesn't catch namedtuple instances
    elif isinstance(ty, tuple):
        return isinstance(x, (list, tuple)) and len(x) == len(ty) and \
                all(check_type(y, field_ty) for y, field_ty in zip(x, ty))
    else:
        return False

def ensure_type(x, ty):
    if not check_type(x, ty):
        raise TypeError('expected %s, but got value %r' % (ty, x))

def typed(dct, k, ty, default=None):
    v = dct.get(k, default)
    if not check_type(v, ty):
        raise TypeError('expected %r to be %s, but got value %r' % (k, ty, v))
    return v
