import argparse
import json
import os
import textwrap
import sys
import yaml

from outpost_data.lib import util
from outpost_data.gen.data.misc import *
from outpost_data.gen.data.structure import conv_structure, process_structures
from outpost_data.gen.data.ui import *


def build_parser():
    args = argparse.ArgumentParser()

    args.add_argument('--atlas-index',
            help='JSON file containing atlas positions of object images')

    args.add_argument('--mode', choices=('game', 'ui'),
            help='which kind of data to process')

    args.add_argument('--json-out-dir',
            help='where to store generated JSON files')

    args.add_argument('--dep-file',
            help='where to write makefile-style dependencies')

    return args


class ObjectType:
    def __init__(self, convert):
        self.objs = {}
        self.convert = convert

    def __call__(self, *args, **kwargs):
        k, v = self.convert(*args, **kwargs)
        assert k not in self.objs, 'duplicate key: %r' % k
        self.objs[k] = v

    def __getitem__(self, k):
        return self.objs[k]

    def objects(self):
        return self.objs.values()

# Stupid hack, needed for depfile output
FIRST_JSON = None

def write_json(args, name, content):
    global FIRST_JSON

    path = os.path.join(args.json_out_dir, '%s.json' % name)

    if FIRST_JSON is None:
        FIRST_JSON = path

    with open(path, 'w') as f:
        json.dump(content, f)


# We support two different modes, "game" and "ui".  Each one handles a
# different set of data types.  Processing .des files under the wrong mode will
# result in errors, since `DATA` will not have the expected fields.

class GameData:
    def __init__(self):
        self.structure = ObjectType(conv_structure)
        self.item = ObjectType(conv_item)
        self.recipe = ObjectType(conv_recipe)
        self.block = ObjectType(conv_block)
        self.terrain = ObjectType(conv_terrain)
        self.sprite = ObjectType(conv_sprite)
        self.pony_layer = ObjectType(conv_pony_layer)
        self.extra = ObjectType(conv_extra)
        self.recipe_crafting_class = None
        self.pony_layer_anim = None

class GameIdMaps:
    def __init__(self, data):
        self.structure = util.assign_ids(data.structure.objs, ('none',))
        self.item = util.assign_ids(data.item.objs, ('none',))
        self.recipe = util.assign_ids(data.recipe.objs, ('none',))
        self.recipe_crafting_class = util.assign_ids(data.recipe_crafting_class)
        self.block = util.assign_ids(data.block.objs, ('empty', 'placeholder'))
        self.terrain = util.assign_ids(data.terrain.objs)
        self.sprite = util.assign_ids(data.sprite.objs)
        self.pony_layer = util.assign_ids(data.pony_layer.objs)
        self.pony_layer_anim = util.assign_ids(data.pony_layer_anim)

        if len(self.recipe_crafting_class) > 32:
            raise ValueError('too many recipe crafting classes - %d > 32' %
                    len(self.recipe_crafting_class))

def game_finish(args, data):
    data.recipe_crafting_class = sorted(set(o.crafting_class
        for o in data.recipe.objects()))
    data.pony_layer_anim = sorted(set(a
            for l in data.pony_layer.objects()
            for a in l.anim_names()))

    id_maps = GameIdMaps(data)

    write_json(args, 'structures', process_structures(data, id_maps))
    write_json(args, 'items', process_items(data, id_maps))
    write_json(args, 'recipes', process_recipes(data, id_maps))
    write_json(args, 'blocks', process_blocks(data, id_maps))
    write_json(args, 'terrains', process_terrains(data, id_maps))
    write_json(args, 'sprites', process_sprites(data, id_maps))
    write_json(args, 'pony_layers', process_pony_layers(data, id_maps))
    write_json(args, 'extras', process_extras(data, id_maps))
    write_json(args, 'recipe_crafting_classes',
            sorted(data.recipe_crafting_class,
                key=lambda k: id_maps.recipe_crafting_class[k]))
    write_json(args, 'pony_layer_anims',
            sorted(data.pony_layer_anim,
                key=lambda k: id_maps.pony_layer_anim[k]))


class UIData:
    def __init__(self):
        self.ui_card = ObjectType(conv_ui_card)
        self.ui_border = ObjectType(conv_ui_border)
        self.font = ObjectType(conv_font)
        self.inventory_layout = ObjectType(conv_inventory_layout)

def ui_finish(args, data):
    write_json(args, 'ui_cards_dict', process_ui_cards(data))
    write_json(args, 'ui_borders_dict', process_ui_borders(data))
    write_json(args, 'fonts_dict', process_fonts(data))
    write_json(args, 'inventory_layouts_dict', process_inventory_layouts(data))


def gen_data(argv, funcs):
    args = build_parser().parse_args(argv)

    if args.mode == 'game':
        Data, finish = GameData, game_finish
    elif args.mode == 'ui':
        Data, finish = UIData, ui_finish

    with open(args.atlas_index) as f:
        index = yaml.load(f)

    # Run the functions
    data = Data()
    for f in funcs:
        f(data, index[f.__name__])

    # Process and write JSON
    finish(args, data)

    # Save dependencies
    deps = util.get_python_deps()
    with open(args.dep_file, 'w') as f:
        f.write('%s: %s\n' % (FIRST_JSON, ' '.join(deps)))
