import argparse
import builtins
import os
import re
import sys
import traceback

import minitemplate
import outpost_data.compiler.parser
from outpost_data.lib import util


def build_parser():
    args = argparse.ArgumentParser()

    args.add_argument('--data-dir', dest='data_dirs', default=[], action='append',
            help='directory to scan for data files')

    args.add_argument('--image-script-out',
            help='where to write image processing script')
    args.add_argument('--data-script-out',
            help='where to write data generation script')
    args.add_argument('--server-script-out',
            help='where to write server-side script')

    args.add_argument('--dep-file',
            help='where to write makefile-style dependencies')

    return args



_DEPS = set()

def read_file(path):
    _DEPS.add(path)
    with open(path) as f:
        return f.read()

def get_deps():
    _DEPS.update(util.get_python_deps())
    return sorted(_DEPS)


def find_files(search_dirs):
    # Keep track of which files we've seen so far.  We only use the first file
    # of each name, so that earlier data directories can override later ones.
    data_files = {}
    template_files = {}

    for base in search_dirs:
        def walk(d):
            _DEPS.add(os.path.join(base, d))
            for f in os.listdir(os.path.join(base, d)):
                name, ext = os.path.splitext(os.path.join(d, f))
                path = os.path.join(base, d, f)

                if os.path.isdir(path):
                    walk(name)
                else:
                    if ext == '.des':
                        if name not in data_files:
                            data_files[name] = path
                    elif ext == '.tmpl':
                        if name not in template_files:
                            template_files[name] = path
                    else:
                        pass

        walk('')

    return data_files, template_files


class Undefined:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        raise NameError('undefined variable in template: %r' % self.name)

    def __repr__(self):
        raise NameError('undefined variable in template: %r' % self.name)

    def __bool__(self):
        return False

class DefaultEnv:
    def __init__(self, dct, default):
        self.dct = dct
        self.default = default

    def __getitem__(self, k):
        if k in self.dct:
            return self.dct[k]
        elif hasattr(builtins, k):
            return getattr(builtins, k)
        else:
            return self.default(k)

    def __setitem__(self, k, v):
        self.dct[k] = v

    def copy(self):
        return DefaultEnv(self.dct.copy(), self.default)

NON_IDENT_RE = re.compile(r'[^a-zA-Z0-9_]+')

def collect_defns(data_files, template_files):
    templates = dict((name, read_file(path)) for name, path in template_files.items())

    raw_defns = []

    counter = 0
    def ident(name):
        nonlocal counter
        s = '%s__%d' % (NON_IDENT_RE.sub('__', name), counter)
        counter += 1
        return s

    for base_name in data_files:
        def _process(name, args):
            if args is None:
                raw_text = read_file(data_files[name])
                text = minitemplate.template(raw_text, {})
            else:
                env = DefaultEnv(args, Undefined)
                text = minitemplate.template(templates[name], env)
            #print(' ===== %s =====' % name)
            #print(text)
            #print(' ===== END %s =====' % name)
            defns = outpost_data.compiler.parser.parse(text.splitlines())

            for d in defns:
                # Arguments listed in `__inherit__` will be copied verbatim
                # from the call site into the expanded definition.  If the list
                # contains only "*", all arguments will be copied.
                if '__inherit__' in d.args:
                    if d.args['__inherit__'].strip() == '*':
                        for k, v in args.items():
                            if k not in d.args:
                                d.args[k] = v
                    else:
                        for k in d.args['__inherit__'].split(','):
                            k = k.strip()
                            if k in args:
                                assert k not in d.args, \
                                        "can't inherit locally-defined arg %r" % k
                                d.args[k] = args[k]

                # For arguments marked `__override__`, `arg` in the expansion
                # can be overridden by setting `override_arg` in the call site.
                if '__override__' in d.args:
                    for k in d.args['__override__'].split(','):
                        k = k.strip()
                        ok = 'override_' + k
                        if ok in args:
                            d.args[k] = args[ok]

                #from pprint import pprint
                #print('expanded args:')
                #pprint(d.args)
                #print('')

                if d.kind == 'raw':
                    # Record the raw definition
                    raw_defns.append((d, base_name, ident(d.name)))
                else:
                    # Expand the indicated template
                    process(d.kind, d.args)

        def process(name, args):
            try:
                return _process(name, args)
            except Exception as e:
                raise RuntimeError('error expanding %s with %r' % (name, args)) from e

        process(base_name, None)

    return raw_defns


def check_defns(defns):
    origin_map = {}
    ok = True
    for d, origin, ident in defns:
        name = d.name
        if name not in origin_map:
            origin_map[name] = origin
        else:
            print('error: duplicate definition %r (from %s and %s)' %
                    (name, origin_map[name], origin))
            ok = False

    return ok


def gen_image_script(defns):
    return minitemplate.template('''
        from outpost_data.lib.consts import *
        from outpost_data.lib.image import load, Image, Anim

        %for defn, origin, ident in defns
        # %{origin}
        def do_%{ident}(ATLAS):
            pass
            %block defn.args.get('gen_preamble', '')
            %block defn.args.get('gen_images_code', '')
        %end

        if __name__ == '__main__':
            from outpost_data.gen import gen_images
            import sys
            gen_images(sys.argv[1:], (
                %for _, _, ident in defns
                do_%{ident},
                %end
            ))
    ''', defns=defns)

def gen_data_script(defns):
    return minitemplate.template('''
        from outpost_data.lib.consts import *
        from outpost_data.lib import mesh_util
        from outpost_data.lib.mesh import Mesh
        from outpost_data.gen.data.structure import \
            DrawNormal, DrawFence, DrawTree, DrawFence16, DrawDoor, DrawTerrain


        %for defn, origin, ident in defns
        # %{origin}
        def do_%{ident}(DATA, IMG):
            pass
            %block defn.args.get('gen_preamble', '')
            %block defn.args.get('gen_data_code', '')
        %end

        if __name__ == '__main__':
            from outpost_data.gen import gen_data
            import sys
            gen_data(sys.argv[1:], (
                %for _, _, ident in defns
                do_%{ident},
                %end
            ))
    ''', defns=defns)

def gen_server_script(defns):
    return minitemplate.template('''
        %for defn, origin, ident in defns
        # %{origin}
        def do_%{ident}():
            pass
            %block defn.args.get('script_preamble', '')
            %block defn.args.get('script_code', '')
        %end

        %for _, _, ident in defns
        do_%{ident}()
        %end
    ''', defns=defns)


def main(argv):
    args = build_parser().parse_args(argv)

    data_files, template_files = find_files(args.data_dirs)
    print(data_files)
    print(template_files)
    print('found %d data files, %d templates' % (len(data_files), len(template_files)))

    defns = collect_defns(data_files, template_files)
    print('collected %d defns' % len(defns))

    if not check_defns(defns):
        sys.exit(1)


    outputs = []

    if args.image_script_out is not None:
        outputs.append(args.image_script_out)
        with open(args.image_script_out, 'w') as f:
            f.write(gen_image_script(defns))

    if args.data_script_out is not None:
        outputs.append(args.data_script_out)
        with open(args.data_script_out, 'w') as f:
            f.write(gen_data_script(defns))

    if args.server_script_out is not None:
        outputs.append(args.server_script_out)
        with open(args.server_script_out, 'w') as f:
            f.write(gen_server_script(defns))

    if args.dep_file is not None:
        outputs.append(args.dep_file)
        with open(args.dep_file, 'w') as f:
            f.write('%s: %s\n' % (outputs[0], ' '.join(get_deps())))


if __name__ == '__main__':
    main(sys.argv[1:])
