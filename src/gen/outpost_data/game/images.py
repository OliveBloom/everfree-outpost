from outpost_data.lib.consts import *
from outpost_data.lib.image import load, Image

def load_structure_image(path, extract_pos, size):
    sx, sy, sz = size

    img = load(path, unit=TILE_SIZE)

    if extract_pos is not None:
        img = img.extract(extract_pos, size=(sx, sy + sz))

    return img

def gen_item_image(img):
    return img.scale((1, 1), unit=ICON_SIZE, smooth=True)

def chop_george_tileset(img):
    '''
    Load a terrain tileset, returning a list of tiles.  The input image should
    use the same format as the terrains in George_'s "16x16 game assets".  As
    extensions, the two bottom-right tiles should contain NE-SW and NW-SE
    diagonal tiles, and the image may contain a fourth row with variants on the
    center tile.
    '''
    img = img.with_unit(TILE_SIZE2)

    grid = [
            [ 4, 12,  8, 11,  7],
            [ 6, 15,  9, 13, 14],
            [ 2,  3,  1, 10,  5],
            ]
    num_tiles = 16

    if img.size[1] == 4:
        # Tileset includes alternate center tiles
        grid.append([16, 17, 18, 19, 20])
        num_tiles += 5

    dct = img.chop_grid(grid)
    dct[0] = Image((1, 1), unit=TILE_SIZE)

    # TODO: check which alternate tiles are actually used (non-empty)
    # (Currently we return all of them, but outpost_data.lib.terrain uses only
    # the first one, if present.)

    return [dct[i] for i in range(num_tiles)]

def load_image_layer(image, layer, unit=1):
    if layer:
        return load(image, unit=unit, layers=True)[layer]
    else:
        return load(image, unit=unit)
