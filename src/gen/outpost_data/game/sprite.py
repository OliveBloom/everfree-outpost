from outpost_data.lib.image import Image, Anim
from outpost_data import lib
import outpost_data.lib.palette

def bounce(base, offsets, rate, oneshot=False):
    frames = [Image.sheet([(base, (0, off))], size=base.size, unit=base.unit)
        for off in offsets]
    return Anim(frames, rate, oneshot)

PONY_BODY_LAYERS = set(('body', 'wing', 'horn'))


def m(x):
    '''Produce a shade of magenta.  Shades of magenta are used to index into
    the shading palette.

    Each entity layer has a "layer color", derived from the entity appearance.
    This layer color is used to provide the body/mane/eye color for pony sprite
    layers.  Since these elements require shading, the fragment shader
    (entity2.frag) actually derives a 5-color palette from the base layer
    color.  Then it replaces color values (100, 0, 100) through (104, 0, 104)
    with colors from the palette.'''
    return (x, 0, x)

PONY_BODY_PALETTE = [
        m(101),             # outline (black)
        m(102),             # alt/shadowed outline (dark gray)
        m(103),             # body shading
        m(100),             # body
        m(104),             # body highlights
        (255, 255, 255),     # white (appears in eye in trot animation)
        ]

PONY_EYE_PALETTE = [
        (0, 0, 0),
        m(102),
        (255, 255, 255),
        ]

PONY_HAIR_PALETTE = [
        (0, 0, 0),
        m(102),
        m(100),
        ]


def colorize_pony_layer(name, img):
    old_palette = lib.palette.collect_all_colors(img)

    if name in PONY_BODY_LAYERS:
        new_palette = PONY_BODY_PALETTE
    elif 'eye' in name:
        new_palette = PONY_EYE_PALETTE
    elif name.startswith('mane') or name.startswith('tail'):
        new_palette = PONY_HAIR_PALETTE
    else:
        return img

    return lib.palette.recolor(img, new_palette, old_palette)

def colorize_pony_layer_dict(dct):
    return dict((k, colorize_pony_layer(k, v)) for k, v in dct.items())
