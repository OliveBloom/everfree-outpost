from outpost_data.lib.consts import *
from outpost_data.lib import mesh_util

TILE_SIZE = TILE_SIZE2

def process_struct_size(sx, sy, sz):
    assert sx != 0, "x size can't be zero"
    assert not (sy == 0 and sz == 0), "y and z sizes can't both be zero"

    px, py, pz = (x * TILE_SIZE for x in (sx, sy, sz))
    bounds = [0, 0, 0, px, py, pz]

    if sy == 0:
        sy = 1
        bounds[1] += TILE_SIZE
        bounds[4] += TILE_SIZE
        mesh = mesh_util.front(px, TILE_SIZE, pz)
        block_flags = B_OCCUPIED | B_SOLID | B_SHAPE(S_SOLID)
    elif sz == 0:
        sz = 1
        mesh = mesh_util.bottom(px, py)
        block_flags = B_OCCUPIED | B_FLOOR
    else:
        mesh = mesh_util.solid(px, py, pz)
        block_flags = B_OCCUPIED | B_SOLID | B_SHAPE(S_SOLID)

    shape = [block_flags] * sx * sy * sz

    return (sx, sy, sz), shape, mesh, bounds


