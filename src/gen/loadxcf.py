from collections import namedtuple, OrderedDict
import io
import struct

import wand.image
import PIL.Image


PROP_END = 0
PROP_OFFSETS = 15
PROP_COMPRESSION = 17
PROP_GROUP_ITEM = 29
PROP_ITEM_PATH = 30


class XcfReader:
    def __init__(self, f):
        self.f = io.BufferedReader(f)

    def read(self, fmt):
        fmt = '>' + fmt
        b = self.f.read(struct.calcsize(fmt))
        return struct.unpack(fmt, b)

    def read_str(self):
        length, = self.read('I')
        b = self.read_raw(length)
        return b[:-1].decode('utf-8')

    def read_raw(self, count):
        return self.f.read(count)


    def iter_prop_list(self):
        offset = self.f.tell()

        while True:
            self.f.seek(offset)
            ty, length = self.read('II')
            offset += 8 + length
            if ty == PROP_END:
                break
            yield ty, length

        return offset

    def read_image_header(self):
        magic, ver, zero = self.read('9s4sB')
        if magic != b'gimp xcf ' or zero != 0:
            raise ValueError('bad magic number')

        self.f.seek(14 + 12)
        for ty, length in self.iter_prop_list():
            pass

        layer_ptrs = []
        while True:
            ptr, = self.read('I')
            if ptr == 0:
                break
            layer_ptrs.append(ptr)

        return layer_ptrs

    def read_layer(self, ptr):
        self.f.seek(ptr + 12)

        info = dict(
                name=self.read_str(),
                offset=(0, 0),
                is_group=False,
                )

        for ty, length in self.iter_prop_list():
            if ty == PROP_ITEM_PATH:
                info['idxs'] = self.read('%dI' % (length // 4))
            elif ty == PROP_GROUP_ITEM:
                info['is_group'] = True
            elif ty == PROP_OFFSETS:
                info['offset'] = self.read('II')

        return info

    def read_layer_paths(self):
        layer_ptrs = self.read_image_header()

        counter = 0
        layers = []
        path_map = {}
        for ptr in layer_ptrs:
            info = self.read_layer(ptr)

            if 'idxs' not in info:
                info['idxs'] = (counter,)
                counter += 1

            if len(info['idxs']) > 1:
                parent = info['idxs'][:-1]
                info['name'] = path_map[parent] + '/' + info['name']
            path_map[info['idxs']] = info['name']

            layers.append(info)

        return layers

    def __del__(self):
        self.f.detach()


LayerInfo = namedtuple('LayerInfo', ('name', 'is_group', 'offset', 'img'))

def load_xcf(filename):
    with open(filename, 'rb') as f:
        layer_info = XcfReader(f).read_layer_paths()
        f.seek(0)

        img = wand.image.Image(file=f)
        assert len(layer_info) == len(img.sequence)

        layers = OrderedDict()
        for l, info in zip(img.sequence, reversed(layer_info)):
            l = l.clone()
            img = PIL.Image.frombytes('RGBA', l.size, l.make_blob('rgba'))
            if info['name'] in layers:
                raise KeyError('duplicate layer %r in image %r' % (info['name'], filename))

            layers[info['name']] = LayerInfo(
                    name=info['name'],
                    is_group=info['is_group'],
                    offset=info['offset'],
                    img=img)

        return layers


if __name__ == '__main__':
    import sys
    path, = sys.argv[1:]
    from pprint import pprint
    pprint(load_xcf(path))
