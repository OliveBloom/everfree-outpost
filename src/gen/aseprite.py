import struct
import sys
import zlib
from PIL import Image


CHUNK_OLD_PALETTE = 0x0004
CHUNK_OLD_PALETTE2 = 0x0011
CHUNK_LAYER = 0x2004
CHUNK_CEL = 0x2005
CHUNK_PALETTE = 0x2019

CEL_RAW = 0
CEL_LINKED = 1
CEL_COMPRESSED = 2

DEPTH_INDEXED = 8
DEPTH_GRAYSCALE = 16
DEPTH_RGBA = 32

class Reader:
    def __init__(self, f):
        self.f = f

    def get(self, fmt):
        fmt = '<' + fmt
        b = self.f.read(struct.calcsize(fmt))
        return struct.unpack(fmt, b)

    def skip(self, n):
        self.f.seek(n, 1)

    def get_string(self):
        size, = self.get('H')
        return self.f.read(size).decode('utf-8')

    def get_raw(self, n):
        return self.f.read(n)

    def tell(self):
        return self.f.tell()


    def read_header(self):
        file_size, magic_number, frames, width, height, depth, flags, \
                speed, transparent_index, colors, px_w, px_h = \
                self.get('IHHHHHIH8xB3xHBB92x')
                #self.get('IHHHHHIH8xB3xIBB92x')

        assert magic_number == 0xa5e0
        assert px_w == 0 or px_h == 0 or px_w == px_h, \
                'non-square pixels are unsupported'

        if colors == 0:
            colors = 256

        return {
                'frames': frames,
                'width': width,
                'height': height,
                'depth': depth,
                'has_layer_opacity': (flags & 1) != 0,
                'frame_duration': speed,
                'transparent_index': transparent_index,
                'colors': colors,
                }

    def read_frame(self):
        frame_size, magic_number, chunks1, duration, chunks2 = \
                self.get('IHHH2xI')

        assert magic_number == 0xf1fa

        if chunks1 < 0xffff:
            chunks = chunks1
        else:
            chunks = chunks2

        return {
                'chunks': chunks,
                'duration': duration,
                }

    def read_chunk(self):
        size, chunk_type = self.get('IH')

        read_chunk = getattr(self, 'read_chunk_%04x' % chunk_type, None)
        if read_chunk is None:
            data = self.get_raw(size - 6)
        else:
            data = read_chunk(size - 6)

        return {
                'size': size,
                'type': chunk_type,
                'data': data,
                }

    def read_chunk_0004(self, data_size):
        '''Old palette chunk 0x0004.  Contains a sequence of RGB color values,
        except the sequence may have holes (`None`).'''
        num_packets, = self.get('H')
        colors = []
        for i in range(num_packets):
            skip, size = self.get('BB')
            if size == 0:
                size = 256
            for j in range(skip):
                colors.append(None)
            for j in range(size):
                colors.append(self.get('BBB'))  # RGB tuple
        return colors

    def read_chunk_0011(self, data_size):
        '''Old palette chunk 0x0011.  Similar to 0x0004, but color values are
        only in the range 0-63.'''
        return self.read_chunk_0004()

    def read_chunk_2004(self, data_size):
        '''Layer chunk.'''
        flags, layer_type, child_level, default_width, default_height, \
                blend_mode, opacity = \
                self.get('HHHHHHB3x')
        name = self.get_string()

        return {
                'visible': (flags & 1) != 0,
                'editable': (flags & 2) != 0,
                'movement_locked': (flags & 4) != 0,
                'is_background': (flags & 8) != 0,
                'prefer_linked_cels': (flags & 16) != 0,
                'display_collapsed': (flags & 32) != 0,
                'is_reference': (flags & 64) != 0,
                'type': layer_type,
                'child_level': child_level,
                'blend_mode': blend_mode,
                'opacity': opacity,
                'name': name,
                }

    def read_chunk_2005(self, data_size):
        '''Cel chunk.'''
        start = self.tell()
        layer_index, x, y, opacity, cel_type = self.get('HhhBH7x')

        dct = {
                'layer_index': layer_index,
                'x': x,
                'y': y,
                'opacity': opacity,
                'type': cel_type,
                }

        if cel_type == CEL_RAW:
            w, h = self.get('HH')
            end = self.tell()
            data = self.get_raw(data_size - (end - start))
            dct.update({
                'width': w,
                'height': h,
                'data': data,
                })
        elif cel_type == CEL_LINKED:
            link_frame, = self.get('H')
            dct['link_frame'] = link_frame
        elif cel_type == CEL_COMPRESSED:
            w, h = self.get('HH')
            end = self.tell()
            compressed_data = self.get_raw(data_size - (end - start))
            data = zlib.decompress(compressed_data)
            dct.update({
                'width': w,
                'height': h,
                'data': data,
                })
        else:
            raise ValueError('unsupported cel type %d' % cel_type)

        return dct

    def read_chunk_2019(self, data_size):
        '''New palette chunk.'''
        palette_size, first_index, last_index = self.get('III8x')
        entries = []
        for i in range(last_index - first_index + 1):
            flags, r, g, b, a = self.get('HBBBB')
            dct = {
                    'color': (r, g, b, a),
                    }
            if (flags & 1) != 0:
                dct['name'] = self.get_string()
            entries.append(dct)
        return {
                'palette_size': palette_size,
                'entries': entries,
                'first_index': first_index,
                }

def parse_aseprite(f):
    r = Reader(f)

    layers = []
    palette = []
    saw_new_palette = False

    hdr = r.read_header()

    for i in range(hdr['frames']):
        frame = r.read_frame()

        for j in range(frame['chunks']):
            chunk = r.read_chunk()
            data = chunk['data']

            if chunk['type'] == CHUNK_OLD_PALETTE:
                if not saw_new_palette:
                    palette = [x + (255,) for x in data]

            elif chunk['type'] == CHUNK_PALETTE:
                if not saw_new_palette:
                    palette = []
                if len(palette) < data['palette_size']:
                    palette.extend(None for _ in range(data['palette_size'] - len(palette)))
                for k, e in enumerate(data['entries']):
                    palette[data['first_index'] + k] = e['color']
                saw_new_palette = True

            elif chunk['type'] == CHUNK_LAYER:
                layers.append({
                    'name': data['name'],
                    # `cels` contains a list of cels for each frame
                    'cels': [[] for _ in range(hdr['frames'])],
                    'valid': data['type'] == 0 and not data['is_reference'],
                    })

            elif chunk['type'] == CHUNK_CEL:
                if data['type'] == CEL_LINKED:
                    print('warning: linked cels are unsupported')
                    continue

                layers[data['layer_index']]['cels'][i].append({
                    'x': data['x'],
                    'y': data['y'],
                    'width': data['width'],
                    'height': data['height'],
                    'data': data['data'],
                    })

    return {
            'header': hdr,
            'layers': layers,
            'layer_map': dict((l['name'], i) for i, l in enumerate(layers)),
            'palette': palette,
            }

def convert_image(ase, w, h, data):
    '''Convert raw pixel data from an aseprite file to an RGBA PIL image.'''

    if ase['header']['depth'] == DEPTH_INDEXED:
        # PIL doesn't support RGBA palettes.  We handle this by making
        # two images, one using the RGB part of the palette and one
        # using the alpha part, then set the former as the alpha
        # channel for the latter.
        palette = ase['palette']

        rgb_palette = []
        a_palette = []
        for c in palette:
            rgb_palette.extend(c[:3])
            a_palette.extend((c[3], 0, 0))
        padding = [0, 0, 0] * (256 - len(palette))

        rgb_palette += padding
        a_palette += padding

        a_palette[ase['header']['transparent_index'] * 3] = 0


        image = Image.frombytes('P', (w, h), data)
        alpha_image = image.copy()

        image.putpalette(rgb_palette)
        image = image.convert('RGB')

        alpha_image.putpalette(a_palette)
        alpha_image = alpha_image.convert('RGB').getchannel(0)
        image.putalpha(alpha_image)

        return image

    elif ase['header']['depth'] == DEPTH_RGBA:
        return Image.frombytes('RGBA', (w, h), data)

    else:
        raise ValueError('unsupported bit depth %d' % ase['header']['depth'])


def convert_layer(ase, layer_index):
    layer = ase['layers'][layer_index]

    header = ase['header']
    w = header['width']
    h = header['height']
    num_frames = header['frames']

    frame_images = [Image.new('RGBA', (w, h)) for _ in range(num_frames)]

    for i, cels in enumerate(layer['cels']):
        for cel in cels:
            cel_image = convert_image(ase, cel['width'], cel['height'], cel['data'])
            frame_images[i].paste(cel_image, (cel['x'], cel['y']), cel_image)

    return frame_images


if __name__ == '__main__':
    path, = sys.argv[1:]
    ase = parse_aseprite(open(path, 'rb'))
    layer = convert_layer(ase, '16x22 good 6')
    for i in range(len(layer)):
        layer[i].save('frame%d.png' % i)
