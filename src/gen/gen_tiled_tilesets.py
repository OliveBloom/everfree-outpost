import argparse
import base64
import io
import json
import os
from pprint import pprint
import sys
import xml.etree.ElementTree as ET

from PIL import Image


TILE_SIZE = 16


def build_parser():
    args = argparse.ArgumentParser()

    args.add_argument('--json-dir',
            help='directory containing data and origin json files')
    args.add_argument('--atlas-dir',
            help='directory containing texture atlases')
    args.add_argument('--out-dir',
            help='where to place generated .tsx tilesets')

    return args


_DEPS = set()

def read_file(path):
    _DEPS.add(path)
    with open(path) as f:
        return f.read()

def read_json_file(path):
    _DEPS.add(path)
    with open(path) as f:
        return json.load(f)

def get_deps():
    return sorted(_DEPS)


class MultisheetAtlas:
    def __init__(self, base):
        self.base = base
        self.imgs = {}

    def __getitem__(self, idx):
        if idx not in self.imgs:
            self.imgs[idx] = Image.open('%s%d.png' % (self.base, idx))
        return self.imgs[idx]

def calc_bounding_box(verts):
    return (
            min(x for x,y,z in verts),
            min(y - z for x,y,z in verts),
            max(x for x,y,z in verts),
            max(y - z for x,y,z in verts),
            )

def raw_image_data(img):
    '''Convert PIL image to base64'ed PNG.'''
    bio = io.BytesIO()
    img.save(bio, 'png')
    return bio.getvalue()

def xml_image_data(img):
    '''Convert PIL image to <data> element.'''
    x = ET.Element('data')
    x.set('encoding', 'base64')
    b = raw_image_data(img)
    x.text = base64.b64encode(b).decode('ascii')
    assert isinstance(x.text, str)
    return x

def encode_property(value):
    if isinstance(value, str):
        return ('string', value)
    elif isinstance(value, bool):
        return ('bool', 'true' if value else 'false')
    # Must be after the `bool` case, since `bool` is a subtype of `int`
    elif isinstance(value, int):
        return ('int', str(value))
    elif isinstance(value, float):
        return ('float', str(value))

def xml_property(name, value):
    '''Convert key/value pair to <property> element.'''
    ty, val_str = encode_property(value)
    x = ET.Element('property')
    x.set('name', name)
    x.set('type', ty)
    x.text = val_str
    assert isinstance(x.text, str)
    return x

def xml_properties(dct):
    '''Convert a dict to a <properties> element.'''
    x = ET.Element('properties')
    for k,v in sorted(dct.items(), key=lambda x: x[0]):
        x.append(xml_property(k, v))
    return x

def xml_image(img):
    '''Convert PIL image to an <image> element.'''
    sx, sy = img.size
    x = ET.Element('image')
    x.set('width', str(sx))
    x.set('height', str(sy))
    x.append(xml_image_data(img))
    return x

def bbox_size(bbox):
    x0, y0, x1, y1 = bbox
    return (x1 - x0, y1 - y0)

def bbox_offset(bbox, off):
    x0, y0, x1, y1 = bbox
    ox, oy = off
    return (x0 + ox, y0 + oy, x1 + ox, y1 + oy)

def load_structures(args):
    '''Load all structures.  Produces a dict mapping structure names to <tile>
    nodes, except the `id` attribute of the <tile> is unset.'''
    j = read_json_file(os.path.join(args.json_dir, 'structures.json'))
    sheets = MultisheetAtlas(os.path.join(args.atlas_dir, 'structures'))
    xs = []
    for js in j:
        sx, sy, sz = js['size']
        img = Image.new('RGBA', (sx * TILE_SIZE, (sy + sz) * TILE_SIZE))

        for jp in js['parts']:
            bbox = calc_bounding_box(jp['verts'])
            src_bbox = bbox_offset(bbox, jp['offset'])
            dest_bbox = bbox_offset(bbox, (0, sz * TILE_SIZE))
            part_img = sheets[jp['sheet']].crop(src_bbox)
            img.paste(part_img, dest_bbox[:2], part_img)

        x_image = xml_image(img)
        x_props = xml_properties({
            'kind': 'structure',
            'name': js['name'],
            })

        x = ET.Element('tile')
        x.append(x_image)
        x.append(x_props)
        xs.append(x)

    return xs

def load_blocks(args):
    '''Load all blocks.  Produces a dict mapping block names to <tile> nodes,
    except the `id` attribute of the <tile> is unset.  The image for each
    <tile> is obtained from the block's associated terrain, if it has one.'''

    j_blocks = read_json_file(os.path.join(args.json_dir, 'blocks.json'))
    j_terrains = read_json_file(os.path.join(args.json_dir, 'terrains.json'))

    sheet = Image.open(os.path.join(args.atlas_dir, 'terrain.png'))
    xs = []

    for jb in j_blocks:
        terrain_id = jb.get('terrain')
        if terrain_id is not None:
            jt = j_terrains[terrain_id]
            y0 = 2 * TILE_SIZE * jt['index']
            x0 = 15 * TILE_SIZE
            x1, y1 = x0 + TILE_SIZE, y0 + TILE_SIZE
            img = sheet.crop((x0, y0, x1, y1))
        else:
            img = Image.new('RGBA', (TILE_SIZE, TILE_SIZE))

        x_image = xml_image(img)
        x_props = xml_properties({
            'kind': 'block',
            'name': jb['name'],
            })

        x = ET.Element('tile')
        x.append(x_image)
        x.append(x_props)
        xs.append(x)

    return xs

def xml_tileset(x_tiles, name):
    x = ET.Element('tileset')
    x.set('version', '1.2')
    x.set('name', name)

    x_grid = ET.SubElement(x, 'grid')
    x_grid.set('orientation', 'orthogonal')
    x_grid.set('width', '1')
    x_grid.set('height', '1')

    for i, x_tile in enumerate(x_tiles):
        # XXX This mutates `x_tile` in place!  `x_tile` must not be shared
        # across multiple calls to xml_tileset.
        x_tile.set('id', str(i + 1))
        x.append(x_tile)

    return x

def main(argv):
    args = build_parser().parse_args(argv)

    structures = load_structures(args)
    blocks = load_blocks(args)

    os.makedirs(args.out_dir, exist_ok=True)
    with open(os.path.join(args.out_dir, 'blocks.tsx'), 'wb') as f:
        xml = ET.ElementTree(xml_tileset(blocks, 'blocks'))
        xml.write(f, xml_declaration=True)
    with open(os.path.join(args.out_dir, 'structures.tsx'), 'wb') as f:
        xml = ET.ElementTree(xml_tileset(structures, 'structures'))
        xml.write(f, xml_declaration=True)


if __name__ == '__main__':
    main(sys.argv[1:])
