import os
import sys

if __name__ == '__main__':
    sys.path.append(os.path.dirname(__file__))
    from binary_defs.__main__ import main
    main()
