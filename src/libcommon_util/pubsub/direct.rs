use btree_multimap::BTreeMultiMap;

use super::Name;


/// A basic publish/subscribe system.
///
/// Each subscriber can associate itself with any number of publishers.  When a publisher sends a
/// message, each subscriber associated with it will receive a copy of the message.
///
/// Subscription counts are tracked properly, so it is safe to let one subscriber subscribe to the
/// same publisher multiple times.  The subscriber will receive only one copy of each message, and
/// the subscription will end only after an equal number of unsubscribes.
#[derive(Debug)]
pub struct DirectSub<P: Name, S: Name> {
    mmap: BTreeMultiMap<P, S>,
}

impl<P: Name, S: Name> DirectSub<P, S> {
    pub fn new() -> DirectSub<P, S> {
        DirectSub {
            mmap: BTreeMultiMap::new(),
        }
    }

    pub fn subscribe(&mut self, subscriber: S, publisher: P) -> bool {
        self.mmap.insert(publisher, subscriber)
    }

    pub fn unsubscribe(&mut self, subscriber: S, publisher: P) -> bool {
        self.mmap.remove(publisher, subscriber)
    }


    pub fn message<F>(&self, publisher: &P, mut f: F)
            where F: FnMut(&P, &S) {
        for subscriber in self.mmap.get_distinct(publisher.clone()) {
            f(publisher, subscriber);
        }
    }
}
