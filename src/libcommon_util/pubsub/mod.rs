use btree_multimap::Bounded;

mod channel;
mod direct;

pub use self::channel::PubSub;
pub use self::direct::DirectSub;


pub trait Name: Ord+Bounded+Clone {}

impl<T: Ord+Bounded+Clone> Name for T {}
