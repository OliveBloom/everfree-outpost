use std::cmp::Ordering;
use common_types::{Vn, scalar};

use btree_multimap::Bounded;


#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct GridOrdered<V: Vn>(pub V);

impl<V: Vn+Eq> Ord for GridOrdered<V> {
    fn cmp(&self, other: &GridOrdered<V>) -> Ordering {
        let mut ord = Ordering::Equal;

        V::fold_axes((), |a, ()| {
            if ord == Ordering::Equal {
                ord = self.0.get(a).cmp(&other.0.get(a));
            }
        });

        ord
    }
}

impl<V: Vn+Eq> PartialOrd for GridOrdered<V> {
    fn partial_cmp(&self, other: &GridOrdered<V>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<V: Vn+Eq+Clone> Bounded for GridOrdered<V> {
    fn min_bound() -> GridOrdered<V> {
        GridOrdered(scalar(i32::min_bound()))
    }

    fn max_bound() -> GridOrdered<V> {
        GridOrdered(scalar(i32::max_bound()))
    }
}


#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct ZOrdered<V: Vn>(pub V);

impl<V: Vn+Eq> Ord for ZOrdered<V> {
    fn cmp(&self, other: &ZOrdered<V>) -> Ordering {
        let mut ord = Ordering::Equal;
        let mut min_lz = 32;

        V::fold_axes((), |a, ()| {
            let lz = (self.0.get(a) ^ other.0.get(a)).leading_zeros();
            if lz < min_lz {
                min_lz = lz;
                ord = self.0.get(a).cmp(&other.0.get(a));
            }
        });

        ord
    }
}

impl<V: Vn+Eq> PartialOrd for ZOrdered<V> {
    fn partial_cmp(&self, other: &ZOrdered<V>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<V: Vn+Eq+Clone> Bounded for ZOrdered<V> {
    fn min_bound() -> ZOrdered<V> {
        ZOrdered(scalar(i32::min_bound()))
    }

    fn max_bound() -> ZOrdered<V> {
        ZOrdered(scalar(i32::max_bound()))
    }
}

