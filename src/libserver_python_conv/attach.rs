use common::types::*;
use multipython::Interp;
use multipython::{PyBox, PyRef, PyResult};
use multipython::api as py;
use multipython::class;
use server_world_types::{EntityAttachment, InventoryAttachment, StructureAttachment};
use syntax_exts::python_class;

use {Pack, Unpack};
use ServerTypes;



pub struct Types {
    world: PyBox,
    plane: PyBox,
    chunk: PyBox,
}

macro_rules! unit_type {
    ($Name:ident, $field_name:ident) => {
        python_class! {
            name = $Name;
            data: ();
            conv_module = super;

            fn __init__() {}    // Take no args, return no data
            fn __repr__(self) -> String { stringify!($Name).to_owned() }

            fn __hash__(self) -> u64 { 999 }

            fn __eq__(self, other: PyBox) -> PyResult<bool> {
                let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                                    runtime_error, "ServerTypes not available");
                pyassert!(py::object::is_instance(other.borrow(), tys.attach.$field_name.borrow()),
                          type_error, concat!("expected an instance of ", stringify!($Name)));
                Ok(true)
            }
        }
    };
}

pub fn register() -> PyResult<Types> {
    Ok(Types {
        world: unit_type!(WorldAttach, world)?,
        plane: unit_type!(PlaneAttach, plane)?,
        chunk: unit_type!(ChunkAttach, chunk)?,
    })
}

pub fn init_module(m: PyRef, tys: &Types) -> PyResult<()> {
    py::object::set_attr_str(m, "WorldAttach", tys.world.borrow())?;
    py::object::set_attr_str(m, "PlaneAttach", tys.plane.borrow())?;
    py::object::set_attr_str(m, "ChunkAttach", tys.chunk.borrow())?;
    Ok(())
}


impl Unpack for EntityAttachment {
    fn unpack(obj: PyRef) -> PyResult<EntityAttachment> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");
        if py::object::is_instance(obj, tys.attach.world.borrow()) {
            Ok(EntityAttachment::World)
        } else if py::object::is_instance(obj, tys.attach.chunk.borrow()) {
            Ok(EntityAttachment::Chunk)
        } else if let Ok(cid) = <ClientId as Unpack>::unpack(obj) {
            Ok(EntityAttachment::Client(cid))
        } else {
            pyraise!(type_error, "expected entity attachment");
        }
    }
}

impl Unpack for InventoryAttachment {
    fn unpack(obj: PyRef) -> PyResult<InventoryAttachment> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");
        if py::object::is_instance(obj, tys.attach.world.borrow()) {
            Ok(InventoryAttachment::World)
        } else if let Ok(cid) = <ClientId as Unpack>::unpack(obj) {
            Ok(InventoryAttachment::Client(cid))
        } else if let Ok(cid) = <EntityId as Unpack>::unpack(obj) {
            Ok(InventoryAttachment::Entity(cid))
        } else if let Ok(cid) = <StructureId as Unpack>::unpack(obj) {
            Ok(InventoryAttachment::Structure(cid))
        } else {
            pyraise!(type_error, "expected inventory attachment");
        }
    }
}

impl Unpack for StructureAttachment {
    fn unpack(obj: PyRef) -> PyResult<StructureAttachment> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");
        if py::object::is_instance(obj, tys.attach.plane.borrow()) {
            Ok(StructureAttachment::Plane)
        } else if py::object::is_instance(obj, tys.attach.chunk.borrow()) {
            Ok(StructureAttachment::Chunk)
        } else {
            pyraise!(type_error, "expected structure attachment");
        }
    }
}

impl Pack for EntityAttachment {
    fn pack(self) -> PyResult<PyBox> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");
        match self {
            EntityAttachment::World =>
                unsafe { class::instantiate(tys.attach.world.borrow(), ()) },
            EntityAttachment::Chunk =>
                unsafe { class::instantiate(tys.attach.chunk.borrow(), ()) },
            EntityAttachment::Client(cid) => Pack::pack(cid),
        }
    }
}

impl Pack for InventoryAttachment {
    fn pack(self) -> PyResult<PyBox> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");
        match self {
            InventoryAttachment::World =>
                unsafe { class::instantiate(tys.attach.world.borrow(), ()) },
            InventoryAttachment::Client(cid) => Pack::pack(cid),
            InventoryAttachment::Entity(eid) => Pack::pack(eid),
            InventoryAttachment::Structure(sid) => Pack::pack(sid),
        }
    }
}

impl Pack for StructureAttachment {
    fn pack(self) -> PyResult<PyBox> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");
        match self {
            StructureAttachment::Plane =>
                unsafe { class::instantiate(tys.attach.plane.borrow(), ()) },
            StructureAttachment::Chunk =>
                unsafe { class::instantiate(tys.attach.chunk.borrow(), ()) },
        }
    }
}
