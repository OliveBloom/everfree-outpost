use common::types::*;
use common::util::hash;
use multipython::Interp;
use multipython::{PyBox, PyRef, PyResult};
use multipython::api as py;
use multipython::class;
use syntax_exts::python_class_builder;

use ServerTypes;
use {Pack, Unpack};


pub struct Types {
    pub v3: PyBox,
    pub v2: PyBox,
    pub region3: PyBox,
    pub region2: PyBox,
}

pub fn register() -> PyResult<Types> {
    Ok(Types {
        v3: register_V3()?,
        v2: register_V2()?,
        region3: register_Region3()?,
        region2: register_Region2()?,
    })
}

pub fn init_module(m: PyRef, tys: &Types) -> PyResult<()> {
    py::object::set_attr_str(m, "V3", tys.v3.borrow())?;
    py::object::set_attr_str(m, "V2", tys.v2.borrow())?;
    py::object::set_attr_str(m, "Region3", tys.region3.borrow())?;
    py::object::set_attr_str(m, "Region2", tys.region2.borrow())?;
    Ok(())
}


python_class_builder! {
    name = V3;
    builder_name = register_V3;
    data: V3;
    conv_module = super;

    fn __init__(x: i32, y: i32, z: i32) -> V3 { V3::new(x, y, z) }

    fn __repr__(self) -> String {
        format!("V3({}, {}, {})", self.x, self.y, self.z)
    }

    fn __hash__(self) -> u64 { hash(&self) }

    get x(&self) -> i32 { self.x }
    get y(&self) -> i32 { self.y }
    get z(&self) -> i32 { self.z }

    fn __eq__(self, other: V3) -> bool { self == other }

    fn __add__(a: V3, b: V3) -> V3 { a + b }
    fn __sub__(a: V3, b: V3) -> V3 { a - b }
    fn __mul__(a: V3, b: V3) -> V3 { a * b }
    fn __div__(a: V3, b: V3) -> V3 { a.div_floor(b) }
    fn __mod__(a: V3, b: V3) -> V3 { a.mod_floor(b) }
    fn __neg__(self) -> V3 { -self }

    fn dot(self, other: V3) -> i32 { self.dot(other) }

    fn reduce(self) -> V2 { self.reduce() }
}

impl Pack for V3 {
    fn pack(self) -> PyResult<PyBox> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");
        unsafe { class::instantiate(tys.v3.v3.borrow(), self) }
    }
}

impl Unpack for V3 {
    fn unpack(obj: PyRef) -> PyResult<V3> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");

        if py::object::is_instance(obj, tys.v3.v3.borrow()) {
            let r = pyunwrap!(unsafe { class::get::<V3>(obj) },
                              runtime_error, "object has already been destructed");
            Ok(*r)
        } else if py::int::check(obj) {
            let x = py::int::as_i64(obj)?;
            Ok(scalar(x as i32))
        } else {
            pyraise!(type_error, "expected an instance of V3 or int");
        }
    }
}


python_class_builder! {
    name = V2;
    builder_name = register_V2;
    data: V2;
    conv_module = super;

    fn __init__(x: i32, y: i32) -> V2 { V2::new(x, y) }

    fn __repr__(self) -> String {
        format!("V2({}, {})", self.x, self.y)
    }

    fn __hash__(self) -> u64 { hash(&self) }

    get x(&self) -> i32 { self.x }
    get y(&self) -> i32 { self.y }

    fn __eq__(self, other: V2) -> bool { self == other }

    fn __add__(a: V2, b: V2) -> V2 { a + b }
    fn __sub__(a: V2, b: V2) -> V2 { a - b }
    fn __mul__(a: V2, b: V2) -> V2 { a * b }
    fn __div__(a: V2, b: V2) -> V2 { a.div_floor(b) }
    fn __mod__(a: V2, b: V2) -> V2 { a.mod_floor(b) }
    fn __neg__(self) -> V2 { -self }

    fn dot(self, other: V2) -> i32 { self.dot(other) }

    fn extend(self, z: i32) -> V3 { self.extend(z) }
}

impl Pack for V2 {
    fn pack(self) -> PyResult<PyBox> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");
        unsafe { class::instantiate(tys.v3.v2.borrow(), self) }
    }
}

impl Unpack for V2 {
    fn unpack(obj: PyRef) -> PyResult<V2> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");

        if py::object::is_instance(obj, tys.v3.v2.borrow()) {
            let r = pyunwrap!(unsafe { class::get::<V2>(obj) },
                              runtime_error, "object has already been destructed");
            Ok(*r)
        } else if py::int::check(obj) {
            let x = py::int::as_i64(obj)?;
            Ok(scalar(x as i32))
        } else {
            pyraise!(type_error, "expected an instance of V2 or int");
        }
    }
}


python_class_builder! {
    name = Region3;
    builder_name = register_Region3;
    data: Region<V3>;
    conv_module = super;

    fn __init__(min: V3, max: V3) -> Region<V3> { Region::new(min, max) }

    fn __repr__(self) -> String {
        format!("Region3({:?}, {:?})", self.min, self.max)
    }

    fn __hash__(self) -> u64 { hash(&self) }

    get min(&self) -> V3 { self.min }
    get max(&self) -> V3 { self.max }

    fn __eq__(self, other: Region<V3>) -> bool { self == other }

    fn __add__(a: Region<V3>, b: V3) -> Region<V3> { a + b }
    fn __sub__(a: Region<V3>, b: V3) -> Region<V3> { a - b }
    fn __mul__(a: Region<V3>, b: V3) -> Region<V3> { a * b }
    fn __neg__(self) -> Region<V3> { -self }

    fn div_round_signed(&self, b: i32) -> Region<V3> { self.div_round_signed(b) }

    fn intersect(&self, other: Region<V3>) -> Region<V3> { self.intersect(other) }
    fn overlaps(&self, other: Region<V3>) -> bool { self.overlaps(other) }
    fn contains(&self, point: V3) -> bool { self.contains(point) }
    fn index(&self, point: V3) -> usize { self.index(point) }
    fn from_index(&self, index: usize) -> V3 { self.from_index(index) }
    fn size(&self) -> V3 { self.size() }
    fn volume(&self) -> i32 { self.volume() }
    fn reduce(&self) -> Region<V2> { self.reduce() }
}

impl Pack for Region<V3> {
    fn pack(self) -> PyResult<PyBox> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");
        unsafe { class::instantiate(tys.v3.region3.borrow(), self) }
    }
}

impl Unpack for Region<V3> {
    fn unpack(obj: PyRef) -> PyResult<Region<V3>> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");

        pyassert!(py::object::is_instance(obj, tys.v3.region3.borrow()),
                  type_error, concat!("expected an instance of Region<V3>"));
        let r = pyunwrap!(unsafe { class::get::<Region<V3>>(obj) },
                          runtime_error, "object has already been destructed");
        Ok(*r)
    }
}


python_class_builder! {
    name = Region2;
    builder_name = register_Region2;
    data: Region<V2>;
    conv_module = super;

    fn __init__(min: V2, max: V2) -> Region<V2> { Region::new(min, max) }

    fn __repr__(self) -> String {
        format!("Region2({:?}, {:?})", self.min, self.max)
    }

    fn __hash__(self) -> u64 { hash(&self) }

    get min(&self) -> V2 { self.min }
    get max(&self) -> V2 { self.max }

    fn __eq__(self, other: Region<V2>) -> bool { self == other }

    fn __add__(a: Region<V2>, b: V2) -> Region<V2> { a + b }
    fn __sub__(a: Region<V2>, b: V2) -> Region<V2> { a - b }
    fn __mul__(a: Region<V2>, b: V2) -> Region<V2> { a * b }
    fn __neg__(self) -> Region<V2> { -self }

    fn div_round_signed(&self, b: i32) -> Region<V2> { self.div_round_signed(b) }

    fn intersect(&self, other: Region<V2>) -> Region<V2> { self.intersect(other) }
    fn overlaps(&self, other: Region<V2>) -> bool { self.overlaps(other) }
    fn contains(&self, point: V2) -> bool { self.contains(point) }
    fn index(&self, point: V2) -> usize { self.index(point) }
    fn from_index(&self, index: usize) -> V2 { self.from_index(index) }
    fn size(&self) -> V2 { self.size() }
    fn volume(&self) -> i32 { self.volume() }
}

impl Pack for Region<V2> {
    fn pack(self) -> PyResult<PyBox> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");
        unsafe { class::instantiate(tys.v3.region2.borrow(), self) }
    }
}

impl Unpack for Region<V2> {
    fn unpack(obj: PyRef) -> PyResult<Region<V2>> {
        let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                            runtime_error, "ServerTypes not available");

        pyassert!(py::object::is_instance(obj, tys.v3.region2.borrow()),
                  type_error, concat!("expected an instance of Region<V2>"));
        let r = pyunwrap!(unsafe { class::get::<Region<V2>>(obj) },
                          runtime_error, "object has already been destructed");
        Ok(*r)
    }
}



#[cfg(test)]
mod test {
    use multipython::Interp;
    use multipython::util;
    use super::*;

    fn interp() -> Interp {
        let mut i = Interp::new();
        i.add_state(ServerTypes::new);
        i
    }

    #[test]
    fn v3_basic() {
        interp().enter(|| {
            let v3 = Interp::get_state::<ServerTypes>().unwrap().v3.v3.clone();
            util::exec_vars("if True:
                v = V3(1, 2, 3)
                assert (v.x, v.y, v.z) == (1, 2, 3)
            ", &[("V3", v3.borrow())]).unwrap();
        });
    }

    #[test]
    fn v3_repr() {
        interp().enter(|| {
            let v3 = Interp::get_state::<ServerTypes>().unwrap().v3.v3.clone();
            util::exec_vars("if True:
                assert repr(V3(1, 2, 3)) == 'V3(1, 2, 3)'
            ", &[("V3", v3.borrow())]).unwrap();
        });
    }

    #[test]
    fn v3_hash() {
        interp().enter(|| {
            let v3 = Interp::get_state::<ServerTypes>().unwrap().v3.v3.clone();
            util::exec_vars("if True:
                # Separate vars so both are live simultaneously, guaranteeing different addresses
                a = V3(1, 2, 3)
                b = V3(1, 2, 3)
                c = V3(1, 2, 4)
                assert hash(a) == hash(b)
                assert hash(a) != hash(c)
            ", &[("V3", v3.borrow())]).unwrap();
        });
    }

    #[test]
    fn v3_eq() {
        interp().enter(|| {
            let v3 = Interp::get_state::<ServerTypes>().unwrap().v3.v3.clone();
            util::exec_vars("if True:
                # Separate vars so both are live simultaneously, guaranteeing different addresses
                a = V3(1, 2, 3)
                b = V3(1, 2, 3)
                c = V3(1, 2, 4)
                assert a == b
                assert a != c
            ", &[("V3", v3.borrow())]).unwrap();
        });
    }

    #[test]
    fn v3_arith() {
        interp().enter(|| {
            let v3 = Interp::get_state::<ServerTypes>().unwrap().v3.v3.clone();
            util::exec_vars("if True:
                assert V3(1, 2, 3) == V3(0, 1, 2) + 1
                assert V3(1, 2, 3) == 1 + V3(0, 1, 2)
                assert V3(1, 2, 3) == -V3(-1, -2, -3)
            ", &[("V3", v3.borrow())]).unwrap();
        });
    }
}
