use common::types::*;
use common::util;
use multipython::{PyBox, PyRef, PyResult};
use multipython::Interp;
use multipython::api as py;
use multipython::class;
use server_types::{Stable, StableId};
use syntax_exts::{ident_str, expand_objs_named, python_class};

use {Pack, Unpack};
use ServerTypes;


expand_objs_named! {
    {
        client, clients, Raw = u16;
        entity, entities, Raw = u32;
        inventory, inventories, Raw = u32;
        plane, planes, Raw = u32;
        terrain_chunk, terrain_chunks, Raw = u32;
        structure, structures, Raw = u32;
    }

    pub struct Types {
        $(
            pub $obj_id: PyBox,
            pub $stable_obj_id: PyBox,
        )*
    }

    pub fn register() -> PyResult<Types> {
        Ok(Types {
            $(
                $obj_id: python_class! {
                    name = $ObjId;
                    data: $ObjId;
                    conv_module = super;

                    fn __init__(raw: $Raw) -> $ObjId { $ObjId(raw) }
                    fn __repr__(self) -> String {
                        format!(concat!(ident_str!($ObjId), "({})"), self.unwrap())
                    }

                    fn __hash__(self) -> u64 { util::hash(&self) }
                    fn __eq__(self, other: $ObjId) -> bool { self == other }

                    get raw(self) -> $Raw { self.unwrap() }
                }?,

                $stable_obj_id: python_class! {
                    name = $StableObjId;
                    data: Stable<$ObjId>;
                    conv_module = super;

                    fn __init__(raw: StableId) -> PyResult<Stable<$ObjId>> {
                        pyassert!(raw != 0, value_error, "stable IDs must be nonzero");
                        Ok(Stable::new(raw))
                    }
                    fn __repr__(self) -> String {
                        format!(concat!(ident_str!($StableObjId), "({})"), self.unwrap())
                    }

                    fn __hash__(self) -> u64 { util::hash(&self) }
                    fn __eq__(self, other: Stable<$ObjId>) -> bool { self == other }

                    get raw(self) -> StableId { self.unwrap() }
                }?,
            )*
        })
    }

    pub fn init_module(m: PyRef, tys: &Types) -> PyResult<()> {
        $(
            py::object::set_attr_str(m, ident_str!($ObjId), tys.$obj_id.borrow())?;
            py::object::set_attr_str(m, ident_str!($StableObjId), tys.$stable_obj_id.borrow())?;
        )*
        Ok(())
    }

    $(
        impl Pack for $ObjId {
            fn pack(self) -> PyResult<PyBox> {
                let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                                    runtime_error, "ServerTypes not available");
                unsafe { class::instantiate(tys.id.$obj_id.borrow(), self) }
            }
        }

        impl Pack for Stable<$ObjId> {
            fn pack(self) -> PyResult<PyBox> {
                let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                                    runtime_error, "ServerTypes not available");
                unsafe { class::instantiate(tys.id.$stable_obj_id.borrow(), self) }
            }
        }

        impl Unpack for $ObjId {
            fn unpack(obj: PyRef) -> PyResult<$ObjId> {
                let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                                    runtime_error, "ServerTypes not available");
                pyassert!(py::object::is_instance(obj, tys.id.$obj_id.borrow()),
                          type_error, concat!("expected an instance of ",
                                              ident_str!($ObjId)));
                let r = pyunwrap!(unsafe { class::get::<$ObjId>(obj) },
                                  runtime_error, "object has already been destructed");
                Ok(*r)
            }
        }

        impl Unpack for Stable<$ObjId> {
            fn unpack(obj: PyRef) -> PyResult<Stable<$ObjId>> {
                let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                                    runtime_error, "ServerTypes not available");
                pyassert!(py::object::is_instance(obj, tys.id.$stable_obj_id.borrow()),
                          type_error, concat!("expected an instance of ",
                                              ident_str!($StableObjId)));
                let r = pyunwrap!(unsafe { class::get::<Stable<$ObjId>>(obj) },
                                  runtime_error, "object has already been destructed");
                Ok(*r)
            }
        }
    )*

    impl Pack for AnyId {
        fn pack(self) -> PyResult<PyBox> {
            match self {
                $( AnyId::$Obj(id) => <$ObjId as Pack>::pack(id), )*
            }
        }
    }

    impl Unpack for AnyId {
        fn unpack(obj: PyRef) -> PyResult<AnyId> {
            let tys = pyunwrap!(Interp::get_state::<ServerTypes>(),
                                runtime_error, "ServerTypes not available");
            $( if py::object::is_instance(obj, tys.id.$obj_id.borrow()) {
                return <$ObjId as Unpack>::unpack(obj).map(AnyId::$Obj);
            } )*

            pyraise!(type_error, "expected an object ID");
        }
    }
}

