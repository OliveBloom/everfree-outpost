function $(x) { return document.getElementById(x); }

function enable_send(name) {
    $('send-' + name + '-label').classList.remove('unavailable');
    $('send-' + name).disabled = false;
    $('send-' + name).checked = true;
}

function get_screenshot(data) {
    if (window.opener == null) {
        return;
    }

    var exports = window.opener.OUTPOST;
    if (exports == null) {
        return;
    }

    var getScreenshot = exports.getScreenshot;
    if (getScreenshot == null) {
        return;
    }

    getScreenshot(function(blob) {
        var url = URL.createObjectURL(blob);

        $('screenshot-preview').src = url;
        $('view-screenshot').href = url;
        $('view-screenshot').target = '_blank';
        enable_send('screenshot');

        data['screenshot'] = blob;
    });
}

function get_account(data) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', new URL('api/check_login', AUTH_URL).href, true);
    xhr.responseType = 'json';
    xhr.withCredentials = true;

    xhr.onload = function(evt) {
        if (xhr.status == 200) {
            console.log(xhr.response);
            var acc = {
                'name': xhr.response['name'] || 'Anon',
                'uid': xhr.response['uid'] || '0',
            };

            $('account-name').textContent = acc['name'];
            $('account-id').textContent = acc['uid'];
            enable_send('account');

            data['account'] = acc;
        }
    };

    xhr.send(JSON.stringify({'auto_guest': true}));
}

function get_server(data) {
    if (window.opener == null) {
        return;
    }

    var info = window.opener.OUTPOST_LAUNCHER_INFO;
    if (info == null) {
        return;
    }

    var url = info.server_url;
    if (url == null) {
        return;
    }

    $('server-url').textContent = url;
    enable_send('server');

    data['server'] = url;
}

function get_log(data) {
    if (window.opener == null) {
        return;
    }

    var exports = window.opener.OUTPOST;
    if (exports == null) {
        return;
    }

    var logHistory = exports.logHistory;
    if (logHistory == null) {
        return;
    }

    var log_lines = logHistory.get();
    console.log(log_lines);

    var parts = [];
    for (var i = 0; i < log_lines.length; ++i) {
        parts.push(log_lines[i]);
        parts.push('\n');
    }

    var blob = new Blob(parts, {
        'type': 'text/plain',
    });

    var url = URL.createObjectURL(blob);

    $('view-log').href = url;
    $('view-log').target = '_blank';
    enable_send('log');

    data['log'] = blob;
}

function build_form_data(data) {
    var fd = new FormData();

    fd.append('report', $('report').value);
    fd.append('timestamp', data['timestamp']);

    if ($('send-screenshot').checked) {
        fd.append('screenshot', data['screenshot'], 'screenshot.png');
    }

    if ($('send-account').checked) {
        fd.append('account_name', data['account']['name']);
        fd.append('account_uid', data['account']['uid']);
    }

    if ($('send-server').checked) {
        fd.append('server_url', data['server']);
    }

    if ($('send-log').checked) {
        fd.append('log', data['log'], 'client_log.txt');
    }

    return fd;
}

document.addEventListener('DOMContentLoaded', function() {
    var data = {
        'timestamp': Date.now(),
    };
    get_server(data);
    get_account(data);
    get_screenshot(data);
    get_log(data);

    $('submit').onclick = function() {
        var fd = build_form_data(data);

        var xhr = new XMLHttpRequest();
        xhr.open('POST', BUG_URL, true);

        xhr.onreadystatechange = function(evt) {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                if (xhr.status == 200) {
                    window.close();
                    setTimeout(function() {
                        window.location.assign(window.RETURN_URL);
                    });
                } else {
                    var msg;
                    if (xhr.statusText != '') {
                        msg = 'Error: ' + xhr.statusText + '. Please try again.';
                    } else {
                        msg = 'An error occurred. Please try again.';
                    }
                    $('error-message').textContent = msg;
                }
            }
        };


        xhr.send(fd);

    };
});
