#ifndef	ASMRT_STRING_H
#define	ASMRT_STRING_H

// Minimal implementation of `string.h`, which avoids referencing `features.h`
// or other libc headers.

#include <stddef.h>

void *memcpy (void *__restrict, const void *__restrict, size_t);
void *memmove (void *, const void *, size_t);
void *memset (void *, int, size_t);
int memcmp (const void *, const void *, size_t);

#endif // ASMRT_STRING_H
