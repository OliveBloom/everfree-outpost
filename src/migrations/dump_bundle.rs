extern crate server_bundle;
extern crate server_extra;

use std::env;
use std::fs::File;
use std::io::Read;

use server_extra as extra;
use server_extra::Extra;
use server_bundle::flat;
use server_bundle::types::*;

fn load_bundle(path: &str) -> Bundle {
    let mut f = File::open(path).unwrap();
    let mut buf = Vec::new();
    f.read_to_end(&mut buf).unwrap();
    let flat = flat::FlatView::from_bytes(&buf).unwrap();
    flat.unflatten_bundle()
}

fn dump_extra(e: &Extra, indent: usize) {
    if e.len() == 0 {
        for _ in 0 .. indent {
            print!("  ");
        }
        println!("(empty)");
        return;
    }
    for (k, v) in e.iter() {
        for _ in 0 .. indent {
            print!("  ");
        }
        print!("{:?}: ", k);
        match v {
            extra::View::Value(v) => dump_extra_value(v),
            extra::View::Array(a) => dump_extra_array(a, indent + 1),
            extra::View::Hash(h) => dump_extra_hash(h, indent + 1),
        }
    }
}

fn dump_extra_value(v: extra::Value) {
    use server_extra::Value;
    match v {
        Value::Null => println!("!!null _"),
        Value::Bool(b) => println!("!!bool {}", b),
        Value::Int(i) => println!("{}", i),
        Value::Float(f) => println!("{}", f),
        Value::Str(s) => println!("{:?}", s),
        Value::Bytes(b) => println!("{:?}", b),

        Value::ClientId(id) => println!("!!client {}", id.unwrap()),
        Value::EntityId(id) => println!("!!entity {}", id.unwrap()),
        Value::InventoryId(id) => println!("!!inventory {}", id.unwrap()),
        Value::PlaneId(id) => println!("!!plane {}", id.unwrap()),
        Value::TerrainChunkId(id) => println!("!!terrain_chunk {}", id.unwrap()),
        Value::StructureId(id) => println!("!!structure {}", id.unwrap()),

        Value::StableClientId(id) => println!("!!stable_client {}", id.unwrap()),
        Value::StableEntityId(id) => println!("!!stable_entity {}", id.unwrap()),
        Value::StableInventoryId(id) => println!("!!stable_inventory {}", id.unwrap()),
        Value::StablePlaneId(id) => println!("!!stable_plane {}", id.unwrap()),
        Value::StableTerrainChunkId(id) => println!("!!stable_terrain_chunk {}", id.unwrap()),
        Value::StableStructureId(id) => println!("!!stable_structure {}", id.unwrap()),

        Value::V2(v) => println!("!!v2 [{}, {}]", v.x, v.y),
        Value::V3(v) => println!("!!v3 [{}, {}, {}]", v.x, v.y, v.z),
        Value::Region2(r) => println!("!!region2 [[{}, {}], [{}, {}]]",
                                      r.min.x, r.min.y,
                                      r.max.x, r.max.y),
        Value::Region3(r) => println!("!!region3 [[{}, {}, {}], [{}, {}, {}]]",
                                      r.min.x, r.min.y, r.min.z,
                                      r.max.x, r.max.y, r.max.z),
    }
}

fn dump_extra_array(a: extra::ArrayView, indent: usize) {
    println!("");
    for v in a.iter() {
        for _ in 0 .. indent {
            print!("  ");
        }
        print!("- ");
        match v {
            extra::View::Value(v) => dump_extra_value(v),
            extra::View::Array(a) => dump_extra_array(a, indent + 1),
            extra::View::Hash(h) => dump_extra_hash(h, indent + 1),
        }
    }
}

fn dump_extra_hash(h: extra::HashView, indent: usize) {
    println!("");
    if h.len() == 0 {
        for _ in 0 .. indent {
            print!("  ");
        }
        println!("(empty)");
        return;
    }
    for (k, v) in h.iter() {
        for _ in 0 .. indent {
            print!("  ");
        }
        print!("{:?}: ", k);
        match v {
            extra::View::Value(v) => dump_extra_value(v),
            extra::View::Array(a) => dump_extra_array(a, indent + 1),
            extra::View::Hash(h) => dump_extra_hash(h, indent + 1),
        }
    }
}

macro_rules! print_field {
    ($name:ident) => {
        print_field!($name, "{:?}")
    };
    ($name:ident, $fmt:expr) => {
        println!(concat!(
                "  ",
                stringify!($name),
                " = ",
                $fmt), $name);
    };
}

macro_rules! print_data_field {
    ($data:expr, $name:ident) => {
        println!(concat!(
                "  ",
                stringify!($name),
                " = {} ({})"),
            $name,
            $data.get($name as usize).map(|s| s as &str).unwrap_or("<not found>"));
    };
}

fn dump_world(base: &Bundle, b: &World) {
    let &World {
        now,

        next_client, next_entity, next_inventory,
        next_plane, next_terrain_chunk, next_structure,

        ref extra,
        ref child_entities, ref child_inventories,
    } = b;

    println!("World:");

    print_field!(now);

    print_field!(next_client);
    print_field!(next_entity);
    print_field!(next_inventory);
    print_field!(next_plane);
    print_field!(next_terrain_chunk);
    print_field!(next_structure);

    if extra.len() == 0 {
        println!("  extra = (empty)");
    } else {
        println!("  extra:");
        dump_extra(extra, 2);
    }
    println!("  child_entities = {:?}",
             child_entities.iter().map(|&id| id.unwrap()).collect::<Vec<_>>());
    println!("  child_inventories = {:?}",
             child_inventories.iter().map(|&id| id.unwrap()).collect::<Vec<_>>());
}

fn dump_client(base: &Bundle, idx: usize, b: &Client) {
    let &Client {
        ref name, pawn,
        
        ref extra, stable_id,
        ref child_entities, ref child_inventories,
    } = b;

    println!("Client #{}:", idx);

    print_field!(name);
    print_field!(pawn);

    if extra.len() == 0 {
        println!("  extra = (empty)");
    } else {
        println!("  extra:");
        dump_extra(extra, 2);
    }
    print_field!(stable_id, "{:x}");
    println!("  child_entities = {:?}",
             child_entities.iter().map(|&id| id.unwrap()).collect::<Vec<_>>());
    println!("  child_inventories = {:?}",
             child_inventories.iter().map(|&id| id.unwrap()).collect::<Vec<_>>());
}

fn dump_entity(base: &Bundle, idx: usize, b: &Entity) {
    let &Entity {
        stable_plane,

        ref motion, anim, facing, target_velocity, ref appearance,

        ref extra, stable_id, attachment,
        ref child_inventories,
    } = b;

    println!("Entity #{}:", idx);

    println!("  stable_plane = {:x}", stable_plane.unwrap());

    print_field!(motion);
    print_field!(anim);
    print_field!(facing);
    print_field!(target_velocity);
    print_field!(appearance);

    if extra.len() == 0 {
        println!("  extra = (empty)");
    } else {
        println!("  extra:");
        dump_extra(extra, 2);
    }
    print_field!(stable_id, "{:x}");
    print_field!(attachment);
    println!("  child_inventories = {:?}",
             child_inventories.iter().map(|&id| id.unwrap()).collect::<Vec<_>>());
}

fn dump_inventory(base: &Bundle, idx: usize, b: &Inventory) {
    let &Inventory {
        ref contents,

        ref extra, stable_id, flags, attachment,
    } = b;

    println!("Inventory #{}:", idx);

    println!("  contents:");
    print!("    ");
    for (i, item) in contents.iter().enumerate() {
        print!("{}x #{} ({})",
               item.count,
               item.id,
               base.items.get(item.id as usize).map(|s| s as &str).unwrap_or("<not found>"));
        if i == contents.len() - 1 {
            print!("\n");
        } else if i % 6 == 5 {
            print!("\n    ");
        } else {
            print!(", ");
        }
    }

    if extra.len() == 0 {
        println!("  extra = (empty)");
    } else {
        println!("  extra:");
        dump_extra(extra, 2);
    }
    print_field!(stable_id, "{:x}");
    print_field!(flags);
    print_field!(attachment);
}

fn dump_plane(base: &Bundle, idx: usize, b: &Plane) {
    let &Plane {
        ref name,

        ref extra, stable_id,
    } = b;

    println!("Plane #{}:", idx);

    print_field!(name);

    if extra.len() == 0 {
        println!("  extra = (empty)");
    } else {
        println!("  extra:");
        dump_extra(extra, 2);
    }
    print_field!(stable_id, "{:x}");
}

fn dump_terrain_chunk(base: &Bundle, idx: usize, b: &TerrainChunk) {
    let &TerrainChunk {
        stable_plane, cpos, ref blocks,

        ref extra, stable_id, flags,
        ref child_structures,
    } = b;

    println!("TerrainChunk #{}:", idx);

    println!("  stable_plane = {:x}", stable_plane.unwrap());
    print_field!(cpos);
    println!("  blocks = ...");

    if extra.len() == 0 {
        println!("  extra = (empty)");
    } else {
        println!("  extra:");
        dump_extra(extra, 2);
    }
    print_field!(stable_id, "{:x}");
    print_field!(flags);
    println!("  child_structures = {:?}",
             child_structures.iter().map(|&id| id.unwrap()).collect::<Vec<_>>());
}

fn dump_structure(base: &Bundle, idx: usize, b: &Structure) {
    let &Structure {
        stable_plane, pos, template,

        ref extra, stable_id, flags, attachment,
        ref child_inventories,
    } = b;

    println!("Structure #{}:", idx);

    println!("  stable_plane = {:x}", stable_plane.unwrap());
    print_field!(pos);
    print_data_field!(base.templates, template);

    if extra.len() == 0 {
        println!("  extra = (empty)");
    } else {
        println!("  extra:");
        dump_extra(extra, 2);
    }
    print_field!(stable_id, "{:x}");
    print_field!(flags);
    print_field!(attachment);
    println!("  child_inventories = {:?}",
             child_inventories.iter().map(|&id| id.unwrap()).collect::<Vec<_>>());
}

fn main() {
    let args = env::args().collect::<Vec<_>>();
    println!("{:?}", args);
    let path = &args[1];

    let bundle = load_bundle(path);

    if let Some(ref w) = bundle.world {
        dump_world(&bundle, w);
        println!();
    }

    for (i, c) in bundle.clients.iter().enumerate() {
        dump_client(&bundle, i, c);
        println!();
    }

    for (i, e) in bundle.entities.iter().enumerate() {
        dump_entity(&bundle, i, e);
        println!();
    }

    for (i, inv) in bundle.inventories.iter().enumerate() {
        dump_inventory(&bundle, i, inv);
        println!();
    }

    for (i, p) in bundle.planes.iter().enumerate() {
        dump_plane(&bundle, i, p);
        println!();
    }

    for (i, tc) in bundle.terrain_chunks.iter().enumerate() {
        dump_terrain_chunk(&bundle, i, tc);
        println!();
    }

    for (i, s) in bundle.structures.iter().enumerate() {
        dump_structure(&bundle, i, s);
        println!();
    }
}

