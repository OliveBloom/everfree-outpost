#![crate_name = "common_proto"]

extern crate common_types;
#[macro_use] extern crate common_util;

pub mod types;
pub mod wire;
pub mod extra_arg;

#[macro_use] mod protocol;

pub mod game;
pub mod control;

pub use self::extra_arg::ExtraArg;
