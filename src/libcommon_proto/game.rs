use std::prelude::v1::*;
use common_types::*;

use types::*;


protocol! {
    protocol Request [request_op::Opcode = u16] {
        [0x0003] Ping { cookie: u16 },
        [0x000a] Chat { msg: String },
        [0x000d] UseItem { when: LocalTime, item: ItemId },
        [0x0013] MoveItem {
            inv1: InventoryId, slot1: SlotId, inv2: InventoryId, slot2: SlotId, count: u8 },
        [0x0015] CreateCharacter { appearance: Vec<u8> },
        [0x0016] Ready {__: ()},
        [0x0017] CloseDialog {__: ()},
        [0x001b] OpenEquipment { __: () },
        [0x001c] PointInteract { pos: LocalPos },
        [0x001d] PointDestroy { pos: LocalPos },
        [0x001e] PointUseItem { pos: LocalPos, item: ItemId },
        [0x001f] PredictMotion {
            delay: u16,
            input_bits: u16,
            pos: LocalPos,
            velocity: LocalOffset,
            validity: u8
        },
        [0x0020] PredictMotionRel {
            rel_time: u16,
            input_bits: u16,
            pos: LocalPos,
            velocity: LocalOffset
        },
        [0x0021] InputStart {
            delay: u16,
            dir: u8,
            speed: u8
        },
        [0x0022] InputChange {
            rel_time: u16,
            dir: u8,
            speed: u8
        },
        [0x0023] OpenInventory { __: () },
        [0x0024] OpenAbilities { __: () },

        [0x0025] CraftingStart { recipe_id: RecipeId, count: u8 },
        [0x0026] CraftingStop { __: () },
    }
}

protocol! {
    protocol Response [response_op::Opcode = u16] {
        [0x8001] TerrainChunk { idx: u16, data: Vec<u16> },
        [0x8003] Pong { cookie: u16, now: LocalTime },
        [0x8005] Init { now: LocalTime, day_night_base: u32, day_night_ms: u32 },
        [0x8006] KickReason { msg: String },
        [0x8007] UnloadChunk { idx: u16 },
        [0x800b] ChatUpdate { msg: String },
        [0x800c] EntityAppear { id: EntityId, appearance: Vec<u8>, name: String },
        [0x800d] EntityGone { id: EntityId, when: LocalTime },
        [0x800f] StructureAppear { id: StructureId, template: TemplateId, pos: (u8, u8, u8) },
        [0x8010] StructureGone { id: StructureId },
        [0x8013] PlaneFlags { flags: u32 },
        [0x8017] SyncStatus { status: u8 },
        [0x8018] StructureReplace { id: StructureId, template: TemplateId },
        [0x8019] InventoryUpdate { id: InventoryId, slot: u8, item: (u8, u8, ItemId) },
        [0x801a] InventoryAppear { id: InventoryId, items: Vec<(u8, u8, ItemId)>, flags: u32 },
        [0x801b] InventoryGone { id: InventoryId },
        [0x8024] OpenPonyEdit { name: String },
        [0x8026] CancelDialog { __: () },
        [0x8027] EnergyUpdate { cur: u16, max: u16, rate: (i16, u16), time: LocalTime },
        [0x8029] OpenEquipment { iid1: InventoryId, iid2: InventoryId },
        [0x802a] EntityActStand { id: EntityId, time: LocalTime,
            pos: LocalPos, dir: u8 },
        [0x802b] EntityActWalk { id: EntityId, time: LocalTime,
            pos: LocalPos, velocity: LocalOffset, dir: u8 },
            /* TODO
        [0x802c] EntityActBusy { id: EntityId, time: LocalTime,
            pos: LocalPos, anim: AnimId, icon: AnimId },
            */
        [0x802d] MotionMispredict { time: LocalTime, next_validity: u8 },
        [0x802e] TickBegin { time: LocalTime },
        [0x802f] TickEnd { time: LocalTime },

        /// The server has received the client's request, and it will take effect at the indicated
        /// time (or has taken effect immediately, if `apply_time == now`).
        ///
        /// AckRequest and NakRequest let the client's prediction module precisely track of which
        /// of the client's requests have been applied to the game state.  Only some requests
        /// require acknowledgement (grep for `ack_request` to see which ones), but for those that
        /// do, every such request must generate either an ACK or a NAK, as otherwise the client's
        /// predictor will get out of sync.
        [0x8031] AckRequest { apply_time: LocalTime },
        /// The server has received the client's request, but an error occurred and it will never
        /// take effect.
        [0x803e] NakRequest { __: () },

        [0x8032] StructureAppearMulti {
            base_id: StructureId, template: TemplateId,
            structures: Vec<CondensedStructure>
        },
        [0x8033] StructureGoneMulti {
            base_id: StructureId,
            rel_ids: Vec<u8>
        },

        [0x8034] OpenInventory { iid: InventoryId },
        [0x8035] OpenAbilities { iid: InventoryId },
        [0x8036] OpenContainer { iid1: InventoryId, iid2: InventoryId },

        /// Change the appearance (and/or name) of an already-existing entity.
        [0x8037] EntityChange { id: EntityId, appearance: Vec<u8>, name: String },
        /// Change the complete contents of an already-existing inventory.
        [0x8038] InventoryContents { id: InventoryId, items: Vec<(u8, u8, ItemId)> },
        [0x8041] InventoryFlags { id: InventoryId, flags: u32 },

        [0x8039] OpenCrafting {
            station: StructureId,
            class_mask: u32,
            contents: InventoryId,
            char_main: InventoryId,
            char_ability: InventoryId
        },
        [0x803a] CraftingIdle { station: StructureId },
        [0x803b] CraftingPaused {
            station: StructureId, recipe: RecipeId, count: u8, progress: u32
        },
        [0x803c] CraftingActive {
            station: StructureId, recipe: RecipeId, count: u8,
            progress: u32, start_time: LocalTime
        },

        [0x803d] CharacterInventories {
            main: InventoryId, ability: InventoryId, equip: InventoryId
        },

        /// Set the camera to view a fixed position.
        [0x803f] CameraFixed { pos: LocalPos },
        /// Set the camera to follow a pawn entity.
        [0x8040] CameraPawn { pawn: EntityId },

        // Next unused ID: 0x8042
    }
}

// old:
// EntityMotionStart
// EntityMotionEnd
// EntityMotionStartEnd
// ActivityChange
// EntityActivityIcon
// ResetMotion

