use ast::*;


#[derive(Clone, Debug)]
pub struct PipelineB(Pipeline);

impl PipelineB {
    pub fn new() -> PipelineB {
        PipelineB(Pipeline {
            items: Vec::new(),
        })
    }

    pub fn add_item(&mut self, i: Item) {
        self.0.items.push(i);
    }

    pub fn add_pass(&mut self, p: Pass) {
        self.add_item(Item::Pass(p));
    }

    pub fn add_type_alias(&mut self, name: impl Into<String>, ty: impl Into<Ty>) {
        self.add_item(Item::TypeAlias(name.into(), ty.into()));
    }

    pub fn pass(&mut self,
                name: impl Into<String>,
                f: impl FnOnce(&mut PassB) -> &mut PassB) -> &mut Self {
        let mut pb = PassB::new(name, false);
        f(&mut pb);
        self.add_pass(pb.into());
        self
    }

    pub fn pub_pass(&mut self,
                    name: impl Into<String>,
                    f: impl FnOnce(&mut PassB) -> &mut PassB) -> &mut Self {
        let mut pb = PassB::new(name, true);
        f(&mut pb);
        self.add_pass(pb.into());
        self
    }

    pub fn type_alias(&mut self, name: impl Into<String>, ty: impl Into<Ty>) -> &mut Self {
        self.add_type_alias(name, ty);
        self
    }
}

impl From<PipelineB> for Pipeline {
    fn from(x: PipelineB) -> Pipeline { x.0 }
}


#[derive(Clone, Debug)]
pub struct PassB(Pass);

impl PassB {
    pub fn new(name: impl Into<String>, public: bool) -> PassB {
        PassB(Pass {
            name: name.into(),
            public,
            inputs: Vec::new(),
            outputs: Vec::new(),
            stmts: Vec::new(),
        })
    }

    pub fn input(&mut self, name: impl Into<String>, ty: impl Into<Ty>) -> &mut Self {
        self.0.inputs.push(Param {
            name: name.into(),
            ty: ty.into(),
        });
        self
    }

    pub fn output(&mut self, name: impl Into<String>, ty: impl Into<Ty>) -> &mut Self {
        self.0.outputs.push(Param {
            name: name.into(),
            ty: ty.into(),
        });
        self
    }

    pub fn local(&mut self,
                 name: impl Into<String>,
                 ty: impl Into<Ty>,
                 f: impl FnOnce(&mut ExprVecB) -> &mut ExprVecB) -> &mut Self {
        let mut evb = ExprVecB::new();
        f(&mut evb);
        self.0.stmts.push(Stmt::Local(Local {
            name: name.into(),
            ty: ty.into(),
            init: evb.into(),
        }));
        self
    }

    pub fn clear(&mut self,
                 f: impl FnOnce(&mut ClearStmtB) -> &mut ClearStmtB) -> &mut Self {
        let mut csb = ClearStmtB::new();
        f(&mut csb);
        self.0.stmts.push(Stmt::Clear(csb.into()));
        self
    }

    pub fn render(&mut self,
                  vert: impl Into<String>,
                  frag: impl Into<String>,
                  f: impl FnOnce(&mut RenderStmtB) -> &mut RenderStmtB) -> &mut Self {
        let mut rsb = RenderStmtB::new(vert, frag);
        f(&mut rsb);
        self.0.stmts.push(Stmt::Render(rsb.into()));
        self
    }

    pub fn call(&mut self,
                pass: impl Into<String>,
                f: impl FnOnce(&mut CallStmtB) -> &mut CallStmtB) -> &mut Self {
        let mut csb = CallStmtB::new(pass);
        f(&mut csb);
        self.0.stmts.push(Stmt::Call(csb.into()));
        self
    }
}

impl From<PassB> for Pass {
    fn from(x: PassB) -> Pass { x.0 }
}


#[derive(Clone, Debug)]
pub struct ClearStmtB(ClearStmt);

impl ClearStmtB {
    pub fn new() -> ClearStmtB {
        ClearStmtB(ClearStmt {
            color: None,
            depth: None,
            outputs: DrawOutputs {
                color: Vec::new(),
                depth: None,
                viewport: None,
            },
        })
    }

    pub fn color(&mut self, color: impl Into<Expr>) -> &mut Self {
        assert!(self.0.color.is_none(), "clear color is already set");
        self.0.color = Some(color.into());
        self
    }

    pub fn depth(&mut self, depth: impl Into<Expr>) -> &mut Self {
        assert!(self.0.depth.is_none(), "clear depth is already set");
        self.0.depth = Some(depth.into());
        self
    }

    pub fn out_color(&mut self, color: impl Into<Expr>) -> &mut Self {
        self.0.outputs.color.push(color.into());
        self
    }

    pub fn out_depth(&mut self, depth: impl Into<Expr>) -> &mut Self {
        assert!(self.0.outputs.depth.is_none(), "depth output is already set");
        self.0.outputs.depth = Some(depth.into());
        self
    }

    pub fn out_viewport(&mut self, viewport: impl Into<Expr>) -> &mut Self {
        assert!(self.0.outputs.viewport.is_none(), "viewport output is already set");
        self.0.outputs.viewport = Some(viewport.into());
        self
    }
}

impl From<ClearStmtB> for ClearStmt {
    fn from(x: ClearStmtB) -> ClearStmt { x.0 }
}


#[derive(Clone, Debug)]
pub struct RenderStmtB(RenderStmt);

impl RenderStmtB {
    pub fn new(vert: impl Into<String>, frag: impl Into<String>) -> RenderStmtB {
        RenderStmtB(RenderStmt {
            shader: Shader {
                vertex: vert.into(),
                fragment: frag.into(),
            },
            bindings: Vec::new(),
            outputs: DrawOutputs {
                color: Vec::new(),
                depth: None,
                viewport: None,
            },
            config: RenderConfig {
                blend_mode: BlendMode::None,
                depth_test: DepthTest::Disable,
            },
        })
    }

    pub fn binding(&mut self,
                   name: impl Into<String>,
                   scope: Scope,
                   ty: impl Into<Ty>,
                   expr: impl Into<Expr>) -> &mut Self {
        self.0.bindings.push(Binding {
            name: name.into(),
            scope,
            ty: ty.into(),
            expr: expr.into(),
        });
        self
    }

    pub fn uniform(&mut self,
                   name: impl Into<String>,
                   ty: impl Into<Ty>,
                   expr: impl Into<Expr>) -> &mut Self {
        self.binding(name, Scope::Uniform, ty, expr)
    }

    pub fn vertex(&mut self,
                  name: impl Into<String>,
                  ty: impl Into<Ty>,
                  expr: impl Into<Expr>) -> &mut Self {
        self.binding(name, Scope::Attrib, ty, expr)
    }

    pub fn instance(&mut self,
                    name: impl Into<String>,
                    ty: impl Into<Ty>,
                    expr: impl Into<Expr>) -> &mut Self {
        self.binding(name, Scope::Instance, ty, expr)
    }

    pub fn out_color(&mut self, color: impl Into<Expr>) -> &mut Self {
        self.0.outputs.color.push(color.into());
        self
    }

    pub fn out_depth(&mut self, depth: impl Into<Expr>) -> &mut Self {
        assert!(self.0.outputs.depth.is_none(), "depth output is already set");
        self.0.outputs.depth = Some(depth.into());
        self
    }

    pub fn out_viewport(&mut self, viewport: impl Into<Expr>) -> &mut Self {
        assert!(self.0.outputs.viewport.is_none(), "viewport output is already set");
        self.0.outputs.viewport = Some(viewport.into());
        self
    }

    pub fn blend_mode(&mut self, mode: BlendMode) -> &mut Self {
        self.0.config.blend_mode = mode;
        self
    }

    pub fn depth_test(&mut self, test: DepthTest) -> &mut Self {
        self.0.config.depth_test = test;
        self
    }
}

impl From<RenderStmtB> for RenderStmt {
    fn from(x: RenderStmtB) -> RenderStmt { x.0 }
}


#[derive(Clone, Debug)]
pub struct CallStmtB(CallStmt);

impl CallStmtB {
    pub fn new(pass: impl Into<String>) -> CallStmtB {
        CallStmtB(CallStmt {
            pass: pass.into(),
            inputs: Vec::new(),
            outputs: Vec::new(),
        })
    }

    pub fn input(&mut self, name: impl Into<String>, expr: impl Into<Expr>) -> &mut Self {
        self.0.inputs.push(Arg {
            name: name.into(),
            expr: expr.into(),
        });
        self
    }

    pub fn output(&mut self, name: impl Into<String>, expr: impl Into<Expr>) -> &mut Self {
        self.0.outputs.push(Arg {
            name: name.into(),
            expr: expr.into(),
        });
        self
    }
}

impl From<CallStmtB> for CallStmt {
    fn from(x: CallStmtB) -> CallStmt { x.0 }
}


#[derive(Clone, Debug)]
pub struct TyB(Ty);

impl TyB {
    pub fn u8() -> TyB { TyB(Ty::Prim(PrimTy::Uint(IntSize::I8))) }
    pub fn u16() -> TyB { TyB(Ty::Prim(PrimTy::Uint(IntSize::I16))) }
    pub fn u32() -> TyB { TyB(Ty::Prim(PrimTy::Uint(IntSize::I32))) }
    pub fn i8() -> TyB { TyB(Ty::Prim(PrimTy::Int(IntSize::I8))) }
    pub fn i16() -> TyB { TyB(Ty::Prim(PrimTy::Int(IntSize::I16))) }
    pub fn i32() -> TyB { TyB(Ty::Prim(PrimTy::Int(IntSize::I32))) }
    pub fn f32() -> TyB { TyB(Ty::Prim(PrimTy::Float)) }

    pub fn vec(self, len: u8) -> TyB {
        match self.0 {
            Ty::Prim(p) => TyB(Ty::Vec(p, len)),
            _ => panic!("vec() only works on primitive types"),
        }
    }

    pub fn texture(fmt: TexFormat) -> TyB { TyB(Ty::Texture(fmt)) }
    pub fn tex_rgb() -> TyB { TyB(Ty::Texture(TexFormat::RGB)) }
    pub fn tex_rgba() -> TyB { TyB(Ty::Texture(TexFormat::RGBA)) }
    pub fn tex_depth() -> TyB { TyB(Ty::Texture(TexFormat::Depth)) }

    pub fn geometry(f: impl FnOnce(&mut FieldVecB) -> &mut FieldVecB) -> TyB {
        let mut fvb = FieldVecB::new();
        f(&mut fvb);
        TyB(Ty::Geometry(fvb.into()))
    }

    pub fn alias(name: impl Into<String>) -> TyB {
        TyB(Ty::Alias(name.into()))
    }
}

impl From<TyB> for Ty {
    fn from(x: TyB) -> Ty { x.0 }
}


#[derive(Clone, Debug)]
pub struct ExprB(Expr);

impl ExprB {
    pub fn u8(x: u8) -> ExprB { ExprB(Expr::ConstU(IntSize::I8, x as u32)) }
    pub fn u16(x: u16) -> ExprB { ExprB(Expr::ConstU(IntSize::I16, x as u32)) }
    pub fn u32(x: u32) -> ExprB { ExprB(Expr::ConstU(IntSize::I32, x)) }
    pub fn i8(x: i8) -> ExprB { ExprB(Expr::ConstI(IntSize::I8, x as i32)) }
    pub fn i16(x: i16) -> ExprB { ExprB(Expr::ConstI(IntSize::I16, x as i32)) }
    pub fn i32(x: i32) -> ExprB { ExprB(Expr::ConstI(IntSize::I32, x)) }
    pub fn f32(x: f32) -> ExprB { ExprB(Expr::ConstF(x)) }


    pub fn u8_vec(xs: &[u8]) -> ExprB {
        ExprB(Expr::Ctor(TyB::u8().vec(xs.len() as u8).into(),
                         xs.iter().map(|&x| ExprB::u8(x).into()).collect()))
    }

    pub fn u16_vec(xs: &[u16]) -> ExprB {
        ExprB(Expr::Ctor(TyB::u16().vec(xs.len() as u8).into(),
                         xs.iter().map(|&x| ExprB::u16(x).into()).collect()))
    }

    pub fn u32_vec(xs: &[u32]) -> ExprB {
        ExprB(Expr::Ctor(TyB::u32().vec(xs.len() as u8).into(),
                         xs.iter().map(|&x| ExprB::u32(x).into()).collect()))
    }

    pub fn i8_vec(xs: &[i8]) -> ExprB {
        ExprB(Expr::Ctor(TyB::i8().vec(xs.len() as u8).into(),
                         xs.iter().map(|&x| ExprB::i8(x).into()).collect()))
    }

    pub fn i16_vec(xs: &[i16]) -> ExprB {
        ExprB(Expr::Ctor(TyB::i16().vec(xs.len() as u8).into(),
                         xs.iter().map(|&x| ExprB::i16(x).into()).collect()))
    }

    pub fn i32_vec(xs: &[i32]) -> ExprB {
        ExprB(Expr::Ctor(TyB::i32().vec(xs.len() as u8).into(),
                         xs.iter().map(|&x| ExprB::i32(x).into()).collect()))
    }

    pub fn f32_vec(xs: &[f32]) -> ExprB {
        ExprB(Expr::Ctor(TyB::f32().vec(xs.len() as u8).into(),
                         xs.iter().map(|&x| ExprB::f32(x).into()).collect()))
    }


    pub fn var(name: impl Into<String>) -> ExprB {
        ExprB(Expr::Var(name.into()))
    }

    pub fn field(self, field: impl Into<String>) -> ExprB {
        ExprB(Expr::Field(Box::new(self.into()), field.into()))
    }

    pub fn ctor(ty: impl Into<Ty>,
                f: impl FnOnce(&mut ExprVecB) -> &mut ExprVecB) -> ExprB {
        let mut evb = ExprVecB::new();
        f(&mut evb);
        ExprB(Expr::Ctor(ty.into(), evb.into()))
    }
}

impl From<ExprB> for Expr {
    fn from(x: ExprB) -> Expr { x.0 }
}


#[derive(Clone, Debug)]
pub struct ExprVecB(Vec<Expr>);

impl ExprVecB {
    pub fn new() -> ExprVecB { ExprVecB(Vec::new()) }

    pub fn init_arg(&mut self, e: impl Into<Expr>) -> &mut Self {
        self.0.push(e.into());
        self
    }
}

impl From<ExprVecB> for Vec<Expr> {
    fn from(x: ExprVecB) -> Vec<Expr> { x.0 }
}


#[derive(Clone, Debug)]
pub struct FieldVecB(Vec<Field>);

impl FieldVecB {
    pub fn new() -> FieldVecB { FieldVecB(Vec::new()) }

    pub fn field(&mut self, name: impl Into<String>, ty: impl Into<Ty>) -> &mut Self {
        self.0.push(Field {
            name: name.into(),
            ty: ty.into(),
        });
        self
    }
}

impl From<FieldVecB> for Vec<Field> {
    fn from(x: FieldVecB) -> Vec<Field> { x.0 }
}


pub fn u8() -> TyB { TyB::u8() }
pub fn u16() -> TyB { TyB::u16() }
pub fn u32() -> TyB { TyB::u32() }
pub fn i8() -> TyB { TyB::i8() }
pub fn i16() -> TyB { TyB::i16() }
pub fn i32() -> TyB { TyB::i32() }
pub fn f32() -> TyB { TyB::f32() }
pub fn geometry(f: impl FnOnce(&mut FieldVecB) -> &mut FieldVecB) -> TyB {
    TyB::geometry(f)
}

pub fn var(name: impl Into<String>) -> ExprB { ExprB::var(name) }

pub fn pipeline(f: impl FnOnce(&mut PipelineB) -> &mut PipelineB) -> Pipeline {
    let mut pb = PipelineB::new();
    f(&mut pb);
    pb.into()
}

pub fn u8_val(x: u8) -> ExprB { ExprB::u8(x) }
pub fn u16_val(x: u16) -> ExprB { ExprB::u16(x) }
pub fn u32_val(x: u32) -> ExprB { ExprB::u32(x) }
pub fn i8_val(x: i8) -> ExprB { ExprB::i8(x) }
pub fn i16_val(x: i16) -> ExprB { ExprB::i16(x) }
pub fn i32_val(x: i32) -> ExprB { ExprB::i32(x) }
pub fn f32_val(x: f32) -> ExprB { ExprB::f32(x) }

pub fn u8_vec(xs: &[u8]) -> ExprB { ExprB::u8_vec(xs) }
pub fn u16_vec(xs: &[u16]) -> ExprB { ExprB::u16_vec(xs) }
pub fn u32_vec(xs: &[u32]) -> ExprB { ExprB::u32_vec(xs) }
pub fn i8_vec(xs: &[i8]) -> ExprB { ExprB::i8_vec(xs) }
pub fn i16_vec(xs: &[i16]) -> ExprB { ExprB::i16_vec(xs) }
pub fn i32_vec(xs: &[i32]) -> ExprB { ExprB::i32_vec(xs) }
pub fn f32_vec(xs: &[f32]) -> ExprB { ExprB::f32_vec(xs) }
