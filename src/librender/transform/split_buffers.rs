//! Split `Clear` and `Render` statements with multiple color outputs into multiple statements with
//! one color output each.
use intern::Interners;
use ir::*;
use transform::stmts;

pub fn split_buffers<'a>(intern: &Interners<'a>, pass: Pass<'a>) -> Pass<'a> {
    stmts::rewrite_pass(intern, pass, |mut curs| {
        match *curs.get() {
            Stmt::Local(_) => curs.keep(),

            Stmt::Clear(cs) => {
                if cs.outputs.color.len() <= 1 {
                    curs.keep();
                    return;
                }

                curs.delete();
                for &color in cs.outputs.color {
                    let mut new_cs = cs;
                    new_cs.outputs.color = intern.slice(&[color]);
                    curs.insert(Stmt::Clear(new_cs));
                }
            },

            Stmt::Render(rs) => {
                if rs.outputs.color.len() <= 1 {
                    curs.keep();
                    return;
                }

                curs.delete();
                for (i, &color) in rs.outputs.color.iter().enumerate() {
                    let mut new_rs = rs;
                    new_rs.outputs.color = intern.slice(&[color]);
                    super::add_def(intern, &mut new_rs,
                                   "#define OUTPOST_NO_DRAW_BUFFERS");
                    super::add_def(intern, &mut new_rs,
                                   format_args!("#define OUTPOST_CUR_OUTPUT_IDX {}", i));
                    if i > 0 && rs.config.depth_test != DepthTest::Disable {
                        new_rs.config.depth_test = DepthTest::Equal;
                    }
                    curs.insert(Stmt::Render(new_rs));
                }
            },

            Stmt::Call(_) => curs.keep(),
        }
    })
}
