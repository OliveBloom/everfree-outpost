//! Eliminate use of the `depth_texture` extension by passing depth information through a color
//! texture instead.
use std::collections::HashMap;

use intern::Interners;
use ir::*;
use transform::stmts;

pub fn replace_depth<'a>(intern: &Interners<'a>, pass: Pass<'a>) -> Pass<'a> {
    // Identify depth textures, and distinguish full textures from renderbuffers.

    // For each depth texture, records whether it's been used as an input to some render statment.
    // NB: This analysis assumes copy propagation has already occurred.
    let mut depth_used: HashMap<VarId, bool> = HashMap::new();

    for (id, v) in pass.vars.iter() {
        if let TyKind::Texture(TexFormat::Depth) = *v.ty.resolve() {
            depth_used.insert(id, false);
        }
    }

    for s in pass.stmts {
        if let Stmt::Render(rs) = *s {
            for u in rs.uniforms {
                if let Some(used) = depth_used.get_mut(&u.var) {
                    *used = true;
                }
            }
        }
    }


    let mut replace_tex = HashMap::new();

    stmts::rewrite_pass(intern, pass, |mut curs| {
        match *curs.get() {
            Stmt::Local(ls) => {
                curs.keep();

                if let Some(&true) = depth_used.get(&ls.var) {
                    match ls.insn {
                        Insn::TextureCtor(_, size) => {
                            let old_name = pass.vars[ls.var].name;
                            let new_var = curs.insert_local(
                                intern.symbol(&format!("{}__rd_tex", old_name)),
                                // TODO: used 2- or 3-channel format if possible
                                intern.ty(&TyKind::Texture(TexFormat::RGBA)),
                                Insn::TextureCtor(TexFormat::RGBA, size),
                            );
                            replace_tex.insert(ls.var, new_var);
                        },
                        _ => panic!("unsupported depth texture initializer {:?}", ls.insn),
                    }
                }
            },

            Stmt::Clear(cs) => {
                curs.keep();

                if let (Some(tex), Some(depth_val)) = (cs.outputs.depth, cs.depth) {
                    if let Some(&repl) = replace_tex.get(&tex) {
                        // Type should match `DEPTH_TY_KIND` in `front.rs`
                        assert!(pass.vars[depth_val].ty.resolve() == &TyKind::Prim(PrimTy::Float));

                        let color_val = curs.insert_local(
                            intern.symbol("__rd_depth_color"),
                            intern.ty(&TyKind::Vec(PrimTy::Float, 4)),
                            // TODO: Need some arithmetic ops to properly initialize the color.
                            // Copying the depth value to all four channels is valid for 0.0 and
                            // 1.0, but loses bits in other cases.
                            Insn::VecCtor(PrimTy::Float, intern.slice(&[depth_val; 4])),
                        );

                        curs.insert(Stmt::Clear(ClearStmt {
                            color: Some(color_val),
                            depth: None,
                            outputs: DrawOutputs {
                                color: intern.slice(&[repl]),
                                depth: None,
                                viewport: cs.outputs.viewport,
                            },
                        }));
                    }
                }
            },

            Stmt::Render(rs) => {
                let has_input = rs.uniforms.iter()
                    .any(|u| replace_tex.contains_key(&u.var));
                let has_output = rs.outputs.depth
                    .map_or(false, |d| replace_tex.contains_key(&d));
                if !has_input && !has_output {
                    curs.keep();
                    return;
                }

                let mut new_rs = rs;

                super::add_def(intern, &mut new_rs,
                               "#define OUTPOST_NO_DEPTH_TEXTURE");

                if has_input {
                    let mut new_uniforms = rs.uniforms.to_owned();
                    for u in &mut new_uniforms {
                        if let Some(&repl) = replace_tex.get(&u.var) {
                            u.var = repl;
                            u.ty = intern.ty(&TyKind::Texture(TexFormat::RGBA));
                        }
                    }
                    new_rs.uniforms = intern.slice(&new_uniforms);
                }

                if let Some(tex) = rs.outputs.depth {
                    if let Some(&repl) = replace_tex.get(&tex) {
                        let mut new_color = Vec::with_capacity(rs.outputs.color.len() + 1);
                        new_color.extend_from_slice(rs.outputs.color);
                        new_color.push(repl);
                        new_rs.outputs.color = intern.slice(&new_color);

                        super::add_def(intern, &mut new_rs,
                                       format_args!("#define OUTPOST_DEPTH_OUT_IDX {}",
                                                    new_color.len() - 1));
                    }
                }

                curs.insert(Stmt::Render(new_rs));
            },

            Stmt::Call(_) => curs.keep(),
        }
    })
}
