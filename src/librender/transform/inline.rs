use intern::Interners;
use ir::*;
use ir_builder::PassBuilder;
use transform::subst::Substitution;

struct Frame<'a: 'i, 'i> {
    id: PassId,
    /// In this substitution, the "old" ID is the ID as it appears in the original definition of
    /// the pass being inlined, and the "new" ID is the one used in the generated (post-inlining)
    /// pass.
    subst: Substitution<'a, 'i>,
}

struct Stack<'a: 'i, 'i>(Vec<Frame<'a, 'i>>);

impl<'a, 'i> Stack<'a, 'i> {
    fn cur(&mut self) -> &mut Frame<'a, 'i> {
        self.0.last_mut().expect("empty stack")
    }

    fn subst(&mut self) -> &mut Substitution<'a, 'i> {
        &mut self.cur().subst
    }
}

struct Inliner<'a: 'i, 'i> {
    intern: &'i Interners<'a>,
    pipeline: Pipeline<'a>,
    builder: PassBuilder<'a>,
    stack: Stack<'a, 'i>,
}

impl<'a, 'i> Inliner<'a, 'i> {
    pub fn new(intern: &'i Interners<'a>,
               pipeline: Pipeline<'a>) -> Inliner<'a, 'i> {
        Inliner {
            intern, pipeline,
            builder: PassBuilder::new(),
            stack: Stack(Vec::new()),
        }
    }

    fn root_subst(&mut self, pass_id: PassId) -> Substitution<'a, 'i> {
        let mut subst = Substitution::new(self.intern);
        let pass = &self.pipeline.passes[pass_id];

        for &old_id in pass.inputs {
            let v = &pass.vars[old_id];
            let new_id = self.builder.input(v.name, v.ty);
            subst.rename(old_id, new_id);
        }

        for &old_id in pass.outputs {
            let v = &pass.vars[old_id];
            let new_id = self.builder.output(v.name, v.ty);
            subst.rename(old_id, new_id);
        }

        subst
    }

    fn call_subst(&mut self, call: CallStmt<'a>) -> Substitution<'a, 'i> {
        let mut subst = Substitution::new(self.intern);
        let pass = &self.pipeline.passes[call.pass];

        for (&old_id, &new_id) in pass.inputs.iter().zip(call.inputs.iter()) {
            let new_id = self.stack.subst().subst_var_id(new_id);
            subst.rename(old_id, new_id);
        }

        for (&old_id, &new_id) in pass.outputs.iter().zip(call.outputs.iter()) {
            let new_id = self.stack.subst().subst_var_id(new_id);
            subst.rename(old_id, new_id);
        }

        subst
    }

    fn body(&mut self, pass: Pass<'a>) {
        for &s in pass.stmts {
            match s {
                Stmt::Local(l) => {
                    let old_id = l.var;
                    let v = &pass.vars[old_id];
                    let insn = self.stack.subst().subst_insn(l.insn);
                    let new_id = self.builder.local(v.name, v.ty, insn);
                    self.stack.subst().rename(old_id, new_id);
                },

                Stmt::Call(c) => {
                    self.do_call(c);
                },

                s => {
                    let s = self.stack.subst().subst_stmt(s);
                    self.builder.stmt(s);
                },
            }
        }
    }

    fn do_call(&mut self, call: CallStmt<'a>) {
        if self.stack.0.iter().any(|f| f.id == call.pass) {
            panic!("infinite recursion detected - {:?} ({:?}) calls itself",
                   call.pass, self.pipeline.passes[call.pass].name);
        }

        let new_subst = self.call_subst(call);
        self.stack.0.push(Frame {
            id: call.pass,
            subst: new_subst,
        });
        self.body(self.pipeline.passes[call.pass]);
        self.stack.0.pop();
    }

    pub fn flatten_pass(mut self, id: PassId) -> Pass<'a> {
        let pass = self.pipeline.passes[id];

        let new_subst = self.root_subst(id);
        self.stack.0.push(Frame {
            id: id,
            subst: new_subst,
        });
        self.body(pass);
        self.stack.0.pop();

        self.builder.finish(self.intern, pass.name, pass.public)
    }
}


pub fn flatten_pass<'a>(intern: &Interners<'a>,
                        pipeline: Pipeline<'a>,
                        id: PassId) -> Pass<'a> {
    Inliner::new(intern, pipeline).flatten_pass(id)
}
