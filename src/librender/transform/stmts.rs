use intern::Interners;
use ir::*;
use ir_builder::PassBuilder;
use transform::subst::Substitution;
use util::{IndexVec, IndexSlice};

struct Context<'a: 'i, 'i> {
    builder: PassBuilder<'a>,
    subst: Substitution<'a, 'i>,
    next_old_var: u32,
    old_vars: &'a IndexSlice<VarId, Var<'a>>,
}

impl<'a, 'i> Context<'a, 'i> {
    /// Generate a fresh ID in the old variable namespace.  The resulting ID can then be used in
    /// stmts or insns passed in to `Cursor` methods, as long as the caller sets up a mapping to
    /// some new ID in the subst.
    fn next_old_var(&mut self) -> VarId {
        let id = VarId(self.next_old_var);
        self.next_old_var += 1;
        id
    }

    fn init_inputs(&mut self, inputs: &[VarId]) {
        for &old_id in inputs {
            let v = &self.old_vars[old_id];
            let new_id = self.builder.input(v.name, v.ty);
            self.subst.rename(old_id, new_id);
        }
    }

    fn init_outputs(&mut self, outputs: &[VarId]) {
        for &old_id in outputs {
            let v = &self.old_vars[old_id];
            let new_id = self.builder.output(v.name, v.ty);
            self.subst.rename(old_id, new_id);
        }
    }

    fn cursor<'b>(&'b mut self, stmt: &'b Stmt<'a>) -> Cursor<'a, 'i, 'b> {
        Cursor::new(self, stmt)
    }
}

pub struct Cursor<'a: 'i, 'i: 'b, 'b> {
    cx: &'b mut Context<'a, 'i>,
    stmt: &'b Stmt<'a>,
    used: bool,
}

#[allow(dead_code)]
impl<'a, 'i, 'b> Cursor<'a, 'i, 'b> {
    fn new(cx: &'b mut Context<'a, 'i>,
           stmt: &'b Stmt<'a>) -> Cursor<'a, 'i, 'b> {
        Cursor { cx, stmt, used: false }
    }

    pub fn get(&self) -> &'b Stmt<'a> {
        self.stmt
    }

    pub fn insert(&mut self, stmt: Stmt<'a>) {
        if let Stmt::Local(_) = stmt {
            panic!("`insert` does not support Stmt::Local - use `insert_local` instead");
        }
        let stmt = self.cx.subst.subst_stmt(stmt);
        self.cx.builder.stmt(stmt);
    }

    /// Insert a `Local` statement at the current position.  Returns a `VarId` that can be used in
    /// later statements to refer to the new variable.
    pub fn insert_local(&mut self,
                        name: Symbol<'a>,
                        ty: Ty<'a>,
                        insn: Insn<'a>) -> VarId {
        let insn = self.cx.subst.subst_insn(insn);
        let old_id = self.cx.next_old_var();
        let new_id = self.cx.builder.local(name, ty, insn);
        self.cx.subst.rename(old_id, new_id);
        old_id
    }

    /// Replace the `Insn` of the current `Local` statement.  Returns the `VarId` of the local for
    /// convenience (this is always the same as `self.get()`'s `LocalStmt::var`).
    pub fn replace_local(&mut self, insn: Insn<'a>) -> VarId {
        assert!(!self.used, "replace_local: current stmt has already been consumed");
        let insn = self.cx.subst.subst_insn(insn);
        let old_id = match *self.stmt {
            Stmt::Local(l) => l.var,
            _ => panic!("`replace_local`: current stmt is not a `Local`"),
        };
        let v = &self.cx.old_vars[old_id];
        let new_id = self.cx.builder.local(v.name, v.ty, insn);
        self.cx.subst.rename(old_id, new_id);
        self.used = true;
        old_id
    }

    /// Delete the current `Local` stmt, and replace all its uses with uses of some other `var`.
    pub fn replace_with_var(&mut self, var: VarId) {
        assert!(!self.used, "replace_local: current stmt has already been consumed");
        let old_id = match *self.stmt {
            Stmt::Local(l) => l.var,
            _ => panic!("`replace_local`: current stmt is not a `Local`"),
        };
        // Whatever new ID `var` maps to, make `old_id` map to the same thing.
        let new_id = self.cx.subst.subst_var_id(var);
        self.cx.subst.rename(old_id, new_id);
        self.used = true;
    }

    pub fn keep(&mut self) {
        assert!(!self.used, "keep: current stmt has already been consumed");
        match *self.stmt {
            Stmt::Local(l) => { self.replace_local(l.insn); },
            s => { self.insert(s); },
        }
        self.used = true;
    }

    pub fn delete(&mut self) {
        assert!(!self.used, "keep: current stmt has already been consumed");
        match *self.stmt {
            Stmt::Local(l) => {
                // Mark var as explicitly removed.
                self.cx.subst.remove(l.var);
            },
            _ => {},
        }
        self.used = true;
    }
}

impl<'a, 'i, 'b> Drop for Cursor<'a, 'i, 'b> {
    fn drop(&mut self) {
        if !self.used {
            self.delete();
        }
    }
}

pub fn rewrite_pass<'a, 'i, F>(intern: &'i Interners<'a>,
                               pass: Pass<'a>,
                               mut callback: F) -> Pass<'a>
        where F: FnMut(Cursor<'a, 'i, '_>) {
    let mut cx = Context {
        builder: PassBuilder::new(),
        subst: Substitution::new(intern),
        next_old_var: pass.vars.len() as u32,
        old_vars: pass.vars,
    };
    cx.init_inputs(pass.inputs);
    cx.init_outputs(pass.outputs);

    for s in pass.stmts {
        callback(cx.cursor(s));
    }

    cx.builder.finish(intern, pass.name, pass.public)
}

#[allow(dead_code)]
pub fn rewrite_pipeline<'a, 'i, F>(intern: &'i Interners<'a>,
                                   pipeline: Pipeline<'a>,
                                   mut callback: F) -> Pipeline<'a>
        where F: FnMut(&'a Pass<'a>, Cursor<'a, 'i, '_>) {
    let mut passes = IndexVec::with_capacity(pipeline.passes.len());
    for (_, pass) in pipeline.passes.iter() {
        passes.push(rewrite_pass(intern, *pass, |curs| callback(pass, curs)));
    }
    Pipeline {
        passes: intern.index_slice(&passes),
    }
}
