extern crate rustc_arena;

extern crate asmgl;

#[macro_use]
mod util;

pub mod ast;
pub mod ast_builder;
pub mod traits;

mod analysis;
mod back;
mod front;
mod intern;
mod ir;
mod ir_builder;
mod print;
mod transform;
mod vm;
mod vm_builder;

pub use back::asmgl::Backend as AsmglBackend;
pub use traits::*;
