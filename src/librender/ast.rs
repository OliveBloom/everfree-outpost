#[derive(Clone, Debug)]
pub struct Pipeline {
    pub items: Vec<Item>,
}

#[derive(Clone, Debug)]
pub enum Item {
    Pass(Pass),
    TypeAlias(String, Ty),
}

// Types

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum IntSize { I8, I16, I32 }

impl IntSize {
    pub fn size(self) -> usize {
        match self {
            IntSize::I8 => 1,
            IntSize::I16 => 2,
            IntSize::I32 => 4,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum PrimTy {
    Float,
    Uint(IntSize),
    Int(IntSize),
}

impl PrimTy {
    pub fn size(self) -> usize {
        match self {
            PrimTy::Float => 4,
            PrimTy::Uint(i) | PrimTy::Int(i) => i.size(),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum TexFormat {
    Red,
    RGB,
    RGBA,
    Depth,
}

#[derive(Clone, Debug)]
pub struct Field {
    pub name: String,
    pub ty: Ty,
}

#[derive(Clone, Debug)]
pub enum Ty {
    Prim(PrimTy),
    Vec(PrimTy, u8),
    Texture(TexFormat),
    /// Geometry with the given vertex struct layout.
    Geometry(Vec<Field>),
    Alias(String),
}

// Passes

#[derive(Clone, Debug)]
pub struct Param {
    pub name: String,
    pub ty: Ty,
}

#[derive(Clone, Debug)]
pub struct Pass {
    pub name: String,
    /// Indicates whether this pass is a possible entry point.
    pub public: bool,
    pub inputs: Vec<Param>,
    pub outputs: Vec<Param>,
    pub stmts: Vec<Stmt>,
}

#[derive(Clone, Debug)]
pub enum Stmt {
    Local(Local),
    Clear(ClearStmt),
    Render(RenderStmt),
    Call(CallStmt),
}

#[derive(Clone, Debug)]
pub struct Local {
    pub name: String,
    pub ty: Ty,
    /// Initializer arguments, like in `Expr::Ctor`.
    pub init: Vec<Expr>,
}

#[derive(Clone, Debug)]
pub struct ClearStmt {
    pub color: Option<Expr>,
    pub depth: Option<Expr>,
    pub outputs: DrawOutputs,
}

#[derive(Clone, Debug)]
pub struct Arg {
    pub name: String,
    pub expr: Expr,
}

#[derive(Clone, Debug)]
pub struct CallStmt {
    pub pass: String,
    /// Input arguments.  Names must match those in the pass signature, though they may appear in a
    /// different order.
    pub inputs: Vec<Arg>,
    pub outputs: Vec<Arg>,
}

// Render

#[derive(Clone, Debug)]
pub struct RenderStmt {
    pub shader: Shader,
    /// Uniform (including texture sampler), vertex attribute, and instance attribute bindings.
    pub bindings: Vec<Binding>,
    pub outputs: DrawOutputs,
    pub config: RenderConfig,
}

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct Shader {
    pub vertex: String,
    pub fragment: String,
}

/// Output buffers for a draw command (`Clear` or `Render`).
#[derive(Clone, Debug)]
pub struct DrawOutputs {
    pub color: Vec<Expr>,
    pub depth: Option<Expr>,
    pub viewport: Option<Expr>,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum BlendMode {
    /// Disable blending.
    None,
    /// Normal non-premultiplied alpha blending: `rgb * a + old_rgb * (1 - a)`.
    Alpha,
    /// Additive blending: `rgb + old_rgb`.
    Add,
    /// Blending for light sources, where the new color indicates how far to pull the old color
    /// toward pure white: `rgb * (1 - old_rgb) + old_rgb`.
    MultiplyInv,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum DepthTest {
    Disable,
    GEqual,
    Equal,
    Always,
}

/// Fixed-function render pipeline settings.
#[derive(Clone, Copy, Debug)]
pub struct RenderConfig {
    pub blend_mode: BlendMode,
    pub depth_test: DepthTest,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Scope {
    Uniform,
    Attrib,
    Instance,
}

#[derive(Clone, Debug)]
pub struct Binding {
    pub name: String,
    pub scope: Scope,
    pub ty: Ty,
    pub expr: Expr,
}

// Expressions

#[derive(Clone, Debug)]
pub enum Expr {
    ConstI(IntSize, i32),
    ConstU(IntSize, u32),
    ConstF(f32),

    Var(String),
    Field(Box<Expr>, String),
    /// Construct a value of a given `Ty`, using the `Expr`s as initializer arguments.
    ///
    ///  * `Ty::Vec(prim, size)`: requires `size` args of type `prim`.
    ///  * `Ty::Texture(_)`: requires two args of any uint type.
    ///
    /// In both cases, an expression producing `Ty::Vec(prim, size)` counts as `size` args of type
    /// `prim`.
    Ctor(Ty, Vec<Expr>),
}
