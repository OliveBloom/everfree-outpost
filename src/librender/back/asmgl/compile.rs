use std::collections::HashMap;
use std::rc::Rc;
use std::usize;

use asmgl as ffi;

use analysis::var_class::{self, VarClass};
use ast::{IntSize, PrimTy};
use ir::{self, VarId};
use vm::{self, Addr, BufferSpec};
use vm_builder::{VmBuilder, Layout, Field};
use util::AlignedBytes;

use super::{
    Pass, TextureHandle,
    Op, ClearInfo, RenderInfo,
    UniformBinding, AttribBinding, TextureBinding, TextureAttachment, BufferUpdate,
    PassBuffer, AttribKind, BufferInfo, Viewport,
};
use super::gl_objects::{Texture, Shader, Buffer, Framebuffer};


struct UniformLayouts<'a> {
    l_args: &'a Layout<VarId>,
    l_uniforms: &'a Layout<VarId>,
}

struct PassCompiler<'a> {
    uniform_layouts: UniformLayouts<'a>,

    geom_info: HashMap<VarId, GeomInfo>,

    shaders: Vec<Shader>,
    buffers: Vec<BufferInfo>,
    framebuffers: Vec<Framebuffer>,
}

struct GeomInfo {
    /// Index of the matching buffer in `buffers`.
    buf_idx: usize,
    /// Layout of each buffer element.
    layout: Layout<usize>,
}

impl<'a> PassCompiler<'a> {
    pub fn new_shader(&mut self, vert: &str, frag: &str, defs: &str) -> usize {
        let id = self.shaders.len();
        self.shaders.push(Shader::new(vert, frag, defs));
        id
    }

    pub fn new_buffer(&mut self, stride: usize) -> usize {
        let id = self.buffers.len();
        self.buffers.push(BufferInfo {
            buffer: Buffer::new(),
            stride,
            len: 0,
        });
        id
    }

    pub fn new_framebuffer(&mut self) -> usize {
        let id = self.framebuffers.len();
        self.framebuffers.push(Framebuffer::new());
        id
    }

    pub fn compile_geom_var(&mut self, v: VarId, fs: &[ir::Field]) -> BufferUpdate {
        let mut layout = Layout::new();
        for (i, f) in fs.iter().enumerate() {
            let (pty, len) = type_rep(f.ty);
            layout.add(i, pty, len);
        }
        let stride = layout.size() as usize;

        let buf = self.new_buffer(stride);

        self.geom_info.insert(v, GeomInfo {
            buf_idx: buf,
            layout,
        });

        BufferUpdate {
            src: v,
            dest: buf,
        }
    }

    pub fn compile_stmt(&mut self, s: &ir::Stmt) -> Option<Op> {
        match *s {
            ir::Stmt::Local(_) => None,
            ir::Stmt::Clear(ref cs) => Some(Op::Clear(self.compile_clear(cs))),
            ir::Stmt::Render(ref rs) => Some(Op::Render(self.compile_render(rs))),
            ir::Stmt::Call(_) => panic!("saw Stmt::Call after inlining?"),
        }
    }

    pub fn compile_clear(&mut self, cs: &ir::ClearStmt) -> ClearInfo {
        let (framebuffer, texture_attachments, viewport) = self.build_outputs(&cs.outputs);

        let color = cs.color.map(|v| self.uniform_layouts.lookup_var(v));
        let depth = cs.depth.map(|v| self.uniform_layouts.lookup_var(v));

        ClearInfo {
            framebuffer,
            color, depth,
            texture_attachments,
            viewport,
        }
    }

    pub fn compile_render(&mut self, rs: &ir::RenderStmt) -> RenderInfo {
        let shader = self.new_shader(rs.shader.vertex.0,
                                     rs.shader.fragment.0,
                                     rs.shader.defs.0);

        let mut uniform_bindings = Vec::new();
        let mut attrib_bindings = Vec::new();
        let mut texture_bindings = Vec::new();
        {
            let s = &self.shaders[shader];
            let mut next_tex_unit = 0;

            // Bind shader so we can initialize texture uniforms.
            s.bind();

            for u in rs.uniforms {
                let loc = s.uniform_loc(u.name.0);
                if let ir::TyKind::Texture(_) = *u.ty.resolve() {
                    texture_bindings.push(TextureBinding {
                        src: u.var,
                        unit: next_tex_unit,
                    });
                    s.set_uniform_1i(loc, next_tex_unit as i32);
                    next_tex_unit += 1;
                } else {
                    let (buf, field) = self.uniform_layouts.lookup_var(u.var);
                    uniform_bindings.push(UniformBinding { loc, buf, field });
                }
            }

            let vert_attribs = rs.vert_attribs.iter().map(|x| (x, AttribKind::Vertex));
            let inst_attribs = rs.inst_attribs.iter().map(|x| (x, AttribKind::Instance));
            for (a, kind) in vert_attribs.chain(inst_attribs) {
                let loc = s.attrib_loc(a.name.0);
                if loc < 0 {
                    continue;
                }
                let gi = &self.geom_info[&a.var];
                let buf = gi.buf_idx;
                let field = gi.layout[&a.field];
                attrib_bindings.push(AttribBinding { loc, buf, field, kind });
            }
        }

        let (framebuffer, texture_attachments, viewport) = self.build_outputs(&rs.outputs);

        RenderInfo {
            shader, framebuffer,

            uniform_bindings,
            attrib_bindings,
            texture_bindings,
            texture_attachments,
            viewport,

            config: rs.config,
        }
    }

    fn build_outputs(&mut self, out: &ir::DrawOutputs)
                     -> (usize, Vec<TextureAttachment>, Viewport) {
        let framebuffer = self.new_framebuffer();

        let mut texture_attachments = Vec::new();
        {
            let fb = &self.framebuffers[framebuffer];

            fb.bind();
            for (i, &v) in out.color.iter().enumerate() {
                texture_attachments.push(TextureAttachment {
                    src: v,
                    attach: i as i8,
                });
            }
            if let Some(v) = out.depth {
                texture_attachments.push(TextureAttachment {
                    src: v,
                    attach: -1,
                });
            }
            unsafe { ffi::asmgl_draw_buffers(out.color.len() as u8) };
        }

        let viewport = match out.viewport {
            Some(v) => {
                let (buf, field) = self.uniform_layouts.lookup_var(v);
                Viewport::Explicit(buf, field)
            },
            None => {
                Viewport::AutoSize(out.color[0])
            },
        };

        (framebuffer, texture_attachments, viewport)
    }
}

impl<'a> UniformLayouts<'a> {
    fn lookup_var(&self, v: VarId) -> (PassBuffer, Field) {
        if let Some(f) = self.l_args.get(&v) {
            (PassBuffer::Args, *f)
        } else if let Some(f) = self.l_uniforms.get(&v) {
            (PassBuffer::Uniforms, *f)
        } else {
            panic!("non-uniform variable {:?} in uniform position", v);
        }
    }
}

/// Get the `vm`-level representation of an `ir::Ty`.
fn type_rep(ty: ir::Ty) -> (PrimTy, u8) {
    match *ty.resolve() {
        ir::TyKind::Prim(pty) => (pty, 1),
        ir::TyKind::Vec(pty, len) => (pty, len),
        ir::TyKind::Texture(_) => (PrimTy::Uint(IntSize::I32), 2),
        ir::TyKind::Geometry(_) => (PrimTy::Uint(IntSize::I32), 0),
        ir::TyKind::Alias(..) => unreachable!(),
    }
}


pub fn compile_pass(ir: ir::Pass) -> Pass {
    eprintln!(" === compiling pass ===\n{}\n =======", ::print::print_pass(None, &ir));

    // Compute arg buffer layout, build name map, and identify geometry inputs.
    let mut l_args = Layout::new();
    let mut name_map = HashMap::new();
    let mut geom_fields = Vec::new();

    for &v in ir.inputs.iter().chain(ir.outputs.iter()) {
        let var = &ir.vars[v];
        let (pty, len) = type_rep(var.ty);
        l_args.add(v, pty, len);

        name_map.insert((**var.name.0).to_owned(), v);

        if let ir::TyKind::Geometry(fields) = *ir.vars[v].ty.resolve() {
            geom_fields.push((v, fields));
        }
    }

    // Compile uniform program, compute uniform buffer layout, and create temporary textures.
    let mut b = VmBuilder::new();
    let mut l_uniforms = Layout::new();
    let mut textures = HashMap::new();
    let var_class = var_class::classify_vars(ir);

    b.add_var_layout(0, &l_args);
    for s in ir.stmts {
        let l = match *s {
            ir::Stmt::Local(ref l) => l,
            _ => continue,
        };
        let v = l.var;

        if !var_class[v].le(VarClass::Uniform) {
            continue;
        }

        // Add newly declared var to layout
        let (pty, len) = type_rep(ir.vars[v].ty);
        let off = l_uniforms.add(v, pty, len);
        let addr = Addr::new(1, off);
        b.add_var(v, addr, pty, len);

        // Emit the current instruction
        b.emit_ir(addr, &l.insn);

        // Record temp texture, if needed
        if let ir::Insn::TextureCtor(fmt, _) = l.insn {
            textures.insert(v, TextureHandle(Rc::new(Texture::new(fmt, (4, 4)))));
        }
    }

    let uniform_buf_specs = vec![
        BufferSpec::new(l_args.size(), l_args.align() as u8, vm::Perm::Read),
        BufferSpec::new(l_uniforms.size(), l_uniforms.align() as u8, vm::Perm::ReadWrite),
    ];
    let uniform_prog = vm::Program::validate(b.finish(), uniform_buf_specs).unwrap();


    let b_args = AlignedBytes::with_len(l_args.size() as usize);
    let b_uniforms = AlignedBytes::with_len(l_uniforms.size() as usize);


    // Compile Clear + Render statements
    let (shaders, buffers, framebuffers);
    let mut buffer_updates = Vec::new();
    let mut ops = Vec::new();
    {
        let mut compiler = PassCompiler {
            uniform_layouts: UniformLayouts {
                l_args: &l_args,
                l_uniforms: &l_uniforms,
            },

            geom_info: HashMap::new(),

            shaders: Vec::new(),
            buffers: Vec::new(),
            framebuffers: Vec::new(),
        };

        for &v in ir.inputs.iter().chain(ir.outputs.iter()) {
            match *ir.vars[v].ty.resolve() {
                ir::TyKind::Geometry(fields) => {
                    buffer_updates.push(compiler.compile_geom_var(v, fields));
                },
                _ => {},
            }
        }

        for s in ir.stmts {
            if let Some(op) = compiler.compile_stmt(s) {
                ops.push(op);
            }
        }

        shaders = compiler.shaders;
        buffers = compiler.buffers;
        framebuffers = compiler.framebuffers;
    }


    Pass {
        uniform_prog,
        l_args, l_uniforms,
        b_args, b_uniforms,

        name_map,
        geometry: HashMap::new(),
        // Note we pre-initialize `textures` with temporary/intermediate textures only.  Input
        // and output textures still need to be set by the caller.
        textures,

        shaders, buffers, framebuffers,
        buffer_updates, ops,
    }
}
