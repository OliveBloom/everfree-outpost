use std::cell::Cell;
use std::ptr;
use std::usize;

use ast::TexFormat;

use asmgl as ffi;


#[derive(Debug)]
pub struct Framebuffer {
    name: u32,
}

impl Framebuffer {
    pub fn new() -> Framebuffer {
        let name = unsafe { ffi::asmgl_gen_framebuffer() };
        Framebuffer { name }
    }

    #[allow(dead_code)]
    pub fn name(&self) -> u32 {
        self.name
    }

    pub fn attach(&self, tex: &Texture, attachment: i8) {
        unsafe { ffi::asmgl_framebuffer_texture(tex.name(), attachment as i32) };
    }

    pub fn bind(&self) {
        unsafe { ffi::asmgl_bind_framebuffer(self.name); }
    }

    pub fn bind_default() {
        unsafe { ffi::asmgl_bind_framebuffer(0); }
    }
}

impl Drop for Framebuffer {
    fn drop(&mut self) {
        unsafe { ffi::asmgl_delete_framebuffer(self.name) };
    }
}


#[derive(Debug)]
pub struct Buffer {
    name: u32,
}

impl Buffer {
    pub fn new() -> Buffer {
        let name = unsafe { ffi::asmgl_gen_buffer() };
        Buffer { name }
    }

    #[allow(dead_code)]
    pub fn name(&self) -> u32 {
        self.name
    }

    pub fn set(&self, b: &[u8]) {
        unsafe {
            ffi::asmgl_bind_buffer(ffi::BufferTarget::Array as u8, self.name);
            ffi::asmgl_buffer_data_alloc(ffi::BufferTarget::Array as u8, b.len());
            ffi::asmgl_buffer_subdata(ffi::BufferTarget::Array as u8, 0, b.as_ptr(), b.len());
        }
    }

    pub fn bind(&self) {
        unsafe {
            ffi::asmgl_bind_buffer(ffi::BufferTarget::Array as u8, self.name);
        }
    }
}

impl Drop for Buffer {
    fn drop(&mut self) {
        unsafe { ffi::asmgl_delete_buffer(self.name) };
    }
}


#[derive(Debug)]
pub struct Shader {
    name: u32,
}

impl Shader {
    pub fn new(vert: &str, frag: &str, defs: &str) -> Shader {
        let name = unsafe {
            ffi::asmgl_load_shader(vert.as_ptr(), vert.len(),
                                   frag.as_ptr(), frag.len(),
                                   defs.as_ptr(), defs.len())
        };
        Shader { name }
    }

    #[allow(dead_code)]
    pub fn name(&self) -> u32 {
        self.name
    }

    pub fn uniform_loc(&self, name: &str) -> i32 {
        unsafe {
            ffi::asmgl_get_uniform_location(self.name, name.as_ptr(), name.len())
        }
    }

    pub fn attrib_loc(&self, name: &str) -> i32 {
        unsafe {
            ffi::asmgl_get_attrib_location(self.name, name.as_ptr(), name.len())
        }
    }

    pub fn set_uniform_1i(&self, loc: i32, val: i32) {
        unsafe {
            ffi::asmgl_bind_shader(self.name);
            ffi::asmgl_set_uniform_1i(loc, val);
        }
    }

    pub fn set_uniform_1f(&self, loc: i32, val: f32) {
        unsafe {
            ffi::asmgl_bind_shader(self.name);
            ffi::asmgl_set_uniform_1f(loc, val);
        }
    }

    pub fn set_uniform_2f(&self, loc: i32, val: &[f32; 2]) {
        unsafe {
            ffi::asmgl_bind_shader(self.name);
            ffi::asmgl_set_uniform_2f(loc, val);
        }
    }

    pub fn set_uniform_3f(&self, loc: i32, val: &[f32; 3]) {
        unsafe {
            ffi::asmgl_bind_shader(self.name);
            ffi::asmgl_set_uniform_3f(loc, val);
        }
    }

    pub fn set_uniform_4f(&self, loc: i32, val: &[f32; 4]) {
        unsafe {
            ffi::asmgl_bind_shader(self.name);
            ffi::asmgl_set_uniform_4f(loc, val);
        }
    }

    pub fn set_uniform_f(&self, loc: i32, val: &[f32]) {
        match val.len() {
            1 => self.set_uniform_1f(loc, val[0]),
            2 => self.set_uniform_2f(loc, unsafe { &*(val.as_ptr() as *const [f32; 2]) }),
            3 => self.set_uniform_3f(loc, unsafe { &*(val.as_ptr() as *const [f32; 3]) }),
            4 => self.set_uniform_4f(loc, unsafe { &*(val.as_ptr() as *const [f32; 4]) }),
            l => panic!("unsupported vector length ({}) for uniform", l),
        }
    }

    pub fn bind(&self) {
        unsafe {
            ffi::asmgl_bind_shader(self.name);
        }
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe { ffi::asmgl_delete_shader(self.name) };
    }
}


const OUTPUT_TEX_NAME: u32 = 0xffff_ffff;

#[derive(Debug)]
pub struct Texture {
    name: u32,
    size: Cell<(u16, u16)>,
    fmt: TexFormat,
}

impl Texture {
    pub fn new(fmt: TexFormat, size: (u16, u16)) -> Texture {
        let (w, h) = size;
        let ffi_fmt = ffi_tex_format(fmt);
        let name = unsafe { ffi::asmgl_gen_texture(w, h, ffi_fmt as u8) };

        Texture {
            name,
            size: Cell::new(size),
            fmt,
        }
    }

    pub fn from_image(path: &str) -> Texture {
        let mut size = ffi::Size16 { x: 0, y: 0 };
        let name = unsafe {
            ffi::asmgl_load_texture(path.as_ptr(), path.len(), &mut size)
        };

        Texture {
            name,
            size: Cell::new((size.x, size.y)),
            fmt: TexFormat::RGBA,
        }
    }

    pub fn output(size: (u16, u16)) -> Texture {
        Texture {
            name: OUTPUT_TEX_NAME,
            size: Cell::new(size),
            fmt: TexFormat::RGBA,
        }
    }

    pub fn bind(&self, unit: usize) {
        unsafe {
            ffi::asmgl_active_texture(unit);
            ffi::asmgl_bind_texture(self.name);
        }
    }

    pub fn name(&self) -> u32 {
        self.name
    }


    pub fn size(&self) -> (u16, u16) {
        self.size.get()
    }

    pub fn format(&self) -> TexFormat {
        self.fmt
    }

    pub fn is_output(&self) -> bool {
        self.name == OUTPUT_TEX_NAME
    }


    pub fn resize(&self, size: (u16, u16)) {
        self.size.set(size);
        if self.name != OUTPUT_TEX_NAME {
            let ffi_fmt = ffi_tex_format(self.fmt);
            self.bind(0);
            unsafe {
                ffi::asmgl_texture_image(size.0, size.1, ffi_fmt as u8, ptr::null(), 0);
            }
        }
    }

    pub fn load_image(&mut self, path: &str) {
        *self = Self::from_image(path);
    }

    pub fn load_data(&self, b: &[u8]) {
        unsafe {
            ffi::asmgl_bind_texture(self.name);
            ffi::asmgl_texture_image(
                self.size.get().0,
                self.size.get().1,
                ffi_tex_format(self.fmt) as u8,
                b.as_ptr(),
                b.len());
        }
    }

    pub fn load_subdata(&self, offset: (u16, u16), size: (u16, u16), b: &[u8]) {
        unsafe {
            ffi::asmgl_bind_texture(self.name);
            ffi::asmgl_texture_subimage(
                offset.0,
                offset.1,
                size.0,
                size.1,
                ffi_tex_format(self.fmt) as u8,
                b.as_ptr(),
                b.len());
        }
    }
}

impl Drop for Texture {
    fn drop(&mut self) {
        if self.name != OUTPUT_TEX_NAME {
            unsafe {
                ffi::asmgl_delete_texture(self.name);
            }
        }
    }
}

fn ffi_tex_format(fmt: TexFormat) -> ffi::TextureKind {
    match fmt {
        TexFormat::Red => ffi::TextureKind::Luminance,
        TexFormat::RGB => unimplemented!("RGB"),
        TexFormat::RGBA => ffi::TextureKind::RGBA,
        TexFormat::Depth => ffi::TextureKind::Depth,
    }
}


