use intern::Interners;
use ir::*;
use util::IndexVec;


pub struct PassBuilder<'a> {
    inputs: Vec<VarId>,
    outputs: Vec<VarId>,
    stmts: Vec<Stmt<'a>>,
    vars: IndexVec<VarId, Var<'a>>,
}

#[allow(dead_code)]
impl<'a> PassBuilder<'a> {
    pub fn new() -> PassBuilder<'a> {
        PassBuilder {
            inputs: Vec::new(),
            outputs: Vec::new(),
            stmts: Vec::new(),
            vars: IndexVec::new(),
        }
    }

    fn add_var(&mut self, name: Symbol<'a>, ty: Ty<'a>, origin: VarOrigin) -> VarId {
        self.vars.push(Var { name, ty, origin })
    }

    pub fn input(&mut self,
                 name: Symbol<'a>,
                 ty: Ty<'a>) -> VarId {
        let idx = self.inputs.len();
        let id = self.add_var(name, ty, VarOrigin::Input(idx));
        self.inputs.push(id);
        id
    }

    pub fn output(&mut self,
                  name: Symbol<'a>,
                  ty: Ty<'a>) -> VarId {
        let idx = self.outputs.len();
        let id = self.add_var(name, ty, VarOrigin::Output(idx));
        self.outputs.push(id);
        id
    }

    pub fn stmt(&mut self, stmt: Stmt<'a>) {
        if let Stmt::Local(_) = stmt {
            panic!("`stmt` does not support Stmt::Local - use `local` instead");
        }
        self.stmts.push(stmt);
    }

    pub fn local(&mut self,
                 name: Symbol<'a>,
                 ty: Ty<'a>,
                 insn: Insn<'a>) -> VarId {
        let idx = self.stmts.len();
        let id = self.add_var(name, ty, VarOrigin::Stmt(idx));
        self.stmts.push(Stmt::Local(LocalStmt { var: id, insn }));
        id
    }

    pub fn clear(&mut self, cs: ClearStmt<'a>) {
        self.stmts.push(Stmt::Clear(cs));
    }

    pub fn render(&mut self, rs: RenderStmt<'a>) {
        self.stmts.push(Stmt::Render(rs));
    }

    pub fn call(&mut self, cs: CallStmt<'a>) {
        self.stmts.push(Stmt::Call(cs));
    }

    pub fn finish(&self,
                  intern: &Interners<'a>,
                  name: Symbol<'a>,
                  public: bool) -> Pass<'a> {
        Pass {
            name, public,
            inputs: intern.slice(&self.inputs),
            outputs: intern.slice(&self.outputs),
            stmts: intern.slice(&self.stmts),
            vars: intern.index_slice(&self.vars),
        }
    }
}

