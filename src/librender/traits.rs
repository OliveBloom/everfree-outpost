use std::collections::HashMap;
use std::mem;
use ast::{Pipeline, TexFormat};

pub trait Backend {
    type Pass: Pass<Texture=Self::Texture, Geometry=Self::Geometry>;
    type Texture: Texture;
    type Geometry: Geometry;

    fn compile(&mut self, pipeline: &Pipeline) -> HashMap<String, Self::Pass>;

    fn new_texture(&self, fmt: TexFormat, size: (u16, u16)) -> Self::Texture;
    fn texture_from_image(&self, path: &str) -> Self::Texture;
    fn output_texture(&self) -> Self::Texture;

    fn new_geometry<T: Copy>(&self) -> Self::Geometry {
        self.geometry_from_layout(mem::size_of::<T>(), mem::align_of::<T>())
    }

    fn geometry_from_layout(&self, size: usize, align: usize) -> Self::Geometry;
}

#[allow(unused_variables)]
pub trait Pass {
    type Geometry: Geometry;
    type Texture: Texture;

    fn set_i(&mut self, name: &str, val: i32);
    fn set_u(&mut self, name: &str, val: u32);
    fn set_f(&mut self, name: &str, val: f32);

    fn set_iv(&mut self, name: &str, val: &[i32]);
    fn set_uv(&mut self, name: &str, val: &[u32]);
    fn set_fv(&mut self, name: &str, val: &[f32]);

    fn set_texture(&mut self, name: &str, tex: &Self::Texture);

    fn set_geometry(&mut self, name: &str, geom: &Self::Geometry);

    /// Configure `geom` for future use as input `name` of this pass.  Using a `Geometry` buffer
    /// that is optimized for the input makes rendering more efficient.
    fn set_geometry_target(&mut self, name: &str, geom: &mut Self::Geometry) {}
    fn unset_geometry_target(&mut self, name: &str, geom: &mut Self::Geometry) {}

    fn run(&mut self);
}

pub trait Geometry {
    /// Append an element to the buffer.  Panics if the size of element type `T` does not match the
    /// expected element size for this buffer.
    fn push<T: Copy>(&mut self, x: &T) {
        let len = self.len();
        self.set_len(len + 1);
        self.set(len, x);
    }

    fn pop(&mut self) {
        let len = self.len();
        assert!(len > 0);
        self.set_len(len - 1);
    }

    fn set<T: Copy>(&mut self, idx: usize, x: &T);

    fn extend<'a, I, T: Copy+'a>(&mut self, xs: I)
            where I: IntoIterator<Item=&'a T> {
        for x in xs {
            self.push(x);
        }
    }

    fn copy<'a, I, T: Copy+'a>(&mut self, start: usize, xs: I)
            where I: IntoIterator<Item=&'a T> {
        for (i, x) in xs.into_iter().enumerate() {
            self.set(start + i, x);
        }
    }

    /// Get the length of the buffer.
    fn len(&self) -> usize;
    /// Set the length of the buffer, 
    fn set_len(&mut self, len: usize);
}

pub trait Texture {
    fn size(&self) -> (u16, u16);
    fn format(&self) -> TexFormat;
    fn is_output(&self) -> bool;

    /// Change the size of the texture, invalidating all image data it contains.
    fn resize(&mut self, size: (u16, u16));
    fn load_image(&mut self, path: &str);
    fn load_data(&mut self, b: &[u8]);
    fn load_subdata(&mut self, offset: (u16, u16), size: (u16, u16), b: &[u8]);
}
