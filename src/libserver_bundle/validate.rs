use types::*;
use server_types::*;
use server_world_types::*;
use std::collections::{HashMap, HashSet};
use server_extra::Extra;


pub trait Context {
    // Check whether the named data entry exists.
    fn has_anim(&self, name: &str) -> bool;
    fn has_item(&self, name: &str) -> bool;
    fn has_block(&self, name: &str) -> bool;
    fn has_template(&self, name: &str) -> bool;

    // Check whether stable IDs are currently in use.
    fn has_stable_client(&self, id: Stable<ClientId>) -> bool;
    fn has_stable_entity(&self, id: Stable<EntityId>) -> bool;
    fn has_stable_inventory(&self, id: Stable<InventoryId>) -> bool;
    fn has_stable_plane(&self, id: Stable<PlaneId>) -> bool;
    fn has_stable_terrain_chunk(&self, id: Stable<TerrainChunkId>) -> bool;
    fn has_stable_structure(&self, id: Stable<StructureId>) -> bool;

    // Check whether a terrain chunk exists at the indicated position.
    fn has_terrain_chunk_at_pos(&self, plane: Stable<PlaneId>, cpos: V2) -> bool;
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum DataKind {
    Anim,
    Item,
    Block,
    Template,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum ObjectKind {
    World,
    Client,
    Entity,
    Inventory,
    Plane,
    TerrainChunk,
    Structure,
}

pub trait ErrorReporter {
    // Data errors

    /// The indicated data reference names a data entry that does not exist in the context.
    fn bad_data_name(&mut self,
                     data: (DataKind, usize),
                     name: &str);

    /// The object refers to a data reference that does not exist in the bundle.
    fn no_such_data(&mut self,
                    object: (ObjectKind, usize),
                    data: (DataKind, usize));


    // Generic object errors

    /// There are enough objects in the bundle to overflow the indicated object ID type.
    fn too_many_objects(&mut self,
                        object_kind: ObjectKind);

    /// The object's `stable_id` field contains an ID that is already in use.
    fn stable_id_in_use(&mut self,
                        object: (ObjectKind, usize),
                        stable_id: StableId);

    /// The child is attached to the parent, but does not appear in the parent's `child_foo` set.
    fn missing_child_ref(&mut self,
                         parent: (ObjectKind, usize),
                         child: (ObjectKind, usize));

    /// The child is listed in the parent's `child_foo` set, but the child's `attachment` field
    /// does not refer to the parent.
    fn child_not_attached(&mut self,
                          parent: (ObjectKind, usize),
                          child: (ObjectKind, usize));

    /// One of the parent's `child_foo` sets contains duplicate entries.
    fn duplicate_child_objects(&mut self,
                               parent: (ObjectKind, usize),
                               child_kind: ObjectKind);

    /// The child is attached to an object that does not exist in the bundle.
    ///
    /// Note that not all attachment types include an ID for the parent.
    fn no_such_parent(&mut self,
                      child: (ObjectKind, usize),
                      parent_kind: ObjectKind,
                      parent_idx: Option<usize>);

    /// One of the parent's `child_foo` sets refers to an object that does not exist in the bundle.
    fn no_such_child(&mut self,
                     parent: (ObjectKind, usize),
                     child: (ObjectKind, usize));

    /// The user refers to an object that does not exist in the bundle.
    fn no_such_object(&mut self,
                     user: (ObjectKind, usize),
                     object: (ObjectKind, usize));


    // Specific object errors

    /// The object's `stable_plane` field refers to a nonexistent plane.
    fn no_such_stable_plane(&mut self,
                            object: (ObjectKind, usize),
                            stable_plane: Stable<PlaneId>);

    /// No chunk is loaded at the object's position.
    fn no_chunk_at_position(&mut self,
                            object: (ObjectKind, usize),
                            plane: Stable<PlaneId>,
                            cpos: V2);

    /// The terrain chunk's position (`stable_plane` and `cpos`) is already occupied.
    fn chunk_position_in_use(&mut self,
                             terrain_chunk_idx: usize,
                             plane: Stable<PlaneId>,
                             cpos: V2);

    /// The object does not have a stable ID.
    fn missing_stable_id(&mut self,
                         object: (ObjectKind, usize));

    /// The client's pawn is not a child of the client.
    fn pawn_isnt_child(&mut self,
                       client_idx: usize,
                       pawn_idx: usize);
}


bitflags! {
    flags ParentChildLink: u8 {
        const PARENT_TO_CHILD = 0x01,
        const CHILD_TO_PARENT = 0x02,
    }
}


pub struct Validator<C: Context, R: ErrorReporter> {
    ctx: C,
    report: R,

    bundle_stable_ids: HashSet<(ObjectKind, StableId)>,
    parent_child_links: HashMap<(ObjectKind, usize, ObjectKind, usize), ParentChildLink>,
    bundle_chunks: HashSet<(Stable<PlaneId>, V2)>,
}

struct BundleValidator<'a, C: Context+'a, R: ErrorReporter+'a> {
    ctx: &'a mut C,
    report: &'a mut R,
    b: &'a Bundle,

    bundle_stable_ids: &'a mut HashSet<(ObjectKind, StableId)>,
    parent_child_links: &'a mut HashMap<(ObjectKind, usize, ObjectKind, usize), ParentChildLink>,
    bundle_chunks: &'a mut HashSet<(Stable<PlaneId>, V2)>,
}

impl<C: Context, R: ErrorReporter> Validator<C, R> {
    pub fn new(ctx: C, report: R) -> Validator<C, R> {
        Validator {
            ctx: ctx,
            report: report,

            bundle_stable_ids: HashSet::new(),
            parent_child_links: HashMap::new(),
            bundle_chunks: HashSet::new(),
        }
    }

    pub fn validate(&mut self, bundle: &Bundle) {
        let mut bv = BundleValidator {
            ctx: &mut self.ctx,
            report: &mut self.report,
            b: bundle,

            bundle_stable_ids: &mut self.bundle_stable_ids,
            parent_child_links: &mut self.parent_child_links,
            bundle_chunks: &mut self.bundle_chunks,
        };

        bv.validate();
    }
}

impl<'a, C: Context, R: ErrorReporter> BundleValidator<'a, C, R> {
    fn validate(&mut self) {
        // Check that we have a reasonable number of objects.
        self.check_object_counts();

        // Collect (and validate) the stable IDs of all objects in the bundle.  This will be used
        // later for checking stable ID references.
        self.record_bundle_stable_ids();

        self.check_data();
        self.check_objects();

        self.check_parent_child_links();
    }


    fn check_object_counts(&mut self) {
        let count = self.b.clients.len();
        if count > 0 && ClientId(count as _).to_usize() != count {
            self.report.too_many_objects(ObjectKind::Client);
        }

        let count = self.b.entities.len();
        if count > 0 && EntityId(count as _).to_usize() != count {
            self.report.too_many_objects(ObjectKind::Entity);
        }

        let count = self.b.inventories.len();
        if count > 0 && InventoryId(count as _).to_usize() != count {
            self.report.too_many_objects(ObjectKind::Inventory);
        }

        let count = self.b.planes.len();
        if count > 0 && PlaneId(count as _).to_usize() != count {
            self.report.too_many_objects(ObjectKind::Plane);
        }

        let count = self.b.terrain_chunks.len();
        if count > 0 && TerrainChunkId(count as _).to_usize() != count {
            self.report.too_many_objects(ObjectKind::TerrainChunk);
        }

        let count = self.b.structures.len();
        if count > 0 && StructureId(count as _).to_usize() != count {
            self.report.too_many_objects(ObjectKind::Structure);
        }
    }


    fn record_bundle_stable_ids(&mut self) {
        for (idx, obj) in self.b.clients.iter().enumerate() {
            self.record_stable_id(ObjectKind::Client, idx, obj.stable_id);
        }

        for (idx, obj) in self.b.entities.iter().enumerate() {
            self.record_stable_id(ObjectKind::Entity, idx, obj.stable_id);
        }

        for (idx, obj) in self.b.inventories.iter().enumerate() {
            self.record_stable_id(ObjectKind::Inventory, idx, obj.stable_id);
        }

        for (idx, obj) in self.b.planes.iter().enumerate() {
            self.record_stable_id(ObjectKind::Plane, idx, obj.stable_id);
        }

        for (idx, obj) in self.b.terrain_chunks.iter().enumerate() {
            self.record_stable_id(ObjectKind::TerrainChunk, idx, obj.stable_id);
        }

        for (idx, obj) in self.b.structures.iter().enumerate() {
            self.record_stable_id(ObjectKind::Structure, idx, obj.stable_id);
        }
    }

    fn record_stable_id(&mut self, kind: ObjectKind, idx: usize, stable_id: StableId) {
        if stable_id == NO_STABLE_ID {
            return;
        }

        let in_ctx = match kind {
            ObjectKind::World => panic!("record_stable_id shouldn't be called on World"),
            ObjectKind::Client => self.ctx.has_stable_client(Stable::new(stable_id)),
            ObjectKind::Entity => self.ctx.has_stable_entity(Stable::new(stable_id)),
            ObjectKind::Inventory => self.ctx.has_stable_inventory(Stable::new(stable_id)),
            ObjectKind::Plane => self.ctx.has_stable_plane(Stable::new(stable_id)),
            ObjectKind::TerrainChunk => self.ctx.has_stable_terrain_chunk(Stable::new(stable_id)),
            ObjectKind::Structure => self.ctx.has_stable_structure(Stable::new(stable_id)),
        };

        if in_ctx || self.bundle_stable_ids.contains(&(kind, stable_id)) {
           self.report.stable_id_in_use((kind, idx), stable_id);
        }

        self.bundle_stable_ids.insert((kind, stable_id));
    }


    fn check_data(&mut self) {
        for (idx, name) in self.b.anims.iter().enumerate() {
            if !self.ctx.has_anim(name) {
                self.report.bad_data_name((DataKind::Anim, idx), name);
            }
        }

        for (idx, name) in self.b.items.iter().enumerate() {
            if !self.ctx.has_item(name) {
                self.report.bad_data_name((DataKind::Item, idx), name);
            }
        }

        for (idx, name) in self.b.blocks.iter().enumerate() {
            if !self.ctx.has_block(name) {
                self.report.bad_data_name((DataKind::Block, idx), name);
            }
        }

        for (idx, name) in self.b.templates.iter().enumerate() {
            if !self.ctx.has_template(name) {
                self.report.bad_data_name((DataKind::Template, idx), name);
            }
        }
    }


    fn check_objects(&mut self) {
        if let Some(ref obj) = self.b.world {
            self.check_world(obj);
        }

        for (idx, obj) in self.b.clients.iter().enumerate() {
            self.check_client(idx, obj);
        }

        for (idx, obj) in self.b.entities.iter().enumerate() {
            self.check_entity(idx, obj);
        }

        for (idx, obj) in self.b.inventories.iter().enumerate() {
            self.check_inventory(idx, obj);
        }

        for (idx, obj) in self.b.planes.iter().enumerate() {
            self.check_plane(idx, obj);
        }

        for (idx, obj) in self.b.terrain_chunks.iter().enumerate() {
            self.check_terrain_chunk(idx, obj);
        }

        for (idx, obj) in self.b.structures.iter().enumerate() {
            self.check_structure(idx, obj);
        }
    }

    fn check_world(&mut self, obj: &World) {
        let this_obj = (ObjectKind::World, 0);

        self.check_extra(&obj.extra, this_obj);
        self.record_children(&obj.child_entities, this_obj, ObjectKind::Entity);
        self.record_children(&obj.child_inventories, this_obj, ObjectKind::Inventory);
    }

    fn check_client(&mut self, idx: usize, obj: &Client) {
        let this_obj = (ObjectKind::Client, idx);

        // `pawn` is checked after processing children

        // Generic object checks
        self.check_extra(&obj.extra, this_obj);
        self.record_children(&obj.child_entities, this_obj, ObjectKind::Entity);
        self.record_children(&obj.child_inventories, this_obj, ObjectKind::Inventory);

        // `pawn` check
        if let Some(pawn_id) = obj.pawn {
            let k = (ObjectKind::Client, idx, ObjectKind::Entity, pawn_id.to_usize());
            if !self.parent_child_links.contains_key(&k) {
                self.report.pawn_isnt_child(idx, pawn_id.to_usize());
            }
        }
    }

    fn check_entity(&mut self, idx: usize, obj: &Entity) {
        let this_obj = (ObjectKind::Entity, idx);

        if !self.ctx.has_stable_plane(obj.stable_plane) {
            self.report.no_such_stable_plane((ObjectKind::Entity, idx), obj.stable_plane);
        }

        self.check_data_ref(this_obj, DataKind::Anim, obj.anim as usize);

        // Generic object checks
        self.check_extra(&obj.extra, this_obj);
        self.record_children(&obj.child_inventories, this_obj, ObjectKind::Inventory);

        match obj.attachment {
            EntityAttachment::World => panic!("EntityAttachment::World: unsupported"),
            EntityAttachment::Chunk => panic!("EntityAttachment::Chunk: unsupported"),
            EntityAttachment::Client(cid) => {
                self.record_parent(this_obj, ObjectKind::Client, cid.to_usize());
            },
        }
    }

    fn check_inventory(&mut self, idx: usize, obj: &Inventory) {
        let this_obj = (ObjectKind::Inventory, idx);

        for i in obj.contents.iter() {
            self.check_item(i, this_obj);
        }

        // Generic object checks
        self.check_extra(&obj.extra, this_obj);

        match obj.attachment {
            InventoryAttachment::World => panic!("InventoryAttachment::World: unsupported"),
            InventoryAttachment::Client(cid) => {
                self.record_parent(this_obj, ObjectKind::Client, cid.to_usize());
            },
            InventoryAttachment::Entity(eid) => {
                self.record_parent(this_obj, ObjectKind::Entity, eid.to_usize());
            },
            InventoryAttachment::Structure(sid) => {
                self.record_parent(this_obj, ObjectKind::Structure, sid.to_usize());
            },
        }
    }

    fn check_plane(&mut self, idx: usize, obj: &Plane) {
        let this_obj = (ObjectKind::Plane, idx);

        if obj.stable_id == NO_STABLE_ID {
            self.report.missing_stable_id(this_obj);
        }

        // Generic object checks
        self.check_extra(&obj.extra, this_obj);
    }

    fn check_terrain_chunk(&mut self, idx: usize, obj: &TerrainChunk) {
        let this_obj = (ObjectKind::TerrainChunk, idx);

        if !self.ctx.has_stable_plane(obj.stable_plane) {
            self.report.no_such_stable_plane((ObjectKind::Entity, idx), obj.stable_plane);
        }

        if obj.stable_id == NO_STABLE_ID {
            self.report.missing_stable_id(this_obj);
        }

        let k = (obj.stable_plane, obj.cpos);
        if self.bundle_chunks.contains(&k) || self.ctx.has_terrain_chunk_at_pos(k.0, k.1) {
            self.report.chunk_position_in_use(idx, k.0, k.1);
        }
        self.bundle_chunks.insert(k);

        for &b in obj.blocks.iter() {
            self.check_data_ref(this_obj, DataKind::Block, b as usize);
        }

        // Generic object checks
        self.check_extra(&obj.extra, this_obj);
        self.record_children(&obj.child_structures, this_obj, ObjectKind::Structure);
    }

    fn check_structure(&mut self, idx: usize, obj: &Structure) {
        let this_obj = (ObjectKind::Structure, idx);

        if !self.ctx.has_stable_plane(obj.stable_plane) {
            self.report.no_such_stable_plane((ObjectKind::Entity, idx), obj.stable_plane);
        }

        self.check_data_ref(this_obj, DataKind::Template, obj.template as usize);


        // Generic object checks
        self.check_extra(&obj.extra, this_obj);
        self.record_children(&obj.child_inventories, this_obj, ObjectKind::Inventory);

        match obj.attachment {
            StructureAttachment::Plane => panic!("StructureAttachment::Plane: unsupported"),
            StructureAttachment::Chunk => {
                let plane = obj.stable_plane;
                let cpos = obj.pos.reduce().div_floor(CHUNK_SIZE);
                if !self.bundle_chunks.contains(&(plane, cpos)) &&
                   !self.ctx.has_terrain_chunk_at_pos(plane, cpos) {
                    self.report.no_chunk_at_position(this_obj, plane, cpos);
                }
            },
        }
    }


    fn check_extra(&mut self, _extra: &Extra, _obj: (ObjectKind, usize)) {
        // TODO
    }

    fn num_data_of_kind(&self, kind: DataKind) -> usize {
        match kind {
            DataKind::Anim => self.b.anims.len(),
            DataKind::Item => self.b.items.len(),
            DataKind::Block => self.b.blocks.len(),
            DataKind::Template => self.b.templates.len(),
        }
    }

    fn num_objects_of_kind(&self, kind: ObjectKind) -> usize {
        match kind {
            ObjectKind::World => if self.b.world.is_some() { 1 } else { 0 },
            ObjectKind::Client => self.b.clients.len(),
            ObjectKind::Entity => self.b.entities.len(),
            ObjectKind::Inventory => self.b.inventories.len(),
            ObjectKind::Plane => self.b.planes.len(),
            ObjectKind::TerrainChunk => self.b.terrain_chunks.len(),
            ObjectKind::Structure => self.b.structures.len(),
        }
    }

    fn check_data_ref(&mut self, object: (ObjectKind, usize), kind: DataKind, idx: usize) {
        if idx >= self.num_data_of_kind(kind) {
            self.report.no_such_data(object, (kind, idx));
        }
    }

    fn record_parent(&mut self,
                     child: (ObjectKind, usize),
                     parent_kind: ObjectKind,
                     parent_idx: usize) {
        if parent_idx >= self.num_objects_of_kind(parent_kind) {
            self.report.no_such_parent(child, parent_kind, Some(parent_idx));
            return;
        }


        let key = (parent_kind, parent_idx, child.0, child.1);
        self.parent_child_links.entry(key).or_insert(ParentChildLink::empty())
            .insert(CHILD_TO_PARENT);
    }

    fn record_children<Id>(&mut self,
                           children: &[Id], 
                           parent: (ObjectKind, usize),
                           child_kind: ObjectKind)
            where Id: ObjectId {
        let mut has_duplicate = false;

        for &child_id in children {
            let child_idx = child_id.to_usize();
            if child_idx >= self.num_objects_of_kind(child_kind) {
                self.report.no_such_child(parent, (child_kind, child_idx));
                continue;
            }

            let key = (parent.0, parent.1, child_kind, child_idx);
            let link = self.parent_child_links.entry(key).or_insert(ParentChildLink::empty());
            if link.contains(PARENT_TO_CHILD) {
                has_duplicate = true;
            }
            link.insert(PARENT_TO_CHILD);
        }

        if has_duplicate {
            self.report.duplicate_child_objects(parent, child_kind);
        }
    }

    fn check_item(&mut self, item: &Item, object: (ObjectKind, usize)) {
        self.check_data_ref(object, DataKind::Item, item.id as usize);
        // TODO: check stack size
    }


    fn check_parent_child_links(&mut self) {
        for (k, link) in &*self.parent_child_links {
            let parent = (k.0, k.1);
            let child = (k.2, k.3);

            if !link.contains(PARENT_TO_CHILD) {
                self.report.missing_child_ref(parent, child);
            }

            if !link.contains(CHILD_TO_PARENT) {
                self.report.child_not_attached(parent, child);
            }
        }
    }
}
