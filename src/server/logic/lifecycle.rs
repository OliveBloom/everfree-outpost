use std::fs::File;

use types::*;
use util::now;
use libcommon_proto::wire::{ReadFrom, WriteTo};

use engine::Engine;
use logic;
use messages::{ClientResponse, SyncKind};
use world::bundle;
use world::object::*;


pub fn start_up(eng: &mut Engine) {
    let world_time =
        if let Some(mut file) = eng.storage.open_world_file() {
            let b = bundle::read_bundle(&mut file).unwrap();
            bundle::import_world(&mut eng.world, &b);
            b.world.as_ref().unwrap().now
        } else {
            0
        };
    trace!("initial world time: {}", world_time);

    let unix_time = now();
    eng.messages.set_world_time(unix_time, world_time);
    eng.timer.set_world_time(unix_time, world_time);
    eng.now = world_time;

    if let Some(mut file) = eng.storage.open_plane_file(STABLE_PLANE_LIMBO) {
        let b = bundle::read_bundle(&mut file).unwrap();
        bundle::import_bundle(&mut eng.world, &b);
    } else {
        let name = "Limbo".to_owned();
        let stable_pid = eng.world.create_plane(name).unwrap().stable_id();
        assert!(stable_pid == STABLE_PLANE_LIMBO);
    }

    if let Some(mut file) = eng.storage.open_plane_file(STABLE_PLANE_FOREST) {
        let b = bundle::read_bundle(&mut file).unwrap();
        bundle::import_bundle(&mut eng.world, &b);
    } else {
        let name = "Everfree Forest".to_owned();
        let stable_pid = eng.world.create_plane(name).unwrap().stable_id();
        assert!(stable_pid == STABLE_PLANE_FOREST);
    }

    warn_on_err!(eng.script_hooks.call_server_startup(eng));
}


pub fn shut_down(eng: &mut Engine) {
    while let Some(cid) = eng.world.clients().next().map(|c| c.id()) {
        warn_on_err!(logic::client::logout(eng, cid));
    }

    while let Some((pid, cpos)) = eng.world.terrain_chunks().next()
                              .map(|tc| (tc.plane_id(), tc.chunk_pos())) {
        logic::chunks::unload_chunk(eng, pid, cpos);
    }

    while let Some(pid) = eng.world.planes().next().map(|p| p.id()) {
        logic::chunks::unload_plane(eng, pid);
    }

    warn_on_err!(eng.script_hooks.call_server_shutdown(eng));

    {
        let mut exporter = bundle::Exporter::new(eng.data);
        exporter.add_world(&eng.world);
        let mut b = exporter.finish();
        b.world.as_mut().unwrap().now = eng.now();
        let b = b;

        let mut file = eng.storage.create_world_file();
        warn_on_err!(bundle::write_bundle(&mut file, &b));
    }
}


pub fn pre_restart(eng: &mut Engine) {
    // TODO: call into eng.chat instead
    let msg = ClientResponse::ChatUpdate("&s\t***\tServer restarting...".to_owned());
    eng.messages.broadcast_clients(msg);
    eng.messages.broadcast_clients(ClientResponse::SyncStatus(SyncKind::Reset));

    {
        info!("recording clients to file...");
        let mut file = eng.storage.create_restart_file();
        for c in eng.world.clients() {
            let wire_id = match eng.messages.client_to_wire(c.id()) {
                Some(x) => x,
                None => {
                    warn!("no wire for client {:?}", c.id());
                    continue;
                },
            };
            let uid = match eng.extra.client_uid.get(&c.id()) {
                Some(&x) => x,
                None => {
                    warn!("no user ID for client {:?}", c.id());
                    continue;
                },
            };

            (wire_id.unwrap(), uid).write_to(&mut file).unwrap();
            // TODO: putting c.name() into the tuple makes trait resolution fail - unclear why
            c.name().write_to(&mut file).unwrap();
        }
    }
}

pub fn post_restart(eng: &mut Engine, mut file: File) {
    info!("retrieving clients from file...");

    while let Ok((raw_id, uid, name)) = ReadFrom::read_from(&mut file) {
        warn_on_err!(logic::client::login(eng, WireId(raw_id), uid, name));
    }

    // TODO: call into eng.chat instead
    let msg = ClientResponse::ChatUpdate("&s\t***\tServer restarted".to_owned());
    eng.messages.broadcast_clients(msg);
}
