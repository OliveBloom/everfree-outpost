use types::*;
use std::cmp;
use util::StrResult;
use libcommon_movement;
use libcommon_proto::types::LocalPos;
use libphysics::{TILE_BITS, CHUNK_BITS, LOCAL_BITS};
use libphysics::CHUNK_SIZE;
use libphysics::ShapeSource;
use libserver_config::Data;

use cache::TerrainCache;
use components::movement::{Update, PendingMotion};
use engine::Engine;
use input::InputBits;
use logic;
use messages::ClientResponse;
use timing::TICK_MS;
use world::{Activity, Motion};
use world::object::*;


pub fn queue_motion(eng: &mut Engine,
                    cid: ClientId,
                    delay: u16,
                    input: InputBits,
                    pos: LocalPos,
                    velocity: V3,
                    validity: u8) -> StrResult<()> {
    let c = unwrap!(eng.world.get_client(cid));
    let e = unwrap!(c.pawn());
    trace!("queue_motion {:?} {} {:?} {:?} {:?} {}", e.id, delay, input, pos, velocity, validity);
    let pos = pos.to_global_bits(e.pos(eng.now),
                                 TILE_BITS + CHUNK_BITS + LOCAL_BITS);
    eng.movement.queue_motion(eng.now, e.id, delay, input, pos, velocity, validity);
    Ok(())
}

pub fn queue_motion_rel(eng: &mut Engine,
                        cid: ClientId,
                        rel_time: u16,
                        input: InputBits,
                        pos: LocalPos,
                        velocity: V3) -> StrResult<()> {
    let c = unwrap!(eng.world.get_client(cid));
    let e = unwrap!(c.pawn());
    trace!("queue_motion_rel {:?} {} {:?} {:?} {:?}", e.id, rel_time, input, pos, velocity);
    let pos = pos.to_global_bits(e.pos(eng.now),
                                 TILE_BITS + CHUNK_BITS + LOCAL_BITS);
    eng.movement.queue_motion_rel(eng.now, e.id, rel_time, input, pos, velocity);
    Ok(())
}


engine_part2!(MovementParts(world, movement, cache));
engine_part2!(EngineVision(vision, messages));

pub fn update(eng: &mut Engine) {
    let now = eng.now;

    let mut movement = eng.movement.take();

    for (&eid, em) in movement.iter() {
        // Check that eid is valid.
        let _ = unwrap_or!(eng.world.get_entity(eid),
                           { error!("{:?} has EntityMovement but no Entity", eid);
                             continue; });

        let mut time = now;
        let mut ok = true;
        while ok && time < now + TICK_MS {
            match em.update(time) {
                Update::Start(m) => {
                    ok = start_motion(eng, time, eid, m);
                },

                Update::Continue(dur, m) => {
                    let next_time = cmp::min(time + dur as Time, now + TICK_MS);
                    match continue_motion(eng, time, next_time, eid, m) {
                        Ok(()) => {
                            time = next_time;
                        },
                        Err(actual_dur) => {
                            time += actual_dur;
                            ok = false;
                        },
                    }
                },

                Update::Idle(dur) => {
                    time = cmp::min(time + dur as Time, now + TICK_MS);
                },
            }
        }

        if !ok {
            let next_validity = em.invalidate();
            trace!("invalidate movement for {:?} - next_validity = {}", eid, next_validity);
            cancel_motion(eng, time, eid);
            if let Some(c) = eng.world.entity(eid).pawn_owner() {
                eng.messages.send_client(c.id, ClientResponse::MotionMispredict(
                        eng.now, next_validity));
            }
        }
    }

    eng.movement.replace(movement);
}

fn start_motion(eng: &mut Engine, now: Time, eid: EntityId, m: &PendingMotion) -> bool {
    let facing;
    let speed;
    let target_velocity;
    {
        let e = eng.world.entity(eid);
        let pos = e.pos(now);

        // Position check
        if m.pos != pos {
            trace!("start_motion({:?}): starting pos doesn't match: {:?} != {:?}",
                  eid, m.pos, pos);
            return false;
        }

        // Physics check
        let s = ChunksSource {
            cache: &eng.cache,
            base_tile: scalar(0),
            plane: e.plane_id(),
        };

        facing = m.input.to_direction().unwrap_or(e.facing());
        speed = m.input.to_speed();
        target_velocity = facing * scalar(speed as i32 * 50);

        let velocity = libcommon_movement::calc_velocity(&s, pos, target_velocity);

        if m.velocity != velocity {
            trace!("start_motion({:?}): velocity doesn't match: {:?} != {:?}",
                  eid, m.velocity, velocity);
            return false;
        }
    }

    // Set activity and motion

    // interrupt() fails only when eid is invalid, and eid is checked before start_motion()
    if !logic::activity::interrupt(eng, eid, Activity::Walk).unwrap() {
        trace!("start_motion({:?}): failed to interrupt current activity", eid);
        return false;
    }

    let msg;
    {
        let mut e = eng.world.entity_mut(eid);
        trace!("set motion to {:?} for {:?}", m, eid);
        e.set_motion(convert_motion(m));
        e.set_facing(facing);
        e.set_target_velocity(target_velocity);
        e.set_anim(facing_anim(eng.data, facing, speed));

        msg = logic::vision::entity_motion_message(e.borrow());
    }


    // Send messages
    let messages = &mut eng.messages;
    eng.vision.entity_update(eid, |cid| {
        messages.send_client(cid, msg.clone());
    });

    true
}

/// Try to continue with the indicated motion.  Returns `Ok(())` on success, or `Err(dur)` if the
/// motion failed after `dur` ms.
fn continue_motion(eng: &mut Engine,
                   now: Time,
                   next: Time,
                   eid: EntityId,
                   m: &PendingMotion) -> Result<(), Time>{
    let e = eng.world.entity(eid);

    if e.activity() != Activity::Walk {
        trace!("continue_motion({:?}): activity is not Walk", eid);
        return Err(0);
    }

    {
        let em = e.motion();
        if em.start_pos != m.pos ||
           em.velocity != m.velocity ||
           em.start_time != m.time ||
           em.end_time != None {
            trace!("continue_motion({:?}): motion does not match", eid);
            return Err(0);
        }
    }

    let pos = e.pos(now);
    let next_pos = e.pos(next);

    let s = ChunksSource {
        cache: &eng.cache,
        base_tile: scalar(0),
        plane: e.plane_id(),
    };

    let facing = m.input.to_direction().unwrap_or(e.facing());
    let speed = m.input.to_speed();
    let target_velocity = facing * scalar(speed as i32 * 50);

    let expect_step = next_pos - pos;
    let expect_dur = (next - now) as i32;

    let walk = libcommon_movement::walk(&s, pos, target_velocity, expect_step, expect_dur);

    if walk.step != expect_step || walk.duration != expect_dur {
        trace!("continue_motion({:?}): collided with obstacle", eid);
        return Err(walk.duration as Time);
    }

    Ok(())
}


fn cancel_motion(eng: &mut Engine,
                 now: Time,
                 eid: EntityId) {
    let mut e = eng.world.entity_mut(eid);

    let pos = e.pos(now);
    let anim = facing_anim(eng.data, e.facing(), 0);

    if e.activity() == Activity::Walk {
        e.set_motion(Motion::stationary(pos, now));
        e.set_anim(anim);

        let msg = ClientResponse::EntityActStand(eid, now, pos, anim);
        let messages = &mut eng.messages;
        eng.vision.entity_update(eid, |cid| {
            messages.send_client(cid, msg.clone());
        });
    }
}


fn convert_motion(m: &PendingMotion) -> Motion {
    Motion {
        start_pos: m.pos,
        velocity: m.velocity,
        start_time: m.time,
        end_time: None,
    }
}

pub fn facing_anim(data: &Data, facing: V3, speed: u8) -> AnimId {
    const ANIM_DIR_COUNT: AnimId = 8;
    static SPEED_NAME_MAP: [&'static str; 4] = ["stand", "walk", "", "run"];
    let idx = (3 * (facing.x + 1) + (facing.y + 1)) as usize;
    let anim_dir = [2, 2, 2, 3, 0, 1, 0, 0, 0][idx];
    let anim_name = format!("pony//{}-{}",
                            SPEED_NAME_MAP[speed as usize],
                            anim_dir);
    data.animation_id(&anim_name)
}


struct ChunksSource<'a> {
    cache: &'a TerrainCache,
    base_tile: V3,
    plane: PlaneId,
}

impl<'a> ShapeSource for ChunksSource<'a> {
    fn get_shape(&self, pos: V3) -> Shape {
        if pos.z < 0 || pos.z >= CHUNK_SIZE {
            return Shape::Empty;
        }

        let pos = pos + self.base_tile;
        self.cache.get(self.plane, pos).shape()
    }
}
