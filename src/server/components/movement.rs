use std::cmp;
use std::collections::hash_map::{self, HashMap};
use std::collections::VecDeque;
use std::mem;
use std::u16;

use types::*;
use libcommon_proto::types::LocalTime;

use components::{Component, EngineComponents};
use input::InputBits;
use world::Entity;


const QUEUE_SIZE: usize = 8;

#[derive(Clone, Debug)]
pub struct PendingMotion {
    pub time: Time,
    pub pos: V3,
    pub velocity: V3,
    pub input: InputBits,
}

pub struct EntityMovement {
    cur_motion: Option<PendingMotion>,

    path: VecDeque<PendingMotion>,

    /// Time of the most recently queued `PredictMotion` (non-`Rel`).
    time_base: Time,

    validity: u8,
    active: bool,
}

pub enum Update<'a> {
    Idle(u16),
    Start(&'a PendingMotion),
    Continue(u16, &'a PendingMotion),
}

impl EntityMovement {
    fn new() -> EntityMovement {
        EntityMovement {
            cur_motion: None,
            path: VecDeque::with_capacity(QUEUE_SIZE),
            time_base: 0,
            validity: 0,
            active: false,
        }
    }

    pub fn queue_motion(&mut self,
                        now: Time,
                        delay: u16,
                        input: InputBits,
                        pos: V3,
                        velocity: V3,
                        validity: u8) {
        if validity != self.validity {
            trace!("client is out of sync: validity {} != {}", validity, self.validity);
            return;
        }

        let time = now + delay as Time;
        self.path.push_back(PendingMotion {
            time: time,
            pos: pos,
            velocity: velocity,
            input: input,
        });
        self.time_base = time;
        self.active = true;
    }

    pub fn queue_motion_rel(&mut self,
                            now: Time,
                            rel_time: u16,
                            input: InputBits,
                            pos: V3,
                            velocity: V3) {
        if !self.active {
            trace!("client is out of sync: PredictMotionRel while inactive");
            return;
        }

        let time = self.time_base + LocalTime(rel_time).to_global_64(now - self.time_base);
        self.path.push_back(PendingMotion {
            time: time,
            pos: pos,
            velocity: velocity,
            input: input,
        });
    }

    pub fn invalidate(&mut self) -> u8 {
        self.cur_motion = None;
        self.path.clear();
        self.validity = self.validity.wrapping_add(1);
        self.active = false;
        self.validity
    }


    /// Update internal state, and return an indication of what happens next.  Possible results
    /// are:
    ///
    ///  * `Idle(dur)`: The entity isn't moving, and won't start for at least `dur` milliseconds.
    ///  * `Start(motion)`: At time `now`, the entity begins moving according to `motion`.
    ///  * `Continue(dur, motion)`: After time `now`, the entity continues moving according to
    ///    `motion` for at least `dur` milliseconds.
    ///
    /// Note that advancing the state up to a particular time (such as `now + TICK_MS`) may require
    /// multiple calls.  For example, to advance by 32ms, the entity may need to continue on its
    /// current path for 10ms, start a new motion, and then continue on the new path for 22ms.
    pub fn update(&mut self, now: Time) -> Update {
        let next_start = self.path.front().map(|m| m.time);
        let dur =
            if let Some(next) = next_start {
                cmp::min(cmp::max(0, next - now), u16::MAX as Time) as u16
            } else {
                u16::MAX
            };

        if next_start.is_some() && next_start.unwrap() <= now {
            self.cur_motion = Some(self.path.pop_front().unwrap());
            Update::Start(self.cur_motion.as_ref().unwrap())
        } else if let Some(ref motion) = self.cur_motion {
            // TODO: check if movement has become idle
            Update::Continue(dur, motion)
        } else {
            Update::Idle(dur)
        }
    }
}


pub struct Movement {
    map: HashMap<EntityId, EntityMovement>,
}

impl Component<Entity> for Movement {
    fn get<'a>(eng: &'a EngineComponents) -> &'a Self {
        &eng.movement
    }

    fn get_mut<'a>(eng: &'a mut EngineComponents) -> &'a mut Self {
        &mut eng.movement
    }

    fn cleanup(&mut self, id: EntityId) {
        // Does the right thing whether or not `id` is present.
        self.map.remove(&id);
    }
}

impl Movement {
    pub fn new() -> Movement {
        Movement {
            map: HashMap::new(),
        }
    }

    pub fn init(&mut self, id: EntityId) -> &mut EntityMovement {
        if !self.map.contains_key(&id) {
            self.map.insert(id, EntityMovement::new());
        }
        self.get(id)
    }

    pub fn clear(&mut self, id: EntityId) {
        self.map.remove(&id);
    }

    pub fn get(&mut self, id: EntityId) -> &mut EntityMovement {
        self.map.get_mut(&id)
            .unwrap_or_else(|| panic!("no EntityMovement for {:?}", id))
    }

    pub fn iter(&mut self) -> hash_map::IterMut<EntityId, EntityMovement> {
        self.map.iter_mut()
    }

    pub fn queue_motion(&mut self,
                        now: Time,
                        id: EntityId,
                        delay: u16,
                        input: InputBits,
                        pos: V3,
                        velocity: V3,
                        validity: u8) {
        self.init(id)
            .queue_motion(now, delay, input, pos, velocity, validity);
    }

    pub fn queue_motion_rel(&mut self,
                            now: Time,
                            id: EntityId,
                            rel_time: u16,
                            input: InputBits,
                            pos: V3,
                            velocity: V3) {
        self.init(id)
            .queue_motion_rel(now, rel_time, input, pos, velocity);
    }


    pub fn take(&mut self) -> Movement {
        mem::replace(self, Movement::new())
    }

    pub fn replace(&mut self, m: Movement) {
        *self = m;
    }
}
