var Config = require('./config').Config;
var EXPORTS = require('./export').EXPORTS;


/** @constructor */
function LogBuffer() {
    this.buf = []
    this.pos = 0;
    this.cap = 1000;
    this.len = 0;
}

LogBuffer.prototype.push = function(line) {
    this.buf[this.pos] = line;
    this.pos = (this.pos + 1) % this.cap;
    if (this.len < this.cap) {
        ++this.len;
    }
};

LogBuffer.prototype.get = function() {
    var start = this.pos - this.len;
    if (start < 0) {
        var first = this.buf.slice(start + this.cap, this.cap)
        var second = this.buf.slice(0, this.pos);
        return first.concat(second);
    } else {
        return this.buf.slice(start, this.pos);
    }
};


/** @constructor */
function LogServer(url) {
    this.ws = new WebSocket(url);
    this.buffer = [];
    this.history = new LogBuffer();

    var this_ = this;
    this.ws.onopen = function() { this_._handleOpen(); };
    this.ws.onerror = function(evt) { this_._handleError(); };

    this.config = null;
    this.asms = [];
    this.ws.onmessage = function(evt) {
        this_.config = evt.data;
        for (var i = 0; i < this_.asms.length; ++i) {
            this_.asms[i].setLogConfig(this_.config);
        }
        this_.asms = [];
    };
}

LogServer.prototype._handleOpen = function() {
    for (var i = 0; i < this.buffer.length; ++i) {
        this.ws.send(this.buffer[i]);
    }
    this.buffer = [];
};

LogServer.prototype._handleError = function() {
    console.warn('failed to connect to logging server: ' + this.ws.url);
};

LogServer.prototype.send = function(msg) {
    this.history.push(msg);
    if (this.ws.readyState == WebSocket.OPEN) {
        this.ws.send(msg);
    } else {
        this.buffer.push(msg);
    }
};

LogServer.prototype.sendConfig = function(asm) {
    if (this.config != null) {
        asm.setLogConfig(this.config);
    } else {
        this.asms.push(asm);
    }
};


/** @constructor */
function DummyLogServer() {
    this.history = new LogBuffer();
}

DummyLogServer.prototype.send = function(msg) {
    this.history.push(msg);
};

DummyLogServer.prototype.sendConfig = function(asm) {};


function makeLogServer(url) {
    console.log('make server with url ', url);
    if (url) {
        return new LogServer(url);
    } else {
        return new DummyLogServer();
    }
}

exports.LogServer = makeLogServer(Config.debug_log_server.get());
EXPORTS['logHistory'] = exports.LogServer.history;
