use server_types::*;
use std::boxed::FnBox;
use std::collections::{HashMap, HashSet};
use std::mem;
use std::u16;
use common::util::{self, SmallSet};
use tick::TICK_MS;

use engine::Engine;
use ext::Ext;


pub type TimerId = u16;

struct TimerData<E> {
    time: Time,
    callback: Box<FnBox(&mut Engine<E>)>,
}


#[cfg(not(test))]
const NUM_BUCKETS: usize = 128;
#[cfg(not(test))]
const NUM_WHEELS: usize = 4;

#[cfg(test)]
const NUM_BUCKETS: usize = 3;
#[cfg(test)]
const NUM_WHEELS: usize = 3;

struct Wheel {
    buckets: [SmallSet<TimerId>; NUM_BUCKETS],
}

impl Wheel {
    pub fn new() -> Wheel {
        Wheel {
            buckets: *mk_buckets(),
        }
    }
}

fn mk_buckets() -> Box<[SmallSet<TimerId>; NUM_BUCKETS]> {
    let buckets: Box<[SmallSet<TimerId>]> = util::make_array_with(NUM_BUCKETS, SmallSet::new);
    let buckets: Box<[SmallSet<TimerId>; NUM_BUCKETS]> = unsafe { util::size_array(buckets) };
    buckets
}

fn mk_wheels() -> Box<[Wheel; NUM_WHEELS]> {
    let wheels: Box<[Wheel]> = util::make_array_with(NUM_WHEELS, Wheel::new);
    let wheels: Box<[Wheel; NUM_WHEELS]> = unsafe { util::size_array(wheels) };
    wheels
}




/// Timer implementation, using hierarchical timing wheels.
///
///
/// # Implementation concept
///
/// We subdivide the next `TICK_MS * NUM_BUCKETS ** NUM_WHEELS` ms into buckets, with higher
/// resolution for buckets in the near future than for those far away:
///
///                  v-- now
///            ---0-1-2-3-4-5-6-7-8-9-...
///     Wheel 0: |x|x| |
///     Wheel 1: |-----|     |     |
///     Wheel 2: |-----------------|                 |                 |
///
/// Buckets marked `x` are unused because they lie in the past - all timers in those buckets have
/// already been fired.  Buckets marked `---` are also unused, because the same timespan is covered
/// by a finer grained (lower numbered) wheel.
///
/// Each timer is stored in the finest-grained bucket whose timespan contains its timestamp.  So a
/// timer set for time `2` will be stored in wheel 0, bucket 2, which covers the time `2`.  Timers
/// for `3` and `5` will both be stored in wheel 1, bucket 1, which covers `3..5` inclusive.
///
/// When the current time `now` reaches the end of a wheel, we advance the next wheel and use its
/// next bucket's contents to refill the current wheel.  For example, when `now` reaches `3`:
///
///                    v-- now
///            ---0-1-2-3-4-5-6-7-8-9-...
///     Wheel 0:       | | | |
///     Wheel 1: |xxxxx|^^^^^|     |
///     Wheel 2: |-----------------|                 |                 |
///
/// Wheel 1 bucket 1 previously contained timers expiring at times `3`, `4`, and `5`.  We remove
/// them and sort them into buckets of wheel 0, which now covers the same time span.  This leaves
/// wheel 1 bucket 1 empty.
///
///
/// # Optimization 1
///
/// This simple approach has an inefficiency.  In the example above, when `now == 2`, a timer
/// scheduled for `3` will be placed in bucket 1.1, then on the next tick, when the contents of
/// bucket 1.1 is moved into wheel 0, the timer will be moved back into bucket 0.0.  But bucket 0.0
/// is already empty and unused (`x`) when `now == 2`, so we can instead insert the new timer there
/// directly.
///
/// This optimization effectively gives us the following picture:
///
///                  v-- now
///            ---0-1-2-3-4-5-6-7-8-9-...
///     Wheel 0:     | * | |
///     Wheel 1: |-----|     |     |
///     Wheel 2: |-----------------|                 |                 |
///
/// Where `*` in wheel 1 denotes the point at which the buckets wrap around.
///
/// Note that there are now two active (non-`---`) buckets for time `4`: one in wheel 0 and one in
/// wheel 1.  We always use the finer-grained one for insertions, but for removals, we need to
/// check both: if one timer was scheduled for time `4` when `now == 0` (before wheel 0 covered
/// time `4`), and a second time was schedule for time `4` when when `now == 2`, then the first
/// timer will be in the wheel 1 bucket and the second will be in wheel 0.
///
///
/// # Optimization 2
///
/// We also reuse inactive buckets, by treating them as "in the past".  Suppose we scheduled a
/// timer for `t = 9`, when `now == 2`:
///
///                  v-- now        v-- t
///            ---0-1-2-3-4-5-6-7-8-9-...
///     Wheel 0:     | * | |
///     Wheel 1: |-----|     |     |
///     Wheel 2: ...
///
/// `9 - 2 >= 3`, so the timer won't fit in wheel 0, even with wraparound.  So we divide both `t`
/// and `now` by 3 to turn them into indices into wheel 1:
///
///               v-- now           v-- t
///            ---0-----1-----2-----3-...
///     Wheel 1: |-----|     |     |
///     Wheel 2: ...
///
/// `3 - 0 >= 3`, so again we shift to the next wheel.  But this is undesirable - we could instead
/// place the timer into bucket 1.0.
///
/// To get the effect we want, we apply a small tweak: increment `now` after dividing by
/// `NUM_BUCKETS`.  This gives us a slightly different picture:
///
///                     v-- now     v-- t
///            ---0-----1-----2-----3-...
///     Wheel 1:       |     |     *     |
///     Wheel 2: ...
///
/// Now the timer lands in the right bucket after wraparound.
///
/// This doesn't cause anything to land in the wrong bucket because, if we reach the point where
/// the division happens, it means that `t - now >= NUM_BUCKETS`, and thus, after division,
/// `t / NUM_BUCKETS - now / NUM_BUCKETS >= 1`.

pub struct Timers<E> {
    wheels: Box<[Wheel; NUM_WHEELS]>,
    overflow: HashSet<TimerId>,
    now: usize,

    timers: HashMap<TimerId, TimerData<E>>,
    next_id: TimerId,
}

impl<E> Timers<E> {
    pub fn new(now: Time) -> Timers<E> {
        assert!(now >= 0, "negative times are not supported");
        Timers {
            wheels: mk_wheels(),
            overflow: HashSet::new(),
            now: now as usize / TICK_MS as usize,

            timers: HashMap::new(),
            next_id: 0,
        }
    }

    fn tick(&mut self) -> Vec<(TimerId, TimerData<E>)> {
        let idx = self.now % NUM_BUCKETS;
        let id_set = mem::replace(&mut self.wheels[0].buckets[idx], SmallSet::new());

        // TODO: can simplify this w/o performance penalty if SmallSet iter supports ExactSize
        let mut results = Vec::with_capacity(id_set.len());
        for &id in id_set.iter() {
            if let Some(td) = self.timers.remove(&id) {
                results.push((id, td));
            } else {
                error!("tick {}: timer {} in bucket 0.{} had no associated time",
                       self.now, id, idx);
            }
        }
        results.sort_by_key(|&(id, ref td)| (td.time, id));

        // Advance time
        self.now += 1;
        trace!("ticked to time {}", self.now);
        self.maybe_refill(0);

        results
    }

    /// Refill layer `l`, if needed.
    ///
    /// Suppose we start at time 8:
    ///
    ///                              v-- now
    ///            ---0-1-2-3-4-5-6-7-8-9-...
    ///     Wheel 0:                 | * | |
    ///     Wheel 1:                   *     |     |     *
    ///     Wheel 2:                   |                 |                 *                 |
    ///
    /// Then advance to time 9:
    ///
    ///                                v-- now
    ///            ---0-1-2-3-4-5-6-7-8-9-...
    ///     Wheel 0:                 |x*~|~|
    ///     Wheel 1:                   *~~~~~|~~~~~|~~~~~*
    ///     Wheel 2:                   |                 |                 *                 |
    ///
    /// Some buckets are known to be empty, so we can apply wraparound.  The `~`s indicate
    /// partially-filled buckets - some timers that should be in these buckets are in
    /// coarser-grained buckets instead.
    ///
    ///                                v-- now
    ///            ---0-1-2-3-4-5-6-7-8-9-...
    ///     Wheel 0:                   *~|~|x*
    ///     Wheel 1:                   *~~~~~|~~~~~|~~~~~*
    ///     Wheel 2:                   |                 |                 *                 |
    ///
    /// The first two wheels have both reached their end.  We can't refill wheel 0 yet because some
    /// elements in the bucket it would fill from are still in wheel 2.  So refill wheel 1 first:
    ///
    ///                                v-- now
    ///            ---0-1-2-3-4-5-6-7-8-9-...
    ///     Wheel 0:                   *~|~|x*
    ///     Wheel 1:                   *     |     |     *
    ///     Wheel 2:                   |^^^^^^^^^^^^^^^^^|                 *                 |
    ///
    /// Then refill wheel 0:
    ///
    ///                                v-- now
    ///            ---0-1-2-3-4-5-6-7-8-9-...
    ///     Wheel 0:                   *~|~|x*
    ///     Wheel 1:                   *^^^^^|     |     *
    ///     Wheel 2:                   |xxxxxxxxxxxxxxxxx|                 *                 |
    ///
    /// Now everything is ready for the next tick:
    ///
    ///                                v-- now
    ///            ---0-1-2-3-4-5-6-7-8-9-...
    ///     Wheel 0:                   * | | *
    ///     Wheel 1:                   *xxxxx|     |     *
    ///     Wheel 2:                   |xxxxxxxxxxxxxxxxx|                 *                 |
    ///
    /// Due to the +1 post-division adjustment in `insert`, the `xxx` buckets will be wrapped
    /// around for use at the end:
    ///
    ///                                v-- now
    ///            ---0-1-2-3-4-5-6-7-8-9-...
    ///     Wheel 0:                   * | | *
    ///     Wheel 1:                         |     |     *     |
    ///     Wheel 2:                                     |                 *                 | ...
    fn maybe_refill(&mut self, l: usize) {
        if l >= NUM_WHEELS {
            return;
        }

        // Calculate `NUM_BUCKETS ** l`.
        let mut mult = 1;
        for _ in 0 .. l {
            mult *= NUM_BUCKETS;
        }

        let layer_now = self.now / mult;
        if layer_now % NUM_BUCKETS != 0 {
            // This layer doesn't need refilling.
            return;
        }

        // First, make sure the next layer is fully populated...
        self.maybe_refill(l + 1);

        // Now fill layer `l` from the contents of the next layer.
        if l + 1 < NUM_WHEELS {
            let idx = layer_now / NUM_BUCKETS % NUM_BUCKETS;
            trace!("refill wheel {} from bucket {}.{}", l, l + 1, idx);
            let ids = mem::replace(&mut self.wheels[l + 1].buckets[idx], SmallSet::new());
            for &id in ids.iter() {
                let time = unwrap_or!(self.timers.get(&id).map(|td| td.time), {
                    error!("when refilling layer {} at {}: missing timestamp for timer {}",
                           l, self.now, id);
                    continue;
                });

                let t = time as usize / TICK_MS as usize / mult;
                trace!("  place timer {} (@{}) into bucket {}", id, time, t % NUM_BUCKETS);
                self.wheels[l].buckets[t % NUM_BUCKETS].insert(id);
            }
        } else {
            trace!("refill wheel {} from overflow", l);

            let mut moved_ids = Vec::new();
            for &id in &self.overflow {
                let time = unwrap_or!(self.timers.get(&id).map(|td| td.time), {
                    error!("when refilling layer {} at {}: missing timestamp for timer {}",
                           l, self.now, id);
                    continue;
                });

                let t = time as usize / TICK_MS as usize / mult;
                if t - layer_now < NUM_BUCKETS {
                    trace!("  place timer {} (@{}) into bucket {}", id, time, t % NUM_BUCKETS);
                    self.wheels[l].buckets[t % NUM_BUCKETS].insert(id);
                    moved_ids.push(id)
                }
            }

            for id in moved_ids {
                self.overflow.remove(&id);
            }

                /* TODO: use retain() instead - requires rust 1.18
            let wheel = &mut self.wheels[l];
            let timers = &self.timers;
            let now = self.now;
            self.overflow.retain(|&id| {
                let time = unwrap_or!(timers.get(&id).map(|td| td.time), {
                    error!("when refilling layer {} at {}: missing timestamp for timer {}",
                           l, now, id);
                    return false;
                });

                let t = time as usize / TICK_MS as usize / mult;
                if t - layer_now < NUM_BUCKETS {
                    trace!("  place timer {} (@{}) into bucket {}", id, time, t % NUM_BUCKETS);
                    wheel.buckets[t % NUM_BUCKETS].insert(id);
                    false
                } else {
                    true
                }
            });
            */
        }
    }

    fn take_next_id(&mut self) -> TimerId {
        assert!(self.timers.len() <= u16::MAX as usize);

        // Find an unused ID
        while self.timers.contains_key(&self.next_id) {
            self.next_id = self.next_id.wrapping_add(1);
        }

        // Return that ID and increment
        let id = self.next_id;
        self.next_id = self.next_id.wrapping_add(1);
        id
    }

    pub fn insert<F>(&mut self, time: Time, f: F) -> TimerId
            where F: FnOnce(&mut Engine<E>) + 'static {
        let id = self.take_next_id();
        self.timers.insert(id, TimerData {
            time: time,
            callback: Box::new(f),
        });

        let now_time = self.now as Time * TICK_MS;
        let time = if time < now_time { now_time } else { time };
        let t = time as usize / TICK_MS as usize;

        let mut level_t = t;
        let mut level_now = self.now;
        for wheel in self.wheels.iter_mut() {
            if level_t < level_now + NUM_BUCKETS {
                // Landed in a wheel of the current bucket.
                wheel.buckets[level_t % NUM_BUCKETS].insert(id);
                return id;
            }

            level_t /= NUM_BUCKETS;
            level_now = level_now / NUM_BUCKETS + 1;
        }

        // Didn't land in any bucket.  Drop it into overflow instead.
        self.overflow.insert(id);
        id
    }

    pub fn remove(&mut self, id: TimerId) -> bool {
        let time = unwrap_or!(self.timers.remove(&id).map(|td| td.time), return false);

        let t = 
            if time < self.now as Time * TICK_MS { self.now }
            else { time as usize / TICK_MS as usize };

        let mut level_t = t;
        let mut level_now = self.now;
        // Check every bucket that the timer could be in.
        for wheel in self.wheels.iter_mut() {
            if level_t < level_now + NUM_BUCKETS {
                if wheel.buckets[level_t % NUM_BUCKETS].remove(&id) {
                    // Found and removed the target timer.
                    return true;
                }
            }

            level_t /= NUM_BUCKETS;
            level_now = level_now / NUM_BUCKETS + 1;
        }

        // It's not in any bucket, so try the overflow instead.
        self.overflow.remove(&id)
    }
}


pub fn run<E: Ext>(eng: &mut Engine<E>) {
    let fired = eng.timers.tick();

    for (_, td) in fired {
        td.callback.call_box((eng,));
    }
}


#[cfg(test)]
mod test {
    use super::*;
    use tick::TICK_MS;

    fn print_state<E>(t: &Timers<E>) {
        println!("timer state:");
        for i in 0 .. NUM_WHEELS {
            println!("  wheel {}:", i);
            for j in 0 .. NUM_BUCKETS {
                println!("    bucket {}.{}: {:?}", i, j,
                         t.wheels[i].buckets[j].iter().collect::<Vec<_>>());
            }
        }
        println!("  overflow: {:?}", t.overflow);
    }

    #[test]
    fn full_rotation() {
        extern crate env_logger;
        let _ = env_logger::init();

        let mut t = Timers::<()>::new(0);

        let mut ids = Vec::new();

        for i in 0 .. 40 {
            ids.push(t.insert(i * TICK_MS, |_| {}));
        }

        trace!("inserted 40 timers; ids = {:?}", ids);
        print_state(&t);

        for i in 0 .. 40 {
            let fired = t.tick().into_iter().map(|(id, td)| (id, td.time)).collect::<Vec<_>>();
            assert_eq!(fired, vec![(ids[i as usize], i * TICK_MS)]);
        }

        for _ in 40 .. 100 {
            let fired = t.tick().into_iter().map(|(id, td)| (id, td.time)).collect::<Vec<_>>();
            assert_eq!(fired, vec![]);
        }
    }


    #[test]
    fn delayed_insert() {
        extern crate env_logger;
        let _ = env_logger::init();

        let mut t = Timers::<()>::new(0);

        let mut ids = Vec::new();
        let mut ids2 = Vec::new();


        for _ in 0 .. 5 {
            t.tick();
        }

        for i in 5 .. 15 {
            ids.push(t.insert(i * TICK_MS, |_| {}));
        }
        println!("inserted 10 timers; ids = {:?}", ids);
        print_state(&t);


        for i in 5 .. 10 {
            let fired = t.tick().into_iter().map(|(id, td)| (id, td.time)).collect::<Vec<_>>();
            assert_eq!(fired, vec![(ids[i as usize - 5], i * TICK_MS)]);
        }

        for i in 10..20 {
            // +1 to ensure a specific ordering of the returned values
            ids2.push(t.insert(i * TICK_MS + 1, |_| {}));
        }
        println!("inserted 10 more timers; ids = {:?}", ids2);
        print_state(&t);


        for i in 10 .. 15 {
            let fired = t.tick().into_iter().map(|(id, td)| (id, td.time)).collect::<Vec<_>>();
            assert_eq!(fired, vec![
                       (ids[i as usize - 5], i * TICK_MS),
                       (ids2[i as usize - 10], i * TICK_MS + 1),
            ]);
        }


        for i in 15 .. 20 {
            let fired = t.tick().into_iter().map(|(id, td)| (id, td.time)).collect::<Vec<_>>();
            assert_eq!(fired, vec![(ids2[i as usize - 10], i * TICK_MS + 1)]);
        }
    }


    #[test]
    fn insert_now() {
        extern crate env_logger;
        let _ = env_logger::init();

        let mut t = Timers::<()>::new(0);

        let id = t.insert(0, |_| {});
        let id2 = t.insert(TICK_MS, |_| {});

        let fired = t.tick().into_iter().map(|(id, td)| id).collect::<Vec<_>>();
        assert_eq!(fired, vec![id]);

        let fired = t.tick().into_iter().map(|(id, td)| id).collect::<Vec<_>>();
        assert_eq!(fired, vec![id2]);
    }
}
