use server_types::*;
use std::collections::hash_map::{self, HashMap};


pub struct ConnMap {
    uid_cid: HashMap<u32, ClientId>,
    name_cid: HashMap<String, ClientId>,
    uid_name: HashMap<u32, String>,
}

impl ConnMap {
    pub fn new() -> ConnMap {
        ConnMap {
            uid_cid: HashMap::new(),
            name_cid: HashMap::new(),
            uid_name: HashMap::new(),
        }
    }

    pub fn create_conn(&mut self, uid: u32, name: String) {
        self.uid_name.insert(uid, name);
    }

    pub fn destroy_conn(&mut self, uid: u32) {
        self.uid_cid.remove(&uid);
        if let Some(name) = self.uid_name.remove(&uid) {
            self.name_cid.remove(&name);
        }
    }

    pub fn create_client(&mut self, uid: u32, id: ClientId) {
        let name = unwrap_or_error!(self.uid_name.get(&uid),
                                    "no name for uid 0x{:x}", uid);
        self.uid_cid.insert(uid, id);
        self.name_cid.insert(name.clone(), id);
    }

    pub fn destroy_client(&mut self, uid: u32) {
        self.uid_cid.remove(&uid);
        if let Some(name) = self.uid_name.get(&uid) {
            self.name_cid.remove(name);
        }
    }

    pub fn get_name(&self, uid: u32) -> Option<&str> {
        self.uid_name.get(&uid).map(|x| -> &str { x })
    }

    pub fn get_by_uid(&self, uid: u32) -> Option<ClientId> {
        self.uid_cid.get(&uid).map(|&x| x)
    }

    pub fn get_by_name(&self, name: &str) -> Option<ClientId> {
        self.name_cid.get(name).map(|&x| x)
    }

    pub fn len(&self) -> usize {
        self.name_cid.len()
    }

    pub fn names(&self) -> Names {
        self.name_cid.keys()
    }
}

pub type Names<'a> = hash_map::Keys<'a, String, ClientId>;
