//! This module is responsible for managing object lifecycles.
//!
//! The goal of this module is to eventually reach a state where:
//!
//!  - For each connection, there is a corresponding client object.
//!  - For each entity, the chunks surrounding the entity are loaded.
//!  - For each client with a camera, the chunks surrounding the camera are loaded.
//!
//! It also provides notifications on several types of events:
//!
//!  - When a new client appears.
//!  - When the number of loaded chunks around an entity or client camera decreases (usually
//!    because the entity moved, and the chunks at its destination are not loaded yet).
//!  - When the chunks surrounding an entity or client camera finish loading.
//!
//! These notifications take the form of calls into `logic::client` and `logic::entity` functions.

pub mod conn_map;
pub mod demand;
pub mod import_fsm;
pub mod login_fsm;


use server_types::*;
use std::collections::HashSet;

use engine::Engine;
use ext::Ext;
use logic;
use update::{self, Delta, UpdateItem};
use update::flags;

use self::demand::{Source, Target};


pub fn user_connect<E: Ext>(eng: &mut Engine<E>, uid: u32, name: String) {
    login_fsm::start(eng, uid, name);
}

pub fn user_disconnect<E: Ext>(eng: &mut Engine<E>, uid: u32) {
    if eng.login_fsm.contains(uid) {
        login_fsm::cancel(eng, uid);
    } else {
        eng.demand.dec_ref(Source::Conn(uid), Target::Client(Stable::new(uid as u64)));
        eng.conn_map.destroy_conn(uid);
        eng.ext.client_unloaded(uid);
    }
}


pub fn client_ready<E: Ext>(eng: &mut Engine<E>, cid: ClientId) {
    logic::client::on_ready(eng, cid);
}


pub fn start_import<E: Ext>(eng: &mut Engine<E>, delta: Delta) {
    // Kick off the delta-import state machine.  `import_fsm` will do some stuff with `demand` (to
    // load any other objects that this delta depends on), and eventually (or possibly immediately,
    // if there is nothing to load) will call `finish_import`.
    import_fsm::start(eng, delta);
}

fn finish_import<E: Ext>(eng: &mut Engine<E>, mut delta: Delta) {
    let mut load_targets = HashSet::new();
    let mut load_terrain_chunks = HashSet::new();

    for it in delta.items() {
        match it {
            UpdateItem::Client(id, f) if f.contains(flags::C_CREATED) => {
                let raw = delta.new_client_stable_id(id);
                if let Some(stable_id) = Stable::new_checked(raw) {
                    load_targets.insert(Target::Client(stable_id));
                }
            },

            UpdateItem::Plane(id, f) if f.contains(flags::P_CREATED) => {
                let raw = delta.new_plane_stable_id(id);
                if let Some(stable_id) = Stable::new_checked(raw) {
                    load_targets.insert(Target::Plane(stable_id));
                }
            },

            UpdateItem::TerrainChunk(id, f) if f.contains(flags::TC_CREATED) => {
                // Terrain chunks get special handling because we can only translate the plane ID
                // properly after `apply`.  (We could do some translation now, but it would be
                // tricky to handle the rare-but-valid case of a `Delta` containing both a plane
                // and some of the plane's chunks.)
                let tc = delta.new_terrain_chunk(id);
                load_terrain_chunks.insert((tc.plane, tc.cpos));
            },

            _ => {},
        }
    }

    trace!("finishing import of {:?} + {:?}", load_targets, load_terrain_chunks);

    let id_map = match update::apply::apply(eng, &mut delta) {
        Ok(x) => x,
        Err(e) => {
            error!("failed to apply delta: {}", e);
            return;
        },
    };

    // With the `IdRemapper` in hand, we can translate any `load_terrain_chunks` entries into
    // `load_targets`.
    for (pid, cpos) in load_terrain_chunks {
        let new_pid = unwrap_or!(id_map.map_plane(pid), {
            error!("no new plane id for terrain chunk {:?} {:?}", pid, cpos);
            continue;
        });
        load_targets.insert(Target::TerrainChunk(new_pid, cpos));
    }

    // Fire off lifecycle handlers for loaded objects.  Note this may reenter `finish_import`, via
    // `demand::mark_loaded` -> `import_fsm::advance` -> `import_fsm::finish` -> `finish_import`.
    // This process always terminates because there are only finitely many imports pending in
    // `import_fsm`, and starting a new one requires an asynchronous call through `Ext`.
    //
    // The other lifecycle handlers cannot reenter here, because the other FSMs don't keep `Delta`s
    // around internally.  Instead, they add refs to `demand` and wait for the object to appear,
    // and adding refs has no effect until the end of the tick.
    for target in load_targets {
        demand::mark_loaded(eng, target);
    }
}

