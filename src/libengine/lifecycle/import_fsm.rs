use server_types::*;
use std::u32;
use common::util::SmallSet;
use world::maps::IdMap;

use engine::Engine;
use ext::Ext;
use lifecycle;
use lifecycle::demand::{Source, Target};
use update::Delta;


mk_id_newtypes! { ImportFsmId(u32); }


struct State {
    delta: Delta,
    /// Number of planes that are currently missing (not yet loaded).
    need_planes: usize,
}

pub struct ImportFsm {
    map: IdMap<ImportFsmId, State>,
}

impl ImportFsm {
    pub fn new() -> ImportFsm {
        ImportFsm {
            map: IdMap::new(),
        }
    }
}


fn collect_stable_planes(delta: &Delta) -> SmallSet<Stable<PlaneId>> {
    let mut s = SmallSet::new();
    for (_pid, stable_id) in delta.new_plane_refs() {
        s.insert(stable_id);
    }
    s
}

pub fn start<E: Ext>(eng: &mut Engine<E>, delta: Delta) -> Option<ImportFsmId> {
    // Early exit: don't even set up the refs with `eng.demand` if everything's already loaded.
    let all_loaded = delta.new_plane_refs()
        .all(|(_, stable_id)| eng.demand.loaded(Target::Plane(stable_id)));
    if all_loaded {
        lifecycle::finish_import(eng, delta);
        return None;
    }

    let id = eng.import_fsm.map.next_id();
    let mut need_planes = 0;
    // Be careful that we don't add the same edge twice, even if it appears twice in `delta`.
    let stable_ids = collect_stable_planes(&delta);
    for &stable_id in stable_ids.iter() {
        // Increment the refcounts for all required planes.  `demand` will load them and eventually
        // call into `advance`.
        let loaded = eng.demand.inc_ref(Source::ImportFsm(id), Target::Plane(stable_id));
        if !loaded {
            need_planes += 1;
        }
    }
    // The early exit didn't fire, so there should be at least one non-loaded plane.
    assert!(need_planes > 0);

    let ok = eng.import_fsm.map.insert_as(id, State { delta, need_planes });
    assert!(ok);

    Some(id)
}

pub fn advance<E: Ext>(eng: &mut Engine<E>, id: ImportFsmId, target: Target) {
    let remove;
    {
        let state = unwrap_or_error!(eng.import_fsm.map.get_mut(id),
                                     "bad import FSM id {:?}", id);
        state.need_planes -= 1;
        trace!("{:?} got requested target {:?} - {} remain",
               id, target, state.need_planes);
        remove = state.need_planes == 0;
    }

    if remove {
        finish(eng, id);
    }
}

fn finish<E: Ext>(eng: &mut Engine<E>, id: ImportFsmId) {
    let state = eng.import_fsm.map.remove(id).unwrap();
    let delta = state.delta;

    // Remove all `demand` refs.  Presumably, whatever initiated the import in the first place also
    // has refs to the loaded objects.
    let stable_ids = collect_stable_planes(&delta);
    for &stable_id in stable_ids.iter() {
        eng.demand.dec_ref(Source::ImportFsm(id), Target::Plane(stable_id));
    }

    lifecycle::finish_import(eng, delta);
}
