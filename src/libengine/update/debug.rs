use std::fmt::{self, Write};
use syntax_exts::for_each_obj_named_macro_safe;

use update::{UpdateKey, flags};
use update::data_map::ObjectType;
use update::delta::DeltaInternals;

pub fn dump(d: &DeltaInternals) -> Result<String, fmt::Error> {
    let mut s = String::new();

    writeln!(s, " == BEGIN DELTA ==")?;

    writeln!(s, "  World: {:?}", d.flags.world(()))?;
    for_each_obj! {
        if d.flags.world(()).contains(flags::$W_NEXT_OBJ) {
            writeln!(s, "    next_{}_stable_id: {:?} -> {:?}",
                     stringify!($obj),
                     d.old.get_world_next_stable_id(ObjectType::$Obj),
                     d.new.world_next_stable_id(ObjectType::$Obj))?;
        }
    }

    for_each_obj_named_macro_safe! {
        [[world_objs]]

        let mut ids = d.flags.keys().filter_map(|k| match k {
            UpdateKey::#Obj(id) => Some(id),
            _ => None,
        }).collect::<Vec<_>>();
        ids.sort();

        for id in ids {
            let f = d.flags.#obj(id);
            writeln!(s, "  {:?}: {:?}", id, f)?;
        }
    }

    Ok(s)
}
