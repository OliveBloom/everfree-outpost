use server_types::*;
use std::collections::hash_map::{HashMap, Entry};
use std::fmt::{self, Debug};
use std::mem;
use std::ptr;
use syntax_exts::{define_expand_objs_list, expand_objs_named};
use world::objects::*;


expand_objs! {
    define_expand_objs_list! {
        data_map_objs = {
            world_time, Key = (), Value = Time;
            world_next_stable_id, Key = ObjectType, Value = StableId;
            plane_ref, Key = PlaneId, Value = (Stable<PlaneId>);

            $( $obj, Key = $ObjId, Value = $Obj; )*

            $( [[core_fields]] $obj,
                Key = $Id,
                Value = (owned_ty!($DM)); )*

            $( [[components]] $obj,
                Key = $Id,
                Value = (Option<owned_ty!($DM)>); )*
        }
    }
}

macro_rules! expand_data_map_objs {
    ($($args:tt)*) => { expand_objs_named! { [[data_map_objs]] $($args)* } };
}

expand_objs! {
    #[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
    pub enum ObjectType {
        $( $Obj, )*
    }
}

expand_data_map_objs! {
    #[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
    pub enum DataKey {
        $( $Obj($Key), )*
    }

    impl DataKey {
        fn word_size(&self) -> usize {
            match *self {
                $( DataKey::$Obj(_) =>
                    (mem::size_of::<$Value>() + mem::size_of::<usize>() - 1)
                        / mem::size_of::<usize>(), )*
            }
        }

        unsafe fn drop_in_place(&self, ptr: *mut usize) {
            match *self {
                $( DataKey::$Obj(_) => ptr::drop_in_place::<$Value>(ptr as *mut $Value), )*
            }
        }

        unsafe fn as_debug(&self, ptr: *const usize) -> &Debug {
            match *self {
                $( DataKey::$Obj(_) => &*(ptr as *const $Value) as &Debug, )*
            }
        }
    }

    impl fmt::Debug for DataMap {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            let mut fm = f.debug_map();
            for (key, &idx) in &self.data {
                unsafe {
                    let ptr = self.storage.as_ptr().add(idx);
                    fm.entry(key as &Debug, key.as_debug(ptr));
                }
            }
            fm.finish()
        }
    }

    impl Drop for DataMap {
        fn drop(&mut self) {
            // Panics here will cause leaks, by draining the `data` iterator without clearing
            // `storage`.  Hopefully none of the destructors we're calling will panic, but it's
            // not the end of the world if they do.
            unsafe {
                for (key, idx) in self.data.drain() {
                    let ptr = self.storage.as_mut_ptr().add(idx);
                    key.drop_in_place(ptr);
                }
            }
        }
    }

    impl DataMap {
        $(
        pub fn $insert_obj(&mut self, key: $Key, value: $Value) {
            unsafe { self.insert(DataKey::$Obj(key), value); }
        }

        pub fn $try_remove_obj(&mut self, key: $Key) -> Option<$Value> {
            unsafe { self.remove(&DataKey::$Obj(key)) }
        }

        pub fn $remove_obj(&mut self, key: $Key) -> $Value {
            self.$try_remove_obj(key)
                .unwrap_or_else(|| panic!("no entry for key {:?}", key))
        }

        pub fn $contains_obj(&self, key: $Key) -> bool {
            self.data.contains_key(&DataKey::$Obj(key))
        }

        pub fn $get_obj(&self, key: $Key) -> Option<&$Value> {
            unsafe { self.get(&DataKey::$Obj(key)) }
        }

        pub fn $obj(&self, key: $Key) -> &$Value {
            self.$get_obj(key)
                .unwrap_or_else(|| panic!("no entry for key {:?}", key))
        }

        pub fn $get_obj_mut(&mut self, key: $Key) -> Option<&mut $Value> {
            unsafe { self.get_mut(&DataKey::$Obj(key)) }
        }

        pub fn $obj_mut(&mut self, key: $Key) -> &mut $Value {
            self.$get_obj_mut(key)
                .unwrap_or_else(|| panic!("no entry for key {:?}", key))
        }
        )*
    }

    impl Clone for DataMap {
        fn clone(&self) -> DataMap {
            let mut d = DataMap {
                data: HashMap::with_capacity(self.data.len()),
                storage: Vec::with_capacity(self.storage.len()),
            };

            for &key in self.data.keys() {
                match key {
                    $(DataKey::$Obj(_) => {
                        unsafe {
                            let v = self.get::<$Value>(&key).unwrap();
                            d.insert::<$Value>(key, v.clone());
                        }
                    },)*
                }
            }

            d
        }
    }
}


/// Heterogeneous map, used as data storage for `Delta`.
///
/// The key of each entry is a `DataKey`, and the value type depends on the `DataKey` variant.
/// Since the map methods need to take and return different types, there is a separate set of
/// methods (`insert_foo`, `remove_foo`, `get_foo`, etc.) for each key variant.
pub struct DataMap {
    /// Pointers to additional data for some events.  For example, if an object was removed, the
    /// previous state of the object will be stored here.
    ///
    /// *Invariant*: For each `(key, idx)` entry in `data`, an initialized value of the appropriate
    /// value type for `key` is present at `storage[idx]`.
    data: HashMap<DataKey, usize>,

    /// Backing storage for additional data.
    storage: Vec<usize>,
}

impl DataMap {
    pub fn new() -> DataMap {
        DataMap {
            data: HashMap::new(),
            storage: Vec::new(),
        }
    }

    pub fn keys<'a>(&'a self) -> impl Iterator<Item=DataKey> + 'a {
        self.data.keys().map(|&x| x)
    }

    // These functions are unsafe because they assume that `T` has the proper type for `key`.

    unsafe fn insert<T>(&mut self, key: DataKey, value: T) {
        assert!(mem::align_of::<T>() <= mem::align_of::<usize>());

        match self.data.entry(key) {
            Entry::Vacant(e) => {
                self.storage.reserve(key.word_size());

                let idx = self.storage.len();
                self.storage.set_len(idx + key.word_size());

                // We are still memory-safe on panic at this point.  The uninitialized elements of
                // `storage` aren't accessed on drop, because no `data` entry references them yet.

                e.insert(idx);
                // Must not panic past this point.  There is now a `data` entry pointing to `idx`,
                // so  we must initialize the memory there.
                let ptr = self.storage.as_mut_ptr().add(idx) as *mut T;
                ptr.write(value);
            },

            Entry::Occupied(e) => {
                let &idx = e.get();
                let ptr = self.storage.as_mut_ptr().add(idx) as *mut T;
                // The DataKey is the same, so the amount and type of data stored at `idx` must be
                // correct for `T`.  That means we can do a normal move into `*ptr`, overwriting
                // the old value.
                //
                // TODO - memory unsafe!  If the `drop` method for `*ptr` panics, `*ptr` will be
                // dropped a second time by `DataMap::drop`.
                *ptr = value;
            },
        }
    }

    unsafe fn remove<T>(&mut self, key: &DataKey) -> Option<T> {
        debug_assert!(mem::align_of::<T>() <= mem::align_of::<usize>());
        let idx = self.data.remove(key)?;
        // Any panic past this point may leak the value.

        debug_assert!(idx + key.word_size() <= self.storage.len());

        let ptr = self.storage.as_mut_ptr().add(idx) as *mut T;
        Some(ptr.read())
    }

    unsafe fn get<'a, T>(&'a self, key: &DataKey) -> Option<&'a T> {
        let &idx = self.data.get(key)?;
        let ptr = self.storage.as_ptr().add(idx) as *const T;
        Some(&*ptr)
    }

    unsafe fn get_mut<'a, T>(&'a mut self, key: &DataKey) -> Option<&'a mut T> {
        let &idx = self.data.get(key)?;
        let ptr = self.storage.as_mut_ptr().add(idx) as *mut T;
        Some(&mut *ptr)
    }


    /// Merge `other` into `self`, letting elements of `other` take precedence over elements of
    /// `self` when both contain the same key.
    pub fn merge_over(&mut self, mut other: DataMap) {
        // Memory allocation and element drops may panic.  If that happens, elements of `other` may
        // be leaked.
        for (key, old_idx) in mem::replace(&mut other.data, HashMap::new()) {
            let size = key.word_size();
            match self.data.entry(key) {
                Entry::Vacant(e) => unsafe {
                    self.storage.reserve(size);
                    let new_idx = self.storage.len();
                    self.storage.set_len(new_idx + size);

                    let src = &other.storage[old_idx .. old_idx + size];
                    let dest = &mut self.storage[new_idx .. new_idx + size];
                    e.insert(new_idx);
                    // Must not panic past this point.
                    dest.copy_from_slice(src);
                },

                Entry::Occupied(e) => unsafe {
                    let &new_idx = e.get();
                    let src = &other.storage[old_idx .. old_idx + size];
                    let dest = &mut self.storage[new_idx .. new_idx + size];
                    // TODO - memory unsafe!  If the `drop` method for `*ptr` panics, `*ptr` will
                    // be dropped a second time by `DataMap::drop`.
                    key.drop_in_place(dest.as_mut_ptr());
                    dest.copy_from_slice(src);
                },
            }
        }
    }

    /// Merge `other` into `self`, retaining the original elements from `self` when both contain
    /// the same key.
    pub fn merge_under(&mut self, mut other: DataMap) {
        // Memory allocation and element drops may panic.  If that happens, elements of `other` may
        // be leaked.
        for (key, old_idx) in mem::replace(&mut other.data, HashMap::new()) {
            let size = key.word_size();
            match self.data.entry(key) {
                Entry::Vacant(e) => unsafe {
                    self.storage.reserve(size);
                    let new_idx = self.storage.len();
                    self.storage.set_len(new_idx + size);

                    let src = &other.storage[old_idx .. old_idx + size];
                    let dest = &mut self.storage[new_idx .. new_idx + size];
                    e.insert(new_idx);
                    // Must not panic past this point.
                    dest.copy_from_slice(src);
                },

                Entry::Occupied(_) => unsafe {
                    // Leave `self`'s entry unchanged, and drop the one from `other`.
                    let src = &mut other.storage[old_idx .. old_idx + size];
                    key.drop_in_place(src.as_mut_ptr());
                },
            }
        }
    }
}
