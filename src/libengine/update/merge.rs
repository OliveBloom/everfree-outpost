pub fn merge(mut a: Delta, mut b: Delta) -> Delta {
    a.as_internals_mut(|ai| {
        b.as_internals_mut(|bi| {
            merge_internals(ai, bi);
        });
    });
    a
}

fn merge_internals(a: &mut DeltaInternals, b: &mut DeltaInternals) {
    a.flags.merge(&b.flags);
}
