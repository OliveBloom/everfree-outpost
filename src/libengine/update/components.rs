use server_types::*;
use std::collections::HashMap;
use common::util::StringResult;
use server_config::data;
use server_extra::Value;
use world::import::IdRemapper;

use component::bitmap::{self, Bitmap};
use component::camera::CameraPos;
use component::char_invs::{self, CharInvs};
use component::compass_targets::{CompassTargets, LootedVaults, VaultId};
use component::contents;
use component::crafting::{self, CraftingState, StationId};
use component::dialog::Dialog;
use component::key_value;
use component::ward_map::{self, WardMap};
use component::ward_permits::{self, WardPermits};
use engine::Engine;
use ext::Ext;
use update::traits::*;


pub struct WardPermitsTag;

impl Component<()> for WardPermitsTag {
    type DataMode = Medium<WardPermits>;

    fn get<E>(eng: &Engine<E>, _id: ()) -> Option<WardPermits> {
        Some(eng.ward_permits.clone())
    }

    fn apply<E: Ext>(eng: &mut Engine<E>,
                     _id: (),
                     component: Option<WardPermits>) {
        if let Some(wp) = component {
            ward_permits::apply_silent(eng, wp);
        }
    }
}


pub struct CameraPosTag;

impl Component<ClientId> for CameraPosTag {
    type DataMode = Small<CameraPos>;

    fn get<E>(eng: &Engine<E>, id: ClientId) -> Option<CameraPos> {
        eng.client_cameras.get(id)
    }
}


pub struct DialogTag;

impl Component<ClientId> for DialogTag {
    type DataMode = Medium<Dialog>;

    fn get<E>(eng: &Engine<E>, id: ClientId) -> Option<Dialog> {
        eng.dialogs.get(id).cloned()
    }
}


pub struct CharInvsTag;

impl Component<EntityId> for CharInvsTag {
    type DataMode = Small<CharInvs>;

    fn get<E>(eng: &Engine<E>, id: EntityId) -> Option<CharInvs> {
        eng.char_invs.get(id)
    }

    fn pre_apply(invs: &mut CharInvs,
                 mapper: &IdRemapper) -> StringResult<()> {
        let map_inv = |iid| {
            mapper.map_inventory(iid)
                .ok_or_else(|| format!("failed to map {:?}", iid))
        };
        invs.main = map_inv(invs.main)?;
        invs.equip = map_inv(invs.equip)?;
        invs.ability = map_inv(invs.ability)?;
        Ok(())
    }

    fn apply<E: Ext>(eng: &mut Engine<E>,
                     id: EntityId,
                     component: Option<CharInvs>) {
        char_invs::apply_silent(eng, id, component);
    }
}


pub struct WardMapTag;

impl Component<PlaneId> for WardMapTag {
    type DataMode = Medium<WardMap>;

    fn get<E>(eng: &Engine<E>, id: PlaneId) -> Option<WardMap> {
        eng.ward_maps.get(id).cloned()
    }

    fn apply<E: Ext>(eng: &mut Engine<E>,
                     id: PlaneId,
                     component: Option<WardMap>) {
        ward_map::apply_silent(eng, id, component);
    }
}


pub struct BitmapTag;

impl Component<StructureId> for BitmapTag {
    type DataMode = Medium<Bitmap>;

    fn get<E>(eng: &Engine<E>, id: StructureId) -> Option<Bitmap> {
        eng.bitmaps.get(id).cloned()
    }

    fn apply<E: Ext>(eng: &mut Engine<E>,
                     id: StructureId,
                     component: Option<Bitmap>) {
        bitmap::apply_silent(eng, id, component);
    }
}


pub struct ContentsTag;

impl Component<StructureId> for ContentsTag {
    type DataMode = Small<InventoryId>;

    fn get<E>(eng: &Engine<E>, id: StructureId) -> Option<InventoryId> {
        eng.contents.get(id)
    }

    fn pre_apply(iid: &mut InventoryId,
                 mapper: &IdRemapper) -> StringResult<()> {
        *iid = mapper.map_inventory(*iid)
            .ok_or_else(|| format!("failed to map {:?}", *iid))?;
        Ok(())
    }

    fn apply<E: Ext>(eng: &mut Engine<E>,
                     id: StructureId,
                     component: Option<InventoryId>) {
        contents::apply_silent(eng, id, component);
    }
}


pub struct CraftingTag;

impl Component<StructureId> for CraftingTag {
    type DataMode = MediumSmall<CraftingState, InventoryId>;

    fn get<E>(eng: &Engine<E>, id: StructureId) -> Option<(CraftingState, InventoryId)> {
        eng.crafting.get(StationId::Structure(id))
            .map(|(s, i)| (s.clone(), i))
    }

    fn pre_apply(component: &mut (CraftingState, InventoryId),
                 mapper: &IdRemapper) -> StringResult<()> {
        let s = &component.0;
        let inv = &mut component.1;

        if data().get_recipe(s.recipe()).is_none() {
            fail!("bad recipe id {}", s.recipe());
        }

        *inv = mapper.map_inventory(*inv)
            .ok_or_else(|| format!("failed to map {:?}", *inv))?;

        Ok(())
    }

    fn apply<E: Ext>(eng: &mut Engine<E>,
                     id: StructureId,
                     component: Option<(CraftingState, InventoryId)>) {
        crafting::apply_silent(eng, StationId::Structure(id), component);
    }
}


pub struct KeyValueTag;

impl KeyValueTag {
    fn get<E>(eng: &Engine<E>, id: AnyId) -> Option<HashMap<String, Value>> {
        eng.key_value.get_map(id).cloned()
    }

    fn pre_apply(component: &mut HashMap<String, Value>,
                 mapper: &IdRemapper) -> StringResult<()> {
        for (k,v) in component {
            expand_objs! {
                match *v {
                    $( Value::$ObjId(ref mut id) => {
                        *id = mapper.$map_obj(*id)
                            .ok_or_else(|| format!("id mapping failed for \
                                key-value entry {:?} = {:?}", k, *id))?;
                    }, )*
                    _ => {},
                }
            }
        }
        Ok(())
    }

    fn apply<E: Ext>(eng: &mut Engine<E>,
                     id: AnyId,
                     component: Option<HashMap<String, Value>>) {
        key_value::apply_silent(eng, id, component);
    }
}

for_each_obj! {
    impl Component<$ObjId> for KeyValueTag {
        type DataMode = Medium<HashMap<String, Value>>;

        fn get<E>(eng: &Engine<E>, id: $ObjId) -> Option<HashMap<String, Value>> {
            <KeyValueTag>::get(eng, AnyId::$Obj(id))
        }

        fn pre_apply(component: &mut HashMap<String, Value>,
                     mapper: &IdRemapper) -> StringResult<()> {
            <KeyValueTag>::pre_apply(component, mapper)
        }

        fn apply<E: Ext>(eng: &mut Engine<E>,
                         id: $ObjId,
                         component: Option<HashMap<String, Value>>) {
            <KeyValueTag>::apply(eng, AnyId::$Obj(id), component);
        }
    }
}


pub struct CompassTargetsTag;

impl Component<TerrainChunkId> for CompassTargetsTag {
    type DataMode = Medium<CompassTargets>;

    fn get<E>(eng: &Engine<E>, id: TerrainChunkId) -> Option<CompassTargets> {
        eng.compass_data.get_compass_targets(id).cloned()
    }

    fn apply<E: Ext>(eng: &mut Engine<E>,
                     id: TerrainChunkId,
                     component: Option<CompassTargets>) {
        eng.compass_data.apply_compass_targets_silent(id, component);
    }
}

pub struct LootedVaultsTag;

impl Component<PlaneId> for LootedVaultsTag {
    type DataMode = Medium<LootedVaults>;

    fn get<E>(eng: &Engine<E>, id: PlaneId) -> Option<LootedVaults> {
        eng.compass_data.get_looted_vaults(id).cloned()
    }

    fn apply<E: Ext>(eng: &mut Engine<E>,
                     id: PlaneId,
                     component: Option<LootedVaults>) {
        eng.compass_data.apply_looted_vaults_silent(id, component);
    }
}

pub struct ParentVaultTag;

impl Component<StructureId> for ParentVaultTag {
    type DataMode = Small<VaultId>;

    fn get<E>(eng: &Engine<E>, id: StructureId) -> Option<VaultId> {
        eng.compass_data.get_parent_vault(id)
    }

    fn apply<E: Ext>(eng: &mut Engine<E>,
                     id: StructureId,
                     component: Option<VaultId>) {
        eng.compass_data.apply_parent_vault_silent(id, component);
    }
}
