use server_types::*;
use world::objects::*;

use update::traits::*;


pub struct ClientPawnTag;
impl Field<Client> for ClientPawnTag {
    type DataMode = Small<Option<EntityId>>;
    fn get_ref(obj: &Client) -> Option<EntityId> { obj.pawn }
}

pub struct ClientNameTag;
impl Field<Client> for ClientNameTag {
    type DataMode = Medium<String>;
    fn get_ref(obj: &Client) -> &String { &obj.name }
}


pub struct EntityPlaneTag;
impl Field<Entity> for EntityPlaneTag {
    type DataMode = Small<PlaneId>;
    fn get_ref(obj: &Entity) -> PlaneId { obj.plane }
}

pub struct EntityActivityTag;
impl Field<Entity> for EntityActivityTag {
    type DataMode = MediumSmall<Activity, Time>;
    fn get_ref(obj: &Entity) -> (&Activity, Time) { (&obj.activity, obj.activity_start) }
}

pub struct EntityAppearanceTag;
impl Field<Entity> for EntityAppearanceTag {
    type DataMode = Medium<Appearance>;
    fn get_ref(obj: &Entity) -> &Appearance { &obj.appearance }
}

pub struct EntityAttachmentTag;
impl Field<Entity> for EntityAttachmentTag {
    type DataMode = Small<EntityAttachment>;
    fn get_ref(obj: &Entity) -> EntityAttachment { obj.attachment }
}


pub struct InventoryContentsTag;
impl Field<Inventory> for InventoryContentsTag {
    type DataMode = LargeSlice<Item>;
    fn get_ref(obj: &Inventory) -> &[Item] { &obj.contents }
}

pub struct InventoryAttachmentTag;
impl Field<Inventory> for InventoryAttachmentTag {
    type DataMode = Small<InventoryAttachment>;
    fn get_ref(obj: &Inventory) -> InventoryAttachment { obj.attachment }
}


pub struct TerrainChunkBlocksTag;
impl Field<TerrainChunk> for TerrainChunkBlocksTag {
    type DataMode = Large<BlockChunk>;
    fn get_ref(obj: &TerrainChunk) -> &BlockChunk { &obj.blocks }
}


pub struct StructureTemplateTag;
impl Field<Structure> for StructureTemplateTag {
    type DataMode = Small<TemplateId>;
    fn get_ref(obj: &Structure) -> TemplateId { obj.template }
}

pub struct StructureAttachmentTag;
impl Field<Structure> for StructureAttachmentTag {
    type DataMode = Small<StructureAttachment>;
    fn get_ref(obj: &Structure) -> StructureAttachment { obj.attachment }
}


pub struct FlagsTag;
for_each_obj! {
    impl Field<$Obj> for FlagsTag {
        type DataMode = Small<$ObjFlags>;
        fn get_ref(obj: &$Obj) -> $ObjFlags { obj.flags }
    }
}

pub struct StableIdTag;
for_each_obj! {
    impl Field<$Obj> for StableIdTag {
        type DataMode = Small<StableId>;
        fn get_ref(obj: &$Obj) -> StableId { obj.stable_id }
    }
}
