use server_types::*;
use std::cell::UnsafeCell;
use std::collections::HashMap;
use std::mem;
use common::movement::InputBits;
use server_extra::Value;
use syntax_exts::{for_each_obj_named, expand_objs_named_macro_safe};
use world;
use world::objects::*;

use engine::Engine;
use ext::Ext;

use update::delta::{Delta, IncDelta};
use update::event::Event;


macro_rules! dispatchers {
    ($( fn $func:ident ( $($arg:ident : $ArgTy:ty,)* ); )*) => {
        $(
        pub fn $func(&self, $($arg: $ArgTy),*) {
            unsafe { self.with(|inner| inner.$func($($arg),*)) };
        }
        )*
    };
}

/// Helper struct for incrementally constructing a `Delta`.
///
/// ## Usage
///
/// Correct usage of `UpdateBuilder` is essential to ensure that the `Delta` invariants are upheld
/// and that no game state updates are omitted from the delta.
///
/// The requirements for object core data are as follows:
///
///  - On object creation: invoke `$obj_created`.  This will set the `X_CREATED` flag, but won't
///    record any data in the `old` or `new` maps.
///  - On object destruction: invoke `$obj_destroyed` with a copy of the object's final state.
///    This will set `X_DESTROYED` and record the final state in the `new` map.
///  - On field modification: invoke `$obj_field` with a copy of the old field value.  This will
///    set `$O_FIELD` and record the value in the `old` map.
///  - For object loading, invoke `$obj_created` followed by `$obj_loaded`.  During loading, do
///    *not* call any `$obj_field` methods, even if the loading procedure internally creates the
///    object first and then sets its fields as a separate step.  (This rule is for performance,
///    not correctness.  Wrongly calling `$obj_field` on load is equivalent to loading and then
///    immediately setting `obj.field = obj.field`.)
///  - For object unloading, invoke `$obj_destroyed` followed by `$obj_unloaded`.
///
/// The requirements for component data are as follows:
///
///  - On object creation: do nothing.  Implicitly, all components of a created object are `None`.
///  - On object destruction: invoke `$obj_component_destroyed` with the final component value.
///    This will record the value in the `old` map but will *not* set `$O_COMPONENT`.  (This allows
///    distinguishing create-modify-destroy sequences from create-destroy when components are
///    involved.)
///  - On component modification: invoke `$obj_component` with a copy of the old component value.
///    If this component had no data present before, use `None`.  This will set the `$O_COMPONENT`
///    flag and record the value in the `old` map.
///  - For object loading and unloading, do nothing special beyond normal creation/destruction
///    handling.  Specifically, for loading, do not call `$obj_component`, even if loading is
///    implemented as setting the component to the loaded value.  (Like avoiding `$obj_field`
///    during loading, this rule is for performance, not correctness.)  This will likely require
///    separate entry points for "set component value" and "load component value".
///
pub struct UpdateBuilder {
    inner: UnsafeCell<IncDelta>,
}

impl UpdateBuilder {
    pub fn new() -> UpdateBuilder {
        UpdateBuilder {
            inner: UnsafeCell::new(IncDelta::new()),
        }
    }

    pub unsafe fn with<F: FnOnce(&mut IncDelta) -> R, R>(&self, f: F) -> R {
        f(&mut *self.inner.get())
    }

    pub fn take<E: Ext>(&self, eng: &Engine<E>) -> Delta {
        let inc = unsafe { mem::replace(&mut *self.inner.get(), IncDelta::new()) };
        inc.into_delta(eng)
    }

    pub fn event<T: Event>(&self, evt: T) {
        unsafe { self.with(|inner| inner.event(evt)) };
    }

    expand_objs! {
        dispatchers! {
            $(
                fn $obj_created(id: $ObjId,);
                fn $obj_destroyed(id: $ObjId, old: $Obj,);
                fn $obj_loaded(id: $ObjId,);
                fn $obj_unloaded(id: $ObjId,);
            )*

            $( [[core_fields]]
                fn $obj(id: $Id, old: ref_ty!($DM),);
            )*

            $( [[components]]
                fn $obj(id: $Id, old: Option<ref_ty!($DM)>,);
                fn $obj_move(id: $Id, old: Option<owned_ty!($DM)>,);
                fn $obj_destroyed(id: $Id, new: Option<owned_ty!($DM)>,);
            )*
        }
    }

    for_each_obj_named! { [[update_events]]
        expand_objs_named_macro_safe! { $args
            dispatchers! {
                fn $event_obj(#(#obj: #Ty,)*);
            }
        }
    }

    dispatchers! {
        fn object_key_value(id: AnyId, old: Option<&HashMap<String, Value>>,);
        fn object_key_value_destroyed(id: AnyId, new: Option<HashMap<String, Value>>,);
    }
}

// It's easy to accidentally get infinite recursion here, since the trait and inherent method names
// are identical.
#[deny(unconditional_recursion)]
impl world::ext::Update for UpdateBuilder {
    for_each_obj! {
        fn $create_obj(&self, id: $ObjId) {
            self.$obj_created(id);
        }

        fn $destroy_obj(&self, id: $ObjId, old: $Obj){ 
            self.$obj_destroyed(id, old);
        }

        fn $obj_flags(&self, id: $ObjId, old: $ObjFlags) {
            self.$obj_world_flags(id, old);
        }

        fn $obj_stable_id(&self, id: $ObjId, old: StableId) {
            self.$obj_stable_id(id, old);
        }

        fn $next_obj_stable_id(&self, old: StableId) {
            unsafe { self.with(|inner| inner.$next_obj_stable_id(old)) };
        }
    }

    fn client_name(&self, id: ClientId, old: &String) {
        self.client_name(id, &old);
    }
    fn client_pawn(&self, id: ClientId, old: Option<EntityId>) {
        self.client_pawn(id, old);
    }
    fn client_current_input(&self, _id: ClientId, _old: InputBits) {
        // Not tracked
    }

    fn entity_activity(&self, id: EntityId, act: &Activity, start: Time) {
        self.entity_activity(id, (act, start));
    }
    fn entity_plane(&self, id: EntityId, old: PlaneId) {
        self.entity_plane(id, old);
    }
    fn entity_appearance(&self, id: EntityId, old: &Appearance) {
        self.entity_appearance(id, old);
    }
    fn entity_attachment(&self, id: EntityId, old: EntityAttachment) {
        self.entity_attachment(id, old);
    }

    fn inventory_contents(&self, id: InventoryId, old: &[Item]) {
        self.inventory_contents(id, old);
    }
    fn inventory_attachment(&self, id: InventoryId, old: InventoryAttachment) {
        self.inventory_attachment(id, old);
    }

    fn terrain_chunk_blocks(&self, id: TerrainChunkId, old: &BlockChunk) {
        self.terrain_chunk_blocks(id, old);
    }

    fn structure_template(&self, id: StructureId, old: TemplateId) {
        self.structure_template(id, old);
    }
    fn structure_attachment(&self, id: StructureId, old: StructureAttachment) {
        self.structure_attachment(id, old);
    }
}

