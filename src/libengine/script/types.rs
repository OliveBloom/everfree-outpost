use multipython::{self, PyBox, PyResult};
use server_python_conv;
use syntax_exts::python_module;

pub fn register() -> PyResult<PyBox> {
    let m = python_module! {
        name = _outpost_types;
        data: () = ();
    }?;
    server_python_conv::init_module(m.borrow())?;
    multipython::util::register_module(m.borrow())?;
    Ok(m)
}

