use server_types::*;
use std::collections::{HashMap, HashSet};
use std::collections::hash_map::Entry;

use engine::Engine;
use ext::Ext;


#[derive(Clone, Debug)]
pub struct WardPermits {
    permits: HashMap<Stable<ClientId>, HashSet<String>>,
}

impl WardPermits {
    pub fn new() -> WardPermits {
        WardPermits {
            permits: HashMap::new(),
        }
    }

    pub fn from_parts(permits: HashMap<Stable<ClientId>, HashSet<String>>) -> WardPermits {
        WardPermits { permits }
    }

    pub fn permits(&self) -> &HashMap<Stable<ClientId>, HashSet<String>> {
        &self.permits
    }

    fn add(&mut self, owner: Stable<ClientId>, user: String) -> bool {
        self.permits.entry(owner).or_insert_with(HashSet::new).insert(user)
    }

    fn remove(&mut self, owner: Stable<ClientId>, user: &str) -> bool {
        match self.permits.entry(owner) {
            Entry::Vacant(_) => false,
            Entry::Occupied(mut e) => {
                let removed = e.get_mut().remove(user);
                if e.get().len() == 0 {
                    e.remove();
                }
                removed
            },
        }
    }

    pub fn get<'a>(&'a self, owner: Stable<ClientId>)
                   -> impl Iterator<Item=&'a String>+'a {
        self.permits.get(&owner)
            .into_iter()
            .flat_map(|names| names.iter())
    }

    pub fn has_permit(&self, owner: Stable<ClientId>, user: &str) -> bool {
        self.permits.get(&owner)
            .map_or(false, |names| names.contains(user))
    }
}

/// Change the ward map for plane `id` to `wm`, without recording any update events.  This is used
/// to import a ward map from a saved game.
pub fn apply_silent<E: Ext>(eng: &mut Engine<E>,
                            wp: WardPermits) {
    eng.w.ward_permits = wp;
}

pub fn add_permit<E: Ext>(eng: &mut Engine<E>,
                          owner: Stable<ClientId>,
                          user: String) -> bool {
    let added = eng.w.ward_permits.add(owner, user.clone());
    trace!("{:?}: added permit for {:?} ({})", owner, user, added);
    if added {
        eng.update.event_permit_add(owner, user);
    }
    added
}

pub fn remove_permit<E: Ext>(eng: &mut Engine<E>,
                             owner: Stable<ClientId>,
                             user: String) -> bool {
    let removed = eng.w.ward_permits.remove(owner, &user);
    trace!("{:?}: removed permit for {:?} ({})", owner, user, removed);
    if removed {
        eng.update.event_permit_remove(owner, user);
    }
    removed
}
