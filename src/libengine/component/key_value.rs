use server_types::*;
use std::borrow::Borrow;
use std::collections::hash_map::{HashMap, Entry};
use std::hash::Hash;

use server_extra::Value;

use engine::Engine;
use ext::Ext;
use update::UpdateBuilder;


pub struct KeyValueStore {
    map: HashMap<AnyId, HashMap<String, Value>>,
}

impl KeyValueStore {
    pub fn new() -> KeyValueStore {
        KeyValueStore {
            map: HashMap::new(),
        }
    }

    fn insert(&mut self, id: AnyId, key: String, value: Value, ub: &UpdateBuilder) {
        ub.event_key_value_set(id, key.clone(), value.clone());
        self.map.entry(id).or_insert_with(HashMap::new).insert(key, value);
    }

    fn remove(&mut self, id: AnyId, key: String, ub: &UpdateBuilder) {
        match self.map.entry(id) {
            Entry::Occupied(mut e) => {
                e.get_mut().remove(&key);
                if e.get().len() == 0 {
                    e.remove();
                }
            },
            Entry::Vacant(_) => {},
        }
        ub.event_key_value_clear(id, key);
    }

    pub fn get<Q: ?Sized>(&self, id: AnyId, key: &Q) -> Option<&Value>
            where String: Borrow<Q>, Q: Hash+Eq {
        self.map.get(&id).and_then(|m| m.get(key))
    }

    pub fn get_map(&self, id: AnyId) -> Option<&HashMap<String, Value>> {
        self.map.get(&id)
    }

    pub fn contains<Q: ?Sized>(&self, id: AnyId, key: &Q) -> bool
            where String: Borrow<Q>, Q: Hash+Eq {
        self.map.get(&id).map_or(false, |m| m.contains_key(key))
    }

    pub fn handle_destroy(&mut self, id: AnyId, ub: &UpdateBuilder) {
        ub.object_key_value_destroyed(id, self.map.remove(&id));
    }
}


pub fn set<E: Ext>(eng: &mut Engine<E>,
                   id: AnyId,
                   key: String,
                   value: Value) {
    eng.w.key_value.insert(id, key, value, &eng.update);
}

pub fn clear<E: Ext>(eng: &mut Engine<E>,
                     id: AnyId,
                     key: String) {
    eng.w.key_value.remove(id, key, &eng.update);
}

pub fn apply_silent<E: Ext>(eng: &mut Engine<E>,
                            id: AnyId,
                            map: Option<HashMap<String, Value>>) {
    if let Some(map) = map {
        eng.w.key_value.map.insert(id, map);
    } else {
        eng.w.key_value.map.remove(&id);
    }
}
