use server_types::*;

use component::dialog::{Dialog, DialogImpl, Action};
use engine::Engine;
use ext::Ext;
use logic;


#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Inventory {
    pub id: InventoryId,
}

pub fn inventory(id: InventoryId) -> Dialog {
    Dialog::Inventory(Inventory {
        id: id,
    })
}


#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Abilities {
    pub id: InventoryId,
}

pub fn abilities(id: InventoryId) -> Dialog {
    Dialog::Abilities(Abilities {
        id: id,
    })
}


#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Equipment {
    pub inv_id: InventoryId,
    pub equip_id: InventoryId,
}

pub fn equipment(inv_id: InventoryId, equip_id: InventoryId) -> Dialog {
    Dialog::Equipment(Equipment {
        inv_id: inv_id,
        equip_id: equip_id,
    })
}


#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Container {
    pub id0: InventoryId,
    pub id1: InventoryId,
    pub sid: StructureId,
}

pub fn container(id0: InventoryId, id1: InventoryId, sid: StructureId) -> Dialog {
    Dialog::Container(Container {
        id0: id0,
        id1: id1,
        sid: sid,
    })
}


impl DialogImpl for Inventory {
    fn handle_action<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId, action: Action) {
        handle_action(self, eng, id, action);
    }

    fn with_inventories<F: FnMut(InventoryId)>(&self, mut f: F) {
        f(self.id);
    }
}

impl DialogImpl for Abilities {
    fn handle_action<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId, action: Action) {
        handle_action(self, eng, id, action);
    }

    fn with_inventories<F: FnMut(InventoryId)>(&self, mut f: F) {
        f(self.id);
    }
}

impl DialogImpl for Equipment {
    fn handle_action<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId, action: Action) {
        handle_action(self, eng, id, action);
    }

    fn with_inventories<F: FnMut(InventoryId)>(&self, mut f: F) {
        f(self.inv_id);
        f(self.equip_id);
    }
}

impl DialogImpl for Container {
    fn handle_action<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId, action: Action) {
        handle_action(self, eng, id, action);
    }

    fn with_inventories<F: FnMut(InventoryId)>(&self, mut f: F) {
        f(self.id0);
        f(self.id1);
    }

    fn with_structures<F: FnMut(StructureId)>(&self, mut f: F) {
        f(self.sid);
    }
}


fn handle_action<D, E>(d: &D, eng: &mut Engine<E>, id: ClientId, action: Action)
        where D: DialogImpl, E: Ext {
    match action {
        Action::MoveItem { src_inv, src_slot, dest_inv, dest_slot, count } => {
            // Only allow transfers between inventories relevant to the dialog.
            let mut saw_src = false;
            let mut saw_dest = false;
            d.with_inventories(|iid| {
                if iid == src_inv {
                    saw_src = true;
                }
                if iid == dest_inv {
                    saw_dest = true;
                }
            });

            if saw_src && saw_dest {
                logic::inventory::transfer_slot(
                    eng, src_inv, src_slot, dest_inv, dest_slot, count);
            }
        },

        _ => {
            error!("{:?} sent invalid action for inventory dialog: {:?}", id, action);
        },
    }
}
