use server_types::*;

use component::crafting::{self, StationId};
use component::dialog::{Dialog, DialogImpl, Action, SpecialSub};
use engine::Engine;
use ext::Ext;
use logic;


#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Crafting {
    pub station: StationId,
    pub class_mask: u32,
    pub contents: InventoryId,
    pub char_main: InventoryId,
    pub char_ability: InventoryId,
}

pub fn crafting(station: StationId,
                class_mask: u32,
                contents: InventoryId,
                char_main: InventoryId,
                char_ability: InventoryId) -> Dialog {
    Dialog::Crafting(Crafting {
        station: station,
        class_mask: class_mask,
        contents: contents,
        char_main: char_main,
        char_ability: char_ability,
    })
}

impl DialogImpl for Crafting {
    fn handle_action<E: Ext>(&self, eng: &mut Engine<E>, id: ClientId, action: Action) {
        match action {
            Action::MoveItem { src_inv, src_slot, dest_inv, dest_slot, count } => {
                let src_ok = src_inv == self.contents || src_inv == self.char_main;
                let dest_ok = dest_inv == self.contents || dest_inv == self.char_main;

                if src_ok && dest_ok {
                    logic::inventory::transfer_slot(
                        eng, src_inv, src_slot, dest_inv, dest_slot, count);
                } else {
                    error!("{:?} attempted invalid MoveItem from {:?} to {:?}",
                           id, src_inv, dest_inv);
                }
            },

            Action::CraftingStart { recipe_id, count } => {
                if logic::crafting::can_craft(eng,
                                              recipe_id,
                                              self.class_mask,
                                              Some(self.char_ability)) {
                    crafting::start(eng, self.station, self.contents, recipe_id, count);
                }
            },

            Action::CraftingStop => {
                crafting::pause(eng, self.station);
            },

            _ => {
                error!("{:?} sent invalid action for crafting dialog: {:?}", id, action);
            },
        }
    }

    fn handle_cancel<E: Ext>(&self, eng: &mut Engine<E>, _id: ClientId) {
        crafting::pause(eng, self.station);
    }

    fn handle_close<E: Ext>(&self, eng: &mut Engine<E>, _id: ClientId) {
        crafting::pause(eng, self.station);
    }

    fn with_inventories<F: FnMut(InventoryId)>(&self, mut f: F) {
        f(self.contents);
        f(self.char_main);
        f(self.char_ability);
    }

    fn with_structures<F: FnMut(StructureId)>(&self, mut f: F) {
        if let StationId::Structure(sid) = self.station {
            f(sid);
        }
    }

    fn with_special_subs<F: FnMut(SpecialSub)>(&self, mut f: F) {
        f(SpecialSub::CraftingState(self.station));
    }
}
