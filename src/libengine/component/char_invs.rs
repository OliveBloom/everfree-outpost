use server_types::*;
use std::collections::{HashMap, HashSet};
use syntax_exts::for_each_obj_named;
use world::objects::*;
use world::object_ref::{ObjectRef, ObjectMut};

use engine::Engine;
use ext::{Ext, WorldExt};
use update::UpdateBuilder;


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct CharInvs {
    pub main: InventoryId,
    pub ability: InventoryId,
    pub equip: InventoryId,
}

pub struct CharInvMap {
    map: HashMap<EntityId, CharInvs>,

    /// IDs of all inventories that appear in `map`.  Currently used only for sanity checking.
    invs: HashSet<InventoryId>,
}

impl CharInvMap {
    pub fn new() -> CharInvMap {
        CharInvMap {
            map: HashMap::new(),
            invs: HashSet::new(),
        }
    }

    /// Insert the inventories for the given entity.  This always triggers an event.
    fn insert(&mut self, id: EntityId, invs: CharInvs, ub: &UpdateBuilder) {
        if let Some(old_invs) = self.remove_inner(id) {
            error!("{:?} already had character inventories", id);
            ub.entity_char_invs(id, Some(old_invs));
        } else {
            ub.entity_char_invs(id, None);
        }

        for &iid in &[invs.main, invs.ability, invs.equip] {
            if !self.invs.insert(iid) {
                error!("when adding {:?} for {:?}: inventory already belongs to an entity",
                       iid, id);
            }
        }

        self.map.insert(id, invs);
    }

    fn remove_inner(&mut self, id: EntityId)  -> Option<CharInvs> {
        if let Some(invs) = self.map.remove(&id) {
            for &iid in &[invs.main, invs.ability, invs.equip] {
                if !self.invs.remove(&iid) {
                    error!("when removing {:?} for {:?}: inventory not present in set", iid, id);
                }
            }
            Some(invs)
        } else {
            None
        }
    }

    pub fn contains(&self, id: EntityId) -> bool {
        self.map.contains_key(&id)
    }

    pub fn get(&self, id: EntityId) -> Option<CharInvs> {
        self.map.get(&id).cloned()
    }

    pub fn handle_destroy_entity(&mut self, id: EntityId, ub: &UpdateBuilder) {
        if let Some(invs) = self.remove_inner(id) {
            ub.entity_char_invs_destroyed(id, Some(invs));
        }
    }
}

/// Add character inventories to an entity.
pub fn add<E: Ext>(eng: &mut Engine<E>,
                   id: EntityId,
                   invs: CharInvs) {
    eng.w.char_invs.insert(id, invs, &eng.update);
}

/// Set the component value without recording an update.  Call this only when applying a `Delta`.
pub fn apply_silent<E: Ext>(eng: &mut Engine<E>,
                            id: EntityId,
                            opt_invs: Option<CharInvs>) {
    trace!("apply_silent: {:?} => {:?}", id, opt_invs);
    if let Some(invs) = opt_invs {
        eng.w.char_invs.map.insert(id, invs);
    } else {
        eng.w.char_invs.map.remove(&id);
    }
}



pub trait CharInvsExt {
    fn has_char_invs(&self) -> bool;
    fn char_invs(&self) -> CharInvs;
    fn get_char_invs(&self) -> Option<CharInvs>;
}

pub trait CharInvsExtMut {
    fn set_char_invs(&mut self, invs: CharInvs);
}

for_each_obj_named! {
    { object_ref; object_mut; }

    impl<'a, 'd> CharInvsExt for $Obj<'a, WorldExt, Entity> {
        fn has_char_invs(&self) -> bool {
            self.world().char_invs.contains(self.id())
        }

        fn char_invs(&self) -> CharInvs {
            let id = self.id();
            self.get_char_invs()
                .unwrap_or_else(|| panic!("{:?} has no char_invs", id))
        }

        fn get_char_invs(&self) -> Option<CharInvs> {
            let id = self.id();
            self.world().char_invs.get(id)
        }
    }

}

impl<'a, 'd> CharInvsExtMut for ObjectMut<'a, WorldExt, Entity> {
    fn set_char_invs(&mut self, invs: CharInvs) {
        let id = self.id();
        let w = self.world_mut();
        w.components.char_invs.insert(id, invs, &w.update);
    }
}
