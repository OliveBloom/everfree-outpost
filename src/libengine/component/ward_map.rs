use server_types::*;
use std::collections::HashMap;

use engine::Engine;
use ext::Ext;
use update::UpdateBuilder;


#[derive(Clone, Debug)]
pub struct Ward {
    pub owner: Stable<ClientId>,
    pub owner_name: String,
    pub region: Region<V2>,
}

#[derive(Clone, Debug)]
pub struct WardMap {
    /// Invariant: Every `Ward` in this list has a distinct `owner`.
    wards: Vec<Ward>,
}

impl WardMap {
    pub fn new() -> WardMap {
        WardMap {
            wards: Vec::new(),
        }
    }

    pub fn from_iter<I: Iterator<Item=Ward>>(it: I) -> WardMap {
        WardMap {
            wards: it.collect(),
        }
    }

    pub fn contains(&self, id: Stable<ClientId>) -> bool {
        self.get(id).is_some()
    }

    pub fn get(&self, id: Stable<ClientId>) -> Option<&Ward> {
        self.wards.iter().find(|&w| w.owner == id)
    }

    pub fn wards_at<'a>(&'a self, region: Region<V2>) -> impl Iterator<Item=&'a Ward>+'a {
        self.wards.iter().filter(move |&w| {
            w.region.overlaps(region)
        })
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item=&'a Ward>+'a {
        self.wards.iter()
    }
}

pub struct WardMaps {
    maps: HashMap<PlaneId, WardMap>,
}

impl WardMaps {
    pub fn new() -> WardMaps {
        WardMaps {
            maps: HashMap::new(),
        }
    }

    pub fn contains(&self, id: PlaneId) -> bool {
        self.maps.contains_key(&id)
    }

    pub fn get(&self, id: PlaneId) -> Option<&WardMap> {
        self.maps.get(&id)
    }

    pub fn handle_destroy_plane(&mut self, id: PlaneId, ub: &UpdateBuilder) {
        ub.plane_ward_map_destroyed(id, self.maps.remove(&id));
    }

    fn create(&mut self, id: PlaneId, ub: &UpdateBuilder) {
        let ok = self.maps.insert(id, WardMap::new()).is_none();
        assert!(ok, "create: duplicate ward map for {:?}", id);
        ub.plane_ward_map(id, None);
    }

    fn destroy(&mut self, id: PlaneId, ub: &UpdateBuilder) {
        if let Some(wm) = self.maps.remove(&id) {
            ub.plane_ward_map_destroyed(id, Some(wm));
        } else {
            panic!("destroy: no ward map for {:?}", id);
        }
    }
}

/// Create a new, empty ward map for plane `id`.  Panics if `id` already has a ward map.
pub fn create_map<E: Ext>(eng: &mut Engine<E>,
                          id: PlaneId) {
    eng.w.ward_maps.create(id, &eng.update);
}

/// Destroy the ward map for plane `id`.  Panics if `id` does not have a ward map.
pub fn destroy_map<E: Ext>(eng: &mut Engine<E>,
                           id: PlaneId) {
    eng.w.ward_maps.destroy(id, &eng.update);
}

/// Change the ward map for plane `id` to `wm`, without recording any update events.  This is used
/// to import a ward map from a saved game.
pub fn apply_silent<E: Ext>(eng: &mut Engine<E>,
                            id: PlaneId,
                            wm: Option<WardMap>) {
    if let Some(wm) = wm {
        eng.w.ward_maps.maps.insert(id, wm);
    } else {
        eng.w.ward_maps.maps.remove(&id);
    }
}

pub enum AddError {
    NoWardMap,
    KeyPresent,
}

/// Try to add a ward at the given location.  Fails (returning `false`) if the target plane does
/// not have a ward map.
pub fn add_ward<E: Ext>(eng: &mut Engine<E>,
                        pid: PlaneId,
                        region: Region<V2>,
                        owner: Stable<ClientId>,
                        owner_name: String) -> Result<(), AddError> {
    let wm = unwrap_or!(eng.w.ward_maps.maps.get_mut(&pid),
                        return Err(AddError::NoWardMap));
    if wm.contains(owner) {
        return Err(AddError::KeyPresent);
    }

    wm.wards.push(Ward {
        owner, region,
        owner_name: owner_name.clone(),
    });

    eng.update.event_ward_add(pid, owner, owner_name, region);
    Ok(())
}

/// Remove all wards owned by the given client.
pub fn remove_ward<E: Ext>(eng: &mut Engine<E>,
                           pid: PlaneId,
                           owner: Stable<ClientId>) {
    let wm = unwrap_or!(eng.w.ward_maps.maps.get_mut(&pid), return);
    wm.wards.retain(|w| w.owner != owner);
    eng.update.event_ward_remove(pid, owner);
}
