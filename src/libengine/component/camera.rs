use server_types::*;
use std::collections::hash_map::{HashMap, Entry};

use ext::Ext;
use engine::Engine;
use update::UpdateBuilder;



pub type CameraPos = (PlaneId, V3);


/// Camera position overrides for clients.  By default, the client's camera tracks the pawn, or (if
/// there is no pawn) displays nothing.  This module provides a third option, allowing the camera
/// to be pointed at an arbitrary location.
pub struct ClientCameras {
    map: HashMap<ClientId, CameraPos>,
}

impl ClientCameras {
    pub fn new() -> ClientCameras {
        ClientCameras {
            map: HashMap::new(),
        }
    }

    pub fn get(&self, id: ClientId) -> Option<CameraPos> {
        self.map.get(&id).cloned()
    }

    pub fn contains(&self, id: ClientId) -> bool {
        self.map.contains_key(&id)
    }

    pub fn set(&mut self, id: ClientId, plane: PlaneId, pos: V3, u: &UpdateBuilder) {
        trace!("set: {:?} = {:?}, {:?}", id, plane, pos);
        match self.map.entry(id) {
            Entry::Vacant(e) => {
                e.insert((plane, pos));
                u.client_camera_pos(id, None);
            },
            Entry::Occupied(e) => {
                u.client_camera_pos(id, Some(*e.get()));
                *e.into_mut() = (plane, pos);
            },
        }

        trace!("  wtf {:?}", self.get(id));
    }

    pub fn clear(&mut self, id: ClientId, u: &UpdateBuilder) {
        trace!("clear: {:?}", id);
        match self.map.entry(id) {
            Entry::Vacant(_) => {},
            Entry::Occupied(e) => {
                let old = e.remove();
                u.client_camera_pos(id, Some(old));
            },
        }
    }

    pub fn handle_destroy_client(&mut self, id: ClientId, ub: &UpdateBuilder) {
        trace!("destroy: {:?}", id);
        if let Some(camera_pos) = self.map.remove(&id) {
            ub.client_camera_pos_destroyed(id, Some(camera_pos));
        }
    }
}

pub fn set<E: Ext>(eng: &mut Engine<E>, id: ClientId, plane: PlaneId, pos: V3) {
    eng.w.client_cameras.set(id, plane, pos, &eng.update);
}

pub fn clear<E: Ext>(eng: &mut Engine<E>, id: ClientId) {
    eng.w.client_cameras.clear(id, &eng.update);
}
