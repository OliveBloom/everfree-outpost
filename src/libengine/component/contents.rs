//! Component for tracking the "contents" inventory of a container structure.
//!
//! This is largely similar to `char_invs`, which tracks inventories for entities instead.
use server_types::*;
use std::collections::HashMap;
use syntax_exts::for_each_obj_named;
use world::objects::*;
use world::object_ref::{ObjectRef, ObjectMut};

use engine::Engine;
use ext::{Ext, WorldExt};
use update::UpdateBuilder;


pub struct Contents {
    map: HashMap<StructureId, InventoryId>,
}

impl Contents {
    pub fn new() -> Contents {
        Contents {
            map: HashMap::new(),
        }
    }

    pub fn insert(&mut self, sid: StructureId, iid: InventoryId, ub: &UpdateBuilder) {
        if let Some(old_iid) = self.map.remove(&sid) {
            error!("{:?} already has contents", sid);
            ub.structure_contents(sid, Some(old_iid));
        } else {
            ub.structure_contents(sid, None);
        }

        self.map.insert(sid, iid);
    }

    pub fn remove(&mut self, sid: StructureId, ub: &UpdateBuilder) {
        if let Some(old_iid) = self.map.remove(&sid) {
            ub.structure_contents(sid, Some(old_iid));
        }
    }

    pub fn contains(&self, sid: StructureId) -> bool {
        self.map.contains_key(&sid)
    }

    pub fn get(&self, sid: StructureId) -> Option<InventoryId> {
        self.map.get(&sid).cloned()
    }

    pub fn handle_destroy_structure(&mut self, id: StructureId, ub: &UpdateBuilder) {
        if let Some(old_iid) = self.map.remove(&id) {
            ub.structure_contents_destroyed(id, Some(old_iid));
        }
    }
}

/// Set the component value without recording an update.  Call this only when applying a `Delta`.
pub fn apply_silent<E: Ext>(eng: &mut Engine<E>,
                            sid: StructureId,
                            opt_iid: Option<InventoryId>) {
    if let Some(iid) = opt_iid {
        eng.w.contents.map.insert(sid, iid);
    } else {
        eng.w.contents.map.remove(&sid);
    }
}



pub trait ContentsExt {
    fn has_contents(&self) -> bool;
    fn contents(&self) -> InventoryId;
    fn get_contents(&self) -> Option<InventoryId>;
}

pub trait ContentsExtMut {
    fn set_contents(&mut self, iid: InventoryId);
    fn clear_contents(&mut self);
}

for_each_obj_named! {
    { object_ref; object_mut; }

    impl<'a, 'd> ContentsExt for $Obj<'a, WorldExt, Structure> {
        fn has_contents(&self) -> bool {
            self.world().contents.contains(self.id())
        }

        fn contents(&self) -> InventoryId {
            let id = self.id();
            self.get_contents()
                .unwrap_or_else(|| panic!("{:?} has no contents", id))
        }

        fn get_contents(&self) -> Option<InventoryId> {
            let id = self.id();
            self.world().contents.get(id)
        }
    }

}

impl<'a, 'd> ContentsExtMut for ObjectMut<'a, WorldExt, Structure> {
    fn set_contents(&mut self, iid: InventoryId) {
        let id = self.id();
        let w = self.world_mut();
        w.components.contents.insert(id, iid, &w.update);
    }

    fn clear_contents(&mut self) {
        let id = self.id();
        let w = self.world_mut();
        w.components.contents.remove(id, &w.update);
    }
}
