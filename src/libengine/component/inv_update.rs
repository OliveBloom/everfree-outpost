use server_types::*;
use std::collections::hash_map::{HashMap, Entry};
use std::collections::HashSet;
use std::mem;
use common::util;

use component::crafting;
use engine::Engine;
use ext::Ext;
use script::Scripting;
use update::{Update, UpdateItem, flags};


#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum Behavior {
    UpdateCrafting(crafting::StationId),
    RunScript,
}


pub struct InvUpdates {
    map: HashMap<InventoryId, Behavior>,

    /// Separate map for inventories with multiple behaviors.  This is an uncommon case, so it gets
    /// handled separately.
    ///
    /// *Invariant*: The keys of `map` and `multi_map` are disjoint.
    ///
    /// *Invariant*: Every `Vec` in `multi_map` has at least two elements.
    multi_map: HashMap<InventoryId, Vec<Behavior>>,

    pending: HashSet<InventoryId>,
}

impl InvUpdates {
    pub fn new() -> InvUpdates {
        InvUpdates {
            map: HashMap::new(),
            multi_map: HashMap::new(),
            pending: HashSet::new(),
        }
    }

    /// Note: Only call this method for inventories that currently exist - otherwise the behavior
    /// entry may leak!
    pub fn add_behavior(&mut self, id: InventoryId, b: Behavior) {
        if let Some(bs) = self.multi_map.get_mut(&id) {
            bs.push(b);
            return;
        }

        match self.map.entry(id) {
            Entry::Occupied(e) => {
                let old_b = e.remove();
                self.multi_map.insert(id, vec![old_b, b]);
            },
            Entry::Vacant(e) => {
                e.insert(b);
            },
        }
    }

    pub fn remove_behavior(&mut self, id: InventoryId, b: Behavior) {
        match self.map.entry(id) {
            Entry::Vacant(_) => {},
            Entry::Occupied(e) => {
                if e.get() == &b {
                    e.remove();
                }
                return;
            },
        }

        match self.multi_map.entry(id) {
            Entry::Vacant(_) => {},
            Entry::Occupied(mut e) => {
                if util::remove_first(e.get_mut(), &b) {
                    // Actually removed an item.  If the list got small enough, we might need
                    // to move back from `multi_map` to `map`.
                    if e.get().len() == 1 {
                        let v = e.remove();
                        self.map.insert(id, v[0]);
                    } else if e.get().len() == 0 {
                        error!("invariant broken: multi_map entry has 0 items");
                        e.remove();
                    }
                    return;
                }
            },
        }

        error!("couldn't find behavior {:?} for {:?}", b, id);
    }

    pub fn update(&mut self, u: &Update) {
        for it in u.items() {
            match it {
                UpdateItem::Inventory(id, f) => {
                    if f.contains(flags::I_DESTROYED) {
                        self.map.remove(&id);
                        self.multi_map.remove(&id);
                        self.pending.remove(&id);
                        continue;
                    }

                    if f.contains(flags::I_CONTENTS) {
                        self.pending.insert(id);
                    }
                },

                _ => {},
            }
        }
    }
}

pub fn tick<E: Ext>(eng: &mut Engine<E>) {
    for iid in mem::replace(&mut eng.inv_updates.pending, HashSet::new()) {
        if let Some(&b) = eng.inv_updates.map.get(&iid) {
            run_behavior(eng, iid, b);
        } else if let Some(bs) = eng.inv_updates.multi_map.get(&iid).cloned() {
            for b in bs {
                run_behavior(eng, iid, b);
            }
        }
    }
}

fn run_behavior<E: Ext>(eng: &mut Engine<E>,
                        iid: InventoryId,
                        b: Behavior) {
    match b {
        Behavior::UpdateCrafting(id) => {
            crafting::update_state(eng, id);
        },
        Behavior::RunScript => {
            Scripting::handle(eng, |h| h.on_inventory_change(iid));
        },
    }
}
