use server_types::*;
use server_config::data;

use engine::Engine;
use ext::Ext;
use script::Scripting;

/// Maximum distance (in pixels) for characters to interact with objects.  This is one TILE_SIZE
/// bigger than the client's NEAR_DIST, to account for the fact that the client-side mouse code
/// chesks if any pixel of the target square is within range, while here we check only if the
/// origin is within range.
const NEAR_DIST: i32 = 96;

pub fn on_point_interact<E: Ext>(eng: &mut Engine<E>,
                                 id: ClientId,
                                 pos: V3) {
    if let Some(sid) = get_target_structure(eng, id, pos) {
        Scripting::handle(eng, |h| h.on_action_interact(id, sid));
    }
}

pub fn on_point_destroy<E: Ext>(eng: &mut Engine<E>,
                                id: ClientId,
                                pos: V3) {
    if let Some(sid) = get_target_structure(eng, id, pos) {
        Scripting::handle(eng, |h| h.on_action_destroy(id, sid));
    }
}

pub fn on_point_use_item<E: Ext>(eng: &mut Engine<E>,
                                 id: ClientId,
                                 pos: V3,
                                 item: ItemId) {
    Scripting::handle(eng, |h| h.on_action_use_item(id, pos, item));
}

fn get_target_structure<E: Ext>(eng: &mut Engine<E>,
                                id: ClientId,
                                pos: V3) -> Option<StructureId> {
    // Validity checks
    let pid = {
        let c = unwrap_or!(eng.w.get_client(id), {
            error!("get_target_structure: nonexistent {:?}", id);
            return None;
        });
        let e = unwrap_or!(c.pawn(), {
            warn!("get_target_structure: no pawn for {:?}", id);
            return None;
        });
        let dist2 = (e.pos(eng.w.now) - pos * TILE_SIZE).mag2();
        if dist2 > NEAR_DIST * NEAR_DIST {
            info!("{:?} tried to interact with a distant object ({:?} -> {:?}, {} > {})",
                  id, e.pos(eng.w.now), pos * TILE_SIZE,
                  dist2, NEAR_DIST * NEAR_DIST);
            return None;
        }

        e.plane_id()
    };

    // Find a structure at the indicated position.
    let target = eng.structure_cache.structures_at_point(&eng.w, pid, pos)
        .filter_map(|sid| {
            let s = eng.w.structure(sid);
            let t = data().template(s.template());
            let bounds = Region::sized(t.size) + s.pos();
            if t.shape()[bounds.index(pos)].contains(B_OCCUPIED) {
                Some((t.layer, sid))
            } else {
                None
            }
        }).max();
    target.map(|(_, id)| id)
}
