//! Handling of implicit references that update as object fields change.  For example, as entity
//! position changes, this module updates its `demand` refs to point to the chunks surrounding its
//! current location.

use server_types::*;
use world::World;

use ext::WorldExt;
use lifecycle::demand::{Demand, Source, Target};
use update::Delta;
use update::flags;

const VIEW_SIZE: Region<V2> = Region {
    min: V2 { x: -2, y: -2 },
    max: V2 { x: 3, y: 4 },
};

pub fn update_client_refs(id: ClientId,
                          f: flags::ClientUpdate,
                          demand: &mut Demand,
                          w: &World<WorldExt>,
                          delta: &Delta) {
    info!("client autorefs for {:?} {:?}", id, f);
    let created = f.contains(flags::C_CREATED);
    let destroyed = f.contains(flags::C_DESTROYED);
    let changed = f.contains(flags::C_CAMERA_POS);

    if (created && destroyed) || (!created && !destroyed && !changed) {
        return;
    }

    let old = if created { None } else { delta.old_client_camera_pos(id) };
    let new = if destroyed { None } else { delta.new_client_camera_pos(id) };
    info!("client camera changed {:?} -> {:?}", old, new);

    if old == new {
        return;
    }

    if let Some((plane, pos)) = old {
        let stable_pid = w.plane(plane).stable_id().unwrap();
        demand.dec_ref(Source::ClientCamera(id), Target::Plane(stable_pid));
        for cpos in (VIEW_SIZE + pos.px_to_cpos()).points() {
            demand.dec_ref(Source::ClientCamera(id), Target::TerrainChunk(plane, cpos));
        }
    }

    if let Some((plane, pos)) = new {
        let stable_pid = w.plane(plane).stable_id().unwrap();
        demand.inc_ref(Source::ClientCamera(id), Target::Plane(stable_pid));
        for cpos in (VIEW_SIZE + pos.px_to_cpos()).points() {
            demand.inc_ref(Source::ClientCamera(id), Target::TerrainChunk(plane, cpos));
        }
    }
}

pub fn update_entity_refs(id: EntityId,
                          f: flags::EntityUpdate,
                          demand: &mut Demand,
                          w: &World<WorldExt>,
                          delta: &Delta) {
    let created = f.contains(flags::E_CREATED);
    let destroyed = f.contains(flags::E_DESTROYED);
    let plane_changed = f.contains(flags::E_PLANE);
    let pos_changed = f.contains(flags::E_ACTIVITY);
    let changed = plane_changed || pos_changed;

    if (created && destroyed) || (!created && !destroyed && !changed) {
        return;
    }

    let old_plane =
        if created { None }
        else if destroyed || plane_changed { Some(delta.old_entity_plane(id)) }
        else { Some(w.entity(id).plane_id()) };
    let new_plane =
        if destroyed { None }
        else if created || plane_changed { Some(delta.new_entity_plane(id)) }
        else { Some(w.entity(id).plane_id()) };

    let get_stable_plane_id = |pid| {
        if delta.plane_flags(pid).intersects(flags::P_CREATED | flags::P_DESTROYED) {
            Stable::new(delta.new_plane(pid).stable_id)
        } else {
            w.plane(pid).stable_id().unwrap()
        }
    };

    let old_pos =
        if created { None }
        else if destroyed || pos_changed { Some(delta.old_entity_activity(id).0.start_pos()) }
        else { Some(w.entity(id).activity().start_pos()) };
    let new_pos =
        if destroyed { None }
        else if created || pos_changed { Some(delta.new_entity_activity(id).0.start_pos()) }
        else { Some(w.entity(id).activity().start_pos()) };

    if old_plane == new_plane && old_pos == new_pos {
        return;
    }

    if let (Some(plane), Some(pos)) = (old_plane, old_pos) {
        let stable_pid = get_stable_plane_id(plane);
        demand.dec_ref(Source::EntityPos(id), Target::Plane(stable_pid));
        for cpos in (VIEW_SIZE + pos.px_to_cpos()).points() {
            demand.dec_ref(Source::EntityPos(id), Target::TerrainChunk(plane, cpos));
        }
    }

    if let (Some(plane), Some(pos)) = (new_plane, new_pos) {
        let stable_pid = get_stable_plane_id(plane);
        demand.inc_ref(Source::EntityPos(id), Target::Plane(stable_pid));
        for cpos in (VIEW_SIZE + pos.px_to_cpos()).points() {
            demand.inc_ref(Source::EntityPos(id), Target::TerrainChunk(plane, cpos));
        }
    }
}

pub fn update_inventory_refs(_id: InventoryId,
                             _f: flags::InventoryUpdate,
                             _demand: &mut Demand,
                             _w: &World<WorldExt>,
                             _delta: &Delta) {
}

pub fn update_plane_refs(_id: PlaneId,
                         _f: flags::PlaneUpdate,
                         _demand: &mut Demand,
                         _w: &World<WorldExt>,
                         _delta: &Delta) {
}

pub fn update_terrain_chunk_refs(id: TerrainChunkId,
                                 f: flags::TerrainChunkUpdate,
                                 demand: &mut Demand,
                                 w: &World<WorldExt>,
                                 delta: &Delta) {
    let created = f.contains(flags::TC_CREATED);
    let destroyed = f.contains(flags::TC_DESTROYED);

    if (created && destroyed) || (!created && !destroyed) {
        return;
    }

    if created {
        let stable_pid = w.terrain_chunk(id).plane().stable_id().unwrap();
        demand.inc_ref(Source::TerrainChunk(id), Target::Plane(stable_pid));
    }
    if destroyed {
        let pid = delta.new_terrain_chunk(id).plane;
        let stable_pid = w.get_plane(pid).map_or_else(
            || Stable::new(delta.new_plane_stable_id(pid)),
            |p| p.stable_id().unwrap());
        demand.dec_ref(Source::TerrainChunk(id), Target::Plane(stable_pid));
    }
}

pub fn update_structure_refs(_id: StructureId,
                             _f: flags::StructureUpdate,
                             _demand: &mut Demand,
                             _w: &World<WorldExt>,
                             _delta: &Delta) {
}
