//! Contains functions for nontrivial updates and queries of the game state.  These are mostly
//! meant to be called from scripts, through the `outpost.core.logic` module.

pub mod client;
pub mod inventory;
pub mod structure;

pub mod auto_refs;
pub mod crafting;
pub mod permission;
