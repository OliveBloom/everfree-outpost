use server_types::*;
use server_config::data;
use world::{self, World};

use engine::Engine;


pub fn can_place<E>(eng: &Engine<E>, pid: PlaneId, pos: V3, template_id: TemplateId) -> bool {
    overlap_check(&eng.w, None, pid, pos, template_id)
}

pub fn can_replace<E>(eng: &Engine<E>, sid: StructureId, template_id: TemplateId) -> bool {
    let s = unwrap_or!(eng.w.get_structure(sid), {
        error!("replacement overlap check on nonexistent {:?}", sid);
        return false;
    });
    // Check if it's okay to place a new `template_id` structure at `s`'s position, ignoring any
    // overlaps with `s` itself.
    overlap_check(&eng.w, Some(sid), s.plane_id(), s.pos(), template_id)
}

/// Perform an overlap check for placing a new `template_id` structure at `(pid, pos)`.  Ignores
/// overlaps with structure `ignore_sid`, if set.
fn overlap_check<E: world::Ext>(w: &World<E>,
                                ignore_sid: Option<StructureId>,
                                pid: PlaneId,
                                pos: V3,
                                template_id: TemplateId) -> bool {
    let data = data();
    let template = data.template(template_id);

    let p = unwrap_or!(w.get_plane(pid), {
        error!("overlap check on nonexistent {:?}", pid);
        return false;
    });
    let bounds = Region::sized(template.size);
    for offset in bounds.points() {
        let subpos = pos + offset;
        let shape = template.shape_at(offset);
        if !shape.contains(B_OCCUPIED) {
            continue;
        }

        for sid in p.structures_at(subpos) {
            if ignore_sid == Some(sid) {
                continue;
            }

            let s = w.structure(sid);
            let other_template = data.template(s.template());
            let other_shape = other_template.shape_at(subpos - s.pos());

            if other_shape.contains(B_OCCUPIED) && other_shape.parts().intersects(shape.parts()) {
                return false;
            }
        }
    }

    true
}
