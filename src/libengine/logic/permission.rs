use server_types::*;

use engine::Engine;

pub fn get_blocker<E>(eng: &Engine<E>,
                      actor: Stable<ClientId>,
                      actor_name: String,
                      plane: PlaneId,
                      region: Region<V2>) -> Option<String> {
    let wm = unwrap_or!(eng.ward_maps.get(plane), return None);
    for w in wm.wards_at(region) {
        if w.owner != actor && !eng.w.ward_permits.has_permit(w.owner, &actor_name) {
            return Some(w.owner_name.clone());
        }
    }
    return None;
}
