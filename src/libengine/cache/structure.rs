use server_types::*;
use std::collections::hash_map::{HashMap, Entry};
use std::collections::hash_set::{self, HashSet};
use server_config::data;
use world;
use world::World;

use ext::FullWorld;
use update::{Update, flags};


pub struct StructureCache {
    map: HashMap<(PlaneId, V2), HashSet<StructureId>>,
}

impl StructureCache {
    pub fn new() -> StructureCache {
        StructureCache {
            map: HashMap::new(),
        }
    }

    fn create_structure(&mut self,
                        id: StructureId,
                        plane: PlaneId,
                        pos: V3,
                        template: TemplateId) {
        let bounds = Region::sized(data().template(template).size) + pos;
        let chunk_bounds = bounds.reduce().div_round_signed(CHUNK_SIZE);
        for cpos in chunk_bounds.points() {
            self.map.entry((plane, cpos)).or_insert_with(HashSet::new)
                .insert(id);
        }
    }

    fn destroy_structure(&mut self,
                         id: StructureId,
                         plane: PlaneId,
                         pos: V3,
                         template: TemplateId) {
        let bounds = Region::sized(data().template(template).size) + pos;
        let chunk_bounds = bounds.reduce().div_round_signed(CHUNK_SIZE);
        for cpos in chunk_bounds.points() {
            let mut e = match self.map.entry((plane, cpos)) {
                Entry::Occupied(e) => e,
                Entry::Vacant(_) => {
                    error!("destroy_structure: no structures are present at {:?}", cpos);
                    continue;
                },
            };

            e.get_mut().remove(&id);
            if e.get().len() == 0 {
                e.remove();
            }
        }
    }

    pub fn update(&mut self, w: &FullWorld, u: &Update) {
        for (id, f) in u.structures() {
            if f.contains(flags::S_CREATED) && f.contains(flags::S_DESTROYED) {
                // Do nothing
            } else if f.contains(flags::S_CREATED) {
                // Create, using the final post-update values
                let s = w.structure(id);
                self.create_structure(id, s.plane_id(), s.pos(), s.template());
            } else if f.contains(flags::S_DESTROYED) {
                // Destroy, using the original pre-update values
                // We load only immutable values from `s`, so it's okay to consult the `new`
                // version added by the destroy handler instead of the `old`.
                let s = u.new_structure(id);
                let template = u.old_structure_template(id);
                self.destroy_structure(id, s.plane, s.pos, template);
            } else {
                if f.contains(flags::S_TEMPLATE) {
                    let s = w.structure(id);
                    let old_template = u.old_structure_template(id);
                    self.destroy_structure(id, s.plane_id(), s.pos(), old_template);
                    self.create_structure(id, s.plane_id(), s.pos(), s.template());
                }
            }
        }
    }

    pub fn structures_in_chunk(&self, plane: PlaneId, cpos: V2) -> ChunkIter {
        ChunkIter {
            inner: self.map.get(&(plane, cpos)).map(|s| s.iter()),
        }
    }

    pub fn structures_at_point<'a, 'b, E: world::Ext>(&'a self,
                                                      world: &'b World<E>,
                                                      plane: PlaneId,
                                                      pos: V3) -> PointIter<'a, 'b, E> {
        let cpos = pos.reduce().div_floor(CHUNK_SIZE);
        PointIter {
            world: world,
            pos: pos,
            inner: self.map.get(&(plane, cpos)).map(|s| s.iter()),
        }
    }
}

pub struct ChunkIter<'a> {
    inner: Option<hash_set::Iter<'a, StructureId>>,
}

impl<'a> Iterator for ChunkIter<'a> {
    type Item = StructureId;

    fn next(&mut self) -> Option<StructureId> {
        if let Some(ref mut inner) = self.inner {
            inner.next().map(|&x| x)
        } else {
            None
        }
    }
}

pub struct PointIter<'a, 'b, E: world::Ext+'b> {
    world: &'b World<E>,
    pos: V3,
    inner: Option<hash_set::Iter<'a, StructureId>>,
}

impl<'a, 'b, E: world::Ext> Iterator for PointIter<'a, 'b, E> {
    type Item = StructureId;

    fn next(&mut self) -> Option<StructureId> {
        let inner = match self.inner {
            Some(ref mut x) => x,
            None => return None,
        };

        for &sid in inner {
            let s = match self.world.get_structure(sid) {
                Some(x) => x,
                None => {
                    error!("{:?} is present in cache but not in world", sid);
                    continue;
                },
            };

            let pos = s.pos();
            let size = data().template(s.template()).size;
            let bounds = Region::sized(size) + pos;
            if bounds.contains(self.pos) {
                return Some(sid);
            }
        }

        None
    }
}
