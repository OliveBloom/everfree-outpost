use common::types::*;
use std::collections::btree_set::{self, BTreeSet};
use std::u8;

use util;


/// Wrapper type for cache lookup keys.  The cache keys use Morton order for the x/y axes, and we
/// also use a spare bit here to track whether the cache entry refers to the 0,0,0 point of the
/// structure, or to some other point in it.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
struct PosKey(u32);

impl PosKey {
    fn new(pos: V3, origin: bool) -> PosKey {
        assert!(0 <= pos.x && pos.x <= u8::MAX as i32);
        assert!(0 <= pos.y && pos.y <= u8::MAX as i32);
        assert!(0 <= pos.z && pos.z <= u8::MAX as i32);
        let xy = util::morton8(pos.x as u8, pos.y as u8);
        PosKey(((xy as u32) << 16) | ((pos.z as u32) << 8) | (origin as u32))
    }

    fn next(self) -> PosKey {
        PosKey(self.0 + 1)
    }
}


const MASK: i32 = (1 << (CHUNK_BITS + LOCAL_BITS)) - 1;

pub struct Cache {
    map: BTreeSet<(PosKey, StructureId)>,
}

impl Cache {
    pub fn new() -> Cache {
        Cache {
            map: BTreeSet::new(),
        }
    }

    pub fn add(&mut self, id: StructureId, pos: V3, size: V3) {
        let bounds = Region3::sized(size);
        for offset in bounds.points() {
            let origin = offset == 0;
            let p = (pos + offset) & MASK;
            self.map.insert((PosKey::new(p, origin), id));
        }
    }

    pub fn remove(&mut self, id: StructureId, pos: V3, size: V3) {
        let bounds = Region3::sized(size);
        for offset in bounds.points() {
            let origin = offset == 0;
            let p = (pos + offset) & MASK;
            self.map.remove(&(PosKey::new(p, origin), id));
        }
    }

    pub fn at_pos(&self, pos: V3) -> PosIter {
        let pos = pos & MASK;
        let lo = (PosKey::new(pos, false), StructureId(0));
        let hi = (PosKey::new(pos, true).next(), StructureId(0));
        PosIter {
            iter: self.map.range(lo .. hi),
        }
    }

    pub fn origin_at_pos(&self, pos: V3) -> PosIter {
        let pos = pos & MASK;
        let lo = (PosKey::new(pos, true), StructureId(0));
        let hi = (PosKey::new(pos, true).next(), StructureId(0));
        PosIter {
            iter: self.map.range(lo .. hi),
        }
    }
}


pub struct PosIter<'a> {
    iter: btree_set::Range<'a, (PosKey, StructureId)>,
}

impl<'a> Iterator for PosIter<'a> {
    type Item = StructureId;

    fn next(&mut self) -> Option<StructureId> {
        if let Some(&(_, id)) = self.iter.next() {
            Some(id)
        } else {
            None
        }
    }
}
