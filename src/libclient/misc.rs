//use std::prelude::v1::*;

use hotbar::Hotbar;
use inv_changes::InvChanges;

/// Miscellaneous client state
pub struct Misc {
    pub hotbar: Hotbar,
    pub inv_changes: InvChanges,
    pub show_cursor: bool,
}

impl Misc {
    pub fn new() -> Misc {
        Misc {
            hotbar: Hotbar::new(),
            inv_changes: InvChanges::new(),
            show_cursor: false,
        }
    }

    pub fn reset(&mut self) {
        self.inv_changes.clear();
    }
}
