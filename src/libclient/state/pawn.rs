use std::prelude::v1::*;
use std::mem;
use common::types::*;
use common::Gauge;

use super::{Delta, Log};


#[derive(Clone)]
pub struct Pawn {
    id: Option<EntityId>,
    name: String,
    energy: Gauge,
    dialog: Dialog,
}

#[derive(Clone, Debug)]
pub enum Dialog {
    None,
    Inventory(InventoryId),
    Abilities(InventoryId),
    Equipment(InventoryId, InventoryId),
    Container(InventoryId, InventoryId),
    Crafting(StructureId, u32, InventoryId, InventoryId, InventoryId),
    PonyEdit,
}

impl Pawn {
    pub fn new() -> Pawn {
        Pawn {
            id: None,
            name: String::new(),
            energy: Gauge::new(0, (0, 1), 0, 0, 1),
            dialog: Dialog::None,
        }
    }


    pub fn id(&self) -> Option<EntityId> {
        self.id
    }

    pub fn set(&mut self, id: Option<EntityId>, name: String) -> (Option<EntityId>, String) {
        let old_id = mem::replace(&mut self.id, id);
        let old_name = mem::replace(&mut self.name, name);
        (old_id, old_name)
    }

    pub fn set_name(&mut self, name: String) -> String {
        mem::replace(&mut self.name, name)
    }

    pub fn is(&self, id: EntityId) -> bool {
        self.id == Some(id)
    }


    pub fn dialog(&self) -> &Dialog {
        &self.dialog
    }

    pub fn set_dialog<L>(&mut self, dialog: Dialog, log: &mut L)
            where L: Log<Delta> {
        let old_dialog = mem::replace(&mut self.dialog, dialog);
        log.record(Delta::SetDialog(old_dialog));
    }


    pub fn energy(&self) -> &Gauge {
        &self.energy
    }
}
