use std::prelude::v1::*;
use common::types::*;
use std::mem;
use common::Gauge;

use data::data;
use super::{Delta, Log};

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum CraftingStateKind {
    Idle,
    Paused,
    Active,
}

#[derive(Clone)]
pub struct CraftingState {
    pub sid: StructureId,
    pub recipe: RecipeId,
    pub count: u8,
    pub kind: CraftingStateKind,
    pub progress: Gauge,
}

impl CraftingState {
    pub fn new() -> CraftingState {
        CraftingState {
            sid: StructureId(0),
            recipe: NO_RECIPE,
            count: 0,
            kind: CraftingStateKind::Idle,
            progress: Gauge::new(0, (0, 1), 0, 0, 1),
        }
    }

    pub fn idle(sid: StructureId) -> CraftingState {
        CraftingState {
            sid: sid,
            recipe: NO_RECIPE,
            count: 0,
            kind: CraftingStateKind::Idle,
            progress: Gauge::new(0, (0, 1), 0, 0, 1),
        }
    }

    pub fn paused(sid: StructureId, recipe: RecipeId, count: u8, progress: u32) -> CraftingState {
        let max_progress = data().get_recipe(recipe).map_or(1000, |r| r.time as i32);
        CraftingState {
            sid: sid,
            recipe: recipe,
            count: count,
            kind: CraftingStateKind::Paused,
            progress: Gauge::new(progress as i32, (0, 1), 0, 0, max_progress),
        }
    }

    pub fn active(sid: StructureId, recipe: RecipeId, count: u8,
                  progress: u32, start_time: Time) -> CraftingState {
        let max_progress = data().get_recipe(recipe).map_or(1000, |r| r.time as i32);
        CraftingState {
            sid: sid,
            recipe: recipe,
            count: count,
            kind: CraftingStateKind::Active,
            progress: Gauge::new(progress as i32, (1000, 1), start_time, 0, max_progress),
        }
    }

    pub fn as_delta(&self) -> Delta {
        match self.kind {
            CraftingStateKind::Idle =>
                Delta::CraftingStateIdle(self.sid),
            CraftingStateKind::Paused =>
                Delta::CraftingStatePaused(
                    self.sid,
                    self.recipe,
                    self.count,
                    self.progress.last_value() as u32),
            CraftingStateKind::Active =>
                Delta::CraftingStateActive(
                    self.sid,
                    self.recipe,
                    self.count,
                    self.progress.last_value() as u32,
                    self.progress.last_time()),
        }
    }
}

#[derive(Clone)]
pub struct Misc {
    pub crafting_state: CraftingState,
}

impl Misc {
    pub fn new() -> Misc {
        Misc {
            crafting_state: CraftingState::new(),
        }
    }

    pub fn crafting_state_idle<L>(&mut self, sid: StructureId, log: &mut L)
            where L: Log<Delta> {
        let old = mem::replace(&mut self.crafting_state, CraftingState::idle(sid));
        log.record(old.as_delta());
    }

    pub fn crafting_state_paused<L>(&mut self,
                                    sid: StructureId,
                                    recipe: RecipeId,
                                    count: u8,
                                    progress: u32,
                                    log: &mut L)
            where L: Log<Delta> {
        let old = mem::replace(&mut self.crafting_state,
                               CraftingState::paused(sid, recipe, count, progress));
        log.record(old.as_delta());
    }

    pub fn crafting_state_active<L>(&mut self,
                                    sid: StructureId,
                                    recipe: RecipeId,
                                    count: u8,
                                    progress: u32,
                                    start_time: Time,
                                    log: &mut L)
            where L: Log<Delta> {
        let old = mem::replace(&mut self.crafting_state,
                               CraftingState::active(sid, recipe, count, progress, start_time));
        log.record(old.as_delta());
    }

    /// Get crafting state for station `sid`.  Returns `None` if the currently stored crafting
    /// state is for a different station.
    pub fn station_crafting_state(&self, sid: StructureId) -> Option<&CraftingState> {
        let cs = &self.crafting_state;
        if cs.sid == sid {
            Some(cs)
        } else {
            None
        }
    }
}


