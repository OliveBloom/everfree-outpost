use common::types::*;
use common::appearance::APP_LIGHT;

use data::data;
use state::entity::Entities;
use state::structure::Structures;
use util;

use graphics::{IntrusiveCorner, GeometryGenerator};
use graphics::emit_quad;
use graphics::types::HAS_LIGHT;



#[derive(Clone, Copy)]
#[repr(C)]
pub struct Vertex {
    // 0
    corner: (u8, u8),
    center: (u16, u16, u16),

    // 8
    color: (u8, u8, u8),
    _pad1: u8,
    radius: u16,
    _pad2: u16,

    // 16
}

impl IntrusiveCorner for Vertex {
    fn corner(&self) -> &(u8, u8) { &self.corner }
    fn corner_mut(&mut self) -> &mut (u8, u8) { &mut self.corner }
}


#[derive(Clone)]
pub struct StructureGeomGen<'a> {
    structures: &'a Structures,
    bounds: Region<V2>,
}

impl<'a> StructureGeomGen<'a> {
    pub fn new(structures: &'a Structures,
               bounds: Region<V2>) -> StructureGeomGen<'a> {
        StructureGeomGen {
            structures: structures,
            bounds: bounds * CHUNK_SIZE * TILE_SIZE,
        }
    }
}

impl<'a> GeometryGenerator for StructureGeomGen<'a> {
    type Vertex = Vertex;

    fn generate<F: FnMut(Vertex)>(&mut self, mut emit: F) {
        for (_, s) in self.structures.iter() {
            let data = data();
            let t = data.template(s.template_id);

            if !t.flags.contains(HAS_LIGHT) {
                continue;
            }

            // Be careful to avoid emitting duplicate geometry.  Two copies of a structure looks
            // the same as one, but two copies of a light is twice as bright.
            let offset = V3::new(t.light_pos.0 as i32,
                                 t.light_pos.1 as i32,
                                 t.light_pos.2 as i32);
            let center = s.pos * TILE_SIZE + offset;

            const MASK: i32 = (1 << (LOCAL_BITS + CHUNK_BITS + TILE_BITS)) - 1;
            if !util::contains_wrapped(self.bounds, center.reduce(), scalar(MASK)) {
                continue;
            }

            emit_quad(|v| emit(v), Vertex {
                corner: (0, 0),
                // Give the position of the front corner of the structure, since the quad should
                // cover the front plane.
                center: (center.x as u16,
                         center.y as u16,
                         center.z as u16),

                color: t.light_color,
                radius: t.light_radius,

                _pad1: 0,
                _pad2: 0,
            });
        }
    }
}


#[derive(Clone)]
pub struct EntityGeomGen<'a> {
    entities: &'a Entities,
    bounds: Region<V2>,
    now: Time,
}

impl<'a> EntityGeomGen<'a> {
    pub fn new(entities: &'a Entities,
               bounds: Region<V2>,
               now: Time) -> EntityGeomGen<'a> {
        EntityGeomGen {
            entities: entities,
            bounds: bounds * CHUNK_SIZE * TILE_SIZE,
            now: now,
        }
    }
}

impl<'a> GeometryGenerator for EntityGeomGen<'a> {
    type Vertex = Vertex;

    fn generate<F: FnMut(Vertex)>(&mut self, mut emit: F) {
        for (_, e) in self.entities.iter() {
            if !e.appearance.flags.contains(APP_LIGHT) {
                continue;
            }

            let pos = e.pos(self.now);
            // TODO: hard-coded constant based on entity size
            let center = pos + V3::new(16, 16, 48);
            let center = util::wrap_base(center, self.bounds.min.extend(0), scalar(MASK));
            if !self.bounds.contains(center.reduce()) {
                // Not visible
                continue;
            }

            const MASK: i32 = (1 << (LOCAL_BITS + CHUNK_BITS + TILE_BITS)) - 1;
            if !util::contains_wrapped(self.bounds, center.reduce(), scalar(MASK)) {
                continue;
            }

            emit_quad(|v| emit(v), Vertex {
                corner: (0, 0),
                // Give the position of the front corner of the entity, since the quad should
                // cover the front plane.
                center: (center.x as u16,
                         center.y as u16,
                         center.z as u16),

                color: (100, 180, 255),
                radius: 200,

                _pad1: 0,
                _pad2: 0,
            });
        }
    }
}
