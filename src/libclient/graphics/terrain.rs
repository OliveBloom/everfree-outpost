use common::types::*;

use data::{data, HAS_TERRAIN};
use graphics::{IntrusiveCorner, GeometryGenerator};
use graphics::emit_quad;
use state::terrain_chunk::TerrainChunks;
use util;

/// Vertex attributes for terrain.
#[allow(dead_code)]
#[derive(Clone, Copy)]
pub struct Vertex {
    corner: (u8, u8),
    pos: (u8, u8, u8),
    side: u8,
    tex_coord: (u8, u8),
}

impl IntrusiveCorner for Vertex {
    fn corner(&self) -> &(u8, u8) { &self.corner }
    fn corner_mut(&mut self) -> &mut (u8, u8) { &mut self.corner }
}


#[derive(Clone)]
pub struct GeomGen<'a> {
    chunks: &'a TerrainChunks,
    bounds: Region<V2>,
}

impl<'a> GeomGen<'a> {
    pub fn new(chunks: &'a TerrainChunks,
               bounds: Region<V2>) -> GeomGen<'a> {
        GeomGen {
            chunks: chunks,
            bounds: bounds,
        }
    }

    fn generate_tile<F: FnMut(u8, u8)>(&self,
                                       pos: V3,
                                       a: TerrainId,
                                       b: TerrainId,
                                       c: TerrainId,
                                       d: TerrainId,
                                       mut emit: F) {
        let data = data();

        const MASK: i32 = (1 << (CHUNK_BITS + LOCAL_BITS)) - 1;
        if a == b && b == c && c == d {
            if a == NO_TERRAIN {
                return;
            }

            let t = data.terrain(a);
            let use_alt = util::hash(&(pos & MASK)) % 2 == 0;
            let idx = if t.has_alt() && use_alt { 0 } else { 15 };
            emit(idx, t.index as u8);
            return;
        }

        let mk_mask = |id, bit: u8| (data.terrain(id).priority, id, 1 << bit);
        let mut masks = [mk_mask(a, 0),
                         mk_mask(b, 1),
                         mk_mask(c, 2),
                         mk_mask(d, 3)];
        sort_terrain_masks(&mut masks);

        // under_mask is the combined mask for the current layer and all layers above.
        let mut over_mask = 0xf;
        let mut mask_acc = 0x0;
        for i in 0 .. 4 {
            let (_, id, mask) = masks[i];
            mask_acc |= mask;

            if i + 1 < 4 && masks[i + 1].1 == id {
                continue;
            }

            if id != NO_TERRAIN {
                let image_index = data.terrain(id).index;
                emit(over_mask, image_index as u8);
            }

            over_mask &= !mask_acc;
            mask_acc = 0;
        }
    }
}

impl<'a> GeometryGenerator for GeomGen<'a> {
    type Vertex = Vertex;

    fn generate<F: FnMut(Vertex)>(&mut self, mut emit: F) {
        let data = data();
        let grid_bounds = self.bounds * CHUNK_SIZE;
        // Avoid leaving half-tile gaps at the edge of the viewport.
        let ext_bounds = grid_bounds.expand(scalar(1));
        for z in 0 .. 1 {
            for pos in ext_bounds.points() {
                let get_terrain = |dx, dy| {
                    let p = V3::new(pos.x + dx, pos.y + dy, z);
                    let block_id = self.chunks.block(p);
                    let block = data.block(block_id);

                    if !block.display_flags().contains(HAS_TERRAIN) {
                        NO_TERRAIN
                    } else {
                        block.terrain
                    }
                };

                self.generate_tile(pos.extend(z),
                                   get_terrain(0, 0),
                                   get_terrain(1, 0),
                                   get_terrain(1, 1),
                                   get_terrain(0, 1),
                                   |s, t| {
                    emit_quad(|v| emit(v), Vertex {
                        corner: (0, 0),
                        pos: (pos.x as u8,
                              pos.y as u8,
                              z as u8),
                        side: 3,
                        tex_coord: (s, t),
                    });
                });
            }
        }
    }
}

fn sort_terrain_masks(arr: &mut [(u8, TerrainId, u8); 4]) {
    // Sort ranges 0..2 and 2..4
    if arr[1].0 < arr[0].0 {
        arr.swap(0, 1);
    }
    if arr[3].0 < arr[2].0 {
        arr.swap(2, 3);
    }

    // Interleave the two sorted pairs
    if arr[0].0 < arr[2].0 {
        arr.swap(1, 2);
    } else {
        *arr = [arr[2], arr[0], arr[3], arr[1]];
    }

    // Finish sorting.  At most one of these tests can fail.
    if arr[2].0 < arr[1].0 {
        arr.swap(1, 2);
    } else if arr[3].0 < arr[2].0 {
        arr.swap(2, 3);
    }

    if arr[0].0 > arr[1].0 || arr[1].0 > arr[2].0 || arr[2].0 > arr[3].0 {
        panic!("mis-sorted array: {:?}", arr as &[_]);
    }
}
