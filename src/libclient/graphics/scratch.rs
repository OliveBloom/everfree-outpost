use common::types::*;
use std::collections::BTreeMap;
use std::cmp::Ordering;
use std::ops::{Add, Sub, AddAssign, SubAssign};
use common::util::Alloc2D;

use ui2::context::Channel;


struct Allocation {
    bounds: Region<V2>,
    epoch: Epoch,
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
pub enum Key {
    Pony {
        hash: u64,
    },

    ColorPlotXY {
        size: (u8, u8),
        // Can't store the actual color, because `f32`s are not `Ord`.
        color_hash: u64,
    },
    ColorPlotZ {
        size: (u8, u8),
        color_hash: u64,
    },

    ColorSlider {
        color: (u8, u8, u8),
        channel: Channel,
    },

    /// Contents of a color palette, one pixel per color.
    Palette {
        hash: u64,
    },

    SelectionCell,
    SelectionStructure {
        template: TemplateId,
        part_mask: u32,
        oneshot_start: u16,
        // TODO: remove `now` to allow caching across multiple frames
        now: Time,
    },
    SelectionEntity {
        /// Hash of entity's `appearance`, `activity`, and `activity_start`.
        hash: u64,
    },
}

pub enum AllocResult {
    /// Found an existing allocation.
    Existing(Region<V2>),
    /// Created a new allocation.
    NewAlloc(Region<V2>),
    /// Failed to obtain an allocation.
    NoSpace,
}

impl AllocResult {
    pub fn is_new(&self) -> bool {
        match *self {
            AllocResult::NewAlloc(_) => true,
            _ => false,
        }
    }

    pub fn region(&self) -> Option<Region<V2>> {
        match *self {
            AllocResult::Existing(r) |
            AllocResult::NewAlloc(r) => Some(r),
            AllocResult::NoSpace => None,
        }
    }
}

pub struct ScratchAlloc {
    alloc: Alloc2D,
    map: BTreeMap<Key, Allocation>,

    /// Frame counter, used to distinguish scratch regions that are currently in use from old ones
    /// that can be GCed.
    epoch: Epoch,
    /// Amount of area allocated for use in the current frame.
    cur_area: usize,
    /// Amount of area allocated in previous frames but not yet GCed.
    old_area: usize,
}

impl ScratchAlloc {
    pub fn new(size: V2) -> ScratchAlloc {
        ScratchAlloc {
            alloc: Alloc2D::new(size),
            map: BTreeMap::new(),

            epoch: Epoch(0),
            cur_area: 0,
            old_area: 0,
        }
    }

    fn try_alloc(&mut self, size: V2) -> Option<Region<V2>> {
        if let Some(bounds) = self.alloc.alloc(size) {
            return Some(bounds);
        }

        let alloc_area = Region2::sized(size).volume() as usize;
        let total_area = Region2::sized(self.alloc.size()).volume() as usize;
        // The old_area check avoids an unnecessary GC when we have enough available area in total,
        // but it can't accomodate the requested shape.
        if self.old_area > 0 && total_area - self.cur_area >= alloc_area {
            let limit = self.epoch;
            trace!("ran out of space - running a full GC");
            self.gc(limit);
        }

        self.alloc.alloc(size)
    }

    pub fn get(&mut self, key: Key, size: V2) -> AllocResult {
        if let Some(a) = self.map.get_mut(&key) {
            if a.epoch != self.epoch {
                a.epoch = self.epoch;
                let area = a.bounds.volume() as usize;
                self.old_area -= area;
                self.cur_area += area;
            }
            return AllocResult::Existing(a.bounds);
        }

        let bounds = unwrap_or!(self.try_alloc(size), return AllocResult::NoSpace);
        self.map.insert(key, Allocation {
            bounds: bounds,
            epoch: self.epoch,
        });
        self.cur_area += bounds.volume() as usize;
        AllocResult::NewAlloc(bounds)
    }

    /// Garbage-collect all allocations that are strictly older than `epoch_limit`.
    fn gc(&mut self, epoch_limit: Epoch) {
        assert!(epoch_limit <= self.epoch);

        let mut remove_keys = Vec::new();
        for (&k, a) in &self.map {
            // `a.epoch < epoch_limit`
            if a.epoch < epoch_limit {
                remove_keys.push(k);
            }
        }

        trace!("GC: freeing {} items", remove_keys.len());

        for k in remove_keys {
            let a = self.map.remove(&k).unwrap();
            self.old_area -= a.bounds.volume() as usize;
            self.alloc.free(a.bounds);
        }
    }

    pub fn end_frame(&mut self) {
        let limit = self.epoch - 60;
        self.gc(limit);
        self.epoch += 1;
        self.old_area += self.cur_area;
        self.cur_area = 0;
    }
}


/// Wrapping epoch counter, supporting "older"/"newer" comparisons.
#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
struct Epoch(pub usize);

impl Add<Epoch> for Epoch {
    type Output = Epoch;
    fn add(self, other: Epoch) -> Epoch {
        Epoch(self.0.wrapping_add(other.0))
    }
}

impl Add<usize> for Epoch {
    type Output = Epoch;
    fn add(self, other: usize) -> Epoch {
        Epoch(self.0.wrapping_add(other))
    }
}

impl<T> AddAssign<T> for Epoch where Epoch: Add<T, Output=Epoch> {
    fn add_assign(&mut self, other: T) {
        *self = *self + other;
    }
}

impl Sub<Epoch> for Epoch {
    type Output = Epoch;
    fn sub(self, other: Epoch) -> Epoch {
        Epoch(self.0.wrapping_sub(other.0))
    }
}

impl Sub<usize> for Epoch {
    type Output = Epoch;
    fn sub(self, other: usize) -> Epoch {
        Epoch(self.0.wrapping_sub(other))
    }
}

impl<T> SubAssign<T> for Epoch where Epoch: Sub<T, Output=Epoch> {
    fn sub_assign(&mut self, other: T) {
        *self = *self - other;
    }
}

/// Comparisons such as `a < b` indicate whether `a` is "older than" `b`, taking into account the
/// wrapping behavior of `Epoch`.  For example, `Epoch(usize::MAX) < Epoch(0)`.  This stops working
/// due to wraparound when one `Epoch` gets ahead of the other by more than `usize::MAX / 2`.
impl PartialOrd for Epoch {
    fn partial_cmp(&self, other: &Epoch) -> Option<Ordering> {
        // Typical `a < b` -> `a - b < 0` transformation, but with a cast to `isize` to use signed
        // comparison.
        //
        // Note this is consistent with the derived `PartialEq`/`Eq` impls, since `a - b == 0`
        // exactly when `a == b`.
        ((*self - *other).0 as isize).partial_cmp(&0)
    }
}

impl Ord for Epoch {
    fn cmp(&self, other: &Epoch) -> Ordering {
        ((*self - *other).0 as isize).cmp(&0)
    }
}


