use common::types::*;
use std::u16;

use cache::draw_mode;
use data::{data, StructureTemplateRef};
use state::structure::Structures;
use util;

use graphics::GeometryGenerator;


#[derive(Clone, Copy)]
#[repr(C)]
pub struct Vertex {
    // 0
    vert_offset: (i16, i16, i16),
    src_pos: (i16, i16),

    // 10
    struct_pos: (u8, u8, u8),
    struct_size: (u8, u8, u8),

    // 16
    anim_length: i8,
    anim_rate: u8,
    anim_oneshot_start: u16,
    anim_step: u16,

    // 22
}


#[derive(Clone)]
pub struct GeomGen<'a> {
    structures: &'a Structures,
    draw_mode_cache: &'a draw_mode::Cache,
    bounds: Region<V2>,
}

impl<'a> GeomGen<'a> {
    pub fn new(structures: &'a Structures,
               draw_mode_cache: &'a draw_mode::Cache,
               bounds: Region<V2>) -> GeomGen<'a> {
        GeomGen {
            structures: structures,
            draw_mode_cache: draw_mode_cache,
            bounds: bounds * CHUNK_SIZE,
        }
    }
}

impl<'a> GeometryGenerator for GeomGen<'a> {
    type Vertex = Vertex;

    fn generate<F: FnMut(Vertex)>(&mut self, mut emit: F) {
        const MASK: i32 = (1 << (CHUNK_BITS + LOCAL_BITS)) - 1;
        let data = data();

        let mut visible_structures = Vec::new();
        for (&id, s) in self.structures.iter() {
            let s_pos = util::wrap_base(s.pos, self.bounds.min.extend(0), scalar(MASK));
            if !self.bounds.contains(s_pos.reduce()) {
                // Not visible
                continue;
            }

            let t = data.template(s.template_id);
            let mask = self.draw_mode_cache.part_mask(id);
            visible_structures.push((s_pos, s, mask, t));
        }

        visible_structures.sort_by_key(|&(p, _s, _m, t)| {
            (p.y, p.z, t.layer, p.x)
        });

        let draw_bounds = self.bounds;
        let mut depth_buffer = util::make_array(u16::MAX, draw_bounds.volume() as usize);
        let render_cb = |i, d: &mut [u16], wb| {
            let (p, _s, _m, t) = visible_structures[i];
            render(p, t, d, draw_bounds, wb)
        };
        vis_sort(visible_structures.len(), &mut depth_buffer, u16::MAX, render_cb, |i| {
            let (_p, s, m, t) = visible_structures[i];
            gen_geom(s.pos, s.oneshot_start, t, m, |v| emit(v));
        });
    }
}


#[derive(Clone)]
pub struct SingleGeomGen {
    pos: V3,
    oneshot_start: Time,
    template_id: TemplateId,
    part_mask: u32,
}

impl SingleGeomGen {
    pub fn new(pos: V3,
               oneshot_start: Time,
               template_id: TemplateId,
               part_mask: u32) -> SingleGeomGen {
        SingleGeomGen {
            pos, oneshot_start, template_id, part_mask,
        }
    }
}

impl GeometryGenerator for SingleGeomGen {
    type Vertex = Vertex;

    fn generate<F: FnMut(Vertex)>(&mut self, mut emit: F) {
        let data = data();
        let t = data.template(self.template_id);
        gen_geom(self.pos, self.oneshot_start, t, self.part_mask, |v| emit(v));
    }
}


fn gen_geom<F: FnMut(Vertex)>(pos: V3,
                              oneshot_start: Time,
                              t: StructureTemplateRef,
                              mask: u32,
                              mut emit: F) {
    let data = data();
    let base = t.part_idx as usize;
    for part_idx in t.iter_swizzle() {
        if mask & (1 << part_idx) == 0 {
            continue;
        }
        let p = &data.template_parts()[base + part_idx];

        let j0 = p.vert_idx as usize;
        let j1 = j0 + p.vert_count as usize;
        for v in &data.template_verts()[j0 .. j1] {
            emit(Vertex {
                vert_offset: (v.x, v.y, v.z),
                src_pos: p.offset,
                struct_pos: util::pack_v3(pos),
                struct_size: t.size,
                anim_length: p.anim_length,
                anim_rate: p.anim_rate,
                anim_oneshot_start: oneshot_start as u16,
                anim_step: p.anim_step,
            });
        }
    }
}


fn fill<T: Copy>(buf: &mut [T], val: T) {
    for x in buf {
        *x = val;
    }
}

fn vis_sort<D, R, C>(num_objs: usize,
                     depth_buffer: &mut [D],
                     clear_depth: D,
                     mut render: R,
                     mut commit: C)
        where D: Copy,
              R: FnMut(usize, &mut [D], bool) -> bool,
              C: FnMut(usize) {
    // Sort into rendering order, using the Vis-Sort algorithm by Govindaraju et al.
    // http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.96.9228&rep=rep1&type=pdf
    // The general idea is to make several passes over the data, in each pass "committing" some
    // objects in sorted order.  (Normally each committed object would be pushed onto the
    // vector of sorted items, but here we directly emit the geometry at commit time instead.)
    let mut num_committed = 0;
    let mut pending = util::make_array(true, num_objs);
    let mut in_subseq = util::make_array(false, num_objs);

    while num_committed < num_objs {
        let old_num_committed = num_committed;

        // Phase 1: Find a sorted subsequence that includes the object(s) furthest from the
        // camera.  We do that by iterating in reverse and taking an object only if it is
        // behind all previous objects.  Since the furthest object is behind all other objects,
        // we will definitely include it in the subsequence.
        fill(depth_buffer, clear_depth);
        fill(&mut in_subseq, false);
        for i in (0 .. num_objs).rev() {
            if !pending[i] {
                // Object has already been committed.
                continue;
            }

            // Render with writeback.
            let occluded = render(i, depth_buffer, true);
            if !occluded {
                // This object is behind 
                in_subseq[i] = true;
            }
        }

        // Phase 2: Commit objects from the subsequence, as long as they are behind all
        // uncommitted objects.
        fill(depth_buffer, clear_depth);
        for i in 0 .. num_objs {
            // In this phase, we render with writeback only for uncommitted objects.
            if in_subseq[i] {
                let occluded = render(i, depth_buffer, false);
                if !occluded {
                    pending[i] = false;
                    commit(i);
                    num_committed += 1;
                }
            }
            if pending[i] {
                render(i, depth_buffer, true);
            }
        }

        if num_committed == old_num_committed {
            error!("depth sorting got stuck - committing an arbitrary object...");
            for i in 0 .. num_objs {
                if pending[i] {
                    pending[i] = false;
                    commit(i);
                    num_committed += 1;
                    break;
                }
            }
        }
    }
}

fn render(p: V3,
          t: StructureTemplateRef,
          depth_buffer: &mut [u16],
          draw_bounds: Region<V2>,
          writeback: bool) -> bool {
     // x,v position of the top-right corner on the screen.  Note the object extends both upward
     // and downward from the reference point (x, y-z).
     let xv_pos = V2::new(p.x, p.y - p.z - t.size().z);
     let xv_size = V2::new(t.size().x, t.size().y + t.size().z);
     let bounds = Region2::sized(xv_size);
     let mask = t.depth_mask();
     let mut occluded = false;

     for offset in bounds.points() {
         let depth = mask[bounds.index(offset)] as u16 + p.z as u16;
         if depth == 0 {
             continue;
         }

         let pos = xv_pos + offset;
         if !draw_bounds.contains(pos) {
             continue;
         }

         let idx = draw_bounds.index(pos);
         if depth < depth_buffer[idx] {
             if writeback {
                 depth_buffer[idx] = depth;
             }
         } else {
             occluded = true;
         }
     }

     occluded
}
