//! # The Y-axis
//!
//! OpenGL puts the origin for texture and framebuffer/window coordinates at the bottom left, not
//! the top left.  This is the opposite of our in-game coordinate system, where the Y axis extends
//! downward.  So that we don't have to fiddle with the Y coordinates all over the place, we simply
//! pretend that 0,0 is the top left (matching game coordinates, as well as the image coordinates
//! used for all atlas lookups) for all rendering.  This results in all our intermediate
//! framebuffers rendering "upside down", so during the final output to the screen, we flip the
//! image vertically.  WebGL already flips textures during loading (putting the top-left at 0,0).
//!
//! The upshot is: write all shaders and all viewport/scissor calls as if 0,0 is the top left, as
//! in normal game coordinates.

use common::types::*;
use std::mem;
use colorspace::LchUv;
use common::{Activity, Appearance};
use physics::floodfill;
use render::{Backend, Pass, Geometry, Texture};
use render::ast::TexFormat;

use cache::draw_mode;
use data::data;
use graphics::GeometryGenerator;
use graphics::scratch::{self, ScratchAlloc};
use state::entity::{Entity, Entities};
use state::structure::Structures;
use state::terrain_chunk::TerrainChunks;
use ui2::context::Channel;
use util;

use super::entity;
use super::light;
use super::structure;
use super::terrain;

mod pipeline;
pub mod ui;


pub const RAW_MODULUS: i32 = 55440;

// The `now` value passed to the animation shader must be reduced to fit in a
// float.  We use the magic number 55440 for this, since it's divisible by
// every number from 1 to 12 (and most "reasonable" numbers above that).  This
// is useful because repeating animations will glitch when `now` wraps around
// unless `length / framerate` divides evenly into the modulus.
//
// Note that the shader `now` and ANIM_MODULUS are both in seconds, not ms.
pub const ANIM_MODULUS: f64 = RAW_MODULUS as f64;

// We also need a smaller modulus for one-shot animation start times.  These
// are measured in milliseconds and must fit in a 16-bit int.  It's important
// that the one-shot modulus divides evenly into 1000 * ANIM_MODULUS, because
// the current frame time in milliseconds will be modded by 1000 * ANIM_MODULUS
// and then again by the one-shot modulus.
//
// We re-use ANIM_MODULUS as the one-shot modulus, since it obviously divides
// evenly into 1000 * ANIM_MODULUS.  This is okay as long as ANIM_MODULUS fits
// into 16 bits.
pub const ONESHOT_MODULUS: Time = ANIM_MODULUS as Time;


pub const SCRATCH_SIZE: (u16, u16) = (1024, 1024);

pub struct Renderer<B: Backend> {
    b: B,

    terrain_geom: B::Geometry,
    structure_geom: B::Geometry,
    structure_light_geom: B::Geometry,
    entity_geom: B::Geometry,
    entity_light_geom: B::Geometry,
    ui_geom: B::Geometry,
    square01_geom: B::Geometry,
    /// Geometry buffer compatible with `square01` (type `(u8, u8)`), but empty.
    square01_empty_geom: B::Geometry,

    /// `Some(region)` if `terrain_geom` contains up-to-date geometry for `region`.  `None` if
    /// `terrain_geom` has been explicitly invalidated.
    terrain_validity: Option<Region<V2>>,
    structure_validity: Option<Region<V2>>,
    /// `structure_light_geom` uses different bounds from normal `structure_geom`, so it needs a
    /// separate validity check.
    structure_light_validity: Option<Region<V2>>,

    /// Gives the source region (in the scratch buffer) and destination position (in the world
    /// buffer) where the selection outline should be drawn, if relevant.
    selection_info: Option<(Region<V2>, V2)>,

    // Sprite sheets
    terrain_atlas: B::Texture,
    structure_atlas: B::Texture,
    sprite_sheet: B::Texture,
    ui_items: B::Texture,
    ui_parts2: B::Texture,

    // Persistent textures
    cavern_map: B::Texture,
    scratch: B::Texture,

    // Output textures
    final_: B::Texture,
    screen: B::Texture,

    /// Region allocator for the scratch texture.
    scratch_alloc: ScratchAlloc,

    main_pass: B::Pass,
    output_pass: B::Pass,
    pass_special_pony: B::Pass,
    pass_special_color_plot_xy: B::Pass,
    pass_special_color_plot_z: B::Pass,
    pass_special_color_slider: B::Pass,
    pass_special_selection_cell: B::Pass,
    pass_special_selection_structure: B::Pass,
    pass_special_selection_entity: B::Pass,

    pub render_names: bool,
}

impl<B: Backend> Renderer<B> {
    pub fn new(mut b: B) -> Renderer<B> {
        use render::traits::*;

        let mut passes = b.compile(&pipeline::build_pipeline());

        let mut square01_geom = b.new_geometry::<(u8, u8)>();
        square01_geom.push::<(u8, u8)>(&(0, 0));
        square01_geom.push::<(u8, u8)>(&(0, 1));
        square01_geom.push::<(u8, u8)>(&(1, 1));
        square01_geom.push::<(u8, u8)>(&(0, 0));
        square01_geom.push::<(u8, u8)>(&(1, 1));
        square01_geom.push::<(u8, u8)>(&(1, 0));

        Renderer {
            terrain_geom: b.new_geometry::<terrain::Vertex>(),
            structure_geom: b.new_geometry::<structure::Vertex>(),
            structure_light_geom: b.new_geometry::<light::Vertex>(),
            entity_geom: b.new_geometry::<entity::Vertex>(),
            entity_light_geom: b.new_geometry::<light::Vertex>(),
            ui_geom: b.new_geometry::<ui::Vertex>(),
            square01_geom: square01_geom,
            square01_empty_geom: b.new_geometry::<(u8, u8)>(),

            terrain_validity: None,
            structure_validity: None,
            structure_light_validity: None,

            selection_info: None,

            terrain_atlas: b.texture_from_image("terrain"),
            structure_atlas: b.texture_from_image("structures0"),
            sprite_sheet: b.texture_from_image("sprites0"),
            ui_items: b.texture_from_image("items_img"),
            ui_parts2: b.texture_from_image("ui"),

            cavern_map: b.new_texture(TexFormat::Red, (96, 96)),
            scratch: b.new_texture(TexFormat::RGBA, SCRATCH_SIZE),

            final_: b.new_texture(TexFormat::RGBA, (4, 4)),
            screen: b.output_texture(),

            scratch_alloc: ScratchAlloc::new(V2::new(64, 64)),

            main_pass: passes.remove("main").unwrap(),
            output_pass: passes.remove("output").unwrap(),
            
            pass_special_pony: passes.remove("special_pony").unwrap(),
            pass_special_color_plot_xy: passes.remove("special_color_plot_xy").unwrap(),
            pass_special_color_plot_z: passes.remove("special_color_plot_z").unwrap(),
            pass_special_color_slider: passes.remove("special_color_slider").unwrap(),
            pass_special_selection_cell: passes.remove("special_selection_cell").unwrap(),
            pass_special_selection_structure:
                passes.remove("special_selection_structure").unwrap(),
            pass_special_selection_entity:
                passes.remove("special_selection_entity").unwrap(),

            render_names: true,

            b,
        }
    }


    pub fn update_terrain_geometry(&mut self,
                                   chunks: &TerrainChunks,
                                   bounds: Region<V2>) {
        if self.terrain_validity != Some(bounds) {
            self.terrain_geom = self.b.new_geometry::<terrain::Vertex>();
            terrain::GeomGen::new(chunks, bounds)
                .generate(|v| self.terrain_geom.push(&v));
            self.terrain_validity = Some(bounds);
        }
    }

    pub fn invalidate_terrain_geometry(&mut self) {
        self.terrain_validity = None;
    }


    pub fn update_structure_geometry(&mut self,
                                     structures: &Structures,
                                     draw_mode_cache: &draw_mode::Cache,
                                     bounds: Region<V2>) {
        if self.structure_validity != Some(bounds) {
            self.structure_geom = self.b.new_geometry::<structure::Vertex>();
            structure::GeomGen::new(structures, draw_mode_cache, bounds)
                .generate(|v| self.structure_geom.push(&v));
            self.structure_validity = Some(bounds);
        }
    }

    pub fn invalidate_structure_geometry(&mut self) {
        self.structure_validity = None;
    }

    pub fn update_structure_light_geometry(&mut self,
                                           structures: &Structures,
                                           bounds: Region<V2>) {
        if self.structure_light_validity != Some(bounds) {
            self.structure_light_geom = self.b.new_geometry::<light::Vertex>();
            light::StructureGeomGen::new(structures, bounds)
                .generate(|v| self.structure_light_geom.push(&v));
            self.structure_light_validity = Some(bounds);
        }
    }

    pub fn invalidate_structure_light_geometry(&mut self) {
        self.structure_light_validity = None;
    }


    pub fn update_entity_geometry(&mut self,
                                  entities: &Entities,
                                  bounds: Region<V2>,
                                  now: Time) {
        self.entity_geom = self.b.new_geometry::<entity::Vertex>();
        entity::GeomGen::new(entities, self.render_names, bounds, now)
            .generate(|v| self.entity_geom.push(&v));

        self.entity_light_geom = self.b.new_geometry::<light::Vertex>();
        light::EntityGeomGen::new(entities, bounds, now)
            .generate(|v| self.entity_light_geom.push(&v));
    }


    pub fn load_cavern_map(&mut self, data: &[floodfill::flags::Flags]) {
        assert!(mem::size_of::<floodfill::flags::Flags>() == mem::size_of::<u8>());
        let raw_data: &[u8] = unsafe { util::transmute_slice(data) };
        self.cavern_map.load_data(raw_data);
    }


    pub fn update_framebuffers(&mut self, scene: &Scene) {
        self.final_.resize((scene.camera_size.x as u16,
                            scene.camera_size.y as u16));
        self.screen.resize((scene.canvas_size.x as u16,
                            scene.canvas_size.y as u16));
        // FIXME this workaround for some ScratchAlloc bug destroys all caching
        self.scratch_alloc = ScratchAlloc::new(V2::new(64, 64));
    }

    /// Render the world into the final offscreen framebuffer.
    pub fn render(&mut self, scene: &Scene) {
        let pass = &mut self.main_pass;

        pass.set_fv("camera_pos", &scene.f_camera_pos);
        pass.set_fv("slice_center", &scene.f_slice_center);
        pass.set_f("slice_z", scene.f_slice_z);
        pass.set_f("anim_now", scene.anim_now());
        pass.set_u("clip_enabled", 0);
        pass.set_fv("ambient_light", &scene.ambient_light_f());

        pass.set_texture("terrain_atlas", &self.terrain_atlas);
        pass.set_texture("structure_atlas", &self.structure_atlas);
        pass.set_texture("entity_atlas", &self.sprite_sheet);
        pass.set_texture("item_atlas", &self.ui_items);
        pass.set_texture("ui_atlas", &self.ui_parts2);
        pass.set_texture("cavern_map", &self.cavern_map);
        pass.set_texture("scratch", &self.scratch);

        pass.set_geometry("square01_geom", &self.square01_geom);
        pass.set_geometry("terrain_geom", &self.terrain_geom);
        pass.set_geometry("structure_geom", &self.structure_geom);
        pass.set_geometry("entity_geom", &self.entity_geom);
        pass.set_geometry("structure_light_geom", &self.structure_light_geom);
        pass.set_geometry("entity_light_geom", &self.entity_light_geom);
        pass.set_geometry("ui_geom", &self.ui_geom);

        if let Some((src_region, dest_pos)) = self.selection_info {
            pass.set_iv("selection_src_region", &[src_region.min.x, src_region.min.y,
                                                  src_region.size().x, src_region.size().y]);
            pass.set_iv("selection_dest_pos", &[dest_pos.x, dest_pos.y]);
            pass.set_geometry("selection_geom", &self.square01_geom);
        } else {
            pass.set_iv("selection_src_region", &[0, 0, 0, 0]);
            pass.set_iv("selection_dest_pos", &[0, 0]);
            pass.set_geometry("selection_geom", &self.square01_empty_geom);
        }

        pass.set_texture("final", &self.final_);

        pass.run();
    }

    /// Display the offscreen framebuffer on the screen.  This may block if vsync is active.
    pub fn display(&mut self, _scene: &Scene) {
        let pass = &mut self.output_pass;
        pass.set_texture("final", &self.final_);
        pass.set_geometry("geom", &self.square01_geom);
        pass.set_texture("screen", &self.screen);
        pass.run();

        self.scratch_alloc.end_frame();
    }

    pub fn prepare_selection(&mut self,
                             scene: &Scene,
                             structures: &Structures,
                             draw_mode_cache: &draw_mode::Cache,
                             entities: &Entities) {
        self.selection_info = match scene.selection {
            Selection::None => None,
            Selection::Cell(pos) => {
                let display_pos = calc_display_pos(pos * TILE_SIZE, scene.camera_pos);
                self.get_special_selection_cell()
                    .map(|(region, ref_pos)| (region, display_pos - ref_pos))
            },
            Selection::Structure(sid) => {
                let s = &structures[sid];
                let mask = draw_mode_cache.part_mask(sid);
                let display_pos = calc_display_pos(s.pos * TILE_SIZE, scene.camera_pos);
                self.get_special_selection_structure(s.oneshot_start,
                                                     s.template_id,
                                                     mask,
                                                     scene.now,
                                                     scene.anim_now())
                    .map(|(region, ref_pos)| (region, display_pos - ref_pos))
            },
            Selection::Entity(eid) => {
                let e = &entities[eid];
                let display_pos = calc_display_pos(e.pos(scene.now) * TILE_SIZE,
                                                   scene.camera_pos);
                self.get_special_selection_entity(e,
                                                  scene.now,
                                                  scene.anim_now())
                    .map(|(region, ref_pos)| (region, display_pos - ref_pos))
            },
        };
    }


    pub fn get_special_pony(&mut self,
                            appearance: Appearance) -> Option<Region<V2>> {
        let key = scratch::Key::Pony {
            hash: util::hash(&appearance),
        };
        let result = self.scratch_alloc.get(key, 6.into());

        if result.is_new() {
            let region = result.region().unwrap() * 16;
            info!("drawing pony {:?} ({:x}) at {:?}", appearance, util::hash(&appearance), region);
            let dest = (region.min.x as u16,
                        region.min.y as u16);

            // TODO: hard-coded offsets
            let pos = V3::new(dest.0 as i32 + 32,
                              dest.1 as i32 + 80,
                              0);

            let entity = Entity::dummy(
                Activity::Stand { pos: 0.into(), dir: 4 },
                0,
                appearance,
                String::new());

            let mut geom = self.b.new_geometry::<entity::Vertex>();
            entity::render_entity_layers(&entity, pos, |v| geom.push(&v));

            let pass = &mut self.pass_special_pony;
            pass.set_iv("out_region", &[region.min.x, region.min.y,
                                        region.size().x, region.size().y]);
            pass.set_texture("entity_atlas", &self.sprite_sheet);
            pass.set_texture("ui_atlas", &self.ui_parts2);
            pass.set_geometry("entity_geom", &geom);
            pass.set_texture("scratch", &self.scratch);
            pass.run();
        }

        result.region().map(|r| r * 16)
    }

    pub fn get_special_color_plot_xy(&mut self,
                                     size: V2,
                                     color: LchUv) -> Option<Region<V2>> {
        let key = scratch::Key::ColorPlotXY {
            size: (size.x as u8,
                   size.y as u8),
            color_hash: util::hash(&color),
        };
        let alloc_size = (size + 16 - 1) / 16;
        let result = self.scratch_alloc.get(key, alloc_size);

        let region = result.region().map(|r| Region::sized(size) + r.min * 16);

        if result.is_new() {
            let region = region.unwrap();

            // Offset by region.min because `target` needs to be in absolute window coordinates,
            // not viewport-relative coordinates.
            let tx = util::roundf(color.h * (region.size().x - 1) as f32) + region.min.x as f32;
            let ty = util::roundf(color.c * (region.size().y - 1) as f32) + region.min.y as f32;

            let pass = &mut self.pass_special_color_plot_xy;
            pass.set_iv("out_region", &[region.min.x, region.min.y,
                                        region.size().x, region.size().y]);
            pass.set_fv("target", &[tx, ty]);
            pass.set_fv("cur_color", &[color.h, color.c, color.l]);
            pass.set_geometry("square01_geom", &self.square01_geom);
            pass.set_texture("scratch", &self.scratch);
            pass.run();
        }

        region
    }

    pub fn get_special_color_plot_z(&mut self,
                                    size: V2,
                                    color: LchUv) -> Option<Region<V2>> {
        let key = scratch::Key::ColorPlotZ {
            size: (size.x as u8,
                   size.y as u8),
            color_hash: util::hash(&color),
        };
        let alloc_size = (size + 16 - 1) / 16;
        let result = self.scratch_alloc.get(key, alloc_size);

        let region = result.region().map(|r| Region::sized(size) + r.min * 16);

        if result.is_new() {
            let region = region.unwrap();

            // Offset by region.min because `target` needs to be in absolute window coordinates,
            // not viewport-relative coordinates.
            let tz = util::roundf(color.l * (region.size().y - 1) as f32) + region.min.y as f32;

            let cx = ((region.min.x + region.max.x) / 2) as f32;

            let pass = &mut self.pass_special_color_plot_z;
            pass.set_iv("out_region", &[region.min.x, region.min.y,
                                        region.size().x, region.size().y]);
            pass.set_fv("target", &[cx, tz]);
            pass.set_fv("cur_color", &[color.h, color.c, color.l]);
            pass.set_geometry("square01_geom", &self.square01_geom);
            pass.set_texture("scratch", &self.scratch);
            pass.run();
        }

        region
    }

    pub fn get_special_color_slider(&mut self,
                                    size: V2,
                                    color: (u8, u8, u8),
                                    channel: Channel) -> Option<Region<V2>> {
        let key = scratch::Key::ColorSlider {
            color: color,
            channel: channel,
        };
        let alloc_size = (size + 16 - 1) / 16;
        let result = self.scratch_alloc.get(key, alloc_size);

        let region = result.region().map(|r| Region::sized(size) + r.min * 16);

        if result.is_new() {
            let region = region.unwrap();

            let tx = channel.get(color) as i32 * (region.size().x - 1) / 255 + region.min.x;

            let pass = &mut self.pass_special_color_slider;
            pass.set_iv("out_region", &[region.min.x, region.min.y,
                                        region.size().x, region.size().y]);
            pass.set_i("target", tx);
            pass.set_u("channel", channel as u32);
            pass.set_fv("cur_color", &[color.0 as f32 / 255.,
                                       color.1 as f32 / 255.,
                                       color.2 as f32 / 255.]);
            pass.set_geometry("square01_geom", &self.square01_geom);
            pass.set_texture("scratch", &self.scratch);
            pass.run();
        }

        region
    }

    pub fn get_special_palette_color(&mut self,
                                     palette: &[(u8, u8, u8)],
                                     index: usize) -> Option<Region<V2>> {
        let key = scratch::Key::Palette {
            hash: util::hash(palette),
        };
        let result = self.scratch_alloc.get(key, 1.into());
        let region = result.region().map(|r| r * 16);

        if result.is_new() {
            let region = region.unwrap();

            let mut data = unsafe { util::zeroed_boxed_slice::<u8>(16 * 16 * 4) };
            for (i, &(r, g, b)) in palette.iter().enumerate() {
                data[i * 4 + 0] = r;
                data[i * 4 + 1] = g;
                data[i * 4 + 2] = b;
                data[i * 4 + 3] = 255;
            }

            self.scratch.load_subdata((region.min.x as u16,
                                       region.min.y as u16),
                                      (region.size().x as u16,
                                       region.size().y as u16),
                                      &data);
        }

        // Get the 1x1 region over the selected color.
        let offset = V2::new(index as i32 % 16, index as i32 / 16);
        region.map(|r| Region::sized(1) + r.min + offset)
    }

    /// Draw a selection cell in the scratch buffer, or find an existing copy.  Returns both the
    /// region itself and a reference point within that region.  When using the result, the
    /// reference point should be aligned with the upper left of the selected cell.
    pub fn get_special_selection_cell(&mut self) -> Option<(Region<V2>, V2)> {
        let key = scratch::Key::SelectionCell;
        let size: V2 = (TILE_SIZE + 2).into();
        let alloc_size = (size + 16 - 1) / 16;
        let result = self.scratch_alloc.get(key, alloc_size);
        let region = result.region().map(|r| Region::sized(size) + r.min * 16);

        if result.is_new() {
            let region = region.unwrap();

            let pass = &mut self.pass_special_selection_cell;
            pass.set_iv("out_region", &[region.min.x, region.min.y,
                                        region.size().x, region.size().y]);
            pass.set_geometry("square01_geom", &self.square01_geom);
            pass.set_texture("scratch", &self.scratch);
            pass.run();
        }

        // Reference point is the upper-left of the cell, at 1,1, not the upper left of the
        // outline.
        region.map(|r| (r, 1.into()))
    }

    pub fn get_special_selection_structure(&mut self,
                                           oneshot_start: Time,
                                           template_id: TemplateId,
                                           part_mask: u32,
                                           raw_now: Time,
                                           now: f32) -> Option<(Region<V2>, V2)> {
        let key = scratch::Key::SelectionStructure {
            template: template_id,
            part_mask,
            oneshot_start: oneshot_start as u16,
            now: raw_now,
        };

        let data = data();
        let t = data.template(template_id);
        let tile_size = V2::new(2 + t.size().x, 2 + t.size().y + t.size().z);
        let tile_pos = V2::new(1, 1 + t.size().z);
        let size = tile_size * TILE_SIZE;

        let alloc_size = (size + 16 - 1) / 16;
        let result = self.scratch_alloc.get(key, alloc_size);
        let region = result.region().map(|r| Region::sized(size) + r.min * 16);

        if result.is_new() {
            let region = region.unwrap();

            let mut geom = self.b.new_geometry::<structure::Vertex>();
            structure::SingleGeomGen::new(
                    tile_pos.extend(0), oneshot_start, template_id, part_mask)
                .generate(|v| geom.push(&v));

            let pass = &mut self.pass_special_selection_structure;
            pass.set_iv("out_region", &[region.min.x, region.min.y,
                                        region.size().x, region.size().y]);
            pass.set_f("anim_now", now);
            pass.set_texture("atlas", &self.structure_atlas);
            pass.set_geometry("square01_geom", &self.square01_geom);
            pass.set_geometry("structure_geom", &geom);
            pass.set_texture("scratch", &self.scratch);
            pass.run();
        }

        region.map(|r| (r, tile_pos * TILE_SIZE))
    }

    pub fn get_special_selection_entity(&mut self,
                                        e: &Entity,
                                        raw_now: Time,
                                        now: f32) -> Option<(Region<V2>, V2)> {
        let key = scratch::Key::SelectionEntity {
            // TODO: remove raw_now to enable caching between frames
            hash: util::hash(&(&e.appearance, &e.activity, e.activity_start, raw_now)),
        };

        let size = V2::new(2, 3) * TILE_SIZE;
        let pos = V3::new(8, 24, 0);

        let alloc_size = (size + 16 - 1) / 16;
        let result = self.scratch_alloc.get(key, alloc_size);
        let region = result.region().map(|r| Region::sized(size) + r.min * 16);

        if result.is_new() {
            let region = region.unwrap();

            let mut geom = self.b.new_geometry::<entity::Vertex>();
            entity::SingleGeomGen::new(e, pos)
                .generate(|v| geom.push(&v));

            let pass = &mut self.pass_special_selection_entity;
            pass.set_iv("out_region", &[region.min.x, region.min.y,
                                        region.size().x, region.size().y]);
            pass.set_f("anim_now", now);
            pass.set_texture("entity_atlas", &self.sprite_sheet);
            pass.set_texture("ui_atlas", &self.ui_parts2);
            pass.set_geometry("square01_geom", &self.square01_geom);
            pass.set_geometry("entity_geom", &geom);
            pass.set_texture("scratch", &self.scratch);
            pass.run();
        }

        region.map(|r| (r, V2::new(pos.x, pos.y - pos.z)))
    }
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Selection {
    None,
    Cell(V3),
    Structure(StructureId),
    Entity(EntityId),
}


pub struct Scene {
    pub canvas_size: V2,
    pub camera_pos: V2,
    pub camera_size: V2,
    pub slice_center: V3,
    pub now: Time,
    pub ambient_light: (u8, u8, u8, u8),
    pub mouse_near_pawn: bool,
    pub selection: Selection,
    pub ui_scale: u16,

    pub f_camera_pos: [f32; 2],
    pub f_slice_center: [f32; 2],
    pub f_slice_z: f32,
}

impl Scene {
    pub fn calc_camera_bounds(center: V3, view_size: (u16, u16)) -> Region<V2> {
        let camera_center = V2::new(center.x, center.y - center.z);
        let camera_size = V2::new(view_size.0 as i32, view_size.1 as i32);
        let camera_pos = camera_center - camera_size / 2;
        Region::sized(camera_size) + camera_pos
    }

    pub fn new(now: Time,
               window_size: (u16, u16),
               view_size: (u16, u16),
               ui_scale: u16,
               center: V3,
               ambient_light: (u8, u8, u8, u8),
               mouse_near_pawn: bool,
               selection: Selection) -> Scene {
        let camera_bounds = Scene::calc_camera_bounds(center, view_size);
        let camera_pos = camera_bounds.min;
        let camera_size = camera_bounds.size();

        let canvas_size = V2::new(window_size.0 as i32, window_size.1 as i32);

        let slice_center = center.div_floor(TILE_SIZE);

        Scene {
            canvas_size,
            camera_pos,
            camera_size,
            slice_center,
            now,
            ambient_light,
            mouse_near_pawn,
            selection,
            ui_scale,

            f_camera_pos: [camera_pos.x as f32,
                           camera_pos.y as f32],
            f_slice_center: [slice_center.x as f32,
                             slice_center.y as f32],
            f_slice_z: slice_center.z as f32,
        }
    }

    fn ambient_light_f(&self) -> [f32; 4] {
        let (r,g,b,a) = self.ambient_light;
        [r as f32 / 255.0,
         g as f32 / 255.0,
         b as f32 / 255.0,
         a as f32 / 255.0]
    }

    fn anim_now(&self) -> f32 {
        let mut anim_now = self.now as f64 / 1000.0 % ANIM_MODULUS;
        if anim_now < 0.0 {
            anim_now += ANIM_MODULUS;
        }
        anim_now as f32
    }
}

/// Convert a world position (in pixels) to a position on the screen.  Since the coordinates wrap
/// around, this function will try to find a copy of the point that falls within the camera bounds,
/// if one exists.
fn calc_display_pos(pos: V3, camera_pos: V2) -> V2 {
    const STEP: i32 = TILE_SIZE * CHUNK_SIZE * LOCAL_SIZE;
    const MARGIN: i32 = TILE_SIZE * CHUNK_SIZE;
    let pos = V2::new(pos.x, pos.y - pos.z) - camera_pos;
    // Adjust `pos` to fall in the range `-MARGIN .. STEP - MARGIN`.
    (pos + MARGIN).mod_floor(STEP) - MARGIN
}
