use common::types::*;
use std::cmp::{self, Ordering};
use std::collections::{BinaryHeap, VecDeque};
use common::Activity;
use common::movement::{self, ShapeSource};
use common::proto::game::Request;

use cache::terrain_shape;
use debug::Debug;
use state::{State, Delta};


/// # Request lifecycle
///
/// There are three states a request can be in.
///
///  1. In flight.  The request was sent (at `send_time`), but we haven't received anything back
///     from the server yet.
///  2. Scheduled.  We received an acknowledgement from the server, which indicated that the
///     request will take effect at `apply_time`.
///  3. Retired.  All effects of the request are reflected in the game state, and we no longer need
///     to track it.
pub struct Predictor {
    in_flight: VecDeque<InFlightReq>,
    scheduled: BinaryHeap<ScheduledReq>,
    next_scheduled_uid: u32,

    movement: MovementState,
}


#[derive(Clone, Debug)]
struct InFlightReq {
    req: Request,
    send_time: Time,
}

#[derive(Clone, Debug)]
struct ScheduledReq {
    req: Request,
    send_time: Time,
    apply_time: Time,
    uid: u32,
}

fn wrapping_cmp(x: u32, y: u32) -> Ordering {
    (y.wrapping_sub(x) as i32).cmp(&0)
}

impl PartialEq for ScheduledReq {
    fn eq(&self, other: &ScheduledReq) -> bool {
        self.apply_time == other.apply_time &&
            self.uid == other.uid
    }
}

impl Eq for ScheduledReq {}

impl Ord for ScheduledReq {
    fn cmp(&self, other: &ScheduledReq) -> Ordering {
        other.apply_time.cmp(&self.apply_time)
            .then(wrapping_cmp(self.uid, other.uid))
    }
}

impl PartialOrd for ScheduledReq {
    fn partial_cmp(&self, other: &ScheduledReq) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}



impl Predictor {
    pub fn new() -> Predictor {
        Predictor {
            in_flight: VecDeque::new(),
            scheduled: BinaryHeap::new(),
            next_scheduled_uid: 0,

            movement: MovementState::new(),
        }
    }

    pub fn request_sent(&mut self, now: Time, req: &Request) {
        let req = match req {
            &Request::InputStart(..) |
            &Request::InputChange(..) => req.clone(),
            _ => return,
        };

        trace!("push in_flight: {:?}, sent {}", req, now);

        if self.in_flight.back().map_or(false, |ifr| now < ifr.send_time) {
            error!("TIME IS RUNNING BACKWARD! {} < {} (client side)",
                   now, self.in_flight.back().unwrap().send_time);
        }
        self.in_flight.push_back(InFlightReq {
            req: req,
            send_time: now,
        });
    }

    pub fn ack_request(&mut self, apply_time: Time) {
        let ifr = unwrap_or_error!(self.in_flight.pop_front(),
                                   "desync: got an ack with no pending reqs");
        trace!("schedule req: {:?}, sent {}, applied {}", ifr.req, ifr.send_time, apply_time);
        self.scheduled.push(ScheduledReq {
            req: ifr.req,
            send_time: ifr.send_time,
            apply_time: apply_time,
            uid: self.next_scheduled_uid,
        });
        self.next_scheduled_uid += 1;
    }

    pub fn nak_request(&mut self) {
        // NAK means it will never have any effect on game state
        let ifr = unwrap_or_error!(self.in_flight.pop_back(),
                                   "desync: got an nak with no pending reqs");
        trace!("nak: {:?}, sent {}", ifr.req, ifr.send_time);
    }

    pub fn advance(&mut self, now: Time) {
        // TODO: figure out whether < or <= is the right operator here
        while self.scheduled.peek().map_or(false, |sr| sr.apply_time < now) {
            let sr = self.scheduled.pop().unwrap();
            trace!("retire: {:?}, sent {}, applied {}, now {}",
                   sr.req, sr.send_time, sr.apply_time, now);

            match sr.req {
                Request::InputStart(_, dir, speed) => {
                    self.movement.input_start(sr.send_time, sr.apply_time, dir, speed);
                },
                Request::InputChange(rel_time, dir, speed) => {
                    self.movement.input_change(rel_time, dir, speed);
                },
                req => panic!("unexpected event type in advance: {:?}", req),
            }
        }
    }

    pub fn predict(&mut self,
                   platform_now: Time,
                   s: &mut State,
                   shape: &terrain_shape::Cache) {
        let now = s.now();
        trace!(" == start predict @ {} ({}) ==", now, platform_now);

        let mut sim = Sim::new(now, s, shape, &self.movement);

        let mut last_apply = now;

        for sr in self.scheduled.clone().into_sorted_vec().into_iter().rev() {
            trace!("  scheduled: {:?}, sent {}, apply at {}", sr.req, sr.send_time, sr.apply_time);
            last_apply = cmp::max(last_apply, sr.apply_time);
            sim.handle_scheduled(&sr);
        }

        let delta = if let Some(ifr) = self.in_flight.front() {
            last_apply - ifr.send_time
        } else {
            // Doesn't matter, since there are no in-flight requests to use it.
            0
        };

        // TODO: what happens if we do this:
        //  - 0: start
        //  - 1000: change(1010)
        //  - 1005: start
        // can this even happen?
        for ifr in &self.in_flight {
            trace!("  in_flight: {:?}, sent {}", ifr.req, ifr.send_time);
            sim.handle_in_flight(ifr, delta);
        }

        sim.finish(platform_now);
    }

    pub fn update_debug(&self, _now: Time, d: &mut Debug) {
        d.predict_pending = self.in_flight.len();
        d.predict_acknowledged = self.scheduled.len();
        //d.predict_next_pending_delta = self.pending_reqs.front().map_or(0, |&(t, _)| now - t);
        //d.predict_last_ack_delay = self.last_ack_delay;
    }
}


struct Sim<'a> {
    now: Time,
    state: &'a mut State,
    shape: &'a terrain_shape::Cache,
    movement: MovementState,
    activity: Activity,
    /// Stores the predicted "official" start time of the activity, meaning the start time in the
    /// server's canonical timeline.  The start time actually stored in client state will be offset
    /// from this by the final value of `movement.backdate_by`.
    activity_start: Time,
    predicted_activity_change: bool,
}

impl<'a> Sim<'a> {
    pub fn new(now: Time,
               state: &'a mut State,
               shape: &'a terrain_shape::Cache,
               movement: &MovementState) -> Sim<'a> {
        let (activity, activity_start) = {
            let pawn = state.pawn().id().and_then(|id| state.entities().get(id));
            if let Some(e) = pawn {
                (e.activity.clone(), e.activity_start)
            } else {
                (Activity::default(), now)
            }
        };
        trace!("  sim: initial activity {:?}, starting {}", activity, activity_start);

        let movement = movement.clone();

        Sim {
            now, state, shape, movement, activity, activity_start,
            predicted_activity_change: false,
        }
    }

    pub fn handle_scheduled(&mut self, sr: &ScheduledReq) {
        self.handle_one(sr.send_time, sr.apply_time, &sr.req);
    }

    pub fn handle_in_flight(&mut self, ifr: &InFlightReq, delta: Time) {
        let apply_time = match ifr.req {
            // InputStart apply time doesn't really matter, because InputStart only gets sent when
            // the pawn is stationary.
            Request::InputStart(..) => ifr.send_time + delta,

            // InputChange send_time is ignored for computing apply_time.  The apply time is based
            // only on `rel_time` and `time_base` (unless that would put application time in the
            // past).
            Request::InputChange(rel_time, _ ,_) =>
                cmp::max(self.now, self.movement.time_base + rel_time as Time),

            _ => unreachable!("unexpected request type"),
        };

        self.handle_one(ifr.send_time, apply_time, &ifr.req);
    }

    pub fn handle_one(&mut self,
                      send_time: Time,
                      apply_time: Time,
                      req: &Request) {
        trace!("  sim: fast-forwarding to {}", apply_time);
        self.advance(apply_time);
        trace!("  sim: applying {:?}", req);

        match req {
            &Request::InputStart(_, dir, speed) => {
                self.movement.input_start(send_time, apply_time, dir, speed);
            },
            &Request::InputChange(rel_time, dir, speed) => {
                self.movement.input_change(rel_time, dir, speed);
            },
            _ => unreachable!("unexpected request type"),
        }
    }

    fn advance(&mut self, time: Time) {
        let now = self.now;
        if time < now {
            error!("sim time is running backward! {} < {}", time, now);
            return;
        }

        self.sim_movement(now, time);
        self.now = time;
    }

    fn sim_movement(&mut self, start: Time, end: Time) {
        let (dir, speed) = self.movement.input;

        let mut shape = CacheShapeSource {
            terrain: &self.shape,
            z: self.activity.pos(self.activity_start, start).z >> TILE_BITS,
        };

        trace!("simulate movement from {} .. {}", start, end);
        let mut now = start;
        while let Some((new_act, new_start)) = movement::update_movement(
                &mut shape, self.activity.clone(), self.activity_start,
                dir, speed as i32 * MOVE_SPEED,
                now, end) {
            trace!("  update: {:?}, at {}", new_act, new_start);
            self.activity = new_act;
            self.activity_start = new_start;
            now = new_start;
            self.predicted_activity_change = true;
        }
    }

    pub fn finish(&mut self, platform_now: Time) {
        if !self.movement.seen_start {
            return;
        }

        // It's been `sim_duration` real-world milliseconds since the last `InputStart` occurred,
        // so be sure to simulate exactly `sim_duration` milliseconds since that `InputStart` was
        // applied.
        let sim_duration = platform_now - self.movement.start_send_time;
        let end_time = self.movement.start_apply_time + sim_duration;
        self.advance(end_time);

        // Don't fiddle with the pawn's activity if it's standing still.  But we do need to adjust
        // the activity if it stopped moving during the prediction window (in the "official" client
        // state, it's still moving.)
        if self.predicted_activity_change || self.movement.input.1 != 0 {
            // We've run the simulation up through `self.time`, and now we want to copy the final
            // `Activity` result into `state` such that the state at time `state.now()` shows the
            // same thing as the simulation at time `self.now`.  To do this, we shift the start
            // time of the final activity backward.
            let backdate_by = self.now - self.state.now();
            let pawn_id = unwrap_or!(self.state.pawn().id());
            let adjusted_start = self.activity_start - backdate_by;
            trace!("  set new activity for {:?}: {:?}, starting {}, adjusted {}",
                   pawn_id, self.activity, self.activity_start, adjusted_start);
            self.state.predict(Delta::EntityActivitySet(pawn_id,
                                                        Box::new(self.activity.clone()),
                                                        adjusted_start));
        }

        let e = unwrap_or!(self.state.pawn_entity());
        trace!("{}: pawn pos = {:?}", self.state.now(), e.pos(self.state.now()));
    }
}


/// Additional movement state not stored in the main game state.
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct MovementState {
    /// Current input direction and speed.
    input: (u8, u8),

    /// Time of the most recent input start or change.  
    time_base: Time,

    /// `send_time` of the most recent `InputStart`.
    start_send_time: Time,

    /// `apply_time` of the most recent `InputStart`.
    start_apply_time: Time,

    /// Have we ever seen an `InputStart`?
    seen_start: bool,
}

impl MovementState {
    pub fn new() -> MovementState {
        MovementState {
            input: (0, 0),
            time_base: 0,
            start_send_time: 0,
            start_apply_time: 0,
            seen_start: false,
        }
    }

    pub fn input_start(&mut self, send_time: Time, apply_time: Time, dir: u8, speed: u8) {
        self.input = (dir, speed);
        self.time_base = apply_time;
        self.start_send_time = send_time;
        self.start_apply_time = apply_time;
        self.seen_start = true;
    }

    pub fn input_change(&mut self, rel_time: u16, dir: u8, speed: u8) {
        self.input = (dir, speed);
        self.time_base += rel_time as Time;
        // Note that `time_base + rel_time` may not match the request's `apply_time`.  This can
        // happen if `time_base + rel_time` is in the past when the request arrives.  The request
        // will get applied late, but time_base is incremented as normal.
    }
}


struct CacheShapeSource<'a> {
    terrain: &'a terrain_shape::Cache,
    z: i32,
}

impl ShapeSource for CacheShapeSource<'_> {
    fn can_pass(&mut self, _start: V2, end: V2) -> bool {
        self.terrain.get_shape(end.extend(self.z)) == Shape::Floor
    }
}
