use std::prelude::v1::*;
use std::ops::Deref;
use std::ptr;
pub use client_data::*;


/// Get access to the global `Data` object.
///
/// ## Safety Warning
///
/// It is possible to violate memory safety by moving the returned `DataGuard` into a location that
/// outlives the caller.  So, don't do that.
///
/// It is also unsafe to call this before `set_data()` or after `clear_data()`.  Normally this is
/// not a problem because `set_data()` should be called soon after program startup.
pub fn data() -> DataGuard {
    DataGuard {
        _dummy: (),
    }
}


static mut DATA: *mut Data = 0 as *mut Data;

pub fn set_data(data: Box<Data>) {
    unsafe {
        if !DATA.is_null() {
            drop(Box::from_raw(DATA));
        }
        DATA = Box::into_raw(data);
    }
}

pub fn clear_data() {
    unsafe {
        if !DATA.is_null() {
            drop(Box::from_raw(DATA));
            DATA = ptr::null_mut();
        }
    }
}

pub fn has_data() -> bool {
    unsafe { !DATA.is_null() }
}

pub struct DataGuard {
    _dummy: (),
}

impl Deref for DataGuard {
    type Target = Data;
    fn deref(&self) -> &Data {
        unsafe { &*DATA }
    }
}
