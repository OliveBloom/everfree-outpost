use std::prelude::v1::*;
use types::*;

use data::data;
use platform::{Config, ConfigKey};


#[derive(Clone, Copy)]
struct HotbarSlot {
    item_id: u16,
    is_ability: bool,
}

impl HotbarSlot {
    #[allow(dead_code)]
    fn is_item(&self) -> bool {
        self.item_id != 0 && !self.is_ability
    }

    #[allow(dead_code)]
    fn is_ability(&self) -> bool {
        self.item_id != 0 && self.is_ability
    }

    #[allow(dead_code)]
    fn is_empty(&self) -> bool {
        self.item_id == 0
    }
}

pub struct Hotbar {
    slots: [HotbarSlot; 9],
    cur: i8,
}

impl Hotbar {
    pub fn new() -> Hotbar {
        Hotbar {
            slots: [HotbarSlot { item_id: 0, is_ability: false }; 9],
            cur: -1,
        }
    }

    pub fn init<C: Config>(&mut self, cfg: &C) {
        for i in 0 .. 9 {
            let name = cfg.get_str(ConfigKey::HotbarItemName(i as u8));
            let item_id = data().find_item_id(&name).unwrap_or(0);
            let is_item = cfg.get_int(ConfigKey::HotbarIsItem(i as u8)) != 0;

            self.slots[i].item_id = item_id;
            self.slots[i].is_ability = !is_item;
        }
    }

    pub fn item_id(&self, idx: u8) -> u16 {
        self.slots[idx as usize].item_id
    }

    #[allow(dead_code)]
    pub fn is_item(&self, idx: u8) -> bool {
        self.slots[idx as usize].is_item()
    }

    #[allow(dead_code)]
    pub fn is_ability(&self, idx: u8) -> bool {
        self.slots[idx as usize].is_ability()
    }

    pub fn active_index(&self) -> Option<u8> {
        if self.cur >= 0 && self.cur < 9 {
            Some(self.cur as u8)
        } else {
            None
        }
    }

    pub fn active_item(&self) -> Option<ItemId> {
        if self.cur >= 0 && self.cur < 9 {
            let slot = &self.slots[self.cur as usize];
            if slot.item_id != NO_ITEM {
                return Some(slot.item_id);
            }
        }
        None
    }

    pub fn set_slot<C: Config>(&mut self,
                               cfg: &mut C,
                               idx: u8, 
                               item_id: u16,
                               is_ability: bool) {
        if idx >= 9 {
            return;
        }

        self.slots[idx as usize].item_id = item_id;
        self.slots[idx as usize].is_ability = is_ability;

        let data = data();
        let name = data.item_def(item_id).name();
        cfg.set_str(ConfigKey::HotbarItemName(idx), name);
        cfg.set_int(ConfigKey::HotbarIsItem(idx), (!is_ability) as i32);
    }

    pub fn select(&mut self, idx: i8) {
        if idx >= 9 {
            return;
        }

        if idx < 0 {
            self.cur = -1
        } else {
            self.cur = idx;
        }
    }

    pub fn toggle_select(&mut self, idx: i8) {
        if self.cur == idx {
            self.select(-1);
        } else {
            self.select(idx);
        }
    }
}
