//! This file contains enums listing the UI graphics that are expected to appear in the client-side
//! UI data tables.  All are simple C-like enums, which can be cast to `usize` for indexing into
//! the data tables.  The enum values are collected by a Python script, which produces some tables
//! for use in building the UI data.

use client_data::{self, Data};


// NB: The script that processes this file expects the enums to be in a specific format.  The first
// line should be `pub enum Name {`, each variant should be on its own line in the form `Variant,`,
// and the final line should be exactly `}`.  Whitespace is ignored, and line comments (but not
// block comments) are allowed.
//
// Parts of the file outside the enum declarations will be ignored, so arbitrary Rust code is
// allowed there.
//
// Semantically, the relevant enums must be C-like (though they need not actually be declared
// `#[repr(C)]`), and the variants cannot have explicit discriminants assigned to them (as in
// `Variant = 123`).

/// Fixed-size rectangular images.
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Card {
    ScrollListMarker,
    ScrollBarThumb,

    // Item slots, with highlight
    ItemSlotRoundInactive,
    ItemSlotRoundSemiactive,
    ItemSlotRoundActive,
    ItemSlotSquareInactive,
    ItemSlotSquareSemiactive,
    ItemSlotSquareActive,

    // Public item slots, with highlight
    ItemSlotPublicRoundInactive,
    ItemSlotPublicRoundSemiactive,
    ItemSlotPublicRoundActive,
    ItemSlotPublicSquareInactive,
    ItemSlotPublicSquareSemiactive,
    ItemSlotPublicSquareActive,

    // Colored hotbar item frames
    ItemFrameYellow,
    ItemFrameGreen,
    ItemFrameBlue,
    ItemFrameRed,

    // PonyEdit dialog icons
    IconEarthPony,
    IconPegasus,
    IconUnicorn,

    IconFemale,
    IconMale,

    IconColorSliders,
    IconColorSwatches,
    IconColorPlot,

    // Toggle buttons, for rows and columns
    ToggleRowNormalLeft,
    ToggleRowNormalMid,
    ToggleRowNormalRight,

    ToggleRowDownLeft,
    ToggleRowDownMid,
    ToggleRowDownRight,

    ToggleRowVertNormalTop,
    ToggleRowVertNormalMid,
    ToggleRowVertNormalBottom,

    ToggleRowVertDownTop,
    ToggleRowVertDownMid,
    ToggleRowVertDownBottom,

    // Left and right spinner buttons
    SpinnerButtonLeftUp,
    SpinnerButtonLeftActive,
    SpinnerButtonLeftDown,
    SpinnerButtonLeftDisabled,

    SpinnerButtonRightUp,
    SpinnerButtonRightActive,
    SpinnerButtonRightDown,
    SpinnerButtonRightDisabled,

    // Downward arrow for active color boxes
    ColorBoxArrow,

    CraftingArrowEmpty,
    CraftingArrowFull,
}

/// Nine-part border definitions.  These can be stretched along one or both axes by tiling the
/// N/S/E/W parts.
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Border {
    /// A thin frame for surrounding groups of elements inside a dialog.  Also used for drawing
    /// separators.
    Frame,

    /// `Frame` with inverted highlights, so it appears to pop "out" instead of "in".
    FrameInv,

    /// Background rectangle for a scroll list entry.
    ScrollListEntry,
    /// Vertical scroll bar, consisting of the bar itself and the end caps.  Stretches only
    /// vertically.
    ScrollBar,

    /// Frame for dialog box content.
    DialogContent,
    /// Dialog box title bar.  Stretches only horizontally.
    DialogTitle,
    /// The little wooden connectors between the dialog box title and the content area frame.
    /// These need some special handling to avoid drawing half of a connector - see `dialog.rs`.
    DialogConnectors,

    /// Background of the hotbar UI.  Includes the end caps, the bar behind the item slots, and the
    /// energy bar background.
    HotbarBg,

    /// Energy bar fills for each tribe.
    EnergyFillEarth,
    EnergyFillPegasus,
    EnergyFillUnicorn,

    /// Box for a "selected color" display.
    ColorBox,
    ColorBoxActive,
    ColorBoxHover,

    // Normal buttons
    ButtonUp,
    ButtonActive,
    ButtonDown,

    /// Horizontal and vertical separators.  The 0/1 bits at the end indicate whether the endpoints
    /// of the separator are open (for connecting to a perpendicular separator) or closed.
    SepHoriz00,
    SepHoriz01,
    SepHoriz10,
    SepHoriz11,

    SepVert00,
    SepVert01,
    SepVert10,
    SepVert11,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Font {
    /// Default font for UI text.  8px sans-serif, with 1px drop shadow.  Also used to display
    /// player names in-game.
    Default,

    /// Bold font for UI text.  Based on `Default`.
    Bold,

    /// Dialog title font.  14px gothic.
    DialogTitle,

    /// Small font for displaying inventory counts.  6px sans-serif.  Only supports digits, '.',
    /// and 'k' characters.
    ItemCount,

    /// Red and yellow versions of the item count font, for missing recipe inputs.
    ItemCountRed,
    ItemCountYellow,
}

impl Default for Font {
    fn default() -> Font {
        Font::Default
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum InventoryLayout {
    Equipment,
}


pub trait UIDataExt {
    fn ui_card(&self, card: Card) -> &client_data::ui::UICard;
    fn ui_border(&self, border: Border) -> &client_data::ui::UIBorder;
    fn font(&self, font: Font) -> client_data::ui::Font;
    fn inventory_layout(&self, layout: InventoryLayout) -> client_data::ui::InventoryLayout;
}

impl UIDataExt for Data {
    fn ui_card(&self, card: Card) -> &client_data::ui::UICard {
        &self.ui_cards()[card as usize]
    }

    fn ui_border(&self, border: Border) -> &client_data::ui::UIBorder {
        &self.ui_borders()[border as usize]
    }

    fn font(&self, font: Font) -> client_data::ui::Font {
        self.index_font(font as usize)
    }

    fn inventory_layout(&self, layout: InventoryLayout) -> client_data::ui::InventoryLayout {
        self.index_inventory_layout(layout as usize)
    }
}
