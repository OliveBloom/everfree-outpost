use std::cell::Cell;
use std::ops::Deref;
use outpost_ui::context::Void;
use outpost_ui::event::{KeyEvent, MouseEvent, UIResult};
use outpost_ui::geom::Point;
use outpost_ui::widget::Widget;
use outpost_ui::widgets::container::{Group, ChildWidget, Align};

use state::entity::Entity;
use state::inventory;
use ui2::atlas::{Border, InventoryLayout};
use ui2::context::Context;
use ui2::widgets;

use super::container::{self, ContainerEvent};


pub struct CharDisplay<'a> {
    entity: Option<&'a Entity>,
}

impl<'a> CharDisplay<'a> {
    pub fn new(entity: Option<&'a Entity>) -> CharDisplay<'a> {
        CharDisplay {
            entity: entity,
        }
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for CharDisplay<'a> {
    type Event = Void;

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(96 + 2 * 2,
                   96 + 2 * 2)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        ctx.draw_border(Border::Frame);
        if let Some(entity) = self.entity {
            ctx.draw_special_pony(entity.appearance, Point::new(2, 2));
        }
    }
}


pub struct Equipment<'a> {
    inv: [&'a inventory::Inventory; 2],
    entity: Option<&'a Entity>,
    focus: [&'a Cell<usize>; 2],
    group_focus: &'a Cell<usize>,
}

impl<'a> Equipment<'a> {
    /// Build a new equipment dialog.  Inventory 0 is the "normal" inventory and inventory 1 is the
    /// equipment inventory.
    pub fn new(inv: [&'a inventory::Inventory; 2],
               entity: Option<&'a Entity>,
               focus: [&'a Cell<usize>; 2],
               group_focus: &'a Cell<usize>) -> Equipment<'a> {
        Equipment {
            inv: inv,
            entity: entity,
            focus: focus,
            group_focus: group_focus,
        }
    }

    fn with_inner<Ctx, CtxRef, F, R>(&self, ctx: CtxRef, f: F) -> R
            where Ctx: Context,
                  CtxRef: Deref<Target=Ctx>,
                  F: FnOnce(CtxRef, &Widget<Ctx, Event=(usize, usize)>) -> R {
        let grid = widgets::Inventory::new(self.inv[0], self.focus[0])
            .hide_slot(container::drag_hide_slot::<Ctx>(&ctx, self.inv[0].id));
        let custom = widgets::CustomInventory::new(self.inv[1],
                                                    InventoryLayout::Equipment,
                                                    self.focus[1])
            .hide_slot(container::drag_hide_slot::<Ctx>(&ctx, self.inv[1].id));
        let display = CharDisplay::new(self.entity);
        let w = Group::horiz(contents![
            ChildWidget::new(grid, |i| (0, i)).align(Align::Center),
            ChildWidget::new(custom, |i| (1, i)).align(Align::Center),
            ChildWidget::new(display, |v| match v {}).align(Align::Center),
        ]).focus(self.group_focus).spacing(4);

        f(ctx, &w)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for Equipment<'a> {
    type Event = ContainerEvent;

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.with_inner(ctx, |ctx, w| w.min_size(ctx))
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.with_inner(ctx, |ctx, w| w.on_paint(ctx));
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        self.with_inner(ctx, |ctx, w| w.on_key(ctx, evt)).and_then(|_| {
            unreachable!()
        }).or_else(|| {
            // TODO
            UIResult::Unhandled
        })
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.with_inner(&mut *ctx, |ctx, w| w.on_mouse(ctx, evt))
            .and_then(|(side, idx)| container::handle_inv_click(ctx, self.inv[side], idx))
            .map(|xfer| ContainerEvent::Transfer { transfer: xfer })
    }
}
