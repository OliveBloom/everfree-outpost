use common::types::*;
use std::cell::Cell;
use std::ops::Deref;
use outpost_ui::event::{KeyEvent, MouseEvent, UIResult};
use outpost_ui::geom::Point;
use outpost_ui::widget::Widget;
use outpost_ui::widgets::container::{Group, ChildWidget};

use state::inventory;
use ui2::context::{Context, DragItem};
use ui2::widgets;


pub struct Container<'a> {
    inv: [&'a inventory::Inventory; 2],
    focus: [&'a Cell<usize>; 2],
    group_focus: &'a Cell<usize>,
}

pub struct ItemTransfer {
    pub src_inv: InventoryId,
    pub src_slot: u8,
    pub dest_inv: InventoryId,
    pub dest_slot: u8,
    pub quantity: u8,
}

pub enum ContainerEvent {
    Transfer { transfer: ItemTransfer },
}

impl<'a> Container<'a> {
    pub fn new(inv: [&'a inventory::Inventory; 2],
               focus: [&'a Cell<usize>; 2],
               group_focus: &'a Cell<usize>) -> Container<'a> {
        Container {
            inv: inv,
            focus: focus,
            group_focus: group_focus,
        }
    }

    fn with_inner<Ctx, CtxRef, F, R>(&self, ctx: CtxRef, f: F) -> R
            where Ctx: Context,
                  CtxRef: Deref<Target=Ctx>,
                  F: FnOnce(CtxRef, &Widget<Ctx, Event=(usize, usize)>) -> R {
        let grid0 = widgets::Inventory::new(self.inv[0], self.focus[0])
            .hide_slot(drag_hide_slot::<Ctx>(&ctx, self.inv[0].id));
        let grid1 = widgets::Inventory::new(self.inv[1], self.focus[1])
            .hide_slot(drag_hide_slot::<Ctx>(&ctx, self.inv[1].id));
        let w = Group::horiz(contents![
            ChildWidget::new(grid0, |i| (0, i)),
            ChildWidget::new(grid1, |i| (1, i)),
        ]).focus(self.group_focus).spacing(4);

        f(ctx, &w)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for Container<'a> {
    type Event = ContainerEvent;

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.with_inner(ctx, |ctx, w| w.min_size(ctx))
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.with_inner(ctx, |ctx, w| w.on_paint(ctx));
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        self.with_inner(ctx, |ctx, w| w.on_key(ctx, evt)).and_then(|_| {
            unreachable!()
        }).or_else(|| {
            // TODO
            UIResult::Unhandled
        })
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.with_inner(&mut *ctx, |ctx, w| w.on_mouse(ctx, evt))
            .and_then(|(side, idx)| handle_inv_click(ctx, self.inv[side], idx))
            .map(|xfer| ContainerEvent::Transfer { transfer: xfer })
    }
}

pub fn drag_hide_slot<Ctx: Context>(ctx: &Ctx, iid: InventoryId) -> Option<u8> {
    if let Some(item) = ctx.drag_item() {
        if item.inv_id == iid {
            return Some(item.slot);
        }
    }
    None
}

pub fn handle_inv_click<Ctx: Context>(ctx: &mut Ctx,
                                      inv: &inventory::Inventory,
                                      idx: usize) -> UIResult<ItemTransfer> {
    if let Some(drag) = ctx.end_drag() {
        UIResult::Event(ItemTransfer {
            src_inv: drag.inv_id,
            src_slot: drag.slot,
            dest_inv: inv.id,
            dest_slot: idx as u8,
            quantity: drag.quantity,
        })
    } else {
        let item = unwrap_or!(inv.items.get(idx), {
            error!("container click index out of range: {}", idx);
            return UIResult::NoEvent;
        });

        if item.id == NO_ITEM {
            return UIResult::NoEvent;
        }

        ctx.start_drag(DragItem {
            item: item.id,
            quantity: item.quantity,
            inv_id: inv.id,
            slot: idx as u8,
        });
        UIResult::NoEvent
    }
}
