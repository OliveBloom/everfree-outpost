use std::prelude::v1::*;
use common::types::*;
use std::cell::{Cell, RefCell};
use std::cmp;
use std::ops::Deref;
use outpost_ui::context::Void;
use outpost_ui::event::{KeyEvent, MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;
use outpost_ui::widgets::Button;
use outpost_ui::widgets::container::{Group, ChildWidget, GenWidgets, Align};
use outpost_ui::widgets::map_event::MapEvent;
use outpost_ui::widgets::scroll::ScrollPane;

use data::{data, RecipeItem};
use state::{inventory, misc};
use state::misc::CraftingStateKind;
use ui2::atlas::{Card, Font};
use ui2::context::Context;
use ui2::widgets;
use ui2::widgets::{Spinner, HSep, VSep};
use ui2::widgets::list::TextListItem;
use util;

use super::container::{self, ItemTransfer};


pub struct RecipeCacheEntry {
    id: RecipeId,
}

pub struct RecipeCache {
    /// Hash of the inputs that produced the output in `recipes`.
    input_hash: u64,

    recipes: Vec<RecipeCacheEntry>,
}

impl RecipeCache {
    pub fn new() -> RecipeCache {
        RecipeCache {
            input_hash: 0,
            recipes: Vec::new(),
        }
    }

    pub fn recipes(&mut self,
                   ability: &inventory::Inventory,
                   class_mask: u32) -> &[RecipeCacheEntry] {
        let new_hash = util::hash(&(ability, class_mask));
        if new_hash != self.input_hash {
            self.update(ability, class_mask);
            self.input_hash = new_hash;
        }
        &self.recipes
    }

    fn update(&mut self,
              ability: &inventory::Inventory,
              class_mask: u32) {
        self.recipes = Vec::with_capacity(self.recipes.len());
        let data = data();
        for i in 0 .. data.recipes().len() {
            let id = i as RecipeId;
            let r = data.recipe(id);

            // Should this recipe be available in the list?
            let avail = (class_mask & (1 << r.crafting_class) != 0) &&
                        (r.ability == 0 || ability.count(r.ability) > 0);
            if !avail {
                continue;
            }

            self.recipes.push(RecipeCacheEntry {
                id: id,
            });
        }
    }
}


pub struct RecipeList<'a> {
    scroll_top: &'a Cell<i32>,
    focus: &'a Cell<usize>,
    recipes: &'a [RecipeCacheEntry],
}

impl<'a> RecipeList<'a> {
    pub fn new(scroll_top: &'a Cell<i32>,
               focus: &'a Cell<usize>,
               recipes: &'a [RecipeCacheEntry]) -> RecipeList<'a> {
        RecipeList {
            scroll_top: scroll_top,
            focus: focus,
            recipes: recipes,
        }
    }

    fn with_inner<Ctx, CtxRef, F, R>(&self, ctx: CtxRef, f: F) -> R
        where Ctx: Context,
              CtxRef: Deref<Target=Ctx>,
              F: FnOnce(CtxRef, &Widget<Ctx, Event=RecipeId>) -> R {
        let data = data();
        let size = ctx.cur_bounds().size();
        let contents = GenWidgets::new(0 .. self.recipes.len(), |idx| {
            let rce = &self.recipes[idx];
            let name =
                if let Some(r) = data.get_recipe(rce.id) { r.ui_name() }
                else { "(error)" };
            ChildWidget::new(TextListItem::new(name, size.x),
                             move |()| rce.id)
        });
        let w = ScrollPane::new(self.scroll_top, size, Group::vert(contents).focus(self.focus));
        f(ctx, &w)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for RecipeList<'a> {
    type Event = RecipeId;

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(100, 50)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.with_inner(ctx,  |ctx, w| w.on_paint(ctx));
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        self.with_inner(ctx, |ctx, w| w.on_key(ctx, evt))
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.with_inner(ctx, |ctx, w| w.on_mouse(ctx, evt))
    }
}


struct RecipeDisplay<'a> {
    recipe: RecipeId,
    progress: u32,
    count: u8,
    inv: &'a inventory::Inventory,
}

impl<'a> RecipeDisplay<'a> {
    pub fn new(recipe: RecipeId,
               progress: u32,
               count: u8,
               inv: &'a inventory::Inventory) -> RecipeDisplay<'a> {
        RecipeDisplay {
            recipe: recipe,
            progress: progress,
            count: count,
            inv: inv,
        }
    }
}

const RECIPE_ITEM_SIZE: i32 = 16;
const RECIPE_ITEM_PADDING: i32 = 2;

impl<'a, Ctx: Context> Widget<Ctx> for RecipeDisplay<'a> {
    type Event = Void;

    fn min_size(&self, ctx: &Ctx) -> Point {
        let item_size = Point::new(RECIPE_ITEM_SIZE, RECIPE_ITEM_SIZE);
        let padding = RECIPE_ITEM_PADDING;

        let arrow_size = ctx.card_size(Card::CraftingArrowEmpty);
        // Space for 2 columns of items on each side of the arrow.  Each column has 2px padding,
        // plus the size of the actual item
        let w = arrow_size.x + 2 * 2 * (item_size.x + padding);
        // Space for 3 rows of items.
        let h = 3 * item_size.x + 2 * padding;
        Point::new(w, h)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let data = data();
        let recipe = data.recipe(self.recipe);
        let max_progress = recipe.time as u32;

        let bounds = Rect::sized(ctx.cur_bounds().size());
        let draw_bounds = bounds.center(Rect::sized(self.min_size(ctx)));

        // Get the counts for the leftmost column of inputs and outputs.  Note the left column is
        // outer (away from the arrow) on the input side and inner (near the arrow) on the output
        // side.
        let split1 = outer_column_size(recipe.inputs().len());
        let split2 = inner_column_size(recipe.outputs().len());

        ctx.with_bounds(draw_bounds, |ctx| {
            let x0 = 0;
            let x1 = RECIPE_ITEM_SIZE + RECIPE_ITEM_PADDING;
            let x2 = draw_bounds.size().x - 2 * RECIPE_ITEM_SIZE - RECIPE_ITEM_PADDING;
            let x3 = draw_bounds.size().x - RECIPE_ITEM_SIZE;

            draw_recipe_column(ctx, x0, &recipe.inputs()[..split1], self.count, Some(self.inv));
            draw_recipe_column(ctx, x1, &recipe.inputs()[split1..], self.count, Some(self.inv));
            draw_recipe_column(ctx, x2, &recipe.outputs()[..split2], self.count, None);
            draw_recipe_column(ctx, x3, &recipe.outputs()[split2..], self.count, None);

            let arrow_size = ctx.card_size(Card::CraftingArrowEmpty);
            let arrow_pos = Rect::sized(draw_bounds.size()).center(Rect::sized(arrow_size)).min;
            ctx.draw_card_at(Card::CraftingArrowEmpty, arrow_pos);
            let fill_x = arrow_size.x * self.progress as i32 / max_progress as i32;
            let fill_src = Rect::sized(Point::new(fill_x, arrow_size.y));
            ctx.draw_card_partial(Card::CraftingArrowFull, fill_src, arrow_pos);
        });
    }
}

fn draw_recipe_column<Ctx: Context>(ctx: &mut Ctx,
                                    x: i32,
                                    items: &[RecipeItem],
                                    count: u8,
                                    inv: Option<&inventory::Inventory>) {
    let h = ctx.cur_bounds().size().y;
    let len = items.len() as i32;
    let draw_h = len * RECIPE_ITEM_SIZE + (len - 1) * RECIPE_ITEM_PADDING;
    let base = (h - draw_h) / 2;
    let item_size = Point::new(RECIPE_ITEM_SIZE, RECIPE_ITEM_SIZE);

    for (i, ri) in items.iter().enumerate() {
        let y = base + i as i32 * (RECIPE_ITEM_SIZE + RECIPE_ITEM_PADDING);
        let pos = Point::new(x, y);
        ctx.draw_item_at(ri.item, pos);

        // Amount needed for a single run of the recipe
        let need1 = ri.quantity as u16;
        // Amount needed for `count` runs
        let need = count as u16 * need1;

        let style = if let Some(inv) = inv {
            let avail = inv.count(ri.item);

            if avail < need1 { Font::ItemCountRed }
            else if avail < need { Font::ItemCountYellow }
            else { Font::ItemCount }
        } else {
            Font::ItemCount
        };

        let text = widgets::item::quantity_string(need);
        let text_size = ctx.text_size(&text, style);
        ctx.draw_text_at(&text, style, pos + item_size - text_size);
    }
}

/// When displaying `len` items, how many should go in the outer column?
fn outer_column_size(len: usize) -> usize {
    if len <= 3 {
        // Only 3 items, so put them all in the inner column.
        0
    } else {
        // Put half (rounded down) in the outer column, and half (rounded up) in the inner.
        len / 2
    }
}

fn inner_column_size(len: usize) -> usize {
    len - outer_column_size(len)
}


struct InputLine<'a> {
    count: &'a Cell<u8>,
    /// Did the user adjust the current count manually, since the last pause?
    user_set: &'a Cell<bool>,
    state: Option<&'a misc::CraftingState>,
}

enum InputEvent {
    Start(u8),
    Pause,
}

impl<'a> InputLine<'a> {
    fn new(count: &'a Cell<u8>,
           user_set: &'a Cell<bool>,
           state: Option<&'a misc::CraftingState>) -> InputLine<'a> {
        InputLine {
            count: count,
            user_set: user_set,
            state: state,
        }
    }

    fn display_count(&self) -> u8 {
        // Pause behavior:
        //
        // Suppose the user sets the count to 10 and starts crafting.  The spinner will be disabled
        // during crafting, and the number it displays will count down from 10 to 0, at which point
        // crafting is done.  Then the spinner will be enabled and set back to 10, so the user can
        // craft the same number of things again.
        //
        // Now suppose the user pauses while crafting, but immediately resumes without adjusting
        // the count.  Then the count should still reset back to 10 at the end, no matter what the
        // value was when the user resumed crafting.
        //
        // But the spinner is enabled while paused, so the user can (for example) start with count
        // = 10, pause when count = 5, adjust the spinner to count = 3, and resume.  In this
        // situation, when crafting finishes, we reset the count to the value input by the user, in
        // this case 3.
        //
        // In the implementation, there are two counts: the "user-input" count and the
        // "station-state" count.  The user-input count is set by the spinner and doesn't change
        // except in response to spinner events.  The station-state count is part of the crafting
        // station state provided by the server.
        //
        // In Idle state, we show the user-input count, and in Active state we show the
        // station-state count.  This means the displayed count will count down as crafting
        // proceeds in Active state, then reset to the initial input once crafting finishes.
        //
        // In Paused state, we may need to display either the station-state count or the user-input
        // count.  By default, we use the station-state count.  (Recall that the user-input count
        // will still be set to the initial input.)  But if the user adjusts the spinner, we set
        // the user-input count to the new spinner value (which, remember, is derived from the
        // station-state count), and switch to displaying the user-input count.  We track whether
        // the user has adjusted the count using the `user_set` flag.

        match self.state.map_or(CraftingStateKind::Idle, |s| s.kind) {
            CraftingStateKind::Idle => self.count.get(),

            CraftingStateKind::Paused => {
                if self.user_set.get() {
                    self.count.get()
                } else {
                    self.state.unwrap().count
                }
            },

            CraftingStateKind::Active => {
                self.state.unwrap().count
            },
        }
    }

    fn with_inner<Ctx, CtxRef, F, R>(&self, ctx: CtxRef, f: F) -> R
            where Ctx: Context,
                  CtxRef: Deref<Target=Ctx>,
                  F: FnOnce(CtxRef, &Widget<Ctx, Event=InputEvent>) -> R {
        match self.state.map_or(CraftingStateKind::Idle, |s| s.kind) {
            CraftingStateKind::Idle => {
                let spinner = Spinner::new(self.count.get() as i32, 2)
                    .handle_event(|_, c| self.count.set(cmp::max(1, cmp::min(99, c as u8))));
                let button = Button::new("Craft")
                    .and_then_event(|_, ()| UIResult::Event(InputEvent::Start(self.count.get())));
                let line = Group::horiz(contents![
                    ChildWidget::new(spinner, |v| match v {}),
                    ChildWidget::new(button, |e| e),
                ]).spacing(5);
                f(ctx, &line)
            },

            CraftingStateKind::Paused => {
                let show_count = if self.user_set.get() {
                    self.count.get()
                } else {
                    self.state.unwrap().count
                };

                let spinner = Spinner::new(show_count as i32, 2)
                    .handle_event(|_, c| {
                        self.count.set(cmp::max(1, cmp::min(99, c as u8)));
                        self.user_set.set(true);
                    });
                let button = Button::new("Craft")
                    .and_then_event(|_, ()| UIResult::Event(InputEvent::Start(show_count)));
                let line = Group::horiz(contents![
                    ChildWidget::new(spinner, |v| match v {}),
                    ChildWidget::new(button, |e| e),
                ]).spacing(5);
                f(ctx, &line)
            },

            CraftingStateKind::Active => {
                if self.user_set.get() {
                    self.user_set.set(false);
                }

                let spinner = Spinner::new(self.state.unwrap().count as i32, 2)
                    .enabled(false)
                    .ignore_event();
                let button = Button::new("Pause")
                    .and_then_event(|_, ()| UIResult::Event(InputEvent::Pause));
                let line = Group::horiz(contents![
                    ChildWidget::new(spinner, |v| match v {}),
                    ChildWidget::new(button, |e| e),
                ]).spacing(5);
                f(ctx, &line)
            },
        }
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for InputLine<'a> {
    type Event = InputEvent;

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.with_inner(ctx, |ctx, w| w.min_size(ctx))
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.with_inner(ctx, |ctx, w| w.on_paint(ctx));
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        self.with_inner(ctx, |ctx, w| w.on_key(ctx, evt))
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.with_inner(&mut *ctx, |ctx, w| w.on_mouse(ctx, evt))
    }
}





pub struct CraftingState {
    cache: RecipeCache,
    scroll_top: i32,
    craft_count: u8,
    craft_count_user_set: bool,
    recipe_focus: usize,
}

impl CraftingState {
    pub fn new() -> CraftingState {
        CraftingState {
            cache: RecipeCache::new(),
            scroll_top: 0,
            craft_count: 1,
            craft_count_user_set: false,
            recipe_focus: 0,
        }
    }
}


pub struct Crafting<'a> {
    now: Time,
    state: &'a RefCell<CraftingState>,
    class_mask: u32,
    contents: &'a inventory::Inventory,
    inv: &'a inventory::Inventory,
    ability: &'a inventory::Inventory,
    station_state: Option<&'a misc::CraftingState>,
}

pub enum CraftingEvent {
    Transfer { transfer: ItemTransfer },
    Start { id: RecipeId, count: u8 },
    Stop,
}

impl<'a> Crafting<'a> {
    pub fn new(now: Time,
               state: &'a RefCell<CraftingState>,
               class_mask: u32,
               contents: &'a inventory::Inventory,
               inv: &'a inventory::Inventory,
               ability: &'a inventory::Inventory,
               station_state: Option<&'a misc::CraftingState>) -> Crafting<'a> {
        Crafting {
            now: now,
            state: state,
            class_mask: class_mask,
            contents: contents,
            inv: inv,
            ability: ability,
            station_state: station_state,
        }
    }

    fn with_inner<Ctx, CtxRef, F, R>(&self, ctx: CtxRef, f: F) -> R
            where Ctx: Context,
                  CtxRef: Deref<Target=Ctx>,
                  F: FnOnce(CtxRef, &Widget<Ctx, Event=CraftingEvent>) -> R {
        let mut s = self.state.borrow_mut();
        let s = &mut *s;

        // Recipe list
        let recipes = s.cache.recipes(self.ability, self.class_mask);
        let recipe_focus = util::mut_to_cell(&mut s.recipe_focus);
        let recipe_id = recipes.get(recipe_focus.get()).map(|r| r.id).unwrap_or(NO_RECIPE);
        let scroll_top = util::mut_to_cell(&mut s.scroll_top);
        let recipe_list = RecipeList::new(scroll_top, recipe_focus, recipes)
            .and_then_event(|_, id| {
                if let Some(ss) = self.station_state {
                    if ss.kind == CraftingStateKind::Active && ss.recipe != id {
                        return UIResult::Event(CraftingEvent::Stop)
                    }
                }
                UIResult::NoEvent
            });

        // Recipe count spinner and "Craft" button
        let craft_count = util::mut_to_cell(&mut s.craft_count);
        let craft_count_user_set = util::mut_to_cell(&mut s.craft_count_user_set);
        let craft_line = InputLine::new(craft_count, craft_count_user_set, self.station_state)
            .map_event(|_, e| match e {
                InputEvent::Start(count) => CraftingEvent::Start { id: recipe_id, count: count },
                InputEvent::Pause => CraftingEvent::Stop,
            });

        // Inventory display
        let dummy_inv_focus = Cell::new(self.inv.len());
        let inv = widgets::Inventory::new(self.inv, &dummy_inv_focus)
            .hide_slot(container::drag_hide_slot::<Ctx>(&ctx, self.inv.id))
            .and_then_event(|ctx, idx| container::handle_inv_click(ctx, self.inv, idx));
        let contents = widgets::Inventory::new(self.contents, &dummy_inv_focus)
            .hide_slot(container::drag_hide_slot::<Ctx>(&ctx, self.contents.id))
            .and_then_event(|ctx, idx| container::handle_inv_click(ctx, self.contents, idx));

        // Recipe display
        let progress = if let Some(ss) = self.station_state {
            if ss.recipe == recipe_id {
                ss.progress.get(self.now) as u32
            } else {
                0
            }
        } else {
            0
        };
        trace!("station {}, progress {}", self.station_state.is_some(), progress);
        let recipe_display = RecipeDisplay::new(recipe_id,
                                                progress,
                                                craft_line.display_count(),
                                                self.contents);

        // Groups
        let right = Group::vert(contents![
            ChildWidget::new(inv, |e| CraftingEvent::Transfer { transfer: e }),
            ChildWidget::new(HSep::new().left_ext(5), |v| match v {}),
            ChildWidget::new(contents, |e| CraftingEvent::Transfer { transfer: e }),
            ChildWidget::new(recipe_display, |v| match v{}).align(Align::Center),
            ChildWidget::new(craft_line, |e| e).align(Align::Center),
        ]).spacing(6);

        let w = Group::horiz(contents![
            ChildWidget::new(recipe_list, |e| e),
            ChildWidget::new(VSep::new(), |v| match v {}),
            ChildWidget::new(right, |e| e),
        ]).spacing(4);

        f(ctx, &w)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for Crafting<'a> {
    type Event = CraftingEvent;

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.with_inner(ctx, |ctx, w| w.min_size(ctx))
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.with_inner(ctx, |ctx, w| w.on_paint(ctx));
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        self.with_inner(ctx, |ctx, w| w.on_key(ctx, evt))
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.with_inner(&mut *ctx, |ctx, w| w.on_mouse(ctx, evt))
    }
}
