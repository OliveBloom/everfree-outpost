use common::types::*;
use std::cell::{Cell, RefCell};
use common::Appearance;
use common::Gauge;
use outpost_ui::context::Context as ContextTrait;
use outpost_ui::event::{KeyEvent, MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;
use outpost_ui::widgets::map_event::MapEvent;
use render::Backend;

use debug::Debug;
use graphics::renderer::ui::Output;
use hotbar::Hotbar;
use input::{Key, Button};
use inv_changes::InvChanges;
use state::entity::Entity;
use state::inventory::{Inventories, Inventory};
use state::misc::Misc;
use state::pawn;
use ui2::atlas::Border;
use ui2::context::{Context, ContextState, ContextImpl};
use ui2::dialogs;
use ui2::dialogs::inventory::InventoryEvent;
use ui2::dialogs::container::{ContainerEvent, ItemTransfer};
use ui2::dialogs::crafting::{CraftingState, CraftingEvent};
use ui2::dialogs::pony_edit::PonyEditState;
use ui2::util::*;
use ui2::widgets;
use ui2::widgets::hotbar::HotbarEvent;


pub enum DialogState {
    None,

    Inventory {
        inv_id: InventoryId,
        focus: Cell<usize>,
    },

    Abilities {
        inv_id: InventoryId,
        focus: Cell<usize>,
    },

    Container {
        inv_id: [InventoryId; 2],
        focus: [Cell<usize>; 2],
        group_focus: Cell<usize>,
    },

    Equipment {
        inv_id: [InventoryId; 2],
        focus: [Cell<usize>; 2],
        group_focus: Cell<usize>,
    },

    Crafting {
        state: Box<RefCell<CraftingState>>,
        station: StructureId,
        class_mask: u32,
        contents_id: InventoryId,
        inv_id: InventoryId,
        ability_id: InventoryId,
        // TODO: crafting classes
    },

    PonyEdit {
        state: Box<RefCell<PonyEditState>>,
    }
}

impl DialogState {
    pub fn none() -> DialogState {
        DialogState::None
    }

    pub fn is_none(&self) -> bool {
        match *self {
            DialogState::None => true,
            _ => false,
        }
    }

    pub fn inventory(inv_id: InventoryId) -> DialogState {
        DialogState::Inventory {
            inv_id: inv_id,
            focus: Cell::new(0),
        }
    }

    pub fn abilities(inv_id: InventoryId) -> DialogState {
        DialogState::Abilities {
            inv_id: inv_id,
            focus: Cell::new(0),
        }
    }

    pub fn container(inv_id0: InventoryId, inv_id1: InventoryId) -> DialogState {
        DialogState::Container {
            inv_id: [inv_id0, inv_id1],
            focus: [Cell::new(0), Cell::new(0)],
            group_focus: Cell::new(0),
        }
    }

    pub fn equipment(inv_id0: InventoryId, inv_id1: InventoryId) -> DialogState {
        DialogState::Equipment {
            inv_id: [inv_id0, inv_id1],
            focus: [Cell::new(0), Cell::new(0)],
            group_focus: Cell::new(0),
        }
    }

    pub fn crafting(station: StructureId,
                    class_mask: u32,
                    contents_id: InventoryId,
                    inv_id: InventoryId,
                    ability_id: InventoryId) -> DialogState {
        DialogState::Crafting {
            state: Box::new(RefCell::new(CraftingState::new())),
            station: station,
            class_mask: class_mask,
            contents_id: contents_id,
            inv_id: inv_id,
            ability_id: ability_id,
        }
    }

    pub fn pony_edit() -> DialogState {
        DialogState::PonyEdit {
            state: Box::new(RefCell::new(PonyEditState::new())),
        }
    }

    /// Update the dialog state `*self` to match the requested `pawn::Dialog`.  Returns `true` if
    /// it actually changed `*self`.
    pub fn update(&mut self, dialog: &pawn::Dialog) -> bool {
        // If the dialog kind and read-only parts match, then do nothing.
        match (&*self, dialog) {
            (&DialogState::None,
             &pawn::Dialog::None) => return false,
            (&DialogState::Inventory { inv_id, .. },
             &pawn::Dialog::Inventory(expect))
                if inv_id == expect => return false,
            (&DialogState::Abilities { inv_id, .. },
             &pawn::Dialog::Abilities(expect))
                if inv_id == expect => return false,
            (&DialogState::Container { inv_id, .. },
             &pawn::Dialog::Container(expect0, expect1))
                if inv_id == [expect0, expect1] => return false,
            (&DialogState::Equipment { inv_id, .. },
             &pawn::Dialog::Equipment(expect0, expect1))
                if inv_id == [expect0, expect1] => return false,
            (&DialogState::Crafting { station, class_mask, contents_id, inv_id, ability_id, .. },
             &pawn::Dialog::Crafting(expect_station, expect_class_mask, expect_contents,
                                     expect_inv, expect_ability))
                if station == expect_station &&
                   class_mask == expect_class_mask &&
                   contents_id == expect_contents &&
                   inv_id == expect_inv &&
                   ability_id == expect_ability => return false,
            (&DialogState::PonyEdit { .. },
             &pawn::Dialog::PonyEdit) => return false,
            _ => {},
        }

        // Otherwise, change *self to the requested dialog kind.
        *self = match *dialog {
            pawn::Dialog::None => DialogState::none(),
            pawn::Dialog::Inventory(iid) => DialogState::inventory(iid),
            pawn::Dialog::Abilities(iid) => DialogState::abilities(iid),
            pawn::Dialog::Equipment(iid0, iid1) => DialogState::equipment(iid0, iid1),
            pawn::Dialog::Container(iid0, iid1) => DialogState::container(iid0, iid1),
            pawn::Dialog::Crafting(station, class_mask, contents, inv, ability) =>
                DialogState::crafting(station, class_mask, contents, inv, ability),
            pawn::Dialog::PonyEdit => DialogState::pony_edit(),
        };
        true
    }
}


#[derive(Clone)]
pub struct Args<'a> {
    pub now: Time,
    pub pawn: Option<&'a Entity>,
    pub invs: &'a Inventories,
    pub inv_changes: &'a InvChanges,
    pub misc: &'a Misc,
    pub hotbar: &'a Hotbar,
    pub energy: &'a Gauge,
    pub debug: &'a Debug,
    pub dialog: &'a pawn::Dialog,
}


pub struct Root<'a> {
    dialog_state: &'a DialogState,
    args: Args<'a>,
}

pub enum RootEvent {
    CloseDialog,

    CreateCharacter {
        appearance: Appearance,
    },

    ItemTransfer {
        src_slot: u8,
        dest_slot: u8,
        quantity: u8,
        src_inv: InventoryId,
        dest_inv: InventoryId,
    },

    CraftingStart {
        recipe_id: RecipeId,
        count: u8,
    },

    CraftingStop,

    HotbarSet {
        slot: u8,
        item: ItemId,
    },

    HotbarSelect {
        slot: u8,
    },
}

impl RootEvent {
    fn from_item_transfer(xfer: ItemTransfer) -> RootEvent {
        let ItemTransfer { src_inv, src_slot, dest_inv, dest_slot, quantity } = xfer;
        RootEvent::ItemTransfer {
            src_slot: src_slot,
            dest_slot: dest_slot,
            quantity: quantity,
            src_inv: src_inv,
            dest_inv: dest_inv,
        }
    }
}

impl<'a> Root<'a> {
    fn with_hotbar<Ctx: Context, F, R>(&self, ctx: &mut Ctx, f: F) -> R
            where F: FnOnce(&mut Ctx, &Widget<Ctx, Event=RootEvent>) -> R {
        let w = widgets::Hotbar::new(self.args.hotbar,
                                     self.args.invs.main_inventory(),
                                     self.args.energy.get(self.args.now),
                                     self.args.energy.max(),
                                     Border::EnergyFillEarth)
            .map_event(|_, evt| match evt {
                HotbarEvent::Set { slot, item } =>
                    RootEvent::HotbarSet { slot: slot, item: item },
                HotbarEvent::Select { slot } =>
                    RootEvent::HotbarSelect { slot: slot },
            });

        let size = w.min_size(ctx);
        let bounds = Rect::sized(size);
        ctx.with_bounds(bounds, |ctx| f(ctx, &w))
    }

    fn with_dialog_raw<Ctx: Context, F, R>(&self, f: F) -> R
            where F: FnOnce(&Widget<Ctx, Event=RootEvent>) -> R, R: Default {
        macro_rules! get_inv {
            ($id:expr) => {
                match self.args.invs.get($id) {
                    Some(x) => x,
                    None => {
                        error!("dialog references nonexistent inventory {:?}", $id);
                        return R::default();
                    },
                }
            };
        }

        match *self.dialog_state {
            DialogState::None => R::default(),

            DialogState::Inventory { inv_id, ref focus } => {
                let w = dialogs::Inventory::new(get_inv!(inv_id), focus);
                let w = w.map_event(|_, evt| match evt {
                    InventoryEvent::Transfer { src_slot, dest_slot, quantity } =>
                        RootEvent::ItemTransfer {
                            src_slot: src_slot,
                            dest_slot: dest_slot,
                            quantity: quantity,
                            src_inv: inv_id,
                            dest_inv: inv_id,
                        },
                    InventoryEvent::SetHotbar { slot, item } =>
                        RootEvent::HotbarSet { slot: slot, item: item },
                });
                let w = widgets::Dialog::new(w, "Inventory");

                f(&w)
            },

            DialogState::Abilities { inv_id, ref focus } => {
                let w = dialogs::Inventory::new(get_inv!(inv_id), focus);
                let w = w.map_event(|_, evt| match evt {
                    InventoryEvent::Transfer { src_slot, dest_slot, quantity } =>
                        RootEvent::ItemTransfer {
                            src_slot: src_slot,
                            dest_slot: dest_slot,
                            quantity: quantity,
                            src_inv: inv_id,
                            dest_inv: inv_id,
                        },
                    InventoryEvent::SetHotbar { slot, item } =>
                        RootEvent::HotbarSet { slot: slot, item: item },
                });
                let w = widgets::Dialog::new(w, "Abilities");

                f(&w)
            },

            DialogState::Container { inv_id, ref focus, ref group_focus } => {
                let w = dialogs::Container::new([get_inv!(inv_id[0]), get_inv!(inv_id[1])],
                                                [&focus[0], &focus[1]],
                                                group_focus);
                let w = w.map_event(|_, evt| match evt {
                    ContainerEvent::Transfer { transfer } =>
                        RootEvent::from_item_transfer(transfer),
                });
                let w = widgets::Dialog::new(w, "Container");

                f(&w)
            },

            DialogState::Equipment { inv_id, ref focus, ref group_focus } => {
                let w = dialogs::Equipment::new([get_inv!(inv_id[0]), get_inv!(inv_id[1])],
                                                self.args.pawn,
                                                [&focus[0], &focus[1]],
                                                group_focus);
                let w = w.map_event(|_, evt| match evt {
                    ContainerEvent::Transfer { transfer } =>
                        RootEvent::from_item_transfer(transfer),
                });
                let w = widgets::Dialog::new(w, "Equipment");

                f(&w)
            },

            DialogState::Crafting {
                ref state, station, class_mask, contents_id, inv_id, ability_id
            } => {
                let w = dialogs::Crafting::new(self.args.now,
                                               state,
                                               class_mask,
                                               get_inv!(contents_id),
                                               get_inv!(inv_id),
                                               get_inv!(ability_id),
                                               self.args.misc.station_crafting_state(station));
                let w = w.map_event(|_, evt| match evt {
                    CraftingEvent::Transfer { transfer } =>
                        RootEvent::from_item_transfer(transfer),
                    CraftingEvent::Start { id, count } => RootEvent::CraftingStart {
                        recipe_id: id,
                        count: count,
                    },
                    CraftingEvent::Stop => RootEvent::CraftingStop,
                });
                let w = widgets::Dialog::new(w, "Crafting");

                f(&w)
            },

            DialogState::PonyEdit { ref state } => {
                let w = dialogs::PonyEdit::new(state);
                let w = w.map_event(|_, app| RootEvent::CreateCharacter { appearance: app });
                let w = widgets::Dialog::new(w, "Character");

                f(&w)
            },
        }
    }

    fn with_dialog<Ctx: Context, F, R>(&self, ctx: &mut Ctx, f: F) -> R
            where F: FnOnce(&mut Ctx, &Widget<Ctx, Event=RootEvent>) -> R, R: Default {
        self.with_dialog_raw(|w| {
            let size = w.min_size(ctx);
            let bounds = Rect::sized(size);
            let cur_bounds = Rect::sized(ctx.cur_bounds().size());
            ctx.with_bounds(cur_bounds.center(bounds), |ctx| f(ctx, w))
        })
    }

    fn with_debug<Ctx: Context, F, R>(&self, ctx: &mut Ctx, f: F) -> R
            where F: FnOnce(&mut Ctx, &widgets::Debug) -> R, R: Default {
        let w = widgets::Debug::new(self.args.debug);
        let size = w.min_size(ctx);
        let offset = ctx.cur_bounds().size().x - size.x;
        let bounds = Rect::sized(size) + Point::new(offset, 0);
        ctx.with_bounds(bounds, |ctx| f(ctx, &w))
    }

    fn with_cursor_item<Ctx: Context, F, R>(&self, ctx: &mut Ctx, f: F) -> R
            where F: FnOnce(&mut Ctx, &widgets::Item) -> R, R: Default {
        let info = if self.dialog_state.is_none() {
            hotbar_item_info(self.args.hotbar, self.args.invs.main_inventory())
        } else {
            ctx.drag_item().map(|i| (i.item, Some(i.quantity as u16)))
        };
        if let Some((item, quantity)) = info {
            if let Some(pos) = ctx.mouse_pos() {
                let w = widgets::Item::new(item, quantity);
                let size = w.min_size(ctx);
                let bounds = Rect::sized(size) + pos;
                return ctx.with_bounds(bounds, |ctx| f(ctx, &w));
            }
        }
        R::default()
    }

    fn with_inv_changes<Ctx: Context, F, R>(&self, ctx: &mut Ctx, f: F) -> R
            where F: FnOnce(&mut Ctx, &widgets::InvChanges) -> R {
        let w = widgets::InvChanges::new(self.args.now, self.args.inv_changes);
        let size = w.min_size(ctx);
        let max = ctx.cur_bounds().size();
        let pos = max - size;
        ctx.with_bounds(Rect::new(pos.x, pos.y, max.x, max.y), |ctx| {
            f(ctx, &w)
        })
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for Root<'a> {
    type Event = RootEvent;

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(0, 0)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.with_hotbar(ctx, |ctx, w| w.on_paint(ctx));
        self.with_dialog(ctx, |ctx, w| w.on_paint(ctx));
        self.with_debug(ctx, |ctx, w| w.on_paint(ctx));
        self.with_cursor_item(ctx, |ctx, w| w.on_paint(ctx));
        self.with_inv_changes(ctx, |ctx, w| w.on_paint(ctx));

        //ctx.draw_special_debug(Point::new(700, 0));
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        try_handle!(self.with_dialog(ctx, |ctx, w| w.on_key(ctx, evt)));
        try_handle!(self.with_hotbar(ctx, |ctx, w| w.on_key(ctx, evt)));

        // ESC cancels drag
        match evt {
            KeyEvent::Down((Key::Cancel, _)) => {
                ctx.end_drag();
            },
            _ => {},
        }

        UIResult::Unhandled
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        try_handle!(self.with_dialog(ctx, |ctx, w| w.on_mouse(ctx, evt)));
        try_handle!(self.with_hotbar(ctx, |ctx, w| w.on_mouse(ctx, evt)));

        // Click on nothing cancels drag
        match evt {
            MouseEvent::Up(_) => {
                ctx.end_drag();
            },
            _ => {},
        }

        UIResult::Unhandled
    }
}

fn hotbar_item_info(hotbar: &Hotbar,
                    main_inv: Option<&Inventory>) -> Option<(ItemId, Option<u16>)> {
    let idx = unwrap_or!(hotbar.active_index(), return None);

    let item = hotbar.item_id(idx);
    if item == NO_ITEM {
        return None;
    }

    let qty = if hotbar.is_item(idx) {
        main_inv.map(|i| i.count(item))
    } else {
        None
    };

    return Some((item, qty));
}


pub struct UI {
    dialog_state: DialogState,
    ctx: ContextState,
}

impl UI {
    pub fn new() -> UI {
        UI {
            dialog_state: DialogState::None,
            ctx: ContextState::new(Region::sized(V2::new(400, 300))),
        }
    }

    pub fn resize(&mut self, size: (u16, u16)) {
        self.ctx.get().resize(Point::new(size.0 as i32, size.1 as i32));
    }

    /// Update the UI dialog state to match the current dialog in the game state.
    ///
    /// When a request such as `OpenInventory` is pending, the "current dialog" according to the
    /// game state is constantly getting rolled back to `None` (the server-side value) and being
    /// predicted forward to `Inventory`.  But closing a dialog loses transient UI state like the
    /// current focus index and dragged item.  So we use this approach of comparing the UI and game
    /// state dialog fields on each frame, and changing the UI dialog only when there's a mismatch.
    fn update_dialog(&mut self, dialog: &pawn::Dialog) {
        let changed = self.dialog_state.update(dialog);
        if changed {
            // Cancel any ongoing drag.  This should only be relevant when the dialog closes from
            // the server side, as locally, the "cancel drag" binding takes precedence over "close
            // dialog".
            self.ctx.get().end_drag();
        }
    }

    pub fn paint<B: Backend>(&mut self, args: Args, output: &mut Output<B>) {
        self.update_dialog(args.dialog);

        let w = Root {
            dialog_state: &self.dialog_state,
            args: args,
        };
        w.on_paint(&mut self.ctx.with_output(&mut *output));
    }

    pub fn key_event(&mut self,
                     args: Args,
                     evt: KeyEvent<<ContextImpl<()> as ContextTrait>::Key>)
                     -> UIResult<RootEvent> {
        self.update_dialog(args.dialog);

        let w = Root {
            dialog_state: &self.dialog_state,
            args: args,
        };
        w.on_key(&mut self.ctx.get(), evt).or_else(|| {
            match evt {
                KeyEvent::Down((Key::Cancel, _)) if !self.dialog_state.is_none() => {
                    UIResult::Event(RootEvent::CloseDialog)
                },
                _ => UIResult::Unhandled,
            }
        })
    }

    pub fn mouse_event(&mut self,
                       args: Args,
                       pos: V2,
                       evt: MouseEvent<<ContextImpl<()> as ContextTrait>::Button>)
                       -> UIResult<RootEvent> {
        self.update_dialog(args.dialog);

        let evt = match evt {
            MouseEvent::Down((Button::WheelUp, mods)) =>
                MouseEvent::Wheel(if mods.shift() { 10 } else { 1 }),
            MouseEvent::Down((Button::WheelDown, mods)) =>
                MouseEvent::Wheel(if mods.shift() { -10 } else { -1 }),
            MouseEvent::Up((Button::WheelUp, _)) |
            MouseEvent::Up((Button::WheelDown, _)) => return UIResult::NoEvent,
            e => e,
        };

        match evt {
            MouseEvent::Down((Button::Left, _)) =>
                self.ctx.get().state_mut().record_mouse_down(from_v2(pos)),
            // Defer handling of `Up` until after the widget runs.  This way `mouse_pressed_over`
            // still returns true when it should.
            _ =>
                self.ctx.get().state_mut().record_mouse_move(from_v2(pos)),
        }

        let w = Root {
            dialog_state: &self.dialog_state,
            args: args,
        };
        let r = w.on_mouse(&mut self.ctx.get(), evt);

        match evt {
            MouseEvent::Up((Button::Left, _)) =>
                self.ctx.get().state_mut().record_mouse_up(from_v2(pos)),
            _ => {},
        }

        r
    }
}
