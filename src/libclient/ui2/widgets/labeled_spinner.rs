use std::cmp;

use outpost_ui::event::{KeyEvent, KeyInterp, MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;
use outpost_ui::widgets::button::Button;

use data::data;
use ui2::atlas::{Card, Font, UIDataExt};
use ui2::context::{Context, ButtonStyle};


/// A horizontal spinner.
pub struct Spinner {
    value: i32,
    /// Number of digits to display.
    digits: usize,
    enabled: bool,
}

impl Spinner {
    pub fn new(value: i32,
               digits: usize) -> Spinner {
        Spinner {
            value: value,
            digits: digits,
            enabled: true,
        }
    }

    pub fn enabled(self, enabled: bool) -> Self {
        Self {
            enabled: enabled,
            .. self
        }
    }
}

impl<Ctx: Context> Widget<Ctx> for Spinner {
    type Event = i32;

    fn min_size(&self, ctx: &Ctx) -> Point {
        let l = data().ui_card(Card::SpinnerButtonLeftUp).size();
        let r = data().ui_card(Card::SpinnerButtonRightUp).size();

        let text_w = self.digits as i32 * ctx.text_size("0", Font::Default).x;
        let text_h = ctx.text_line_height(Font::Default);

        // 2px padding between the label and the buttons on each side.
        let w = l.x + 2 + text_w + 2 + r.x;
        let h = cmp::max(l.y, text_h);
        Point::new(w, h)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let l_btn = Button::new("").style(ButtonStyle::SpinnerLeft);
        let r_btn = Button::new("").style(ButtonStyle::SpinnerRight);

        let spin_size = ctx.cur_bounds().size();

        let l_size = l_btn.min_size(ctx);
        let l_bounds = Rect::sized(l_size) +
            Point::new(0, (spin_size.y - l_size.y) / 2);

        let r_size = r_btn.min_size(ctx);
        let r_bounds = Rect::sized(r_size) +
            Point::new(spin_size.x - r_size.x, (spin_size.y - r_size.y) / 2);

        // Reminder: display values are `1` to `max`, not `0` to `max - 1`.
        let text_max_w = self.digits as i32 * ctx.text_size("0", Font::Default).x;
        let text = format!("{}", self.value);
        let text_size = ctx.text_size(&text, Font::Default);
        // Center a region of width `text_max_w`, then right-align the actual text inside it.
        // Extra +1 offset to Y makes it look better.
        let label_pos = Point::new((spin_size.x - text_max_w) / 2 + (text_max_w - text_size.x),
                                   (spin_size.y - text_size.y) / 2 + 1);

        if self.enabled {
            ctx.with_bounds(l_bounds, |ctx| {
                l_btn.on_paint(ctx);
            });
            ctx.with_bounds(r_bounds, |ctx| {
                r_btn.on_paint(ctx);
            });
        } else {
            ctx.with_bounds(l_bounds, |ctx| {
                ctx.draw_card(Card::SpinnerButtonLeftDisabled);
            });
            ctx.with_bounds(r_bounds, |ctx| {
                ctx.draw_card(Card::SpinnerButtonRightDisabled);
            });
        }
        ctx.draw_text_at(&text, Font::Default, label_pos);
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        match ctx.interp_key(evt) {
            Some(KeyInterp::FocusX(dx)) => {
                UIResult::Event(self.value.wrapping_add(dx as i32))
            },

            _ => UIResult::Unhandled,
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let l_btn = Button::new("").style(ButtonStyle::SpinnerLeft);
        let r_btn = Button::new("").style(ButtonStyle::SpinnerRight);

        let spin_size = ctx.cur_bounds().size();

        let l_size = l_btn.min_size(ctx);
        let l_bounds = Rect::sized(l_size) +
            Point::new(0, (spin_size.y - l_size.y) / 2);

        let r_size = r_btn.min_size(ctx);
        let r_bounds = Rect::sized(r_size) +
            Point::new(spin_size.x - l_size.x, (spin_size.y - l_size.y) / 2);

        match evt {
            MouseEvent::Down((_, mods)) |
            MouseEvent::Up((_, mods)) => {
                let mag = if mods.shift() { 10 } else { 1 };
                try_handle!(ctx.with_bounds(l_bounds, |ctx| {
                    if ctx.mouse_target() {
                        l_btn.on_mouse(ctx, evt).map(|()| {
                            self.value.wrapping_sub(mag)
                        })
                    } else {
                        UIResult::Unhandled
                    }
                }));
                try_handle!(ctx.with_bounds(r_bounds, |ctx| {
                    if ctx.mouse_target() {
                        r_btn.on_mouse(ctx, evt).map(|()| {
                            self.value.wrapping_add(mag)
                        })
                    } else {
                        UIResult::Unhandled
                    }
                }));
                UIResult::Unhandled
            }

            MouseEvent::Move => UIResult::Unhandled,

            MouseEvent::Wheel(n) => {
                UIResult::Event(self.value + n as i32)
            },
        }
    }
}


/// A horizontal spinner, for selecting from a sequence of items labeled `Foo 1` through `Foo N`.
///
/// The `value` field is in the usual range `0 .. max`, unlike the displayed numbers which run from
/// `1` to `max` inclusive.
#[derive(Clone, Debug)]
pub struct LabeledSpinner<'a> {
    label: &'a str,
    max: usize,
    value: usize,
}

impl<'a> LabeledSpinner<'a> {
    pub fn new(label: &'a str,
               max: usize,
               value: usize) -> LabeledSpinner<'a> {
        assert!(value < max);
        LabeledSpinner {
            label: label,
            max: max,
            value: value,
        }
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for LabeledSpinner<'a> {
    type Event = usize;

    fn min_size(&self, ctx: &Ctx) -> Point {
        let l = data().ui_card(Card::SpinnerButtonLeftUp).size();
        let r = data().ui_card(Card::SpinnerButtonRightUp).size();

        let text_base = ctx.text_size(self.label, Font::Default);
        let text_extra = ctx.text_space_width(Font::Default) +
            display_digits(self.max) as i32 * ctx.text_size("0", Font::Default).x;

        // 2px padding between the label and the buttons on each side.
        let w = l.x + 2 + text_base.x + text_extra + 2 + r.x;
        let h = cmp::max(l.y, text_base.y);
        Point::new(w, h)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let l_btn = Button::new("").style(ButtonStyle::SpinnerLeft);
        let r_btn = Button::new("").style(ButtonStyle::SpinnerRight);

        let spin_size = ctx.cur_bounds().size();

        let l_size = l_btn.min_size(ctx);
        let l_bounds = Rect::sized(l_size) +
            Point::new(0, (spin_size.y - l_size.y) / 2);

        let r_size = r_btn.min_size(ctx);
        let r_bounds = Rect::sized(r_size) +
            Point::new(spin_size.x - r_size.x, (spin_size.y - r_size.y) / 2);

        // Reminder: display values are `1` to `max`, not `0` to `max - 1`.
        let label = format!("{} {}", self.label, self.value + 1);
        let label_size = ctx.text_size(&label, Font::Default);
        // Extra +1 offset to Y makes it look better.
        let label_pos = Point::new((spin_size.x - label_size.x) / 2,
                                   (spin_size.y - label_size.y) / 2 + 1);

        ctx.with_bounds(l_bounds, |ctx| {
            l_btn.on_paint(ctx);
        });
        ctx.with_bounds(r_bounds, |ctx| {
            r_btn.on_paint(ctx);
        });
        ctx.draw_text_at(&label, Font::Default, label_pos);
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        match ctx.interp_key(evt) {
            Some(KeyInterp::FocusX(dx)) => {
                let step =
                    if dx < 0 {
                        self.max - (-dx) as usize % self.max
                    } else {
                        dx as usize % self.max
                    };
                UIResult::Event((self.value + step) % self.max)
            },

            _ => UIResult::Unhandled,
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let l_btn = Button::new("").style(ButtonStyle::SpinnerLeft);
        let r_btn = Button::new("").style(ButtonStyle::SpinnerRight);

        let spin_size = ctx.cur_bounds().size();

        let l_size = l_btn.min_size(ctx);
        let l_bounds = Rect::sized(l_size) +
            Point::new(0, (spin_size.y - l_size.y) / 2);

        let r_size = r_btn.min_size(ctx);
        let r_bounds = Rect::sized(r_size) +
            Point::new(spin_size.x - l_size.x, (spin_size.y - l_size.y) / 2);

        try_handle!(ctx.with_bounds(l_bounds, |ctx| {
            if ctx.mouse_target() {
                l_btn.on_mouse(ctx, evt).map(|()| {
                    (self.value + self.max - 1) % self.max
                })
            } else {
                UIResult::Unhandled
            }
        }));
        try_handle!(ctx.with_bounds(r_bounds, |ctx| {
            if ctx.mouse_target() {
                r_btn.on_mouse(ctx, evt).map(|()| {
                    (self.value + 1) % self.max
                })
            } else {
                UIResult::Unhandled
            }
        }));
        UIResult::Unhandled
    }
}


pub fn display_digits(n: usize) -> usize {
    if n < 10 { 1 }
    else if n < 100 { 2 }
    else if n < 1000 { 3 }
    else { format!("{}", n).len() }
}
