use outpost_ui::context::Void;
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;

use ui2::atlas::Border;
use ui2::context::Context;


pub struct HSep {
    left_ext: Option<u8>,
    right_ext: Option<u8>,
}

impl HSep {
    pub fn new() -> HSep {
        HSep {
            left_ext: None,
            right_ext: None,
        }
    }

    pub fn left_ext(self, ext: u8) -> HSep {
        Self {
            left_ext: Some(ext),
            .. self
        }
    }

    pub fn right_ext(self, ext: u8) -> HSep {
        Self {
            right_ext: Some(ext),
            .. self
        }
    }

    fn border_name(&self) -> Border {
        // `left_ext.is_none()` means we aren't connecting to anything on the left, so the left end
        // should be closed (`1`).
        match (self.left_ext.is_none(), self.right_ext.is_none()) {
            (false, false) => Border::SepHoriz00,
            (false, true) => Border::SepHoriz01,
            (true, false) => Border::SepHoriz10,
            (true, true) => Border::SepHoriz11,
        }
    }
}

impl<Ctx: Context> Widget<Ctx> for HSep {
    type Event = Void;

    fn min_size(&self, ctx: &Ctx) -> Point {
        ctx.border_min_size(self.border_name())
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let size = ctx.cur_bounds().size();
        let mut bounds = Rect::sized(size);

        bounds.min.x -= self.left_ext.unwrap_or(0) as i32;
        bounds.max.x += self.right_ext.unwrap_or(0) as i32;

        ctx.draw_border_at(self.border_name(), bounds);
    }
}


pub struct VSep {
    top_ext: Option<u8>,
    bottom_ext: Option<u8>,
}

impl VSep {
    pub fn new() -> VSep {
        VSep {
            top_ext: None,
            bottom_ext: None,
        }
    }

    pub fn top_ext(self, ext: u8) -> VSep {
        Self {
            top_ext: Some(ext),
            .. self
        }
    }

    pub fn bottom_ext(self, ext: u8) -> VSep {
        Self {
            bottom_ext: Some(ext),
            .. self
        }
    }

    fn border_name(&self) -> Border {
        match (self.top_ext.is_none(), self.bottom_ext.is_none()) {
            (false, false) => Border::SepVert00,
            (false, true) => Border::SepVert01,
            (true, false) => Border::SepVert10,
            (true, true) => Border::SepVert11,
        }
    }
}

impl<Ctx: Context> Widget<Ctx> for VSep {
    type Event = Void;

    fn min_size(&self, ctx: &Ctx) -> Point {
        ctx.border_min_size(self.border_name())
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let size = ctx.cur_bounds().size();
        let mut bounds = Rect::sized(size);

        bounds.min.y -= self.top_ext.unwrap_or(0) as i32;
        bounds.max.y += self.bottom_ext.unwrap_or(0) as i32;

        ctx.draw_border_at(self.border_name(), bounds);
    }
}


