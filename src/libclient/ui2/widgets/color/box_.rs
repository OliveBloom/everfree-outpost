use outpost_ui::event::{MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;

use ui2::atlas::{Border, Card};
use ui2::context::Context;


pub struct ColorBox {
    color: (u8, u8, u8),
    active: bool,
}

impl ColorBox {
    pub fn new(color: (u8, u8, u8), active: bool) -> ColorBox {
        ColorBox {
            color: color,
            active: active,
        }
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for ColorBox {
    type Event = ();

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(21, 21)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let border =
            if self.active { Border::ColorBoxActive }
            // `mouse_target` instead of `mouse_over` so we don't highlight when mouse is grabbed
            else if ctx.mouse_target() { Border::ColorBoxHover }
            else { Border::ColorBox };
        ctx.draw_border(border);

        let size = ctx.cur_bounds().size();
        if self.active {
            // Slight hack: this little arrow actually extends past `cur_bounds`.
            ctx.draw_card_at(Card::ColorBoxArrow, Point::new((size.x - 5) / 2, size.y));
        }

        let bounds = Rect::sized(ctx.cur_bounds().size()).inset(4, 4, 4, 4);
        ctx.with_bounds(bounds, |ctx| {
            ctx.draw_special_palette_color(&[self.color], 0);
        });
    }

    fn on_mouse(&self, _ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        match evt {
            MouseEvent::Down(_) => UIResult::Event(()),
            MouseEvent::Up(_) => UIResult::NoEvent,
            _ => UIResult::Unhandled,
        }
    }
}
