use std::cmp;

use outpost_ui::event::{MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;

use ui2::atlas::{Border, Font};
use ui2::context::{Context, Channel};
use util;


fn clamp_u8(x: f32) -> u8 {
    if x < 0.0 {
        return 0;
    }
    if x > 1.0 {
        return 255;
    }
    util::roundf(x * 255.) as u8
}


struct Slider {
    color: (u8, u8, u8),
    channel: Channel,
}

impl Slider {
    pub fn new(color: (u8, u8, u8), channel: Channel) -> Slider {
        Slider {
            color: color,
            channel: channel,
        }
    }
}

impl<Ctx: Context> Widget<Ctx> for Slider {
    type Event = (u8, u8, u8);

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(102, 12)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let size = ctx.cur_bounds().size();
        // Subtract 2 to ignore space occupied by descenders and the drop shadow.
        let text_height = ctx.text_line_height(Font::Default) - 2;
        let text_y = (size.y - (text_height - 2)) / 2;

        let label_width = ctx.text_size("R:", Font::Default).x;
        let number_max_width = ctx.text_size("999", Font::Default).x;
        let total_text_width = label_width + 2 + number_max_width;

        let label = match self.channel {
            Channel::Red => "R:",
            Channel::Green => "G:",
            Channel::Blue => "B:",
        };
        let label_x = size.x - total_text_width;
        ctx.draw_text_at(label, Font::Default, Point::new(label_x, text_y));

        let number = format!("{}", self.channel.get(self.color));
        let number_width = ctx.text_size(&number, Font::Default).x;
        let number_x = size.x - number_width;
        ctx.draw_text_at(&number, Font::Default, Point::new(number_x, text_y));


        let bounds = Rect::sized(ctx.cur_bounds().size()).inset(0, 2 + total_text_width, 0, 0);
        ctx.draw_border_at(Border::Frame, bounds);
        ctx.with_bounds(bounds.inset(1, 1, 1, 1), |ctx| {
            ctx.draw_special_color_slider(self.color, self.channel);
        });
    }

    fn on_mouse(&self, ctx: &mut Ctx, _evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let label_width = ctx.text_size("R:", Font::Default).x;
        let number_max_width = ctx.text_size("999", Font::Default).x;
        let total_text_width = label_width + 2 + number_max_width;

        let bounds = Rect::sized(ctx.cur_bounds().size()).inset(0, 2 + total_text_width, 0, 0);
        ctx.with_bounds(bounds.inset(1, 1, 1, 1), |ctx| {
            if !ctx.mouse_pressed_over() {
                return UIResult::Unhandled;
            }
            let pos = ctx.mouse_pos().unwrap();
            let size = ctx.cur_bounds().size();

            let new_val = clamp_u8(pos.x as f32 / (size.x - 1) as f32);
            let new_color = self.channel.set(self.color, new_val);
            UIResult::Event(new_color)
        })
    }
}


pub struct SliderColorPicker {
    color: (u8, u8, u8),
}

impl SliderColorPicker {
    pub fn new(color: (u8, u8, u8)) -> SliderColorPicker {
        SliderColorPicker {
            color: color,
        }
    }

    fn inner(&self, channel: Channel) -> Slider {
        Slider::new(self.color, channel)
    }

    fn bounds<Ctx: Context>(&self, ctx: &Ctx, channel: Channel) -> Rect {
        let size = ctx.cur_bounds().size();
        let idx0 = channel as i32;
        let idx1 = idx0 + 1;
        let y0 = (size.y + 2) * idx0 / 3;
        let y1 = (size.y + 2) * idx1 / 3;

        Rect::new(0, y0, size.x, y1 - 2)
    }
}

impl<Ctx: Context> Widget<Ctx> for SliderColorPicker {
    type Event = (u8, u8, u8);

    fn min_size(&self, ctx: &Ctx) -> Point {
        let s1 = self.inner(Channel::Red).min_size(ctx);
        let s2 = self.inner(Channel::Green).min_size(ctx);
        let s3 = self.inner(Channel::Blue).min_size(ctx);
        Point::new(cmp::max(cmp::max(s1.x, s2.x), s3.x),
                   s1.y + 2 + s2.y + 2 + s3.y)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        for &c in &[Channel::Red, Channel::Green, Channel::Blue] {
            let bounds = self.bounds(ctx, c);
            ctx.with_bounds(bounds, |ctx| {
                self.inner(c).on_paint(ctx);
            });
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        for &c in &[Channel::Red, Channel::Green, Channel::Blue] {
            let bounds = self.bounds(ctx, c);
            try_handle!(ctx.with_bounds(bounds, |ctx| {
                if ctx.mouse_target() {
                    self.inner(c).on_mouse(ctx, evt)
                } else {
                    UIResult::Unhandled
                }
            }));
        }

        UIResult::Unhandled
    }
}
