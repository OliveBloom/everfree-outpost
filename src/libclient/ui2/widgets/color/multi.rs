use std::cell::Cell;

use colorspace::LchUv;
use outpost_ui::event::{KeyEvent, MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;

use ui2::context::Context;

use super::plot::PlotColorPicker;
use super::slider::SliderColorPicker;
use super::swatch::{SwatchColorPicker, Color};


#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
pub enum Mode {
    Swatch,
    Plot,
    Slider,
}

pub struct MultiColorPicker<'a> {
    color: (u8, u8, u8),
    mode: Mode,

    // Swatch picker state
    palette: &'a [Color],

    // Plot picker state
    precise: &'a Cell<LchUv>,
}

impl<'a> MultiColorPicker<'a> {
    pub fn new(color: (u8, u8, u8),
               mode: Mode,
               palette: &'a [Color],
               precise: &'a Cell<LchUv>) -> MultiColorPicker<'a> {
        MultiColorPicker {
            color: color,
            mode: mode,
            palette: palette,
            precise: precise,
        }
    }

    fn with_inner<Ctx, F, R>(&self, f: F) -> R
            where Ctx: Context,
                  F: FnOnce(&Widget<Ctx, Event=(u8, u8, u8)>) -> R {
        match self.mode {
            Mode::Swatch =>
                f(&SwatchColorPicker::new(self.palette)),
            Mode::Plot =>
                f(&PlotColorPicker::new(self.color, self.precise)),
            Mode::Slider =>
                f(&SliderColorPicker::new(self.color)),
        }
    }

    fn bounds<Ctx: Context>(&self, ctx: &Ctx) -> Rect {
        let full = Rect::sized(ctx.cur_bounds().size());
        match self.mode {
            Mode::Swatch =>
                self.with_inner(|w| full.center(Rect::sized(w.min_size(ctx)))),
            Mode::Plot => full,
            Mode::Slider => {
                let size = self.with_inner(|w| w.min_size(ctx));
                let y = (full.size().y - size.y) / 2;
                Rect::sized(Point::new(full.size().x, size.y)) + Point::new(0, y)
            },
        }
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for MultiColorPicker<'a> {
    type Event = (u8, u8, u8);

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.with_inner(|w| w.min_size(ctx))
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let bounds = self.bounds(ctx);
        self.with_inner(|w| ctx.with_bounds(bounds, |ctx| w.on_paint(ctx)));
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        let bounds = self.bounds(ctx);
        self.with_inner(|w| ctx.with_bounds(bounds, |ctx| w.on_key(ctx, evt)))
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let bounds = self.bounds(ctx);
        self.with_inner(|w| ctx.with_bounds(bounds, |ctx| w.on_mouse(ctx, evt)))
    }
}
