use std::marker::PhantomData;

use outpost_ui::event::{MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::geom::{Direction, Vertical, Horizontal};
use outpost_ui::widget::Widget;

use ui2::atlas::Card;
use ui2::context::Context;


/// Invariants:
///
///  * `value` is in the range `0 .. icons.len()`.
///  * Emitted events are in the same range.
#[derive(Clone, Debug)]
pub struct ToggleRow<'a, D> {
    icons: &'a [Card],
    value: usize,
    _marker: PhantomData<D>,
}

impl<'a> ToggleRow<'a, Horizontal> {
    pub fn new(icons: &'a [Card],
               value: usize) -> ToggleRow<'a, Horizontal> {
        assert!(icons.len() >= 2);
        assert!(value < icons.len());
        ToggleRow {
            icons: icons,
            value: value,
            _marker: PhantomData,
        }
    }
}

impl<'a> ToggleRow<'a, Vertical> {
    pub fn vert(icons: &'a [Card],
                value: usize) -> ToggleRow<'a, Vertical> {
        assert!(icons.len() >= 2);
        assert!(value < icons.len());
        ToggleRow {
            icons: icons,
            value: value,
            _marker: PhantomData,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Pos {
    Start,
    Mid,
    End,
}

pub trait ToggleRowDirection: Direction {
    fn card(pos: Pos, down: bool) -> Card;
}

impl ToggleRowDirection for Horizontal {
    fn card(pos: Pos, down: bool) -> Card {
        match (down, pos) {
            (false, Pos::Start) => Card::ToggleRowNormalLeft,
            (false, Pos::Mid) => Card::ToggleRowNormalMid,
            (false, Pos::End) => Card::ToggleRowNormalRight,
            (true, Pos::Start) => Card::ToggleRowDownLeft,
            (true, Pos::Mid) => Card::ToggleRowDownMid,
            (true, Pos::End) => Card::ToggleRowDownRight,
        }
    }
}

impl ToggleRowDirection for Vertical {
    fn card(pos: Pos, down: bool) -> Card {
        match (down, pos) {
            (false, Pos::Start) => Card::ToggleRowVertNormalTop,
            (false, Pos::Mid) => Card::ToggleRowVertNormalMid,
            (false, Pos::End) => Card::ToggleRowVertNormalBottom,
            (true, Pos::Start) => Card::ToggleRowVertDownTop,
            (true, Pos::Mid) => Card::ToggleRowVertDownMid,
            (true, Pos::End) => Card::ToggleRowVertDownBottom,
        }
    }
}

impl<'a, Ctx: Context, D: ToggleRowDirection> Widget<Ctx> for ToggleRow<'a, D> {
    type Event = usize;

    fn min_size(&self, ctx: &Ctx) -> Point {
        let s = ctx.card_size(D::card(Pos::Start, false));
        let m = ctx.card_size(D::card(Pos::Mid, false));
        let e = ctx.card_size(D::card(Pos::End, false));

        let major = s.x + m.x * (self.icons.len() - 2) as i32 + e.x;
        let minor = s.y;
        D::make_point(major, minor)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let s_maj = D::major(ctx.card_size(D::card(Pos::Start, false)));
        let m_maj = D::major(ctx.card_size(D::card(Pos::Mid, false)));

        for (i, &icon) in self.icons.iter().enumerate() {
            let pos =
                if i == 0 { Pos::Start }
                else if i == self.icons.len() - 1 { Pos::End }
                else { Pos::Mid };

            let maj = if i == 0 { 0 } else { s_maj + m_maj * (i - 1) as i32 };
            let size = ctx.card_size(D::card(pos, false));
            let bounds = Rect::sized(size) + D::make_point(maj, 0);

            ctx.with_bounds(bounds, |ctx| {
                let down = self.value == i ||
                    (ctx.mouse_pressed_over() && ctx.mouse_over());
                ctx.draw_card(D::card(pos, down));
                // TODO: hardcoded icon offsets (for 8x8 icons)
                let icon_pos = if down { Point::new(5, 5) } else { Point::new(4, 4) };
                ctx.draw_card_at(icon, icon_pos);
            });
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        match evt {
            MouseEvent::Down(_) => return UIResult::NoEvent,
            MouseEvent::Move |
            MouseEvent::Wheel(_) => return UIResult::Unhandled,
            MouseEvent::Up(_) => {},
        }

        let s_maj = D::major(ctx.card_size(D::card(Pos::Start, false)));
        let m_maj = D::major(ctx.card_size(D::card(Pos::Mid, false)));

        let mut result = UIResult::NoEvent;

        for i in 0 .. self.icons.len() {
            let pos =
                if i == 0 { Pos::Start }
                else if i == self.icons.len() - 1 { Pos::End }
                else { Pos::Mid };

            let maj = if i == 0 { 0 } else { s_maj + m_maj * (i - 1) as i32 };
            let size = ctx.card_size(D::card(pos, false));
            let bounds = Rect::sized(size) + D::make_point(maj, 0);

            ctx.with_bounds(bounds, |ctx| {
                if ctx.mouse_pressed_over() {
                    result = UIResult::Event(i);
                }
            });
        }

        result
    }
}


