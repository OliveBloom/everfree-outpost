use std::prelude::v1::*;
use outpost_ui::context::Void;
use outpost_ui::geom::Point;
use outpost_ui::widget::Widget;

use debug::{self, DisplayMode};
use ui2::atlas::Font;
use ui2::context::Context;


pub struct Debug<'a> {
    debug: &'a debug::Debug,
}

impl<'a> Debug<'a> {
    pub fn new(debug: &'a debug::Debug) -> Debug<'a> {
        Debug {
            debug: debug,
        }
    }
}

const LABEL_WIDTH: i32 = 32;
const ROW_WIDTH: i32 = 128;
const ROW_HEIGHT: i32 = 10;

const NUM_ROWS: i32 = 17;

impl<'a, Ctx: Context> Widget<Ctx> for Debug<'a> {
    type Event = Void;

    fn min_size(&self, _ctx: &Ctx) -> Point {
        // TODO: add graph height
        Point::new(ROW_WIDTH, ROW_HEIGHT * NUM_ROWS)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let bounds_size = ctx.cur_bounds().size();

        match self.debug.mode {
            DisplayMode::None => {},

            DisplayMode::Framerate => {
                let rate = calc_framerate(self.debug.total_interval);
                let s = format!("{}.{} FPS", rate / 10, rate % 10);
                let size = ctx.text_size(&s, Font::Default);
                let offset = bounds_size.x - size.x;
                let pos = Point::new(offset, 0);
                ctx.draw_text_at(&s, Font::Default, pos)
            },

            DisplayMode::Full => {
                let mut pos = Point::new(0, 0);
                let value_off = Point::new(LABEL_WIDTH, 0);

                {
                    let mut row = |label: &str, value: String| {
                        ctx.draw_text_at(label, Font::Default, pos);
                        ctx.draw_text_at(&value, Font::Default, pos + value_off);
                        pos.y += ROW_HEIGHT;
                    };

                    let rate = calc_framerate(self.debug.total_interval);
                    row("FPS", format!("{}.{}", rate / 10, rate % 10));
                    row("Timing", format!("-{}/+{}/+{} ms",
                                          self.debug.timing_mid - self.debug.timing_low,
                                          self.debug.timing_high - self.debug.timing_mid,
                                          self.debug.timing_highest - self.debug.timing_mid));
                    row("Timing", format!("{} samp, D={} ({} ticks)",
                                          self.debug.timing_samples,
                                          self.debug.timing_delta,
                                          self.debug.timing_skew_ticks));
                    row("Pos", format!("{:?}", self.debug.pos));
                    row("Time", format!("{}.{:03} ({})",
                                        self.debug.day_time / 1000,
                                        self.debug.day_time % 1000,
                                        self.debug.day_phase));
                    row("Mouse", format!("{:?} @ {:?} {}",
                                         self.debug.mouse_hit_structure,
                                         self.debug.mouse_hit_pos,
                                         if self.debug.mouse_near_pawn { "(near)" }
                                         else { "" }));
                    row("Mem", format!("{}k free in {} fragments",
                                       self.debug.mem_free / 1024,
                                       self.debug.mem_frags));
                    row("Pend", format!("{} msgs, {} ms",
                                        self.debug.state_pending_count,
                                        self.debug.state_pending_time));
                    row("Pre", format!("{}/{} ({}ms), D={}ms",
                                       self.debug.predict_acknowledged,
                                       self.debug.predict_pending,
                                       self.debug.predict_next_pending_delta,
                                       self.debug.predict_last_ack_delay));
                    // NB: Update NUM_ROWS if you add a new line here.

                    let ft = self.debug.average_frame_times();
                    row("", format!(""));
                    row("Fr.Tms", format!("Advance: {}ms", ft.advance));
                    row("", format!("Predict: {}ms", ft.predict));
                    row("", format!("Prepare: {}ms", ft.prepare));
                    row("", format!("Render: {}ms", ft.render));
                    row("", format!("Display: {}ms", ft.display));
                    row("", format!("Total: {}ms", ft.total));
                    row("", format!("Interval: {}ms", ft.interval));
                }

                // TODO: graph
            },
        }
    }
}

/// Calculate the framerate in 1/10s of FPS, based on `Debug::total_interval`.
fn calc_framerate(total_interval: usize) -> usize {
    if total_interval == 0 {
        return 0;
    }

    // total_interval is in milliseconds.  rate is in 1/10s of FPS.
    debug::NUM_FRAMES * 10 * 1000 / total_interval
}
