use common::types::*;
use std::cmp;
use std::ops::Deref;

use outpost_ui::context::Void;
use outpost_ui::geom::Point;
use outpost_ui::geom::Vertical;
use outpost_ui::widget::Widget;
use outpost_ui::widgets::container::{Group, ChildWidget, Align, GenWidgets, Contents};
use outpost_ui::widgets::text::{FixedLabel, WrappedLabel};

use data::data;
use inv_changes;
use ui2::context::Context;
use ui2::widgets::item;


// ItemDisplay is 16px, so that's enough for a quantity_string.  Then add 4px for the +/-.
const COUNT_WIDTH: i32 = 20;

/// Max item name width we can handle without wrapping.
const MAX_TEXT_WIDTH: i32 = 100;


pub struct Entry<'a> {
    pub entry: &'a inv_changes::Entry,
}

impl<'a> Entry<'a> {
    pub fn new(entry: &'a inv_changes::Entry) -> Entry<'a> {
        Entry { entry }
    }

    fn with_inner<Ctx, CtxRef, F, R>(&self, ctx: CtxRef, f: F) -> R
        where Ctx: Context,
              CtxRef: Deref<Target=Ctx>,
              F: FnOnce(CtxRef, &Widget<Ctx, Event=()>) -> R {
        let data = data();

        let sign = if self.entry.count >= 0 { '+' } else { '-' };
        let count_str_raw = item::quantity_string(self.entry.count.abs() as u16);
        let count_str = format!("{}{}", sign, count_str_raw);
        let name = data.item_def(self.entry.item).ui_name();
        let unwrapped_name_width = ctx.text_size(&name, Ctx::TextStyle::default()).x;
        let name_width = cmp::min(unwrapped_name_width, MAX_TEXT_WIDTH);

        let w = Group::horiz(contents![
            ChildWidget::new(
                FixedLabel::new(&count_str, COUNT_WIDTH).align(Align::End), |()| ())
                .align(Align::Center),
            ChildWidget::new(item::Item::new(self.entry.item, None), |()| ())
                .align(Align::Center),
            ChildWidget::new(WrappedLabel::new(name, name_width), |()| ())
                .align(Align::Center),
        ]).spacing(2);

        f(ctx, &w)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for Entry<'a> {
    type Event = Void;

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.with_inner(ctx, |ctx, w| w.min_size(ctx))
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.with_inner(ctx, |ctx, w| w.on_paint(ctx))
    }
}


pub struct InvChanges<'a> {
    now: Time,
    inv_changes: &'a inv_changes::InvChanges,
}

impl<'a> InvChanges<'a> {
    pub fn new(now: Time,
               inv_changes: &'a inv_changes::InvChanges) -> InvChanges<'a> {
        InvChanges { now, inv_changes }
    }

    fn inner<Ctx: Context+'a>(&self) ->
            Group<'static, Ctx, Vertical, Void, impl Contents<Ctx, Void>+'a> {
        let items = self.inv_changes.iter()
            .filter(|e| e.time >= self.now - inv_changes::DISPLAY_TIME)
            .collect::<Vec<_>>();
        let contents = GenWidgets::new(0 .. items.len(), move |idx| {
            ChildWidget::new(Entry::new(items[idx]), move |v| v).align(Align::End)
        });
        Group::vert(contents)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for InvChanges<'a> {
    type Event = usize;

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.inner().min_size(ctx)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        trace!("paint at {:?} - {} changes", ctx.cur_bounds(), self.inv_changes.len());
        self.inner().on_paint(ctx);
    }
}
