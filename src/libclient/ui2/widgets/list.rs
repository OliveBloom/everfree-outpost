use std::cell::Cell;

use outpost_ui::context::Focus;
use outpost_ui::event::{KeyEvent, KeyInterp, MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::geom::Vertical;
use outpost_ui::widget::Widget;
use outpost_ui::widgets::container::{Group, ChildWidget, GenWidgets, Contents};
use outpost_ui::widgets::scroll::ScrollPane;
use outpost_ui::widgets::text::WrappedLabel;

use ui2::atlas::{Card, Border};
use ui2::context::Context;


pub struct TextListItem<'a, Ctx: Context> {
    pub text: &'a str,
    pub width: i32,
    pub style: Ctx::TextStyle,
}

impl<'a, Ctx: Context> TextListItem<'a, Ctx> {
    pub fn new(text: &'a str, width: i32) -> TextListItem<'a, Ctx> {
        TextListItem {
            text: text,
            width: width,
            style: Ctx::TextStyle::default(),
        }
    }

    pub fn style(self, style: Ctx::TextStyle) -> Self {
        TextListItem {
            style: style,
            .. self
        }
    }

    fn inner(&self) -> WrappedLabel<'a, Ctx> {
        WrappedLabel::new(self.text, self.width - 2 - 2).style(self.style)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for TextListItem<'a, Ctx> {
    type Event = ();

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.inner().min_size(ctx) + Point { x: 9 + 2, y: 2 + 2 }
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let size = ctx.cur_bounds().size();
        ctx.draw_border(Border::ScrollListEntry);

        if ctx.state().focus >= Focus::Semiactive {
            ctx.draw_card_at(Card::ScrollListMarker, Point::new(2, 2));
        }

        let text_bounds = Rect::sized(size).inset(9, 2, 2, 2);
        ctx.with_bounds(text_bounds, |ctx| {
            self.inner().on_paint(ctx);
        });
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        match ctx.interp_key(evt) {
            Some(KeyInterp::Activate) => UIResult::Event(()),
            _ => UIResult::Unhandled,
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        match evt {
            MouseEvent::Down(_) => UIResult::NoEvent,
            MouseEvent::Up(_) => {
                if ctx.mouse_pressed_over() {
                    UIResult::Event(())
                } else {
                    UIResult::NoEvent
                }
            },
            _ => UIResult::Unhandled,
        }
    }
}


pub struct TextList<'s, 'a> {
    pub top: &'s Cell<i32>,
    pub focus: &'s Cell<usize>,
    pub items: &'a [String],
    pub size: Point,
}

impl<'s, 'a> TextList<'s, 'a> {
    fn inner<Ctx: Context+'a>(&self) ->
            ScrollPane<'s, Ctx,
                Group<'s, Ctx, Vertical, usize,
                    impl Contents<Ctx, usize>+'a>> {
        let items = self.items;
        let width = self.size.x;
        let contents = GenWidgets::new(0 .. items.len(), move |idx| {
            ChildWidget::new(TextListItem::new(&items[idx], width), move |()| idx)
        });
        ScrollPane::new(self.top, self.size, Group::vert(contents).focus(self.focus))
    }
}

impl<'s, 'a, Ctx: Context> Widget<Ctx> for TextList<'s, 'a> {
    type Event = usize;

    fn min_size(&self, _ctx: &Ctx) -> Point {
        self.size
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.inner().on_paint(ctx);
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        self.inner().on_key(ctx, evt)
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.inner().on_mouse(ctx, evt)
    }
}
