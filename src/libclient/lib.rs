#![crate_name = "client"]

#![feature(
    // Have to use box syntax because Box::new() in Client::new() tries to allocate 1.5MB of
    // temporary arrays on the stack.
    box_syntax,
    core_intrinsics,
    )]

#[macro_use] extern crate bitflags;
#[macro_use] extern crate log;

extern crate client_data;
extern crate client_fonts;
extern crate colorspace;
extern crate common;
extern crate physics;
extern crate render;
#[macro_use] extern crate ui as outpost_ui;

pub use client::Client;
pub use data::Data;

#[macro_use] pub mod platform;

mod types;

pub mod client;
pub mod data;
mod fonts;

pub mod util;

mod hotbar;
mod inv_changes;
mod misc;
mod debug;
mod timing;
mod input;
mod mouse;
pub mod state;
pub mod predictor;
mod movement;
mod cache;

pub mod graphics;
pub mod ui2;

pub use common::types::Time;

pub mod test {
    pub use cache::terrain_shape::Cache as TerrainShape;
}
