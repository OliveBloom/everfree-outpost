extern crate common;

use std::fmt;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};
use std::u16;
use common::util::ByteCast;
use common::util::btree_multimap::Bounded;

pub use common::v3::*;
pub use common::types::*;


// Stable IDs, for use with StableIdMap.

pub type StableId = u64;

pub const NO_STABLE_ID: StableId = 0;

/// A wrapper around StableId that prevents mixing StableIds for different types of resources.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Stable<Id> {
    pub val: StableId,
    pub _marker0: PhantomData<Id>,
}

macro_rules! const_Stable {
    ($val:expr) => {
        $crate::Stable { val: $val, _marker0: ::std::marker::PhantomData }
    };
}

impl<Id> Stable<Id> {
    // TODO: remove.  `Stable` instances should always me nonzero.
    pub fn none() -> Stable<Id> {
        Stable {
            val: NO_STABLE_ID,
            _marker0: PhantomData,
        }
    }

    pub fn new(val: StableId) -> Stable<Id> {
        Stable {
            val: val,
            _marker0: PhantomData,
        }
    }

    pub fn new_checked(val: StableId) -> Option<Stable<Id>> {
        if val == NO_STABLE_ID {
            None
        } else {
            Some(Stable::new(val))
        }
    }

    pub fn unwrap(self) -> StableId {
        self.val
    }
}

unsafe impl<Id: Copy> ByteCast for Stable<Id> {}

impl<Id> fmt::Debug for Stable<Id> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.debug_tuple("Stable")
            .field(&self.val)
            .finish()
    }
}


// Other server-only ID types

mk_id_newtypes! {
    WireId(u16);
}

impl Bounded for WireId {
    fn min_bound() -> WireId { WireId(Bounded::min_bound()) }
    fn max_bound() -> WireId { WireId(Bounded::max_bound()) }
}


// Well-known newtype ID values.
pub const STABLE_PLANE_LIMBO: Stable<PlaneId> = const_Stable!(1);
pub const STABLE_PLANE_FOREST: Stable<PlaneId> = const_Stable!(2);
pub const CONTROL_WIRE_ID: WireId = WireId(0);


// Chunks

pub const CHUNK_TOTAL: usize = 1 << (3 * CHUNK_BITS);
#[derive(Clone, Copy)]
pub struct BlockChunk(pub [BlockId; CHUNK_TOTAL]);

impl fmt::Debug for BlockChunk {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        <[BlockId] as fmt::Debug>::fmt(&self.0, f)
    }
}

impl Deref for BlockChunk {
    type Target = [BlockId];
    fn deref(&self) -> &Self::Target { &self.0 }
}

impl DerefMut for BlockChunk {
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.0 }
}

// 0 is always the BlockId of "empty" (no appearance; empty shape)
pub static EMPTY_CHUNK: BlockChunk = BlockChunk([0; CHUNK_TOTAL]);
// 1 is always the BlockId of "placeholder" (no appearance; solid shape)
pub static PLACEHOLDER_CHUNK: BlockChunk = BlockChunk([1; CHUNK_TOTAL]);
