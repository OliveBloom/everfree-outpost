//! Helper data structure for collecting multiple `StructureAppear` events into a single
//! `StructureAppearMulti`.

use server_types::*;
use std::collections::{HashMap, HashSet};
use std::u8;
use common::proto::types::CondensedStructure;

/// Buffer of structure changes, for collecting a sequence of StructureAppear / StructureGone
/// updates into a small number of multi-update messages.  Note that this only works on contiguous
/// sequences of Appear / Gone messages, with nothing else in between.  Be sure to flush the buffer
/// before processing any other type of message (including StructureReplace).
///
/// For a single ID, `Appear, Gone` is a no-op, and `Appear, Appear` and `Gone, Gone` are both
/// impossible.  `Gone, Appear` (when a structure is removed and its ID is immediately reused) is
/// the only interesting multi-update sequence that must be preserved.  Thus, the only possible
/// outcomes for a given ID (after collapsing no-ops) are `Appear`, `Gone`, and `Gone, Appear`.
/// And events for different IDs may be reordered arbitrarily, so we can safely reorder any
/// sequence of updates into a sequence of `Gone` messages followed by a sequence of `Appear`
/// messages.
pub struct Buffer {
    gone: HashSet<StructureId>,
    appear: HashMap<StructureId, (TemplateId, (u8, u8, u8))>,
}

impl Buffer {
    pub fn new() -> Buffer {
        Buffer {
            gone: HashSet::new(),
            appear: HashMap::new(),
        }
    }

    pub fn appear(&mut self, id: StructureId, template: TemplateId, pos: (u8, u8, u8)) {
        self.appear.insert(id, (template, pos));
    }

    pub fn gone(&mut self, id: StructureId) {
        // Cancel out a preceding `Appear` for this ID, if there is one.
        if self.appear.remove(&id).is_some() {
            return;
        }
        // Otherwise, add a `Gone`.
        self.gone.insert(id);
    }

    pub fn flush_gone<F>(&mut self, mut callback: F)
            where F: FnMut(StructureId, Vec<u8>) {
        if self.gone.len() == 0 {
            return;
        }

        let mut ids = self.gone.drain().collect::<Vec<_>>();
        ids.sort();

        let mut base_id = StructureId(0);
        let mut prev_id = base_id;
        let mut rel_ids = Vec::new();

        for id in ids {
            if id.unwrap() > prev_id.unwrap() + u8::MAX as u32 {
                if rel_ids.len() > 0 {
                    callback(base_id, rel_ids);
                }
                base_id = id;
                prev_id = id;
                rel_ids = Vec::new();
            }

            rel_ids.push((id.unwrap() - prev_id.unwrap()) as u8);
            prev_id = id;
        }

        if rel_ids.len() > 0 {
            callback(base_id, rel_ids);
        }
    }

    pub fn flush_appear<F>(&mut self, mut callback: F)
            where F: FnMut(StructureId, TemplateId, Vec<CondensedStructure>) {
        if self.appear.len() == 0 {
            return;
        }

        let mut structures = self.appear.drain().map(|(s, (t, p))| (s, t, p)).collect::<Vec<_>>();
        structures.sort_by_key(|&(s, t, _)| (t, s));

        let mut base_id = StructureId(0);
        let mut prev_id = base_id;
        let mut cur_template = 0;
        let mut group = Vec::new();

        for (id, template, pos) in structures {
            if template != cur_template || id.unwrap() > prev_id.unwrap() + u8::MAX as u32 {
                if group.len() > 0 {
                    callback(base_id, cur_template, group);
                }
                base_id = id;
                prev_id = id;
                cur_template = template;
                group = Vec::new();
            }

            group.push(CondensedStructure {
                rel_id: (id.unwrap() - prev_id.unwrap()) as u8,
                pos: pos,
            });
            prev_id = id;
        }

        if group.len() > 0 {
            callback(base_id, cur_template, group);
        }
    }
}
