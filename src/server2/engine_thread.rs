use server_types::*;
use std::path::Path;
use std::sync::Arc;
use std::sync::mpsc::{self, Sender, Receiver};

use engine::{self, Engine};
use engine::chat;
use engine::ext::{Ext, FullWorld};
use engine::update::{Update, UpdateKey};
use server_bundle::types::Bundle;
use server_config::{with_tls_data, Data};

use conn::ToConn;
use messages::{ClientRequest, ClientResponse};
use save::ToSave;
use stats::EngineStats;
use vision::{self, ToVision};


pub enum ToEngine {
    ClientReq(ClientId, ClientRequest),
    UserConnect(u32, String),
    UserDisconnect(u32),

    ClientBundle(u32, Box<Bundle>),
    PlaneBundle(Stable<PlaneId>, Box<Bundle>),
    TerrainChunkBundle(Stable<PlaneId>, V2, Box<Bundle>),

    Tick,
    GetStats(Sender<EngineStats>),
    IPython(Box<String>, Sender<String>),
}


struct EngineExt {
    conn: Sender<ToConn>,
    vision: Sender<ToVision>,
    save: Sender<ToSave>,
}

impl EngineExt {
    fn send_vision(&mut self, msg: ToVision) {
        self.vision.send(msg).unwrap();
    }

    fn send_conn(&mut self, msg: ToConn) {
        self.conn.send(msg).unwrap();
    }

    fn send_resp(&mut self, cid: ClientId, msg: ClientResponse) {
        self.send_vision(ToVision::RelayResp(cid, msg));
    }

    fn send_save(&mut self, msg: ToSave) {
        self.save.send(msg).unwrap();
    }
}

impl Ext for EngineExt {
    fn handle_update(&mut self, w: &FullWorld, u: Update) {
        use engine::update::flags::*;

        let mut keys = u.keys().collect::<Vec<_>>();
        keys.sort();
        let keys = keys;

        if keys.len() > 0 {
            trace!("replay updates for {} keys", keys.len());
        }

        for k in keys {
            macro_rules! if_flag {
                ($f:ident, $flag:expr, $call:expr) => {
                    if $f.contains($flag) {
                        $call;
                    }
                };
            }

            match k {
                UpdateKey::World => {
                    // Nothing to do
                },

                UpdateKey::Client(_id) => {},

                UpdateKey::Entity(_id) => {},

                UpdateKey::Inventory(_id) => {},

                UpdateKey::Plane(id) => {
                    let f = u.plane_flags(id);
                    if_flag!(f, P_CREATED | P_DESTROYED, continue);
                    if f.contains(P_DESTROYED) {
                        // FIXME: old_plane_stable_id
                        let stable_id = u.new_plane(id).stable_id().unwrap();
                        self.send_save(ToSave::DestroyPlane(stable_id));
                        continue;
                    }

                    let p = w.plane(id);

                    if f.contains(P_CREATED) {
                        // All planes have stable IDs - see World::create_plane
                        let stable_id = p.stable_id().unwrap();
                        let mode = if stable_id == STABLE_PLANE_FOREST { 0 } else { 1 };
                        self.send_save(ToSave::CreatePlane(stable_id, mode));
                    }
                },

                UpdateKey::TerrainChunk(_id) => {},

                UpdateKey::Structure(_id) => {},
            }
        }

        let u = Arc::new(u);
        self.send_save(ToSave::Delta(w.now, u.clone()));
        self.send_vision(ToVision::Delta(w.now, u));
    }


    fn request_client_bundle(&mut self, uid: u32, name: &str) {
        self.send_save(ToSave::LoadClient(uid, name.to_owned()));
    }

    fn request_plane_bundle(&mut self, stable_id: Stable<PlaneId>) {
        self.send_save(ToSave::LoadPlane(stable_id));
    }

    fn request_terrain_chunk_bundle(&mut self, plane: Stable<PlaneId>, cpos: V2) {
        self.send_save(ToSave::LoadTerrainChunk(plane, cpos));
    }


    fn client_world_info(&mut self,
                         id: ClientId,
                         now: Time,
                         day_night_cycle_ms: u32) {
        trace!("client_world_info {} {}", now, day_night_cycle_ms);
        self.send_resp(id, ClientResponse::WorldInfo(now, day_night_cycle_ms));
    }

    fn client_readiness(&mut self, _id: ClientId, _readiness: bool) {
        // TODO: remove this method - handle readiness/loading screens on the client side instead
    }


    fn client_loaded(&mut self, uid: u32, cid: ClientId) {
        self.send_conn(ToConn::ClientLoaded(uid, cid));
    }

    fn client_unloaded(&mut self, uid: u32) {
        self.send_conn(ToConn::ClientUnloaded(uid));
    }

    fn kick_user(&mut self, uid: u32, reason: &str) {
        self.send_conn(ToConn::KickUser(uid, reason.to_owned()));
    }


    fn chat_message(&mut self, channel: chat::Channel, name: &str, text: &str) {
        self.send_vision(ToVision::ChatMessage(Box::new(vision::ChatMessage {
            channel: channel,
            name: name.to_owned(),
            text: text.to_owned(),
        })));
    }


    fn ack_request(&mut self, cid: ClientId, apply_time: Time) {
        self.send_resp(cid, ClientResponse::AckRequest(apply_time));
    }

    fn nak_request(&mut self, cid: ClientId) {
        self.send_resp(cid, ClientResponse::NakRequest);
    }
}

fn handle(eng: &mut Engine<EngineExt>,
          msg: ToEngine) {
    use self::ToEngine::*;
    match msg {
        ClientReq(cid, req) => {
            trace!("req: {:?} {:?}", cid, req);
            match req {
                ClientRequest::InputStart(delay, dir, speed) => {
                    engine::movement::on_queue_start(eng, cid, delay, dir, speed);
                },
                ClientRequest::InputChange(rel_time, dir, speed) => {
                    engine::movement::on_queue_change(eng, cid, rel_time, dir, speed);
                },
                ClientRequest::Interact(pos) => {
                    engine::input::on_point_interact(eng, cid, pos);
                },
                ClientRequest::Destroy(pos) => {
                    engine::input::on_point_destroy(eng, cid, pos);
                },
                ClientRequest::UseItem(pos, item) => {
                    engine::input::on_point_use_item(eng, cid, pos, item);
                },
                ClientRequest::ConnReady => {
                    engine::lifecycle::client_ready(eng, cid);
                },
                ClientRequest::Chat(msg) => {
                    engine::chat::on_chat(eng, cid, &msg);
                },
                ClientRequest::DialogOpen(dialog) => {
                    engine::component::dialog::on_request_open(eng, cid, dialog);
                },
                ClientRequest::DialogCancel => {
                    engine::component::dialog::on_cancel(eng, cid);
                },
                ClientRequest::DialogAction(act) => {
                    engine::component::dialog::on_action(eng, cid, *act);
                },
            }
        },


        UserConnect(uid, name) => {
            engine::lifecycle::user_connect(eng, uid, name);
            // Eventually calls `client_loaded` (or `kick_user`, on error)
        },

        UserDisconnect(uid) => {
            engine::lifecycle::user_disconnect(eng, uid);
            // Eventually calls `client_unloaded`
        },


        ClientBundle(uid, bundle) => {
            let mut i = ::save::import::Importer::new(&bundle);
            let delta = match i.import_bundle() {
                Ok(x) => x,
                Err(e) => {
                    error!("error converting client bundle for 0x{:x}: {}",
                           uid, e);
                    return;
                },
            };
            engine::lifecycle::start_import(eng, delta);
        },

        PlaneBundle(stable_id, bundle) => {
            let mut i = ::save::import::Importer::new(&bundle);
            let delta = match i.import_bundle() {
                Ok(x) => x,
                Err(e) => {
                    error!("error converting plane bundle {:?}: {}",
                           stable_id, e);
                    return;
                },
            };
            engine::lifecycle::start_import(eng, delta);
        },

        TerrainChunkBundle(stable_plane, cpos, bundle) => {
            let mut i = ::save::import::Importer::new(&bundle);
            let delta = match i.import_bundle() {
                Ok(x) => x,
                Err(e) => {
                    error!("error converting terrain chunk bundle {:?} {:?}: {}",
                           stable_plane, cpos, e);
                    return;
                },
            };
            engine::lifecycle::start_import(eng, delta);
        },


        Tick => {
            engine::tick::on_tick(eng);
            // `on_tick` always calls `handle_update`, even if the update is empty.  So we can send
            // the new engine time along with the `Delta`, instead of in a separate message.
        },

        GetStats(dest) => {
            dest.send(get_stats(eng)).unwrap();
        },

        IPython(req, dest) => {
            let resp = engine::script::on_ipython(eng, *req);
            dest.send(resp).unwrap();
        },
    }
}

fn get_stats(eng: &Engine<EngineExt>) -> EngineStats {
    EngineStats {
        now: eng.now,

        num_clients: eng.w.clients_len(),
        num_entities: eng.w.entities_len(),
        num_inventories: eng.w.inventories_len(),
        num_planes: eng.w.planes_len(),
        num_terrain_chunks: eng.w.terrain_chunks_len(),
        num_structures: eng.w.structures_len(),

        client_list: eng.conn_map.names()
            .map(|s| s.to_owned().into_boxed_str())
            .collect::<Vec<_>>().into_boxed_slice(),
    }
}

pub fn run_engine(data: Arc<Data>,
                  recv: Receiver<ToEngine>,
                  to_conn: Sender<ToConn>,
                  to_vision: Sender<ToVision>,
                  to_save: Sender<ToSave>) {
    // Load initial delta from the save thread.  This is a blocking operation.
    let (resp_send, resp_recv) = mpsc::channel();
    to_save.send(ToSave::LoadWorld(resp_send)).unwrap();
    let world_bundle = resp_recv.recv().unwrap();
    let mut i = ::save::import::Importer::new(&world_bundle);
    let init_delta = i.import_bundle().unwrap();

    let ext = EngineExt {
        conn: to_conn,
        vision: to_vision,
        save: to_save,
    };

    with_tls_data(&data, move || {
        let boot_path = Path::new("scripts/boot.py");
        let mut engine = Engine::new(boot_path, init_delta, ext);

        for msg in recv.iter() {
            handle(&mut engine, msg);
        }

        info!("input channel disconnected");
    });
}
