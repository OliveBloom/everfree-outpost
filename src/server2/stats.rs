use server_types::*;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::sync::mpsc::{self, Sender};
use rustc_serialize::Encodable;
use rustc_serialize::json;

use engine_thread::ToEngine;
use util;


#[derive(RustcEncodable)]
pub struct EngineStats {
    pub now: Time,

    pub num_clients: usize,
    pub num_entities: usize,
    pub num_inventories: usize,
    pub num_planes: usize,
    pub num_terrain_chunks: usize,
    pub num_structures: usize,

    pub client_list: Box<[Box<str>]>,
}

#[derive(RustcEncodable)]
pub struct Stats {
    engine: EngineStats,
}


pub fn run_stats(to_engine: Sender<ToEngine>,
                 path: &Path) {
    let server = util::unix_bind(path).unwrap();
    for socket in server.incoming() {
        let mut socket = socket.unwrap();

        let (eng_send, eng_recv) = mpsc::channel();
        to_engine.send(ToEngine::GetStats(eng_send)).unwrap();

        let stats = Stats {
            engine: eng_recv.recv().unwrap(),
        };

        let mut s = String::new();
        stats.encode(&mut json::Encoder::new(&mut s)).unwrap();
        socket.write_all(s.as_bytes()).unwrap();
    }
}

pub fn start_stats(to_engine: Sender<ToEngine>,
                   path: PathBuf) {
    let path = path.to_path_buf();
    util::spawn_named("stats", move || {
        run_stats(to_engine, &path);
    });
}
