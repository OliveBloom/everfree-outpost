use std::thread;
use std::time::{Instant, Duration};
use std::sync::mpsc::Sender;
use engine::tick::TICK_MS;

use engine_thread::ToEngine;
use util;


pub fn run_tick(to_engine: Sender<ToEngine>) {
    let mut next_tick = Instant::now();
    let tick_dur = Duration::from_millis(TICK_MS as u64);
    loop {
        to_engine.send(ToEngine::Tick).unwrap();

        let now = Instant::now();
        next_tick += tick_dur;
        if next_tick > now {
            thread::sleep(next_tick - now);
        } else {
            // next_tick <= now
            warn!("tick thread fell behind by {:?}", now - next_tick);
            next_tick = now;
        }
    }
}

pub fn start_tick(to_engine: Sender<ToEngine>) {
    util::spawn_named("tick", move || {
        run_tick(to_engine);
    });
}
