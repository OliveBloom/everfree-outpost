#![crate_name = "server2"]
#![feature(
    proc_macro_hygiene,
    vec_resize_default,
)]

extern crate env_logger;
#[macro_use] extern crate log;
extern crate rustc_serialize;
extern crate time;

extern crate common;
extern crate engine;
extern crate server_bundle;
extern crate server_config;
extern crate server_extra;
extern crate server_types;
extern crate server_world_types;
extern crate syntax_exts;
#[macro_use] extern crate world;

// Import `expand_objs` name list
define_obj_list_world!();

mod util;

mod conn;
mod engine_thread;
mod io;
mod ipython;
mod messages;
mod save;
mod stats;
mod terrain_gen;
mod tick;
mod vision;


use std::env;
use std::io::prelude::*;
use std::mem::drop;
use std::panic;
use std::path::Path;
use std::process;
use std::sync::Arc;
use std::sync::mpsc;

use server_config::{Data, Storage};


fn init_panic() {
    // `-C panic=abort` doesn't print the whole stack trace on panic.  Here's a custom panic hook
    // that runs the normal `unwind` hook to print the stack, then aborts.
    let old_hook = panic::take_hook();
    let new_hook = Box::new(move |info: &panic::PanicInfo| {
        old_hook(info);
        // TODO: attempt some kind of orderly shutdown
        // (kill terrain_gen, save files, notify clients, notify wrapper, restart?)
        process::abort();
    });
    panic::set_hook(new_hook);
}


fn main() {
    env_logger::init().unwrap();
    init_panic();

    // Initialize engine environment.
    let args = env::args().collect::<Vec<_>>();
    let storage = Storage::new(&args[1]);

    let mut raw_bytes = Vec::new();
    storage.open_binary_data().read_to_end(&mut raw_bytes).unwrap();
    let data = Arc::new(Data::new(raw_bytes.into_boxed_slice()));

    let (to_output, output_recv) = mpsc::channel();
    let (to_engine, engine_recv) = mpsc::channel();
    let (to_vision, vision_recv) = mpsc::channel();
    let (to_conn, conn_recv) = mpsc::channel();
    let (to_save, save_recv) = mpsc::channel();
    let (to_terrain_gen, terrain_gen_recv) = mpsc::channel();

    conn::start_conn(conn_recv,
                     to_output,
                     to_engine.clone(),
                     to_vision.clone());

    io::start_input(std::io::stdin(),
                    to_conn.clone());
    io::start_output(std::io::stdout(),
                     output_recv);

    ipython::start_ipython(to_engine.clone(),
                           Path::new(&args[1]).join("ipython"));

    save::start_save(save_recv,
                     to_engine.clone(),
                     to_terrain_gen,
                     Path::new("save"),
                     data.clone());

    stats::start_stats(to_engine.clone(),
                       Path::new(&args[1]).join("stats"));

    tick::start_tick(to_engine.clone());

    terrain_gen::start_terrain_gen(terrain_gen_recv,
                                   to_engine.clone(),
                                   Path::new("."));

    vision::start_vision(vision_recv,
                         to_conn.clone());

    drop(to_engine);


    engine_thread::run_engine(data,
                              engine_recv,
                              to_conn,
                              to_vision,
                              to_save);
}
