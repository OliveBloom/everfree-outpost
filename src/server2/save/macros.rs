use syntax_exts::define_expand_objs_list;

define_expand_objs_list! {
    world_components = {
        world_ward_permits, FLAG = W_WARD_PERMITS,
            Tag = (::engine::update::components::WardPermitsTag);
    }
}

define_expand_objs_list! {
    client_components = {
        client_key_value, FLAG = C_KEY_VALUE,
            Tag = (::engine::update::components::KeyValueTag);
    }
}

define_expand_objs_list! {
    entity_components = {
        entity_char_invs, FLAG = E_CHAR_INVS,
            Tag = (::engine::update::components::CharInvsTag);
        entity_key_value, FLAG = E_KEY_VALUE,
            Tag = (::engine::update::components::KeyValueTag);
    }
}

define_expand_objs_list! {
    inventory_components = {
        inventory_key_value, FLAG = I_KEY_VALUE,
            Tag = (::engine::update::components::KeyValueTag);
    }
}

define_expand_objs_list! {
    plane_components = {
        plane_ward_map, FLAG = P_WARD_MAP,
            Tag = (::engine::update::components::WardMapTag);
        plane_key_value, FLAG = P_KEY_VALUE,
            Tag = (::engine::update::components::KeyValueTag);
        plane_looted_vaults, FLAG = P_LOOTED_VAULTS,
            Tag = (::engine::update::components::LootedVaultsTag);
    }
}

define_expand_objs_list! {
    terrain_chunk_components = {
        terrain_chunk_key_value, FLAG = TC_KEY_VALUE,
            Tag = (::engine::update::components::KeyValueTag);
        terrain_chunk_compass_targets, FLAG = TC_COMPASS_TARGETS,
            Tag = (::engine::update::components::CompassTargetsTag);
    }
}

define_expand_objs_list! {
    structure_components = {
        structure_bitmap, FLAG = S_BITMAP,
            Tag = (::engine::update::components::BitmapTag);
        structure_contents, FLAG = S_CONTENTS,
            Tag = (::engine::update::components::ContentsTag);
        structure_crafting, FLAG = S_CRAFTING,
            Tag = (::engine::update::components::CraftingTag);
        structure_key_value, FLAG = S_KEY_VALUE,
            Tag = (::engine::update::components::KeyValueTag);
        structure_parent_vault, FLAG = S_PARENT_VAULT,
            Tag = (::engine::update::components::ParentVaultTag);
    }
}
