use std::collections::HashSet;
use std::ffi::{OsString, OsStr};
use std::fs::{self, File, ReadDir};
use std::io;
use std::mem;
use std::path::{Path, PathBuf};


/// Simple abstraction layer for filesystems.  All methods work with files only.  Directories are
/// created automatically in `create`, and are otherwise ignored.
///
/// Note that a `Filesystem` object represents a *logical* filesystem.  It is typically backed by a
/// directory in the real underlying physical filesystem.
///
/// # Atomic rename
///
/// The `Filesystem` trait provides support for atomic renames both within and between (logical)
/// filesystems.  The logical rename is implemented as a single (atomic) rename at the physical
/// level, possibly preceded and followed by some cleanup operations in the source and destination
/// filesystems.  Specifically, the trait includes a set of methods `{pre,post}_rename_{src,dest}`,
/// which should be called on the source/destination filesystem before/after the physical
/// `fs::rename`.  Each method is a no-op on the contents of the logical filesystem, but they may
/// perform any necessary operations to enable a single physical `fs::rename` to cause the desired
/// logical rename.
///
/// The `Filesystem` trait also provides a method `pre_copy_src`, which can be used together with
/// `pre/post_rename_dest` and `fs::copy` to efficiently copy a file within or between filesystems.
pub trait Filesystem {
    fn open(&self, path: &Path) -> io::Result<File>;
    fn create(&self, path: &Path) -> io::Result<File>;
    fn remove(&self, path: &Path) -> io::Result<()>;
    fn exists(&self, path: &Path) -> bool;

    /// Remove all empty directories.
    fn cleanup(&self) -> io::Result<bool>;

    /// Prepare for a rename operation with `path` as its source.  Returns the physical path of the
    /// file to be moved.
    fn pre_rename_src(&self, path: &Path) -> io::Result<PathBuf>;

    /// Finish a rename operation with `path` as its source.
    fn post_rename_src(&self, path: &Path) -> io::Result<()>;

    /// Prepare for a rename operation with `path` as its destination.  Returns the physical path
    /// of the file to be moved.
    fn pre_rename_dest(&self, path: &Path) -> io::Result<PathBuf>;

    /// Finish a rename operation with `path` as its destination.
    fn post_rename_dest(&self, path: &Path) -> io::Result<()>;

    /// Prepare for a copy operation with `path` as its source.  Returns the physical path of the
    /// file to be copied.
    ///
    /// There is no `post_copy_src` because the copy operation should have no effect on the source
    /// filesystem.
    fn pre_copy_src(&self, path: &Path) -> io::Result<PathBuf>;


    type Iter: Iterator<Item=io::Result<PathBuf>>;
    fn iter(&self) -> io::Result<Self::Iter>;
}

#[derive(Clone, Debug)]
pub struct RealFs {
    base: PathBuf,
}

impl RealFs {
    pub fn new(base: PathBuf) -> RealFs {
        RealFs { base }
    }
}

impl Filesystem for RealFs {
    fn open(&self, path: &Path) -> io::Result<File> {
        File::open(self.base.join(path))
    }

    fn create(&self, path: &Path) -> io::Result<File> {
        let path = self.base.join(path);

        match File::create(&path) {
            Ok(f) => return Ok(f),
            Err(e) =>
                if e.kind() == io::ErrorKind::NotFound {} // keep going
                else { return Err(e) },
        }

        // Creating the file failed with NotFound, so create the dirs and then retry.
        if let Some(parent) = path.parent() {
            trace!("create dir {:?} for file {:?}", parent, path);
            fs::create_dir_all(parent)?;
        }
        File::create(path)
    }

    fn remove(&self, path: &Path) -> io::Result<()> {
        fs::remove_file(self.base.join(path))
    }

    fn exists(&self, path: &Path) -> bool {
        self.base.join(path).exists()
    }

    fn cleanup(&self) -> io::Result<bool> {
        fn cleanup_path(path: &Path) -> io::Result<bool> {
            let mut empty = true;
            for de in fs::read_dir(path)? {
                let de = de?;
                if de.file_type()?.is_dir() {
                    let de_path = de.path();
                    let emptied_child = cleanup_path(&de_path)?;
                    if emptied_child {
                        fs::remove_dir(&de_path)?;
                    } else {
                        empty = false;
                    }
                } else {
                    empty = false;
                }
            }

            Ok(empty)
        }

        cleanup_path(&self.base)
    }

    fn pre_rename_src(&self, path: &Path) -> io::Result<PathBuf> {
        Ok(self.base.join(path))
    }

    fn post_rename_src(&self, _path: &Path) -> io::Result<()> {
        Ok(())
    }

    fn pre_rename_dest(&self, path: &Path) -> io::Result<PathBuf> {
        let path = self.base.join(path);
        if let Some(parent) = path.parent() {
            trace!("create dir {:?} for file {:?}", parent, path);
            fs::create_dir_all(parent)?;
        }
        Ok(path)
    }

    fn post_rename_dest(&self, _path: &Path) -> io::Result<()> {
        Ok(())
    }

    fn pre_copy_src(&self, path: &Path) -> io::Result<PathBuf> {
        Ok(self.base.join(path))
    }


    type Iter = RealFsIter;
    
    fn iter(&self) -> io::Result<Self::Iter> {
        let mut stack = Vec::new();
        if self.base.exists() {
            // If self.base exists but is not a directory, we'll get an error here.
            stack.push((fs::read_dir(&self.base)?, PathBuf::new()));
        }
        // Otherwise, leave the stack empty.  There are no files in this filesystem.

        Ok(RealFsIter { stack })
    }
}

pub struct RealFsIter {
    stack: Vec<(ReadDir, PathBuf)>,
}

impl Iterator for RealFsIter {
    type Item = io::Result<PathBuf>;

    fn next(&mut self) -> Option<io::Result<PathBuf>> {
        macro_rules! try {
            ($e:expr) => {
                match $e {
                    Ok(x) => x,
                    Err(e) => return Some(Err(e)),
                }
            };
        }

        loop {
            match self.stack.last_mut().map(|rd_pb| rd_pb.0.next()) {
                None => {
                    // Stack is empty
                    return None;
                },
                Some(None) => {
                    // Reached the end of the current directory.  Go up.
                    self.stack.pop();
                },
                Some(Some(Ok(de))) => {
                    // Got a new entry.  Enter (if it's a directory) or yield.
                    let ft = try!(de.file_type());
                    let ft =
                        if !ft.is_symlink() { ft }
                        else { try!(fs::metadata(&de.path())).file_type() };
                    let path = self.stack.last().unwrap().1.join(de.file_name());

                    if ft.is_dir() {
                        let rd = try!(fs::read_dir(&de.path()));
                        self.stack.push((rd, path));
                    } else if ft.is_file() {
                        return Some(Ok(path));
                    } else {
                        // We shouldn't see other kinds of files here.
                        let msg = format!("unexpected special file in traversal: {}",
                                          de.path().display());
                        return Some(Err(io::Error::new(io::ErrorKind::Other, msg)));
                    }
                },
                Some(Some(Err(e))) => {
                    return Some(Err(e));
                },
            }
        }
    }
}




/// Union filesystem layer.  Writes go to the `upper` filesystem, while reads can come from either
/// `upper` or `lower`.  Also provides a `commit` operation that updates `lower` to match the
/// contents of the union.
///
/// # "Whiteout" files
///
/// In addition to normal files, the upper layer may also contain "whiteout" files.  The presence
/// of a whiteout file for a given path indicates that the file with the same path in the lower
/// layer should be hidden in the unioned filesystem.  This is used for deleting files that exist
/// in the lower layer.
///
/// Normally, for any given path, the upper layer contains either a data file for that path, a
/// whiteout file for that path, or neither.  If both the data and whiteout files exist for a given
/// path, the whiteout is ignored.  This is useful for implementing atomic renames: create whiteout
/// at the source (a logical no-op, because the data file is still at source), move the data file,
/// and delete whiteout at the destination (also a logical no-op).
#[derive(Clone, Debug)]
pub struct UnionFs<FS1, FS2> {
    pub lower: FS1,
    pub upper: FS2,
}

impl<FS1: Filesystem, FS2: Filesystem> UnionFs<FS1, FS2> {
    pub fn new(lower: FS1, upper: FS2) -> UnionFs<FS1, FS2> {
        UnionFs { lower, upper }
    }

    // All paths in the upper layer are transformed.  Normal files get a `_` appended, and whiteout
    // files get `_wo`.  This guarantees that no choice of user name will make a normal file look
    // like a whiteout.

    fn data_path(&self, path: &Path) -> PathBuf {
        let mut s = OsString::with_capacity(path.as_os_str().len() + 1);
        s.push(path);
        s.push("_");
        s.into()
    }

    fn whiteout_path(&self, path: &Path) -> PathBuf {
        let mut s = OsString::with_capacity(path.as_os_str().len() + 3);
        s.push(path);
        s.push("_wo");
        s.into()
    }

    /// Commit changes from the upper layer to the lower one.  Has no effect on the logical
    /// contents of `self`, but afterward, `self` and `self.lower` are identical, and `self.upper`
    /// is empty.
    ///
    /// Since this operation is a logical no-op, it's safe to run it again or just continue
    /// normally if it fails or gets interrupted.
    pub fn commit(&self) -> io::Result<()> {
        for path_r in self.upper.iter()? {
            let path = path_r?;

            let (base_path, is_whiteout) = match parse_upper_path(&path) {
                Some(x) => x,
                None => {
                    let msg = format!("unexpected file in unionfs upper layer: {}",
                                      path.display());
                    return Err(io::Error::new(io::ErrorKind::Other, msg));
                },
            };

            if is_whiteout {
                match self.lower.remove(&base_path) {
                    Ok(()) => {},
                    Err(e) =>
                        if e.kind() == io::ErrorKind::NotFound {} // keep going
                        else { return Err(e) },
                }
                self.upper.remove(&path)?;
            } else {
                rename(&self.upper, &path, &self.lower, &base_path)?;
            }
        }

        self.upper.cleanup()?;

        Ok(())
    }
}

impl<FS1: Filesystem, FS2: Filesystem> Filesystem for UnionFs<FS1, FS2> {
    fn open(&self, path: &Path) -> io::Result<File> {
        match self.upper.open(&self.data_path(path)) {
            Ok(f) => return Ok(f),
            Err(e) =>
                if e.kind() == io::ErrorKind::NotFound {} // keep going
                else { return Err(e) },
        }

        // First try to open the lower file, then if that succeeds, also check for whiteout.
        let f = self.lower.open(path)?;

        if self.upper.exists(&self.whiteout_path(path)) {
            return Err(io::Error::new(
                    io::ErrorKind::NotFound, "file blocked by whiteout"));
        }

        Ok(f)
    }

    fn create(&self, path: &Path) -> io::Result<File> {
        let whiteout_path = self.whiteout_path(path);
        if self.upper.exists(&whiteout_path) {
            self.upper.remove(&whiteout_path)?;
        }

        self.upper.create(&self.data_path(path))
    }

    fn remove(&self, path: &Path) -> io::Result<()> {
        let data_path = self.data_path(path);
        let whiteout_path = self.whiteout_path(path);

        let whiteout = self.upper.exists(&whiteout_path);
        let in_upper = self.upper.exists(&data_path);
        let in_lower = self.lower.exists(path);

        if !in_upper && (whiteout || !in_lower) {
            return Err(io::Error::new(
                    io::ErrorKind::NotFound, "file not found"));
        }

        if in_upper {
            fs::remove_file(&data_path)?;
        }

        if in_lower {
            self.upper.create(&whiteout_path)?;
        }

        Ok(())
    }

    fn exists(&self, path: &Path) -> bool {
        if self.upper.exists(&self.data_path(path)) {
            return true;
        }

        if self.lower.exists(path) && !self.upper.exists(&self.whiteout_path(path)) {
            return true;
        }

        false
    }

    fn cleanup(&self) -> io::Result<bool> {
        // TODO: might want to clean up unneeded whiteout files first
        let empty = self.lower.cleanup()? && self.upper.cleanup()?;
        Ok(empty)
    }


    // `rename` implementation:
    //
    // At some point, a logical rename will invoke `fs::rename` on some physical file.  The
    // logical rename should take effect at exactly that point, ensuring it is atomic.  There may
    // be additional bookkeeping operations before and after, but as long as none of those
    // operations has a visible effect at the logical level, the atomicity of logical renames will
    // be preserved.

    fn pre_rename_src(&self, path: &Path) -> io::Result<PathBuf> {
        let data_path = self.data_path(path);
        let whiteout_path = self.whiteout_path(path);

        let whiteout = self.upper.exists(&whiteout_path);
        let in_upper = self.upper.exists(&data_path);
        let in_lower = self.lower.exists(path);

        if in_lower {
            // We need to get into a state where `upper` contains both the data and whiteout files
            // for this path.  Then a physical rename of the data file will leave only the
            // whiteout, removing the file at the logical level.
            if !in_upper {
                copy(&self.lower, path, &self.upper, &data_path)?;
            }

            if !whiteout {
                self.upper.create(&whiteout_path)?;
            }
        }
        // If the file isn't present in the lower layer, we don't need to do anything special.  The
        // fs::rename will move the file out of the upper layer, leaving the file absent at the
        // logical level.

        self.upper.pre_rename_src(&data_path)
    }

    fn post_rename_src(&self, path: &Path) -> io::Result<()> {
        self.upper.post_rename_src(&self.data_path(path))
    }

    fn pre_rename_dest(&self, path: &Path) -> io::Result<PathBuf> {
        let data_path = self.data_path(path);
        self.upper.pre_rename_dest(&data_path)
    }

    fn post_rename_dest(&self, path: &Path) -> io::Result<()> {
        let whiteout_path = self.whiteout_path(path);
        if self.upper.exists(&whiteout_path) {
            self.upper.remove(&whiteout_path)?;
        }
        self.upper.post_rename_dest(&self.data_path(path))
    }

    fn pre_copy_src(&self, path: &Path) -> io::Result<PathBuf> {
        let data_path = self.data_path(path);
        if self.upper.exists(&data_path) {
            self.upper.pre_copy_src(&data_path)
        } else {
            self.lower.pre_copy_src(path)
        }
    }


    type Iter = UnionFsIter<FS1, FS2>;

    fn iter(&self) -> io::Result<Self::Iter> {
        Ok(UnionFsIter {
            lower: self.lower.iter()?,
            upper: self.upper.iter()?,

            upper_done: false,
            upper_files: HashSet::new(),
        })
    }
}

pub struct UnionFsIter<FS1: Filesystem, FS2: Filesystem> {
    lower: FS1::Iter,
    upper: FS2::Iter,
    upper_done: bool,

    /// Logical paths of all files encountered while traversing the upper layer.  These files
    /// should not be yielded during the lower layer traversal.
    upper_files: HashSet<PathBuf>,
}

impl<FS1: Filesystem, FS2: Filesystem> Iterator for UnionFsIter<FS1, FS2> {
    type Item = io::Result<PathBuf>;

    fn next(&mut self) -> Option<io::Result<PathBuf>> {
        loop {
            if !self.upper_done {
                match self.upper.next() {
                    None => {
                        self.upper_done = true;
                        // On the next iteration, we enter the `self.lower` case.
                    },
                    Some(Err(e)) => return Some(Err(e)),
                    Some(Ok(path)) => {
                        let (base_path, is_whiteout) = match parse_upper_path(&path) {
                            Some(x) => x,
                            None => {
                                let msg = format!("unexpected file in unionfs upper layer: {}",
                                                  path.display());
                                return Some(Err(io::Error::new(io::ErrorKind::Other, msg)));
                            },
                        };
                        self.upper_files.insert(base_path.to_owned());

                        if !is_whiteout {
                            return Some(Ok(base_path.to_owned()));
                        }
                        // Otherwise, keep going.
                    },
                }
            } else {
                match self.lower.next() {
                    None => {
                        return None;
                    }
                    Some(Err(e)) => return Some(Err(e)),
                    Some(Ok(path)) => {
                        if !self.upper_files.contains(&path) {
                            return Some(Ok(path));
                        }
                        // Otherwise, keep going.
                    },
                }
            }
        }
    }
}


/// Read the suffix of a UnionFs upper layer path, and return the unsuffixed path and a flag
/// indicating whether the original path referred to a whiteout file.
fn parse_upper_path<'a>(path: &'a Path) -> Option<(&'a Path, bool)> {
    // XXX Bad hacks going on here
    //
    // Turns out there's no way to read or manipulate an OsStr.  All you can do
    // is convert to an OsString and append to it.  But here, we need to check
    // for a suffix ("_" or "_wo") and remove it.
    //
    // We do this by transmuting OsStr to &[u8], doing the check and update on
    // raw ASCII bytes, and transmuting back.  Turns out this is roughly what
    // the `std::path` module does internally for things like `set_file_name`
    // (as of 1.26.2), so this hack will probably be safe for a good while.
    // It's even cross-platform, since Rust uses "WTF-8" encoding instead of
    // UTF-16 for OsStrings on Windows.

    unsafe fn os_to_u8<'a>(s: &'a OsStr) -> &'a [u8] {
        mem::transmute(s)
    }
    unsafe fn u8_to_os<'a>(s: &'a [u8]) -> &'a OsStr {
        mem::transmute(s)
    }

    let path_bytes = unsafe { os_to_u8(path.as_os_str()) };

    let (base_path_bytes, is_whiteout) =
        if path_bytes.ends_with(b"_") {
            (&path_bytes[.. path_bytes.len() - 1], false)
        } else if path_bytes.ends_with(b"_wo") {
            (&path_bytes[.. path_bytes.len() - 3], true)
        } else {
            return None;
        };

    let base_path = Path::new(unsafe { u8_to_os(base_path_bytes) });

    Some((base_path, is_whiteout))
}



pub fn rename<FS1: Filesystem, FS2: Filesystem>(fs1: &FS1,
                                                p1: &Path,
                                                fs2: &FS2,
                                                p2: &Path) -> io::Result<()> {
    let phys1 = fs1.pre_rename_src(p1)?;
    let phys2 = fs2.pre_rename_dest(p2)?;
    fs::rename(&phys1, &phys2)?;
    fs1.post_rename_src(p1)?;
    fs2.post_rename_dest(p2)?;
    Ok(())
}

pub fn copy<FS1: Filesystem, FS2: Filesystem>(fs1: &FS1,
                                              p1: &Path,
                                              fs2: &FS2,
                                              p2: &Path) -> io::Result<()> {
    let phys1 = fs1.pre_copy_src(p1)?;
    let phys2 = fs2.pre_rename_dest(p2)?;
    fs::copy(&phys1, &phys2)?;
    fs2.post_rename_dest(p2)?;
    Ok(())
}
