use server_types::*;
use std::io;
use std::path::Path;
use std::sync::Arc;
use std::sync::mpsc::{Sender, Receiver, TryRecvError, RecvTimeoutError};
use std::time::{Instant, Duration};
use std::u64;

use engine::update::Delta;
use server_bundle::builder::Builder;
use server_bundle::types::Bundle;
use server_config::{data, with_tls_data, Data};

use engine_thread::ToEngine;
use terrain_gen::{ToTerrainGen, GenParams};
use util;

use self::memory::{ExportWorld, BundleKey};
use self::storage::SaveStorage;


mod macros;

pub mod import;
pub mod memory;
mod objects;
mod storage;
mod fs;
mod components;
mod events;

pub enum ToSave {
    LoadWorld(Sender<Bundle>),
    LoadClient(u32, String),
    LoadPlane(Stable<PlaneId>),
    LoadTerrainChunk(Stable<PlaneId>, V2),

    /// Register a new plane with the given ID and metadata.
    CreatePlane(Stable<PlaneId>, u32),
    /// Unregister a plane.  It can no longer be referenced by `LoadTerrainChunk` messages.
    DestroyPlane(Stable<PlaneId>),

    Delta(Time, Arc<Delta>),

    /// Set the autosave interval in seconds.
    #[allow(dead_code)]
    SetAutosaveInterval(u32),
}


fn create_initial_save(ss: &mut SaveStorage) -> io::Result<()> {
    let mut world_build = Builder::new(data());
    world_build.world()
        .next_client(1)
        .next_entity(1)
        .next_inventory(1)
        .next_plane(3)
        .next_terrain_chunk(1)
        .next_structure(1);
    ss.save(BundleKey::World, &world_build.finish())?;

    let mut limbo_build = Builder::new(data());
    limbo_build.plane()
        .name("Limbo")
        .stable_id(STABLE_PLANE_LIMBO.unwrap());
    ss.save(BundleKey::Plane(STABLE_PLANE_LIMBO), &limbo_build.finish())?;

    let mut forest_build = Builder::new(data());
    forest_build.plane()
        .name("Everfree Forest")
        .stable_id(STABLE_PLANE_FOREST.unwrap())
        .extra(|e| {
            e.set_array("ward_map");
            e.set_array("looted_vaults");
        });
    ss.save(BundleKey::Plane(STABLE_PLANE_FOREST), &forest_build.finish())?;

    Ok(())
}

fn new_client_bundle(uid: u32, name: &str) -> Box<Bundle> {
    trace!("generating new bundle for client 0x{:x} ({:?})", uid, name);
    let mut b = Builder::new(data());
    {
        let mut c = b.client();
        c.name(name)
         .stable_id(uid as StableId);
    }
    Box::new(b.finish())
}

struct SaveThread {
    recv: Receiver<ToSave>,
    to_engine: Sender<ToEngine>,
    to_terrain_gen: Sender<ToTerrainGen>,
    ew: ExportWorld,
    ss: SaveStorage,
    autosave_interval: Duration,
    last_autosave: Instant,
}

impl SaveThread {
    pub fn new(recv: Receiver<ToSave>,
               to_engine: Sender<ToEngine>,
               to_terrain_gen: Sender<ToTerrainGen>,
               save_dir: &Path) -> SaveThread {
        SaveThread {
            recv, to_engine, to_terrain_gen,
            ew: ExportWorld::new(),
            ss: SaveStorage::new(save_dir).unwrap(),
            autosave_interval: Duration::from_secs(300),
            last_autosave: Instant::now(),
        }
    }

    fn handle_one_io(&mut self, msg: ToSave) -> io::Result<()> {
        use self::ToSave::*;
        match msg {
            LoadWorld(resp) => {
                let b = self.ss.load(BundleKey::World)?;
                resp.send(b).unwrap();
            },

            LoadClient(uid, name) => {
                let bk = BundleKey::Client(Stable::new(uid as StableId));
                let b =
                    if self.ss.contains(bk) { Box::new(self.ss.load(bk)?) }
                    else { new_client_bundle(uid, &name) };
                self.to_engine.send(ToEngine::ClientBundle(uid, b)).unwrap();
            },

            LoadPlane(stable_id) => {
                let bk = BundleKey::Plane(stable_id);
                let b = if self.ss.contains(bk) {
                    Box::new(self.ss.load(bk)?)
                } else {
                    // The engine shouldn't try to load nonexistent planes.  It should create
                    // the plane itself and then send `CreatePlane` instead.
                    fail!("engine requested nonexistent plane {:?}", stable_id);
                };
                self.to_engine.send(ToEngine::PlaneBundle(stable_id, b)).unwrap();

                // We don't register this plane with terrain gen until the engine reports that it's
                // been imported, by sending an appropriate `CreatePlane` message.
            },

            LoadTerrainChunk(stable_plane, cpos) => {
                let bk = BundleKey::TerrainChunk(stable_plane, cpos);
                if self.ss.contains(bk) {
                    let b = Box::new(self.ss.load(bk)?);
                    let eng_msg = ToEngine::TerrainChunkBundle(stable_plane, cpos, b);
                    self.to_engine.send(eng_msg).unwrap();
                } else {
                    // Send request to terrain_gen.  The response will be sent directly to the
                    // engine thread.
                    let tg_msg = ToTerrainGen::GenChunk(stable_plane, cpos);
                    self.to_terrain_gen.send(tg_msg).unwrap();
                }
            },


            CreatePlane(stable_id, mode) => {
                let params = Box::new(GenParams {
                    mode: mode,
                });
                self.to_terrain_gen.send(ToTerrainGen::InitPlane(stable_id, params)).unwrap();
            },

            DestroyPlane(stable_id) => {
                self.to_terrain_gen.send(ToTerrainGen::ForgetPlane(stable_id)).unwrap();
            },


            Delta(now, delta) => {
                let was_empty = self.ew.is_empty();
                let unloads = self.ew.apply(now, &delta);
                for (bk, b) in unloads {
                    self.ss.save(bk, &b)?;
                }
                if !was_empty && self.ew.is_empty() {
                    // There are no objects in `self.ew`, but the world could still be dirty.
                    // `autosave` will write it to disk, and then `commit()` so it's safe to kill
                    // the server.
                    self.autosave();
                }
            },

            SetAutosaveInterval(secs) => {
                if secs == 0 {
                    self.autosave_interval = Duration::from_secs(u64::MAX);
                } else {
                    self.autosave_interval = Duration::from_secs(secs as u64);
                }
            },
        }

        Ok(())
    }

    fn handle_one(&mut self, msg: ToSave) {
        self.handle_one_io(msg).unwrap();
    }

    fn autosave(&mut self) {
        let unloads = self.ew.export_dirty();
        let count = unloads.len();
        trace!("begin autosave ({} bundles)", count);

        let mut saw_interesting_bundles = false;
        for (bk, b) in unloads {
            self.ss.save(bk, &b).unwrap();

            if bk != BundleKey::World {
                saw_interesting_bundles = true;
            }
        }
        self.ss.commit().unwrap();
        self.last_autosave = Instant::now();

        if saw_interesting_bundles {
            info!("autosaved {} bundles", count);
        } else {
            trace!("autosaved {} bundles (all uninteresting)", count);
        }
    }

    fn start(&mut self) {
        if !self.ss.contains(BundleKey::World) {
            create_initial_save(&mut self.ss).unwrap();
        }
    }

    pub fn run(&mut self) {
        self.start();

        let mut running = true;
        while running {
            let mut now = Instant::now();
            let mut next = self.last_autosave + self.autosave_interval;

            if now >= next {
                // We already missed the deadline.  It's possible autosave is too slow to run at
                // the specified interval.  We delay a bit to process engine messages so that
                // autosave can't completely block everything else, but also cap the time spent on
                // messages so a flood of messages can't block autosave.

                let msg_end = now + Duration::from_secs(5);
                loop {
                    match self.recv.try_recv() {
                        Ok(msg) => self.handle_one(msg),
                        Err(TryRecvError::Empty) => break,
                        Err(TryRecvError::Disconnected) => {
                            running = false;
                            break;
                        },
                    }

                    if Instant::now() >= msg_end {
                        break;
                    }
                }
            } else {
                // Process messages until we hit the deadline.
                while now < next {
                    match self.recv.recv_timeout(next - now) {
                        Ok(msg) => self.handle_one(msg),
                        Err(RecvTimeoutError::Timeout) => break,
                        Err(RecvTimeoutError::Disconnected) => {
                            running = false;
                            break;
                        },
                    }

                    // Recompute `next` - an incoming message could change `autosave_interval`.
                    next = self.last_autosave + self.autosave_interval;
                    now = Instant::now();
                }
            }

            self.autosave();
        }
    }
}

pub fn start_save(recv: Receiver<ToSave>,
                  to_engine: Sender<ToEngine>,
                  to_terrain_gen: Sender<ToTerrainGen>,
                  save_dir: &Path,
                  data: Arc<Data>) {
    let save_dir = save_dir.to_path_buf();
    util::spawn_named("save", move || {
        with_tls_data(&data, move || {
            SaveThread::new(recv, to_engine, to_terrain_gen, &save_dir).run();
        });
    });
}
