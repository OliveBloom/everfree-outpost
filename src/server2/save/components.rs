use server_types::*;
use std::collections::{HashMap, HashSet};
use std::str::FromStr;

use common::util::Bytes;
use common::util::StringResult;
use engine::component::bitmap::{self, Bitmap};
use engine::component::compass_targets::{CompassTargets, LootedVaults};
use engine::component::char_invs::CharInvs;
use engine::component::crafting::{self, CraftingState};
use engine::component::ward_map::{WardMap, Ward};
use engine::component::ward_permits::WardPermits;
use engine::update::{Delta, flags};
use engine::update::components::*;
use engine::update::delta::DeltaInternals;
use server_config::data;
use server_extra::{Extra, AnyValue};

use save::events;


pub struct EncodeCx<'a> {
    pub delta: &'a Delta,
    pub now: Time,
}

impl<'a> EncodeCx<'a> {
    pub fn new(delta: &'a Delta, now: Time) -> EncodeCx<'a> {
        EncodeCx { delta, now }
    }
}


pub struct DecodeCx<'a> {
    pub extra: &'a Extra,
}


pub trait Component<Id> {
    fn encode(cx: &EncodeCx, id: Id, extra: &mut Extra);
    fn decode(cx: &DecodeCx, id: Id, delta: &mut DeltaInternals) -> StringResult<()>;
}


macro_rules! extra_get_value {
    ($extra:expr, $id:expr, $parent:expr, $key:expr, $type:ty) => {{
        let v = $extra.get($key)
            .ok_or_else(|| format!("{:?}: missing {}[{:?}]", $id, $parent, $key))?;
        v.as_value().and_then(<$type as AnyValue>::from_value)
            .ok_or_else(|| format!("{:?}: expected {}[{:?}] to be {}",
                                   $id, $parent, $key, stringify!($type)))
    }};
}


impl Component<()> for WardPermitsTag {
    fn encode(cx: &EncodeCx, _id: (), extra: &mut Extra) {
        if cx.delta.world_flags().intersects(flags::W_WARD_PERMITS) {
            if let Some(wp) = cx.delta.new_world_ward_permits(()) {
                let mut e = extra.get_or_set_hash("ward_permits");
                for (&owner, names) in wp.permits() {
                    let owner_str = owner.unwrap().to_string();
                    let mut e_names = e.borrow().get_or_set_hash(&owner_str);
                    for name in names {
                        e_names.borrow().set(name, true.into_value());
                    }
                }
            } else {
                extra.remove("ward_permits");
            }
        }
    }

    fn decode(cx: &DecodeCx, _id: (), delta: &mut DeltaInternals) -> StringResult<()> {
        if let Some(e_wp) = cx.extra.get("ward_permits") {
            let e_wp = e_wp.as_hash()
                .ok_or_else(|| format!("ward_permits is not a hash"))?;
            let mut permits = HashMap::new();
            for (owner_str, e_names) in e_wp.iter() {
                let owner = Stable::new(
                    StableId::from_str(owner_str)
                        .map_err(|e| format!("ward_permits[{}]: error parsing key: {}",
                                             owner_str, e))?);

                let mut names = HashSet::new();
                let e_names = e_names.as_hash()
                    .ok_or_else(|| format!(
                        "ward_permits[{}][names] is not a hash", owner_str))?;
                for (name, _) in e_names.iter() {
                    names.insert(name.to_owned());
                }

                permits.insert(owner, names);
            }

            delta.flags.world_mut(()).insert(flags::W_WARD_PERMITS);
            delta.new.insert_world_ward_permits((), Some(WardPermits::from_parts(permits)));
        }
        Ok(())
    }
}


impl Component<EntityId> for CharInvsTag {
    fn encode(cx: &EncodeCx, id: EntityId, extra: &mut Extra) {
        if let Some(w) = cx.delta.new_entity_char_invs(id) {
            let mut e = extra.get_or_set_hash("char_invs");
            e.borrow().set("main", w.main.into_value());
            e.borrow().set("ability", w.ability.into_value());
            e.borrow().set("equip", w.equip.into_value());
        } else {
            extra.remove("char_invs");
        }
    }

    fn decode(cx: &DecodeCx, id: EntityId, delta: &mut DeltaInternals) -> StringResult<()> {
        if let Some(extra) = cx.extra.get("char_invs") {
            let extra = extra.as_hash()
                .ok_or_else(|| format!("{:?}: char_invs is not a hash", id))?;
            let main = extra_get_value!(extra, id, "char_invs", "main", InventoryId)?;
            let ability = extra_get_value!(extra, id, "char_invs", "ability", InventoryId)?;
            let equip = extra_get_value!(extra, id, "char_invs", "equip", InventoryId)?;

            delta.flags.entity_mut(id).insert(flags::E_CHAR_INVS);
            let invs = CharInvs { main, ability, equip };
            delta.new.insert_entity_char_invs(id, Some(invs));
        }
        Ok(())
    }
}


impl Component<PlaneId> for WardMapTag {
    fn encode(cx: &EncodeCx, id: PlaneId, extra: &mut Extra) {
        if cx.delta.plane_flags(id).intersects(
                flags::P_CREATED | flags::P_DESTROYED | flags::P_WARD_MAP) {
            if let Some(wm) = cx.delta.new_plane_ward_map(id) {
                let mut e = extra.get_or_set_array("ward_map");
                for ward in wm.iter() {
                    events::add_ward(e.borrow(),
                                     ward.owner,
                                     &ward.owner_name,
                                     ward.region);
                }
            } else {
                extra.remove("ward_map");
            }
        }
    }

    fn decode(cx: &DecodeCx, id: PlaneId, delta: &mut DeltaInternals) -> StringResult<()> {
        if let Some(e_wm) = cx.extra.get("ward_map") {
            let e_wm = e_wm.as_array()
                .ok_or_else(|| format!("{:?}: ward_map is not an array", id))?;
            let mut wards = Vec::new();
            for (i, e_ward) in e_wm.iter().enumerate() {
                let e_ward = e_ward.as_hash()
                    .ok_or_else(|| format!("{:?}: ward_map[{}] is not a hash", id, i))?;

                let owner = extra_get_value!(e_ward, id, "ward", "owner", Stable<ClientId>)?;
                let owner_name = extra_get_value!(e_ward, id, "ward", "owner_name", String)?;
                let region = extra_get_value!(e_ward, id, "ward", "region", Region<V2>)?;

                wards.push(Ward { owner, owner_name, region });
            }

            delta.flags.plane_mut(id).insert(flags::P_WARD_MAP);
            delta.new.insert_plane_ward_map(id, Some(WardMap::from_iter(wards.into_iter())));
        }
        Ok(())
    }
}


impl Component<StructureId> for BitmapTag {
    fn encode(cx: &EncodeCx, id: StructureId, extra: &mut Extra) {
        if let Some(w) = cx.delta.new_structure_bitmap(id) {
            let mut e = extra.get_or_set_hash("bitmap");
            e.borrow().set("size", w.size.into_value());
            e.borrow().set("data", w.data.clone().into_value());
        } else {
            extra.remove("bitmap");
        }
    }

    fn decode(cx: &DecodeCx, id: StructureId, delta: &mut DeltaInternals) -> StringResult<()> {
        if let Some(extra) = cx.extra.get("bitmap") {
            let extra = extra.as_hash()
                .ok_or_else(|| format!("{:?}: bitmap is not a hash", id))?;
            let size = extra_get_value!(extra, id, "bitmap", "size", V2)?;
            let data = extra_get_value!(extra, id, "bitmap", "data", Bytes)?;

            if data.len() != bitmap::data_len(size) {
                fail!("{:?}: bitmap data has wrong length for size", id);
            }

            delta.flags.structure_mut(id).insert(flags::S_BITMAP);
            delta.new.insert_structure_bitmap(id, Some(Bitmap { size, data }));
        }
        Ok(())
    }
}

impl Component<StructureId> for ContentsTag {
    fn encode(cx: &EncodeCx, id: StructureId, extra: &mut Extra) {
        if let Some(inv) = cx.delta.new_structure_contents(id) {
            extra.set("contents", inv.into_value());
        } else {
            extra.remove("contents");
        }
    }

    fn decode(cx: &DecodeCx, id: StructureId, delta: &mut DeltaInternals) -> StringResult<()> {
        if let Some(extra) = cx.extra.get("contents") {
            let iid = extra.as_value().and_then(InventoryId::from_value)
                .ok_or_else(|| format!("{:?}: contents is not an InventoryId", id))?;
            delta.flags.structure_mut(id).insert(flags::S_CONTENTS);
            delta.new.insert_structure_contents(id, Some(iid));
        }
        Ok(())
    }
}

impl Component<StructureId> for CraftingTag {
    fn encode(cx: &EncodeCx, id: StructureId, extra: &mut Extra) {
        if let Some((state, inv)) = cx.delta.new_structure_crafting(id) {
            let recipe_name = data().recipe(state.recipe()).name();
            let progress = state.saved_progress(cx.now);
            let mut e = extra.get_or_set_hash("crafting");
            e.borrow().set("recipe", recipe_name.to_owned().into_value());
            e.borrow().set("count", (state.count() as i64).into_value());
            e.borrow().set("progress", (progress as i64).into_value());
            e.borrow().set("inv", inv.into_value());
        } else {
            extra.remove("crafting");
        }
    }

    fn decode(cx: &DecodeCx, id: StructureId, delta: &mut DeltaInternals) -> StringResult<()> {
        if let Some(extra) = cx.extra.get("crafting") {
            let extra = extra.as_hash()
                .ok_or_else(|| format!("{:?}: crafting is not a hash", id))?;
            let recipe_name = extra_get_value!(extra, id, "crafting", "recipe", String)?;
            let count = extra_get_value!(extra, id, "crafting", "count", i64)? as u8;
            let progress = extra_get_value!(extra, id, "crafting", "progress", i64)? as u32;
            let inv = extra_get_value!(extra, id, "crafting", "inv", InventoryId)?;

            let recipe = data().get_recipe_id(&recipe_name)
                .ok_or_else(|| format!("{:?}: no such recipe {:?}", id, recipe_name))?;
            let state = CraftingState::paused(recipe, count, progress, crafting::PauseReason::User);

            delta.flags.structure_mut(id).insert(flags::S_CRAFTING);
            delta.new.insert_structure_crafting(id, Some((state, inv)));
        }
        Ok(())
    }
}


for_each_obj! {
    impl Component<$ObjId> for KeyValueTag {
        fn encode(cx: &EncodeCx, id: $ObjId, extra: &mut Extra) {
            if cx.delta.$obj_flags(id).intersects(
                    flags::$O_CREATED | flags::$O_DESTROYED | flags::$O_KEY_VALUE) {
                if let Some(map) = cx.delta.$new_obj_key_value(id) {
                    let mut e = extra.get_or_set_hash("key_value");
                    for (k,v) in map {
                        e.borrow().set(k, v.clone());
                    }
                } else {
                    extra.remove("key_value");
                }
            }
        }

        fn decode(cx: &DecodeCx, id: $ObjId, delta: &mut DeltaInternals) -> StringResult<()> {
            if let Some(e) = cx.extra.get("key_value") {
                let e = e.as_hash()
                    .ok_or_else(|| format!("{:?}: key_value is not a hash", id))?;

                let mut map = HashMap::new();
                for (k,v) in e.iter() {
                    let v = v.as_value()
                        .ok_or_else(|| format!("{:?}: non-simple value at key_value[{}]", id, k))?;
                    map.insert(k.to_owned(), v);
                }

                if map.len() > 0 {
                    delta.flags.$obj_mut(id).insert(flags::$O_KEY_VALUE);
                    delta.new.$insert_obj_key_value(id, Some(map));
                }
            }
            Ok(())
        }
    }
}


impl Component<PlaneId> for LootedVaultsTag {
    fn encode(cx: &EncodeCx, id: PlaneId, extra: &mut Extra) {
        if let Some(looted) = cx.delta.new_plane_looted_vaults(id) {
            let mut e = extra.get_or_set_array("looted_vaults");
            for &vid in &looted.set {
                e.borrow().push(vid.into_value());
            }
        } else {
            extra.remove("looted_vaults");
        }
    }

    fn decode(cx: &DecodeCx, id: PlaneId, delta: &mut DeltaInternals) -> StringResult<()> {
        if let Some(extra) = cx.extra.get("looted_vaults") {
            let extra = extra.as_array()
                .ok_or_else(|| format!("{:?}: looted_vaults is not an array", id))?;

            let mut looted = LootedVaults::default();
            for view in extra.iter() {
                let val = view.as_value()
                    .ok_or_else(|| format!("{:?}: looted_vaults entry is not a value", id))?;
                let pos = V2::from_value(val)
                    .ok_or_else(|| format!("{:?}: looted_vaults entry is not a V2", id))?;
                looted.set.insert(pos);
            }

            delta.flags.plane_mut(id).insert(flags::P_LOOTED_VAULTS);
            delta.new.insert_plane_looted_vaults(id, Some(looted));
        }
        Ok(())
    }
}

impl Component<TerrainChunkId> for CompassTargetsTag {
    fn encode(cx: &EncodeCx, id: TerrainChunkId, extra: &mut Extra) {
        if let Some(targets) = cx.delta.new_terrain_chunk_compass_targets(id) {
            let mut e = extra.get_or_set_array("compass_targets");
            for &pos in &targets.points {
                e.borrow().push(pos.into_value());
            }
        } else {
            extra.remove("compass_targets");
        }
    }

    fn decode(cx: &DecodeCx, id: TerrainChunkId, delta: &mut DeltaInternals) -> StringResult<()> {
        if let Some(extra) = cx.extra.get("compass_targets") {
            let extra = extra.as_array()
                .ok_or_else(|| format!("{:?}: compass_targets is not an array", id))?;

            let mut targets = CompassTargets::default();
            for view in extra.iter() {
                let val = view.as_value()
                    .ok_or_else(|| format!("{:?}: compass_targets entry is not a value", id))?;
                let pos = V2::from_value(val)
                    .ok_or_else(|| format!("{:?}: compass_targets entry is not a V2", id))?;
                targets.points.push(pos);
            }

            delta.flags.terrain_chunk_mut(id).insert(flags::TC_COMPASS_TARGETS);
            delta.new.insert_terrain_chunk_compass_targets(id, Some(targets));
        }
        Ok(())
    }
}

impl Component<StructureId> for ParentVaultTag {
    fn encode(cx: &EncodeCx, id: StructureId, extra: &mut Extra) {
        if let Some(vid) = cx.delta.new_structure_parent_vault(id) {
            extra.set("parent_vault", vid.into_value());
        } else {
            extra.remove("parent_vault");
        }
    }

    fn decode(cx: &DecodeCx, id: StructureId, delta: &mut DeltaInternals) -> StringResult<()> {
        if let Some(extra) = cx.extra.get("parent_vault") {
            let val = extra.as_value()
                .ok_or_else(|| format!("{:?}: parent_vault is not a value", id))?;
            let vid = V2::from_value(val)
                .ok_or_else(|| format!("{:?}: parent_vault is not a V2", id))?;

            delta.flags.structure_mut(id).insert(flags::S_PARENT_VAULT);
            delta.new.insert_structure_parent_vault(id, Some(vid));
        }
        Ok(())
    }
}
