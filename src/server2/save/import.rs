use server_types::*;
use std::collections::HashSet;
use std::collections::hash_map::{HashMap, Entry};

use common::util::StringResult;
use engine::update::{Delta, flags};
use engine::update::data_map::ObjectType;
use engine::update::delta::DeltaInternals;
use server_bundle::types as b;
use server_bundle::types::Bundle;
use server_config::data;
use server_extra::Extra;
use syntax_exts::for_each_obj_named_macro_safe;
use world::objects::*;

use save::components::{Component, DecodeCx};

pub struct Importer<'a> {
    bundle: &'a Bundle,

    /// Lookup table for mapping stable PlaneIds, which appear in entity/structure bundles, to the
    /// transient PlaneIds used in the corresponding world objects.
    plane_map: HashMap<Stable<PlaneId>, PlaneId>,
    /// List of `PlaneRef`s to include in the generated delta.  These are used when an object
    /// refers (by StableId) to a plane not in the input bundle.
    plane_refs: Vec<(PlaneId, Stable<PlaneId>)>,
    /// Next PlaneId value to use for generated plane refs.
    next_plane_ref: u32,
}

impl<'a> Importer<'a> {
    pub fn new(bundle: &'a Bundle) -> Importer<'a> {
        let plane_map = bundle.planes.iter().enumerate()
            .filter_map(|(idx, p)| {
                if p.stable_id != NO_STABLE_ID {
                    Some((Stable::new(p.stable_id), PlaneId(idx as u32)))
                } else {
                    None
                }
            }).collect();

        Importer {
            bundle: bundle,

            plane_map: plane_map,
            plane_refs: Vec::new(),
            // Within the generated delta, plane IDs `0 .. planes.len()` refer to planes included
            // in the delta itself, while IDs `planes.len() ..` are StableId references only.
            next_plane_ref: bundle.planes.len() as u32,
        }
    }


    pub fn import_block_id(&mut self, id: BlockId) -> StringResult<BlockId> {
        let name = &mut self.bundle.blocks.get(id as usize)
            .ok_or_else(|| format!("bundle block ID out of range: {}", id))?;
        let id = data().get_block_id(name)
            .ok_or_else(|| format!("no such block: {:?}", name))?;
        Ok(id)
    }

    pub fn import_template_id(&mut self, id: TemplateId) -> StringResult<TemplateId> {
        let name = &mut self.bundle.templates.get(id as usize)
            .ok_or_else(|| format!("bundle template ID out of range: {}", id))?;
        let id = data().get_template_id(name)
            .ok_or_else(|| format!("no such template: {:?}", name))?;
        Ok(id)
    }

    pub fn import_item_id(&mut self, id: ItemId) -> StringResult<ItemId> {
        let name = &mut self.bundle.items.get(id as usize)
            .ok_or_else(|| format!("bundle item ID out of range: {}", id))?;
        let id = data().get_item_id(name)
            .ok_or_else(|| format!("no such item: {:?}", name))?;
        Ok(id)
    }


    pub fn import_plane_id(&mut self, stable_id: Stable<PlaneId>) -> StringResult<PlaneId> {
        match self.plane_map.entry(stable_id) {
            Entry::Occupied(e) => Ok(*e.get()),
            Entry::Vacant(e) => {
                let id = PlaneId(self.next_plane_ref);
                self.next_plane_ref += 1;
                e.insert(id);
                self.plane_refs.push((id, stable_id));
                Ok(id)
            },
        }
    }


    pub fn import_bundle(&mut self) -> StringResult<Delta> {
        let mut d = DeltaInternals::new();

        if let Some(ref w) = self.bundle.world {
            d.flags.world_mut(()).insert(expand_objs! { flags::W_TIME $(| flags::$W_NEXT_OBJ)* });
            d.new.insert_world_time((), w.now);
            for_each_obj! {
                d.new.insert_world_next_stable_id(ObjectType::$Obj, w.$next_obj);
            }
            self.import_world_extra(&mut d, &w.extra)?;
        }

        for_each_obj! {
            for (idx, obj) in self.bundle.$objs.iter().enumerate() {
                let id = $ObjId(idx as _);
                d.flags.$obj_mut(id).insert(flags::$O_CREATED);
                d.new.$insert_obj(id, self.$import_obj(obj)?);
                self.$import_obj_extra(&mut d, id, &obj.extra)?;
            }
        }

        for &(id, stable_id) in &self.plane_refs {
            d.new.insert_plane_ref(id, stable_id);
        }

        Ok(d.into())
    }


    fn import_client(&mut self, b: &b::Client) -> StringResult<Client> {
        Ok(Client {
            name: (*b.name).to_owned(),
            pawn: b.pawn,

            stable_id: b.stable_id,
            //flags: b.flags,       // TODO - bundle client doesn't have flags yet
            // `World` clears child object sets on insert, since it maintains those itself, but
            // it's happy to accept an empty set preallocated to the correct size.
            child_entities: HashSet::with_capacity(b.child_entities.len()),
            child_inventories: HashSet::with_capacity(b.child_inventories.len()),

            .. Client::default()
        })
    }

    pub fn import_entity(&mut self, b: &b::Entity) -> StringResult<Entity> {
        fn facing_to_dir(f: V3) -> u8 {
            let idx = ((f.y.signum() + 1) * 3) + (f.x.signum() + 1);
            [5, 6, 7,
             4, 0, 0,
             3, 2, 1][idx as usize]
        }

        let activity =
            if b.target_velocity == 0 {
                Activity::Stand {
                    pos: b.motion.start_pos,
                    dir: facing_to_dir(b.facing),
                }
            } else {
                Activity::Walk {
                    pos: b.motion.start_pos,
                    velocity: b.motion.velocity,
                    dir: facing_to_dir(b.facing),
                }
            };

        Ok(Entity {
            plane: self.import_plane_id(b.stable_plane)?,

            activity: activity,
            activity_start: b.motion.start_time,
            appearance: b.appearance,

            stable_id: b.stable_id,
            attachment: b.attachment,
            child_inventories: HashSet::with_capacity(b.child_inventories.len()),

            .. Entity::default()
        })
    }

    pub fn import_inventory(&mut self, b: &b::Inventory) -> StringResult<Inventory> {
        let mut contents = Vec::with_capacity(b.contents.len());
        for it in b.contents.iter() {
            let item_id = self.import_item_id(it.id)?;
            if item_id == NO_ITEM || it.count == 0 {
                contents.push(Item::none());
            } else {
                contents.push(Item::new(item_id, it.count));
            }
        }

        Ok(Inventory {
            contents: contents.into_boxed_slice(),

            stable_id: b.stable_id,
            flags: b.flags,
            attachment: b.attachment,

            .. Inventory::default()
        })
    }

    pub fn import_plane(&mut self, b: &b::Plane) -> StringResult<Plane> {
        Ok(Plane {
            // TODO: bundle contains `name`, but world doesn't

            stable_id: b.stable_id,
            //flags: b.flags,       // TODO - bundle plane doesn't have flags yet

            .. Plane::default()
        })
    }

    pub fn import_terrain_chunk(&mut self, b: &b::TerrainChunk) -> StringResult<TerrainChunk> {
        let mut blocks = Box::new(EMPTY_CHUNK);
        for i in 0 .. blocks.len() {
            blocks[i] = self.import_block_id(b.blocks[i])?;
        }

        Ok(TerrainChunk {
            plane: self.import_plane_id(b.stable_plane)?,
            cpos: b.cpos,
            blocks: blocks,

            stable_id: b.stable_id,
            flags: b.flags,
            child_structures: HashSet::with_capacity(b.child_structures.len()),

            .. TerrainChunk::default()
        })
    }

    pub fn import_structure(&mut self, b: &b::Structure) -> StringResult<Structure> {
        Ok(Structure {
            plane: self.import_plane_id(b.stable_plane)?,
            pos: b.pos,
            template: self.import_template_id(b.template)?,

            stable_id: b.stable_id,
            flags: b.flags,
            attachment: b.attachment,
            child_inventories: HashSet::with_capacity(b.child_inventories.len()),

            .. Structure::default()
        })
    }

    fn import_world_extra(&mut self,
                          d: &mut DeltaInternals,
                          extra: &Extra) -> StringResult<()> {
        let cx = DecodeCx {
            extra,
        };
        for_each_obj_named_macro_safe! { [[world_components]]
            <#Tag as Component<()>>::decode(&cx, (), d)?;
        }
        Ok(())
    }

    for_each_obj! {
        fn $import_obj_extra(&mut self,
                             d: &mut DeltaInternals,
                             id: $ObjId,
                             extra: &Extra) -> StringResult<()> {
            let cx = DecodeCx {
                extra,
            };
            for_each_obj_named_macro_safe! { [[$obj_components]]
                <#Tag as Component<$ObjId>>::decode(&cx, id, d)?;
            }
            Ok(())
        }
    }
}
