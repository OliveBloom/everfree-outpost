use server_types::*;
use server_world_types::*;
use std::collections::hash_map::{HashMap, Entry};
use std::collections::HashSet;
use std::hash::Hash;
use std::i32;
use std::marker::PhantomData;
use std::mem;
use std::ops::{Index, IndexMut};
use log::LogLevel;

use common::Activity;
use common::util;
use engine::component::crafting::CraftingState;
use engine::update::{self, Delta, UpdateItem, ObjectType, flags};
use engine::update::event::EventRef;
use server_config::{data, Data};
use server_extra::{self, Extra, AnyValue};
use syntax_exts::for_each_obj_named;

use server_bundle::types as b;
use save::objects as s;
use save::components::{Component, EncodeCx};
use save::events;



trait VecMapKey: Sized + Copy + Hash + Eq {
    fn to_usize(self) -> usize;
    fn from_usize(x: usize) -> Self;
}

for_each_obj! {
    impl VecMapKey for $ObjId {
        fn to_usize(self) -> usize {
            self.unwrap() as usize
        }

        fn from_usize(x: usize) -> $ObjId {
            $ObjId(x as _)
        }
    }
}

impl VecMapKey for u16 {
    fn to_usize(self) -> usize {
        self as usize
    }

    fn from_usize(x: usize) -> u16 {
        x as u16
    }
}

impl VecMapKey for u32 {
    fn to_usize(self) -> usize {
        self as usize
    }

    fn from_usize(x: usize) -> u32 {
        x as u32
    }
}


struct VecMap<K, V> {
    data: Vec<V>,
    free: HashSet<usize>,
    _marker: PhantomData<K>,
}

impl<K: VecMapKey, V: Default> VecMap<K, V> {
    pub fn new() -> VecMap<K, V> {
        VecMap {
            data: Vec::new(),
            free: HashSet::new(),
            _marker: PhantomData,
        }
    }

    pub fn len(&self) -> usize {
        self.data.len() - self.free.len()
    }

    pub fn insert(&mut self, key: K, value: V) -> &mut V {
        let rk = key.to_usize();
        if rk >= self.data.len() {
            self.data.resize_default(rk + 1);
        }
        self.free.remove(&rk);
        self.data[rk] = value;
        &mut self.data[rk]
    }

    pub fn remove(&mut self, key: K) -> V {
        let rk = key.to_usize();
        if rk >= self.data.len() {
            return V::default();
        }
        let old_v = mem::replace(&mut self.data[rk], V::default());
        self.free.insert(rk);
        self.compact();
        old_v
    }

    fn compact(&mut self) {
        // Pop elements from `self.data` as long as the last valid index is in `self.free`.
        let mut new_len = self.data.len();
        while new_len > 0 {
            let last = new_len - 1;
            if !self.free.contains(&last) {
                break;
            }
            self.free.remove(&last);
            new_len -= 1;
        }

        if new_len < self.data.len() {
            self.data.truncate(new_len);

            if self.data.capacity() > self.data.len() * 2 {
                self.data.shrink_to_fit();
            }
        }
    }

    /// Look up the value for a given key.  This function *may* return `None` if the key is
    /// invalid, but is not guaranteed to do so (though it will always return a valid reference).
    pub fn get(&self, key: K) -> Option<&V> {
        self.data.get(key.to_usize())
    }

    pub fn get_mut(&mut self, key: K) -> Option<&mut V> {
        self.data.get_mut(key.to_usize())
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item=(K, &'a V)> {
        let free = &self.free;
        self.data.iter().enumerate()
            .filter(move |(idx, _)| !free.contains(&idx))
            .map(|(idx, v)| (K::from_usize(idx), v))
    }
}

impl<K: VecMapKey, V: Default> Index<K> for VecMap<K, V> {
    type Output = V;

    fn index(&self, idx: K) -> &V {
        self.get(idx)
            .expect("key not found")
    }
}

impl<K: VecMapKey, V: Default> IndexMut<K> for VecMap<K, V> {
    fn index_mut(&mut self, idx: K) -> &mut V {
        self.get_mut(idx)
            .expect("key not found")
    }
}


expand_objs! {
    pub struct ExportWorld {
        now: Time,
        plane_map: VecMap<PlaneId, Option<Stable<PlaneId>>>,

        /// Bundles that have changed since they were last written to disk.
        dirty_bundles: HashSet<BundleKey>,

        /// Time-dependent objects.  This is used for things like currently-moving entities, where
        /// the position to be written to disk changes over time, even in the absence of explicit
        /// delta updates.
        time_dep_objects: HashSet<AnyId>,

        world: s::World,
        $( $objs: VecMap<$ObjId, s::$Obj>, )*
    }
}

/// Check whether a given object update should mark the containing bundle as dirty.  Loading and
/// unloading an object don't make it dirty; creation, destruction, and all other changes do.
///
/// Note that attachment changes also dirty the *previous* bundle, not just the current one.  This
/// is handled as a special case inside `apply`.
fn makes_dirty(bits: update::Bits) -> bool {
    let lifecycle = flags::GenericUpdate::from_bits_truncate(bits) & flags::X_LIFECYCLE_MASK;
    if lifecycle.contains(flags::X_CREATED) && !lifecycle.contains(flags::X_LOADED) {
        return true;
    }
    if lifecycle.contains(flags::X_DESTROYED) && !lifecycle.contains(flags::X_UNLOADED) {
        return true;
    }

    let change = bits & !flags::X_LIFECYCLE_MASK.bits();
    if change != 0 {
        return true;
    }

    false
}

impl ExportWorld {
    pub fn new() -> ExportWorld {
        expand_objs! {
            ExportWorld {
                now: 0,
                plane_map: VecMap::new(),
                dirty_bundles: HashSet::new(),
                time_dep_objects: HashSet::new(),

                world: s::World::default(),
                $($objs: VecMap::new(),)*
            }
        }
    }

    pub fn is_empty(&self) -> bool {
        self.clients.len() == 0 && self.planes.len() == 0
    }

    /// Apply a `Delta` to the current world state.  Returns a list of bundles to export,
    /// corresponding to `X_UNLOADED` events in `delta`.
    pub fn apply(&mut self, now: Time, delta: &Delta) -> Vec<(BundleKey, b::Bundle)> {
        if log_enabled!(LogLevel::Debug) {
            let items = delta.items().collect::<Vec<_>>();
            let events = delta.iter_events().collect::<Vec<_>>();
            if items.len() > 0 || events.len() > 0 {
                trace!("apply update at {}\nflags: {:?}\nevents: {:?}", now, items, events);
            }
        }

        // Update plane map first.
        for it in delta.items() {
            match it {
                UpdateItem::Plane(id, f) => {
                    if f.contains(flags::P_CREATED) {
                        let p = delta.new_plane(id);
                        self.plane_map.insert(id, Some(Stable::new(p.stable_id)));
                    }
                },
                _ => {},
            }
        }

        // Handle attachment changes.  When an object changes attachment, both the old and new
        // bundles need to be marked dirty.  The big case below handles the new bundle, and this
        // loop handles the old one.

        for it in delta.items() {
            match it {
                UpdateItem::World(..) |
                UpdateItem::Client(..) |
                UpdateItem::Plane(..) |
                UpdateItem::TerrainChunk(..) => {},

                UpdateItem::Entity(id, f) => {
                    if f.contains(flags::E_ATTACHMENT) && !f.contains(flags::E_CREATED) {
                        let bk = self.entity_bundle_key(id, None);
                        self.dirty_bundles.insert(bk);
                    }
                },

                UpdateItem::Inventory(id, f) => {
                    if f.contains(flags::I_ATTACHMENT) && !f.contains(flags::I_CREATED) {
                        let bk = self.inventory_bundle_key(id, None);
                        self.dirty_bundles.insert(bk);
                    }
                },

                UpdateItem::Structure(id, f) => {
                    if f.contains(flags::S_ATTACHMENT) && !f.contains(flags::S_CREATED) {
                        let bk = self.structure_bundle_key(id, None);
                        self.dirty_bundles.insert(bk);
                    }
                },
            }
        }

        // Update world time.  The engine doesn't normally include `W_TIME` updates, since the time
        // typically changes on every single tick.  So we have to update `self.now` explicitly.
        // This step happens before the main update loop so that if the delta does contain a
        // `W_TIME` entry, it overrides our special logic.
        if self.world.now != now {
            self.world.now = now;
            self.dirty_bundles.insert(BundleKey::World);
        }


        // Apply all updates.  After this, every object in `self` has state matching `delta.new`,
        // which is the state immediately prior to destruction/unloading (for destroyed/unloaded
        // objects).  We also track which objects have changed and update the bundle `dirty` flags
        // accordingly.

        let mut dirty_objs = Vec::new();
        for it in delta.items() {
            expand_objs! {
                match it {
                    UpdateItem::World(f) => {
                        self.update_world(f, delta);
                        // Any change to the world makes it dirty.
                        self.dirty_bundles.insert(BundleKey::World);
                    },

                    $( UpdateItem::$Obj(id, f) => {
                        self.$update_obj(id, f, delta);

                        if makes_dirty(f.bits()) {
                            trace!("{:?} becomes dirty: {:?}", id, f);
                            // We can't mark the object's bundle dirty immediately, because its
                            // BundleKey can change due to updates to other (ancestor) objects.
                            // Instead we collect all dirty objects and compute their BundleKeys
                            // once all updates have been applied.
                            dirty_objs.push(AnyId::$Obj(id));
                        }
                    }, )*
                }
            }
        }


        // Apply all events.  This also updates the dirty flag.

        for er in delta.iter_events() {
            match er {
                EventRef::WardAdd(e) => {
                    if !events::ward_map_overridden(delta, e.plane) {
                        events::event_ward_add(e, self.plane_extra(e.plane));
                        trace!("{:?} becomes dirty: {:?}", e.plane, e);
                        dirty_objs.push(AnyId::Plane(e.plane));
                    }
                },
                EventRef::WardRemove(e) => {
                    if !events::ward_map_overridden(delta, e.plane) {
                        events::event_ward_remove(e, self.plane_extra(e.plane));
                        trace!("{:?} becomes dirty: {:?}", e.plane, e);
                        dirty_objs.push(AnyId::Plane(e.plane));
                    }
                },
                EventRef::PermitAdd(e) => {
                    events::event_permit_add(e, self.world_extra());
                    trace!("world becomes dirty: {:?}", e);
                    self.dirty_bundles.insert(BundleKey::World);
                },
                EventRef::PermitRemove(e) => {
                    events::event_permit_remove(e, self.world_extra());
                    trace!("world becomes dirty: {:?}", e);
                    self.dirty_bundles.insert(BundleKey::World);
                },
                EventRef::KeyValueSet(e) => {
                    if !events::key_value_overridden(delta, e.id) {
                        events::event_key_value_set(e, self.any_extra(e.id));
                        trace!("{:?} becomes dirty: {:?}", e.id, e);
                        dirty_objs.push(e.id);
                    }
                },
                EventRef::KeyValueClear(e) => {
                    if !events::key_value_overridden(delta, e.id) {
                        events::event_key_value_clear(e, self.any_extra(e.id));
                        trace!("{:?} becomes dirty: {:?}", e.id, e);
                        dirty_objs.push(e.id);
                    }
                },
                EventRef::LootedVaultsAdd(e) => {
                    if !events::looted_vaults_overridden(delta, e.plane) {
                        events::looted_vaults_add(e, self.plane_extra(e.plane));
                        dirty_objs.push(AnyId::Plane(e.plane));
                    }
                },
            }
        }


        for id in dirty_objs {
            expand_objs! {
                match id {
                    $( AnyId::$Obj(id) => {
                        let bk = self.$obj_bundle_key(id, None);
                        self.dirty_bundles.insert(bk);
                        trace!("{:?} becomes dirty due to {:?}", bk, id);
                    }, )*
                }
            }
        }

        // Handle object destruction and unloading.  Unloaded objects are collected into bundles as
        // they are destroyed.

        // A `Vec` of `BundleObject`s for each bundle.
        let mut bundle_objs = HashMap::new();

        // Map of unloaded objects to bundle keys.  Used to find the containing bundle for children
        // of objects that were already unloaded.
        let mut unloaded_map = HashMap::new();

        for it in delta.items() {
            expand_objs! {
                match it {
                    UpdateItem::World(_f) => {},     // World can't be destroyed or unloaded

                    $( UpdateItem::$Obj(id, f) => {
                        if !f.contains(flags::$O_DESTROYED) {
                            continue;
                        }

                        // In both cases, we remove the object, but the "unload" case requires some
                        // extra work before and after.  (`$obj_bundle_key` requires the object to
                        // still be in the map.)
                        if f.contains(flags::$O_UNLOADED) {
                            let bk = self.$obj_bundle_key(id, Some(&unloaded_map));
                            let v = self.$objs.remove(id);
                            if $is_obj_time_dependent(&v) {
                                self.time_dep_objects.remove(&AnyId::$Obj(id));
                            }
                            trace!("unloaded {:?} in bundle {:?} - dirty? {:?}",
                                   id, bk, self.dirty_bundles.contains(&bk));
                            if self.dirty_bundles.contains(&bk) {
                                bundle_objs.entry(bk).or_insert_with(Vec::new)
                                    .push(BundleObject::$Obj(id, v));
                            }
                            unloaded_map.insert(AnyId::$Obj(id), bk);
                        } else {
                            self.$objs.remove(id);
                        }
                    }, )*
                }
            }
        }

        let mut bundles = Vec::with_capacity(bundle_objs.len());
        for (bk, objs) in bundle_objs {
            let bundle = Builder::new(data(), self.world.now).build(objs);
            bundles.push((bk, bundle));
            self.dirty_bundles.remove(&bk);
        }

        bundles
    }

    /// Export all objects with unsaved modifications.  Afterward, all bundles are marked clean.
    pub fn export_dirty(&mut self) -> Vec<(BundleKey, b::Bundle)> {
        // Add all time-dependent objects to `dirty_bundles`.  This ensures that we get a
        // consistent snapshot of the world state as of the current time.
        for &id in &self.time_dep_objects {
            expand_objs! {
                match id {
                    $( AnyId::$Obj(id) => {
                        let bk = self.$obj_bundle_key(id, None);
                        self.dirty_bundles.insert(bk);
                    }, )*
                }
            }
        }

        // TODO: might want to cache this (linear scan over all objects)
        let mut bundle_objs = HashMap::new();
        for_each_obj! {
            for (id, v) in self.$objs.iter() {
                let bk = self.$obj_bundle_key(id, None);
                if self.dirty_bundles.contains(&bk) {
                    bundle_objs.entry(bk).or_insert_with(Vec::new)
                        .push(BundleObject::$Obj(id, v.clone()));
                }
            }
        }

        // Add the World if it's dirty.  The loop above will handle any children of World.
        if self.dirty_bundles.contains(&BundleKey::World) {
            bundle_objs.entry(BundleKey::World).or_insert_with(Vec::new)
                .push(BundleObject::World(self.world.clone()));
        }

        let mut bundles = Vec::with_capacity(bundle_objs.len());
        for (bk, objs) in bundle_objs {
            let bundle = Builder::new(data(), self.world.now).build(objs);
            bundles.push((bk, bundle));
            self.dirty_bundles.remove(&bk);
        }

        bundles
    }

    fn update_world(&mut self,
                    flags: flags::WorldUpdate,
                    delta: &Delta) {
        if flags.contains(flags::W_TIME) {
            self.world.now = delta.new_world_time();
        }

        for_each_obj! {
            if flags.contains(flags::$W_NEXT_OBJ) {
                self.world.$next_obj = delta.new_world_next_stable_id(ObjectType::$Obj);
            }
        }

        let cx = EncodeCx::new(delta, self.now);
        for_each_obj_named! { [[world_components]]
            if flags.contains(flags::$FLAG) {
                <$Tag as Component<()>>::encode(&cx, (), &mut self.world.extra);
            }
        }
    }

    /// Update a `Client` object to match the final state described in `delta`.
    fn update_client(&mut self,
                     id: ClientId,
                     flags: flags::ClientUpdate,
                     delta: &Delta) {
        let created = flags.contains(flags::C_CREATED);

        {
            let b = if created {
                let w = delta.new_client(id);
                self.clients.insert(id, s::Client {
                    name: w.name.clone(),
                    stable_id: w.stable_id,
                    .. s::Client::default()
                })
            } else {
                self.clients.get_mut(id).unwrap()
            };

            if flags.contains(flags::C_PAWN) || created {
                let pawn = delta.new_client_pawn(id);
                b.pawn = pawn;
            }

            let cx = EncodeCx::new(delta, self.now);
            for_each_obj_named! { [[client_components]]
                if flags.contains(flags::$FLAG) || created {
                    <$Tag as Component<ClientId>>::encode(&cx, id, &mut b.extra);
                }
            }
        }

        // C_DIALOG: transient
        // C_CAMERA_POS: transient
    }

    fn update_entity(&mut self,
                     id: EntityId,
                     flags: flags::EntityUpdate,
                     delta: &Delta) {
        let created = flags.contains(flags::E_CREATED);

        {
            let b = if created {
                let w = delta.new_entity(id);
                self.entities.insert(id, s::Entity {
                    target_velocity: scalar(0),
                    stable_id: w.stable_id,
                    attachment: w.attachment,
                    .. s::Entity::default()
                })
            } else {
                self.entities.get_mut(id).unwrap()
            };

            if flags.contains(flags::E_ACTIVITY) || created {
                let (act, start_time) = delta.new_entity_activity(id);

                let old_time_dep = is_entity_time_dependent(b);

                b.activity = act.clone();
                b.act_start = start_time;
                b.facing = DIR_VEC[act.dir().unwrap_or(0) as usize];

                let new_time_dep = is_entity_time_dependent(b);

                if old_time_dep != new_time_dep {
                    if new_time_dep {
                        self.time_dep_objects.insert(AnyId::Entity(id));
                    } else {
                        self.time_dep_objects.remove(&AnyId::Entity(id));
                    }
                }

                // b.motion: time-dependent
            }

            if flags.contains(flags::E_PLANE) || created {
                let pid = delta.new_entity_plane(id);
                b.stable_plane = self.plane_map.get(pid).unwrap().unwrap();
            }

            if flags.contains(flags::E_APPEARANCE) || created {
                b.appearance = delta.new_entity_appearance(id).clone();
            }

            let cx = EncodeCx::new(delta, self.now);
            for_each_obj_named! { [[entity_components]]
                if flags.contains(flags::$FLAG) || created {
                    <$Tag as Component<EntityId>>::encode(&cx, id, &mut b.extra);
                }
            }
        }
    }

    fn update_inventory(&mut self,
                        id: InventoryId,
                        flags: flags::InventoryUpdate,
                        delta: &Delta) {
        let created = flags.contains(flags::I_CREATED);

        {
            let b = if created {
                let w = delta.new_inventory(id);
                self.inventories.insert(id, s::Inventory {
                    stable_id: w.stable_id,
                    // TODO: filter flags
                    flags: w.flags,
                    attachment: w.attachment,
                    .. s::Inventory::default()
                })
            } else {
                self.inventories.get_mut(id).unwrap()
            };

            if flags.contains(flags::I_CONTENTS) || created {
                b.contents = delta.new_inventory_contents(id)
                    .to_owned().into_boxed_slice();
            }

            let cx = EncodeCx::new(delta, self.now);
            for_each_obj_named! { [[inventory_components]]
                if flags.contains(flags::$FLAG) || created {
                    <$Tag as Component<InventoryId>>::encode(&cx, id, &mut b.extra);
                }
            }
        }
    }

    fn update_plane(&mut self,
                    id: PlaneId,
                    flags: flags::PlaneUpdate,
                    delta: &Delta) {
        let created = flags.contains(flags::P_CREATED);

        {
            let b = if created {
                let w = delta.new_plane(id);
                self.planes.insert(id, s::Plane {
                    stable_id: w.stable_id,
                    .. s::Plane::default()
                })
            } else {
                self.planes.get_mut(id).unwrap()
            };

            let _ = b;  // suppress warning

            let cx = EncodeCx::new(delta, self.now);
            for_each_obj_named! { [[plane_components]]
                if flags.contains(flags::$FLAG) || created {
                    <$Tag as Component<PlaneId>>::encode(&cx, id, &mut b.extra);
                }
            }
        }
    }

    fn update_terrain_chunk(&mut self,
                            id: TerrainChunkId,
                            flags: flags::TerrainChunkUpdate,
                            delta: &Delta) {
        let created = flags.contains(flags::TC_CREATED);

        {
            let b = if created {
                let w = delta.new_terrain_chunk(id);
                self.terrain_chunks.insert(id, s::TerrainChunk {
                    stable_plane: self.plane_map.get(w.plane).unwrap().unwrap(),
                    cpos: w.cpos,
                    stable_id: w.stable_id,
                    // TODO: filter flags
                    flags: w.flags,
                    .. s::TerrainChunk::default()
                })
            } else {
                self.terrain_chunks.get_mut(id).unwrap()
            };

            if flags.contains(flags::TC_BLOCKS) || created {
                let blocks = (delta.new_terrain_chunk_blocks(id) as &[_])
                    .to_owned().into_boxed_slice();
                b.blocks = unsafe { util::size_array::<BlockId, BlockChunk>(blocks) };
            }

            let cx = EncodeCx::new(delta, self.now);
            for_each_obj_named! { [[terrain_chunk_components]]
                if flags.contains(flags::$FLAG) || created {
                    <$Tag as Component<TerrainChunkId>>::encode(&cx, id, &mut b.extra);
                }
            }
        }
    }

    fn update_structure(&mut self,
                        id: StructureId,
                        flags: flags::StructureUpdate,
                        delta: &Delta) {
        let created = flags.contains(flags::S_CREATED);

        {
            let b = if created {
                let w = delta.new_structure(id);
                self.structures.insert(id, s::Structure {
                    stable_plane: self.plane_map.get(w.plane).unwrap().unwrap(),
                    pos: w.pos,
                    stable_id: w.stable_id,
                    // TODO: filter flags
                    flags: w.flags,
                    attachment: w.attachment,
                    .. s::Structure::default()
                })
            } else {
                self.structures.get_mut(id).unwrap()
            };

            if flags.contains(flags::S_TEMPLATE) || created {
                b.template = delta.new_structure_template(id)
            }

            if flags.contains(flags::S_CRAFTING) || created {
                // This handles only the time-dependent input parts of the crafting state.  The
                // rest is handled in the `Component for CraftingTag` impl.
                let old_time_dep = is_structure_time_dependent(b);
                if let Some((state, _inv)) = delta.new_structure_crafting(id) {
                    b.crafting_progress = match *state {
                        CraftingState::Paused { .. } => None,
                        CraftingState::Active { start_progress, start_time, .. } =>
                            Some((start_time, start_progress)),
                    };
                } else {
                    b.crafting_progress = None;
                }
                let new_time_dep = is_structure_time_dependent(b);

                if old_time_dep != new_time_dep {
                    if new_time_dep {
                        self.time_dep_objects.insert(AnyId::Structure(id));
                    } else {
                        self.time_dep_objects.remove(&AnyId::Structure(id));
                    }
                }
            }

            let cx = EncodeCx::new(delta, self.now);
            for_each_obj_named! { [[structure_components]]
                if flags.contains(flags::$FLAG) || created {
                    <$Tag as Component<StructureId>>::encode(&cx, id, &mut b.extra);
                }
            }
        }
    }


    fn client_bundle_key(&self,
                         id: ClientId,
                         _unloaded: Option<&HashMap<AnyId, BundleKey>>) -> BundleKey {
        BundleKey::Client(Stable::new_checked(self.clients[id].stable_id).unwrap())
    }

    fn entity_bundle_key(&self,
                         id: EntityId,
                         unloaded: Option<&HashMap<AnyId, BundleKey>>) -> BundleKey {
        let e = &self.entities[id];
        match e.attachment {
            EntityAttachment::World =>
                BundleKey::World,
            EntityAttachment::Chunk => {
                assert!(!is_entity_time_dependent(e));
                let cpos = e.activity.pos(e.act_start, self.world.now).px_to_cpos();
                BundleKey::TerrainChunk(e.stable_plane, cpos)
            },
            EntityAttachment::Client(cid) =>
                unloaded.and_then(|u| u.get(&AnyId::Client(cid))).cloned()
                    .unwrap_or_else(|| self.client_bundle_key(cid, unloaded)),
        }
    }

    fn inventory_bundle_key(&self,
                            id: InventoryId,
                            unloaded: Option<&HashMap<AnyId, BundleKey>>) -> BundleKey {
        let i = &self.inventories[id];
        match i.attachment {
            InventoryAttachment::World =>
                BundleKey::World,
            InventoryAttachment::Client(cid) =>
                unloaded.and_then(|u| u.get(&AnyId::Client(cid))).cloned()
                    .unwrap_or_else(|| self.client_bundle_key(cid, unloaded)),
            InventoryAttachment::Entity(eid) =>
                unloaded.and_then(|u| u.get(&AnyId::Entity(eid))).cloned()
                    .unwrap_or_else(|| self.entity_bundle_key(eid, unloaded)),
            InventoryAttachment::Structure(sid) =>
                unloaded.and_then(|u| u.get(&AnyId::Structure(sid))).cloned()
                    .unwrap_or_else(|| self.structure_bundle_key(sid, unloaded)),
        }
    }

    fn plane_bundle_key(&self,
                        id: PlaneId,
                        _unloaded: Option<&HashMap<AnyId, BundleKey>>) -> BundleKey {
        BundleKey::Plane(Stable::new_checked(self.planes[id].stable_id).unwrap())
    }

    fn terrain_chunk_bundle_key(&self,
                                id: TerrainChunkId,
                                _unloaded: Option<&HashMap<AnyId, BundleKey>>) -> BundleKey {
        let tc = &self.terrain_chunks[id];
        BundleKey::TerrainChunk(tc.stable_plane, tc.cpos)
    }

    fn structure_bundle_key(&self,
                            id: StructureId,
                            _unloaded: Option<&HashMap<AnyId, BundleKey>>) -> BundleKey {
        let s = &self.structures[id];
        match s.attachment {
            StructureAttachment::Plane =>
                BundleKey::Plane(s.stable_plane),
            StructureAttachment::Chunk => {
                let cpos = s.pos.tile_to_cpos();
                BundleKey::TerrainChunk(s.stable_plane, cpos)
            },
        }
    }


    fn world_extra(&mut self) -> &mut Extra {
        &mut self.world.extra
    }

    for_each_obj! {
        fn $obj_extra(&mut self, id: $ObjId) -> &mut Extra {
            &mut self.$objs.get_mut(id).unwrap().extra
        }
    }

    fn any_extra(&mut self, id: AnyId) -> &mut Extra {
        expand_objs! {
            match id {
                $( AnyId::$Obj(id) => self.$obj_extra(id), )*
            }
        }
    }
}


fn is_client_time_dependent(_c: &s::Client) -> bool {
    false
}

fn is_entity_time_dependent(e: &s::Entity) -> bool {
    match e.activity {
        Activity::Stand { .. } => false,
        Activity::Walk { velocity, .. } => velocity != 0,
    }
}

fn is_inventory_time_dependent(_i: &s::Inventory) -> bool {
    false
}

fn is_plane_time_dependent(_p: &s::Plane) -> bool {
    false
}

fn is_terrain_chunk_time_dependent(_tc: &s::TerrainChunk) -> bool {
    false
}

fn is_structure_time_dependent(s: &s::Structure) -> bool {
    s.crafting_progress.is_some()
}




#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum BundleKey {
    World,
    Client(Stable<ClientId>),
    Plane(Stable<PlaneId>),
    TerrainChunk(Stable<PlaneId>, V2),
}

expand_objs! {
    enum BundleObject {
        World(s::World),
        $( $Obj($ObjId, s::$Obj), )*
    }
}



struct DataMap<K, V> {
    old_to_new: HashMap<K, K>,
    data: Vec<V>,
}

impl<K: VecMapKey, V> DataMap<K, V> {
    pub fn new() -> DataMap<K, V> {
        DataMap {
            old_to_new: HashMap::new(),
            data: Vec::new(),
        }
    }

    pub fn lookup<F: FnOnce(K) -> V>(&mut self, old: K, mk_data: F) -> K {
        match self.old_to_new.entry(old) {
            Entry::Occupied(e) => *e.get(),
            Entry::Vacant(e) => {
                let new = K::from_usize(self.data.len());
                self.data.push(mk_data(old));
                e.insert(new);
                new
            },
        }
    }

    pub fn take_data(&mut self) -> Vec<V> {
        self.old_to_new = HashMap::new();
        mem::replace(&mut self.data, Vec::new())
    }
}

fn data_names<K: VecMapKey>(dm: &mut DataMap<K, &str>) -> Box<[Box<str>]> {
    dm.take_data().into_iter()
        .map(|s| s.to_owned().into_boxed_str())
        .collect::<Vec<_>>().into_boxed_slice()
}

expand_objs! {
    /// `Builder` takes a `Data` reference so that it can also keep references to `data`'s
    /// contents.  Of course, this means `Builder`s shouldn't outlive their parent stack frames.
    struct Builder<'d> {
        data: &'d Data,
        now: Time,

        blocks: DataMap<BlockId, &'d str>,
        items: DataMap<ItemId, &'d str>,
        templates: DataMap<TemplateId, &'d str>,

        $( $obj_ids: HashMap<$ObjId, $ObjId>, )*
    }
}

impl<'d> server_extra::Visitor for Builder<'d> {
    for_each_obj! {
        fn $visit_obj_id(&mut self, id: &mut $ObjId) {
            *id = self.$map_obj_id(*id);
        }
    }
}

impl<'d> Builder<'d> {
    pub fn new(data: &'d Data, now: Time) -> Builder<'d> {
        expand_objs! {
            Builder {
                data: data,
                now: now,

                blocks: DataMap::new(),
                items: DataMap::new(),
                templates: DataMap::new(),

                $( $obj_ids: HashMap::new(), )*
            }
        }
    }

    fn map_block_id(&mut self, id: BlockId) -> BlockId {
        let data = self.data;
        self.blocks.lookup(id, |id| data.block(id).name())
    }

    fn map_item_id(&mut self, id: ItemId) -> ItemId {
        let data = self.data;
        self.items.lookup(id, |id| data.item(id).name())
    }

    fn map_template_id(&mut self, id: TemplateId) -> TemplateId {
        let data = self.data;
        self.templates.lookup(id, |id| data.template(id).name())
    }

    for_each_obj! {
        fn $map_obj_id(&self, id: $ObjId) -> $ObjId {
            self.$obj_ids.get(&id).cloned()
                .unwrap_or(<$ObjId as VecMapKey>::from_usize(-1_isize as usize))
        }
    }

    fn map_entity_attachment(&self, attach: EntityAttachment) -> EntityAttachment {
        match attach {
            EntityAttachment::World =>
                EntityAttachment::World,
            EntityAttachment::Chunk =>
                EntityAttachment::Chunk,
            EntityAttachment::Client(cid) =>
                EntityAttachment::Client(self.map_client_id(cid)),
        }
    }

    fn map_inventory_attachment(&self, attach: InventoryAttachment) -> InventoryAttachment {
        match attach {
            InventoryAttachment::World =>
                InventoryAttachment::World,
            InventoryAttachment::Client(cid) =>
                InventoryAttachment::Client(self.map_client_id(cid)),
            InventoryAttachment::Entity(cid) =>
                InventoryAttachment::Entity(self.map_entity_id(cid)),
            InventoryAttachment::Structure(cid) =>
                InventoryAttachment::Structure(self.map_structure_id(cid)),
        }
    }

    fn map_structure_attachment(&self, attach: StructureAttachment) -> StructureAttachment {
        match attach {
            StructureAttachment::Plane =>
                StructureAttachment::Plane,
            StructureAttachment::Chunk =>
                StructureAttachment::Chunk,
        }
    }

    fn map_extra(&mut self, mut e: Extra) -> Extra {
        e.traverse(self);
        e
    }

    expand_objs! {
        pub fn build(&mut self, objs: Vec<BundleObject>) -> b::Bundle {
            // Build object ID maps first.
            for obj in &objs {
                match *obj {
                    BundleObject::World(_) => {},
                    $( BundleObject::$Obj(id, _) => {
                        let new_id = <$ObjId as VecMapKey>::from_usize(self.$obj_ids.len());
                        self.$obj_ids.insert(id, new_id);
                    }, )*
                }
            }

            let mut world = None;
            $( let mut $objs = Vec::with_capacity(self.$obj_ids.len()); )*
            for obj in objs {
                match obj {
                    BundleObject::World(s) => {
                        assert!(world.is_none(), "can't have multiple worlds in one bundle");
                        world = Some(Box::new(self.build_world(s)));
                    },
                    $( BundleObject::$Obj(_id, s) => {
                        $objs.push(self.$build_obj(s));
                    }, )*
                }
            }

            b::Bundle {
                anims: Vec::new().into_boxed_slice(),
                items: data_names(&mut self.items),
                blocks: data_names(&mut self.blocks),
                templates: data_names(&mut self.templates),

                world: world,
                $( $objs: $objs.into_boxed_slice(), )*
            }
        }
    }

    pub fn build_world(&mut self, s: s::World) -> b::World {
        expand_objs! {
            b::World {
                now: s.now,

                $( $next_obj: s.$next_obj, )*

                extra: self.map_extra(s.extra),
                child_entities: Vec::new().into_boxed_slice(),
                child_inventories: Vec::new().into_boxed_slice(),
            }
        }
    }

    pub fn build_client(&mut self, s: s::Client) -> b::Client {
        b::Client {
            name: s.name.into_boxed_str(),
            pawn: s.pawn.map(|id| self.map_entity_id(id)),

            extra: self.map_extra(s.extra),
            stable_id: s.stable_id,
            child_entities: Vec::new().into_boxed_slice(),
            child_inventories: Vec::new().into_boxed_slice(),
        }
    }

    pub fn build_entity(&mut self, s: s::Entity) -> b::Entity {
        let pos = s.activity.pos(s.act_start, self.now);
        let motion = Motion::stationary(pos, self.now);

        b::Entity {
            stable_plane: s.stable_plane,

            motion: motion,
            anim: 0,
            facing: s.facing,
            target_velocity: s.target_velocity,
            appearance: s.appearance,

            extra: self.map_extra(s.extra),
            stable_id: s.stable_id,
            attachment: self.map_entity_attachment(s.attachment),
            child_inventories: Vec::new().into_boxed_slice(),
        }
    }

    fn map_item_list(&mut self, mut item_list: Box<[Item]>) -> Box<[Item]> {
        for it in item_list.iter_mut() {
            it.id = self.map_item_id(it.id);
        }
        item_list
    }

    pub fn build_inventory(&mut self, s: s::Inventory) -> b::Inventory {
        b::Inventory {
            contents: self.map_item_list(s.contents),

            extra: self.map_extra(s.extra),
            stable_id: s.stable_id,
            flags: s.flags,
            attachment: self.map_inventory_attachment(s.attachment),
        }
    }

    pub fn build_plane(&mut self, s: s::Plane) -> b::Plane {
        b::Plane {
            name: s.name.into_boxed_str(),

            extra: self.map_extra(s.extra),
            stable_id: s.stable_id,
        }
    }

    fn map_block_chunk(&mut self, mut blocks: Box<BlockChunk>) -> Box<BlockChunk> {
        for b in blocks.iter_mut() {
            *b = self.map_block_id(*b);
        }
        blocks
    }

    pub fn build_terrain_chunk(&mut self,
                               s: s::TerrainChunk) -> b::TerrainChunk {
        b::TerrainChunk {
            stable_plane: s.stable_plane,
            cpos: s.cpos,
            blocks: self.map_block_chunk(s.blocks),

            extra: self.map_extra(s.extra),
            stable_id: s.stable_id,
            flags: s.flags,
            child_structures: Vec::new().into_boxed_slice(),
        }
    }

    pub fn build_structure(&mut self, s: s::Structure) -> b::Structure {
        let mut extra = self.map_extra(s.extra);

        if let Some((start_time, start_progress)) = s.crafting_progress {
            let delta = self.now - start_time;
            let progress =
                if delta < 0 {
                    start_progress
                } else if delta >= (i32::MAX - start_progress as i32) as Time {
                    // Cap at i32::MAX instead of u32::MAX because Time might be i32.
                    i32::MAX as u32
                } else {
                    start_progress + delta as u32
                };
            extra.get_or_set_hash("crafting")
                 .set("progress", (progress as i64).into_value());
        }

        b::Structure {
            stable_plane: s.stable_plane,
            pos: s.pos,
            template: self.map_template_id(s.template),

            extra: extra,
            stable_id: s.stable_id,
            flags: s.flags,
            attachment: self.map_structure_attachment(s.attachment),
            child_inventories: Vec::new().into_boxed_slice(),
        }
    }
}
