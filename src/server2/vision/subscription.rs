use server_types::*;
use std::collections::HashMap;

use common::util::pubsub::{PubSub, DirectSub};
use engine::component::dialog::{Dialog, DialogImpl};
use engine::component::dialog::SpecialSub;
use engine::update::Delta;

use messages::ClientResponse;

use super::AppearGone;
use super::{Location, loc};
use super::SendWrapper;
use super::ViewableId;
use super::world::*;

/// All the subscription-handling and message-sending parts of `Vision`.
///
/// The general rule here is that a `WireId` can be subscribed to a `ViewableId` only when that
/// `ViewableId` exists.  This is easy for the stuff in `ps`.  For `direct_ps` it's a little
/// harder, since there is no intermediate between the `WireId` and `ViewableId`, and in fact there
/// are a few exceptions:
///
///  * A wire that's bound to a client is always subscribed to its `ClientId`, even when the client
///    object doesn't exist.  There is special logic in `subscribe`/`unsubscribe`/`update_wire` to
///    ensure that we post appear/gone notifications only when the client is created/destroyed, so
///    calling code can basically treat it like a normal subscription.
///
///  * `SpecialSub::CraftingState` is special in that the crafting state can legitimately be `None`
///    ("not currenly crafting") while the crafting dialog is open.  Separately tracking "watching
///    for creation/destruction of crafting state" and "watching for changes in currently-existing
///    crafting state" would be annoying.  Instead, we post appear/gone messages only when the
///    subscription begins/ends, and it's up to the calling code to deal with transitions to/from
///    `None` states.
pub struct Subscriptions {
    pub ps: PubSub<ViewableId, Location, WireId>,
    pub direct_ps: DirectSub<ViewableId, WireId>,

    pub wire_client: HashMap<WireId, ClientId>,

    pub to_conn: SendWrapper,
}

impl Subscriptions {
    fn send_no_appear(&mut self,
                      ag: &AppearGone,
                      id: ViewableId,
                      mk_msg: impl Fn() -> ClientResponse) {
        let to_conn = &mut self.to_conn;
        self.ps.message(&id, |_, &wire_id| {
            if ag.appear.contains(&(wire_id, id)) {
                return;
            }
            to_conn.send(wire_id, mk_msg());
        });
    }

    fn send_direct_no_appear(&mut self,
                             ag: &AppearGone,
                             id: ViewableId,
                             mk_msg: impl Fn() -> ClientResponse) {
        let to_conn = &mut self.to_conn;
        self.direct_ps.message(&id, |_, &wire_id| {
            if ag.appear.contains(&(wire_id, id)) {
                return;
            }
            to_conn.send(wire_id, mk_msg());
        });
    }

    pub fn send_client_no_appear(&mut self,
                                 ag: &AppearGone,
                                 id: ClientId,
                                 mk_msg: impl Fn() -> ClientResponse) {
        self.send_direct_no_appear(ag, ViewableId::Client(id), mk_msg);
    }

    pub fn send_entity_no_appear(&mut self,
                                 ag: &AppearGone,
                                 id: EntityId,
                                 mk_msg: impl Fn() -> ClientResponse) {
        self.send_no_appear(ag, ViewableId::Entity(id), mk_msg);
    }

    pub fn send_pawn_no_appear(&mut self,
                               ag: &AppearGone,
                               id: EntityId,
                               mk_msg: impl Fn() -> ClientResponse) {
        self.send_direct_no_appear(ag, ViewableId::PawnEntity(id), mk_msg);
    }

    pub fn send_inventory_no_appear(&mut self,
                                 ag: &AppearGone,
                                 id: InventoryId,
                                 mk_msg: impl Fn() -> ClientResponse) {
        self.send_direct_no_appear(ag, ViewableId::Inventory(id), mk_msg);
    }

    pub fn send_terrain_chunk_no_appear(&mut self,
                                        ag: &AppearGone,
                                        id: TerrainChunkId,
                                        mk_msg: impl Fn() -> ClientResponse) {
        self.send_no_appear(ag, ViewableId::TerrainChunk(id), mk_msg);
    }

    pub fn send_structure_no_appear(&mut self,
                                    ag: &AppearGone,
                                    id: StructureId,
                                    mk_msg: impl Fn() -> ClientResponse) {
        self.send_no_appear(ag, ViewableId::Structure(id), mk_msg);
    }

    pub fn send_special_sub_no_appear(&mut self,
                                      ag: &AppearGone,
                                      id: SpecialSub,
                                      mk_msg: impl Fn() -> ClientResponse) {
        self.send_direct_no_appear(ag, ViewableId::SpecialSub(id), mk_msg);
    }


    pub fn for_client_wires<F: FnMut(WireId)>(&self, id: ClientId, mut f: F) {
        self.direct_ps.message(&ViewableId::Client(id), |_, &wire_id| f(wire_id));
    }

    pub fn for_pawn_entity_wires<F: FnMut(WireId)>(&self, id: EntityId, mut f: F) {
        self.direct_ps.message(&ViewableId::PawnEntity(id), |_, &wire_id| f(wire_id));
    }


    pub fn send_client_camera_setting_no_appear(&mut self,
                                                ag: &AppearGone,
                                                id: ClientId,
                                                c: &Client) {
        // TODO: should camera take precedence over pawn, or the other way around?
        if let Some((_, pos)) = c.camera_pos {
            self.send_client_no_appear(ag, id,
                || ClientResponse::CameraFixed(pos));
        } else if let Some(pawn_id) = c.pawn {
            self.send_client_no_appear(ag, id,
                || ClientResponse::CameraPawn(pawn_id));
        } else {
            // No camera - set it to a reasonable default.
            self.send_client_no_appear(ag, id,
                || ClientResponse::CameraFixed(scalar(0)));
        }
    }


    fn sub_change_old<'a>(&'a mut self,
                          ag: &'a mut AppearGone,
                          w: &'a World) -> SubChange<'a, &'a World> {
        SubChange { w, ag, sub: self }
    }

    fn sub_change_new<'a>(&'a mut self,
                          ag: &'a mut AppearGone,
                          w: &'a World,
                          delta: &'a Delta) -> SubChange<'a, NewWorld<'a>> {
        let w = NewWorld::new(w, delta);
        SubChange { w, ag, sub: self }
    }

    pub fn subscribe_wire(&mut self,
                          ag: &mut AppearGone,
                          w: &World,
                          wire_id: WireId,
                          cid: ClientId) {
        debug!("subscribe_wire {:?} -> {:?}", wire_id, cid);

        let first = self.direct_ps.subscribe(wire_id, ViewableId::Client(cid));
        if !first {
            error!("subscribe_wire: duplicate subscription: {:?} -> {:?}", wire_id, cid);
            return;
        }

        self.wire_client.insert(wire_id, cid);

        let mut sc = self.sub_change_old(ag, w);
        if let Some(c) = sc.w.get_client(cid) {
            sc.ag.appear(wire_id, ViewableId::Client(cid));
            sc.subscribe_client_children(wire_id, c);
        }
    }

    pub fn unsubscribe_wire(&mut self,
                            ag: &mut AppearGone,
                            w: &World,
                            wire_id: WireId) {
        let cid = unwrap_or_error!(self.wire_client.remove(&wire_id),
                                   "unsubscribe_wire: unknown {:?}", wire_id);
        debug!("unsubscribe_wire {:?} -> {:?}", wire_id, cid);

        let last = self.direct_ps.unsubscribe(wire_id, ViewableId::Client(cid));
        if !last {
            error!("unsubscribe_wire: duplicate subscription: {:?} -> {:?}", wire_id, cid);
            // Put the wire_client entry back for the next unsubscribe
            self.wire_client.insert(wire_id, cid);
            return;
        }

        let mut sc = self.sub_change_old(ag, w);
        if let Some(c) = sc.w.get_client(cid) {
            sc.ag.gone(wire_id, ViewableId::Client(cid));
            sc.unsubscribe_client_children(wire_id, c);
        }
    }

    pub fn update_view(&mut self,
                       ag: &mut AppearGone,
                       w: &World,
                       d: &Delta,
                       wire_id: WireId) {
        let cid = *unwrap_or_error!(self.wire_client.get(&wire_id),
                                    "update_view: unknown {:?}", wire_id);

        // We handle engine/vision/conn asynchrony by allowing a Wire to be subscribed to a client
        // that doesn't exist yet, or to remain subscribed to one that's already been destroyed.
        // The Wire->Client subscription remains in `direct_ps` throughout, but all the other
        // subscriptions depend on having a client object present.

        {
            let mut sc = self.sub_change_new(ag, w, d);
            if let Some(c) = sc.w.get_client(cid) {
                // Client entries in appear/gone need special handling, since the wire is
                // subscribed to them the entire time, whether or not the client exists.
                sc.ag.appear(wire_id, ViewableId::Client(cid));
                sc.subscribe_client_children(wire_id, c);
            }
        }

        {
            let mut sc = self.sub_change_old(ag, w);
            if let Some(c) = sc.w.get_client(cid) {
                sc.ag.gone(wire_id, ViewableId::Client(cid));
                sc.unsubscribe_client_children(wire_id, c);
            }
        }
    }

    pub fn update_entity(&mut self,
                         ag: &mut AppearGone,
                         w: &World,
                         d: &Delta,
                         id: EntityId) {
        // EntityId -> Location publisher entries are present only for entities that currently
        // exist.  If the entity doesn't exist in the old/new state, then there is nothing to
        // un/publish.
        {
            let mut sc = self.sub_change_new(ag, w, d);
            if let Some(e) = sc.w.get_entity(id) {
                sc.publish_entity(id, e);
            }
        }

        {
            let mut sc = self.sub_change_old(ag, w);
            if let Some(e) = sc.w.get_entity(id) {
                sc.unpublish_entity(id, e);
            }
        }
    }

    pub fn update_terrain_chunk(&mut self,
                                ag: &mut AppearGone,
                                w: &World,
                                d: &Delta,
                                id: TerrainChunkId) {
        {
            let mut sc = self.sub_change_new(ag, w, d);
            if let Some(tc) = sc.w.get_terrain_chunk(id) {
                sc.publish_terrain_chunk(id, tc);
            }
        }

        {
            let mut sc = self.sub_change_old(ag, w);
            if let Some(tc) = sc.w.get_terrain_chunk(id) {
                sc.unpublish_terrain_chunk(id, tc);
            }
        }
    }

    pub fn update_structure(&mut self,
                            ag: &mut AppearGone,
                            w: &World,
                            d: &Delta,
                            id: StructureId) {
        {
            let mut sc = self.sub_change_new(ag, w, d);
            if let Some(s) = sc.w.get_structure(id) {
                sc.publish_structure(id, s);
            }
        }

        {
            let mut sc = self.sub_change_old(ag, w);
            if let Some(s) = sc.w.get_structure(id) {
                sc.unpublish_structure(id, s);
            }
        }
    }
}


struct SubChange<'a, W: AnyWorld> {
    w: W,
    sub: &'a mut Subscriptions,
    ag: &'a mut AppearGone,
}

impl<'a, W: AnyWorld> SubChange<'a, W> {
    /// Subscribe a wire to everything the client can see.  The subscription to the client itself
    /// should be handled elsewhere.
    pub fn subscribe_client_children(&mut self, wire_id: WireId, c: W::Client) {
        trace!("subscribe_client_children ({:?}) - {:?}, {:?}, {:?}",
               wire_id, c.pawn(), c.dialog(), c.camera_pos());
        if let Some(pawn_id) = c.pawn() {
            let subscribe_view = c.camera_pos().is_none();
            // TODO: should camera take precedence over pawn, or the other way around?
            self.subscribe_pawn(wire_id, pawn_id, subscribe_view);
        }
        if let Some(dialog) = c.dialog() {
            self.subscribe_dialog(wire_id, dialog);
        }
        if let Some((plane, pos)) = c.camera_pos() {
            self.subscribe_location(wire_id, loc(plane, pos.px_to_cpos()));
        }
    }

    fn subscribe_pawn(&mut self, wire_id: WireId, id: EntityId, sub_view: bool) {
        let vid = ViewableId::PawnEntity(id);
        if self.sub.direct_ps.subscribe(wire_id, vid) {
            self.ag.appear(wire_id, vid);
            debug!("subscribe: {:?} -> {:?}", wire_id, id);
        }

        let e = self.w.get_entity(id).unwrap();
        if let Some(invs) = e.char_invs() {
            self.subscribe_inventory(wire_id, invs.main);
            self.subscribe_inventory(wire_id, invs.ability);
            self.subscribe_inventory(wire_id, invs.equip);
        }
        if sub_view {
            self.subscribe_location(wire_id, e.view_loc());
        }
    }

    fn subscribe_inventory(&mut self, wire_id: WireId, id: InventoryId) {
        let vid = ViewableId::Inventory(id);
        if self.sub.direct_ps.subscribe(wire_id, vid) {
            self.ag.appear(wire_id, vid);
            debug!("subscribe: {:?} -> {:?}", wire_id, id);
        }
    }

    fn subscribe_dialog(&mut self, wire_id: WireId, d: &Dialog) {
        d.with_inventories(|id| self.subscribe_inventory(wire_id, id));
        d.with_special_subs(|sub| self.subscribe_special_sub(wire_id, sub));
    }

    fn subscribe_special_sub(&mut self, wire_id: WireId, sub: SpecialSub) {
        let vid = ViewableId::SpecialSub(sub);
        if self.sub.direct_ps.subscribe(wire_id, vid) {
            self.ag.appear(wire_id, vid);
            debug!("subscribe: {:?} -> {:?}", wire_id, sub);
        }
    }

    fn subscribe_location(&mut self, wire_id: WireId, loc: Location) {
        let ag = &mut *self.ag;
        // Log only if we're actually going to subscribe
        if log_enabled!(::log::LogLevel::Debug) && !self.sub.ps.is_subscribed(wire_id, loc) {
            debug!("subscribe: {:?} -> {:?} (vision region)", wire_id, loc);
        }
        for near_loc in loc.vision_region() {
            self.sub.ps.subscribe(wire_id, near_loc, |&vid, _, _| {
                ag.appear(wire_id, vid);
            });
        }
    }


    pub fn unsubscribe_client_children(&mut self, wire_id: WireId, c: W::Client) {
        trace!("unsubscribe_client_children ({:?}) - {:?}, {:?}, {:?}",
               wire_id, c.pawn(), c.dialog(), c.camera_pos());
        if let Some(pawn_id) = c.pawn() {
            let unsubscribe_view = c.camera_pos().is_none();
            self.unsubscribe_pawn(wire_id, pawn_id, unsubscribe_view);
        }
        if let Some(dialog) = c.dialog() {
            self.unsubscribe_dialog(wire_id, dialog);
        }
        if let Some((plane, pos)) = c.camera_pos() {
            self.unsubscribe_location(wire_id, loc(plane, pos.px_to_cpos()));
        }
    }

    fn unsubscribe_pawn(&mut self, wire_id: WireId, id: EntityId, sub_view: bool) {
        let vid = ViewableId::PawnEntity(id);
        if self.sub.direct_ps.unsubscribe(wire_id, vid) {
            self.ag.gone(wire_id, vid);
            debug!("unsubscribe: {:?} -> {:?}", wire_id, id);
        }

        let e = self.w.get_entity(id).unwrap();
        if let Some(invs) = e.char_invs() {
            self.unsubscribe_inventory(wire_id, invs.main);
            self.unsubscribe_inventory(wire_id, invs.ability);
            self.unsubscribe_inventory(wire_id, invs.equip);
        }
        if sub_view {
            self.unsubscribe_location(wire_id, e.view_loc());
        }
    }

    fn unsubscribe_inventory(&mut self, wire_id: WireId, id: InventoryId) {
        let vid = ViewableId::Inventory(id);
        if self.sub.direct_ps.unsubscribe(wire_id, vid) {
            self.ag.gone(wire_id, vid);
            debug!("unsubscribe: {:?} -> {:?}", wire_id, id);
        }
    }

    fn unsubscribe_dialog(&mut self, wire_id: WireId, d: &Dialog) {
        d.with_inventories(|id| self.unsubscribe_inventory(wire_id, id));
        d.with_special_subs(|sub| self.unsubscribe_special_sub(wire_id, sub));
    }

    fn unsubscribe_special_sub(&mut self, wire_id: WireId, sub: SpecialSub) {
        let vid = ViewableId::SpecialSub(sub);
        if self.sub.direct_ps.unsubscribe(wire_id, vid) {
            self.ag.gone(wire_id, vid);
            debug!("unsubscribe: {:?} -> {:?}", wire_id, sub);
        }
    }

    fn unsubscribe_location(&mut self, wire_id: WireId, loc: Location) {
        let ag = &mut *self.ag;
        for near_loc in loc.vision_region() {
            self.sub.ps.unsubscribe(wire_id, near_loc, |&vid, _, _| {
                ag.gone(wire_id, vid);
            });
        }
        // Log only if we actually unsubscribed
        if log_enabled!(::log::LogLevel::Debug) && !self.sub.ps.is_subscribed(wire_id, loc) {
            debug!("unsubscribe: {:?} -> {:?} (vision region)", wire_id, loc);
        }
    }


    pub fn publish_entity(&mut self, id: EntityId, e: W::Entity) {
        self.publish_location(ViewableId::Entity(id), e.view_loc());
    }

    pub fn publish_terrain_chunk(&mut self, id: TerrainChunkId, tc: W::TerrainChunk) {
        self.publish_location(ViewableId::TerrainChunk(id), tc.view_loc());
    }

    pub fn publish_structure(&mut self, id: StructureId, s: W::Structure) {
        self.publish_location(ViewableId::Structure(id), s.view_loc());
    }

    fn publish_location(&mut self, vid: ViewableId, loc: Location) {
        let ag = &mut *self.ag;
        self.sub.ps.publish(vid, loc, |_, _, &wire_id| {
            ag.appear(wire_id, vid);
        });
    }


    pub fn unpublish_entity(&mut self, id: EntityId, e: W::Entity) {
        self.unpublish_location(ViewableId::Entity(id), e.view_loc());
    }

    pub fn unpublish_terrain_chunk(&mut self, id: TerrainChunkId, tc: W::TerrainChunk) {
        self.unpublish_location(ViewableId::TerrainChunk(id), tc.view_loc());
    }

    pub fn unpublish_structure(&mut self, id: StructureId, s: W::Structure) {
        self.unpublish_location(ViewableId::Structure(id), s.view_loc());
    }

    fn unpublish_location(&mut self, vid: ViewableId, loc: Location) {
        let ag = &mut *self.ag;
        self.sub.ps.unpublish(vid, loc, |_, _, &wire_id| {
            ag.gone(wire_id, vid);
        });
    }
}
