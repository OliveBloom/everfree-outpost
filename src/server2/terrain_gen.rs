use server_types::*;
use std::io::{self, Read, Write};
use std::path::Path;
use std::process::{Command, Child, Stdio, ChildStdin, ChildStdout};
use std::sync::mpsc::{Sender, Receiver};

use common::util::bytes::{ReadBytes, WriteBytes};
use common::util::warn_on_err;
use server_bundle::flat::FlatView;
use server_bundle::types::Bundle;

use engine_thread::ToEngine;
use util;


pub enum ToTerrainGen {
    InitPlane(Stable<PlaneId>, Box<GenParams>),
    ForgetPlane(Stable<PlaneId>),
    GenChunk(Stable<PlaneId>, V2),
}

pub struct GenParams {
    pub mode: u32,
}


const OP_INIT_PLANE: u32 =      0;
const OP_FORGET_PLANE: u32 =    1;
#[allow(dead_code)] const OP_GEN_PLANE: u32 =       2;
const OP_GEN_CHUNK: u32 =       3;
const OP_SHUTDOWN: u32 =        4;

struct Handle {
    child: Child,
    to_child: ChildStdin,
    from_child: ChildStdout,
}

impl Handle {
    pub fn new(base_path: &Path) -> Handle {
        let binary_path = base_path.join("bin").join("generate_terrain");

        let mut child = Command::new(binary_path)
            .arg(base_path.to_str().unwrap())
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::inherit())
            .spawn()
            .unwrap_or_else(|e| panic!("failed to spawn generate_terrain: {}", e));

        let to_child = child.stdin.take().unwrap();
        let from_child = child.stdout.take().unwrap();

        Handle {
            child: child,
            to_child: to_child,
            from_child: from_child,
        }
    }

    pub fn init_plane(&mut self,
                      plane: Stable<PlaneId>,
                      params: Box<GenParams>) -> io::Result<()> {
        try!(self.to_child.write_bytes(OP_INIT_PLANE));
        try!(self.to_child.write_bytes((plane, params.mode)));
        try!(self.to_child.flush());
        Ok(())
    }

    pub fn forget_plane(&mut self,
                        plane: Stable<PlaneId>) -> io::Result<()> {
        try!(self.to_child.write_bytes(OP_FORGET_PLANE));
        try!(self.to_child.write_bytes(plane));
        try!(self.to_child.flush());
        Ok(())
    }

    pub fn gen_chunk(&mut self, plane: Stable<PlaneId>, cpos: V2) -> io::Result<Box<Bundle>> {
        try!(self.to_child.write_bytes(OP_GEN_CHUNK));
        try!(self.to_child.write_bytes((plane, cpos)));
        try!(self.to_child.flush());

        let b = try!(read_bundle(&mut self.from_child));
        Ok(b)
    }
}

impl Drop for Handle {
    fn drop(&mut self) {
        info!("shutting down terrain gen backend");
        warn_on_err!(self.to_child.write_bytes(OP_SHUTDOWN));
        warn_on_err!(self.to_child.flush());

        // Kill the child process
        info!("waiting for subprocess");
        warn_on_err!(self.child.wait());

        info!("terrain gen has shut down");
    }
}

fn read_bundle<R: Read>(r: &mut R) -> io::Result<Box<Bundle>> {
    let len = try!(r.read_bytes::<u32>()) as usize;

    let mut buf = Vec::with_capacity(len);
    unsafe {
        assert!(buf.capacity() >= len);
        buf.set_len(len);
        try!(r.read_exact(&mut buf));
    }

    let f = try!(FlatView::from_bytes(&buf));
    let b = Box::new(f.unflatten_bundle());
    Ok(b)
}


pub fn run_terrain_gen(recv: Receiver<ToTerrainGen>,
                       to_engine: Sender<ToEngine>,
                       base_path: &Path) {
    use self::ToTerrainGen::*;

    let mut handle = Handle::new(base_path);

    for msg in recv.iter() {
        match msg {
            InitPlane(plane, params) => {
                handle.init_plane(plane, params)
                    .unwrap_or_else(|e| panic!("generate_terrain i/o error: {}", e));
            },

            ForgetPlane(plane) => {
                handle.forget_plane(plane)
                    .unwrap_or_else(|e| panic!("generate_terrain i/o error: {}", e));
            },

            GenChunk(plane, cpos) => {
                let b = handle.gen_chunk(plane, cpos)
                    .unwrap_or_else(|e| panic!("generate_terrain i/o error: {}", e));
                to_engine.send(ToEngine::TerrainChunkBundle(plane, cpos, b)).unwrap();
            },
        }
    }
    info!("input channel disconnected");
}

pub fn start_terrain_gen(recv: Receiver<ToTerrainGen>,
                         to_engine: Sender<ToEngine>,
                         base_path: &Path) {
    let base_path = base_path.to_path_buf();
    util::spawn_named("terrain_gen", move || {
        run_terrain_gen(recv, to_engine, &base_path);
    });
}
