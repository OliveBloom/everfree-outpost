#[allow(unused)] use server_types::*;
use std::io::{self, Write, BufRead, BufReader};
use std::os::unix::net::UnixStream;
use std::path::{Path, PathBuf};
use std::sync::mpsc::{self, Sender};

use common::util::warn_on_err;
use engine_thread::ToEngine;
use util;


pub fn run_ipython(to_engine: Sender<ToEngine>,
                   path: &Path) {
    let mut counter = 0;
    let server = util::unix_bind(path).unwrap();
    for socket in server.incoming() {
        let mut socket = socket.unwrap();
        let to_engine = to_engine.clone();
        util::spawn_named(&format!("ipython-client-{}", counter), move || {
            warn_on_err!(client_handler(to_engine, socket));
        });
        counter += 1;
    }
}

fn client_handler(to_engine: Sender<ToEngine>,
                  socket: UnixStream) -> io::Result<()> {
    trace!("client connected");
    let mut socket = BufReader::new(socket);
    loop {
        let mut req = String::new();
        socket.read_line(&mut req)?;
        trace!("ipython req: {}", req);
        let (eng_send, eng_recv) = mpsc::channel();
        to_engine.send(ToEngine::IPython(Box::new(req), eng_send)).unwrap();

        let resp = eng_recv.recv().unwrap();
        trace!("ipython resp: {}", resp);
        socket.get_mut().write_all(resp.as_bytes())?;
    }
}

pub fn start_ipython(to_engine: Sender<ToEngine>,
                     path: PathBuf) {
    let path = path.to_path_buf();
    util::spawn_named("ipython", move || {
        run_ipython(to_engine, &path);
    });
}
