This is a stripped-down copy of `libsodium` (version 1.0.16), containing only
the primitives used in Outpost's Noise protocol implementation.  It's designed
for use as a wasm module, and thus avoids calling most libc functions.

The supported primitives are:

- `aead_chacha20poly1305_ietf`
- `onetimeauth_poly1305`
- `stream_chacha20_ietf`
- `hash_sha256`
- `scalarmult_curve25519`

Files in this directory are distributed under the `libsodium` license.  See the
`LICENSE` file for details.
