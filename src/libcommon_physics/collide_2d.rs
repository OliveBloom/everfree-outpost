//! # 2D Collision Detection
//!
//! This module is used to do collision detection for walking (ground-based) entities.  It views
//! the terrain as a set of "walls", which block movement between adjacent grid cells.  Walls can
//! have a direction, so a wall might prevent movement from north to south, but allow movement from
//! south to north.  The goal of collision detection is to report how far an entity can move
//! without crossing any walls in a forbidden direction.
//!
//! Walls are directional so entities that somehow end up overlapping an obstacle are able to move
//! out of it.  The assumption is that callers (who provide the `ShapeSource`) will give obstacles
//! only outward-facing walls, which prevent moving into the obstacle but not out of it.  This also
//! works well for rows of adjacent obstacles: an entity clipped into a long wall can move out of
//! it perpendicularly, but can't move along the wall into adjacent obstacle cells.
//!
//! Walls that overlap an entity in its initial position are ignored.  We only check for walls when
//! the entity is about to pass into an adjacent cell.  This design unfortunately allows entities
//! to move further into an obstacle that they already overlap, but it greatly simplifies the code.
//!
//! "Sliding" against walls is implemented by simply checking each axis separately, and zeroing
//! the velocity only on those axes where an obstacle is present.  Unlike previous versions, here
//! entities can't cut through the outermost corner pixel of an obstacle while sliding (or moving
//! diagonally in general).  Upon reaching a 1-tile doorway, the entity will be exactly aligned in
//! with its cell, meaning further diagonal inputs will be blocked entirely by the far corner of
//! the doorway, and movement will stop until the player chooses which of the two available
//! directions to move in.
//!
//! This version of the physics engine implements a "corner assist" feature to help entities move
//! through narrow doorways.  Moving directly into a wall while within a few pixels of an open
//! space will cause the entity to slide sideways, hopefully aligning them with the doorway.
//!
//! Being strictly 2D, this module doesn't do anything special for ramps.  In the future, those
//! will be handled by a higher-level module that handles 3D adjustments to 2D movement.
use common_types::*;
use std::cmp;
use std::u8;


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum MoveResult {
    /// Entity is completely blocked, and takes no steps in any direction.
    Blocked,
    /// Entity takes `.1` steps in direction `.0`.  This is also used when the entity cannot move
    /// at all.
    Steps(V2, u8),
    /// Entity moves `.0` pixels as a single step.  This is used when the distance to the next
    /// check is not divisible by `step`.
    PartialStep(V2),
}

pub trait ShapeSource {
    /// Check if an entity could pass from tile `start` into tile `end`.
    fn can_pass(&mut self, start: V2, end: V2) -> bool;
}

enum BoundaryPos {
    /// Current location is already on a boundary.
    Here,
    /// The next boundary will be hit after moving distance `.0`.
    There(i32),
    /// Entity is not moving along this axis.
    NotMoving,
}

/// Find the position of the next tile boundary in direction `step` from `pos`.  Returns `None` if
/// `step` is zero.  Returns `pos` if `pos` is already a tile boundary.
fn next_tile_boundary(pos: i32, size: i32, step: i32) -> BoundaryPos {
    if step == 0 {
        return BoundaryPos::NotMoving;
    }

    if step > 0 {
        let coord = pos + size;
        if coord & TILE_MASK == 0 {
            return BoundaryPos::Here;
        }
        // Round up
        BoundaryPos::There(((coord + TILE_SIZE - 1) >> TILE_BITS << TILE_BITS) - coord)
    } else {
        let coord = pos;
        if coord & TILE_MASK == 0 {
            return BoundaryPos::Here;
        }
        // Round down
        BoundaryPos::There((coord >> TILE_BITS << TILE_BITS) - coord)
    }
}

struct AxisResult {
    /// How far the entity can move along this axis before stopping.  `None` means the entity is
    /// not moving in this direction; `Some(0)` means it tried to move but was blocked.
    stop_dist: Option<i32>,
    /// Are we directly touching a tile boundary in this direction?
    at_boundary: bool,
    /// If only one corner of the entity is blocked, which corner is it (sign), and how many pixels
    /// are blocked on that corner (magnitude)?
    corner_block: Option<i8>,
}

const CORNER_BLOCK_MAX: u8 = 4;

/// Run movement checks along the X axis.  Flip axes before calling to check the Y axis instead.
fn axis_move(pos: V2, size: V2, step: i32, mut can_pass: impl FnMut(V2, V2) -> bool) -> AxisResult {
    match next_tile_boundary(pos.x, size.x, step) {
        BoundaryPos::Here => {
            let bounds = Region::sized(size) + pos;
            let tile_bounds = bounds.div_round_signed(TILE_SIZE.into());
            let (tx, x_dir) =
                if step > 0 { (tile_bounds.max.x - 1, 1) }
                else { (tile_bounds.min.x, -1) };

            // Find the range of Y-coordinates of blocked pixels on the side facing the boundary.
            // These are measured relative to `pos.y`, but they can exceed the range `0 .. size.y`
            // when the entity is blocked by part of a tile:
            //
            //            +---+
            //      /---\>| T |
            //      | E |>+---+
            //      \---/
            //
            // Here, the entity is blocked by a tile that extends from `-1 .. 2` in these relative
            // coordinates, which is outside the entity's own Y-axis bounds of `0 .. 3`.
            //
            // Note, however, that `first_block` can never exceed `size.y`, and `last_block` can
            // never be less than zero.  (This property is used in computing `corner_block`.)
            let mut first_block = size.y;
            let mut last_block = 0;
            for ty in tile_bounds.min.y .. tile_bounds.max.y {
                if !can_pass(V2::new(tx, ty), V2::new(tx + x_dir, ty)) {
                    let block_px = ty * TILE_SIZE - pos.y;
                    first_block = cmp::min(first_block, block_px);
                    last_block = cmp::max(last_block, block_px + TILE_SIZE);
                }
            }

            if last_block <= first_block {
                // There is no obstacle in this direction.  We can keep moving up to the next tile
                // boundary.
                AxisResult {
                    stop_dist: Some(TILE_SIZE * step.signum()),
                    at_boundary: true,
                    corner_block: None,
                }
            } else {
                // We hit a wall.  If the total number of blocked pixels is small, we might set
                // `corner_block`.

                // Find the positiong of the furthest blocked pixel, counting from the -Y edge.
                let neg_px = last_block;
                // Same, but counting from the +Y edge.
                let pos_px = size.y - first_block;

                // Both should be positive due to the constraints on `first_block` and
                // `last_block`.
                debug_assert!(neg_px >= 0);
                debug_assert!(pos_px >= 0);

                AxisResult {
                    stop_dist: Some(0),
                    at_boundary: true,
                    corner_block:
                        if neg_px <= CORNER_BLOCK_MAX as i32 { Some(-neg_px as i8) }
                        else if pos_px <= CORNER_BLOCK_MAX as i32 { Some(pos_px as i8) }
                        else { None },
                }
            }
        },

        BoundaryPos::There(dist) => {
            AxisResult {
                stop_dist: Some(dist),
                at_boundary: false,
                corner_block: None,
            }
        },

        BoundaryPos::NotMoving => {
            AxisResult {
                stop_dist: None,
                at_boundary: false,
                corner_block: None,
            }
        },
    }
}

fn flip(v: V2) -> V2 {
    V2::new(v.y, v.x)
}

// A few rational arithmetic functions, which we use to figure out how many steps to take.

fn min_frac(f1: (u32, u32), f2: (u32, u32)) -> (u32, u32) {
    let (n1, d1) = f1;
    let (n2, d2) = f2;
    // `n1 / d1 < n2 / d2`, rearranged
    if n1 * d2 < n2 * d1 { f1 } else { f2 }
}

/// Compute `dist / step` as a rational number.
fn dist_frac(dist: i32, step: i32) -> (u32, u32) {
    if dist == 0 {
        // Return "infinity", which here is interpreted as `TILE_SIZE * 2`.  This should be larger
        // than any `dist` returned by `axis_move`.  (We can't just use `u32::MAX` because we'll be
        // multiplying this value inside `min_frac`.)
        //
        // We return "infinity" for axes where movement is blocked (`dist == 0`) so that movement
        // (including fractional-step movement) can continue on other axes.  As long as at least
        // one axis has nonzero movement, the fraction for that axis will be smaller than
        // "infinity", so it will take precedence.
        return (TILE_SIZE as u32 * 2, 1)
    }

    // If `dist` is nonzero, it should be in the same direction as `step`.  This also implies that
    // `step` is nonzero on this axis.
    debug_assert!(step.signum() == dist.signum());

    if dist < 0 {
        ((-dist) as u32, (-step) as u32)
    } else {
        (dist as u32, step as u32)
    }
}

fn frac_ge_1(f: (u32, u32)) -> bool {
    let (n, d) = f;
    n >= d
}

fn frac_to_int(f: (u32, u32)) -> u32 {
    let (n, d) = f;
    n / d
}

/// Starting at `pos`, try to move in increments of `step`.  Returns the number of steps the entity
/// can take before another collision check is necessary.
pub fn try_move(pos: V2, size: V2, step: V2, shape: &mut impl ShapeSource) -> MoveResult {
    let xr = axis_move(pos, size, step.x, |a,b| shape.can_pass(a, b));
    let yr = axis_move(
        flip(pos), flip(size), step.y,
        |a,b| shape.can_pass(flip(a), flip(b))
    );

    let mut max_move = V2::new(
        xr.stop_dist.unwrap_or(0),
        yr.stop_dist.unwrap_or(0),
    );


    // Corner assist.  If the entity's movement along one axis is blocked only at one corner, try
    // moving away from that corner.
    let mut step = step;
    if let Some(corner_block) = xr.corner_block {
        if step.y == 0 {
            // Update `step` in-place, since it's used later on.
            step.y = step.x.abs() * corner_block.signum() as i32 * -1;
            let yr2 = axis_move(
                flip(pos), flip(size), step.y,
                |a,b| shape.can_pass(flip(a), flip(b))
            );
            // Cap movement distance at the number of blocked pixels.  This handles entities where
            // `size` is not a multiple of `TILE_SIZE`, which should stop when their back edge (not
            // their front edge) reaches the edge of the doorway.
            max_move.y = cmp::min(yr2.stop_dist.unwrap_or(0), corner_block.abs() as i32);
        }
    }
    if let Some(corner_block) = yr.corner_block {
        if step.x == 0 {
            step.x = step.y.abs() * corner_block.signum() as i32 * -1;
            let xr2 = axis_move(pos, size, step.x, |a,b| shape.can_pass(a, b));
            max_move.x = cmp::min(xr2.stop_dist.unwrap_or(0), corner_block.abs() as i32);
        }
    }
    let step = step;


    // TODO: handle sliding for odd-sized entities.  When `size` is not a multiple of `TILE_SIZE`,
    // sliding should stop when the back edge of the entity reaches the edge of the doorway, not
    // when the front edge hits the next tile boundary.


    if max_move.x != 0 && max_move.y != 0 && xr.at_boundary && yr.at_boundary {
        // Entity's corner is touching a tile corner, and attempting to move into it.  Since
        // `max_move` is nonzero along both axes, the entity is not blocked by walls on either
        // axis.  We need to make a special check to ensure the corner tile also is not blocked.
        let dir = max_move.signum();
        let x_dir = V2::new(dir.x, 0);
        let y_dir = V2::new(0, dir.y);
        let move_mask = max_move.is_positive();
        let corner = (pos + size * move_mask - move_mask).div_floor(TILE_SIZE);
        // If moving into the corner fails, we stop movement entirely instead of picking a
        // direction arbitrarily.
        if !shape.can_pass(corner + x_dir, corner + dir)
                || !shape.can_pass(corner + y_dir, corner + dir) {
            max_move = 0.into();
        }
    }

    // Make sure there is movement on at least one axis.  Later code relies on this.
    if max_move == 0 {
        return MoveResult::Blocked;
    }


    let steps_frac = min_frac(
        dist_frac(max_move.x, step.x),
        dist_frac(max_move.y, step.y),
    );

    let new_step = V2::new(
        if max_move.x == 0 { 0 } else { step.x },
        if max_move.y == 0 { 0 } else { step.y },
    );
    if frac_ge_1(steps_frac) {
        let steps = frac_to_int(steps_frac);
        debug_assert!(steps <= u8::MAX as u32);
        MoveResult::Steps(new_step, steps as u8)
    } else {
        let (n, d) = steps_frac;
        MoveResult::PartialStep(new_step * n as i32 / d as i32)
    }
}
