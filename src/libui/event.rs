use context::Void;


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum KeyEvent<K> {
    Down(K),
    Up(K),
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum KeyInterp {
    /// Cycle focus forward/backward.
    FocusCycle(i8),
    /// Change focus to the next widget in the X direction.
    FocusX(i8),
    /// Change focus to the next widget in the Y direction.
    FocusY(i8),

    /// Activate the focused widget.
    Activate,
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum MouseEvent<B> {
    Down(B),
    Up(B),
    Move,
    Wheel(i8),
}


#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum UIResult<T> {
    Event(T),
    NoEvent,
    Unhandled,
}

impl<T> UIResult<T> {
    pub fn is_handled(&self) -> bool {
        match *self {
            UIResult::Unhandled => false,
            _ => true,
        }
    }

    pub fn map<F: FnOnce(T) -> U, U>(self, f: F) -> UIResult<U> {
        match self {
            UIResult::Event(t) => UIResult::Event(f(t)),
            UIResult::NoEvent => UIResult::NoEvent,
            UIResult::Unhandled => UIResult::Unhandled,
        }
    }

    pub fn and_then<F: FnOnce(T) -> UIResult<U>, U>(self, f: F) -> UIResult<U> {
        match self {
            UIResult::Event(t) => f(t),
            UIResult::NoEvent => UIResult::NoEvent,
            UIResult::Unhandled => UIResult::Unhandled,
        }
    }

    pub fn or_else<F: FnOnce() -> UIResult<T>>(self, f: F) -> UIResult<T> {
        match self {
            UIResult::Event(t) => UIResult::Event(t),
            UIResult::NoEvent => UIResult::NoEvent,
            UIResult::Unhandled => f(),
        }
    }
}

impl UIResult<Void> {
    pub fn cast<U>(self) -> UIResult<U> {
        match self {
            UIResult::Event(v) => match v {},
            UIResult::NoEvent => UIResult::NoEvent,
            UIResult::Unhandled => UIResult::Unhandled,
        }
    }
}

impl<T> Default for UIResult<T> {
    fn default() -> Self {
        UIResult::Unhandled
    }
}
