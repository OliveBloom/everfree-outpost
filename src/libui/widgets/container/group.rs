use std::prelude::v1::*;
use std::cell::Cell;
use std::cmp;
use std::marker::PhantomData;

use context::Context;
use event::{KeyEvent, KeyInterp, MouseEvent, UIResult};
use geom::*;
use widget::Widget;

use super::{ChildWidget, Align};
use super::contents::{Contents, Visitor};



struct Layout<D> {
    major_pos: i32,
    minor_size: i32,
    spacing: i32,
    _marker: PhantomData<D>,
}

impl<D: Direction> Layout<D> {
    pub fn new(minor_size: i32, spacing: i32) -> Layout<D> {
        Layout {
            major_pos: 0,
            minor_size: minor_size,
            spacing: spacing,
            _marker: PhantomData,
        }
    }

    pub fn place(&mut self, size: Point, align: Align) -> Rect {
        let major = self.major_pos;
        self.major_pos += D::major(size) + self.spacing;

        let minor_size = D::minor(size);
        let minor_total = self.minor_size;
        let (minor0, minor1) = match align {
            Align::Start => (0, minor_size),
            Align::Center => {
                let offset = (minor_total - minor_size) / 2;
                (offset, offset + minor_size)
            },
            Align::End => (minor_total - minor_size, minor_total),
            Align::Stretch => (0, minor_total),
        };

        Rect {
            min: D::make_point(major, minor0),
            max: D::make_point(major + D::major(size), minor1),
        }
    }
}


trait MouseVisitorState<Ctx: Context, R> {
    fn visit<W, F>(&mut self, ctx: &mut Ctx, cw: &ChildWidget<W, F>, rect: Rect, idx: usize)
        where W: Widget<Ctx>, F: Fn(W::Event) -> R;
}

struct MouseVisitor<'c, Ctx: Context+'c, D, S, C> {
    ctx: &'c mut Ctx,
    layout: Layout<D>,
    idx: usize,
    found_target: bool,
    state: S,
    _marker: PhantomData<C>,
}

impl<'c, Ctx: Context, D, S, C> MouseVisitor<'c, Ctx, D, S, C> {
    fn new(ctx: &'c mut Ctx,
           layout: Layout<D>,
           state: S) -> MouseVisitor<'c, Ctx, D, S, C> {
        MouseVisitor {
            ctx: ctx,
            layout: layout,
            idx: 0,
            found_target: false,
            state: state,
            _marker: PhantomData,
        }
    }

    fn hit_idx(&self) -> Option<usize> {
        if self.found_target {
            Some(self.idx)
        } else {
            None
        }
    }
}

impl<'c, Ctx, D, R, C, S> Visitor<Ctx, R> for MouseVisitor<'c, Ctx, D, S, C>
        where Ctx: Context, D: Direction, C: Contents<Ctx, R>, S: MouseVisitorState<Ctx, R> {
    fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
            where W: Widget<Ctx>, F: Fn(W::Event) -> R {
        if self.found_target {
            return;
        }

        let size = cw.w.min_size(self.ctx);
        let rect = self.layout.place(size, cw.align);

        let idx = &mut self.idx;
        let found_target = &mut self.found_target;
        let state = &mut self.state;
        self.ctx.with_bounds(rect, |ctx| {
            if ctx.mouse_target() {
                *found_target = true;
                state.visit(ctx, cw, rect, *idx);
            } else {
                *idx += 1;
            }
        });
    }
}



pub struct Group<'s, Ctx: Context, D: Direction, R, C: Contents<Ctx, R>> {
    focus: Option<&'s Cell<usize>>,
    contents: C,
    spacing: i32,
    hover_focus: bool,
    _marker: PhantomData<(Ctx, D, R)>,
}

impl<'s, Ctx: Context, R, C: Contents<Ctx, R>> Group<'s, Ctx, Horizontal, R, C> {
    pub fn horiz(contents: C) -> Group<'static, Ctx, Horizontal, R, C> {
        Group {
            focus: None,
            contents: contents,
            spacing: 0,
            hover_focus: false,
            _marker: PhantomData,
        }
    }
}

impl<'s, Ctx: Context, R, C: Contents<Ctx, R>> Group<'s, Ctx, Vertical, R, C> {
    pub fn vert(contents: C) -> Group<'static, Ctx, Vertical, R, C> {
        Group {
            focus: None,
            contents: contents,
            spacing: 0,
            hover_focus: false,
            _marker: PhantomData,
        }
    }
}

impl<'s, Ctx: Context, D: Direction, R, C: Contents<Ctx, R>> Group<'s, Ctx, D, R, C> {
    pub fn focus<'t>(self, focus: &'t Cell<usize>) -> Group<'t, Ctx, D, R, C> {
        let Group { focus: _, contents, spacing, hover_focus, _marker } = self;
        Group {
            focus: Some(focus),
            contents, spacing, hover_focus, _marker,
        }
    }

    pub fn spacing(self, spacing: i32) -> Self {
        Group {
            spacing: spacing,
            .. self
        }
    }

    pub fn hover_focus(self, hover_focus: bool) -> Self {
        Group {
            hover_focus: hover_focus,
            .. self
        }
    }

    fn adjust_focus_clamp(&self, delta: i32) {
        let focus = match self.focus {
            Some(ref f) => f,
            None => return,
        };

        let len = self.contents.len();

        if delta >= 0 {
            let delta = delta as usize;
            if delta >= len - focus.get() {
                focus.set(len - 1);
            } else {
                focus.set(focus.get() + delta);
            }
        } else {
            let delta = (-delta) as usize;
            if delta > focus.get() {
                focus.set(0);
            } else {
                focus.set(focus.get() - delta);
            }
        }
    }

    fn get_focus(&self) -> Option<usize> {
        self.focus.as_ref().map(|f| f.get())
    }

    fn set_focus(&self, focus: usize) {
        if let Some(ref f) = self.focus {
            f.set(focus);
        }
    }
}

impl<'s, Ctx, D, R, C> Widget<Ctx> for Group<'s, Ctx, D, R, C>
        where Ctx: Context, D: Direction, C: Contents<Ctx, R> {
    type Event = R;

    fn min_size(&self, ctx: &Ctx) -> Point {
        struct SizeVisitor<'c, Ctx: 'c, D> {
            ctx: &'c Ctx,
            size: Point,
            spacing: i32,
            _marker: PhantomData<D>,
        }
        impl<'c, Ctx: Context, D: Direction, R> Visitor<Ctx, R> for SizeVisitor<'c, Ctx, D> {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let child_size = cw.w.min_size(self.ctx);
                let add_minor = cmp::max(0, D::minor(child_size) - D::minor(self.size));
                let add_major = D::major(child_size) +
                    if D::major(self.size) != 0 { self.spacing } else { 0 };
                self.size = self.size + D::make_point(add_major, add_minor);
            }
        }
        let mut v: SizeVisitor<Ctx, D> = SizeVisitor {
            ctx: ctx,
            size: Point { x: 0, y: 0 },
            spacing: self.spacing,
            _marker: PhantomData,
        };
        self.contents.accept(&mut v);
        v.size
    }

    fn requested_visibility(&self, ctx: &Ctx) -> Option<Rect> {
        struct ReqVisVisitor<'c, Ctx: Context+'c, D: Direction> {
            ctx: &'c Ctx,
            layout: Layout<D>,
            idx: usize,
            focus: Option<usize>,
            req_vis: Option<Rect>,
        }
        impl<'c, Ctx: Context, D: Direction, R> Visitor<Ctx, R> for ReqVisVisitor<'c, Ctx, D> {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let size = cw.w.min_size(self.ctx);
                let bounds = self.layout.place(size, cw.align);

                if Some(self.idx) == self.focus {
                    self.req_vis = Some(bounds);
                }
                self.idx += 1;
            }
        }

        let bounds_size = ctx.cur_bounds().size();
        let mut v: ReqVisVisitor<_, D> = ReqVisVisitor {
            ctx: ctx,
            layout: Layout::new(D::minor(bounds_size), self.spacing),
            idx: 0,
            focus: self.get_focus(),
            req_vis: None,
        };
        // Note we can't use `accept_nth` for most `Group` methods, because the group layout is
        // computed incrementally.
        self.contents.accept(&mut v);
        v.req_vis
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        struct PaintVisitor<'c, Ctx: Context+'c, D: Direction> {
            ctx: &'c mut Ctx,
            layout: Layout<D>,
            idx: usize,
            focus: Option<usize>,
        }
        impl<'c, Ctx: Context, D: Direction, R> Visitor<Ctx, R> for PaintVisitor<'c, Ctx, D> {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let size = cw.w.min_size(self.ctx);
                let bounds = self.layout.place(size, cw.align);
                self.ctx.with_focus(Some(self.idx) == self.focus, |ctx| {
                    ctx.with_bounds(bounds, |ctx| {
                        cw.w.on_paint(ctx);
                    })
                });
                self.idx += 1;
            }
        }

        let bounds_size = ctx.cur_bounds().size();
        let mut v: PaintVisitor<_, D> = PaintVisitor {
            ctx: ctx,
            layout: Layout::new(D::minor(bounds_size), self.spacing),
            idx: 0,
            focus: self.get_focus(),
        };
        self.contents.accept(&mut v);
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<R> {
        struct KeyVisitor<'c, Ctx: Context+'c, D: Direction, R> {
            ctx: &'c mut Ctx,
            idx: usize,
            focus: Option<usize>,
            layout: Layout<D>,
            event: Option<KeyEvent<Ctx::Key>>,
            result: UIResult<R>,
        }
        impl<'c, Ctx, D, R> Visitor<Ctx, R> for KeyVisitor<'c, Ctx, D, R>
                where Ctx: Context, D: Direction {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                if Some(self.idx) == self.focus {
                    let size = cw.w.min_size(self.ctx);
                    let bounds = self.layout.place(size, cw.align);
                    let evt = self.event.take().unwrap();
                    self.result = self.ctx.with_bounds(bounds, |ctx| {
                        cw.w.on_key(ctx, evt).map(|e| (cw.f)(e))
                    });
                }
                self.idx += 1;
            }
        }

        let bounds_size = ctx.cur_bounds().size();
        let mut v: KeyVisitor<Ctx, D, R> = KeyVisitor {
            ctx: ctx,
            idx: 0,
            focus: self.get_focus(),
            layout: Layout::new(D::minor(bounds_size), self.spacing),
            event: Some(evt.clone()),
            result: UIResult::Unhandled,
        };
        self.contents.accept(&mut v);
        try_handle!(v.result);

        let ctx = v.ctx;
        match ctx.interp_key(evt) {
            // TODO: respect choice of Direction
            Some(KeyInterp::FocusY(delta)) => {
                self.adjust_focus_clamp(delta as i32);
                UIResult::NoEvent
            },
            _ => UIResult::Unhandled,
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<R> {
        struct State<Ctx: Context, R> {
            event: Option<MouseEvent<Ctx::Button>>,
            result: UIResult<R>,
        }

        impl<Ctx: Context, R> MouseVisitorState<Ctx, R> for State<Ctx, R> {
            fn visit<W, F>(&mut self,
                           ctx: &mut Ctx,
                           cw: &ChildWidget<W, F>,
                           _rect: Rect,
                           _idx: usize)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let evt = self.event.take().unwrap();
                self.result = cw.w.on_mouse(ctx, evt).map(|e| (cw.f)(e));
            }
        }

        let bounds_size = ctx.cur_bounds().size();
        let mut v = MouseVisitor::<Ctx, D, _, C>::new(
            ctx,
            Layout::new(D::minor(bounds_size), self.spacing),
            State {
                event: Some(evt),
                result: UIResult::Unhandled,
            });
        self.contents.accept(&mut v);

        if let Some(hit_idx) = v.hit_idx() {
            if v.state.result.is_handled() || self.hover_focus {
                self.set_focus(hit_idx);
            }
        }

        v.state.result
    }

    fn check_drop(&self, ctx: &mut Ctx, data: &Ctx::DropData) -> bool {
        struct State<'a, Ctx: Context+'a> {
            data: &'a Ctx::DropData,
            result: bool,
        }

        impl<'a, Ctx: Context, R> MouseVisitorState<Ctx, R> for State<'a, Ctx> {
            fn visit<W, F>(&mut self,
                           ctx: &mut Ctx,
                           cw: &ChildWidget<W, F>,
                           _rect: Rect,
                           _idx: usize)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                self.result = cw.w.check_drop(ctx, self.data);
            }
        }

        let bounds_size = ctx.cur_bounds().size();
        let mut v = MouseVisitor::<Ctx, D, _, C>::new(
            ctx,
            Layout::new(D::minor(bounds_size), self.spacing),
            State {
                data: data,
                result: false,
            });
        self.contents.accept(&mut v);

        v.state.result
    }

    fn on_drop(&self, ctx: &mut Ctx, data: &Ctx::DropData) -> UIResult<R> {
        struct State<'a, Ctx: Context+'a, R> {
            data: &'a Ctx::DropData,
            result: UIResult<R>,
        }

        impl<'a, Ctx: Context, R> MouseVisitorState<Ctx, R> for State<'a, Ctx, R> {
            fn visit<W, F>(&mut self,
                           ctx: &mut Ctx,
                           cw: &ChildWidget<W, F>,
                           _rect: Rect,
                           _idx: usize)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                self.result = cw.w.on_drop(ctx, self.data).map(|e| (cw.f)(e));
            }
        }

        let bounds_size = ctx.cur_bounds().size();
        let mut v = MouseVisitor::<Ctx, D, _, C>::new(
            ctx,
            Layout::new(D::minor(bounds_size), self.spacing),
            State {
                data: data,
                result: UIResult::Unhandled,
            });
        self.contents.accept(&mut v);

        if let Some(hit_idx) = v.hit_idx() {
            if v.state.result.is_handled() {
                self.set_focus(hit_idx);
            }
        }

        v.state.result
    }
}
