mod contents;
pub use self::contents::{Contents, GenWidgets};

mod group;
pub use self::group::Group;

mod grid;
pub use self::grid::Grid;

mod computed;
pub use self::computed::Computed;


/// Data for a single child widget in a container.  Consists of the widget itself, a function for
/// mapping widget events to container events, and layout data for the widget.
pub struct ChildWidget<W, F> {
    w: W,
    f: F,
    align: Align,
}

impl<W, F> ChildWidget<W, F> {
    pub fn new(w: W, f: F) -> ChildWidget<W, F> {
        ChildWidget {
            w: w,
            f: f,
            align: Align::Stretch,
        }
    }

    pub fn align(self, align: Align) -> Self {
        ChildWidget  {
            align: align,
            .. self
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Align {
    Start,
    Center,
    End,
    Stretch,
}
