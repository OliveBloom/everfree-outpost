use std::prelude::v1::*;
use std::cell::Cell;
use std::cmp;
use std::marker::PhantomData;

use context::Context;
use event::{KeyEvent, KeyInterp, MouseEvent, UIResult};
use geom::*;
use widget::Widget;

use super::ChildWidget;
use super::contents::{Contents, Visitor};



/// Grid container layout helper.  Currently grid layout is based on a fixed cell size, computed
/// from the maximum `min_size` of all children.  It would be better to set the size by subdividing
/// the actual bounds assigned to the container, but that's not implemented yet.
struct GridLayout {
    cols: usize,
    cell_size: Point,
    spacing: i32,
}

impl GridLayout {
    pub fn new(cols: usize, cell_size: Point, spacing: i32) -> GridLayout {
        GridLayout {
            cols: cols,
            cell_size: cell_size,
            spacing: spacing,
        }
    }

    pub fn place(&self, idx: usize) -> Rect {
        let x = (idx % self.cols) as i32;
        let y = (idx / self.cols) as i32;

        let pos = Point {
            x: x * (self.cell_size.x + self.spacing),
            y: y * (self.cell_size.y + self.spacing),
        };
        Rect::sized(self.cell_size) + pos
    }

    pub fn find_idx(&self, pos: Point) -> Option<usize> {
        if pos.x < 0 || pos.y < 0 {
            return None;
        }

        let step = self.cell_size + Point::new(self.spacing, self.spacing);
        let cx = pos.x / step.x;
        let cy = pos.y / step.y;

        if pos.x % step.x >= self.cell_size.x || pos.y % step.y >= self.cell_size.y {
            // `pos` falls in the spacing between two cells
            return None;
        }

        Some(cx as usize + self.cols * cy as usize)
    }
}


pub struct Grid<'s, Ctx: Context, R, C: Contents<Ctx, R>> {
    focus: &'s Cell<usize>,
    cols: usize,
    contents: C,
    spacing: i32,
    hover_focus: bool,
    _marker: PhantomData<(Ctx, R)>,
}

impl<'s, Ctx: Context, R, C: Contents<Ctx, R>> Grid<'s, Ctx, R, C> {
    pub fn new(focus: &'s Cell<usize>, cols: usize, contents: C) -> Grid<'s, Ctx, R, C> {
        Grid {
            focus: focus,
            cols: cols,
            contents: contents,
            spacing: 0,
            hover_focus: false,
            _marker: PhantomData,
        }
    }

    pub fn spacing(self, spacing: i32) -> Self {
        Grid {
            spacing: spacing,
            .. self
        }
    }

    pub fn hover_focus(self, hover_focus: bool) -> Self {
        Grid {
            hover_focus: hover_focus,
            .. self
        }
    }

    fn grid_size(&self) -> (i32, i32) {
        let cols = self.cols as i32;
        let rows = (self.contents.len() as i32 + cols - 1) / cols;
        (rows, cols)
    }

    fn focus_pos(&self) -> (i32, i32) {
        let idx = self.focus.get();
        (idx as i32 % self.cols as i32,
         idx as i32 / self.cols as i32)
    }

    fn adjust_focus_clamp(&self, dx: i32, dy: i32) -> bool {
        let (x, y) = self.focus_pos();
        let (rows, cols) = self.grid_size();

        if rows == 0 || cols == 0 {
            return false;
        }

        let mut new_x = cmp::max(0, cmp::min(cols - 1, x + dx));
        let mut new_y = cmp::max(0, cmp::min(rows - 1, y + dy));

        // The last row may be short.  Check if we're moving into the empty space.
        if new_y == rows - 1 {
            let last_row_cols = self.contents.len() as i32 - (rows - 1) * cols;
            if new_x >= last_row_cols {
                if dx != 0 {
                    // Moved right into empty space from the left.
                    new_x = last_row_cols - 1;
                } else {
                    // Moved down into the empty space from above.
                    // Don't go negative - it could cause `new_focus` calculation to overflow.
                    if new_y > 0 {
                        new_y -= 1;
                    }
                }
            }
        }

        let new_focus = new_x as usize + (new_y as usize * self.cols);
        self.focus.set(new_focus);

        true
    }

    fn cell_size(&self, ctx: &Ctx) -> Point {
        struct SizeVisitor<'c, Ctx: 'c> {
            ctx: &'c Ctx,
            size: Point,
        }
        impl<'c, Ctx: Context, R> Visitor<Ctx, R> for SizeVisitor<'c, Ctx> {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let child_size = cw.w.min_size(self.ctx);
                self.size = Point {
                    x: cmp::max(self.size.x, child_size.x),
                    y: cmp::max(self.size.y, child_size.y),
                };
            }
        }
        let mut v: SizeVisitor<Ctx> = SizeVisitor {
            ctx: ctx,
            size: Point { x: 0, y: 0 },
        };
        self.contents.accept(&mut v);
        v.size
    }
}

impl<'s, Ctx, R, C> Widget<Ctx> for Grid<'s, Ctx, R, C>
        where Ctx: Context, C: Contents<Ctx, R> {
    type Event = R;

    fn min_size(&self, ctx: &Ctx) -> Point {
        let cell_size = self.cell_size(ctx);
        let (rows, cols) = self.grid_size();

        let width = cell_size.x * cols + self.spacing * (cols - 1);
        let height = if rows == 0 {
            0
        } else {
            cell_size.y * rows + self.spacing * (rows - 1)
        };

        Point::new(width, height)
    }

    fn requested_visibility(&self, ctx: &Ctx) -> Option<Rect> {
        let idx = self.focus.get();
        if idx >= self.contents.len() {
            return None;
        }

        let l = GridLayout::new(self.cols, self.cell_size(ctx), self.spacing);
        Some(l.place(idx))
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        struct PaintVisitor<'c, Ctx: Context+'c> {
            ctx: &'c mut Ctx,
            layout: GridLayout,
            idx: usize,
            focus: usize,
        }
        impl<'c, Ctx: Context, R> Visitor<Ctx, R> for PaintVisitor<'c, Ctx> {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let bounds = self.layout.place(self.idx);
                self.ctx.with_focus(self.idx == self.focus, |ctx| {
                    ctx.with_bounds(bounds, |ctx| {
                        cw.w.on_paint(ctx);
                    })
                });
                self.idx += 1;
            }
        }

        let cell_size = self.cell_size(ctx);
        let mut v = PaintVisitor {
            ctx: ctx,
            layout: GridLayout::new(self.cols, cell_size, self.spacing),
            idx: 0,
            focus: self.focus.get(),
        };
        self.contents.accept(&mut v);
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<R> {
        struct KeyVisitor<'c, Ctx: Context+'c, R> {
            ctx: &'c mut Ctx,
            bounds: Rect,
            event: Option<KeyEvent<Ctx::Key>>,
            result: UIResult<R>,
        }
        impl<'c, Ctx, R> Visitor<Ctx, R> for KeyVisitor<'c, Ctx, R>
                where Ctx: Context {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let evt = self.event.take().unwrap();
                self.result = self.ctx.with_bounds(self.bounds, |ctx| {
                    cw.w.on_key(ctx, evt).map(|e| (cw.f)(e))
                });
            }
        }

        let l = GridLayout::new(self.cols, self.cell_size(ctx), self.spacing);
        let mut v: KeyVisitor<Ctx, R> = KeyVisitor {
            ctx: ctx,
            bounds: l.place(self.focus.get()),
            event: Some(evt.clone()),
            result: UIResult::Unhandled,
        };
        self.contents.accept_nth(&mut v, self.focus.get());
        try_handle!(v.result);

        let ctx = v.ctx;
        match ctx.interp_key(evt) {
            Some(KeyInterp::FocusX(delta)) => {
                if self.adjust_focus_clamp(delta as i32, 0) {
                    UIResult::NoEvent
                } else {
                    UIResult::Unhandled
                }
            },
            Some(KeyInterp::FocusY(delta)) => {
                if self.adjust_focus_clamp(0, delta as i32) {
                    UIResult::NoEvent
                } else {
                    UIResult::Unhandled
                }
            },
            _ => UIResult::Unhandled,
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<R> {
        // Find the target index.
        let l = GridLayout::new(self.cols, self.cell_size(ctx), self.spacing);
        let idx = match ctx.mouse_target_pos().and_then(|pos| l.find_idx(pos)) {
            Some(x) if x < self.contents.len() => x,
            _ => return UIResult::Unhandled,
        };

        struct MouseVisitor<'c, Ctx: Context+'c, R> {
            ctx: &'c mut Ctx,
            bounds: Rect,
            event: Option<MouseEvent<Ctx::Button>>,
            result: UIResult<R>,
        }
        impl<'c, Ctx, R> Visitor<Ctx, R> for MouseVisitor<'c, Ctx, R>
                where Ctx: Context {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let evt = self.event.take().unwrap();
                self.result = self.ctx.with_bounds(self.bounds, |ctx| {
                    cw.w.on_mouse(ctx, evt).map(|e| (cw.f)(e))
                });
            }
        }

        let mut v: MouseVisitor<Ctx, R> = MouseVisitor {
            ctx: ctx,
            bounds: l.place(idx),
            event: Some(evt),
            result: UIResult::Unhandled,
        };
        self.contents.accept_nth(&mut v, idx);

        if v.result.is_handled() || self.hover_focus {
            self.focus.set(idx);
        }

        v.result
    }

    fn check_drop(&self, ctx: &mut Ctx, data: &Ctx::DropData) -> bool {
        // Find the target index.
        let l = GridLayout::new(self.cols, self.cell_size(ctx), self.spacing);
        let idx = match ctx.mouse_target_pos().and_then(|pos| l.find_idx(pos)) {
            Some(x) if x < self.contents.len() => x,
            _ => return false,
        };

        struct CheckDropVisitor<'c, 'a, Ctx: Context+'c+'a> {
            ctx: &'c mut Ctx,
            bounds: Rect,
            data: &'a Ctx::DropData,
            result: bool,
        }
        impl<'c, 'a, Ctx, R> Visitor<Ctx, R> for CheckDropVisitor<'c, 'a, Ctx>
                where Ctx: Context {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let data = self.data;
                self.result = self.ctx.with_bounds(self.bounds, |ctx| {
                    cw.w.check_drop(ctx, data)
                });
            }
        }

        let mut v: CheckDropVisitor<Ctx> = CheckDropVisitor {
            ctx: ctx,
            bounds: l.place(idx),
            data: data,
            result: false,
        };
        self.contents.accept_nth(&mut v, idx);

        v.result
    }

    fn on_drop(&self, ctx: &mut Ctx, data: &Ctx::DropData) -> UIResult<R> {
        // Find the target index.
        let l = GridLayout::new(self.cols, self.cell_size(ctx), self.spacing);
        let idx = match ctx.mouse_target_pos().and_then(|pos| l.find_idx(pos)) {
            Some(x) if x < self.contents.len() => x,
            _ => return UIResult::Unhandled,
        };

        struct DropVisitor<'c, 'a, Ctx: Context+'c+'a, R> {
            ctx: &'c mut Ctx,
            bounds: Rect,
            data: &'a Ctx::DropData,
            result: UIResult<R>,
        }
        impl<'c, 'a, Ctx, R> Visitor<Ctx, R> for DropVisitor<'c, 'a, Ctx, R>
                where Ctx: Context {
            fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
                    where W: Widget<Ctx>, F: Fn(W::Event) -> R {
                let data = self.data;
                self.result = self.ctx.with_bounds(self.bounds, |ctx| {
                    cw.w.on_drop(ctx, data).map(|e| (cw.f)(e))
                });
            }
        }

        let mut v: DropVisitor<Ctx, R> = DropVisitor {
            ctx: ctx,
            bounds: l.place(idx),
            data: data,
            result: UIResult::Unhandled,
        };
        self.contents.accept_nth(&mut v, idx);

        if v.result.is_handled() {
            self.focus.set(idx);
        }

        v.result
    }
}
