use context::Context;
use event::{KeyEvent, MouseEvent, UIResult};
use geom::{Point, Rect};


#[allow(unused_variables)]
pub trait Widget<Ctx: Context> {
    type Event;


    fn min_size(&self, ctx: &Ctx) -> Point;

    fn requested_visibility(&self, ctx: &Ctx) -> Option<Rect> { None }


    fn on_paint(&self,
                ctx: &mut Ctx) {
        // No-op
    }

    fn on_key(&self,
              ctx: &mut Ctx,
              evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        UIResult::Unhandled
    }

    fn on_mouse(&self,
                ctx: &mut Ctx,
                evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        UIResult::Unhandled
    }

    fn check_drop(&self,
                  ctx: &mut Ctx,
                  data: &Ctx::DropData) -> bool {
        false
    }

    fn on_drop(&self,
               ctx: &mut Ctx,
               data: &Ctx::DropData) -> UIResult<Self::Event> {
        UIResult::Unhandled
    }
}
