use std::collections::HashMap;
use std::hash::Hash;

use api as py;
use exc::PyResult;
use ptr::{PyBox, PyRef};


#[macro_export]
macro_rules! python_conv_tuple_impls {
    ($count:expr, $pack:ident: ($($A:ident $idx:expr),*)) => {
        impl<$($A: Unpack,)*> Unpack for ($($A,)*) {
            fn unpack(obj: PyRef) -> PyResult<($($A,)*)> {
                pyassert!(py::tuple::check(obj), type_error);
                pyassert!(py::tuple::size(obj)? == $count, value_error);
                Ok((
                    $( Unpack::unpack(py::tuple::get_item(obj, $idx)?)?, )*
                ))
            }
        }

        impl<$($A: Pack,)*> Pack for ($($A,)*) {
            #[allow(non_snake_case)]
            fn pack(self) -> PyResult<PyBox> {
                let ($($A,)*) = self;
                py::tuple::$pack(
                    $(Pack::pack($A)?,)*
                )
            }
        }
    };
}

#[macro_export]
macro_rules! python_conv_int_impls {
    ($ty:ident, unsigned) => {
        impl Unpack for $ty {
            fn unpack(obj: PyRef) -> PyResult<$ty> {
                let raw = py::int::as_u64(obj)?;
                pyassert!(raw <= ::std::$ty::MAX as u64, value_error);
                Ok(raw as $ty)
            }
        }

        impl Pack for $ty {
            fn pack(self) -> PyResult<PyBox> {
                py::int::from_u64(self as u64)
            }
        }
    };

    ($ty:ident, signed) => {
        impl Unpack for $ty {
            fn unpack(obj: PyRef) -> PyResult<$ty> {
                let raw = py::int::as_i64(obj)?;
                pyassert!(raw >= ::std::$ty::MIN as i64, value_error);
                pyassert!(raw <= ::std::$ty::MAX as i64, value_error);
                Ok(raw as $ty)
            }
        }

        impl Pack for $ty {
            fn pack(self) -> PyResult<PyBox> {
                py::int::from_i64(self as i64)
            }
        }
    };
}



/// Re-emits the `Pack` and `Unpack` trait definitions, and all their impls defined in this module.
/// This is useful for defining a custom conversion trait for use with the `conv_module` option.
#[macro_export]
macro_rules! python_conv_definitions {
    () => {

pub trait Pack {
    fn pack(self) -> PyResult<PyBox>;
}

pub trait Unpack: Sized {
    fn unpack(obj: PyRef) -> PyResult<Self>;
}


// Python pointers (trivial)
impl Unpack for PyBox {
    fn unpack(obj: PyRef) -> PyResult<PyBox> {
        Ok(obj.to_box())
    }
}
// No `PyRef` Unpack because we can't link the lifetime of the result to the lifetime of `obj`

impl Pack for PyBox {
    fn pack(self) -> PyResult<PyBox> {
        Ok(self)
    }
}

impl<'a> Pack for PyRef<'a> {
    fn pack(self) -> PyResult<PyBox> {
        Ok(self.to_box())
    }
}


// Special case: `Pack for PyResult<T>` lets wrappers uniformly handle both functions returning `T`
// and functions returning `PyResult<T>`.
impl<T: Pack> Pack for PyResult<T> {
    fn pack(self) -> PyResult<PyBox> {
        self.and_then(Pack::pack)
    }
}


// Tuples
python_conv_tuple_impls!(0, pack0: ());
python_conv_tuple_impls!(1, pack1: (A 0));
python_conv_tuple_impls!(2, pack2: (A 0, B 1));
python_conv_tuple_impls!(3, pack3: (A 0, B 1, C 2));
python_conv_tuple_impls!(4, pack4: (A 0, B 1, C 2, D 3));
python_conv_tuple_impls!(5, pack5: (A 0, B 1, C 2, D 3, E 4));
python_conv_tuple_impls!(6, pack6: (A 0, B 1, C 2, D 3, E 4, F 5));


// Ints
python_conv_int_impls!(u8, unsigned);
python_conv_int_impls!(u16, unsigned);
python_conv_int_impls!(u32, unsigned);
python_conv_int_impls!(u64, unsigned);
python_conv_int_impls!(usize, unsigned);

python_conv_int_impls!(i8, signed);
python_conv_int_impls!(i16, signed);
python_conv_int_impls!(i32, signed);
python_conv_int_impls!(i64, signed);
python_conv_int_impls!(isize, signed);


// Floats
impl Unpack for f64 {
    fn unpack(obj: PyRef) -> PyResult<f64> {
        py::float::as_f64(obj)
    }
}

impl Pack for f64 {
    fn pack(self) -> PyResult<PyBox> {
        py::float::from_f64(self)
    }
}

impl Unpack for f32 {
    fn unpack(obj: PyRef) -> PyResult<f32> {
        py::float::as_f64(obj).map(|f| f as f32)
    }
}

impl Pack for f32 {
    fn pack(self) -> PyResult<PyBox> {
        py::float::from_f64(self as f64)
    }
}


// bool
impl Unpack for bool {
    fn unpack(obj: PyRef) -> PyResult<bool> {
        if obj == py::bool::true_() {
            Ok(true)
        } else if obj == py::bool::false_() {
            Ok(false)
        } else {
            pyraise!(type_error, "expected an instance of bool");
        }
    }
}

impl Pack for bool {
    fn pack(self) -> PyResult<PyBox> {
        if self {
            Ok(py::bool::true_().to_box())
        } else {
            Ok(py::bool::false_().to_box())
        }
    }
}


// Strings
impl Unpack for String {
    fn unpack(obj: PyRef) -> PyResult<String> {
        py::unicode::as_string(obj)
    }
}

impl<'a> Pack for &'a str {
    fn pack(self) -> PyResult<PyBox> {
        py::unicode::from_str(self)
    }
}

impl Pack for String {
    fn pack(self) -> PyResult<PyBox> {
        py::unicode::from_str(&self)
    }
}


// Option
impl<A: Unpack> Unpack for Option<A> {
    fn unpack(obj: PyRef) -> PyResult<Option<A>> {
        if obj == py::none() {
            Ok(None)
        } else {
            Ok(Some(Unpack::unpack(obj)?))
        }
    }
}


impl<A: Pack> Pack for Option<A> {
    fn pack(self) -> PyResult<PyBox> {
        match self {
            Some(a) => Pack::pack(a),
            None => Ok(py::none().to_box()),
        }
    }
}


// Collections
impl<T> Unpack for Vec<T>
        where T: Unpack {
    fn unpack(obj: PyRef) -> PyResult<Vec<T>> {
        let len = py::list::size(obj)?;
        let mut v = Vec::with_capacity(len);
        for i in 0 .. len {
            let item = py::list::get_item(obj, i)?;
            v.push(Unpack::unpack(item)?);
        }
        Ok(v)
    }
}

impl<K, V> Unpack for HashMap<K, V>
        where K: Unpack + Eq + Hash,
              V: Unpack {
    fn unpack(obj: PyRef) -> PyResult<HashMap<K, V>> {
        let items = py::dict::items(obj)?;
        let items_ = items.borrow();

        let len = py::list::size(items_)?;
        let mut h = HashMap::with_capacity(len);
        for i in 0 .. len {
            let item = py::list::get_item(items_, i)?;
            let (k, v) = Unpack::unpack(item)?;
            h.insert(k, v);
        }
        Ok(h)
    }
}

impl<T> Pack for Vec<T>
        where T: Pack {
    fn pack(self) -> PyResult<PyBox> {
        // Can't dispatch to the `&[T]` impl because that one clones the items
        let lst = py::list::new()?;
        for x in self {
            let x = Pack::pack(x)?;
            py::list::append(lst.borrow(), x.borrow())?;
        }
        Ok(lst)
    }
}

impl<'a, T> Pack for &'a [T]
        where T: Pack + Clone {
    fn pack(self) -> PyResult<PyBox> {
        let lst = py::list::new()?;
        for x in self {
            let x = Pack::pack(x.clone())?;
            py::list::append(lst.borrow(), x.borrow())?;
        }
        Ok(lst)
    }
}

impl<K, V> Pack for HashMap<K, V>
        where K: Pack + Eq + Hash,
              V: Pack {
    fn pack(self) -> PyResult<PyBox> {
        let dct = py::dict::new()?;
        for (k, v) in self {
            let k = Pack::pack(k)?;
            let v = Pack::pack(v)?;
            py::dict::set_item(dct.borrow(), k.borrow(), v.borrow())?;
        }
        Ok(dct)
    }
}

    };
}

python_conv_definitions!();



/// Helper trait for uniformly handling `PyResult` and non-`PyResult` return values.
pub trait IntoPyResult {
    type Inner;
    fn into_py_result(self) -> PyResult<Self::Inner>;
}

impl<T: NotPyResult> IntoPyResult for T {
    type Inner = T;
    fn into_py_result(self) -> PyResult<T> { Ok(self) }
}

impl<T> IntoPyResult for PyResult<T> {
    type Inner = T;
    fn into_py_result(self) -> PyResult<T> { self }
}

/// Helper trait for hacking around impl overlap rules in `IntoPyResult`.
pub auto trait NotPyResult {}
impl<T> !NotPyResult for PyResult<T> {}
