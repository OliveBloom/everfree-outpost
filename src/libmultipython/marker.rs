
use ptr::{PyBox, PyRef};

/// Marker trait for types that contain no Python values.  Types with Python values inside need
/// special handling because their destructors can only run while the Python GIL is held.
pub unsafe auto trait NonPy {}

impl !NonPy for PyBox {}
impl<'a> !NonPy for PyRef<'a> {}

