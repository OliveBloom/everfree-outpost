//! Reexport specific library definitions so we can access them through `$crate`.

pub use ::python3_sys::{
    self,
    PyObject,
    PyType_Slot, PyType_Spec,
    PyType_FromSpec,
    Py_TPFLAGS_DEFAULT, Py_TPFLAGS_HAVE_GC,
    destructor, traverseproc,
    Py_hash_t,
};

pub use ::libc::{c_char, c_int, c_uint};

pub use ::std::{cmp, mem, ptr};
