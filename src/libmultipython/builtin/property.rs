//! Descriptor type for properties implemented with a getter and a setter.

use std::mem;
use std::ptr;
use libc::{c_char, c_int, c_uint, c_void};
use python3_sys::*;

use api as py;
use builtin::BuiltinTypes;
use exc::{self, PyResult};
use ptr::{PyBox, PyRef};
use util;


//
// Public API
//

/// Type of getter functions.  These functions are unsafe because they (are permitted to) assume
/// that the argument has a specific Python type.
pub type Getter = unsafe fn(PyRef) -> PyResult<PyBox>;
pub type Setter = unsafe fn(PyRef, PyRef) -> PyResult<()>;

/// Construct a new `Getter` object.
///
/// This module guarantees that `func` will only be called on instances of `ty`.
pub fn new(ty: PyRef,
           getter: Option<Getter>,
           setter: Option<Setter>) -> PyResult<PyBox> {
    let bt = BuiltinTypes::get();
    unsafe { new_typed(bt.property.borrow(), ty, getter, setter) }
}

pub unsafe fn new_typed(prop_ty: PyRef,
                        ty: PyRef,
                        getter: Option<Getter>,
                        setter: Option<Setter>) -> PyResult<PyBox> {
    let obj = py::type_::instantiate(prop_ty)?;
    {
        let p = &mut *(obj.as_ptr() as *mut Property);
        ptr::write(&mut (*p).ty, ty.to_box());
        ptr::write(&mut (*p).getter, getter);
        ptr::write(&mut (*p).setter, setter);
    }
    Ok(obj)
}



//
// Implementation
//

#[repr(C)]
struct Property {
    #[allow(dead_code)]
    base: PyObject,
    /// Python type this descriptor is meant to be used with.  `getter` and `setter` will only be
    /// called on objects (instances) that have this type.
    ty: PyBox,
    getter: Option<Getter>,
    setter: Option<Setter>,
}

unsafe extern "C" fn dealloc(slf: *mut PyObject) {
    let p = &mut *(slf as *mut Property);
    ptr::drop_in_place(&mut (*p).ty);
    ptr::drop_in_place(&mut (*p).getter);
    ptr::drop_in_place(&mut (*p).setter);

    let ty = Py_TYPE(slf);
    let free: freefunc = mem::transmute(PyType_GetSlot(ty, Py_tp_free));
    free(slf as *mut c_void);
}

unsafe extern "C" fn repr(_slf: *mut PyObject) -> *mut PyObject {
    let r = py::unicode::from_str(&format!("<property>"));
    exc::return_result(r)
}

unsafe extern "C" fn get(slf: *mut PyObject,
                         obj: *mut PyObject,
                         _ty: *mut PyObject) -> *mut PyObject {
    let r = get_inner(PyRef::new_non_null(slf),
                      PyRef::new_opt(obj));

    exc::return_result(r)
}

unsafe fn get_inner(slf: PyRef,
                    obj: Option<PyRef>) -> PyResult<PyBox> {
    let obj = match obj {
        Some(x) => x,
        None => {
            // Binding to a class is a no-op
            return Ok(slf.to_box());
        },
    };

    let p = &*(slf.as_ptr() as *mut Property);

    if !py::object::is_instance(obj, p.ty.borrow()) {
        pyraise!(type_error, "descriptor operates only on {} instances (got {})",
                 util::repr_default(p.ty.borrow()), util::repr_default(obj));
    }

    if let Some(getter) = p.getter {
        getter(obj)
    } else {
        pyraise!(attribute_error, "attribute is not readable");
    }
}

unsafe extern "C" fn set(slf: *mut PyObject,
                         obj: *mut PyObject,
                         val: *mut PyObject) -> c_int {
    let r = set_inner(PyRef::new_non_null(slf),
                      PyRef::new_non_null(obj),
                      PyRef::new_non_null(val));

    exc::return_result_int(r)
}

unsafe fn set_inner(slf: PyRef,
                    obj: PyRef,
                    val: PyRef) -> PyResult<()> {
    let p = &*(slf.as_ptr() as *mut Property);

    if !py::object::is_instance(obj, p.ty.borrow()) {
        pyraise!(type_error, "descriptor operates only on {} instances (got {})",
                 util::repr_default(p.ty.borrow()), util::repr_default(obj));
    }

    if let Some(setter) = p.setter {
        setter(obj, val)
    } else {
        pyraise!(attribute_error, "attribute is not writable");
    }
}

pub fn register() -> PyResult<PyBox> {
    unsafe {
        macro_rules! slot {
            ($slot:expr, $val:expr) => {
                PyType_Slot { slot: $slot, pfunc: mem::transmute($val) }
            };
        }

        let mut slots = [
            slot!(Py_tp_dealloc, dealloc as destructor),
            slot!(Py_tp_repr, repr as reprfunc),
            slot!(Py_tp_descr_get, get as descrgetfunc),
            slot!(Py_tp_descr_set, set as descrsetfunc),
            slot!(0, 0_u64),
        ];

        let mut spec = PyType_Spec {
            name: "property\0".as_ptr() as *const c_char,
            basicsize: mem::size_of::<Property>() as c_int,
            itemsize: 0,
            flags: Py_TPFLAGS_DEFAULT as c_uint,
            slots: slots.as_mut_ptr(),
        };

        let raw = PyType_FromSpec(&mut spec);
        PyBox::new(raw)
    }
}



#[cfg(test)]
mod test {
    use super::*;
    use interp::Interp;
    use util;

    #[test]
    fn basic() {
        static mut VALUE: i64 = 10;

        fn get(_obj: PyRef) -> PyResult<PyBox> {
            let v = unsafe { VALUE };
            py::int::from_i64(v)
        }

        fn set(_obj: PyRef, val: PyRef) -> PyResult<()> {
            let v = py::int::as_i64(val)?;
            unsafe { VALUE = v };
            Ok(())
        }

        let mut i = Interp::new();
        i.enter(|| {
            let locals = py::dict::new().unwrap();
            util::exec_dict("if True:
                class C:
                    pass
            ", locals.clone()).unwrap();

            let c = py::dict::get_item_str(locals.borrow(), "C").unwrap().unwrap();
            let p = new(c, Some(get), Some(set)).unwrap();
            py::object::set_attr_str(c, "p", p.borrow()).unwrap();

            let old = util::eval_dict("C().p", locals.clone()).unwrap();
            assert_eq!(py::int::as_i64(old.borrow()).unwrap(), 10);
            util::exec_dict("C().p = 20", locals.clone()).unwrap();
            assert_eq!(unsafe { VALUE }, 20);
        });
    }
}
