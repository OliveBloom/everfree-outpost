//! Method type, replacing `PyCFunction`.  This type implements both the free and bound variants of
//! a method.

use std::mem;
use std::ptr;
use libc::{c_char, c_int, c_uint, c_void};
use python3_sys::*;

use api as py;
use builtin::BuiltinTypes;
use exc::{self, PyResult};
use ptr::{PyBox, PyRef};
use util;


//
// Public API
//

/// Construct a new `ClassMethod` object.  Unsafe because `func` must be safe to call when the
/// first argument is an instance of `ty`.
pub unsafe fn new(ty: PyRef,
                  slf: Option<PyRef>,
                  func: unsafe fn(PyRef, PyRef) -> PyResult<PyBox>,
                  name: &'static str) -> PyResult<PyBox> {
    let bt = BuiltinTypes::get();
    new_typed(bt.class_method.borrow(), ty, slf, func, name)
}

pub unsafe fn new_typed(method_ty: PyRef,
                        ty: PyRef,
                        slf: Option<PyRef>,
                        func: unsafe fn(PyRef, PyRef) -> PyResult<PyBox>,
                        name: &'static str) -> PyResult<PyBox> {
    if let Some(slf) = slf {
        pyassert!(py::object::is_instance(slf, ty),
                  type_error, "cannot bind {} to object {} (need instance of {})",
                  name, util::repr_default(slf), util::repr_default(ty));
    }

    let obj = py::type_::instantiate(method_ty)?;
    {
        let cm = &mut *(obj.as_ptr() as *mut ClassMethod);
        ptr::write(&mut (*cm).ty, ty.to_box());
        ptr::write(&mut (*cm).slf, slf.map(|r| r.to_box()));
        ptr::write(&mut (*cm).func, func);
        ptr::write(&mut (*cm).name, name);
    }
    Ok(obj)
}



//
// Implementation
//

#[repr(C)]
struct ClassMethod {
    #[allow(dead_code)]
    base: PyObject,
    ty: PyBox,
    /// Invariant: if `slf` is Some, then it is an instance of `ty`.
    slf: Option<PyBox>,
    /// Invariant: `func` is safe to call as long as the first argument is an instance of `ty`.
    func: unsafe fn(PyRef, PyRef) -> PyResult<PyBox>,
    name: &'static str,
}

unsafe extern "C" fn dealloc(obj: *mut PyObject) {
    let cm = &mut *(obj as *mut ClassMethod);
    ptr::drop_in_place(&mut (*cm).ty);
    ptr::drop_in_place(&mut (*cm).slf);
    ptr::drop_in_place(&mut (*cm).func);
    ptr::drop_in_place(&mut (*cm).name);

    let ty = Py_TYPE(obj);
    let free: freefunc = mem::transmute(PyType_GetSlot(ty, Py_tp_free));
    free(obj as *mut c_void);
}

unsafe extern "C" fn repr(obj: *mut PyObject) -> *mut PyObject {
    let cm = &*(obj as *mut ClassMethod);
    let s =
        if let Some(ref slf) = cm.slf {
            format!("<bound built-in method {} of {}>",
                    cm.name, util::repr_default(slf.borrow()))
        } else {
            format!("<built-in method {} of {} objects>",
                    cm.name, util::repr_default(cm.ty.borrow()))
        };
    let r = py::unicode::from_str(&s);
    exc::return_result(r)
}

unsafe extern "C" fn call(obj: *mut PyObject,
                          arg: *mut PyObject,
                          kw: *mut PyObject) -> *mut PyObject {
    let cm = &*(obj as *mut ClassMethod);
    let r = call_inner(cm, arg, kw);
    exc::return_result(r)
}

unsafe fn call_inner(cm: &ClassMethod,
                     args: *mut PyObject,
                     kws: *mut PyObject) -> PyResult<PyBox> {
    let args = PyRef::new_non_null(args);
    pyassert!(kws.is_null(),
              type_error, "{}() does not support keyword arguments", cm.name);

    if let Some(ref slf) = cm.slf {
        // Bound case
        (cm.func)(slf.borrow(), args)

    } else {
        // Unbound case
        pyassert!(py::tuple::size(args)? > 0,
                  type_error, "method {}() requires a `self` argument", cm.name);

        let slf = py::tuple::get_item(args, 0)?;
        pyassert!(py::object::is_instance(slf, cm.ty.borrow()),
                  type_error, "first argument of {}() must be instance of {}",
                  cm.name, util::repr_default(cm.ty.borrow()));

        let call_args = py::tuple::slice(args, 1..)?;

        (cm.func)(slf, call_args.borrow())
    }
}

unsafe extern "C" fn get(obj: *mut PyObject,
                         inst: *mut PyObject,
                         _ty: *mut PyObject) -> *mut PyObject {
    let r = get_inner(PyRef::new_non_null(obj),
                      PyRef::new_opt(inst));
    exc::return_result(r)
}

unsafe fn get_inner(obj: PyRef,
                    inst: Option<PyRef>) -> PyResult<PyBox> {
    let inst = match inst {
        Some(x) => x,
        None => {
            // Binding to a class is a no-op
            return Ok(obj.to_box());
        },
    };

    let cm = &*(obj.as_ptr() as *mut ClassMethod);

    if !py::object::is_instance(inst, cm.ty.borrow()) {
        pyraise!(type_error, "method {}() operates only on {} instances (got {})",
                 cm.name, util::repr_default(cm.ty.borrow()), util::repr_default(inst));
    }

    // Note we don't check `cm.slf` in __get__.  This is fine, it just means the script can re-bind
    // a bound method to a different object if they do something weird.

    let method_ty = py::object::type_(obj)?;
    new_typed(method_ty.borrow(),
              cm.ty.borrow(),
              Some(inst),
              cm.func,
              cm.name)
}

pub fn register() -> PyResult<PyBox> {
    unsafe {
        macro_rules! slot {
            ($slot:expr, $val:expr) => {
                PyType_Slot { slot: $slot, pfunc: mem::transmute($val) }
            };
        }

        let mut slots = [
            slot!(Py_tp_dealloc, dealloc as destructor),
            slot!(Py_tp_repr, repr as reprfunc),
            slot!(Py_tp_call, call as ternaryfunc),
            slot!(Py_tp_descr_get, get as descrgetfunc),
            slot!(0, 0_u64),
        ];

        let mut spec = PyType_Spec {
            name: "class_method\0".as_ptr() as *const c_char,
            basicsize: mem::size_of::<ClassMethod>() as c_int,
            itemsize: 0,
            flags: Py_TPFLAGS_DEFAULT as c_uint,
            slots: slots.as_mut_ptr(),
        };

        let raw = PyType_FromSpec(&mut spec);
        PyBox::new(raw)
    }
}





#[cfg(test)]
mod test {
    use super::*;
    use interp::Interp;
    use util;

    #[test]
    fn basic() {
        fn f(_slf: PyRef, _args: PyRef) -> PyResult<PyBox> {
            py::int::from_i64(123)
        }

        let mut i = Interp::new();
        i.enter(|| {
            let locals = py::dict::new().unwrap();
            util::exec_dict("if True:
                class C:
                    pass
            ", locals.clone()).unwrap();

            let c = py::dict::get_item_str(locals.borrow(), "C").unwrap().unwrap();
            let m = unsafe { new(c, None, f, "m") }.unwrap();
            py::object::set_attr_str(c, "m", m.borrow()).unwrap();

            let r1 = util::eval_dict("C.m", locals.clone()).unwrap();
            assert_eq!(&py::object::repr(r1.borrow()).unwrap(),
                       "<built-in method m of <class 'C'> objects>");

            let r2 = util::eval_dict("C().m", locals.clone()).unwrap();
            assert!(py::object::repr(r2.borrow()).unwrap().starts_with(
                "<bound built-in method m of <C object at "));

            let r3 = util::eval_dict("C().m()", locals.clone()).unwrap();
            assert_eq!(py::int::as_i64(r3.borrow()).unwrap(), 123);
        });
    }
}
