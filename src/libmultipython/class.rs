
use std::mem;
use std::ptr;
use libc::{c_int, c_void};
use python3_sys::*;

use api as py;
use exc::PyResult;
use marker::NonPy;
use ptr::{PyBox, PyRef};

#[macro_export]
macro_rules! python_class_generic_slot {
    ($DataTy:ty, $slot:ident, $name:ident, $functy:ident) => {
        $crate::reexports::PyType_Slot {
            slot: $crate::reexports::python3_sys::$slot,
            pfunc: $crate::reexports::mem::transmute(
                $crate::class::$name::<$DataTy> as $crate::reexports::$functy),
        }
    };
}

/// Core implementation for `python_class!`.
#[macro_export]
macro_rules! python_class_impl {
    (
        $name:ident, $DataTy:ty, $conv:tt,
        { $( $fname:ident $finfo:tt )* }
        { $( $sname:ident $sslot_id:ident $sctype:ident $sinfo:tt )* }
        { $( $pname:ident $pinfo:tt )* }
    ) => {
        {
            $( python_class_method_impl!($finfo, $DataTy, $conv); )*
            $( python_class_slot_impl!($sinfo, $DataTy, $conv); )*
            $( python_class_property_impl!($pinfo, $DataTy, $conv); )*


            // TODO: replace this with a `catch { ... }`
            fn _build() -> $crate::exc::PyResult<$crate::ptr::PyBox> {
                unsafe {
                    let mut slots = [
                        python_class_generic_slot!(
                            $DataTy, Py_tp_dealloc, dealloc, destructor),
                        python_class_generic_slot!(
                            $DataTy, Py_tp_traverse, traverse, traverseproc),
                        python_class_generic_slot!(
                            $DataTy, Py_tp_clear, clear, destructor),
                        $( $crate::reexports::PyType_Slot {
                            slot: $crate::reexports::python3_sys::$sslot_id,
                            pfunc: $crate::reexports::mem::transmute(
                                $sname as $crate::reexports::python3_sys::$sctype),
                        }, )*
                        $crate::reexports::PyType_Slot {
                            slot: 0,
                            pfunc: $crate::reexports::mem::transmute(0_usize),
                        },
                    ];

                    let mut spec = $crate::reexports::PyType_Spec {
                        name: concat!(stringify!($name), "\0").as_ptr()
                            as *const $crate::reexports::c_char,
                        basicsize: $crate::reexports::mem::size_of::<
                                $crate::class::ClassObject<$DataTy>>()
                            as $crate::reexports::c_int,
                        itemsize: 0,
                        flags: ($crate::reexports::Py_TPFLAGS_DEFAULT |
                                $crate::reexports::Py_TPFLAGS_HAVE_GC)
                            as $crate::reexports::c_uint,
                        slots: slots.as_mut_ptr(),
                    };

                    let ty_raw = $crate::reexports::PyType_FromSpec(&mut spec);
                    let ty = $crate::ptr::PyBox::new(ty_raw)?;

                    $(
                        let m = $crate::builtin::class_method::new(
                            ty.borrow(), None, $fname,
                            concat!(stringify!($name), ".", stringify!($fname)))?;
                        $crate::api::object::set_attr_str(
                            ty.borrow(), stringify!($fname), m.borrow())?;
                    )*

                    $(
                        let p = $crate::builtin::property::new(
                            ty.borrow(),
                            python_class_property_getter_func!($pinfo),
                            python_class_property_setter_func!($pinfo))?;
                        $crate::api::object::set_attr_str(
                            ty.borrow(), stringify!($pname), p.borrow())?;
                    )*

                    Ok(ty)
                }
            }

            _build()
        }
    };
}

/// Core implementation for `python_class_builder!`.
#[macro_export]
macro_rules! python_class_builder_impl {
    (
        $name:ident, $builder:ident, $DataTy:ty, $conv:tt,
        $funcs:tt $slots:tt $props:tt
    ) => {
        #[allow(non_snake_case)]
        fn $builder() -> $crate::exc::PyResult<$crate::ptr::PyBox> {
            python_class_impl! {
                $name, $DataTy, $conv,
                $funcs $slots $props
            }
        }
    };
}

//
// Methods
//

#[macro_export]
macro_rules! python_class_method_impl {
    ((static, $name:ident,
      ($($arg:pat, $ArgTy:ty;)*), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt) => {
        error_static_class_methods_not_yet_implemented
    };

    ((method, $name:ident,
      ($arg0:pat, $ArgTy0:ty; $($arg:pat, $ArgTy:ty;)*), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt) => {
        // Unsafe because `slf` must be a `ClassObject<$DataTy>`
        unsafe fn $name(slf: $crate::ptr::PyRef,
                        args: $crate::ptr::PyRef)
                        -> $crate::exc::PyResult<$crate::ptr::PyBox> {
            // NB: `_inner` is not marked unsafe, but it secretly is!  It requires `slf` to be a
            // `ClassObject<$DataTy>`.  We don't mark it unsafe because this is the easiest way to
            // put `$body` into a non-unsafe context (so that users don't accidentally invoke
            // unsafe code), and this is all okay because `_inner` is accessible only through the
            // outer (unsafe) `$name` function.
            fn _inner(slf: $crate::ptr::PyRef,
                      args: $crate::ptr::PyRef)
                      -> $crate::exc::PyResult<$crate::ptr::PyBox> {
                let $arg0: $ArgTy0 = unsafe {
                    <$ArgTy0 as $crate::class::SelfArg<$DataTy>>::from_py_ref(slf)?
                };
                let ($($arg,)*): ($($ArgTy,)*) =
                    python_do_unpack!(args, ($($ArgTy,)*), $conv)?;
                let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
                python_do_pack!(r, $RetTy, $conv)
            }

            _inner(slf, args)
        }
    };
}

//
// Properties
//

#[macro_export]
macro_rules! python_class_property_impl {
    ((get, $gname:ident, $sname:ident,
      ($gslf:pat, $GSlfTy:ty;), $GRetTy:ty, $gbody:expr;),
     $DataTy:ty, $conv:tt) => {
        python_class_property_getter_impl!(
            $gname, $gslf, $GSlfTy, $GRetTy, $gbody, $DataTy, $conv);
    };

    ((set, $gname:ident, $sname:ident,
      ($sslf:pat, $SSlfTy:ty; $sval:pat, $SValTy:ty;), $SRetTy:ty, $sbody:expr;),
     $DataTy:ty, $conv:tt) => {
        python_class_property_setter_impl!(
            $sname, $sslf, $SSlfTy, $sval, $SValTy, $SRetTy, $sbody, $DataTy, $conv);
    };

    ((get_set, $gname:ident, $sname:ident,
      ($gslf:pat, $GSlfTy:ty;), $GRetTy:ty, $gbody:expr;
      ($sslf:pat, $SSlfTy:ty; $sval:pat, $SValTy:ty;), $SRetTy:ty, $sbody:expr;),
     $DataTy:ty, $conv:tt) => {
        python_class_property_getter_impl!(
            $gname, $gslf, $GSlfTy, $GRetTy, $gbody, $DataTy, $conv);
        python_class_property_setter_impl!(
            $sname, $sslf, $SSlfTy, $sval, $SValTy, $SRetTy, $sbody, $DataTy, $conv);
    };
}

#[macro_export]
macro_rules! python_class_property_getter_impl {
    ($name:ident, $slf:pat, $SlfTy:ty, $RetTy:ty, $body:expr,
     $DataTy:ty, $conv:tt) => {
        unsafe fn $name(slf: $crate::ptr::PyRef)
                        -> $crate::exc::PyResult<$crate::ptr::PyBox> {
            // NB: Same unusual safety story as `python_class_method_impl` - see above
            fn _inner(slf: $crate::ptr::PyRef) -> $crate::exc::PyResult<$crate::ptr::PyBox> {
                let $slf: $SlfTy = unsafe {
                    <$SlfTy as $crate::class::SelfArg<$DataTy>>::from_py_ref(slf)?
                };
                let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
                python_do_pack!(r, $RetTy, $conv)
            }

            _inner(slf)
        }
    };
}

#[macro_export]
macro_rules! python_class_property_setter_impl {
    ($name:ident, $slf:pat, $SlfTy:ty, $val:pat, $ValTy:ty, $RetTy:ty, $body:expr,
     $DataTy:ty, $conv:tt) => {
        unsafe fn $name(slf: $crate::ptr::PyRef,
                        val: $crate::ptr::PyRef)
                        -> $crate::exc::PyResult<()> {
            // NB: Same unusual safety story as `python_class_method_impl` - see above
            fn _inner(slf: $crate::ptr::PyRef,
                      val: $crate::ptr::PyRef)
                      -> $crate::exc::PyResult<()> {
                let $slf: $SlfTy = unsafe {
                    <$SlfTy as $crate::class::SelfArg<$DataTy>>::from_py_ref(slf)?
                };
                let $val: $ValTy = python_do_unpack!(val, $ValTy, $conv)?;
                let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
                <$RetTy as $crate::conv::IntoPyResult>::into_py_result(r)
            }

            _inner(slf, val)
        }
    };
}

#[macro_export]
macro_rules! python_class_property_getter_func {
    ((get, $gname:ident, $sname:ident, $gargs:tt, $GRetTy:ty, $gbody:expr;)) => {
        Some($gname)
    };
    ((set, $sname:ident, $sname:ident, $sargs:tt, $SRetTy:ty, $sbody:expr;)) => {
        None
    };
    ((get_set, $gname:ident, $sname:ident,
      $gargs:tt, $GRetTy:ty, $gbody:expr;
      $sargs:tt, $SRetTy:ty, $sbody:expr;)) => {
        Some($gname)
    };
}

#[macro_export]
macro_rules! python_class_property_setter_func {
    ((get, $gname:ident, $sname:ident, $gargs:tt, $GRetTy:ty, $gbody:expr;)) => {
        None
    };
    ((set, $sname:ident, $sname:ident, $sargs:tt, $SRetTy:ty, $sbody:expr;)) => {
        Some($sname)
    };
    ((get_set, $gname:ident, $sname:ident,
      $gargs:tt, $GRetTy:ty, $gbody:expr;
      $sargs:tt, $SRetTy:ty, $sbody:expr;)) => {
        Some($sname)
    };
}

//
// Slots
//

#[macro_export]
macro_rules! python_class_slot_impl {
    ((initproc, $name:ident, $slot_id:ident,
      ($($arg:pat, $ArgTy:ty;)*), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt) => {
        // Unsafe because `slf` must be a `ClassObject<$DataTy>`
        unsafe extern "C" fn $name(slf: *mut $crate::reexports::PyObject,
                                   args: *mut $crate::reexports::PyObject,
                                   kwargs: *mut $crate::reexports::PyObject)
                                   -> $crate::reexports::c_int {
            fn _inner(args: $crate::ptr::PyRef)
                      -> $crate::exc::PyResult<$DataTy> {
                let ($($arg,)*): ($($ArgTy,)*) =
                    python_do_unpack!(args, ($($ArgTy,)*), $conv)?;
                let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
                <$RetTy as $crate::conv::IntoPyResult>::into_py_result(r)
            }

            // TODO: replace with `catch`
            unsafe fn wrapper(slf: *mut $crate::reexports::PyObject,
                              args: *mut $crate::reexports::PyObject,
                              kwargs: *mut $crate::reexports::PyObject)
                              -> $crate::exc::PyResult<()> {
                let slf = &mut *(slf as *mut $crate::class::ClassObject<$DataTy>);
                let args = $crate::ptr::PyRef::new_non_null(args);
                pyassert!(kwargs.is_null(),
                          type_error, "constructor does not support kwargs");

                let data = _inner(args)?;

                assert!(!slf.inited);
                $crate::reexports::ptr::write(&mut slf.data, data);
                slf.inited = true;
                Ok(())
            }

            $crate::exc::return_result_int(wrapper(slf, args, kwargs))
        }
    };

    ((reprfunc, $name:ident, $slot_id:ident,
      ($slf:pat, $SlfTy:ty;), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt) => {
        // Unsafe because `slf` must be a `ClassObject<$DataTy>`
        unsafe extern "C" fn $name(slf: *mut $crate::reexports::PyObject)
                                   -> *mut $crate::reexports::PyObject {
            fn _inner(slf: $crate::ptr::PyRef)
                      -> $crate::exc::PyResult<$crate::ptr::PyBox> {
                let $slf: $SlfTy = unsafe {
                    <$SlfTy as $crate::class::SelfArg<$DataTy>>::from_py_ref(slf)?
                };
                let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
                python_do_pack!(r, $RetTy, $conv)
            }

            $crate::exc::return_result(_inner(PyRef::new_non_null(slf)))
        }
    };

    ((hashfunc, $name:ident, $slot_id:ident,
      ($slf:pat, $SlfTy:ty;), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt) => {
        // Unsafe because `slf` must be a `ClassObject<$DataTy>`
        unsafe extern "C" fn $name(slf: *mut $crate::reexports::PyObject)
                                   -> $crate::reexports::Py_hash_t {
            fn _inner(slf: $crate::ptr::PyRef)
                      -> $crate::exc::PyResult<$crate::reexports::Py_hash_t> {
                let $slf: $SlfTy = unsafe {
                    <$SlfTy as $crate::class::SelfArg<$DataTy>>::from_py_ref(slf)?
                };
                let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
                <$RetTy as $crate::conv::IntoPyResult>::into_py_result(r)
                    .map(|h| h as $crate::reexports::Py_hash_t)
            }

            match _inner($crate::ptr::PyRef::new_non_null(slf)) {
                // -1 is reserved to indicate failure, so if the value hashes to -1, change it.
                Ok(h) => if h == -1 { -2 } else { h },
                Err(e) => {
                    $crate::api::err::raise(*e);
                    -1
                },
            }
        }
    };

    // `__eq__` and `__cmp__` are two different ways of implementing the `tp_richcompare` slot.
    ((richcmpfunc, __cmp__, $slot_id:ident,
      ($slf:pat, $SlfTy:ty; $other:pat, $OtherTy:ty;), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt) => {
        // Unsafe because `slf` must be a `ClassObject<$DataTy>`
        unsafe extern "C" fn __cmp__(slf: *mut $crate::reexports::PyObject,
                                     other: *mut $crate::reexports::PyObject,
                                     op: $crate::reexports::c_int)
                                     -> *mut $crate::reexports::PyObject {
            fn _inner(slf: $crate::ptr::PyRef,
                      other: $crate::ptr::PyRef)
                      -> $crate::exc::PyResult<$crate::reexports::cmp::Ordering> {
                let $slf: $SlfTy = unsafe {
                    <$SlfTy as $crate::class::SelfArg<$DataTy>>::from_py_ref(slf)?
                };
                let $other: $OtherTy = python_do_unpack!(other, $OtherTy, $conv)?;
                let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
                <$RetTy as $crate::conv::IntoPyResult>::into_py_result(r)
            }

            let order = match _inner(PyRef::new_non_null(slf),
                                     PyRef::new_non_null(other)) {
                Ok(o) => o,
                Err(e) => {
                    if e.type_() == $crate::api::exc::type_error() {
                        return $crate::api::not_implemented().to_box().unwrap();
                    } else {
                        $crate::api::err::raise(*e);
                        return $crate::reexports::ptr::null_mut();
                    }
                },
            };

            let r = match op {
                $crate::reexports::python3_sys::Py_LT =>
                    order == $crate::reexports::cmp::Ordering::Less,
                $crate::reexports::python3_sys::Py_LE =>
                    order == $crate::reexports::cmp::Ordering::Less ||
                    order == $crate::reexports::cmp::Ordering::Equal,
                $crate::reexports::python3_sys::Py_GT =>
                    order == $crate::reexports::cmp::Ordering::Greater,
                $crate::reexports::python3_sys::Py_GE =>
                    order == $crate::reexports::cmp::Ordering::Greater ||
                    order == $crate::reexports::cmp::Ordering::Equal,
                $crate::reexports::python3_sys::Py_EQ =>
                    order == $crate::reexports::cmp::Ordering::Equal,
                $crate::reexports::python3_sys::Py_NE =>
                    order == $crate::reexports::cmp::Ordering::Less ||
                    order == $crate::reexports::cmp::Ordering::Greater,
                _ => unreachable!(),
            };

            $crate::api::bool::from_bool(r).to_box().unwrap()
        }
    };

    ((richcmpfunc, __eq__, $slot_id:ident,
      ($slf:pat, $SlfTy:ty; $other:pat, $OtherTy:ty;), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt) => {
        // Unsafe because `slf` must be a `ClassObject<$DataTy>`
        unsafe extern "C" fn __eq__(slf: *mut $crate::reexports::PyObject,
                                    other: *mut $crate::reexports::PyObject,
                                    op: $crate::reexports::c_int)
                                    -> *mut $crate::reexports::PyObject {
            fn _inner(slf: $crate::ptr::PyRef,
                      other: $crate::ptr::PyRef)
                      -> $crate::exc::PyResult<bool> {
                let $slf: $SlfTy = unsafe {
                    <$SlfTy as $crate::class::SelfArg<$DataTy>>::from_py_ref(slf)?
                };
                let $other: $OtherTy = python_do_unpack!(other, $OtherTy, $conv)?;
                let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
                <$RetTy as $crate::conv::IntoPyResult>::into_py_result(r)
            }

            let eq = match _inner(PyRef::new_non_null(slf),
                                  PyRef::new_non_null(other)) {
                Ok(o) => o,
                Err(e) => {
                    if false && e.type_() == $crate::api::exc::type_error() {
                        return $crate::api::not_implemented().to_box().unwrap();
                    } else {
                        $crate::api::err::raise(*e);
                        return $crate::reexports::ptr::null_mut();
                    }
                },
            };

            let r = match op {
                $crate::reexports::python3_sys::Py_EQ => eq,
                $crate::reexports::python3_sys::Py_NE => !eq,

                $crate::reexports::python3_sys::Py_LT |
                $crate::reexports::python3_sys::Py_LE |
                $crate::reexports::python3_sys::Py_GT |
                $crate::reexports::python3_sys::Py_GE =>
                    return $crate::api::not_implemented().to_box().unwrap(),

                _ => unreachable!(),
            };

            $crate::api::bool::from_bool(r).to_box().unwrap()
        }
    };

        // Arithmetic operations

    ((binaryfunc, $name:ident, $slot_id:ident,
      ($a:pat, $ATy:ty; $b:pat, $BTy:ty;), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt) => {
        unsafe extern "C" fn $name(a: *mut $crate::reexports::PyObject,
                                   b: *mut $crate::reexports::PyObject)
                                   -> *mut $crate::reexports::PyObject {
            // There is no guarantee that the first argument is an instance of the class, so use
            // normal unpacking logic for all args.  We also check for `TypeError` during unpacking
            // and return `NotImplemented` in such cases.
            fn _inner(a: $crate::ptr::PyRef,
                      b: $crate::ptr::PyRef)
                      -> $crate::exc::PyResult<$crate::ptr::PyBox> {
                let $a: $ATy = match python_do_unpack!(a, $ATy, $conv) {
                    Ok(a) => a,
                    Err(e) => {
                        if e.type_() == $crate::api::exc::type_error() {
                            return Ok($crate::api::not_implemented().to_box());
                        } else {
                            return Err(e);
                        }
                    },
                };
                let $b: $BTy = match python_do_unpack!(b, $BTy, $conv) {
                    Ok(b) => b,
                    Err(e) => {
                        if e.type_() == $crate::api::exc::type_error() {
                            return Ok($crate::api::not_implemented().to_box());
                        } else {
                            return Err(e);
                        }
                    },
                };
                let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
                python_do_pack!(r, $RetTy, $conv)
            }

            $crate::exc::return_result(_inner(PyRef::new_non_null(a),
                                              PyRef::new_non_null(b)))
        }
    };

    ((unaryfunc, $name:ident, $slot_id:ident,
      ($slf:pat, $SlfTy:ty;), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt) => {
        // Unsafe because `slf` must be a `ClassObject<$DataTy>`
        unsafe extern "C" fn $name(slf: *mut $crate::reexports::PyObject)
                                   -> *mut $crate::reexports::PyObject {
            fn _inner(slf: $crate::ptr::PyRef)
                      -> $crate::exc::PyResult<$crate::ptr::PyBox> {
                let $slf: $SlfTy = unsafe {
                    <$SlfTy as $crate::class::SelfArg<$DataTy>>::from_py_ref(slf)?
                };
                let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
                python_do_pack!(r, $RetTy, $conv)
            }

            $crate::exc::return_result(_inner(PyRef::new_non_null(slf)))
        }
    };
}


#[repr(C)]
pub struct ClassObject<T> {
    #[allow(dead_code)]
    pub base: PyObject,
    pub inited: bool,
    pub data: T,
}


pub unsafe extern "C" fn dealloc<T>(obj: *mut PyObject) {
    clear::<T>(obj);
    // `clear` dropped `o.data`.  The only other field, `o.inited`, doesn't need to be
    // dropped.

    let ty = Py_TYPE(obj);
    let slot = PyType_GetSlot(ty, Py_tp_free);
    let free: freefunc = mem::transmute(slot);
    free(obj as *mut c_void);
}

pub unsafe extern "C" fn clear<T>(obj: *mut PyObject) {
    let o = &mut *(obj as *mut ClassObject<T>);

    // `clear` may get called from both the cycle collector and `dealloc`.
    if o.inited {
        // It's important to clear `o.inited` first, so the object is not observed
        // while in a partially inited state.
        o.inited = false;
        ptr::drop_in_place(&mut o.data);
    }
}

pub unsafe extern "C" fn traverse<T: ClassData>(obj: *mut PyObject,
                                                visit: visitproc,
                                                arg: *mut c_void) -> c_int {
    let o = &mut *(obj as *mut ClassObject<T>);
    if !o.inited {
        return 0;
    }
    let r = o.data.traverse(|obj| {
        let r = visit(obj.as_ptr(), arg);
        if r == 0 { Ok(()) } else { Err(r) }
    });

    if let Err(r) = r { r } else { 0 }
}



/// Try to get a reference to the instance data of `obj`.  Unsafe because `obj` must be a
/// `ClassObject<T>`.  Returns `None` if the instance data is not initialized.
pub unsafe fn get<'a, T>(obj: PyRef<'a>) -> Option<&'a T> {
    let o = &*(obj.as_ptr() as *const ClassObject<T>);
    if !o.inited {
        return None;
    }
    Some(&o.data)
}

/// Like `get`, but doesn't check that the data is initialized.
pub unsafe fn get_raw<'a, T>(obj: PyRef<'a>) -> &'a T {
    let o = &*(obj.as_ptr() as *const ClassObject<T>);
    &o.data
}

pub unsafe fn get_mut<'a, T>(obj: PyRef<'a>) -> Option<&'a mut T> {
    let o = &mut *(obj.as_ptr() as *mut ClassObject<T>);
    if !o.inited {
        return None;
    }
    Some(&mut o.data)
}

pub unsafe fn get_raw_mut<'a, T>(obj: PyRef<'a>) -> &'a mut T {
    let o = &mut *(obj.as_ptr() as *mut ClassObject<T>);
    &mut o.data
}

/// Create an instance of `ty`, populated with the given instance data.  Unsafe because `ty` must
/// be a Python class implemented as `ClassObject<T>`.
pub unsafe fn instantiate<T>(ty: PyRef, data: T) -> PyResult<PyBox> {
    let obj = py::type_::instantiate(ty)?;
    {
        let raw = &mut *(obj.as_ptr() as *mut ClassObject<T>);

        // `type_::instantiate` should zero-initialize the object, setting `inited` to `false`.
        assert!(!raw.inited);
        ptr::write(&mut raw.data, data);
        raw.inited = true;
    }
    Ok(obj)
}



pub trait ClassData {
    fn traverse<F>(&self, visit: F) -> Result<(), c_int>
        where F: Fn(PyRef) -> Result<(), c_int>;
}

impl<T: NonPy> ClassData for T {
    fn traverse<F>(&self, _visit: F) -> Result<(), c_int>
            where F: Fn(PyRef) -> Result<(), c_int> {
        // NonPy bound implies there are no python refs to traverse.
        Ok(())
    }
}



/// Helper trait for converting a `PyRef` to a more useful `self` argument type.
pub trait SelfArg<'a, D>: Sized {
    /// Convert a `PyRef` to the desired `self` argument type.
    ///
    /// This is unsafe because it assumes `PyRef` is a `ClassObject<D>`.
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<Self>;
}

impl<'a, D> SelfArg<'a, D> for &'a D {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<&'a D> {
        let obj = &*(slf.as_ptr() as *const ClassObject<D>);
        pyassert!(obj.inited,
                  runtime_error, "object has already been destructed");
        Ok(&obj.data)
    }
}

impl<'a, D> SelfArg<'a, D> for &'a mut D {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<&'a mut D> {
        let obj = &mut *(slf.as_ptr() as *mut ClassObject<D>);
        pyassert!(obj.inited,
                  runtime_error, "object has already been destructed");
        Ok(&mut obj.data)
    }
}

/// The `NonPy` bound prevents this impl from overlapping with the one for `PyRef`.
impl<'a, D: Copy + NonPy + 'a> SelfArg<'a, D> for D {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<D> {
        <&'a D as SelfArg<'a, D>>::from_py_ref(slf).map(|x| *x)
    }
}

impl<'a, D> SelfArg<'a, D> for PyRef<'a> {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<PyRef<'a>> {
        Ok(slf)
    }
}

impl<'a, D> SelfArg<'a, D> for PyBox {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<PyBox> {
        Ok(slf.to_box())
    }
}



#[cfg(test)]
mod test {
    use syntax_exts::python_class;

    use interp::Interp;
    use util;

    #[test]
    fn basic() {
        let mut i = Interp::new();

        static mut VALUE: i32 = 0;

        i.enter(|| {
            let c = python_class! {
                name = test_class;
                data: i32;

                fn __init__() -> i32 { 0 }

                get value(&self) -> i32 { *self }
                set value(&mut self, val: i32) { *self = val; }

                fn export(&self) {
                    unsafe { VALUE = *self };
                }
            }.unwrap();

            util::exec_vars("if True:
                inst = C()
                assert inst.value == 0
                inst.value = 10
                assert inst.value == 10
                inst.export()
            ", &[("C", c.borrow())]).unwrap();

            assert_eq!(unsafe { VALUE }, 10);
        });
    }
}
