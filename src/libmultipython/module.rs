use builtin::module;
use exc::PyResult;
use marker::NonPy;
use ptr::{PyBox, PyRef};

#[macro_export]
macro_rules! python_build_generic_fn {
    ($name:ident, ($($GDef:tt)*), $args:tt, $Ret:ty, $body:expr) => {
        fn $name $($GDef)* $args -> $Ret { $body }
    };
}

/// Core implementation for `python_module!` and `python_module_builder!`.
#[macro_export]
macro_rules! python_module_impl {
    (
        $name:ident, $DataTy:ty, $data:expr, $conv:tt, $GDef:tt, $GUse:tt,
        { $( $fname:ident $finfo:tt )* }
        { /* no slots */ }
        { /* no props */ }
    ) => {
        {
            $( python_module_method_impl!($finfo, $DataTy, $conv, $GDef); )*

            python_build_generic_fn! {
                _build, $GDef, (), $crate::exc::PyResult<$crate::ptr::PyBox>, {
                    python_module_body! {
                        $name, $DataTy, $data, $conv, $GUse,
                        { $( $fname $finfo )* }
                        { /* no slots */ }
                        { /* no props */ }
                    }
                }
            }

            python_generic_use!(_build, $GUse)()
        }
    };
}

#[macro_export]
macro_rules! python_module_builder_impl {
    (
        $name:ident, $builder:ident, $DataTy:ty, $conv:tt, $GDef:tt, $GUse:tt,
        { $( $fname:ident $finfo:tt )* }
        { /* no slots */ }
        { /* no props */ }
    ) => {
        fn $builder(data: $DataTy) -> $crate::exc::PyResult<$crate::ptr::PyBox> {
            $( python_module_method_impl!($finfo, $DataTy, $conv, $GDef); )*

            python_module_body! {
                $name, $DataTy, data, $conv, $GUse,
                { $( $fname $finfo )* }
                { /* no slots */ }
                { /* no props */ }
            }
        }
    };
}

#[macro_export]
macro_rules! python_module_body {
    (
        $name:ident, $DataTy:ty, $data:expr, $conv:tt, $GUse:tt,
        { $( $fname:ident $finfo:tt )* }
        { /* no slots */ }
        { /* no props */ }
    ) => {
        let m = $crate::builtin::module::new::<$DataTy>(
            stringify!($name), $data)?;

        $(
            let f = unsafe {
                $crate::builtin::module_func::new(
                    python_generic_use!($fname, $GUse), m.clone(),
                    concat!(stringify!($name), ".", stringify!($fname)))?
            };
            $crate::api::object::set_attr_str(
                m.borrow(), stringify!($fname), f.borrow())?;
        )*

        Ok(m)
    };
}

#[macro_export]
macro_rules! python_module_method_impl {
    ((static, $name:ident,
      ($($arg:pat, $ArgTy:ty;)*), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt, ($($GDef:tt)*)) => {
        fn $name $($GDef)* (_module: $crate::ptr::PyRef,
                            args: $crate::ptr::PyRef)
                            -> $crate::exc::PyResult<$crate::ptr::PyBox> {
            let ($($arg,)*): ($($ArgTy,)*) =
                python_do_unpack!(args, ($($ArgTy,)*), $conv)?;
            let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
            python_do_pack!(r, $RetTy, $conv)
        }
    };

    ((method, $name:ident,
      ($arg0:pat, $ArgTy0:ty; $($arg:pat, $ArgTy:ty;)*), $RetTy:ty, $body:expr;),
     $DataTy:ty, $conv:tt, ($($GDef:tt)*)) => {
        fn $name $($GDef)* (module: $crate::ptr::PyRef,
                            args: $crate::ptr::PyRef)
                            -> $crate::exc::PyResult<$crate::ptr::PyBox> {
            let $arg0: $ArgTy0 = unsafe {
                <$ArgTy0 as $crate::module::SelfArg<$DataTy>>::from_py_ref(module)?
            };
            let ($($arg,)*): ($($ArgTy,)*) =
                python_do_unpack!(args, ($($ArgTy,)*), $conv)?;
            let r: $RetTy = $crate::util::panic_to_exc(|| $body)?;
            python_do_pack!(r, $RetTy, $conv)
        }
    };
}

#[macro_export]
macro_rules! python_do_unpack {
    ($val:expr, $Ty:ty, ()) => {
        <$Ty as $crate::conv::Unpack>::unpack($val)
    };
    ($val:expr, $Ty:ty, ($Pack:path, $Unpack:path)) => {
        <$Ty as $Unpack>::unpack($val)
    };
}

#[macro_export]
macro_rules! python_do_pack {
    ($val:expr, $Ty:ty, ()) => {
        <$Ty as $crate::conv::Pack>::pack($val)
    };
    ($val:expr, $Ty:ty, ($Pack:path, $Unpack:path)) => {
        <$Ty as $Pack>::pack($val)
    };
}

#[macro_export]
macro_rules! python_generic_use {
    ($name:ident, ($($A:ident),*)) => {
        $name::<$($A,)*>
    };
}



/// Helper trait for converting a `PyRef` to a more useful `self` argument type.
pub trait SelfArg<'a, D>: Sized {
    /// Convert a `PyRef` to the desired `self` argument type.
    ///
    /// This is unsafe because it assumes `PyRef` is a module object with data type `D`.
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<Self>;
}

impl<'a, D: 'static> SelfArg<'a, D> for &'a D {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<&'a D> {
        let data = pyunwrap!(module::get_unchecked::<D>(slf),
                             runtime_error, "module has already been destructed");
        Ok(data)
    }
}

impl<'a, D: 'static> SelfArg<'a, D> for &'a mut D {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<&'a mut D> {
        let data = pyunwrap!(module::get_mut_unchecked::<D>(slf),
                             runtime_error, "module has already been destructed");
        Ok(data)
    }
}

/// The `NonPy` bound prevents this impl from overlapping with the one for `PyRef`.
impl<'a, D: Copy + NonPy + 'static> SelfArg<'a, D> for D {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<D> {
        <&'a D as SelfArg<'a, D>>::from_py_ref(slf).map(|x| *x)
    }
}

impl<'a, D> SelfArg<'a, D> for PyRef<'a> {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<PyRef<'a>> {
        Ok(slf)
    }
}

impl<'a, D> SelfArg<'a, D> for PyBox {
    unsafe fn from_py_ref(slf: PyRef<'a>) -> PyResult<PyBox> {
        Ok(slf.to_box())
    }
}



#[cfg(test)]
mod test {
    use syntax_exts::python_module;

    use api as py;
    use interp::Interp;
    use util;

    #[test]
    fn basic() {
        Interp::new().enter(|| {
            let m = python_module! {
                name = test_module;
                data: () = ();

                fn test(&self) -> i32 {
                    1
                }

                fn test2() -> i32 {
                    2
                }
            }.unwrap();

            let r = util::eval_vars("m.test() + m.test2()",
                                    &[("m", m.borrow())]).unwrap();
            let r_int = py::int::as_i64(r.borrow()).unwrap();
            assert_eq!(r_int, 3);
        });
    }
}
