use std::path::Path;
use std::panic::{self, AssertUnwindSafe};

use python3_sys::*;

use api::*;


pub fn eval(code: &str) -> PyResult<PyBox> {
    let locals = dict::new()?;
    eval_dict(code, locals)
}

pub fn eval_dict(code: &str, locals: PyBox) -> PyResult<PyBox> {
    let builtins = eval::get_builtins();
    let eval = dict::get_item_str(builtins.borrow(), "eval")?
        .expect("missing `eval` in `__builtins__`");
    let code_obj = unicode::from_str(code)?;
    let globals = dict::new()?;
    let args = tuple::pack3(code_obj, globals, locals)?;
    object::call(eval, args.borrow(), None)
}

pub fn eval_vars(code: &str, vars: &[(&str, PyRef)]) -> PyResult<PyBox> {
    let locals = dict::new()?;
    for &(name, val) in vars {
        dict::set_item_str(locals.borrow(), name, val)?;
    }
    eval_dict(code, locals)
}


pub fn exec(code: &str) -> PyResult<PyBox> {
    let locals = dict::new()?;
    exec_dict(code, locals)
}

pub fn exec_dict(code: &str, locals: PyBox) -> PyResult<PyBox> {
    let builtins = eval::get_builtins();
    let exec = dict::get_item_str(builtins.borrow(), "exec")?
        .expect("missing `exec` in `__builtins__`");
    let code_obj = unicode::from_str(code)?;
    let globals = dict::new()?;
    let args = tuple::pack3(code_obj, globals, locals)?;
    object::call(exec, args.borrow(), None)
}

pub fn exec_vars(code: &str, vars: &[(&str, PyRef)]) -> PyResult<PyBox> {
    let locals = dict::new()?;
    for &(name, val) in vars {
        dict::set_item_str(locals.borrow(), name, val)?;
    }
    exec_dict(code, locals)
}


pub fn run_file(path: &Path) -> PyResult<()> {
    let builtins = eval::get_builtins();
    let compile = try!(dict::get_item_str(builtins.borrow(), "compile"))
        .expect("missing `compile` in `__builtins__`");
    let exec = try!(dict::get_item_str(builtins.borrow(), "exec"))
        .expect("missing `exec` in `__builtins__`");

    // Compile this little runner program to a code object.  The runner does the actual work of
    // opening and reading the indicated file.
    let runner = try!(unicode::from_str(r#"if True:  # indentation hack
        import sys
        dct = sys.modules['__main__'].__dict__
        dct['__file__'] = filename
        with open(filename, 'r') as f:
            code = compile(f.read(), filename, 'exec')
        exec(code, dct, dct)
        "#));
    let compile_args = try!(tuple::pack3(runner,
                                         try!(unicode::from_str("<runner>")),
                                         try!(unicode::from_str("exec"))));
    let runner_code = try!(object::call(compile, compile_args.borrow(), None));

    // Now `exec` the compiled runner.  We don't call `exec` directly on `runner` because `exec`
    // doesn't allow for setting the filename.
    let globals = try!(dict::new());
    let locals = try!(dict::new());
    // TODO: be smarter about non-UTF8 Path encodings
    try!(dict::set_item_str(locals.borrow(),
                            "filename",
                            try!(unicode::from_str(path.to_str().unwrap())).borrow()));
    let args = try!(tuple::pack3(runner_code, globals, locals));
    try!(object::call(exec, args.borrow(), None));
    Ok(())
}


pub fn import(name: &str) -> PyResult<PyBox> {
    let name_obj = try!(unicode::from_str(name));
    unsafe {
        PyBox::new(PyImport_Import(name_obj.unwrap()))
    }
}

/// Add a module to `sys.modules`, so it can be imported with a Python `import` statement.
///
/// This function has no special handling for dotted module names - the caller must manually
/// register the parent modules and set the appropriate attributes.
pub fn register_module(m: PyRef) -> PyResult<()> {
    let sys = import("sys")?;
    let modules = object::get_attr_str(sys.borrow(), "modules")?;

    let name = object::get_attr_str(m, "__name__")?;
    if dict::contains(modules.borrow(), name.borrow())? {
        if let Ok(s) = unicode::as_string(name.borrow()) {
            pyraise!(key_error, "a module named {:?} already exists", s);
        } else {
            pyraise!(key_error, "a module with the same name already exists");
        }
    }
    dict::set_item(modules.borrow(), name.borrow(), m)?;

    Ok(())
}

pub fn repr_default(obj: PyRef) -> String {
    object::repr(obj)
        .unwrap_or_else(|_| "<repr failed>".to_owned())
}

/// Run `f`, converting any Rust panics to Python exceptions.
pub fn panic_to_exc<F: FnOnce() -> R, R>(f: F) -> PyResult<R> {
    // We take the "YOLO" approach to exception safety here.  All unsafe code in `outpost` is
    // intended to preserve consistency even in the face of panics.  Hopefully `libstd` does the
    // same.
    let r = panic::catch_unwind(AssertUnwindSafe(f));
    match r {
        Ok(x) => Ok(x),
        Err(e) => {
            if let Some(s) = e.downcast_ref::<&'static str>() {
                pyraise!(runtime_error, "panic: {}", s);
            } else if let Some(s) = e.downcast_ref::<String>() {
                pyraise!(runtime_error, "panic: {}", s);
            } else {
                pyraise!(runtime_error, "panic");
            }
        },
    }
}
