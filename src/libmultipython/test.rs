use std::mem;
use std::thread;
use std::time::{Instant, Duration};

use interp::Interp;

#[test]
fn one_interp() {
    let i = Interp::new();
    mem::drop(i);
}

#[test]
fn two_interp() {
    let i1 = Interp::new();
    let i2 = Interp::new();
    mem::drop(i1);
    mem::drop(i2);
}

#[test]
fn two_interp_multithreaded() {
    let sleep_dur = Duration::from_secs(1);

    let start = Instant::now();

    let mut js = Vec::new();
    for _ in 0..10 {
        let j = thread::spawn(move || {
            let i = Interp::new();
            thread::sleep(sleep_dur);
            mem::drop(i);
        });
        js.push(j);
    }

    for j in js {
        j.join().unwrap();
    }

    let dur = start.elapsed();
    // We should definitely be able to run 10 parallel `sleep(1)` calls in under 5 seconds.
    assert!(dur < sleep_dur * 5);
    println!("took {:?}", dur);
}

#[test]
fn enter_leave() {
    let mut i = Interp::new();
    let ok = i.enter(|| {
        Interp::leave(|| {
            true
        })
    });
    assert!(ok);
}
