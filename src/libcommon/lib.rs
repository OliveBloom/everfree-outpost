#![crate_name = "common"]

#[macro_use] extern crate bitflags;
#[macro_use] extern crate log;

pub extern crate common_crypto;
pub extern crate common_data;
pub extern crate common_physics;
pub extern crate common_proto;
pub extern crate common_types;
#[macro_use] pub extern crate common_util;

pub use common_crypto as crypto;
pub use common_data as data;
pub use common_physics as physics;
pub use common_proto as proto;
pub use common_util as util;


pub mod gauge;
pub use self::gauge::Gauge;

#[cfg(asmjs)] pub mod types_client;
#[cfg(asmjs)] pub use self::types_client as types;

#[cfg(not(asmjs))] pub mod types_server;
#[cfg(not(asmjs))] pub use self::types_server as types;

pub mod v3 {}

pub mod appearance;
pub use self::appearance::Appearance;

pub mod activity;
pub use self::activity::Activity;

pub mod movement;
