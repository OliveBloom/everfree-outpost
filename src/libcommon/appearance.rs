use std::prelude::v1::*;
use std::error;
use std::fmt;
use std::result;

use util;


bitflags! {
    pub flags AppearanceFlags: u8 {
        const APP_WING =    1,
        const APP_HORN =    2,
        const APP_MALE =    4,
        const APP_LIGHT =   8,
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub struct Appearance {
    pub flags: AppearanceFlags,
    pub coat_color: (u8, u8, u8),
    pub mane_color: (u8, u8, u8),
    pub mane: u8,
    pub tail: u8,
    // TODO: equipment
}

impl Default for Appearance {
    fn default() -> Appearance {
        Appearance {
            flags: AppearanceFlags::empty(),
            coat_color: (0, 0, 0),
            mane_color: (0, 0, 0),
            mane: 0,
            tail: 0,
        }
    }
}

impl Appearance {
    pub fn to_bytes(&self) -> Box<[u8]> {
        Box::new([
            0x01,

            self.flags.bits(),

            self.coat_color.0,
            self.coat_color.1,
            self.coat_color.2,

            self.mane_color.0,
            self.mane_color.1,
            self.mane_color.2,

            self.mane,
            self.tail,
        ])
    }

    pub fn from_bytes(b: &[u8]) -> Result<Appearance> {
        if b.len() == 0 {
            return Err(Error::MissingFormat);
        }

        let ver = b[0];
        match ver {
            0x00 => Self::from_bytes_fmt_00(b),
            0x01 => Self::from_bytes_fmt_01(b),

            _ => return Err(Error::Format(ver)),
        }
    }

    fn from_bytes_fmt_00(b: &[u8]) -> Result<Appearance> {
        if b.len() != 5 {
            return Err(Error::Length(0x00, b.len()));
        }

        let appearance: u32 = util::from_bytes(&b[1..]);

        const WINGS: u32 = 1 << 6;
        const HORN: u32 = 1 << 7;
        const STALLION: u32 = 1 << 8;
        const LIGHT: u32 = 1 << 9;
        const MANE_SHIFT: usize = 10;
        const TAIL_SHIFT: usize = 13;
        const EQUIP0_SHIFT: usize = 18;
        const EQUIP1_SHIFT: usize = 22;
        const EQUIP2_SHIFT: usize = 26;

        static COLOR_TABLE: [u8; 6] = [0x00, 0x44, 0x88, 0xcc, 0xff, 0xff];

        let red = (appearance as usize >> 4) & 3;
        let green = (appearance as usize >> 2) & 3;
        let blue = (appearance as usize >> 0) & 3;
        let wings = appearance & WINGS != 0;
        let horn = appearance & HORN != 0;
        let stallion = appearance & STALLION != 0;
        let light = appearance & LIGHT != 0;
        let mane = (appearance >> MANE_SHIFT) & 7;
        let tail = (appearance >> TAIL_SHIFT) & 7;
        let _equip0 = (appearance >> EQUIP0_SHIFT) & 15;
        let _equip1 = (appearance >> EQUIP1_SHIFT) & 15;
        let _equip2 = (appearance >> EQUIP2_SHIFT) & 15;

        let nil = AppearanceFlags::empty();

        Ok(Appearance {
            flags:
                (if wings { APP_WING } else { nil }) |
                (if horn { APP_HORN } else { nil }) |
                (if stallion { APP_MALE } else { nil }) |
                (if light { APP_HORN } else { nil }) |
                nil,
            coat_color:
                (COLOR_TABLE[red + 2],
                 COLOR_TABLE[green + 2],
                 COLOR_TABLE[blue + 2]),
            mane_color:
                (COLOR_TABLE[red + 1],
                 COLOR_TABLE[green + 1],
                 COLOR_TABLE[blue + 1]),
            mane: mane as u8,
            tail: tail as u8,
        })
    }

    fn from_bytes_fmt_01(b: &[u8]) -> Result<Appearance> {
        if b.len() != 10 {
            return Err(Error::Length(0x01, b.len()));
        }

        let mut i = b.iter().cloned();
        let mut get = || i.next().unwrap();
        let _ = get();  // Skip format byte

        let flag_bits = get();
        let flags = unwrap_or!(AppearanceFlags::from_bits(flag_bits),
                               return Err(Error::FlagBits(flag_bits)));

        Ok(Appearance {
            flags: flags,
            coat_color: (get(), get(), get()),
            mane_color: (get(), get(), get()),
            mane: get(),
            tail: get(),
        })
    }
}



#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Error {
    MissingFormat,
    Format(u8),
    Length(u8, usize),
    FlagBits(u8),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::MissingFormat =>
                write!(f, "missing format byte (len == 0)"),
            Error::Format(v) =>
                write!(f, "unsupported format 0x{:02x}", v),
            Error::Length(v, l) =>
                write!(f, "invalid length {} for format 0x{:02x}", l, v),
            Error::FlagBits(b) =>
                write!(f, "invalid flag bits 0b{:08b}", b),
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::MissingFormat => "missing format byte (len == 0)",
            Error::Format(..) => "unsupported format",
            Error::Length(..) => "invalid length for format",
            Error::FlagBits(..) => "invalid flag bits",
        }
    }
}

pub type Result<T> = result::Result<T, Error>;
