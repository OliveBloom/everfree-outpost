#![crate_name = "server_extra"]
#![feature(
    proc_macro_hygiene,
)]

extern crate common;
extern crate server_types;
extern crate syntax_exts;

use std::collections::HashMap;
use std::collections::hash_map;
use std::slice;

use common::util::Bytes;
use server_types::*;
use syntax_exts::expand_objs_named;

macro_rules! expand_value_variants {
    ($($args:tt)*) => {
        expand_objs_named! {
            {
                bool, Ty = bool;
                int, Ty = i64;
                float, Ty = f64;
                str, Ty = String;
                bytes, Ty = Bytes;

                client_id, Ty = ClientId;
                entity_id, Ty = EntityId;
                inventory_id, Ty = InventoryId;
                plane_id, Ty = PlaneId;
                terrain_chunk_id, Ty = TerrainChunkId;
                structure_id, Ty = StructureId;

                stable_client_id, Ty = (Stable<ClientId>);
                stable_entity_id, Ty = (Stable<EntityId>);
                stable_inventory_id, Ty = (Stable<InventoryId>);
                stable_plane_id, Ty = (Stable<PlaneId>);
                stable_terrain_chunk_id, Ty = (Stable<TerrainChunkId>);
                stable_structure_id, Ty = (Stable<StructureId>);

                v2, Ty = V2;
                v3, Ty = V3;
                region2, Ty = (Region<V2>);
                region3, Ty = (Region<V3>);
            }
            $($args)*
        }
    };
}

expand_value_variants! {
    #[derive(Clone, Debug)]
    enum Repr {
        Null,
        Array(Vec<Repr>),
        Hash(HashMap<String, Repr>),

        $( $Obj($Ty), )*
    }
}

expand_value_variants! {
    #[allow(unused_variables)]
    pub trait Visitor {
        fn visit_array(&mut self, a: ArrayViewMut) {
            a.traverse_contents(self);
        }

        fn visit_hash(&mut self, h: HashViewMut) {
            h.traverse_contents(self);
        }

        $( fn $visit_obj(&mut self, x: &mut $Ty) {} )*
    }
}

impl Repr {
    fn traverse<V: Visitor + ?Sized>(&mut self, v: &mut V) {
        expand_value_variants! {
            match *self {
                Repr::Null => {},
                Repr::Array(ref mut a) => v.visit_array(ArrayViewMut::new(a)),
                Repr::Hash(ref mut h) => v.visit_hash(HashViewMut::new(h)),
                $( Repr::$Obj(ref mut x) => v.$visit_obj(x), )*
            }
        }
    }
}


expand_value_variants! {
    #[derive(Clone, Debug)]
    pub enum Value {
        Null,
        $( $Obj($Ty), )*
    }

    impl Value {
        fn from_repr(repr: Repr) -> Option<Value> {
            match repr {
                Repr::Null => Some(Value::Null),
                Repr::Array(_) => None,
                Repr::Hash(_) => None,
                $( Repr::$Obj(val) => Some(Value::$Obj(val)),)*
            }
        }

        fn to_repr(self) -> Repr {
            match self {
                Value::Null => Repr::Null,
                $( Value::$Obj(val) => Repr::$Obj(val), )*
            }
        }
    }
}

impl Value {
    pub fn as_bool(self) -> Option<bool> {
        match self {
            Value::Bool(x) => Some(x),
            _ => None,
        }
    }

    pub fn as_int(self) -> Option<i64> {
        match self {
            Value::Int(x) => Some(x),
            _ => None,
        }
    }

    pub fn as_float(self) -> Option<f64> {
        match self {
            Value::Float(x) => Some(x),
            _ => None,
        }
    }

    pub fn as_str(self) -> Option<String> {
        match self {
            Value::Str(x) => Some(x),
            _ => None,
        }
    }

    pub fn as_bytes(self) -> Option<Bytes> {
        match self {
            Value::Bytes(x) => Some(x),
            _ => None,
        }
    }

    // TODO: add the rest as needed
}


pub trait AnyValue: Sized {
    fn from_value(v: Value) -> Option<Self>;
    fn into_value(self) -> Value;
}

expand_value_variants! {
    $(
        impl AnyValue for $Ty {
            fn from_value(v: Value) -> Option<Self> {
                match v {
                    Value::$Obj(x) => Some(x),
                    _ => None,
                }
            }

            fn into_value(self) -> Value {
                Value::$Obj(self)
            }
        }
    )*
}


pub enum View<'a> {
    Value(Value),
    Array(ArrayView<'a>),
    Hash(HashView<'a>),
}

impl<'a> View<'a> {
    fn from_repr(repr: &'a Repr) -> View<'a> {
        match *repr {
            Repr::Array(ref v) => View::Array(ArrayView::new(v)),
            Repr::Hash(ref h) => View::Hash(HashView::new(h)),
            ref val => View::Value(Value::from_repr(val.clone()).unwrap()),
        }
    }


    pub fn as_value(self) -> Option<Value> {
        match self {
            View::Value(x) => Some(x),
            _ => None,
        }
    }

    pub fn unwrap_value(self) -> Value {
        match self {
            View::Value(x) => x,
            _ => panic!("not a Value"),
        }
    }


    pub fn as_array(self) -> Option<ArrayView<'a>> {
        match self {
            View::Array(x) => Some(x),
            _ => None,
        }
    }

    pub fn unwrap_array(self) -> ArrayView<'a> {
        match self {
            View::Array(x) => x,
            _ => panic!("not an Array"),
        }
    }


    pub fn as_hash(self) -> Option<HashView<'a>> {
        match self {
            View::Hash(x) => Some(x),
            _ => None,
        }
    }

    pub fn unwrap_hash(self) -> HashView<'a> {
        match self {
            View::Hash(x) => x,
            _ => panic!("not a Hash"),
        }
    }
}

pub enum ViewMut<'a> {
    Value(Value),
    Array(ArrayViewMut<'a>),
    Hash(HashViewMut<'a>),
}

impl<'a> ViewMut<'a> {
    fn from_repr(repr: &'a mut Repr) -> ViewMut<'a> {
        match *repr {
            Repr::Array(ref mut v) => ViewMut::Array(ArrayViewMut::new(v)),
            Repr::Hash(ref mut h) => ViewMut::Hash(HashViewMut::new(h)),
            ref val => ViewMut::Value(Value::from_repr(val.clone()).unwrap()),
        }
    }


    pub fn as_value(self) -> Option<Value> {
        match self {
            ViewMut::Value(x) => Some(x),
            _ => None,
        }
    }

    pub fn unwrap_value(self) -> Value {
        match self {
            ViewMut::Value(x) => x,
            _ => panic!("not a Value"),
        }
    }


    pub fn as_array(self) -> Option<ArrayViewMut<'a>> {
        match self {
            ViewMut::Array(x) => Some(x),
            _ => None,
        }
    }

    pub fn unwrap_array(self) -> ArrayViewMut<'a> {
        match self {
            ViewMut::Array(x) => x,
            _ => panic!("not an Array"),
        }
    }


    pub fn as_hash(self) -> Option<HashViewMut<'a>> {
        match self {
            ViewMut::Hash(x) => Some(x),
            _ => None,
        }
    }

    pub fn unwrap_hash(self) -> HashViewMut<'a> {
        match self {
            ViewMut::Hash(x) => x,
            _ => panic!("not a Hash"),
        }
    }
}


#[derive(Clone, Copy, Debug)]
pub struct ArrayView<'a> {
    v: &'a Vec<Repr>,
}

impl<'a> ArrayView<'a> {
    fn new(v: &'a Vec<Repr>) -> ArrayView<'a> {
        ArrayView { v: v }
    }

    pub fn get(self, idx: usize) -> View<'a> {
        View::from_repr(&self.v[idx])
    }

    pub fn len(self) -> usize {
        self.v.len()
    }

    pub fn iter(self) -> ArrayViewIter<'a> {
        ArrayViewIter { inner: self.v.iter() }
    }
}

#[derive(Debug)]
pub struct ArrayViewMut<'a> {
    v: &'a mut Vec<Repr>,
}

impl<'a> ArrayViewMut<'a> {
    fn new(v: &'a mut Vec<Repr>) -> ArrayViewMut<'a> {
        ArrayViewMut { v: v }
    }

    pub fn borrow<'b>(&'b mut self) -> ArrayViewMut<'b> {
        ArrayViewMut { v: self.v }
    }

    pub fn get(self, idx: usize) -> View<'a> {
        View::from_repr(&self.v[idx])
    }

    pub fn get_mut(self, idx: usize) -> ViewMut<'a> {
        ViewMut::from_repr(&mut self.v[idx])
    }

    pub fn len(self) -> usize {
        self.v.len()
    }

    pub fn set(self, idx: usize, value: Value) {
        self.v[idx] = value.to_repr();
    }

    pub fn set_array(self, idx: usize) -> ArrayViewMut<'a> {
        let top = self.v;
        top[idx] = Repr::Array(Vec::new());
        if let Repr::Array(ref mut v) = top[idx] {
            ArrayViewMut::new(v)
        } else {
            unreachable!("result of inserting Array should be Array");
        }
    }

    pub fn set_hash(self, idx: usize) -> HashViewMut<'a> {
        let top = self.v;
        top[idx] = Repr::Hash(HashMap::new());
        if let Repr::Hash(ref mut h) = top[idx] {
            HashViewMut::new(h)
        } else {
            unreachable!("result of inserting Hash should be Hash");
        }
    }

    pub fn get_or_set_array(self, idx: usize) -> ArrayViewMut<'a> {
        let is_array = if let Repr::Array(_) = self.v[idx] { true } else { false };
        if is_array {
            self.get_mut(idx).unwrap_array()
        } else {
            self.set_array(idx)
        }

        /* TODO: the obvious implementation seems to trigger a borrow checker bug (?)
        if let Some(&mut Repr::Array(ref mut v)) = top.get_mut(idx) {
            return ArrayViewMut::new(v);
        }
        // Otherwise...
        ArrayViewMut::new(top).set_array(idx)
        */
    }

    pub fn get_or_set_hash(self, idx: usize) -> HashViewMut<'a> {
        let is_hash = if let Repr::Hash(_) = self.v[idx] { true } else { false };
        if is_hash {
            self.get_mut(idx).unwrap_hash()
        } else {
            self.set_hash(idx)
        }
    }

    pub fn push(self, value: Value) {
        self.v.push(value.to_repr());
    }

    pub fn push_hash(self) -> HashViewMut<'a> {
        self.v.push(Repr::Hash(HashMap::new()));
        if let Some(&mut Repr::Hash(ref mut h)) = self.v.last_mut() {
            HashViewMut::new(h)
        } else {
            unreachable!("result of pushing Hash should be Hash");
        }
    }

    pub fn push_array(self) -> ArrayViewMut<'a> {
        self.v.push(Repr::Array(Vec::new()));
        if let Some(&mut Repr::Array(ref mut h)) = self.v.last_mut() {
            ArrayViewMut::new(h)
        } else {
            unreachable!("result of pushing Array should be Array");
        }
    }

    pub fn pop(self) {
        self.v.pop();
    }

    pub fn swap_remove(self, idx: usize) {
        self.v.swap_remove(idx);
    }

    pub fn iter(self) -> ArrayViewIter<'a> {
        ArrayViewIter { inner: self.v.iter() }
    }

    pub fn iter_mut(self) -> ArrayViewIterMut<'a> {
        ArrayViewIterMut { inner: self.v.iter_mut() }
    }

    pub fn traverse<V: Visitor + ?Sized>(self, v: &mut V) {
        v.visit_array(self);
    }

    pub fn traverse_contents<V: Visitor + ?Sized>(self, v: &mut V) {
        for x in self.v {
            x.traverse(v);
        }
    }
}

pub struct ArrayViewIter<'a> {
    inner: slice::Iter<'a, Repr>,
}

impl<'a> Iterator for ArrayViewIter<'a> {
    type Item = View<'a>;

    fn next(&mut self) -> Option<View<'a>> {
        match self.inner.next() {
            Some(x) => Some(View::from_repr(x)),
            None => None,
        }
    }
}

pub struct ArrayViewIterMut<'a> {
    inner: slice::IterMut<'a, Repr>,
}

impl<'a> Iterator for ArrayViewIterMut<'a> {
    type Item = ViewMut<'a>;

    fn next(&mut self) -> Option<ViewMut<'a>> {
        match self.inner.next() {
            Some(x) => Some(ViewMut::from_repr(x)),
            None => None,
        }
    }
}


#[derive(Clone, Copy, Debug)]
pub struct HashView<'a> {
    h: &'a HashMap<String, Repr>,
}

impl<'a> HashView<'a> {
    fn new(h: &'a HashMap<String, Repr>) -> HashView<'a> {
        HashView { h: h }
    }

    pub fn get(self, key: &str) -> Option<View<'a>> {
        self.h.get(key).map(View::from_repr)
    }

    pub fn contains(self, key: &str) -> bool {
        self.h.contains_key(key)
    }

    pub fn len(self) -> usize {
        self.h.len()
    }

    pub fn iter(self) -> HashViewIter<'a> {
        HashViewIter { inner: self.h.iter() }
    }
}

#[derive(Debug)]
pub struct HashViewMut<'a> {
    h: &'a mut HashMap<String, Repr>,
}

impl<'a> HashViewMut<'a> {
    fn new(h: &'a mut HashMap<String, Repr>) -> HashViewMut<'a> {
        HashViewMut { h: h }
    }

    pub fn borrow<'b>(&'b mut self) -> HashViewMut<'b> {
        HashViewMut { h: self.h }
    }

    pub fn get(self, key: &str) -> Option<View<'a>> {
        self.h.get(key).map(View::from_repr)
    }

    pub fn get_mut(self, key: &str) -> Option<ViewMut<'a>> {
        self.h.get_mut(key).map(ViewMut::from_repr)
    }

    pub fn contains(self, key: &str) -> bool {
        self.h.contains_key(key)
    }

    pub fn len(self) -> usize {
        self.h.len()
    }

    pub fn set(self, key: &str, value: Value) {
        self.h.insert(key.to_owned(), value.to_repr());
    }

    pub fn set_array(self, key: &str) -> ArrayViewMut<'a> {
        self.h.insert(key.to_owned(), Repr::Array(Vec::new()));
        if let Repr::Array(ref mut v) = *self.h.get_mut(key).unwrap() {
            ArrayViewMut::new(v)
        } else {
            unreachable!("result of inserting Array should be Array");
        }
    }

    pub fn set_hash(self, key: &str) -> HashViewMut<'a> {
        self.h.insert(key.to_owned(), Repr::Hash(HashMap::new()));
        if let Repr::Hash(ref mut h) = *self.h.get_mut(key).unwrap() {
            HashViewMut::new(h)
        } else {
            unreachable!("result of inserting Hash should be Hash");
        }
    }

    pub fn get_or_set_array(self, key: &str) -> ArrayViewMut<'a> {
        let is_array = if let Some(&Repr::Array(_)) = self.h.get(key) { true } else { false };
        if is_array {
            self.get_mut(key).unwrap().unwrap_array()
        } else {
            self.set_array(key)
        }
    }

    pub fn get_or_set_hash(self, key: &str) -> HashViewMut<'a> {
        let is_hash = if let Some(&Repr::Hash(_)) = self.h.get(key) { true } else { false };
        if is_hash {
            self.get_mut(key).unwrap().unwrap_hash()
        } else {
            self.set_hash(key)
        }
    }

    pub fn remove(self, key: &str) {
        self.h.remove(key);
    }

    pub fn iter(self) -> HashViewIter<'a> {
        HashViewIter { inner: self.h.iter() }
    }

    pub fn iter_mut(self) -> HashViewIterMut<'a> {
        HashViewIterMut { inner: self.h.iter_mut() }
    }

    pub fn traverse<V: Visitor + ?Sized>(self, v: &mut V) {
        v.visit_hash(self);
    }

    pub fn traverse_contents<V: Visitor + ?Sized>(self, v: &mut V) {
        for (_, x) in self.h {
            x.traverse(v);
        }
    }
}

pub struct HashViewIter<'a> {
    inner: hash_map::Iter<'a, String, Repr>,
}

impl<'a> Iterator for HashViewIter<'a> {
    type Item = (&'a str, View<'a>);

    fn next(&mut self) -> Option<(&'a str, View<'a>)> {
        match self.inner.next() {
            Some((k, v)) => Some((k, View::from_repr(v))),
            None => None,
        }
    }
}

pub struct HashViewIterMut<'a> {
    inner: hash_map::IterMut<'a, String, Repr>,
}

impl<'a> Iterator for HashViewIterMut<'a> {
    type Item = (&'a str, ViewMut<'a>);

    fn next(&mut self) -> Option<(&'a str, ViewMut<'a>)> {
        match self.inner.next() {
            Some((k, v)) => Some((k, ViewMut::from_repr(v))),
            None => None,
        }
    }
}


#[derive(Clone, Debug)]
pub struct Extra {
    h: Option<Box<HashMap<String, Repr>>>,
}

impl Extra {
    pub fn new() -> Extra {
        Extra {
            h: None,
        }
    }

    pub fn get(&self, key: &str) -> Option<View> {
        if let Some(ref h) = self.h {
            HashView::new(h).get(key)
        } else {
            None
        }
    }

    pub fn get_mut(&mut self, key: &str) -> Option<ViewMut> {
        if let Some(ref mut h) = self.h {
            HashViewMut::new(h).get_mut(key)
        } else {
            None
        }
    }

    pub fn contains(&self, key: &str) -> bool {
        if let Some(ref h) = self.h {
            HashView::new(h).contains(key)
        } else {
            false
        }
    }

    pub fn len(&self) -> usize {
        if let Some(ref h) = self.h {
            h.len()
        } else {
            0
        }
    }

    fn view_mut(&mut self) -> HashViewMut {
        if self.h.is_none() {
            self.h = Some(Box::new(HashMap::new()));
        }
        if let Some(ref mut h) = self.h {
            HashViewMut::new(h)
        } else {
            unreachable!("value after setting to Some should be Some");
        }
    }

    pub fn set(&mut self, key: &str, value: Value) {
        self.view_mut().set(key, value)
    }

    pub fn set_array(&mut self, key: &str) -> ArrayViewMut {
        self.view_mut().set_array(key)
    }

    pub fn set_hash(&mut self, key: &str) -> HashViewMut {
        self.view_mut().set_hash(key)
    }

    pub fn get_or_set_array(&mut self, key: &str) -> ArrayViewMut {
        self.view_mut().get_or_set_array(key)
    }

    pub fn get_or_set_hash(&mut self, key: &str) -> HashViewMut {
        self.view_mut().get_or_set_hash(key)
    }

    pub fn remove(&mut self, key: &str) {
        let clear = 
            if let Some(ref mut h) = self.h {
                HashViewMut::new(h).remove(key);
                h.len() == 0
            } else {
                // It's already empty.
                return;
            };
        if clear {
            self.h = None;
        }
    }

    pub fn iter(&self) -> ExtraIter {
        if let Some(ref h) = self.h {
            ExtraIter { inner: Some(HashView::new(h).iter()) }
        } else {
            ExtraIter { inner: None }
        }
    }

    pub fn iter_mut(&mut self) -> ExtraIterMut {
        if let Some(ref mut h) = self.h {
            ExtraIterMut { inner: Some(HashViewMut::new(h).iter_mut()) }
        } else {
            ExtraIterMut { inner: None }
        }
    }

    pub fn traverse<V: Visitor + ?Sized>(&mut self, v: &mut V) {
        if let Some (ref mut h) = self.h {
            v.visit_hash(HashViewMut::new(h));
        }
    }

    pub fn traverse_children<V: Visitor + ?Sized>(&mut self, v: &mut V) {
        if let Some(ref mut h) = self.h {
            for (_, x) in h.iter_mut() {
                x.traverse(v);
            }
        }
    }
}

pub struct ExtraIter<'a> {
    inner: Option<HashViewIter<'a>>,
}

impl<'a> Iterator for ExtraIter<'a> {
    type Item = (&'a str, View<'a>);

    fn next(&mut self) -> Option<(&'a str, View<'a>)> {
        if let Some(ref mut inner) = self.inner {
            inner.next()
        } else {
            None
        }
    }
}

pub struct ExtraIterMut<'a> {
    inner: Option<HashViewIterMut<'a>>,
}

impl<'a> Iterator for ExtraIterMut<'a> {
    type Item = (&'a str, ViewMut<'a>);

    fn next(&mut self) -> Option<(&'a str, ViewMut<'a>)> {
        if let Some(ref mut inner) = self.inner {
            inner.next()
        } else {
            None
        }
    }
}
