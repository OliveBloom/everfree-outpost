extern crate env_logger;
#[macro_use] extern crate log;
extern crate outpost_gl;
extern crate png;
extern crate sdl2;
extern crate time;

extern crate client;
extern crate common;
extern crate physics;
extern crate png_file;

use std::fs::File;
use std::io::{self, Read, Write};
use std::sync::mpsc::{Sender, Receiver, channel, TryRecvError};
use std::thread;

use sdl2::event::{Event, WindowEvent};
use sdl2::mouse::MouseButton;
use sdl2::keyboard::{Keycode, Mod};

use client::{Client, Data};
use common::proto::game::{Request, Response};
use common::proto::wire::{ReadFrom, WriteTo, Size};
use common::types::V2;

use self::platform::Platform;

mod platform;


fn read_data() -> Data {
    let mut v = Vec::new();
    File::open("data/client_data.bin").unwrap()
        .read_to_end(&mut v).unwrap();
    let raw = v.into_boxed_slice();
    Data::new(raw)
}

fn init_io() -> (Sender<Request>, Receiver<Response>) {
    let (req_send, req_recv) = channel();
    let (resp_send, resp_recv) = channel();

    thread::spawn(move || {
        let stdin = io::stdin();
        let mut stdin = stdin.lock();
        loop {
            let size = u16::read_from(&mut stdin).unwrap();
            let resp = Response::read_from(&mut stdin).unwrap();
            trace!("received {:?}", resp);
            assert!(resp.size() == size as usize);
            resp_send.send(resp).unwrap();
        }
    });

    thread::spawn(move || {
        let stdout = io::stdout();
        let mut stdout = stdout.lock();
        loop {
            let req: Request = req_recv.recv().unwrap();
            trace!("sending {:?}", req);
            (req.size() as u16).write_to(&mut stdout).unwrap();
            req.write_to(&mut stdout).unwrap();
            stdout.flush().unwrap();
        }
    });

    (req_send, resp_recv)
}

fn map_keycode(code: Keycode) -> Option<u8> {
    /*
    let key = match code {
        Keycode::Left => Key::MoveLeft,
        Keycode::Right => Key::MoveRight,
        Keycode::Up => Key::MoveUp,
        Keycode::Down => Key::MoveDown,
        Keycode::LShift |
        Keycode::RShift => Key::Run,
        Keycode::A => Key::Interact,
        Keycode::S => Key::UseAbility,
        Keycode::D => Key::UseItem,
        Keycode::W => Key::OpenAbilities,
        Keycode::E => Key::OpenInventory,
        Keycode::F3 => Key::ToggleDebugPanel,
        Keycode::C => Key::ToggleCursor,
        Keycode::Return => Key::Select,
        Keycode::Escape => Key::Cancel,
        Keycode::Num1 => Key::Hotbar(0),
        Keycode::Num2 => Key::Hotbar(1),
        Keycode::Num3 => Key::Hotbar(2),
        Keycode::Num4 => Key::Hotbar(3),
        Keycode::Num5 => Key::Hotbar(4),
        Keycode::Num6 => Key::Hotbar(5),
        Keycode::Num7 => Key::Hotbar(6),
        Keycode::Num8 => Key::Hotbar(7),
        Keycode::Num9 => Key::Hotbar(8),
        Keycode::LCtrl => Key::DebugLogSwitch,
        _ => return None,
    };
    */
    let key = match code {
        Keycode::A | Keycode::Left => 0,
        Keycode::D | Keycode::Right => 1,
        Keycode::W | Keycode::Up => 2,
        Keycode::S | Keycode::Down => 3,
        Keycode::LShift |
        Keycode::RShift => 4,
        Keycode::Z => 10,
        Keycode::X => 11,
        Keycode::C => 12,
        Keycode::F => 13,
        Keycode::R => 14,
        Keycode::F3 => 20,
        Keycode::V => 21,
        Keycode::Return => 30,
        Keycode::Escape => 31,
        Keycode::Space => 31,
        Keycode::Num1 | Keycode::Kp1 => 41,
        Keycode::Num2 | Keycode::Kp2 => 42,
        Keycode::Num3 | Keycode::Kp3 => 43,
        Keycode::Num4 | Keycode::Kp4 => 44,
        Keycode::Num5 | Keycode::Kp5 => 45,
        Keycode::Num6 | Keycode::Kp6 => 46,
        Keycode::Num7 | Keycode::Kp7 => 47,
        Keycode::Num8 | Keycode::Kp8 => 48,
        Keycode::Num9 | Keycode::Kp9 => 49,
        Keycode::LCtrl => 99,
        _ => return None,
    };
    Some(key)
}

fn map_keymod(keymod: Mod) -> u8 {
    use sdl2::keyboard::*;
    if keymod.contains(LSHIFTMOD) || keymod.contains(RSHIFTMOD) {
        1
    } else {
        0
    }
}

fn map_button(btn: MouseButton) -> Option<u8> {
    let code = match btn {
        MouseButton::Left => 1,
        MouseButton::Middle => 2,
        MouseButton::Right => 3,
        _ => return None,
    };
    Some(code)
}

pub fn main() {
    env_logger::init().unwrap();


    // SDL init
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let gl_attr = video_subsystem.gl_attr();
    gl_attr.set_double_buffer(true);
    gl_attr.set_depth_size(16);

    let window = video_subsystem.window("Everfree Outpost", 1200, 900)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let ctx = window.gl_create_context().unwrap();
    window.gl_make_current(&ctx).unwrap();

    outpost_gl::load_with(|name| video_subsystem.gl_get_proc_address(name) as *const _);


    // I/O init
    let (to_server, from_server) = init_io();


    // libclient init
    client::data::set_data(Box::new(read_data()));
    to_server.send(Request::Ready(())).unwrap();
    let platform = Platform::new(to_server);
    let mut client = Client::new(platform);
    client.resize_window((800, 600));

    info!("client initialized");


    // Main loop

    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut visible = true;

    let mut last_mouse_pos = V2::new(0, 0);

    'running: loop {
        loop {
            match from_server.try_recv() {
                Ok(msg) => client.handle_message(msg),
                Err(TryRecvError::Empty) => break,
                Err(TryRecvError::Disconnected) => break 'running,
            }
        }

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'running
                },
                Event::KeyDown { keycode: Some(code), keymod, .. } => {
                    trace!("keydown: {:?}", code);
                    if let Some(key) = map_keycode(code) {
                        client.input_key_down(key, map_keymod(keymod));
                    }
                },
                Event::KeyUp { keycode: Some(code), keymod, .. } => {
                    trace!("keyup: {:?}", code);
                    if let Some(key) = map_keycode(code) {
                        client.input_key_up(key, map_keymod(keymod));
                    }
                },

                Event::MouseMotion { x, y, .. } => {
                    let pos = V2::new(x, y);
                    client.input_mouse_move(pos);
                    last_mouse_pos = pos;
                },
                Event::MouseButtonDown { mouse_btn, x, y, .. } => {
                    let pos = V2::new(x, y);
                    last_mouse_pos = pos;
                    if let Some(btn) = map_button(mouse_btn) {
                        let mods = sdl_context.keyboard().mod_state();
                        client.input_mouse_down(pos, btn, map_keymod(mods));
                    }
                },
                Event::MouseButtonUp { mouse_btn, x, y, .. } => {
                    let pos = V2::new(x, y);
                    last_mouse_pos = pos;
                    if let Some(btn) = map_button(mouse_btn) {
                        let mods = sdl_context.keyboard().mod_state();
                        client.input_mouse_up(pos, btn, map_keymod(mods));
                    }
                },
                Event::MouseWheel { y, .. } => {
                    if y > 0 {
                        client.input_mouse_down(last_mouse_pos, 4, 0);
                        client.input_mouse_up(last_mouse_pos, 4, 0);
                    } else {
                        client.input_mouse_down(last_mouse_pos, 5, 0);
                        client.input_mouse_up(last_mouse_pos, 5, 0);
                    }
                },

                Event::Window { win_event, .. } => {
                    match win_event {
                        WindowEvent::SizeChanged(w, h) => {
                            client.resize_window((w as u16, h as u16));
                        },
                        WindowEvent::Hidden => {
                            visible = false;
                        },
                        WindowEvent::Shown => {
                            visible = true;
                        },
                        _ => {},
                    }
                }
                _ => {}
            }
        }

        if visible {
            client.render_frame();
            window.gl_swap_window();
        }
    }
}
