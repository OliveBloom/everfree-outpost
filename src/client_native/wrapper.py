import asyncio
import base64
from collections import deque
import json
import os
import struct
import subprocess
import sys
import urllib.parse

import tornado.httpclient
import tornado.platform.asyncio
from tornado.platform.asyncio import to_tornado_future, to_asyncio_future
import tornado.websocket

import crypto



which = os.environ.get('OUTPOST_HOST')
if which == 'main':
    AUTH_BASE = 'https://auth.everfree-outpost.com/'
    AUTH_HOST = 'auth.everfree-outpost.com'
    AUTH_ORIGIN = 'http://play.everfree-outpost.com'
    GAME_URL = 'ws://game.everfree-outpost.com:8888/ws'
elif which == 'local' or which is None:
    AUTH_BASE = 'http://localhost:5000/'
    AUTH_HOST = 'localhost:5000'
    AUTH_ORIGIN = 'http://localhost:8889'
    GAME_URL = 'ws://localhost:8888/ws'
else:
    assert False, 'unknown OUTPOST_HOST: %r' % which
del which


async def do_login(user, password):
    auth_client = tornado.httpclient.AsyncHTTPClient()

    args = {'name': user, 'password': password}
    headers = {
            'Host': AUTH_HOST,
            }
    resp = await auth_client.fetch(AUTH_BASE + 'login',
            method='POST',
            body=urllib.parse.urlencode(args),
            headers=headers,
            follow_redirects=False,
            raise_error=False)
    assert resp.code == 302

    cookie = resp.headers['Set-Cookie'].partition(';')[0]
    return cookie


async def get_response(challenge, cookie):
    auth_client = tornado.httpclient.AsyncHTTPClient()

    headers = {
            'Origin': AUTH_ORIGIN,
            'Host': AUTH_HOST,
            'Cookie': cookie,
            }
    body = json.dumps({
        'challenge': base64.urlsafe_b64encode(challenge).decode('ascii'),
        })
    resp = await auth_client.fetch(AUTH_BASE + 'api/sign_challenge',
            method='POST',
            body=body,
            headers=headers)

    j = json.loads(resp.body.decode('ascii'))
    assert j['status'] == 'ok'
    return base64.urlsafe_b64decode(j['response'].encode('ascii'))


OP_AUTH_RESPONSE =      0x0014
OP_AUTH_CHALLENGE =     0x8021
OP_AUTH_RESULT =        0x8022

MODE_SSO =      0
MODE_LOCAL =    1

async def do_auth(conn, cookie):
    response = await to_asyncio_future(get_response(conn.cb_token, cookie))
    await conn.send(struct.pack('<H', OP_AUTH_RESPONSE) + response)

    while True:
        msg = await to_asyncio_future(conn.recv())
        if msg is None:
            assert False, 'server disconnected unexpectedly'
        opcode, = struct.unpack('<H', msg[:2])

        if opcode == OP_AUTH_RESULT:
            flags, = struct.unpack('<H', msg[2:4])
            if flags == 1:
                name = msg[4:].decode('ascii')
                return name
            else:
                assert False, 'bad auth result: %s' % flags

        else:
            assert False, 'bad opcode: %x' % opcode

async def sender(conn, stdout):
    try:
        while True:
            msg_len, = struct.unpack('H', await stdout.read(2))
            data = await stdout.read(msg_len)
            assert len(data) == msg_len, 'EOF while reading message'
            await conn.send(data)
    finally:
        asyncio.get_event_loop().stop()

async def receiver(conn, stdout):
    try:
        while True:
            msg = await to_asyncio_future(conn.recv())
            if msg is None:
                assert False, 'server disconnected'
            stdout.write(struct.pack('H', len(msg)))
            stdout.write(msg)
    finally:
        asyncio.get_event_loop().stop()


class EncryptedSocket:
    def __init__(self):
        self.conn = None
        self.proto = None
        self.recv_queue = deque()
        self.cb_token = None

    async def connect(self, url):
        self.conn = await to_asyncio_future(tornado.websocket.websocket_connect(url))
        self.proto = crypto.Protocol(initiator=True)

        # Perform handshake
        self.proto.open()
        while True:
            evt = self.proto.next_event()
            if evt is None:
                msg = await to_asyncio_future(self.conn.read_message())
                self.proto.incoming(msg)
            elif isinstance(evt, crypto.RecvEvent):
                self.recv_queue.append(evt.data)
            elif isinstance(evt, crypto.OutgoingEvent):
                self.conn.write_message(evt.data, binary=True)
            elif isinstance(evt, crypto.HandshakeFinishedEvent):
                self.cb_token = evt.cb_token
                return
            elif isinstance(evt, crypto.ErrorEvent):
                raise ValueError('crypto error: %s' % evt.message)
            else:
                assert False, 'unknown event type in connect: %r' % (evt,)

    async def recv(self):
        if len(self.recv_queue) > 0:
            return self.recv_queue.popleft()

        while True:
            evt = self.proto.next_event()
            if evt is None:
                msg = await to_asyncio_future(self.conn.read_message())
                self.proto.incoming(msg)
            elif isinstance(evt, crypto.RecvEvent):
                return evt.data
            elif isinstance(evt, crypto.OutgoingEvent):
                self.conn.write_message(evt.data, binary=True)
            elif isinstance(evt, crypto.ErrorEvent):
                raise ValueError('crypto error: %s' % evt.message)
            else:
                assert False, 'unexpected event type in recv: %r' % (evt,)

    async def send(self, data):
        self.proto.send(data)

        while True:
            evt = self.proto.next_event()
            if evt is None:
                return
            elif isinstance(evt, crypto.RecvEvent):
                self.recv_queue.append(evt.data)
            elif isinstance(evt, crypto.OutgoingEvent):
                self.conn.write_message(evt.data, binary=True)
            elif isinstance(evt, crypto.ErrorEvent):
                raise ValueError('crypto error: %s' % evt.message)
            else:
                assert False, 'unexpected event type in send: %r' % (evt,)


async def amain(user, password):
    print('logging in as %r...' % user)
    cookie = await to_asyncio_future(do_login(user, password))
    print(cookie)

    print('connecting...')
    conn = EncryptedSocket()
    await conn.connect(GAME_URL)
    print(conn.cb_token)

    print('authenticating...')
    name = await do_auth(conn, cookie)
    print('connected as %r' % name)

    env = os.environ.copy()
    env['RUST_BACKTRACE'] = '1'
    if 'RUST_LOG' not in env:
        env['RUST_LOG'] = 'info'
    cmd = ('bin/client_native',)
    client = await asyncio.create_subprocess_exec(*cmd,
            stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=None, env=env)

    asyncio.get_event_loop().create_task(sender(conn, client.stdout))
    asyncio.get_event_loop().create_task(receiver(conn, client.stdin))
    
    ret = await client.wait()
    asyncio.get_event_loop().stop()


def main():
    user = os.environ.get('OUTPOST_USER') or input('username: ')
    password = os.environ.get('OUTPOST_PASSWORD') or input('password: ')

    tornado.platform.asyncio.AsyncIOMainLoop().install()
    asyncio.get_event_loop().create_task(amain(user, password))
    asyncio.get_event_loop().run_forever()

if __name__ == '__main__':
    os.chdir(os.path.join(os.path.dirname(__file__), '..'))
    main()
