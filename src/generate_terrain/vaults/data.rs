use std::mem;
use std::ops::Deref;
use std::slice;
use std::str;
use common::data::{self, ChdParams, chd_lookup};
use common::types::{V3, V2};
use common::util::BitSlice;
use common::util::ByteCast;


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Vault {
    name_off: u32,
    name_len: u32,
    size: (u16, u16),
    structures_off: u32,
    structures_len: u32,
    tree_mask_off: u32,
    tree_mask_len: u32,
    junk_mask_off: u32,
    junk_mask_len: u32,
}
unsafe impl ByteCast for Vault {}

impl Vault {
    pub fn size(&self) -> V2 {
        V2::new(self.size.0 as i32, self.size.1 as i32)
    }
}

#[derive(Clone, Copy)]
pub struct VaultRef<'a> {
    obj: &'a Vault,
    base: &'a Data,
}

impl<'a> Deref for VaultRef<'a> {
    type Target = Vault;
    fn deref(&self) -> &Vault {
        self.obj
    }
}

impl<'a> VaultRef<'a> {
    pub fn name(&self) -> &'a str {
        self.base.string_slice(self.obj.name_off, self.obj.name_len)
    }

    pub fn raw_structures(&self) -> &'a [Structure] {
        let off = self.structures_off as usize;
        let len = self.structures_len as usize;
        &self.base.structures()[off .. off + len]
    }

    pub fn structures(&self) -> StructureIter {
        StructureIter {
            inner: self.raw_structures().iter(),
            base: self.base,
        }
    }

    pub fn tree_mask(&self) -> &BitSlice {
        let off = self.tree_mask_off as usize;
        let len = self.tree_mask_len as usize;
        let raw = &self.base.bit_masks()[off .. off + len];
        BitSlice::from_bytes(raw)
    }

    pub fn junk_mask(&self) -> &BitSlice {
        let off = self.junk_mask_off as usize;
        let len = self.junk_mask_len as usize;
        let raw = &self.base.bit_masks()[off .. off + len];
        BitSlice::from_bytes(raw)
    }
}

pub struct StructureIter<'a> {
    inner: slice::Iter<'a, Structure>,
    base: &'a Data,
}

impl<'a> Iterator for StructureIter<'a> {
    type Item = StructureRef<'a>;
    fn next(&mut self) -> Option<StructureRef<'a>> {
        self.inner.next().map(|obj| StructureRef { obj: obj, base: self.base })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.inner.size_hint()
    }
}

impl<'a> ExactSizeIterator for StructureIter<'a> {
    fn len(&self) -> usize {
        self.inner.len()
    }
}


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Structure {
    name_off: u32,
    name_len: u32,
    pos: (u16, u16, u16),
}
unsafe impl ByteCast for Structure {}

impl Structure {
    pub fn pos(&self) -> V3 {
        V3::new(self.pos.0 as i32,
                self.pos.1 as i32,
                self.pos.2 as i32)
    }
}

#[derive(Clone, Copy)]
pub struct StructureRef<'a> {
    obj: &'a Structure,
    base: &'a Data,
}

impl<'a> Deref for StructureRef<'a> {
    type Target = Structure;
    fn deref(&self) -> &Structure {
        self.obj
    }
}

impl<'a> StructureRef<'a> {
    pub fn name(&self) -> &'a str {
        self.base.string_slice(self.obj.name_off, self.obj.name_len)
    }
}


data::gen_data! {
    version = (2, 1);

    strings (b"Strings\0"): str,
    bit_masks (b"BitMasks"): [u8],

    structures (b"Structur"): [Structure],

    vaults (b"Vaults\0\0"): [Vault],
    vault_params (b"IxPrValt"): ChdParams<u16>,
    vault_table (b"IxTbValt"): [u16],
}

impl Data {
    fn string_slice(&self, off: u32, len: u32) -> &str {
        let off = off as usize;
        let len = len as usize;
        &self.strings()[off .. off + len]
    }


    pub fn get_vault(&self, id: u16) -> Option<VaultRef> {
        self.vaults().get(id as usize)
            .map(|obj| VaultRef { obj: obj, base: self })
    }

    pub fn vault(&self, id: u16) -> VaultRef {
        self.get_vault(id)
            .unwrap_or_else(|| panic!("unknown vault id: {}", id))
    }

    pub fn get_vault_id(&self, name: &str) -> Option<u16> {
        if let Some(id) = chd_lookup(name, self.vault_table(), self.vault_params()) {
            if self.get_vault(id).map_or(false, |b| b.name() == name) {
                return Some(id);
            }
        }
        None
    }

    pub fn vault_id(&self, name: &str) -> u16 {
        self.get_vault_id(name)
            .unwrap_or_else(|| panic!("unknown vault name: {:?}", name))
    }
}
