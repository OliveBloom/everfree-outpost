#![crate_name = "generate_terrain"]

extern crate env_logger;
extern crate linked_hash_map;
#[macro_use] extern crate log;
extern crate rand;

extern crate common;
extern crate physics;
extern crate server_bundle;
extern crate server_config;
extern crate server_extra;
extern crate server_types;
extern crate server_world_types;
extern crate terrain_gen_algo;

use server_bundle::builder::Builder;
use server_bundle::types::Bundle;
use server_bundle::flat::Flat;
use server_config::{Data, Storage};
use server_types::*;


use std::cmp;
use std::env;
use std::hash::{Hash, Hasher};
#[allow(deprecated)] use std::hash::SipHasher;
use std::io::{self, Read, Write};
use std::path::Path;
use std::u32;
use rand::{XorShiftRng, Rng, SeedableRng};

use common::util::bytes::{ReadBytes, WriteBytes};
use terrain_gen_algo::cellular::CellularGrid;

mod points;
mod vaults;

mod forest;


#[allow(dead_code)]
struct Context<'d> {
    data: &'d Data,
    seed: (u64, u64),
}

#[allow(dead_code)]
impl<'d> Context<'d> {
    #[allow(deprecated)]    // for SipHasher
    fn get_rng<H: Hash>(&self, x: H) -> XorShiftRng {
        let mut h = SipHasher::new_with_keys(self.seed.0, self.seed.1);
        x.hash(&mut h);
        let k = h.finish();
        XorShiftRng::from_seed([k as u32, 0x12345, (k >> 32) as u32, 0xfedcb])
    }

    fn generate(&mut self, pid: Stable<PlaneId>, cpos: V2) -> Bundle {
        let mut rng = self.get_rng((pid, cpos));
        let mut b = Builder::new(self.data);

        {
            let mut tc = b.terrain_chunk();
            tc.stable_plane(pid);
            tc.cpos(cpos);
            for pos in Region3::sized(CHUNK_SIZE).points() {
                if pos.z == 0 {
                    /*
                    let dirt_weight = p.y - 8;
                    let snow_weight = p.x - 8;
                    let sand_weight = 0 - p.x;
                    let grass_weight = 2;
                    */
                    let dirt_weight = 3;
                    let snow_weight = 0;
                    let sand_weight = 0;
                    let grass_weight = 10;
                    let choice = terrain_gen_algo::reservoir_sample_weighted(
                        &mut rng,
                        [("grass", cmp::max(0, grass_weight)),
                         ("dirt", cmp::max(0, dirt_weight)),
                         ("snow", cmp::max(0, snow_weight)),
                         ("sand", cmp::max(0, sand_weight))].iter().map(|&x| x))
                        .unwrap();

                    tc.block(pos, choice);
                } else {
                    tc.block(pos, "empty");
                }
            }

            for _ in 0 .. 10 {
                let pos = V3::new(rng.gen_range(0, 16),
                                  rng.gen_range(0, 16),
                                  0) + cpos.extend(0) * CHUNK_SIZE;
                tc.structure(|s| {
                    s.stable_plane(pid)
                     .pos(pos)
                     .template("rock");
                });
            }

            let mut grid = CellularGrid::new(CHUNK_SIZE.into());
            grid.init(|_| rng.gen_range(0, 10) < 5);
            for _ in 0 .. 5 {
                grid.step(|here, active, total| 2 * (here as u8 + active) > total);
            }

            for pos in Region2::sized(CHUNK_SIZE).points() {
                if (pos.x + pos.y) % 2 == 1 {
                    continue;
                }
                if !grid.get(pos) {
                    continue;
                }
                tc.structure(|s| {
                    s.stable_plane(pid)
                     .pos((pos + cpos * CHUNK_SIZE).extend(0))
                     .template("tree");
                });
            }

        }

        b.finish()
    }
}

const OP_INIT_PLANE: u32 =      0;
const OP_FORGET_PLANE: u32 =    1;
const OP_GEN_PLANE: u32 =       2;
const OP_GEN_CHUNK: u32 =       3;
const OP_SHUTDOWN: u32 =        4;

fn io_main(ctx: &mut forest::Context) -> io::Result<()> {
    let mut stdin = io::stdin();
    let mut stdout = io::stdout();

    loop {
        let opcode: u32 = try!(stdin.read_bytes());
        match opcode {
            OP_INIT_PLANE => {
                let (pid, mode): (Stable<PlaneId>, u32) = try!(stdin.read_bytes());
                info!("init plane: {:?}, mode = {}", pid, mode);
            },
            OP_FORGET_PLANE => {
                let pid: Stable<PlaneId> = try!(stdin.read_bytes());
                info!("forget plane: {:?}", pid);
            },
            OP_GEN_PLANE => unimplemented!(),
            OP_GEN_CHUNK => {
                let (_pid, cpos): (Stable<PlaneId>, V2) = try!(stdin.read_bytes());
                //let b = ctx.generate(pid, cpos);
                let b = ctx.generate(cpos);

                let mut f = Flat::new();
                f.flatten_bundle(&b);
                let bytes = f.to_bytes();
                let len = bytes.len();
                assert!(len <= u32::MAX as usize);

                try!(stdout.write_bytes(len as u32));
                try!(stdout.write_all(&bytes));
                try!(stdout.flush());
            },
            OP_SHUTDOWN => {
                info!("clean shutdown");
                break;
            },
            _ => panic!("unrecognized opcode: {}", opcode),
        }
    }

    Ok(())
}


fn load_config(path: &Path) -> (Data, vaults::Data, Storage) {
    let storage = Storage::new(&path.to_owned());

    let mut raw_bytes = Vec::new();
    storage.open_binary_data().read_to_end(&mut raw_bytes).unwrap();
    let data = Data::new(raw_bytes.into_boxed_slice());

    let mut raw_bytes = Vec::new();
    storage.open_vaults_data().read_to_end(&mut raw_bytes).unwrap();
    let vaults = vaults::Data::new(raw_bytes.into_boxed_slice());

    (data, vaults, storage)
}

fn main() {
    env_logger::init().unwrap();


    let args = env::args().collect::<Vec<_>>();
    let (data, vaults, _storage) = load_config(Path::new(&args[1]));


    let mut ctx = forest::Context::new(&data, &vaults, Stable::new(2), (0x123, 0x456));

    match io_main(&mut ctx) {
        Ok(()) => {},
        Err(e) => error!("io error: {:?}", e),
    }
}
