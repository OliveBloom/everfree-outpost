use server_types::*;
use rand::{XorShiftRng, Rng};


pub fn gen_hex_points_into(rng: &mut XorShiftRng,
                           spacing: i32,
                           bounds: Region<V2>,
                           points: &mut Vec<V2>,
                           overpack: usize) {
    let vstep = spacing * 866 / 1000;   // sqrt(3) / 2
    // Row 0 starts at y = 0.  Rows are numbered outward from there in + and - directions.
    // We have to do some funny adjustments to get "ceil" behavior, since division rounds
    // toward zero.
    fn div_ceil(a: i32, b: i32) -> i32 {
        let adj = if a >= 0 { b - 1 } else { 0 };
        (a + adj) / b
    }
    let row0 = div_ceil(bounds.min.y, vstep);
    let row1 = div_ceil(bounds.max.y, vstep);

    let col0_even = div_ceil(bounds.min.x, spacing);
    let col1_even = div_ceil(bounds.max.x, spacing);
    let col0_odd = div_ceil(bounds.min.x - spacing / 2, spacing);
    let col1_odd = div_ceil(bounds.max.x - spacing / 2, spacing);

    // Place initial points in cells of a hex grid
    for row in row0 .. row1 {
        let y = row * vstep;
        let (range, base) =
            if row % 2 == 0 { (col0_even .. col1_even, 0) }
            else { (col0_odd .. col1_odd, -spacing / 2) };

        for col in range {
            let x = base + col * spacing;

            let count = rng.gen_range(100, 200 + overpack) / 100;
            for _ in 0 .. count {
                // w/h are actually half the available width/height.
                let h = vstep / 2;
                let dy = rng.gen_range(-h, h);
                // dy = 0 -> w = spacing / 2
                // dy = h -> w = spacing / 4
                let w = spacing * (2 * h - dy.abs()) / (4 * h);
                let dx = rng.gen_range(-w, w);

                points.push(V2::new(x + dx, y + dy));
            }
        }
    }
}

#[allow(dead_code)]
pub fn gen_rect_points_into(rng: &mut XorShiftRng,
                            spacing: i32,
                            bounds: Region<V2>,
                            points: &mut Vec<V2>,
                            overpack: usize) {
    // Row 0 starts at y = 0.  Rows are numbered outward from there in + and - directions.
    // We have to do some funny adjustments to get "ceil" behavior, since division rounds
    // toward zero.
    fn div_ceil(a: i32, b: i32) -> i32 {
        let adj = if a >= 0 { b - 1 } else { 0 };
        (a + adj) / b
    }
    let row0 = div_ceil(bounds.min.y, spacing);
    let row1 = div_ceil(bounds.max.y, spacing);
    let col0 = div_ceil(bounds.min.x, spacing);
    let col1 = div_ceil(bounds.max.x, spacing);

    // Place initial points in cells of a hex grid
    for row in row0 .. row1 {
        let y = row * spacing;
        for col in col0 .. col1 {
            let x = col * spacing;

            let count = rng.gen_range(100, 200 + overpack) / 100;
            for _ in 0 .. count {
                // w/h are actually half the available width/height.
                let h = spacing / 2;
                let dy = rng.gen_range(-h, h);
                let w = spacing / 2;
                let dx = rng.gen_range(-w, w);

                points.push(V2::new(x + dx, y + dy));
            }
        }
    }
}

pub fn spread_points(points: &[V2],
                     spacing: i32) -> Vec<V2> {
    // We use a fixed-point accumulator to get more accurate velocities.
    const SCALE: i32 = 1024;

    let mut new_points = Vec::with_capacity(points.len());

    let max_dist2 = spacing * spacing * 4;

    for &p in points {
        let mut v: V2 = 0.into();

        for &q in points {
            let away = p - q;
            let dist2 = away.mag2();
            if dist2 == 0 || dist2 > max_dist2 {
                continue;
            }
            // Dividing by mag^2 normalizes, then divides by mag.  So the acceleration on the point
            // scales by 1/r.
            v = v + away * spacing * SCALE / dist2;
        }

        let offset = (v + SCALE / 2).div_floor(SCALE);
        new_points.push(p + offset);
    }

    new_points
}

pub fn scatter_points<F>(chunk_rng: F,
                         spacing: i32,
                         center: V2,
                         chunk_size: i32,
                         iters: usize) -> Vec<V2>
        where F: Fn(V2) -> XorShiftRng {
    let extra = (spacing * iters as i32 + chunk_size - 1) / chunk_size;
    let bounds_chunk = (Region::sized(1) + center).expand(extra.into());
    let bounds_tile = bounds_chunk * chunk_size;
    let center_bounds = (Region::sized(1) + center) * chunk_size;

    let buffer_area = spacing * spacing * 3 / 4;
    let total_area = bounds_tile.volume();

    let mut points = Vec::with_capacity((total_area / buffer_area) as usize);

    // Place initial points in cells of a hex grid
    for cpos in bounds_chunk.points() {
        let mut rng = chunk_rng(cpos);
        let chunk_bounds = (Region::sized(1) + cpos) * chunk_size;
        gen_hex_points_into(&mut rng, spacing, chunk_bounds, &mut points, 10)
    }

    // Push points away from each other
    for _ in 0 .. iters {
        let new_points = spread_points(&points, spacing);
        points = new_points;
    }

    points.into_iter().filter(|&p| center_bounds.contains(p)).collect()
}

