use std::collections::VecDeque;

use error::{Result};
use primitives::HashOutput;
use noise::*;


/// A `Protocol` instance handles bidirectional translation between a high-level (unencrypted)
/// message stream and a low-level (encrypted) message stream.
///
///                  Protocol
///                 +-------+
///           send  |       | outgoing
///         ------->|------>|------->
///     HIGH        |       |        LOW
///         <-------|<------|<-------
///           recv  |       | incoming
///                 +-------+
///
/// The `Protocol` consumes high-level messages to `send` and low-level `incoming` messages, and
/// emits high-level messages `recv`'ed and low-level `outgoing` messages.  There is no guaranteed
/// correspondence between high-level and low-level messages, so client code must check for both
/// `recv` and `outgoing` messages after every `send` and `incoming` event, as well as immediately
/// after initialization.
pub struct Protocol {
    state: State,
    events: VecDeque<OutputEvent>,
}

pub enum InputEvent<'a> {
    /// Indicates that the underlying (low-level) transport has been opened.
    Open,
    Send(&'a [u8]),
    Incoming(&'a [u8]),
}

pub enum OutputEvent {
    Recv(Box<[u8]>),
    Outgoing(Box<[u8]>),
    HandshakeFinished(Box<HashOutput>),
    Error(String),
    FatalError(String),
}

enum State {
    /// The handshake is in progress.  Any messages sent in this state will be queued until the
    /// handshake completes.
    Handshake {
        hs: HandshakeState,
        to_send: VecDeque<Box<[u8]>>,
    },

    /// The channel is fully open, capable of bidirectional communication.
    Open {
        cs_send: CipherState,
        cs_recv: CipherState,
    },

    /// A protocol error occurred.
    Error,
}

impl Protocol {
    pub fn new(initiator: bool) -> Protocol {
        match HandshakeState::new(initiator) {
            Ok(hs) => {
                // Normal case: start in handshake state
                Protocol {
                    state: State::Handshake {
                        hs: hs,
                        to_send: VecDeque::new(),
                    },
                    events: VecDeque::new(),
                }
            },

            Err(e) => {
                // If the handshake fails to initialize, start directly in State::Error with a
                // FatalError event containing the error message.
                let mut events = VecDeque::with_capacity(1);
                let msg = format!("handshake init failed: {:?}", e);
                events.push_back(OutputEvent::FatalError(msg));
                Protocol {
                    state: State::Error,
                    events: events,
                }
            },
        }
    }


    fn advance_handshake(&mut self) -> Result<()> {
        while let Some(msg) = self.state.handshake_send()? {
            self.events.push_back(OutputEvent::Outgoing(msg));
        }
        if let Some((cs_send, cs_recv, cb_token)) = self.state.handshake_finish() {
            self.events.push_back(OutputEvent::HandshakeFinished(Box::new(cb_token)));
            self.state = State::Open { cs_send, cs_recv };
        }
        Ok(())
    }

    fn input_internal(&mut self, evt: InputEvent) -> Result<()> {
        // Nearly all errors that arise in here are simply propagated to the caller (`input()`,
        // which will transition to `State::Error`).  This behavior is mandatory for handshake
        // errors:
        //
        //      "If any error is signaled by the DECRYPT() or DH() functions then the handshake has
        //      failed and the HandshakeState is deleted."
        //
        // For message decryption errors, we're permitted to drop the message and continue
        // processing.  In that case we push an `OutputEvent::Error` but do *not* propagate the
        // error to the caller.

        match evt {
            InputEvent::Open => {
                assert!(self.state.is_handshake());
                self.advance_handshake()?;
            },

            InputEvent::Send(msg) => {
                match self.state {
                    State::Handshake { ref mut to_send, .. } => {
                        to_send.push_back(msg.to_owned().into_boxed_slice());
                    },
                    State::Open { ref mut cs_send, .. } => {
                        let cipher = cs_send.encrypt_with_ad(&[], msg)?;
                        self.events.push_back(OutputEvent::Outgoing(cipher));
                    },
                    State::Error => {},
                }
            },

            InputEvent::Incoming(msg) => {
                let mut handshake = false;

                match self.state {
                    State::Handshake { ref mut hs, .. } => {
                        hs.recv_message(msg)?;
                        handshake = true;
                    },
                    State::Open { ref mut cs_recv, .. } => {
                        // "If DecryptWithAd() signals an error due to DECRYPT() failure, then the
                        // input message is discarded. The application may choose to delete the
                        // CipherState and terminate the session on such an error, or may continue
                        // to attempt communications.
                        let plain = match cs_recv.decrypt_with_ad(&[], msg) {
                            Ok(x) => x,
                            Err(e) => {
                                self.events.push_back(OutputEvent::Error(format!("{:?}", e)));
                                // Drop the message, but don't go to State::Error.
                                return Ok(());
                            },
                        };
                        self.events.push_back(OutputEvent::Recv(plain));
                    },
                    State::Error => {},
                }

                if handshake {
                    self.advance_handshake()?;
                }
            },
        }

        Ok(())
    }

    pub fn input(&mut self, evt: InputEvent) {
        match self.input_internal(evt) {
            Ok(()) => {},
            Err(e) => {
                self.events.push_back(OutputEvent::FatalError(format!("{:?}", e)));
                self.state = State::Error;
            },
        }
    }

    pub fn output(&mut self) -> Option<OutputEvent> {
        self.events.pop_front()
    }
}

impl State {
    fn is_handshake(&self) -> bool {
        match *self {
            State::Handshake { .. } => true,
            _ => false,
        }
    }

    fn handshake_send(&mut self) -> Result<Option<Box<[u8]>>> {
        match *self {
            State::Handshake { ref mut hs, .. } => {
                if hs.next_action() == HandshakeAction::Send {
                    Ok(Some(hs.send_message()?))
                } else {
                    Ok(None)
                }
            },
            _ => panic!("not in handshake state"),
        }
    }

    fn handshake_finish(&mut self) -> Option<(CipherState, CipherState, HashOutput)> {
        match *self {
            State::Handshake { ref mut hs, .. } => {
                if hs.next_action() == HandshakeAction::Done {
                    Some(hs.finish())
                } else {
                    None
                }
            },
            _ => panic!("not in handshake state"),
        }
    }
}
