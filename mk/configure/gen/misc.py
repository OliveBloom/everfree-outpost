
CONFIG_DEPS = set()

def config_deps():
    return sorted(CONFIG_DEPS)

def add_config_dep(path):
    CONFIG_DEPS.add(path)


def rules(i, r):
    r.rule('list_files', 'LIST',
            '$python3 $root/mk/misc/list_files.py $out $in',
            implicit_deps=('$root/mk/misc/list_files.py',))

    # Note: This rule expects two inputs, paths to both boot.py and game.py
    r.rule('gen_preboot', 'GEN',
            '$python3 $root/mk/misc/gen_preboot.py $in $out',
            implicit_deps=('$root/mk/misc/gen_preboot.py',))


def list_files(i, r, dest, paths):
    r.list_files.build((dest,), paths)

