import os
import yaml

from minitemplate import template

from configure.gen.misc import add_config_dep
from configure.util import join, maybe


def collect_atlas_names(i, src_dir):
    path = os.path.join(src_dir.replace('$root', i.root_dir), 'atlas.yaml')
    add_config_dep(path)
    with open(path) as f:
        y = yaml.load(f)

    filenames = []
    for name, config in y.items():
        if 'sheets' not in config:
            filenames.append('%s.png' % name)
        else:
            for i in range(config['sheets']):
                filenames.append('%s%d.png' % (name, i))
    return filenames

def rules(i):
    return template('''
        rule compile_data
            command = $
                PYTHONPATH=$root/src/gen:$root/src/minitemplate:$$PYTHONPATH $
                $python3 -m outpost_data.compiler $
                --data-dir $src_dir $
                --image-script-out $out_dir/gen_images.py $
                --data-script-out $out_dir/gen_data.py $
                --server-script-out $out_dir/game.py $
                --dep-file $out_dir/data_compiler.d
            depfile = $out_dir/data_compiler.d
            description = GEN $out

        rule run_gen_images
            command = $
                PYTHONPATH=$root/src/gen:$$PYTHONPATH $
                $python3 $in $
                --atlas-config $atlas_yaml $
                --asset-dir $asset_dir $
                --atlas-out-dir $out_dir/atlas $
                --index-out $out_dir/atlas.json $
                --dep-file $out_dir/gen_images.d $
                --cache-dir $out_dir/cache
            depfile = $out_dir/gen_images.d
            description = GEN IMAGES

        rule run_gen_data
            command = $
                PYTHONPATH=$root/src/gen:$$PYTHONPATH $
                $python3 $in $
                --atlas-index $out_dir/atlas.json $
                --mode $mode $
                --json-out-dir $out_dir/json $
                --dep-file $out_dir/gen_data.d
            depfile = $out_dir/gen_data.d
            description = GEN DATA


        # UI handling

        rule collect_ui_enums
            command = $python3 $root/src/gen/collect_ui_atlas_enums.py $in $out
            description = GEN $out

        rule sort_ui_data
            command = $python3 $root/src/gen/sort_ui_data.py $in $enum_name $out
            description = GEN $out


        # Binary defs

        rule gen_binary_defs
            command = $python3 $root/src/gen/gen_binary_defs.py $
                    --gen-phf=$b_native/gen_phf $mode $out $in
            description = GEN $out
            depfile = $out.d

    ''', **locals())

GAME_JSON_NAMES = (
        'structures',
        'blocks',
        'terrains',
        'items',
        'recipes',
        'recipe_crafting_classes',
        'sprites',
        'pony_layers',
        'pony_layer_anims',
        'extras',
        )

UI_JSON_NAMES = (
        'ui_cards',
        'ui_borders',
        'fonts',
        'inventory_layouts',
        )

MODE_JSON_NAMES = {
        'game': GAME_JSON_NAMES,
        'ui': UI_JSON_NAMES,
        }

MODE_RAW_JSON_NAMES = {
        'game': GAME_JSON_NAMES,
        'ui': tuple('%s_dict' % x for x in UI_JSON_NAMES),
        }

def compile(i, src_dir, out_dir, mode='game'):
    atlases = collect_atlas_names(i, src_dir)

    raw_json_names = MODE_RAW_JSON_NAMES[mode]

    return template('''
        build %{out_dir}/gen_images.py %{out_dir}/gen_data.py %{out_dir}/game.py: compile_data 
            src_dir = %{src_dir}
            out_dir = %{out_dir}

        build %{out_dir}/atlas.json $
                %for atlas in atlases% %{out_dir}/atlas/%{atlas} %end%: $
                run_gen_images %{out_dir}/gen_images.py
            atlas_yaml = %{src_dir}/atlas.yaml
            # Currently the game, ui, and test data shares one asset dir.
            asset_dir = $root/assets
            out_dir = %{out_dir}

        build %for name in raw_json_names% %{out_dir}/json/%{name}.json %end%: $
                run_gen_data %{out_dir}/gen_data.py | %{out_dir}/atlas.json
            out_dir = %{out_dir}
            mode = %{mode}

    ''', **locals())

def compile_ui(i, src_dir, out_dir):
    atlases = collect_atlas_names(i, src_dir)

    return compile(i, src_dir, out_dir, mode='ui') + template('''

        # UI handling

        build %{out_dir}/ui_enums.json: collect_ui_enums $
                $root/src/libclient/ui2/atlas.rs $
                | $root/src/gen/collect_ui_atlas_enums.py

        build %{out_dir}/json/ui_cards.json: sort_ui_data $
                %{out_dir}/json/ui_cards_dict.json %{out_dir}/ui_enums.json $
                | $root/src/gen/sort_ui_data.py
            enum_name = Card

        build %{out_dir}/json/ui_borders.json: sort_ui_data $
                %{out_dir}/json/ui_borders_dict.json %{out_dir}/ui_enums.json $
                | $root/src/gen/sort_ui_data.py
            enum_name = Border

        build %{out_dir}/json/fonts.json: sort_ui_data $
                %{out_dir}/json/fonts_dict.json %{out_dir}/ui_enums.json $
                | $root/src/gen/sort_ui_data.py
            enum_name = Font

        build %{out_dir}/json/inventory_layouts.json: sort_ui_data $
                %{out_dir}/json/inventory_layouts_dict.json %{out_dir}/ui_enums.json $
                | $root/src/gen/sort_ui_data.py
            enum_name = InventoryLayout

    ''', **locals())

def binary_defs(out_file, mode, game_dir=None, ui_dir=None):
    if game_dir is not None:
        game_deps = tuple('%s/json/%s.json' % (game_dir, x) for x in GAME_JSON_NAMES)
    else:
        # This is safe because gen_binary_defs.py will fail if it needs a file
        # that's not listed in `deps`.
        game_deps = ()

    if ui_dir is not None:
        ui_deps = tuple('%s/json/%s.json' % (ui_dir, x) for x in UI_JSON_NAMES)
    else:
        ui_deps = ()

    vault_deps = ('$b_data2/game/json/vaults.json',)

    if mode == 'client':
        deps = game_deps + ui_deps
    elif mode == 'server':
        deps = game_deps
    elif mode == 'vaults':
        deps = vault_deps
    else:
        assert False, 'bad binary_defs mode'

    return template('''
        build %out_file: gen_binary_defs $
            %{' '.join(deps)} $
            | $root/src/gen/gen_binary_defs.py $
              $b_native/gen_phf
            mode = %mode
    ''', **locals())

def pony_shading(r):
    r.rule('gen_pony_shading', 'GEN',
            '$python3 $root/util/calc_shading_coeffs.py $in >$out',
            implicit_deps=(
                '$root/util/calc_shading_coeffs.py',
                ))
    r.gen_pony_shading.build(('$b_data2/misc/pony_shading.inc',),
            ('$root/assets/misc/pony-shading-cube.png',))

def vaults(r):
    omx_util_deps = (
            '$root/src/gen/omx_util.py',
            '$b_data2/game/json/blocks.json',
            '$b_data2/game/json/structures.json',
            )

    r.rule('gen_vaults_json', 'GEN',
            '$python3 $root/src/gen/omx_util.py omx-to-json '
            '--in-dir $in --out-file $out '
            '--json-dir $b_data2/game/json '
            '--dep-file $out.d',
            implicit_deps=omx_util_deps,
            depfile='$out.d')
    r.gen_vaults_json.build(('$b_data2/game/json/vaults.json',),
            ('$root/data2/vaults',))

    r.rule('gen_vaults_tmx', 'GEN',
            '$python3 $root/src/gen/omx_util.py omx-to-tmx '
            '--in-dir $in --out-dir $out_dir '
            '--json-dir $b_data2/game/json '
            '--dep-file $out_dir.d --stamp-file $out_dir.stamp '
            '--tileset-rel-dir tilesets',
            implicit_deps=omx_util_deps,
            depfile='$out_dir.d',
            required_args=('out_dir',))
    r.gen_vaults_tmx.build(('$b_misc/vault_pack.stamp',),
            ('$root/data2/vaults',),
            out_dir='$b_misc/vault_pack')

    r.rule('gen_vaults_tilesets', 'GEN',
            '$python3 $root/src/gen/gen_tiled_tilesets.py '
            '--json-dir $b_data2/game/json '
            '--atlas-dir $b_data2/game/atlas '
            '--out-dir $out_dir',
            implicit_deps=omx_util_deps + (
                '$b_data2/game/atlas/structures0.png',
                '$b_data2/game/atlas/terrain.png',
                '$b_data2/game/json/terrains.json',
                ),
            required_args=('out_dir',))
    r.gen_vaults_tilesets.build(
            (
                '$b_misc/vault_pack/tilesets/blocks.tsx',
                '$b_misc/vault_pack/tilesets/structures.tsx',
            ),
            (),
            out_dir='$b_misc/vault_pack/tilesets')

    r.copy_file.build(('$b_misc/vault_pack/README.md',),
            ('$root/doc/vault-pack.md',))
