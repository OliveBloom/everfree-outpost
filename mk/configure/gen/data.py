import os

from minitemplate import template

from configure.util import join, maybe


def rules(i):
    return template('''
        rule process_font
            command = $python3 $root/src/gen/process_font.py $
                --font-image-in=$in $
                $extra_args $
                --font-image-out=$out_img $
                --font-metrics-out=$out_metrics
            description = GEN $out_img

        rule stack_fonts
            command = $python3 $root/src/gen/stack_fonts.py $
                $out_img $out_metrics $out_rust $in
            description = GEN $out_img

        rule gen_server_json
            command = $python3 $root/src/gen/gen_server_json.py $in >$out
            description = GEN $out

        rule gen_credits
            command = $python3 $root/src/gen/gen_credits.py $root $out $dep_files
            description = GEN $out

    ''', **locals())

def font(basename, src_img, charset_args='--first-char=0x21', extra_args=''):
    out_img = '$b_data/fonts/' + basename + '.png'
    out_metrics = '$b_data/fonts/' + basename + '_metrics.json'

    return template('''
        build %out_img %out_metrics: process_font %src_img $
            | $root/src/gen/process_font.py
            extra_args = %charset_args %extra_args
            out_img = %out_img
            out_metrics = %out_metrics
    ''', **locals())

def font_stack(out_base, in_basenames):
    out_img = out_base + '.png'
    out_metrics = out_base + '_metrics.json'
    out_rust = out_base + '_metrics.rs'

    return template('''
        build %out_img %out_metrics %out_rust: stack_fonts $
            %for name in in_basenames
                $b_data/fonts/%name.png $
                $b_data/fonts/%{name}_metrics.json $
            %end
            | $root/src/gen/stack_fonts.py
            out_img = %out_img
            out_metrics = %out_metrics
            out_rust = %out_rust
    ''', **locals())

def server_json(out_json):
    return template('''
        build %out_json: gen_server_json $b_data/outpost.pack $
            | $root/src/gen/gen_server_json.py
    ''', **locals())

def pack():
    extra_data = (
            # TODO: depend on atlases directly.  Right now we rely on
            # client_data.bin implying they've been built
            '$b_data2/client_data.bin',
            '$b_data2/misc/pony_shading.inc',
            )

    return template('''
        rule build_pack
            command = $python3 $root/mk/misc/make_pack.py $root $b_data $b_data/outpost.pack
            description = PACK
            depfile = $b_data/outpost.pack.d

        build $b_data/outpost.pack: build_pack $
            | $root/mk/misc/make_pack.py $
              %{' '.join(extra_data)} $
    ''', **locals())

def credits(out_path):
    return template('''
        build %out_path: gen_credits $
            | $b_data/stamp $b_data/outpost.pack $
              $root/src/gen/gen_credits.py
            dep_files = $b_data/data.d $b_data/outpost.pack.d
    ''', **locals())
