import fnmatch


class Rule:
    def __init__(self,
            name,
            short_name,
            command,
            implicit_deps=None,
            input_patterns=None,
            output_patterns=None,
            required_args=None,
            **kwargs):
        self.name = name

        parts = kwargs.copy()
        parts['command'] = command
        parts['description'] = '%s $out' % short_name

        self.code = 'rule %s\n' % name + \
                '\n'.join('    %s = %s' % (k, v) for (k, v) in parts.items())

        self.implicit_deps = tuple(implicit_deps) if implicit_deps is not None else None
        self.input_patterns = input_patterns
        self.output_patterns = output_patterns
        self.required_args = required_args

    def build(self, outputs, inputs, implicit_inputs=(), **kwargs):
        if self.input_patterns is not None:
            assert len(inputs) == len(self.input_patterns)
            for f, p in zip(inputs, self.input_patterns):
                if not fnmatch.fnmatch(f, p):
                    raise ValueError('input %r does not match pattern %r' % (f, p))

        if self.output_patterns is not None:
            assert len(outputs) == len(self.output_patterns)
            for f, p in zip(outputs, self.output_patterns):
                if not fnmatch.fnmatch(f, p):
                    raise ValueError('output %r does not match pattern %r' % (f, p))

        if self.required_args is not None:
            for k in self.required_args:
                if k not in kwargs:
                    raise KeyError('missing required argument %r' % k)

        header = 'build %s: %s %s' % (' '.join(outputs), self.name, ' '.join(inputs))
        implicit_inputs = tuple(implicit_inputs) + (self.implicit_deps or ())
        if len(implicit_inputs) > 0:
            header += ' | %s' % (' '.join(implicit_inputs))
        header += '\n'

        return header + '\n'.join('    %s = %s' % (k, v) for (k, v) in sorted(kwargs.items()))


class BoundRule:
    def __init__(self, rule, ctx):
        self.rule = rule
        self.ctx = ctx

    def build(self, outputs, inputs, **kwargs):
        key = (
                self.rule.name,
                tuple(sorted(outputs)),
                tuple(sorted(inputs)),
                tuple(sorted(kwargs.items()))
                )

        all_new = True
        all_old = True
        for o in outputs:
            old = self.ctx.built_outputs.get(o)
            if old is not None:
                if old != key:
                    raise ValueError('multiple build steps for %r: %r != %r' %
                            (o, old, key))
                all_new = False
            else:
                all_old = False

        if all_new:
            self.ctx.emit(self.rule.build(outputs, inputs, **kwargs))
            for o in outputs:
                self.ctx.built_outputs[o] = key
        elif all_old:
            pass
        else:
            raise ValueError('output list %r includes both new and old outputs' % outputs)


class Context:
    def __init__(self):
        self._code = None
        self.code_parts = []
        self.rules = {}
        self.bound_rules = {}
        self.built_outputs = {}
        self.filesets = {}
        self.providers = []

    @property
    def code(self):
        if self._code is None:
            self._code = '\n\n'.join(self.code_parts)
        return self._code

    def emit(self, code):
        self.code_parts.append(code)
        self._code = None

    def var(self, name, value):
        self.emit('%s = %s' % (name, value))

    def rule(self, *args, **kwargs):
        r = Rule(*args, **kwargs)
        assert r.name not in self.rules
        self.rules[r.name] = r
        self.bound_rules[r.name] = BoundRule(r, self)
        self.emit(r.code)
        return self.bound_rules[r.name]

    def __getattr__(self, k):
        return self.bound_rules[k]

    def file(self, s, f):
        '''Add `f` to named fileset `s`.'''
        if s not in self.filesets:
            self.filesets[s] = []
        self.filesets[s].append(f)

    def require(self, f):
        if f in self.built_outputs:
            return f

        for (re, func) in self.providers:
            m = re.match(f)
            if m:
                func(f, m)
                if f in self.built_outputs:
                    break
        else:
            raise RuntimeError('no provider can build %r' % f)

        return f

    def provider(self, re, func):
        self.providers.append((re, func))


