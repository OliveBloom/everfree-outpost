from itertools import count
import json
import os
import struct
import sys

def main(src_dir, build_dir, out_file):
    index = []
    paths = []
    hidden_deps = set()

    src = lambda path: os.path.join(src_dir, path)
    build2 = lambda path: os.path.join(build_dir, '..', 'data2', path)

    def add(ty, name, path, hide_dep=False):
        size = os.stat(path).st_size

        index.append({
                'name': name,
                'length': size,
                'type': ty,
                })
        paths.append(path)

        if hide_dep:
            hidden_deps.add(path)

    add('image', 'terrain',     build2('game/atlas/terrain.png'))
    add('image', 'items_img',   build2('game/atlas/items.png'))
    add('image', 'ui',          build2('ui/atlas/ui.png'))

    add('binary', 'client_data',        build2('client_data.bin'))

    def add_shader(name):
        add('text', name, src('assets/shaders/%s' % name))

    add_shader('blit_post.frag')
    add_shader('blit_output.frag')
    add_shader('blit_outline.frag')
    add_shader('blit_fullscreen.vert')
    add_shader('blit_fullscreen_flip.vert')
    add_shader('blit_region.vert')

    add_shader('terrain2.frag')
    add_shader('terrain2.vert')
    add_shader('structure2.vert')
    add_shader('structure2.frag')
    add_shader('structure2_shadow.vert')
    add_shader('structure2_shadow.frag')
    add_shader('ground_tile.vert')
    add_shader('solid_color.frag')
    add_shader('solid_color_masked.frag')
    add_shader('light2.frag')
    add_shader('light2.vert')
    add_shader('entity2.frag')
    add_shader('entity2.vert')
    add_shader('entity2_ui.frag')
    add_shader('entity2_ui.vert')
    add_shader('prelude.inc')
    add_shader('slicing.inc')
    add_shader('colorspace.inc')

    add_shader('color_plot_xy.frag')
    add_shader('color_plot_z.frag')
    add_shader('color_slider.frag')

    add_shader('debug_graph.vert')
    add_shader('debug_graph.frag')

    add_shader('ui_blit2.vert')
    add_shader('ui_blit2.frag')

    # Generated shader include file
    add('text', 'pony_shading.inc', build2('misc/pony_shading.inc'))


    add('image', 'structures0', build2('game/atlas/structures0.png'))
    add('image', 'sprites0', build2('game/atlas/sprites0.png'))


    # Generate the pack containing the files added above.

    offset = 0
    for entry in index:
        entry['offset'] = offset
        offset += entry['length']


    index_str = json.dumps(index)
    index_len = len(index_str.encode())

    with open(out_file, 'wb') as f:
        f.write(struct.pack('<I', len(index_str.encode())))
        f.write(index_str.encode())

        for (entry, path) in zip(index, paths):
            total_len = 0
            with open(path, 'rb') as f2:
                while True:
                    chunk = f2.read(4096)
                    f.write(chunk)
                    total_len += len(chunk)
                    if len(chunk) == 0:
                        break

            assert total_len == entry['length'], \
                    'file %r changed length during packing' % entry['name']

    # Emit dependencies
    with open(out_file + '.d', 'w') as f:
        f.write('%s: \\\n' % out_file)
        for path in paths:
            if path in hidden_deps:
                continue
            f.write('    %s \\\n' % path)

if __name__ == '__main__':
    src_dir, build_dir, out_file = sys.argv[1:]
    main(src_dir, build_dir, out_file)
